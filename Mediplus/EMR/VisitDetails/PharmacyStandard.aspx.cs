﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_BAL;

namespace Mediplus.EMR.VisitDetails
{
    public partial class PharmacyStandard : System.Web.UI.Page
    {
        static string strSessionDeptId, strUserCode;
        static Int32 intSrcType;
        static string strSearchOPtion, strDrugSrcType;
        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindScreenCustomization()
        {
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 ";

            Criteria += " AND SCREENNAME='EMR_PHY' ";

            DataSet DS = new DataSet();
            DS = objCom.ScreenCustomizationGet(Criteria);

            ViewState["PHY_SEARCH_LIST"] = "0";
            ViewState["EMR_DRUG_DISPLAY_FORMAT"] = "A";
            strDrugSrcType = "A";
            ViewState["EMR_DRUG_SEARCH_OPTION"] = "AN";
            strSearchOPtion = "AN";

            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    if (Convert.ToString(DR["SEGMENT"]) == "PHY_SEARCH_LIST")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            ViewState["PHY_SEARCH_LIST"] = Convert.ToString(DR["CUST_VALUE"]);
                        }
                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "EMR_DRUG_DISPLAY_FORMAT")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            ViewState["EMR_DRUG_DISPLAY_FORMAT"] = Convert.ToString(DR["CUST_VALUE"]);
                            strDrugSrcType = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "EMR_DRUG_SEARCH_OPTION")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            ViewState["EMR_DRUG_SEARCH_OPTION"] = Convert.ToString(DR["CUST_VALUE"]);
                            strSearchOPtion = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }
                }
            }


        }

        void BindTime()
        {
            int AppointmentInterval = 15;
            int AppointmentStart = 0;
            int AppointmentEnd = 23;
            int index = 0;

            for (int i = AppointmentStart; i <= AppointmentEnd; i++)
            {
                string strHour = Convert.ToString(index);
                if (index <= 9)
                {
                    strHour = "0" + Convert.ToString(index);
                }




                drpSheduleHour.Items.Insert(index, Convert.ToString(strHour));
                drpSheduleHour.Items[index].Value = Convert.ToString(strHour);


                index = index + 1;

            }
            index = 1;
            int intCount = 0;
            Int32 intMin = AppointmentInterval;





            drpSheduleMin.Items.Insert(0, Convert.ToString("00"));
            drpSheduleMin.Items[0].Value = Convert.ToString("00");


            for (int j = AppointmentInterval; j < 60; j++)
            {



                drpSheduleMin.Items.Insert(index, Convert.ToString(j - intCount));
                drpSheduleMin.Items[index].Value = Convert.ToString(j - intCount);


                j = j + AppointmentInterval;
                index = index + 1;
                intCount = intCount + 1;

            }

        }

        void BindData()
        {

            string SearchFilter = txtSearch.Text.Trim();
            DataSet ds = new DataSet();
            CommonBAL dbo = new CommonBAL();
            if (drpSrcType.SelectedIndex == 0)
            {
                //ds = dbo.HaadServicessListGet("Pharmacy", SearchFilter, "", Convert.ToString(Session["User_DeptID"]));

                string Criteria = " 1=1 ";
                if (radDrugSrcType.SelectedValue == "T")
                {
                    Criteria += " AND HHS_PRODUCTNAME IS NOT NULL AND HHS_PRODUCTNAME <>'' ";

                }

                if (SearchFilter != "")
                {
                    if (Convert.ToString(ViewState["EMR_DRUG_SEARCH_OPTION"]).ToUpper() == "ST")
                    {
                        if (radDrugSrcType.SelectedValue == "A")
                        {
                            Criteria += " AND  (HHS_CODE like '" + SearchFilter + "%' OR HHS_DESCRIPTION  like '" + SearchFilter + "%'" +
                                           " OR HHS_PRODUCTNAME  like '" + SearchFilter + "%')";
                        }
                        else if (radDrugSrcType.SelectedValue == "G")
                        {
                            Criteria += " AND  (HHS_CODE like '" + SearchFilter + "%' OR HHS_DESCRIPTION  like '" + SearchFilter + "%')";
                        }
                        else if (radDrugSrcType.SelectedValue == "T")
                        {
                            Criteria += " AND  (HHS_CODE like '" + SearchFilter + "%'  OR HHS_PRODUCTNAME  like '" + SearchFilter + "%') ";
                        }
                        else
                        {
                            Criteria += " AND  (HHS_CODE like '" + SearchFilter + "%' OR HHS_DESCRIPTION  like '" + SearchFilter + "%'" +
                                         " OR HHS_PRODUCTNAME  like '" + SearchFilter + "%')";
                        }
                    }
                    else if (Convert.ToString(ViewState["EMR_DRUG_SEARCH_OPTION"]).ToUpper() == "AN")
                    {
                        if (radDrugSrcType.SelectedValue == "A")
                        {
                            Criteria += " AND  (HHS_CODE like '%" + SearchFilter + "%' OR HHS_DESCRIPTION  like '%" + SearchFilter + "%'" +
                                           " OR HHS_PRODUCTNAME  like '%" + SearchFilter + "%')";
                        }
                        else if (radDrugSrcType.SelectedValue == "G")
                        {
                            Criteria += " AND  (HHS_CODE like '%" + SearchFilter + "%' OR HHS_DESCRIPTION  like '%" + SearchFilter + "%') ";
                        }
                        else if (radDrugSrcType.SelectedValue == "T")
                        {
                            Criteria += " AND  (HHS_CODE like '%" + SearchFilter + "%'  OR HHS_PRODUCTNAME  like '%" + SearchFilter + "%') ";
                        }
                        else
                        {
                            Criteria += " AND  (HHS_CODE like '%" + SearchFilter + "%' OR HHS_DESCRIPTION  like '%" + SearchFilter + "%'" +
                                           " OR HHS_PRODUCTNAME  like '%" + SearchFilter + "%')";
                        }
                    }
                    else if (Convert.ToString(ViewState["EMR_DRUG_SEARCH_OPTION"]).ToUpper() == "EX")
                    {
                        if (radDrugSrcType.SelectedValue == "A")
                        {
                            Criteria += " AND  (HHS_CODE = '" + SearchFilter + "' OR HHS_DESCRIPTION  = '" + SearchFilter + "'" +
                                           " OR HHS_PRODUCTNAME  = '" + SearchFilter + "')";
                        }
                        else if (radDrugSrcType.SelectedValue == "G")
                        {
                            Criteria += " AND  (HHS_CODE = '" + SearchFilter + "' OR HHS_DESCRIPTION  = '" + SearchFilter + "') ";
                        }
                        else if (radDrugSrcType.SelectedValue == "T")
                        {
                            Criteria += " AND  (HHS_CODE = '" + SearchFilter + "' OR HHS_PRODUCTNAME  = '" + SearchFilter + "') ";
                        }
                        else
                        {
                            Criteria += " AND  (HHS_CODE = '" + SearchFilter + "' OR HHS_DESCRIPTION  = '" + SearchFilter + "'" +
                                                                       " OR HHS_PRODUCTNAME  = '" + SearchFilter + "')";
                        }
                    }
                }
                ds = dbo.HAADDrugListGet(Criteria, radDrugSrcType.SelectedValue);




            }
            else
            {
                string Criteria = " 1=1 ";

                if (txtSearch.Text.Trim() != "")
                {
                    Criteria += " AND( EDF_CODE like'%" + txtSearch.Text.Trim() + "%' OR  EDF_NAME  like'%" + txtSearch.Text.Trim() + "%')";
                }
                Criteria += " AND EDF_TYPE='PHY' AND ( EDF_DR_ID='" + Convert.ToString(Session["User_Code"]) + "' OR EDF_DEP_ID='" + Convert.ToString(Session["User_DeptID"]) + "' )";

                ds = dbo.FavoritesGet(Criteria);
            }
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvServicess.DataSource = ds;
                gvServicess.DataBind();

                if (Convert.ToString(Session["EPM_STATUS"]).ToUpper() == "Y" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                {
                    gvServicess.Columns[0].Visible = false;

                }
                //HIDE THE SAVE OPTION FOR NEXT DAY OF THE VISIT
                if (Convert.ToString(Session["EMRCurrentDateDiff"]) != "0" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                {
                    gvServicess.Columns[0].Visible = false;
                }
                if (Convert.ToString(Session["EMR_VISIT_DTLS_EDIT_ALL_USERS"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "DOCTORS" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                {
                    gvServicess.Columns[0].Visible = false;
                }

            }
            else
            {
                gvServicess.DataBind();
            }
        }

        Boolean CheckFavorite(string Code)
        {
            DataSet ds = new DataSet();
            CommonBAL dbo = new CommonBAL();

            string Criteria = " 1=1 ";

            Criteria += " AND EDF_CODE='" + Code + "'";

            Criteria += " AND EDF_TYPE='PHY' AND ( EDF_DR_ID='" + Convert.ToString(Session["User_Code"]) + "' OR EDF_DEP_ID='" + Convert.ToString(Session["User_DeptID"]) + "')";

            ds = dbo.FavoritesGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            return false;
        }

        void BindPharmacy()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND EPP_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND EPP_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

            if (Convert.ToString(Session["EMR_ID"]) == "")
            {
                goto FunEnd;
            }


            DataSet DS = new DataSet();
            EMR_PTPharmacy objPro = new EMR_PTPharmacy();

            // if (drpTemplate.SelectedIndex == 0)
            // {
            //    DS = objPro.PharmacyGet(Criteria);
            //}
            //else
            //{
            //    Criteria = " 1=1 ";
            //    Criteria += " AND EPP_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            //    Criteria += " AND ET_CODE='" + drpTemplate.SelectedValue + "'";

            //    DS = objPro.PharmacyTemplatGe(Criteria);
            //}

            DS = objPro.PharmacyGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                gvPharmacy.DataSource = DS;
                gvPharmacy.DataBind();

                if (Convert.ToString(Session["EPM_STATUS"]).ToUpper() == "Y" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                {
                    gvPharmacy.Columns[0].Visible = false;

                }
                //HIDE THE SAVE OPTION FOR NEXT DAY OF THE VISIT
                if (Convert.ToString(Session["EMRCurrentDateDiff"]) != "0" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                {
                    gvPharmacy.Columns[0].Visible = false;
                }
                if (Convert.ToString(Session["EMR_VISIT_DTLS_EDIT_ALL_USERS"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "DOCTORS" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                {
                    gvPharmacy.Columns[0].Visible = false;
                }
            }
            else
            {
                gvPharmacy.DataBind();
            }
        FunEnd: ;
        }

        Boolean CheckPharmacy(string Code)
        {
            string Criteria = " 1=1 ";

            Criteria += " AND EPP_LAB_CODE='" + Code + "'";
            Criteria += " AND EPP_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND EPP_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";




            DataSet DS = new DataSet();
            EMR_PTPharmacy objPro = new EMR_PTPharmacy();
            DS = objPro.PharmacyGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                return true;
            }

            return false;
        }

        void BindDossage()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 AND EPT_ACTIVE=1 ";
            Criteria += " AND EPT_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' ";
            DS = objCom.EMRTimeGet(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                drpDossage.DataSource = DS;
                drpDossage.DataTextField = "EPT_NAME";
                drpDossage.DataValueField = "EPT_NAME";
                drpDossage.DataBind();
            }
            drpDossage.Items.Insert(0, "Select When");
            drpDossage.Items[0].Value = "";

            // objCom.BindMasterDropdown(Criteria, out drpDossage);

        }

        void BindTaken()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 AND EPT_ACTIVE=1 ";
            Criteria += " AND EPT_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' ";
            DS = objCom.EMRTakenGet(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                drpTaken.DataSource = DS;
                drpTaken.DataTextField = "EPT_NAME";
                drpTaken.DataValueField = "EPT_CODE";
                drpTaken.DataBind();
            }
            drpTaken.Items.Insert(0, "Select How");
            drpTaken.Items[0].Value = "";

            // objCom.BindMasterDropdown(Criteria, out drpDossage);

        }

        void BindRoute()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 AND EPR_ACTIVE=1 ";
            Criteria += " AND EPR_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' ";
            DS = objCom.EMRRouteGet(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                drpRoute.DataSource = DS;
                drpRoute.DataTextField = "EPR_NAME";
                drpRoute.DataValueField = "EPR_CODE";
                drpRoute.DataBind();
            }
            drpRoute.Items.Insert(0, "Select Route");
            drpRoute.Items[0].Value = "";

            // objCom.BindMasterDropdown(Criteria, out drpDossage);

        }

        void BindTemplate()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1  and ET_TYPE='Pharmacy'  ";
            Criteria += " AND ET_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' ";

            Criteria += " AND ( ET_DR_CODE='" + Convert.ToString(Session["HPV_DR_ID"]) + "' OR ET_Apply=1 )";

            if (GlobalValues.FileDescription.ToUpper() != "MAMPILLY")
            {
                Criteria += " AND ET_DEP_ID='" + Convert.ToString(Session["HPV_DEP_NAME"]) + "'";
            }

            DS = objCom.TemplatesGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                drpTemplate.DataSource = DS;
                drpTemplate.DataTextField = "ET_NAME";
                drpTemplate.DataValueField = "ET_CODE";
                drpTemplate.DataBind();
            }
            drpTemplate.Items.Insert(0, "Select Template");
            drpTemplate.Items[0].Value = "";

            // objCom.BindMasterDropdown(Criteria, out drpDossage);

        }

        void BindHistory()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 ";
            Criteria += " AND EPM_PT_ID = '" + Convert.ToString(Session["EMR_PT_ID"]) + "' ";
            Criteria += " AND EPM_ID   !='" + Convert.ToString(Session["EMR_ID"]) + "' ";


            DS = objCom.PharmacyHistoryGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                drpHistory.DataSource = DS;
                drpHistory.DataTextField = "Name";
                drpHistory.DataValueField = "Code";
                drpHistory.DataBind();
            }
            drpHistory.Items.Insert(0, "----Select History---");
            drpHistory.Items[0].Value = "0";

            /*
            DataSet DS = new DataSet();

            if (GlobalValues.FileDescription.ToUpper() == "REDCRESCENT".ToUpper())
            {
                EMR_PTPharmacy objPTPhar = new EMR_PTPharmacy();

                DS = objPTPhar.WEMR_spS_GetPharmacyHistoryList(Convert.ToString(Session["EMR_PT_ID"]));
                if (DS.Tables[0].Rows.Count > 0)
                {
                    drpHistory.Items.Clear();

                    drpHistory.Items.Insert(0, "Use Prescriptions");
                    drpHistory.Items[0].Value = "0";

                    for (int j = 0; j <= DS.Tables[0].Rows.Count - 1; j++)
                    {

                        drpHistory.Items.Insert(1, Convert.ToString(DS.Tables[0].Rows[j]["EPM_Date"]) + " - " + Convert.ToString(DS.Tables[0].Rows[j]["PatientMasterID"]) + " - " + Convert.ToString(DS.Tables[0].Rows[j]["DoctorName"]));
                        drpHistory.Items[1].Value = Convert.ToString(DS.Tables[0].Rows[j]["PatientMasterID"]) + "-" + Convert.ToString(DS.Tables[0].Rows[j]["BranchID"]);

                    }
                }
            }
            else
            {

                string Criteria = " 1=1 ";
                Criteria += " AND EPM_PT_ID='" + Convert.ToString(Session["EMR_PT_ID"]) + "'";


                EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();
                DS = objEmrPTMast.GetEMR_PTMaster(Criteria);
                if (DS.Tables[0].Rows.Count > 0)
                {
                    drpHistory.Items.Clear();

                    drpHistory.Items.Insert(0, "Use Prescriptions");
                    drpHistory.Items[0].Value = "0";

                    for (int j = 0; j <= DS.Tables[0].Rows.Count - 1; j++)
                    {

                        drpHistory.Items.Insert(1, Convert.ToString(DS.Tables[0].Rows[j]["EPM_DateDesc"]) + " - " + Convert.ToString(DS.Tables[0].Rows[j]["EPM_ID"]) + " - " + Convert.ToString(DS.Tables[0].Rows[j]["EPM_DR_NAME"]));
                        drpHistory.Items[1].Value = Convert.ToString(DS.Tables[0].Rows[j]["EPM_ID"]) + "-" + Convert.ToString(DS.Tables[0].Rows[j]["EPM_BRANCH_ID"]);

                    }
                }

            }
             * 
             * */
        }

        void BindPharmacyHistory(string EMR_ID)
        {
            string Criteria = " 1=1 ";
            Criteria += " AND EPP_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND EPP_ID='" + EMR_ID + "'";
            DataSet DS = new DataSet();
            EMR_PTPharmacy objPro = new EMR_PTPharmacy();

            DS = objPro.PharmacyGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                gvHistory.DataSource = DS;
                gvHistory.DataBind();

            }
            else
            {
                gvHistory.DataBind();
            }



        }


        void BindNurse()
        {
            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();

            string Criteria = " 1=1  AND HSFM_SF_STATUS='Present' AND HSFM_CATEGORY='Nurse' ";
            Criteria += "  AND HSFM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' ";

            DS = objCom.GetStaffMaster(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                drpWardNurse.DataSource = DS;
                drpWardNurse.DataTextField = "FullName";
                drpWardNurse.DataValueField = "HSFM_STAFF_ID";
                drpWardNurse.DataBind();


            }
            drpWardNurse.Items.Insert(0, "--- Select ---");
            drpWardNurse.Items[0].Value = "";



        }


        void BindPharmacySchedul()
        {

            string Criteria = " 1=1 ";
            Criteria += " AND EPPS_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            //  Criteria += " AND EPPS_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
            Criteria += " AND EPPS_ID IN (SELECT EPM_ID FROM EMR_PT_MASTER WHERE EPM_PT_ID='" + Convert.ToString(Session["EMR_PT_ID"]) + "')";

            Criteria += " AND EPPS_PHY_CODE='" + Convert.ToString(ViewState["SchedulePhyCode"]) + "'";
            DataSet DS = new DataSet();
            EMR_PTPharmacy objPro = new EMR_PTPharmacy();
            DS = objPro.PharmacyScheduleGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                gvPhySchedule.DataSource = DS;
                gvPhySchedule.DataBind();

            }
            else
            {
                gvPhySchedule.DataBind();
            }

        }


        void GetHaadPrescriptionDtls(string HaadCode)
        {

            txtDuration.Text = "";

            if (drpDurationType.Items.Count > 0)
                drpDurationType.SelectedIndex = 0;

            if (drpDossage.Items.Count > 0)
                drpDossage.SelectedIndex = 0;

            if (drpRoute.Items.Count > 0)
                drpRoute.SelectedIndex = 0;


            txtRefil.Text = "";

            txtQty.Text = "";

            txtDossage1.Text = "";
            if (drpTaken.Items.Count > 0)
                drpTaken.SelectedIndex = 0;


            string Criteria = " 1=1  AND HHS_TYPE='DRUG' AND HHS_TYPE_VALUE=5"; //AND HHS_STATUS='ACTIVE' 
            Criteria += " AND HHS_CODE='" + HaadCode + "'";


            CommonBAL objCom = new CommonBAL();
            DataSet DSHaad = new DataSet();
            DSHaad = objCom.HaadServiceGet(Criteria, "", "1");

            if (DSHaad.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DSHaad.Tables[0].Rows)
                {
                    if (DR.IsNull("HHS_DURATION") == false && Convert.ToString(DR["HHS_DURATION"]) != "")
                    {
                        txtDuration.Text = Convert.ToString(DR["HHS_DURATION"]);
                    }
                    if (DR.IsNull("HHS_DURATION_TYPE") == false && Convert.ToString(DR["HHS_DURATION_TYPE"]) != "")
                    {
                        drpDurationType.SelectedValue = Convert.ToString(DR["HHS_DURATION_TYPE"]);
                    }
                    if (DR.IsNull("HHS_DOSAGE") == false && Convert.ToString(DR["HHS_DOSAGE"]) != "")
                    {
                        drpDossage.SelectedValue = Convert.ToString(DR["HHS_DOSAGE"]);
                    }
                    if (DR.IsNull("HHS_ROUTE") == false && Convert.ToString(DR["HHS_ROUTE"]) != "")
                    {
                        drpRoute.SelectedValue = Convert.ToString(DR["HHS_ROUTE"]);
                    }
                    if (DR.IsNull("HHS_REFILL") == false && Convert.ToString(DR["HHS_REFILL"]) != "")
                    {
                        txtRefil.Text = Convert.ToString(DR["HHS_REFILL"]);
                    }
                    if (DR.IsNull("HHS_QTY") == false && Convert.ToString(DR["HHS_QTY"]) != "")
                    {
                        txtQty.Text = Convert.ToString(DR["HHS_QTY"]);
                    }
                    if (DR.IsNull("HHS_DOSAGE1") == false && Convert.ToString(DR["HHS_DOSAGE1"]) != "")
                    {
                        txtDossage1.Text = Convert.ToString(DR["HHS_DOSAGE1"]);
                    }
                    if (DR.IsNull("HHS_TIME") == false && Convert.ToString(DR["HHS_TIME"]) != "")
                    {
                        drpTaken.SelectedValue = Convert.ToString(DR["HHS_TIME"]);
                    }

                }
            }

        }



        Boolean CheckPassword(string UserID, string Password)
        {
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 ";
            Criteria += " AND HUM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HUM_REMARK ='" + UserID + "' AND   HUM_USER_PASS= '" + Password + "'";

            DataSet ds = new DataSet();
            //ds = dbo.UserMasterGet(drpBranch.SelectedValue, drpUsers.SelectedValue , txtPassword.Text);
            ds = objCom.UserMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            return false;
        }

        void ClearFavorite()
        {

            ViewState["Code"] = "";
            ViewState["Description"] = "";
            ViewState["Price"] = "";
            ViewState["gvServSelectIndex"] = "";
        }


        void ClearPhar()
        {
            txtServCode.Text = "";
            txtServName.Text = "";
            txtDuration.Text = "";

            if (drpDurationType.Items.Count > 0)
                drpDurationType.SelectedIndex = 0;
            // txtQty.Text = "";
            if (drpDossage.Items.Count > 0)
                drpDossage.SelectedIndex = 0;

            if (drpTaken.Items.Count > 0)
                drpTaken.SelectedIndex = 0;

            if (drpRoute.Items.Count > 0)
                drpRoute.SelectedIndex = 0;

            txtDossage1.Text = "";

            txtRefil.Text = "";
            //  txtFromDate.Text = "";
            // txtToDate.Text = "";

            chkLifeLong.Checked = false;
            txtRemarks.Text = "";

        }

        void ClearPhyShedule()
        {
            // txtSheduleDate.Text =
            drpSheduleHour.SelectedIndex = 0;
            drpSheduleMin.SelectedIndex = 0;
            drpWardNurse.SelectedIndex = 0;
            txtSheduleComment.Text = "";
            lblSheduleMsg.Text = "";
            lblSheduleMsg.Visible = false;
        }


        void BindEndDate()
        {
            //  DateTime strFromDate = Convert.ToDateTime(txtFromDate.Text);
            DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());

            Int32 intDuration;
            if (txtDuration.Text.Trim() != "" && drpDurationType.SelectedValue != "8")
            {
                if (drpDurationType.SelectedValue == "0")
                {
                    intDuration = Convert.ToInt32(txtDuration.Text.Trim());
                    txtToDate.Text = strFromDate.AddDays(intDuration).ToString("dd/MM/yyyy");
                }

                if (drpDurationType.SelectedValue == "3")
                {
                    intDuration = Convert.ToInt32(txtDuration.Text.Trim());
                    intDuration = intDuration * 7;

                    txtToDate.Text = strFromDate.AddDays(intDuration).ToString("dd/MM/yyyy");
                }


                if (drpDurationType.SelectedValue == "1")
                {
                    intDuration = Convert.ToInt32(txtDuration.Text.Trim());
                    txtToDate.Text = strFromDate.AddMonths(intDuration).ToString("dd/MM/yyyy");
                }

                if (drpDurationType.SelectedValue == "2")
                {
                    intDuration = Convert.ToInt32(txtDuration.Text.Trim());
                    txtToDate.Text = strFromDate.AddYears(intDuration).ToString("dd/MM/yyyy");
                }



            }


        }

        Boolean CheckServiceAvailable()
        {
            DataSet ds = new DataSet();
            CommonBAL dbo = new CommonBAL();

            ds = dbo.HaadServicessListGet("Pharmacy", txtNewServCode.Text.Trim(), "", strSessionDeptId);

            if (ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }

            return false;
        }
        #endregion

        #region AutoExt
        [System.Web.Services.WebMethod]
        public static string[] GetServicessList(string prefixText)
        {
            DataSet ds = new DataSet();
            CommonBAL dbo = new CommonBAL();

            string[] Data;
            if (intSrcType == 0)
            {
                string Criteria = " 1=1 ";
                if (strSearchOPtion == "T")
                {
                    Criteria += " AND HHS_PRODUCTNAME IS NOT NULL AND HHS_PRODUCTNAME <>'' ";

                }

                // ds = dbo.HaadServicessListGet("Pharmacy", prefixText, "", strSessionDeptId);
                if (strSearchOPtion == "ST")
                {
                    if (strDrugSrcType == "A")
                    {
                        Criteria += " AND  (HHS_CODE like '" + prefixText + "%' OR HHS_DESCRIPTION  like '" + prefixText + "%'" +
                                       " OR HHS_PRODUCTNAME  like '" + prefixText + "%')";
                    }
                    else if (strDrugSrcType == "G")
                    {
                        Criteria += " AND  (HHS_CODE like '" + prefixText + "%' OR HHS_DESCRIPTION  like '" + prefixText + "%')";
                    }
                    else if (strDrugSrcType == "T")
                    {
                        Criteria += " AND  (HHS_CODE like '" + prefixText + "%'  OR HHS_PRODUCTNAME  like '" + prefixText + "%') ";
                    }
                    else
                    {
                        Criteria += " AND  (HHS_CODE like '" + prefixText + "%' OR HHS_DESCRIPTION  like '" + prefixText + "%'" +
                                     " OR HHS_PRODUCTNAME  like '" + prefixText + "%')";
                    }
                }
                else if (strSearchOPtion == "AN")
                {
                    if (strDrugSrcType == "A")
                    {
                        Criteria += " AND  (HHS_CODE like '%" + prefixText + "%' OR HHS_DESCRIPTION  like '%" + prefixText + "%'" +
                                       " OR HHS_PRODUCTNAME  like '%" + prefixText + "%')";
                    }
                    else if (strDrugSrcType == "G")
                    {
                        Criteria += " AND  (HHS_CODE like '%" + prefixText + "%' OR HHS_DESCRIPTION  like '%" + prefixText + "%') ";
                    }
                    else if (strDrugSrcType == "T")
                    {
                        Criteria += " AND  (HHS_CODE like '%" + prefixText + "%'  OR HHS_PRODUCTNAME  like '%" + prefixText + "%') ";
                    }
                    else
                    {
                        Criteria += " AND  (HHS_CODE like '%" + prefixText + "%' OR HHS_DESCRIPTION  like '%" + prefixText + "%'" +
                                       " OR HHS_PRODUCTNAME  like '%" + prefixText + "%')";
                    }
                }
                else if (strSearchOPtion == "EX")
                {
                    if (strDrugSrcType == "A")
                    {
                        Criteria += " AND  (HHS_CODE = '" + prefixText + "' OR HHS_DESCRIPTION  = '" + prefixText + "'" +
                                       " OR HHS_PRODUCTNAME  = '" + prefixText + "')";
                    }
                    else if (strDrugSrcType == "G")
                    {
                        Criteria += " AND  (HHS_CODE = '" + prefixText + "' OR HHS_DESCRIPTION  = '" + prefixText + "') ";
                    }
                    else if (strDrugSrcType == "T")
                    {
                        Criteria += " AND  (HHS_CODE = '" + prefixText + "' OR HHS_PRODUCTNAME  = '" + prefixText + "') ";
                    }
                    else
                    {
                        Criteria += " AND  (HHS_CODE = '" + prefixText + "' OR HHS_DESCRIPTION  = '" + prefixText + "'" +
                                                                   " OR HHS_PRODUCTNAME  = '" + prefixText + "')";
                    }
                }


                ds = dbo.HAADDrugListGet(Criteria, strDrugSrcType);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Data = new string[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        if (strDrugSrcType == "A")
                        {
                            Data[i] = ds.Tables[0].Rows[i]["Code"].ToString() + "~" + ds.Tables[0].Rows[i]["ProductName"].ToString() + '~' + ds.Tables[0].Rows[i]["GenericName"].ToString();

                        }
                        else if (strDrugSrcType == "G")
                        {
                            Data[i] = ds.Tables[0].Rows[i]["Code"].ToString() + "~" + ds.Tables[0].Rows[i]["GenericName"].ToString();

                        }
                        else if (strDrugSrcType == "T")
                        {
                            Data[i] = ds.Tables[0].Rows[i]["Code"].ToString() + "~" + ds.Tables[0].Rows[i]["ProductName"].ToString();

                        }


                    }

                    return Data;
                }


            }
            else
            {
                string Criteria = " 1=1 ";

                Criteria += " AND( EDF_CODE like'%" + prefixText + "%' OR  EDF_NAME  like'%" + prefixText + "%')";

                Criteria += " AND EDF_TYPE='PHY' AND ( EDF_DR_ID='" + strUserCode + "' OR EDF_DEP_ID='" + strSessionDeptId + "' )";

                ds = dbo.FavoritesGet(Criteria);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Data = new string[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Data[i] = ds.Tables[0].Rows[i]["Code"].ToString() + "~" + ds.Tables[0].Rows[i]["ProductName"].ToString() + '~' + ds.Tables[0].Rows[i]["Description"].ToString();
                        // Data[i] = ds.Tables[0].Rows[i]["Code"].ToString() + "~" + ds.Tables[0].Rows[i]["Description"].ToString();


                    }

                    return Data;
                }

            }

            string[] Data1 = { "" };

            return Data1;
        }
        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Convert.ToString(Session["User_ID"]) == "" || Convert.ToString(Session["User_ID"]) == null)
            {
                Session["ErrorMsg"] = "Session Expired Please login again";
                Response.Redirect("../../ErrorPage.aspx");
            }

            if (Session["User_ID"] == null) { goto FunEnd; }

            string parameter = Request["__EVENTARGUMENT"];
            if (parameter == "Pharmacy1")
            {
                BindData();
                txtSearch.Focus();

            }

            if (!IsPostBack)
            {
                try
                {
                    if (Convert.ToString(Session["HUM_ADDNEW_RECORD_FROMEMR"]) == "True")
                    {
                        btnNewService.Visible = true;
                    }

                    strSessionDeptId = Convert.ToString(Session["User_DeptID"]);
                    strUserCode = Convert.ToString(Session["User_Code"]);


                    BindScreenCustomization();
                    if (Convert.ToString(ViewState["PHY_SEARCH_LIST"]) == "1")
                    {
                        drpSrcType.SelectedIndex = 1;
                        btnDeleteFav.Visible = true;
                        btnAddFav.Visible = false;

                    }
                    else
                    {
                        drpSrcType.SelectedIndex = 0;
                        btnAddFav.Visible = true;
                        btnDeleteFav.Visible = false;

                    }


                    radDrugSrcType.SelectedValue = Convert.ToString(ViewState["EMR_DRUG_DISPLAY_FORMAT"]);

                    intSrcType = drpSrcType.SelectedIndex;

                    DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                    txtFromDate.Text = strFromDate.ToString("dd/MM/yyyy");
                    txtToDate.Text = strFromDate.AddDays(3).ToString("dd/MM/yyyy");
                    txtSheduleDate.Text = strFromDate.ToString("dd/MM/yyyy");

                    BindTime();
                    BindDossage();
                    BindTaken();
                    BindRoute();
                    BindTemplate();
                    BindData();
                    BindPharmacy();
                    BindHistory();
                    BindNurse();


                    if (Convert.ToString(Session["EPM_STATUS"]).ToUpper() == "Y" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                    {
                        btnAddPhar.Visible = false;
                        drpHistory.Enabled = false;
                        drpTemplate.Enabled = false;


                    }
                    //HIDE THE SAVE OPTION FOR NEXT DAY OF THE VISIT
                    if (Convert.ToString(Session["EMRCurrentDateDiff"]) != "0" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                    {
                        btnAddPhar.Visible = false;
                        drpHistory.Enabled = false;
                        drpTemplate.Enabled = false;

                    }
                    if (Convert.ToString(Session["EMR_VISIT_DTLS_EDIT_ALL_USERS"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "DOCTORS" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                    {
                        btnAddPhar.Visible = false;
                        drpHistory.Enabled = false;
                        drpTemplate.Enabled = false;

                    }

                    if (Convert.ToString(Session["EMR_SAVE_PRESC_TO_HAAD"]) == "1")
                    {

                        btnPrescriptioTODB.Visible = true;
                    }

                    if (Convert.ToString(Session["EMR_DISPLAY_TEMPLATE"]) == "1")
                    {
                        divTemplate.Visible = true;
                    }

                    if (Convert.ToString(Session["EMR_DISPLAY_HISTORY"]) == "1")
                    {
                        divHistoryDrp.Visible = true;
                    }
                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }
            }

        FunEnd: ;
        }


        protected void gvServicess_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {
                gvServicess.PageIndex = e.NewPageIndex;
                BindData();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.gvServicess_PageIndexChanging");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;
        }


        protected void Edit_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {
                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
                ViewState["gvServSelectIndex"] = gvScanCard.RowIndex;
                gvServicess.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");

                Label lblCode = (Label)gvScanCard.Cells[0].FindControl("lblCode");
                Label lblDesc = (Label)gvScanCard.Cells[0].FindControl("lblDesc");
                Label lblPrice = (Label)gvScanCard.Cells[0].FindControl("lblPrice");

                ViewState["Code"] = lblCode.Text;
                ViewState["Description"] = lblDesc.Text;
                ViewState["Price"] = lblPrice.Text;
                lblCode.BackColor = System.Drawing.Color.FromName("#c5e26d");
                lblDesc.BackColor = System.Drawing.Color.FromName("#c5e26d");
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.Edit_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;
        }

        protected void drpSrcType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }

            if (drpSrcType.SelectedIndex == 0)
            {
                btnAddFav.Visible = true;
                btnDeleteFav.Visible = false;
            }
            else
            {
                btnAddFav.Visible = false;
                btnDeleteFav.Visible = true;
            }
            intSrcType = drpSrcType.SelectedIndex;
            BindData();
        FunEnd: ;
        }

        protected void btnAddFav_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }

            try
            {

                if (string.IsNullOrEmpty(Convert.ToString(ViewState["Code"])) == true)
                {
                    goto FunEnd;
                }
                if (string.IsNullOrEmpty(Convert.ToString(ViewState["Description"])) == true)
                {
                    goto FunEnd;
                }

                if (CheckFavorite(Convert.ToString(ViewState["Code"])) == true)
                {
                    goto FunEnd;
                }


                CommonBAL objCom = new CommonBAL();
                objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
                objCom.Code = Convert.ToString(ViewState["Code"]);
                objCom.Description = Convert.ToString(ViewState["Description"]);
                objCom.DR_ID = Convert.ToString(Session["User_Code"]);
                objCom.DEP_ID = Convert.ToString(Session["User_DeptID"]);
                objCom.Type = "PHY";
                objCom.Price = Convert.ToString(ViewState["Price"]);
                objCom.FavoritesAdd();

                if (string.IsNullOrEmpty(Convert.ToString(ViewState["gvServSelectIndex"])) == false)
                {
                    Int32 R = 0;
                    R = Convert.ToInt32(ViewState["gvServSelectIndex"]);
                    Label lblCode = (Label)gvServicess.Rows[R].Cells[0].FindControl("lblCode");
                    Label lblDesc = (Label)gvServicess.Rows[R].Cells[0].FindControl("lblDesc");
                    Label lblPrice = (Label)gvServicess.Rows[R].Cells[0].FindControl("lblPrice");

                    if (R % 2 == 0)
                    {
                        lblCode.BackColor = System.Drawing.Color.FromName("#ffffff");
                        lblDesc.BackColor = System.Drawing.Color.FromName("#ffffff");
                        gvServicess.Rows[R].BackColor = System.Drawing.Color.FromName("#ffffff");
                    }
                    else
                    {
                        lblCode.BackColor = System.Drawing.Color.FromName("#f6f6f6");
                        lblDesc.BackColor = System.Drawing.Color.FromName("#f6f6f6");
                        gvServicess.Rows[R].BackColor = System.Drawing.Color.FromName("#f6f6f6");
                    }

                }

                ClearFavorite();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.btnAddFav_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;
        }

        protected void btnDeleteFav_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }

            try
            {

                if (string.IsNullOrEmpty(Convert.ToString(ViewState["Code"])) == true)
                {
                    goto FunEnd;
                }
                if (string.IsNullOrEmpty(Convert.ToString(ViewState["Description"])) == true)
                {
                    goto FunEnd;
                }
                if (drpSrcType.SelectedIndex == 0)
                {
                    goto FunEnd;
                }

                //if (CheckFavorite(Convert.ToString(ViewState["Code"])) == false)
                //{
                //    goto FunEnd;
                //}


                CommonBAL objCom = new CommonBAL();
                objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
                objCom.Code = Convert.ToString(ViewState["Code"]);
                objCom.DR_ID = Convert.ToString(Session["User_Code"]);
                objCom.DEP_ID = Convert.ToString(Session["User_DeptID"]);
                objCom.Type = "PHY";
                objCom.FavoritesDelete();

                ClearFavorite();

                BindData(); ;


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.btnDeleteFav_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;
        }


        protected void Add_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {

                ImageButton btnEdit = new ImageButton();
                btnEdit = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;


                Label lblCode = (Label)gvScanCard.Cells[0].FindControl("lblCode");
                Label lblDesc = (Label)gvScanCard.Cells[0].FindControl("lblDesc");

                txtServCode.Text = lblCode.Text.Trim();
                txtServName.Text = lblDesc.Text.Trim();
                if (Convert.ToString(Session["EMR_SAVE_PRESC_TO_HAAD"]) == "1")
                {
                    GetHaadPrescriptionDtls(txtServCode.Text.Trim());
                }

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.Add_Click");
                TextFileWriting(ex.Message.ToString());
            }
        FunEnd: ;
        }

        protected void btnAddPhar_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }

            try
            {
                if (txtServCode.Text == "" || txtServCode.Text == null)
                {
                    lblMessage.Text = "Please select the Code";
                    lblMessage.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }
                Int32 intPos = 1;
                if (gvPharmacy.Rows.Count >= 1)
                {
                    intPos = Convert.ToInt32(gvPharmacy.Rows.Count) + 1;

                }



                EMR_PTPharmacy objDiag = new EMR_PTPharmacy();
                objDiag.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objDiag.EPP_ID = Convert.ToString(Session["EMR_ID"]);
                objDiag.EPP_PHY_CODE = txtServCode.Text;
                objDiag.EPP_PHY_NAME = txtServName.Text.Replace("'", "''");

                objDiag.EPP_DOSAGE = drpDossage.SelectedValue;
                objDiag.EPP_ROUTE = drpRoute.SelectedValue;
                objDiag.EPP_REFILL = txtRefil.Text.Trim();
                if (chkLifeLong.Checked == true)
                {
                    objDiag.EPP_LIFE_LONG = "1";
                }
                else
                {
                    objDiag.EPP_LIFE_LONG = "0";
                }
                objDiag.EPP_DURATION = txtDuration.Text;
                objDiag.EPP_DURATION_TYPE = drpDurationType.SelectedValue;
                objDiag.EPP_START_DATE = txtFromDate.Text;
                objDiag.EPP_END_DATE = txtToDate.Text;
                objDiag.EPP_REMARKS = txtRemarks.Text.Replace("'", "''");
                objDiag.EPP_REFERENCE = "";
                objDiag.EPP_QTY = txtQty.Text.Trim();
                objDiag.EPP_ICD = "";
                objDiag.EPP_DOSAGE1 = txtDossage1.Text.Trim();
                objDiag.EPP_TYPE = "";
                objDiag.EPP_INV_IsuNo = "";
                objDiag.EPP_HHS_CODE = "";
                objDiag.EPP_TEMPLATE_CODE = "";
                objDiag.EPP_REF_OUTSIDE = "0";
                objDiag.EPP_TAKEN = "";
                objDiag.EPP_TIME = drpTaken.SelectedValue;
                objDiag.EPP_UNITTYPE = "";
                objDiag.EPP_FREQUENCY = "";
                objDiag.EPP_FREQUENCYTYPE = "";
                objDiag.EPP_UNIT = "";

                objDiag.PharmacyAdd();

                BindPharmacy();
                ClearPhar();


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.btnAddPhar_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;

        }

        protected void DeleteeDiag_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }

            try
            {
                ImageButton btnDel = new ImageButton();
                btnDel = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnDel.Parent.Parent;

                Label lblPhyCode = (Label)gvScanCard.Cells[0].FindControl("lblPhyCode");


                EMR_PTPharmacy objDiag = new EMR_PTPharmacy();
                objDiag.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objDiag.EPP_ID = Convert.ToString(Session["EMR_ID"]);
                objDiag.EPP_PHY_CODE = lblPhyCode.Text;

                objDiag.PharmacyDelete();


                BindPharmacy();
                ClearPhar();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.DeletePhar_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;

        }

        protected void PhySelect_Click(object sender, EventArgs e)
        {

            if (Session["User_ID"] == null) { goto FunEnd; }

            try
            {
                LinkButton btnDel = new LinkButton();
                btnDel = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnDel.Parent.Parent;

                Label lblPhyCode = (Label)gvScanCard.Cells[0].FindControl("lblPhyCode");
                Label lblPhyDesc = (Label)gvScanCard.Cells[0].FindControl("lblPhyDesc");

                Label lblPhyDuration = (Label)gvScanCard.Cells[0].FindControl("lblPhyDuration");
                Label lblPhyDurationType = (Label)gvScanCard.Cells[0].FindControl("lblPhyDurationType");

                Label lblPhyQTY = (Label)gvScanCard.Cells[0].FindControl("lblPhyQTY");
                Label lblPhyDosage = (Label)gvScanCard.Cells[0].FindControl("lblPhyDosage");

                Label lblPhyTime = (Label)gvScanCard.Cells[0].FindControl("lblPhyTime");
                Label lblPhyRoute = (Label)gvScanCard.Cells[0].FindControl("lblPhyRoute");



                Label lblPhyRefil = (Label)gvScanCard.Cells[0].FindControl("lblPhyRefil");
                Label lblPhyStDate = (Label)gvScanCard.Cells[0].FindControl("lblPhyStDate");
                Label lblPhyEndDate = (Label)gvScanCard.Cells[0].FindControl("lblPhyEndDate");

                Label lblPhyLifeLong = (Label)gvScanCard.Cells[0].FindControl("lblPhyLifeLong");
                Label lblPhyRemarks = (Label)gvScanCard.Cells[0].FindControl("lblPhyRemarks");
                Label lblPhyDossage1 = (Label)gvScanCard.Cells[0].FindControl("lblPhyDossage1");


                txtServCode.Text = lblPhyCode.Text;
                txtServName.Text = lblPhyDesc.Text;
                txtDuration.Text = lblPhyDuration.Text;

                drpDurationType.SelectedValue = lblPhyDurationType.Text;
                txtQty.Text = lblPhyQTY.Text;

                txtDossage1.Text = lblPhyDossage1.Text;

                drpDossage.SelectedValue = lblPhyDosage.Text;

                drpTaken.SelectedValue = lblPhyTime.Text;
                drpRoute.SelectedValue = lblPhyRoute.Text;

                txtRefil.Text = lblPhyRefil.Text;
                txtFromDate.Text = lblPhyStDate.Text;
                txtToDate.Text = lblPhyEndDate.Text;

                chkLifeLong.Checked = Convert.ToBoolean(lblPhyLifeLong.Text);
                txtRemarks.Text = lblPhyRemarks.Text;



            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.DeleteeDiag_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;

        }

        protected void btnSaveTemp_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }

            try
            {
                string TemplateCode = "";
                CommonBAL objCom = new CommonBAL();
                objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
                objCom.Type = "Pharmacy";
                objCom.Description = txtTemplateName.Value;
                objCom.TemplateData = "";
                if (chkOtherDr.Checked == true)
                {
                    objCom.AllDr = "1";
                }
                else
                {
                    objCom.AllDr = "0";
                }
                objCom.DR_ID = Convert.ToString(Session["HPV_DR_ID"]);
                objCom.DEP_ID = Convert.ToString(Session["HPV_DEP_NAME"]);
                TemplateCode = objCom.TemplatesAdd();


                EMR_PTPharmacy objPhar = new EMR_PTPharmacy();
                objPhar.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objPhar.EPP_ID = Convert.ToString(Session["EMR_ID"]);
                objPhar.TemplateCode = TemplateCode;
                objPhar.PharmacyTemplateAdd();
                BindTemplate();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.btnSaveTemp_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;
        }

        protected void btnDeleteTemp_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }

            try
            {
                EMR_PTPharmacy objPhar = new EMR_PTPharmacy();
                objPhar.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objPhar.EPP_ID = Convert.ToString(Session["EMR_ID"]);
                objPhar.TemplateCode = drpTemplate.SelectedValue;
                objPhar.PharmacyTemplateDelete();
                BindTemplate();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.btnDeleteTemp_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;
        }

        protected void drpTemplate_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {


                if (drpTemplate.SelectedIndex != 0)
                {
                    EMR_PTPharmacy objPhar = new EMR_PTPharmacy();
                    objPhar.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                    objPhar.EPP_ID = Convert.ToString(Session["EMR_ID"]);
                    objPhar.TemplateCode = drpTemplate.SelectedValue;
                    objPhar.PharmacyAddFromTemplate();
                    BindPharmacy();
                }



            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.drpTemplate_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;
        }


        protected void PhySchedule_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }

            try
            {
                lblSheduleMsg.Text = "";

                LinkButton btnDel = new LinkButton();
                btnDel = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnDel.Parent.Parent;

                Label lblPhyCode = (Label)gvScanCard.Cells[0].FindControl("lblPhyCode");
                Label lblPhyDesc = (Label)gvScanCard.Cells[0].FindControl("lblPhyDesc");

                string strMed = lblPhyDesc.Text;


                ViewState["SchedulePhyCode"] = lblPhyCode.Text;

                lblSheMedicinCode.Text = lblPhyCode.Text;

                if (strMed.Length >= 100)
                {
                    lblSheMedicinDesc.Text = strMed.Substring(0, 100);
                }
                else
                {
                    lblSheMedicinDesc.Text = strMed;
                }
                BindPharmacySchedul();

                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowPhySchedule()", true);




            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.DeleteeDiag_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;


        }

        protected void btnPhyScheduleAdd_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }

            lblSheduleMsg.Text = "";
            if (CheckPassword(drpWardNurse.SelectedValue, txtWardNursePass.Text.Trim()) == false)
            {
                lblSheduleMsg.Text = "Wrong Password";
                lblSheduleMsg.Visible = true;
                lblSheduleMsg.ForeColor = System.Drawing.Color.Red;
                goto FunEnd;
            }

            if (drpWardNurse.SelectedIndex == 0)
            {
                lblSheduleMsg.Text = "Select Nurse Name";
                lblSheduleMsg.Visible = true;
                goto FunEnd;
            }
            EMR_PTPharmacy objPro = new EMR_PTPharmacy();
            objPro.BranchID = Convert.ToString(Session["Branch_ID"]);
            objPro.EMRID = Convert.ToString(Session["EMR_ID"]);
            objPro.EPPS_PHY_CODE = Convert.ToString(ViewState["SchedulePhyCode"]);

            objPro.EPPS_DATE = txtSheduleDate.Text + " " + drpSheduleHour.SelectedValue + ":" + drpSheduleMin.SelectedValue + ":00";
            objPro.EPPS_GIVENBY = drpWardNurse.SelectedValue;
            objPro.EPPS_REMARKS = txtSheduleComment.Text;
            objPro.PharmacyScheduleAdd();

            ClearPhyShedule();
            BindPharmacySchedul();
            lblSheduleMsg.Text = "Data Saved";
            lblSheduleMsg.Visible = true;
            lblSheduleMsg.ForeColor = System.Drawing.Color.Green;
        //  ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "HidePhyScedule()", true);

        FunEnd: ;
        }

        protected void drpDurationType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            BindEndDate();
        FunEnd: ;
        }



        protected void txtDuration_TextChanged(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            BindEndDate();
        FunEnd: ;
        }


        protected void chkLifeLong_CheckedChanged(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }

            DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());

            if (chkLifeLong.Checked == true)
            {
                txtToDate.Text = strFromDate.AddYears(10).ToString("dd/MM/yyyy");
            }
            else
            {
                txtToDate.Text = strFromDate.AddDays(3).ToString("dd/MM/yyyy");
            }

        FunEnd: ;
        }

        #endregion

        protected void btnAddNewService_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {
                lblNewServStatus.Text = "";
                if (txtNewServCode.Text == "" || txtNewServCode.Text == null)
                {
                    lblNewServStatus.Text = "Please Enter the Code";
                    lblNewServStatus.Visible = true;
                    lblNewServStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                if (txtNewServDesc.Text == "" || txtNewServDesc.Text == null)
                {
                    lblNewServStatus.Text = "Please Enter the Name";
                    lblNewServStatus.Visible = true;
                    lblNewServStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }
                if (txtNewProdName.Text == "" || txtNewProdName.Text == null)
                {
                    lblNewServStatus.Text = "Please Enter the Product Name";
                    lblNewServStatus.Visible = true;
                    lblNewServStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }


                if (CheckServiceAvailable() == true)
                {
                    lblNewServStatus.Text = "Medicine  Already available";
                    lblNewServStatus.Visible = true;
                    lblNewServStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }


                CommonBAL objCom = new CommonBAL();
                objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
                objCom.ServCode = txtNewServCode.Text.Trim();
                objCom.ServDesc = txtNewServDesc.Text;
                objCom.ProductName = txtNewProdName.Text;
                objCom.ServType = "DRUG";
                objCom.ServTypeValue = "5";
                objCom.UserID = Convert.ToString(Session["User_ID"]);

                objCom.HaadServiceManualAdd();

                lblNewServStatus.Text = "New Medicine added";
                lblNewServStatus.Visible = true;
                lblNewServStatus.ForeColor = System.Drawing.Color.Green;

                txtNewServCode.Text = "";
                txtNewServDesc.Text = "";
                txtNewProdName.Text = "";



            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.btnAddNewService_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;

        }

        protected void btnPrescriptioTODB_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }

            try
            {
                if (txtServCode.Text == "" || txtServCode.Text == null)
                {
                    lblMessage.Text = "Please select the Code";
                    lblMessage.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                EMR_PTPharmacy objDiag = new EMR_PTPharmacy();
                objDiag.EPP_PHY_CODE = txtServCode.Text;
                objDiag.EPP_DOSAGE = drpDossage.SelectedValue;
                objDiag.EPP_ROUTE = drpRoute.SelectedValue;
                objDiag.EPP_REFILL = txtRefil.Text.Trim();
                objDiag.EPP_DURATION = txtDuration.Text;
                objDiag.EPP_DURATION_TYPE = drpDurationType.SelectedValue;
                objDiag.EPP_QTY = txtQty.Text.Trim();
                objDiag.EPP_DOSAGE1 = txtDossage1.Text.Trim();
                objDiag.EPP_TIME = drpTaken.SelectedValue;
                objDiag.PrescriptionSaveToHAAD();




            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.btnPRescTODB_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;

        }

        protected void txtServCode_TextChanged(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            if (Convert.ToString(Session["EMR_SAVE_PRESC_TO_HAAD"]) == "1")
            {
                GetHaadPrescriptionDtls(txtServCode.Text.Trim());
            }

        FunEnd: ;
        }

        protected void radDrugSrcType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {
                BindData();
                strDrugSrcType = radDrugSrcType.SelectedValue;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.radDrugSrcType_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;
        }

        protected void chkPrintNo_CheckedChanged(object sender, EventArgs e)
        {

            if (Session["User_ID"] == null) { goto FunEnd; }

            try
            {
                CheckBox btnCHK = new CheckBox();
                btnCHK = (CheckBox)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnCHK.Parent.Parent;

                Label lblPhyCode = (Label)gvScanCard.Cells[0].FindControl("lblPhyCode");


                CommonBAL objCom = new CommonBAL();

                string FieldNameWithValues = "";

                if (btnCHK.Checked == true)
                {
                    FieldNameWithValues = " EPP_NOT_PRINT=1";
                }
                else
                {
                    FieldNameWithValues = " EPP_NOT_PRINT=null";
                }

                string Criteria = " EPP_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND EPP_ID=" + Convert.ToString(Session["EMR_ID"]) + " AND EPP_PHY_CODE='" + lblPhyCode.Text.Trim() + "'";
                objCom.fnUpdateTableData(FieldNameWithValues, "EMR_PT_PHARMACY", Criteria);



                BindPharmacy();
                ClearPhar();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.DeletePhar_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;
        }

        protected void gvPharmacy_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblPhyNoPrint = (Label)e.Row.FindControl("lblPhyNoPrint");
                CheckBox chkPrintNo = (CheckBox)e.Row.FindControl("chkPrintNo");

                if (lblPhyNoPrint.Text == "True")
                {
                    chkPrintNo.Checked = true;
                }

            }
        }

        protected void imgbtnDown_Click(object sender, ImageClickEventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {


                string[] arrHistory = drpHistory.SelectedValue.ToString().Split('-');
                string BranchID = "", strEMR_ID = "", strDR_Code = "";

                if (arrHistory.Length > 0)
                {
                    BranchID = arrHistory[0];
                    if (arrHistory.Length > 0)
                    {
                        strEMR_ID = arrHistory[1];
                    }

                    if (arrHistory.Length > 2)
                    {
                        strDR_Code = arrHistory[2];
                    }

                }



                if (drpHistory.SelectedIndex != 0)
                {
                    EMR_PTPharmacy objPhar = new EMR_PTPharmacy();
                    objPhar.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                    objPhar.EPP_ID = Convert.ToString(Session["EMR_ID"]);
                    objPhar.SelectedBranch_ID = BranchID;
                    objPhar.SelectedEMR_ID = strEMR_ID;

                    objPhar.PharmacyAddFromHistory();
                    BindPharmacy();
                }



            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PharmacyStandard.imgbtnDown_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;
        }

        protected void imgbtnPrescHistView_Click(object sender, ImageClickEventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {


                string[] arrHistory = drpHistory.SelectedValue.ToString().Split('-');
                string BranchID = "", strEMR_ID = "", strDR_Code = "";

                if (arrHistory.Length > 0)
                {
                    BranchID = arrHistory[0];
                    if (arrHistory.Length > 0)
                    {
                        strEMR_ID = arrHistory[1];
                    }

                    if (arrHistory.Length > 2)
                    {
                        strDR_Code = arrHistory[2];
                    }

                }

                //  string strRptPath = "";

                if (drpHistory.SelectedIndex != 0)
                {
                    BindPharmacyHistory(strEMR_ID);
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowHistory()", true);
                    //strRptPath = "../WebReports/Prescription.aspx";
                    //string rptcall = @strRptPath + "?EMR_ID=" + strEMR_ID + "&EMR_PT_ID=" + Convert.ToString(Session["EMR_PT_ID"]) + "&DR_ID=" + strDR_Code;
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "Presc", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1');", true);

                }



            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PharmacyStandard.imgbtnDown_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;
        }
    }
}