﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_BAL;


namespace Mediplus.EMR.VisitDetails
{
    public partial class Diagnosis1 : System.Web.UI.Page
    {
        static string strSessionDeptId, strUserCode;
        static Int32 intSrcType;

        static Int32 DiagCount = 0;
        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindScreenCustomization()
        {
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 ";

            Criteria += " AND SCREENNAME='EMR_DG' AND SEGMENT='DG_SEARCH_LIST'";

            DataSet DS = new DataSet();
            DS = objCom.ScreenCustomizationGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("CUST_VALUE") == false)
                {
                    ViewState["CUST_VALUE"] = DS.Tables[0].Rows[0]["CUST_VALUE"].ToString();
                }
                else
                {
                    ViewState["CUST_VALUE"] = "0";
                }
            }


        }

        void BindData()
        {

            string SearchFilter = txtSearch.Text.Trim();
            DataSet ds = new DataSet();
            CommonBAL dbo = new CommonBAL();
            if (drpSrcType.SelectedIndex == 0)
            {
                ds = dbo.HaadServicessListGet("Diagnosis", SearchFilter, "", Convert.ToString(Session["User_DeptID"]));
            }
            else
            {
                string Criteria = " 1=1 ";

                if (txtSearch.Text.Trim() != "")
                {
                    if (chkAnySearch.Checked == true)
                    {
                        Criteria += " AND( EDF_CODE like'%" + txtSearch.Text.Trim() + "%' OR  EDF_NAME  like'%" + txtSearch.Text.Trim() + "%')";
                    }
                    else
                    {
                        Criteria += " AND( EDF_CODE like'" + txtSearch.Text.Trim() + "%' OR  EDF_NAME  like'" + txtSearch.Text.Trim() + "%')";

                    }
                }

                if (GlobalValues.FileDescription == "ALNOOR")
                {
                    Criteria += " AND EDF_TYPE='ICD' AND ( EDF_DR_ID='" + Convert.ToString(Session["User_Code"]) + "')";
                }
                else
                {
                    Criteria += " AND EDF_TYPE='ICD' AND ( EDF_DR_ID='" + Convert.ToString(Session["User_Code"]) + "' OR EDF_DEP_ID='" + Convert.ToString(Session["User_DeptID"]) + "')";
                }

                ds = dbo.FavoritesGet(Criteria);

            }
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvServicess.DataSource = ds;
                gvServicess.DataBind();
                if (Convert.ToString(Session["EPM_STATUS"]).ToUpper() == "Y" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                {
                    gvServicess.Columns[0].Visible = false;
                }

                //HIDE THE SAVE OPTION FOR NEXT DAY OF THE VISIT
                if (Convert.ToString(Session["EMRCurrentDateDiff"]) != "0" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                {
                    gvServicess.Columns[0].Visible = false;

                }


                if (Convert.ToString(Session["EMR_VISIT_DTLS_EDIT_ALL_USERS"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "DOCTORS" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                {
                    gvServicess.Columns[0].Visible = false;

                }
            }
            else
            {
                gvServicess.DataBind();
            }
        }

        Boolean CheckFavorite(string Code)
        {
            DataSet ds = new DataSet();
            CommonBAL dbo = new CommonBAL();

            string Criteria = " 1=1 ";

            Criteria += " AND EDF_CODE='" + Code + "'";
            if (GlobalValues.FileDescription == "ALNOOR")
            {
                Criteria += " AND EDF_TYPE='ICD' AND ( EDF_DR_ID='" + Convert.ToString(Session["User_Code"]) + "')";
            }
            else
            {
                Criteria += " AND EDF_TYPE='ICD' AND (EDF_DR_ID='" + Convert.ToString(Session["User_Code"]) + "' OR EDF_DEP_ID='" + Convert.ToString(Session["User_DeptID"]) + "')";
            }

            ds = dbo.FavoritesGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            return false;
        }

        void BindDiagnosis()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND EPD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND EPD_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

            if (Convert.ToString(Session["EMR_ID"]) == "")
            {
                goto FunEnd;
            }



            DataSet DS = new DataSet();
            EMR_PTDiagnosis objDiag = new EMR_PTDiagnosis();
            DS = objDiag.DiagnosisGet(Criteria);
            DiagCount = DS.Tables[0].Rows.Count;
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvDiagnosis.DataSource = DS;
                gvDiagnosis.DataBind();

                ViewState["DiagDT"] = DS.Tables[0];

                if (Convert.ToString(Session["EPM_STATUS"]).ToUpper() == "Y" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                {
                    gvDiagnosis.Columns[0].Visible = false;
                }

                //HIDE THE SAVE OPTION FOR NEXT DAY OF THE VISIT
                if (Convert.ToString(Session["EMRCurrentDateDiff"]) != "0" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                {
                    gvDiagnosis.Columns[0].Visible = false;
                }


                if (Convert.ToString(Session["EMR_VISIT_DTLS_EDIT_ALL_USERS"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "DOCTORS" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                {
                    gvDiagnosis.Columns[0].Visible = false;
                }


            }
            else
            {
                gvDiagnosis.DataBind();
            }

            txtRemarks.Text = "";
            foreach (DataRow DR in DS.Tables[0].Rows)
            {
                if (Convert.ToString(DR["EPD_REMARKS"]) != "")
                    txtRemarks.Text = Convert.ToString(DR["EPD_REMARKS"]);
            }

        FunEnd: ;
        }


        void BindDiagCrosswalk()
        {

            string strSrcDiagCrosswalk = txtSrcDiagCrosswalk.Text.Trim();

            if (txtSrcDiagCrosswalk.Text.Trim() == "")
            {
                goto FunEnd;
            }


            if (strSrcDiagCrosswalk.Length < 3)
            {
                goto FunEnd;
            }


            string Criteria = " 1=1 AND  DIM_STATUS='A'";

            if (radDiagCrosswalkType.SelectedIndex == 0)
            {
                Criteria += " AND( DIM_ICD_ID_9 like'%" + txtSrcDiagCrosswalk.Text.Trim() + "%' OR  DIM_ICD_DESCRIPTION_9  like'%" + txtSrcDiagCrosswalk.Text.Trim() + "%')";
            }
            else
            {
                Criteria += " AND( DIM_ICD_ID_10 like'%" + txtSrcDiagCrosswalk.Text.Trim() + "%' OR  DIM_ICD_DESCRIPTION_10  like'%" + txtSrcDiagCrosswalk.Text.Trim() + "%')";
            }
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            DS = objCom.ICDMasterOldNewMapGet(Criteria);
            DiagCount = DS.Tables[0].Rows.Count;

            gvDiagCrosswalk.Visible = false ;
            gvDiagCrosswalk1.Visible = false;

            if (DS.Tables[0].Rows.Count > 0)
            {
                if (radDiagCrosswalkType.SelectedIndex == 0)
                {
                    gvDiagCrosswalk.Visible = true;
                    gvDiagCrosswalk.DataSource = DS;
                    gvDiagCrosswalk.DataBind();
                }
                else
                {
                    gvDiagCrosswalk1.Visible = true;
                    gvDiagCrosswalk1.DataSource = DS;
                    gvDiagCrosswalk1.DataBind();
                }

                 
            }
            else
            {
                if (radDiagCrosswalkType.SelectedIndex == 0)
                {
                    gvDiagCrosswalk.DataBind();
                }
                else
                {

                    gvDiagCrosswalk1.DataBind();
                }
            }

            
        FunEnd: ;
        }
        Boolean CheckDiagnosis(string Code)
        {
            string Criteria = " 1=1 ";

            Criteria += " AND EPD_DIAG_CODE='" + Code + "'";
            Criteria += " AND EPD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND EPD_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

            DataSet DS = new DataSet();
            EMR_PTDiagnosis objDiag = new EMR_PTDiagnosis();
            DS = objDiag.DiagnosisGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                return true;
            }

            return false;
        }

        Boolean CheckPrincipalDiagnosis()
        {
            string Criteria = " 1=1 ";

            Criteria += " AND EPD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND EPD_TYPE='Principal'";
            Criteria += " AND EPD_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

            DataSet DS = new DataSet();
            EMR_PTDiagnosis objDiag = new EMR_PTDiagnosis();
            DS = objDiag.DiagnosisGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                return true;
            }

            return false;
        }

        void ClearFavorite()
        {

            ViewState["Code"] = "";
            ViewState["Description"] = "";
            ViewState["Price"] = "";
            ViewState["gvServSelectIndex"] = "";
        }

        void UpdateAuditDtls()
        {
            CommonBAL objCom = new CommonBAL();
            objCom.EMRPTMasterUserDtlsUpdate(Convert.ToString(Session["EMR_ID"]), Convert.ToString(Session["User_ID"]));
        }


        Boolean CheckServiceAvailable()
        {
            DataSet ds = new DataSet();
            CommonBAL dbo = new CommonBAL();

            ds = dbo.HaadServicessListGet("Diagnosis", txtNewServCode.Text.Trim(), "", Convert.ToString(Session["User_DeptID"]));

            if (ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }

            return false;
        }

        void BindTemplate()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1  and ET_TYPE='Diagnosis'  ";
            Criteria += " AND ET_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' ";

            Criteria += " AND ( ET_DR_CODE='" + Convert.ToString(Session["HPV_DR_ID"]) + "' OR ET_Apply=1 )";

            if (GlobalValues.FileDescription.ToUpper() != "MAMPILLY")
            {
                Criteria += " AND ET_DEP_ID='" + Convert.ToString(Session["HPV_DEP_NAME"]) + "'";
            }
            drpTemplate.Items.Clear();

            DS = objCom.TemplatesGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                drpTemplate.DataSource = DS;
                drpTemplate.DataTextField = "ET_NAME";
                drpTemplate.DataValueField = "ET_CODE";
                drpTemplate.DataBind();
            }
            drpTemplate.Items.Insert(0, "Select Template");
            drpTemplate.Items[0].Value = "";

            // objCom.BindMasterDropdown(Criteria, out drpDossage);

        }

        void BindHistory()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 ";
            Criteria += " AND EPM_PT_ID = '" + Convert.ToString(Session["EMR_PT_ID"]) + "' ";
            Criteria += " AND EPM_ID   !='" + Convert.ToString(Session["EMR_ID"]) + "' ";


            DS = objCom.DiagnosisHistoryGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                drpHistory.DataSource = DS;
                drpHistory.DataTextField = "Name";
                drpHistory.DataValueField = "Code";
                drpHistory.DataBind();
            }
            drpHistory.Items.Insert(0, "----Select History---");
            drpHistory.Items[0].Value = "0";


        }

        void BindDiagnosisHistory(string EMR_ID)
        {
            string Criteria = " 1=1 ";
            Criteria += " AND EPD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND EPD_ID='" + EMR_ID + "'";

            if (Convert.ToString(Session["EMR_ID"]) == "")
            {
                goto FunEnd;
            }



            DataSet DS = new DataSet();
            EMR_PTDiagnosis objDiag = new EMR_PTDiagnosis();
            DS = objDiag.DiagnosisGet(Criteria);
            DiagCount = DS.Tables[0].Rows.Count;
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvHistory.DataSource = DS;
                gvHistory.DataBind();

            }
            else
            {
                gvHistory.DataBind();
            }


        FunEnd: ;
        }

        void SaveClaimDetail()
        {
            string strInvoiceID = "0";
            string strClaimType = "", strStartDateTime = "", strEndDateTime = "", strStartType = "", strEndType = "";
            string Criteria = " 1=1  ";

            // Criteria += " AND ( HIM_INVOICE_ID ='" + txtInvoiceNo.Text.Trim() + "' OR HIM_EMR_ID='" + Convert.ToString(Session["EMR_ID"]) + "')";

            Criteria += " AND (HIM_EMR_ID='" + Convert.ToString(Session["EMR_ID"]) + "')";

            DataSet DS = new DataSet();
            clsInvoice objInv = new clsInvoice();
            DS = objInv.InvoiceMasterGet(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                strInvoiceID = DS.Tables[0].Rows[0]["HIM_INVOICE_ID"].ToString();

                strClaimType = "1";
                strStartDateTime = DS.Tables[0].Rows[0]["HIM_DATE"].ToString();
                strEndDateTime = DS.Tables[0].Rows[0]["HIM_DATE"].ToString();
                strStartType = "1";
                strEndType = "1";
            }
            else
            {
                goto FunEnd;
            }

            Criteria = "  HIC_INVOICE_ID='" + strInvoiceID + "'";

            clsInvoiceClaims objInvCla = new clsInvoiceClaims();
            DS = new DataSet();
            DS = objInvCla.InvoiceClaimDtlsGet(Criteria);



            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("HIC_TYPE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HIC_TYPE"]) != "")
                {
                    strClaimType = Convert.ToString(DS.Tables[0].Rows[0]["HIC_TYPE"]);
                }
                if (DS.Tables[0].Rows[0].IsNull("HIC_STARTDATE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HIC_STARTDATE"]) != "")
                {
                    strStartDateTime = Convert.ToString(DS.Tables[0].Rows[0]["HIC_STARTDATE"]);
                }
                if (DS.Tables[0].Rows[0].IsNull("HIC_ENDDATE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HIC_ENDDATE"]) != "")
                {
                    strEndDateTime = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ENDDATE"]);
                }
                if (DS.Tables[0].Rows[0].IsNull("HIC_ENDDATE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HIC_ENDDATE"]) != "")
                {
                    strStartType = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ENDDATE"]);
                }
                if (DS.Tables[0].Rows[0].IsNull("HIC_ENDDATE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HIC_ENDDATE"]) != "")
                {
                    strEndType = Convert.ToString(DS.Tables[0].Rows[0]["HIC_ENDDATE"]);
                }

            }
            else
            {


            }
            CommonBAL objCom = new CommonBAL();
            string Criteria1 = "1=1";

            Criteria1 = "HIC_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "'  AND HIC_INVOICE_ID='" + strInvoiceID + "'";
            objCom.fnDeleteTableData("HMS_INVOICE_CLAIMSDTLS", Criteria1);

            clsInvoiceClaims objInvClms = new clsInvoiceClaims();
            objInvClms.HIC_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
            objInvClms.HIC_INVOICE_ID = strInvoiceID;
            objInvClms.HIC_TYPE = strClaimType;
            objInvClms.HIC_STARTDATE = strStartDateTime;
            objInvClms.HIC_ENDDATE = strEndDateTime;
            objInvClms.HIC_STARTTYPE = strStartType;
            objInvClms.HIC_ENDTYPE = strEndType;

            DataTable DT = (DataTable)ViewState["InvoiceClaims"];


            //objrow["HIC_ICD_CODE"] = txtDiagCode.Text.Trim();
            //objrow["HIC_ICD_DESC"] = txtDiagName.Text.Trim();
            //objrow["HIC_ICD_TYPE"] = drpDiag
            string Criteria2 = " 1=1 ";
            Criteria2 += " AND EPD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria2 += " AND EPD_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";




            DataSet DS2 = new DataSet();

            EMR_PTDiagnosis objDiag = new EMR_PTDiagnosis();
            DS2 = objDiag.DiagnosisGet(Criteria2);

            if (DS2.Tables[0].Rows.Count > 0)
            {


                foreach (DataRow DR in DS2.Tables[0].Rows)
                {
                    string strICDType = "", strICDCode = "";

                    strICDCode = Convert.ToString(DR["EPD_DIAG_CODE"]);
                    strICDType = Convert.ToString(DR["EPD_TYPE"]);

                    if (strICDType == "Principal")
                    {
                        objInvClms.HIC_ICD_CODE_P = strICDCode;
                        goto EndFor;
                    }

                    if (strICDType == "Admitting")
                    {
                        objInvClms.HIC_ICD_CODE_A = strICDCode;
                        goto EndFor;
                    }

                    if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S1 == null)
                    {
                        objInvClms.HIC_ICD_CODE_S1 = strICDCode;
                        goto EndFor;
                    }

                    if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S2 == null)
                    {
                        objInvClms.HIC_ICD_CODE_S2 = strICDCode;
                        goto EndFor;
                    }

                    if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S3 == null)
                    {
                        objInvClms.HIC_ICD_CODE_S3 = strICDCode;
                        goto EndFor;
                    }

                    if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S4 == null)
                    {
                        objInvClms.HIC_ICD_CODE_S4 = strICDCode;
                        goto EndFor;
                    }

                    if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S5 == null)
                    {
                        objInvClms.HIC_ICD_CODE_S5 = strICDCode;
                        goto EndFor;
                    }

                    if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S6 == null)
                    {
                        objInvClms.HIC_ICD_CODE_S6 = strICDCode;
                        goto EndFor;
                    }

                    if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S7 == null)
                    {
                        objInvClms.HIC_ICD_CODE_S7 = strICDCode;
                        goto EndFor;
                    }

                    if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S8 == null)
                    {
                        objInvClms.HIC_ICD_CODE_S8 = strICDCode;
                        goto EndFor;
                    }

                    if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S11 == null)
                    {
                        objInvClms.HIC_ICD_CODE_S11 = strICDCode;
                        goto EndFor;
                    }

                    if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S12 == null)
                    {
                        objInvClms.HIC_ICD_CODE_S12 = strICDCode;
                        goto EndFor;
                    }
                    if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S13 == null)
                    {
                        objInvClms.HIC_ICD_CODE_S13 = strICDCode;
                        goto EndFor;
                    }
                    if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S14 == null)
                    {
                        objInvClms.HIC_ICD_CODE_S14 = strICDCode;
                        goto EndFor;
                    }

                    if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S15 == null)
                    {
                        objInvClms.HIC_ICD_CODE_S15 = strICDCode;
                        goto EndFor;
                    }
                    if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S16 == null)
                    {
                        objInvClms.HIC_ICD_CODE_S16 = strICDCode;
                        goto EndFor;
                    }
                    if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S17 == null)
                    {
                        objInvClms.HIC_ICD_CODE_S17 = strICDCode;
                        goto EndFor;
                    }
                    if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S18 == null)
                    {
                        objInvClms.HIC_ICD_CODE_S18 = strICDCode;
                        goto EndFor;
                    }
                    if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S19 == null)
                    {
                        objInvClms.HIC_ICD_CODE_S19 = strICDCode;
                        goto EndFor;
                    }
                    if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S20 == null)
                    {
                        objInvClms.HIC_ICD_CODE_S20 = strICDCode;
                        goto EndFor;
                    }
                    if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S21 == null)
                    {
                        objInvClms.HIC_ICD_CODE_S21 = strICDCode;
                        goto EndFor;
                    }
                    if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S22 == null)
                    {
                        objInvClms.HIC_ICD_CODE_S22 = strICDCode;
                        goto EndFor;
                    }
                    if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S23 == null)
                    {
                        objInvClms.HIC_ICD_CODE_S23 = strICDCode;
                        goto EndFor;
                    }
                    if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S24 == null)
                    {
                        objInvClms.HIC_ICD_CODE_S24 = strICDCode;
                        goto EndFor;
                    }
                    if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S25 == null)
                    {
                        objInvClms.HIC_ICD_CODE_S25 = strICDCode;
                        goto EndFor;
                    }
                    if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S26 == null)
                    {
                        objInvClms.HIC_ICD_CODE_S26 = strICDCode;
                        goto EndFor;
                    }

                    if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S27 == null)
                    {
                        objInvClms.HIC_ICD_CODE_S27 = strICDCode;
                        goto EndFor;
                    }
                    if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S28 == null)
                    {
                        objInvClms.HIC_ICD_CODE_S28 = strICDCode;
                        goto EndFor;
                    }

                EndFor: ;
                }

            }
            if (DS2.Tables[0].Rows.Count > 0)
            {
                objInvClms.InvoiceClaimDtlsAdd();
            }

        FunEnd: ;

        }

        #endregion

        #region AutoExt
        [System.Web.Services.WebMethod]
        public static string[] GetServicessList(string prefixText)
        {
            DataSet ds = new DataSet();
            CommonBAL dbo = new CommonBAL();

            string[] Data;
            if (intSrcType == 0)
            {
                string Criteria = " 1=1 ";




                ds = dbo.HaadServicessListGet("Diagnosis", prefixText, "", strSessionDeptId);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Data = new string[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {

                        Data[i] = ds.Tables[0].Rows[i]["Code"].ToString() + "~" + ds.Tables[0].Rows[i]["Description"].ToString();



                    }

                    return Data;
                }



            }
            else
            {


                string Criteria = " 1=1 ";

                Criteria += " AND( EDF_CODE like'%" + prefixText + "%' OR  EDF_NAME  like'%" + prefixText + "%')";

                Criteria += " AND EDF_TYPE='ICD' AND ( EDF_DR_ID='" + strUserCode + "' OR EDF_DEP_ID='" + strSessionDeptId + "' )";

                ds = dbo.FavoritesGet(Criteria);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Data = new string[ds.Tables[0].Rows.Count];
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Data[i] = ds.Tables[0].Rows[i]["Code"].ToString() + '~' + ds.Tables[0].Rows[i]["Description"].ToString();
                        // Data[i] = ds.Tables[0].Rows[i]["Code"].ToString() + "~" + ds.Tables[0].Rows[i]["Description"].ToString();


                    }

                    return Data;
                }

            }

            string[] Data1 = { "" };

            return Data1;
        }
        #endregion
        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../../Default.aspx?NoSession=1"); }
            
            if (Session["User_ID"] == null) { goto FunEnd; }

            lblMessage.Text = "";
            string parameter = Request["__EVENTARGUMENT"];
            if (parameter == "TextData")
            {
                BindData();
                txtSearch.Focus();

                //ScriptManager.RegisterStartupScript(this, this.GetType(), "LabResult", "ShowSearch();", true);



            }

            if (!IsPostBack)
            {
                try
                {
                    strSessionDeptId = Convert.ToString(Session["User_DeptID"]);
                    strUserCode = Convert.ToString(Session["User_Code"]);

                    BindTemplate();
                    BindHistory();
                    if (Convert.ToString(Session["HUM_ADDNEW_RECORD_FROMEMR"]) == "True")
                    {
                        btnNewService.Visible = true;
                    }
                    if (CheckPrincipalDiagnosis() == true)
                    {
                        drpDiagType.SelectedIndex = 1;
                    }


                    if (GlobalValues.FileDescription.ToUpper() == "MAMPILLY")
                    {
                        chkAnySearch.Visible = true;
                    }
                    BindScreenCustomization();
                    if (Convert.ToString(ViewState["CUST_VALUE"]) == "1")
                    {
                        drpSrcType.SelectedIndex = 1;
                        btnDeleteFav.Visible = true;
                        btnAddFav.Visible = false;
                    }
                    else
                    {
                        drpSrcType.SelectedIndex = 0;
                        btnAddFav.Visible = true;
                        btnDeleteFav.Visible = false;
                    }

                    if (Convert.ToString(Session["EPM_STATUS"]).ToUpper() == "Y" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                    {
                        drpHistory.Enabled = false;
                        drpTemplate.Enabled = false;


                    }
                    //HIDE THE SAVE OPTION FOR NEXT DAY OF THE VISIT
                    if (Convert.ToString(Session["EMRCurrentDateDiff"]) != "0" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                    {
                        drpHistory.Enabled = false;
                        drpTemplate.Enabled = false;

                    }

                    if (Convert.ToString(Session["EMR_VISIT_DTLS_EDIT_ALL_USERS"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "DOCTORS" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                    {
                        drpHistory.Enabled = false;
                        drpTemplate.Enabled = false;

                    }



                    if (Convert.ToString(Session["EMR_DISPLAY_TEMPLATE"]) == "1")
                    {
                        divTemplate.Visible = true;
                    }


                    if (Convert.ToString(Session["EMR_DISPLAY_HISTORY"]) == "1")
                    {
                        divHistoryDrp.Visible = true;
                    }


                    if (Convert.ToString(Session["EMR_DISPLAY_ICD_CROSSWALK"]) == "1")
                    {
                        btnCrosswalk.Visible = true;
                    }

                    if (Convert.ToString(Session["EMR_DIAG_UPDATE_INVOICE"]) == "1" && (Convert.ToString(Session["User_Category"]).ToUpper() == "ADMIN STAFF" || Convert.ToString(Session["User_Category"]).ToUpper() == "OTHERS"))
                    {
                        chkDiagUpdateInvoice.Visible = true;
                    }


                    intSrcType = drpSrcType.SelectedIndex;
                    BindData();
                    BindDiagnosis();


                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      Diagnosis.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }
            }
        FunEnd: ;
        }


        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                BindData();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Diagnosis.txtSearch_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void gvServicess_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvServicess.PageIndex = e.NewPageIndex;
                BindData();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Diagnosis.gvServicess_PageIndexChanging");
                TextFileWriting(ex.Message.ToString());
            }
        }




        protected void Edit_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
                ViewState["gvServSelectIndex"] = gvScanCard.RowIndex;
                gvServicess.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");

                Label lblCode = (Label)gvScanCard.Cells[0].FindControl("lblCode");
                Label lblDesc = (Label)gvScanCard.Cells[0].FindControl("lblDesc");
                Label lblPrice = (Label)gvScanCard.Cells[0].FindControl("lblPrice");

                ViewState["Code"] = lblCode.Text;
                ViewState["Description"] = lblDesc.Text;
                ViewState["Price"] = lblPrice.Text;
                lblCode.BackColor = System.Drawing.Color.FromName("#c5e26d");
                lblDesc.BackColor = System.Drawing.Color.FromName("#c5e26d");
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Diagnosis.Edit_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }


        protected void Add_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {
                TextFileWriting(System.DateTime.Now.ToString() + "     Start Diag Checking");

                if (gvDiagnosis.Rows.Count > 0 && drpDiagType.SelectedValue == "Principal")
                {
                    if (CheckPrincipalDiagnosis() == true)
                    {
                        lblMessage.Text = "Principal diagnosis already added";
                        lblMessage.ForeColor = System.Drawing.Color.Red;
                        goto FunEnd;
                    }
                }
                TextFileWriting(System.DateTime.Now.ToString() + "     End Diag Checking");

                Int32 intPos = 1;
                if (gvDiagnosis.Rows.Count >= 1)
                {
                    intPos = Convert.ToInt32(gvDiagnosis.Rows.Count) + 1;

                }

                ImageButton btnEdit = new ImageButton();
                btnEdit = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

                // GridViewRow currentRow = (GridViewRow)((ImageButton)sender).Parent.Parent.Parent.Parent;


                Label lblCode = (Label)gvScanCard.FindControl("lblCode");
                Label lblDesc = (Label)gvScanCard.FindControl("lblDesc");
                Label lblPrice = (Label)gvScanCard.FindControl("lblPrice");


                if (CheckDiagnosis(lblCode.Text) == true)
                {
                    lblMessage.Text = "This code already added";
                    lblMessage.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }
                TextFileWriting(System.DateTime.Now.ToString() + "     Start Diag Add");
                EMR_PTDiagnosis objDiag = new EMR_PTDiagnosis();
                objDiag.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objDiag.EPD_ID = Convert.ToString(Session["EMR_ID"]);
                objDiag.EPD_DIAG_CODE = lblCode.Text;
                objDiag.EPD_DIAG_NAME = lblDesc.Text;
                objDiag.EPD_REMARKS = txtRemarks.Text.Trim().Replace("'", "''");
                objDiag.EPD_TEMPLATE_CODE = "";
                objDiag.EPD_PROBLEM_TYPE = "";
                objDiag.EPD_TYPE = drpDiagType.SelectedValue;
                objDiag.EPD_POSITION = Convert.ToString(intPos);
                objDiag.DiagnosisAdd();

                TextFileWriting(System.DateTime.Now.ToString() + "     End Diag Add");
                BindDiagnosis();

                TextFileWriting(System.DateTime.Now.ToString() + "     Load Diag  ");
                drpDiagType.SelectedValue = "Secondary";

                CommonBAL objCom = new CommonBAL();
                string FieldNameWithValues = "EPD_REMARKS='" + txtRemarks.Text + "'";
                string Criteria = "EPD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND EPD_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
                objCom.fnUpdateTableData(FieldNameWithValues, "EMR_PT_DIAGNOSIS", Criteria);
                TextFileWriting(System.DateTime.Now.ToString() + "     Update remars");

                if (Convert.ToString(Session["EMR_DISPLAY_AUDIT"]) == "1")
                {
                    UpdateAuditDtls();
                    TextFileWriting(System.DateTime.Now.ToString() + "     Update Audit dtls ");
                }


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Diagnosis.Add_Click");
                TextFileWriting(ex.Message.ToString());
            }
        FunEnd: ;
        }

        protected void btnAddDiag_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }

            try
            {
                if (txtDiagCode.Text == "" || txtDiagCode.Text == null)
                {
                    lblMessage.Text = "Please select the Code";
                    lblMessage.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }



                if (gvDiagnosis.Rows.Count > 0 && drpDiagType.SelectedValue == "Principal")
                {
                    if (CheckPrincipalDiagnosis() == true)
                    {
                        lblMessage.Text = "Principal diagnosis already added";
                        lblMessage.ForeColor = System.Drawing.Color.Red;
                        goto FunEnd;
                    }
                }


                Int32 intPos = 1;
                if (gvDiagnosis.Rows.Count >= 1)
                {
                    intPos = Convert.ToInt32(gvDiagnosis.Rows.Count) + 1;

                }



                if (CheckDiagnosis(txtDiagCode.Text) == true)
                {
                    lblMessage.Text = "This code already added";
                    lblMessage.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                EMR_PTDiagnosis objDiag = new EMR_PTDiagnosis();
                objDiag.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objDiag.EPD_ID = Convert.ToString(Session["EMR_ID"]);
                objDiag.EPD_DIAG_CODE = txtDiagCode.Text;
                objDiag.EPD_DIAG_NAME = txtDiagName.Text;
                objDiag.EPD_REMARKS = txtRemarks.Text.Trim().Replace("'", "''");
                objDiag.EPD_TEMPLATE_CODE = "";
                objDiag.EPD_PROBLEM_TYPE = "";
                objDiag.EPD_TYPE = drpDiagType.SelectedValue;
                objDiag.EPD_POSITION = Convert.ToString(intPos);
                objDiag.DiagnosisAdd();


                BindDiagnosis();

                drpDiagType.SelectedValue = "Secondary";

                CommonBAL objCom = new CommonBAL();
                string FieldNameWithValues = "EPD_REMARKS='" + txtRemarks.Text + "'";
                string Criteria = "EPD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND EPD_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
                objCom.fnUpdateTableData(FieldNameWithValues, "EMR_PT_DIAGNOSIS", Criteria);


                if (Convert.ToString(Session["EMR_DISPLAY_AUDIT"]) == "1")
                {
                    UpdateAuditDtls();
                }

                txtDiagCode.Text = "";
                txtDiagName.Text = "";

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.btnAddPhar_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;

        }

        protected void drpSrcType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            if (drpSrcType.SelectedIndex == 0)
            {
                btnAddFav.Visible = true;
                btnDeleteFav.Visible = false;
            }
            else
            {
                btnAddFav.Visible = false;
                btnDeleteFav.Visible = true;
            }
            intSrcType = drpSrcType.SelectedIndex;
            BindData();
        FunEnd: ;
        }

        protected void btnAddFav_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {

                if (string.IsNullOrEmpty(Convert.ToString(ViewState["Code"])) == true)
                {
                    goto FunEnd;
                }
                if (string.IsNullOrEmpty(Convert.ToString(ViewState["Description"])) == true)
                {
                    goto FunEnd;
                }

                if (CheckFavorite(Convert.ToString(ViewState["Code"])) == true)
                {
                    goto FunEnd;
                }


                CommonBAL objCom = new CommonBAL();
                objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
                objCom.Code = Convert.ToString(ViewState["Code"]);
                objCom.Description = Convert.ToString(ViewState["Description"]);
                objCom.DR_ID = Convert.ToString(Session["User_Code"]);
                objCom.DEP_ID = Convert.ToString(Session["User_DeptID"]);
                objCom.Type = "ICD";
                objCom.Price = Convert.ToString(ViewState["Price"]);
                objCom.FavoritesAdd();

                if (string.IsNullOrEmpty(Convert.ToString(ViewState["gvServSelectIndex"])) == false)
                {
                    Int32 R = 0;
                    R = Convert.ToInt32(ViewState["gvServSelectIndex"]);
                    Label lblCode = (Label)gvServicess.Rows[R].Cells[0].FindControl("lblCode");
                    Label lblDesc = (Label)gvServicess.Rows[R].Cells[0].FindControl("lblDesc");
                    Label lblPrice = (Label)gvServicess.Rows[R].Cells[0].FindControl("lblPrice");

                    if (R % 2 == 0)
                    {
                        lblCode.BackColor = System.Drawing.Color.FromName("#ffffff");
                        lblDesc.BackColor = System.Drawing.Color.FromName("#ffffff");
                        gvServicess.Rows[R].BackColor = System.Drawing.Color.FromName("#ffffff");
                    }
                    else
                    {
                        lblCode.BackColor = System.Drawing.Color.FromName("#f6f6f6");
                        lblDesc.BackColor = System.Drawing.Color.FromName("#f6f6f6");
                        gvServicess.Rows[R].BackColor = System.Drawing.Color.FromName("#f6f6f6");
                    }

                }

                ClearFavorite();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Diagnosis.btnAddFav_Click");
                TextFileWriting(ex.Message.ToString());
            }
        FunEnd: ;
        }

        protected void btnDeleteFav_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {

                if (string.IsNullOrEmpty(Convert.ToString(ViewState["Code"])) == true)
                {
                    goto FunEnd;
                }
                if (string.IsNullOrEmpty(Convert.ToString(ViewState["Description"])) == true)
                {
                    goto FunEnd;
                }
                if (drpSrcType.SelectedIndex == 0)
                {
                    goto FunEnd;
                }

                //if (CheckFavorite(Convert.ToString(ViewState["Code"])) == false)
                //{
                //    goto FunEnd;
                //}


                CommonBAL objCom = new CommonBAL();
                objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
                objCom.Code = Convert.ToString(ViewState["Code"]);
                objCom.DR_ID = Convert.ToString(Session["User_Code"]);
                objCom.DEP_ID = Convert.ToString(Session["User_DeptID"]);
                objCom.Type = "ICD";
                objCom.FavoritesDelete();

                ClearFavorite();

                BindData(); ;


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Diagnosis.btnDeleteFav_Click");
                TextFileWriting(ex.Message.ToString());
            }
        FunEnd: ;
        }

        protected void gvServicess_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblDesc = (Label)e.Row.FindControl("lblDesc");
                lblDesc.Attributes.Add("OnDblClick", "alert('test');");
            }


        }


        protected void gvDiagnosis_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            CommonBAL objCom = new CommonBAL();
            string FieldNameWithValues = "";
            string Criteria = "";

            DataTable dt = new DataTable();
            DataTable dtnew = new DataTable();

            int index = Convert.ToInt32(e.CommandArgument) - 1;

            if (e.CommandName == "Up")
            {

                if (index == 0)
                {
                    lblMessage.Text = "You Cant move recor' Up";
                    lblMessage.Visible = true;
                    return;
                }


                dt = (DataTable)ViewState["DiagDT"];
                int value = Convert.ToInt32(dt.Rows[index]["EPD_POSITION"].ToString());
                dt.Rows[index]["EPD_POSITION"] = Convert.ToInt32(value) - 1;
                dt.Rows[index - 1]["EPD_POSITION"] = value;// Convert.ToInt32(index);
                dt.DefaultView.Sort = "EPD_POSITION";
                dt.AcceptChanges();
                gvDiagnosis.DataSource = dt;
                gvDiagnosis.DataBind();
                dt.AcceptChanges();

            }
            if (e.CommandName == "Down")
            {


                dt = (DataTable)ViewState["DiagDT"];


                if (Convert.ToInt16(index + 1) == dt.Rows.Count)
                {
                    lblMessage.Text = "You Cant move record down";
                    lblMessage.Visible = true;
                    return;
                }



                int value = Convert.ToInt32(dt.Rows[index]["EPD_POSITION"].ToString());
                dt.Rows[index]["EPD_POSITION"] = Convert.ToInt32(value) + 1;
                dt.Rows[index + 1]["EPD_POSITION"] = value;// Convert.ToInt32(index);
                dt.DefaultView.Sort = "EPD_POSITION";
                dt.AcceptChanges();
                gvDiagnosis.DataSource = dt;
                gvDiagnosis.DataBind();
                dt.AcceptChanges();
            }



            for (int i = 0; i <= gvDiagnosis.Rows.Count - 1; i++)
            {

                Label lblDiagType = (Label)gvDiagnosis.Rows[i].FindControl("lblDiagType");
                Label lblDiagCode = (Label)gvDiagnosis.Rows[i].FindControl("lblDiagCode");
                Label lblDiagDesc = (Label)gvDiagnosis.Rows[i].FindControl("lblDiagDesc");
                Label lblPosition = (Label)gvDiagnosis.Rows[i].FindControl("lblPosition");



                objCom = new CommonBAL();
                Int32 intPos = i + 1;


                FieldNameWithValues = " EPD_POSITION=" + intPos;
                Criteria = " EPD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND EPD_ID=" + Convert.ToString(Session["EMR_ID"]) + " AND EPD_DIAG_CODE='" + lblDiagCode.Text.Trim() + "'";
                objCom.fnUpdateTableData(FieldNameWithValues, "EMR_PT_DIAGNOSIS", Criteria);



            }

            objCom = new CommonBAL();
            FieldNameWithValues = " EPD_TYPE='Principal'";
            Criteria = " EPD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND EPD_ID=" + Convert.ToString(Session["EMR_ID"]) + " AND  EPD_POSITION=1";
            objCom.fnUpdateTableData(FieldNameWithValues, "EMR_PT_DIAGNOSIS", Criteria);


            objCom = new CommonBAL();
            FieldNameWithValues = " EPD_TYPE='Secondary'";
            Criteria = " EPD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND EPD_ID=" + Convert.ToString(Session["EMR_ID"]) + "  AND  EPD_POSITION=2";
            objCom.fnUpdateTableData(FieldNameWithValues, "EMR_PT_DIAGNOSIS", Criteria);


            BindDiagnosis();

            if (e.CommandName == "Up")
            {
                gvDiagnosis.Rows[index - 1].BackColor = System.Drawing.Color.FromName("#bfbfbf");
            }

            if (e.CommandName == "Down")
            {
                gvDiagnosis.Rows[index + 1].BackColor = System.Drawing.Color.FromName("#bfbfbf");
            }





        }



        protected void DeleteeDiag_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {
                CommonBAL objCom = new CommonBAL();
                string FieldNameWithValues = "";
                string Criteria = "";
                ImageButton btnDel = new ImageButton();
                btnDel = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnDel.Parent.Parent;

                Label lblDiagCode = (Label)gvScanCard.Cells[0].FindControl("lblDiagCode");


                EMR_PTDiagnosis objDiag = new EMR_PTDiagnosis();
                objDiag.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objDiag.EPD_ID = Convert.ToString(Session["EMR_ID"]);
                objDiag.EPD_DIAG_CODE = lblDiagCode.Text;

                objDiag.DiagnosisDelete();
                BindDiagnosis();


                for (int i = 0; i <= gvDiagnosis.Rows.Count - 1; i++)
                {

                    Label lblDiagType = (Label)gvDiagnosis.Rows[i].FindControl("lblDiagType");
                    Label lblDiagCode1 = (Label)gvDiagnosis.Rows[i].FindControl("lblDiagCode");
                    Label lblDiagDesc = (Label)gvDiagnosis.Rows[i].FindControl("lblDiagDesc");
                    Label lblPosition = (Label)gvDiagnosis.Rows[i].FindControl("lblPosition");




                    Int32 intPos = i + 1;

                    objCom = new CommonBAL();
                    FieldNameWithValues = " EPD_POSITION=" + intPos;
                    Criteria = " EPD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND EPD_ID=" + Convert.ToString(Session["EMR_ID"]) + " AND EPD_DIAG_CODE='" + lblDiagCode1.Text.Trim() + "'";
                    objCom.fnUpdateTableData(FieldNameWithValues, "EMR_PT_DIAGNOSIS", Criteria);




                }




                objCom = new CommonBAL();
                FieldNameWithValues = " EPD_TYPE='Principal'";
                Criteria = " EPD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND EPD_ID=" + Convert.ToString(Session["EMR_ID"]) + " AND  EPD_POSITION=1";
                objCom.fnUpdateTableData(FieldNameWithValues, "EMR_PT_DIAGNOSIS", Criteria);


                objCom = new CommonBAL();
                FieldNameWithValues = " EPD_TYPE='Secondary'";
                Criteria = " EPD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND EPD_ID=" + Convert.ToString(Session["EMR_ID"]) + "  AND  EPD_POSITION=2";
                objCom.fnUpdateTableData(FieldNameWithValues, "EMR_PT_DIAGNOSIS", Criteria);




                BindDiagnosis();


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Diagnosis.DeleteeDiag_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;

        }


        /*
        protected void imgbtnUp_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {
                ImageButton btnDel = new ImageButton();
                btnDel = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnDel.Parent.Parent;


                int intCurRow = gvScanCard.RowIndex;

                Label lblDiagCode = (Label)gvScanCard.Cells[0].FindControl("lblDiagCode");
                Label lblPosition = (Label)gvScanCard.Cells[0].FindControl("lblPosition");


                Int32 intPosition = 1;

                if (lblPosition.Text != "" && lblPosition.Text != "1")
                {

                    intPosition = Convert.ToInt32(lblPosition.Text) - 1;


                    CommonBAL objCom = new CommonBAL();
                    string FieldNameWithValues = " EPD_POSITION=" + intPosition;
                    string Criteria = " EPD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND EPD_ID=" + Convert.ToString(Session["EMR_ID"]) + " AND EPD_DIAG_CODE='" + lblDiagCode.Text.Trim() + "'";
                    objCom.fnUpdateTableData(FieldNameWithValues, "EMR_PT_DIAGNOSIS", Criteria);


                    objCom = new CommonBAL();

                    FieldNameWithValues = " EPD_POSITION=" + lblPosition.Text;
                    Criteria = " EPD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND EPD_ID=" + Convert.ToString(Session["EMR_ID"]) +
                       " AND EPD_POSITION=" + intPosition + " AND EPD_DIAG_CODE !='" + lblDiagCode.Text.Trim() + "'";
                    objCom.fnUpdateTableData(FieldNameWithValues, "EMR_PT_DIAGNOSIS", Criteria);




                    if (gvScanCard.RowIndex == 1)
                    {
                        Label lblFirstDiagCode = (Label)gvDiagnosis.Rows[0].Cells[0].FindControl("lblDiagCode");
                        objCom = new CommonBAL();
                        FieldNameWithValues = " EPD_TYPE='Secondary'";
                        Criteria = " EPD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND EPD_ID=" + Convert.ToString(Session["EMR_ID"]) + " AND EPD_DIAG_CODE='" + lblFirstDiagCode.Text.Trim() + "'";
                        objCom.fnUpdateTableData(FieldNameWithValues, "EMR_PT_DIAGNOSIS", Criteria);

                        Label lblSectDiagCode = (Label)gvDiagnosis.Rows[1].Cells[0].FindControl("lblDiagCode");
                        objCom = new CommonBAL();
                        FieldNameWithValues = " EPD_TYPE='Principal'";
                        Criteria = " EPD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND EPD_ID=" + Convert.ToString(Session["EMR_ID"]) + " AND EPD_DIAG_CODE='" + lblSectDiagCode.Text.Trim() + "'";
                        objCom.fnUpdateTableData(FieldNameWithValues, "EMR_PT_DIAGNOSIS", Criteria);
                    }


                    BindDiagnosis();
                    gvDiagnosis.Rows[intCurRow - 1].BackColor = System.Drawing.Color.FromName("#bfbfbf");


                }



            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Diagnosis.imgbtnUp_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;

        }

        protected void imgbtnDown_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {
                ImageButton btnDel = new ImageButton();
                btnDel = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnDel.Parent.Parent;

                int intCurRow = gvScanCard.RowIndex;

                Label lblDiagCode = (Label)gvScanCard.Cells[0].FindControl("lblDiagCode");
                Label lblPosition = (Label)gvScanCard.Cells[0].FindControl("lblPosition");


                Int32 intPosition = 1;

                if (lblPosition.Text != "" && gvScanCard.RowIndex != gvDiagnosis.Rows.Count)
                {
                    intPosition = Convert.ToInt32(lblPosition.Text) + 1;

                    CommonBAL objCom = new CommonBAL();
                    string FieldNameWithValues = " EPD_POSITION=" + intPosition;
                    string Criteria = " EPD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND EPD_ID=" + Convert.ToString(Session["EMR_ID"]) + " AND EPD_DIAG_CODE='" + lblDiagCode.Text.Trim() + "'";
                    objCom.fnUpdateTableData(FieldNameWithValues, "EMR_PT_DIAGNOSIS", Criteria);


                    objCom = new CommonBAL();

                    FieldNameWithValues = " EPD_POSITION=" + lblPosition.Text;
                    Criteria = " EPD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND EPD_ID=" + Convert.ToString(Session["EMR_ID"]) +
                       " AND EPD_POSITION=" + intPosition + " AND EPD_DIAG_CODE !='" + lblDiagCode.Text.Trim() + "'";
                    objCom.fnUpdateTableData(FieldNameWithValues, "EMR_PT_DIAGNOSIS", Criteria);

                    if (gvScanCard.RowIndex == 0)
                    {
                        Label lblFirstDiagCode = (Label)gvDiagnosis.Rows[0].Cells[0].FindControl("lblDiagCode");
                        objCom = new CommonBAL();
                        FieldNameWithValues = " EPD_TYPE='Secondary'";
                        Criteria = " EPD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND EPD_ID=" + Convert.ToString(Session["EMR_ID"]) + " AND EPD_DIAG_CODE='" + lblFirstDiagCode.Text.Trim() + "'";
                        objCom.fnUpdateTableData(FieldNameWithValues, "EMR_PT_DIAGNOSIS", Criteria);

                        Label lblSectDiagCode = (Label)gvDiagnosis.Rows[1].Cells[0].FindControl("lblDiagCode");
                        objCom = new CommonBAL();
                        FieldNameWithValues = " EPD_TYPE='Principal'";
                        Criteria = " EPD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND EPD_ID=" + Convert.ToString(Session["EMR_ID"]) + " AND EPD_DIAG_CODE='" + lblSectDiagCode.Text.Trim() + "'";
                        objCom.fnUpdateTableData(FieldNameWithValues, "EMR_PT_DIAGNOSIS", Criteria);
                    }

                    BindDiagnosis();

                    gvDiagnosis.Rows[intCurRow + 1].BackColor = System.Drawing.Color.FromName("#bfbfbf");
                }




            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Diagnosis.imgbtnDown_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;




        }


        */
        #endregion


        /*
        protected void gvServicess_RowDataBound1(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                Label lblCode = (Label)e.Row.FindControl("lblCode");
                Label lblDesc = (Label)e.Row.FindControl("lblDesc");

                e.Row.Attributes["ondblclick"] = String.Format("__DoPostBack('{0}','{1}');", lblCode.Text, lblDesc.Text);
                e.Row.Style.Add(HtmlTextWriterStyle.Cursor, "pointer");


            }
        }
         * */

        ////[System.Web.Services.WebMethod]
        ////public static void SaveDiag()
        ////{

        ////    Int32 intPos = 1;
        ////    if (DiagCount >= 1)
        ////    {
        ////        intPos = Convert.ToInt32(DiagCount) + 1;

        ////    }

        ////    EMR_PTDiagnosis objDiag = new EMR_PTDiagnosis();
        ////    objDiag.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
        ////    objDiag.EPD_ID = Convert.ToString(Session["EMR_ID"]);
        ////    objDiag.EPD_DIAG_CODE = lblCode.Text;
        ////    objDiag.EPD_DIAG_NAME = lblDesc.Text;
        ////    objDiag.EPD_REMARKS = txtRemarks.Text.Trim();
        ////    objDiag.EPD_TEMPLATE_CODE = "";
        ////    objDiag.EPD_PROBLEM_TYPE = "";
        ////    objDiag.EPD_POSITION = Convert.ToString(intPos);
        ////    objDiag.DiagnosisAdd();


        ////   // BindDiagnosis();
        ////}

        //[System.Web.Services.WebMethod]
        //public static string[] GetSegmentValue(string prefixText, string Type, string SubType, string FieldName)
        //{
        //    string[] Data;
        //    string Criteria = " 1=1 ";

        //    Criteria += " AND HCM_STATUS = 'A'  ";
        //    Criteria += " AND ESVD_TYPE = '" + Type + "' AND ESVD_SUBTYPE='" + SubType + "' AND ESVD_FIELDNAME='" + FieldName + "'";
        //    Criteria += " AND ESVD_VALUE Like '%" + prefixText + "%'";

        //    DataSet DS = new DataSet();
        //    Criteria += " AND ESVD_ID='" + GlobalValues.EMR_ID + "'";
        //    CommonBAL objCom = new CommonBAL();
        //    DS = _segmentBAL.SegmentVisitDtlsGet(Criteria);
        //    if (DS.Tables[0].Rows.Count > 0)
        //    {
        //        Data = new string[DS.Tables[0].Rows.Count];
        //        for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
        //        {
        //            Data[i] = DS.Tables[0].Rows[i]["ESVD_VALUE"].ToString();
        //        }

        //        return Data;
        //    }
        //    string[] Data1 = { "" };

        //    return Data1;
        //}


        public string GetValue()
        {

            Int32 intPos = 1;
            if (DiagCount >= 1)
            {
                intPos = Convert.ToInt32(DiagCount) + 1;

            }

            EMR_PTDiagnosis objDiag = new EMR_PTDiagnosis();
            objDiag.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
            objDiag.EPD_ID = Convert.ToString(Session["EMR_ID"]);
            objDiag.EPD_DIAG_CODE = hidSeleCode.Value;
            objDiag.EPD_DIAG_NAME = hidSeleDesc.Value;
            objDiag.EPD_REMARKS = txtRemarks.Text.Trim();
            objDiag.EPD_TEMPLATE_CODE = "";
            objDiag.EPD_PROBLEM_TYPE = "";
            objDiag.EPD_POSITION = Convert.ToString(intPos);
            objDiag.DiagnosisAdd();
            BindDiagnosis();



            return "";
        }

        protected void gvDiagnosis_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblDiagType = (Label)e.Row.FindControl("lblDiagType");
                    Button btnColor = (Button)e.Row.FindControl("btnColor");

                    if (lblDiagType.Text == "Principal")
                    {
                        btnColor.BackColor = System.Drawing.ColorTranslator.FromHtml("#ff0000");

                    }
                    else if (lblDiagType.Text == "Secondary")
                    {
                        btnColor.BackColor = System.Drawing.ColorTranslator.FromHtml("#9ACD32");
                    }
                    else if (lblDiagType.Text == "Admitting")
                    {
                        btnColor.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFF00");
                    }
                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Diagnosis.gvDiagnosis_RowDataBound");
                TextFileWriting(ex.Message.ToString());
            }
        }


        private static void EnableNextPrevNavigationForNumericPagedGrid(GridView gv)
        {
            if (gv.BottomPagerRow == null)
                return;
            Table pagerTable = (Table)gv.BottomPagerRow.Controls[0].Controls[0];

            bool prevAdded = false;
            if (gv.PageIndex != 0)
            {
                TableCell prevCell = new TableCell();
                LinkButton prevLink = new LinkButton
                {
                    Text = "<<",
                    CommandName = "Page",
                    CommandArgument = ((LinkButton)pagerTable.Rows[0].Cells[gv.PageIndex - 1].Controls[0]).CommandArgument
                };
                prevCell.Controls.Add(prevLink);
                pagerTable.Rows[0].Cells.AddAt(0, prevCell);
                prevAdded = true;
            }

            if (gv.PageIndex != gv.PageCount - 1)
            {
                TableCell nextCell = new TableCell();
                LinkButton nextLink = new LinkButton
                {
                    Text = ">>",
                    CommandName = "Page",
                    CommandArgument = ((LinkButton)pagerTable.Rows[0].Cells[gv.PageIndex +
                      (prevAdded ? 2 : 1)].Controls[0]).CommandArgument
                };
                nextCell.Controls.Add(nextLink);
                pagerTable.Rows[0].Cells.Add(nextCell);
            }
        }

        protected void gvServicess_DataBound(object sender, EventArgs e)
        {
            // EnableNextPrevNavigationForNumericPagedGrid(gvServicess);
        }

        protected void txtRemarks_TextChanged(object sender, EventArgs e)
        {
            CommonBAL objCom = new CommonBAL();
            string FieldNameWithValues = "EPD_REMARKS='" + txtRemarks.Text + "'";
            string Criteria = "EPD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND EPD_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
            objCom.fnUpdateTableData(FieldNameWithValues, "EMR_PT_DIAGNOSIS", Criteria);
        }

        protected void btnAddNewService_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {
                lblNewServStatus.Text = "";
                if (txtNewServCode.Text == "" || txtNewServCode.Text == null)
                {
                    lblNewServStatus.Text = "Please Enter the Code";
                    lblNewServStatus.Visible = true;
                    lblNewServStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                if (txtNewServDesc.Text == "" || txtNewServDesc.Text == null)
                {
                    lblNewServStatus.Text = "Please Enter the Name";
                    lblNewServStatus.Visible = true;
                    lblNewServStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                if (CheckServiceAvailable() == true)
                {
                    lblNewServStatus.Text = "ICD  Already available";
                    lblNewServStatus.Visible = true;
                    lblNewServStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }
                EMR_PTDiagnosis objDiag = new EMR_PTDiagnosis();
                objDiag.BranchID = Convert.ToString(Session["Branch_ID"]);
                objDiag.ServCode = txtNewServCode.Text.Trim();
                objDiag.ServDesc = txtNewServDesc.Text;
                objDiag.UserID = Convert.ToString(Session["User_ID"]);

                objDiag.ICDAdd();

                lblNewServStatus.Text = "New ICD added";
                lblNewServStatus.Visible = true;
                lblNewServStatus.ForeColor = System.Drawing.Color.Green;

                txtNewServCode.Text = "";
                txtNewServDesc.Text = "";



            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.btnAddNewService_Click");
                TextFileWriting(ex.Message.ToString());
            }
        FunEnd: ;
        }

        protected void btnSaveTemp_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }

            try
            {
                string TemplateCode = "";
                CommonBAL objCom = new CommonBAL();
                objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
                objCom.Type = "Diagnosis";
                objCom.Description = txtTemplateName.Value;
                objCom.TemplateData = "";
                if (chkOtherDr.Checked == true)
                {
                    objCom.AllDr = "1";
                }
                else
                {
                    objCom.AllDr = "0";
                }
                objCom.DR_ID = Convert.ToString(Session["HPV_DR_ID"]);
                objCom.DEP_ID = Convert.ToString(Session["HPV_DEP_NAME"]);
                TemplateCode = objCom.TemplatesAdd();


                EMR_PTDiagnosis objPro = new EMR_PTDiagnosis();
                objPro.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objPro.EPD_ID = Convert.ToString(Session["EMR_ID"]);
                objPro.TemplateCode = TemplateCode;
                objPro.DiagnosisTemplateAdd();
                BindTemplate();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.btnSaveTemp_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;
        }

        protected void btnDeleteTemp_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }

            try
            {

                CommonBAL objCom = new CommonBAL();

                string Criteria = "EPL_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND EPL_TEMPLATE_CODE='" + drpTemplate.SelectedValue + "'";
                objCom.fnDeleteTableData("EMR_PT_DIAGNOSIS_TEMPLATES", Criteria);



                string Criteria1 = "ET_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND ET_CODE='" + drpTemplate.SelectedValue + "' and ET_TYPE='Diagnosis'";
                objCom.fnDeleteTableData("EMR_TEMPLATES", Criteria1);


                BindTemplate();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.btnDeleteTemp_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;
        }

        protected void drpTemplate_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {


                if (drpTemplate.SelectedIndex != 0)
                {
                    EMR_PTDiagnosis objPro = new EMR_PTDiagnosis();
                    objPro.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                    objPro.EPD_ID = Convert.ToString(Session["EMR_ID"]);
                    objPro.TemplateCode = drpTemplate.SelectedValue;
                    objPro.DiagnosisAddFromTemplate();
                    BindDiagnosis();
                }



            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.drpTemplate_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;
        }


        protected void imgbtnDown_Click(object sender, ImageClickEventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {


                string[] arrHistory = drpHistory.SelectedValue.ToString().Split('-');
                string BranchID = "", strEMR_ID = "", strDR_Code = "";

                if (arrHistory.Length > 0)
                {
                    BranchID = arrHistory[0];
                    if (arrHistory.Length > 0)
                    {
                        strEMR_ID = arrHistory[1];
                    }

                    if (arrHistory.Length > 2)
                    {
                        strDR_Code = arrHistory[2];
                    }

                }



                if (drpHistory.SelectedIndex != 0)
                {
                    EMR_PTDiagnosis objDiag = new EMR_PTDiagnosis();
                    objDiag.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                    objDiag.EPD_ID = Convert.ToString(Session["EMR_ID"]);
                    objDiag.SelectedBranch_ID = BranchID;
                    objDiag.SelectedEMR_ID = strEMR_ID;

                    objDiag.DiagnosisAddFromHistory();
                    BindDiagnosis();
                }



            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Diagnosis.imgbtnDown_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;
        }

        protected void imgbtnPrescHistView_Click(object sender, ImageClickEventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {


                string[] arrHistory = drpHistory.SelectedValue.ToString().Split('-');
                string BranchID = "", strEMR_ID = "", strDR_Code = "";

                if (arrHistory.Length > 0)
                {
                    BranchID = arrHistory[0];
                    if (arrHistory.Length > 0)
                    {
                        strEMR_ID = arrHistory[1];
                    }

                    if (arrHistory.Length > 2)
                    {
                        strDR_Code = arrHistory[2];
                    }

                }



                if (drpHistory.SelectedIndex != 0)
                {

                    BindDiagnosisHistory(strEMR_ID);
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowHistory()", true);
                }



            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Diagnosis.imgbtnDown_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;
        }

        protected void ImgDiagCrosswalkShow_Click(object sender, EventArgs e)
        {
            BindDiagCrosswalk();
            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowDiagCrosswalkPopup()", true);
        }

        protected void imgAddDiagCrosswalk_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {
                if (gvDiagnosis.Rows.Count > 0 && drpDiagType.SelectedValue == "Principal")
                {
                    if (CheckPrincipalDiagnosis() == true)
                    {
                        lblMessage.Text = "Principal diagnosis already added";
                        lblMessage.ForeColor = System.Drawing.Color.Red;
                        goto FunEnd;
                    }
                }


                Int32 intPos = 1;
                if (gvDiagnosis.Rows.Count >= 1)
                {
                    intPos = Convert.ToInt32(gvDiagnosis.Rows.Count) + 1;

                }

                ImageButton btnEdit = new ImageButton();
                btnEdit = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

                // GridViewRow currentRow = (GridViewRow)((ImageButton)sender).Parent.Parent.Parent.Parent;

               
                Label lblCode;
                Label lblDesc;

                if (radDiagCrosswalkType.SelectedIndex == 0)
                {
                    lblCode = (Label)gvScanCard.FindControl("lblCode_10");
                    lblDesc = (Label)gvScanCard.FindControl("lblDesc_10");
                }
                else
                {
                    lblCode = (Label)gvScanCard.FindControl("lblCode_9");
                    lblDesc = (Label)gvScanCard.FindControl("lblDesc_9");
                }


                if (CheckDiagnosis(lblCode.Text) == true)
                {
                    lblMessage.Text = "This code already added";
                    lblMessage.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                EMR_PTDiagnosis objDiag = new EMR_PTDiagnosis();
                objDiag.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objDiag.EPD_ID = Convert.ToString(Session["EMR_ID"]);
                objDiag.EPD_DIAG_CODE = lblCode.Text;
                objDiag.EPD_DIAG_NAME = lblDesc.Text;
                objDiag.EPD_REMARKS = txtRemarks.Text.Trim().Replace("'", "''");
                objDiag.EPD_TEMPLATE_CODE = "";
                objDiag.EPD_PROBLEM_TYPE = "";
                objDiag.EPD_TYPE = drpDiagType.SelectedValue;
                objDiag.EPD_POSITION = Convert.ToString(intPos);
                objDiag.DiagnosisAdd();


                BindDiagnosis();
                BindDiagCrosswalk();
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowDiagCrosswalkPopup()", true);
                drpDiagType.SelectedValue = "Secondary";

                CommonBAL objCom = new CommonBAL();
                string FieldNameWithValues = "EPD_REMARKS='" + txtRemarks.Text + "'";
                string Criteria = "EPD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND EPD_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
                objCom.fnUpdateTableData(FieldNameWithValues, "EMR_PT_DIAGNOSIS", Criteria);


                if (Convert.ToString(Session["EMR_DISPLAY_AUDIT"]) == "1")
                {
                    UpdateAuditDtls();
                }


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Diagnosis.Add_Click");
                TextFileWriting(ex.Message.ToString());
            }
        FunEnd: ;
        }


        protected void btnFavDiagCrosswalk_Click(object sender, EventArgs e)
        {
            
            try
            {

                Button btnEdit = new Button();
                btnEdit = (Button)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
                Label lblCode;
                Label lblDesc;

                if (radDiagCrosswalkType.SelectedIndex == 0)
                {
                    lblCode = (Label)gvScanCard.FindControl("lblCode_10");
                    lblDesc = (Label)gvScanCard.FindControl("lblDesc_10");
                }
                else
                {
                    lblCode = (Label)gvScanCard.FindControl("lblCode_9");
                    lblDesc = (Label)gvScanCard.FindControl("lblDesc_9");
                }


                if (CheckFavorite(lblCode.Text ) == true)
                {
                    lblMessage.Text = "This code already added in Favorite";
                    lblMessage.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }


                CommonBAL objCom = new CommonBAL();
                objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
                objCom.Code = lblCode.Text;
                objCom.Description = lblDesc.Text ;
                objCom.DR_ID = Convert.ToString(Session["User_Code"]);
                objCom.DEP_ID = Convert.ToString(Session["User_DeptID"]);
                objCom.Type = "ICD";
                objCom.Price = "0";
                objCom.FavoritesAdd();
 

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Diagnosis.btnFavDiagCrosswalk_Click");
                TextFileWriting(ex.Message.ToString());
            }
        FunEnd: ;
        }


        protected void radDiagCrosswalkType_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindDiagCrosswalk();
        }

        protected void chkDiagUpdateInvoice_CheckedChanged(object sender, EventArgs e)
        {
            if (chkDiagUpdateInvoice.Checked == true)
            {
                SaveClaimDetail();
            }
        }

    
    }
}