﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using EMR_BAL;
using System.IO;

namespace Mediplus.EMR.VisitDetails
{
    public partial class LabResult : System.Web.UI.UserControl
    {
        #region Methods


        public string FileNo { set; get; }

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        public void BindLaboratoryResult()
        {
            string Criteria = " 1=1  AND LTRM_STATUS not in ('Cancelled', 'Completed') ";
            // if( FileNo !=""  && FileNo !=null )
            // {
            Criteria += " AND LTRM_PATIENT_ID='" + FileNo + "'";
            // }
            Criteria += " AND LTRM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();


            DS = objCom.GetLaboratoryResultGroupBy(Criteria);



            //var groupbyfilter = from dr in DS.Tables[0].AsEnumerable()
            //                    group dr by dr["TransNo"];

            //DataTable dt = DS.Tables[0].Clone();
            //foreach (var x in groupbyfilter)
            //{
            //    x.CopyToDataTable(dt, LoadOption.OverwriteChanges);
            //}
            //dt.AcceptChanges();

            if (DS.Tables[0].Rows.Count > 0)
            {
                gvLabResult.DataSource = DS;
                gvLabResult.DataBind();

                if (GlobalValues.FileDescription.ToUpper() == "SMCH")
                {
                    if (gvLabResult.Columns.Count > 3)
                    {
                        gvLabResult.Columns[3].Visible = false;
                    }
                }
            }
            else
            {
                gvLabResult.DataBind();
            }
        }


        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    // BindLaboratoryResult();

                }

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      ResultsView.Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void gvLabResult_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvLabResult.PageIndex = e.NewPageIndex;
                BindLaboratoryResult();


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      ResultsView.gvLabResult_PageIndexChanging");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void Edit_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

                Label lblLabTransNo = (Label)gvScanCard.Cells[0].FindControl("lblLabTransNo");
                Label lblReportType = (Label)gvScanCard.Cells[0].FindControl("lblReportType");

                string ReportName = "LabReport.rpt";

                if (lblReportType.Text.ToUpper() == "U")
                {
                    ReportName = "LabReportUsr.rpt";
                }
                else
                {
                    if (GlobalValues.FileDescription.ToUpper() == "MAMPILLY")
                    {

                        CommonBAL objCom = new CommonBAL();
                        DataSet DS = new DataSet();

                        string Criteria = "  AT_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "' and  AT_TRANSID='" + lblLabTransNo.Text + "'";
                        DS = objCom.fnGetFieldValue("AT_AUTHORISATION_REQUIRED", "HMS_AUTHORISATION", Criteria, " AT_TRANSID ");

                        if (DS.Tables[0].Rows.Count > 0)
                        {
                            if (Convert.ToString(DS.Tables[0].Rows[0]["AT_AUTHORISATION_REQUIRED"]).ToUpper() == "N")
                            {
                                ReportName = "LabReportAuthorized.rpt";
                            }
                            else
                            {
                                ReportName = "LabReportNotAuthorized.rpt";
                            }
                        }
                        else
                        {
                            ReportName = "LabReportNotAuthorized.rpt";
                        }
                    }
                    else
                    {
                        ReportName = "LabReport.rpt";
                    }
                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "LabResult", "LabResultReport('" + ReportName + "','" + lblLabTransNo.Text.Trim() + "','" + lblReportType.Text.Trim() + "');", true);
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      ResultsView.Edit_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }



    }
}