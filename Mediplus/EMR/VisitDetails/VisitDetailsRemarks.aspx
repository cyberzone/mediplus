﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VisitDetailsRemarks.aspx.cs" Inherits="EMR.VisitDetails.VisitDetailsRemarks1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
<link href="../Styles/Maincontrols.css" type="text/css" rel="stylesheet" />
  <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">


        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }

</script></head>
<body>
    <form id="form1" runat="server">
        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ToolkitScriptManager>
    <div>
    <table style="width:100%">
     <tr>
             <td style="float:right;">
                <asp:Button ID="btnSave" runat="server" CssClass="button orange small" Width="100px" Text="Save Record"   OnClick="btnSave_Click" />
            </td>
        </tr>
</table>
<fieldset class="fieldset">
    <legend class="lblCaption1" style="font-weight: bold;">Service Remarks</legend>
    <%--<table>


        <%
            DataSet DS = new DataSet();

            DS = GetSegmentMaster();
            foreach (DataRow DR in DS.Tables[0].Rows)
            {
                DataSet DS1 = new DataSet();
                string Criteria1 = "1=1 ";
                Criteria1 += " AND EST_TYPE='" + Convert.ToString(DR["ESM_TYPE"]) + "'";

                DS1 = SegmentTemplatesGet(Criteria1);

                foreach (DataRow DR1 in DS1.Tables[0].Rows)
                {
                    string strValue = "";
                    string Criteria2 = "1=1 ";
                    Criteria2 += " AND ESVD_TYPE='" + Convert.ToString(DR1["EST_TYPE"]) + "' AND ESVD_SUBTYPE='" + Convert.ToString(DR1["EST_SUBTYPE"]) + "' ";
                    Criteria2 += " AND ESVD_FIELDNAME='" + Convert.ToString(DR1["EST_FIELDNAME"]) + "'";

                    strValue = SegmentVisitDtlsGet(Criteria2);
        %>
        <tr class="remarks-fields">
            <td>
                <label class="lblCaption1" for="remarksinput-<%= Convert.ToString(DR1["EST_TYPE"]) %>-<%=  Convert.ToString(DR1["EST_SUBTYPE"]) %>-<%= Convert.ToString(DR1["EST_FIELDNAME"]) %>"><%= Convert.ToString(DR1["EST_FIELDNAME_ALIAS"]) %></label></td>
            <td>
                <textarea class="remarks-input" style="width: 100%; height: 50px;" id="remarksinput-<%= Convert.ToString(DR1["EST_TYPE"]) %>-<%= Convert.ToString(DR1["EST_SUBTYPE"]) %>-<%= Convert.ToString(DR1["EST_FIELDNAME"]) %>" name="remarksinput-<%=  Convert.ToString(DR1["EST_SUBTYPE"]) %>-<%= Convert.ToString(DR1["EST_FIELDNAME"]) %>"><%=strValue %></textarea></td>
        </tr>
        <%      } // foreach ends for segment template
                   } // foreach ends for segment %>
    </table>--%>




    <table>
       
        <tr>
            <td colspan="2">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblValue" runat="server" Text="" Style="color: Red;"></asp:Label>
            </td>
           
        </tr>
    </table>
</fieldset>
    </div>
    </form>
</body>
</html>
