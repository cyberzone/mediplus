﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using EMR_BAL;

namespace Mediplus.EMR.VisitDetails
{
    public partial class NursingOrder1 : System.Web.UI.Page
    {
        public Boolean isNurse = false;
        CommonBAL objCom = new CommonBAL();


        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindNursingtInstruction()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND EPC_BRANCH_ID	 = '" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND EPC_ID = '" + Convert.ToString(Session["EMR_ID"]) + "'";


            DS = objCom.NursingOrderGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvNursingInsttruction.DataSource = DS;
                gvNursingInsttruction.DataBind();


                if (Convert.ToString(Session["EPM_STATUS"]).ToUpper() == "Y" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                {
                    gvNursingInsttruction.Columns[0].Visible = false;

                }
                //HIDE THE SAVE OPTION FOR NEXT DAY OF THE VISIT
                if (Convert.ToString(Session["EMRCurrentDateDiff"]) != "0" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                {
                    gvNursingInsttruction.Columns[0].Visible = false;
                }
                if (Convert.ToString(Session["EMR_VISIT_DTLS_EDIT_ALL_USERS"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "DOCTORS" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                {
                    gvNursingInsttruction.Columns[0].Visible = false;
                }
            }
            else
            {
                gvNursingInsttruction.DataBind();
            }


        }
        void BindNursingOrderHistory()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND EPC_BRANCH_ID	 = '" + Convert.ToString(Session["Branch_ID"]) + "'";
            //  Criteria += " AND EPC_ID = '" + Convert.ToString(Session["EMR_ID"]) + "'";

            Criteria += " AND EPC_ID in ( SELECT EPM_ID FROM EMR_PT_MASTER WHERE    EPM_PT_ID='" + Convert.ToString(Session["EMR_PT_ID"]) + "' )";
            DS = objCom.NursingOrderGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvNursingOrderHistory.DataSource = DS;
                gvNursingOrderHistory.DataBind();


            }
            else
            {
                gvNursingOrderHistory.DataBind();
            }


        }

        void BindTemplate()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1  AND ET_TYPE='NURSE_INSTRUCTION'";
            Criteria += " AND ET_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' ";

            Criteria += " AND ( ET_DR_CODE='" + Convert.ToString(Session["HPV_DR_ID"]) + "' OR ET_Apply=1 )";

            if (GlobalValues.FileDescription.ToUpper() != "MAMPILLY")
            {
                Criteria += " AND ET_DEP_ID='" + Convert.ToString(Session["HPV_DEP_NAME"]) + "'";
            }

            DS = objCom.TemplatesGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvTemplates.DataSource = DS;
                gvTemplates.DataBind();
            }
            else
            {
                gvTemplates.DataBind();
            }

            // objCom.BindMasterDropdown(Criteria, out drpDossage);

        }


        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    if (Convert.ToString(Session["EPM_STATUS"]).ToUpper() == "Y" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                    {
                        btnSave.Visible = false;
                    }
                    //HIDE THE SAVE OPTION FOR NEXT DAY OF THE VISIT
                    if (Convert.ToString(Session["EMRCurrentDateDiff"]) != "0" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                    {
                        btnSave.Visible = false;
                    }
                    objCom = new CommonBAL();
                    string strDate = "", strTime = "", strAM = ""; ;
                    strDate = objCom.fnGetDate("dd/MM/yyyy");
                    strTime = objCom.fnGetDate("hh:mm");
                    strAM = objCom.fnGetDate("tt");

                    txtFromDate.Text = strDate;
                    txtTreatmentDate.Text = strDate;
                    txtTreatmentTime.Text = strTime;
                    drpAM.SelectedValue = strAM;


                    BindNursingtInstruction();
                    BindNursingOrderHistory();
                    BindTemplate();
                    if (Convert.ToString(Session["User_Category"]).ToUpper() == "NURSE")
                    {
                        Instruction.Disabled = true;
                        txtFromDate.Enabled = false;
                        txtNextDays.Enabled = false;


                    }


                    if (GlobalValues.FileDescription == "ALANAMEL" && Convert.ToString(Session["User_Category"]).ToUpper() != "DOCTORS" && Convert.ToString(Session["User_Category"]).ToUpper() != "DOCTOR")
                    {

                        Instruction.Disabled = false;
                        txtFromDate.Enabled = true;
                        txtNextDays.Enabled = true;


                    }
                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      NursingOrder.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }

            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
                objCom.EMRID = Convert.ToString(Session["EMR_ID"]);
                objCom.Description = Instruction.Value.Replace("'", "''");
                objCom.Comments = Comments.Value.Replace("'", "''");
                objCom.EPC_DATE = txtFromDate.Text.Trim();
                objCom.NextDays = txtNextDays.Text.Trim();
                objCom.EPC_INSTRUCTION_ID = Convert.ToString(ViewState["InstructionID"]);

                if (GlobalValues.FileDescription == "ALANAMEL")
                {
                    objCom.NursingOrderAdd();

                    if (Convert.ToString(ViewState["InstructionID"]) != "")
                    {
                        objCom.UserID = Convert.ToString(Session["User_ID"]);
                        objCom.EPC_INSTRUCTION_ID = Convert.ToString(ViewState["InstructionID"]);
                        objCom.EPC_TIME = txtTreatmentDate.Text + " " + txtTreatmentTime.Text + " " + drpAM.SelectedValue;

                        objCom.NursingOrderUpdate();
                    }

                }
                else
                {

                    if (Convert.ToString(Session["User_Category"]).ToUpper() != "NURSE")
                    {
                        objCom.NursingOrderAdd();
                    }
                    else
                    {
                        objCom.UserID = Convert.ToString(Session["User_ID"]);
                        objCom.EPC_INSTRUCTION_ID = Convert.ToString(ViewState["InstructionID"]);
                        objCom.EPC_TIME = txtTreatmentDate.Text + " " + txtTreatmentTime.Text + " " + drpAM.SelectedValue;

                        objCom.NursingOrderUpdate();
                    }

                }
                BindNursingtInstruction();

                Instruction.Value = "";
                Comments.Value = "";
                txtFromDate.Text = "";
                ViewState["InstructionID"] = "";

                trNurseComment.Visible = false;
                trNurseTime.Visible = false;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      NursingOrder.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void Select_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToString(Session["User_Category"]).ToUpper() == "NURSE")
                {
                    trNurseComment.Visible = true;
                    trNurseTime.Visible = true;
                }
                LinkButton btnDel = new LinkButton();
                btnDel = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnDel.Parent.Parent;

                Label lblDate = (Label)gvScanCard.Cells[0].FindControl("lblDate");
                Label lblComment = (Label)gvScanCard.Cells[0].FindControl("lblComment");
                Label lblInsttruction = (Label)gvScanCard.Cells[0].FindControl("lblInsttruction");
                Label lblInstructionID = (Label)gvScanCard.Cells[0].FindControl("lblInstructionID");
                Label lblTime = (Label)gvScanCard.Cells[0].FindControl("lblTime");

                txtFromDate.Text = lblDate.Text.Trim();
                txtNextDays.Text = "0";
                Instruction.Value = lblInsttruction.Text;
                Comments.Value = lblComment.Text;

                if (lblTime.Text.Trim() != "")
                {
                    string[] strTime = lblTime.Text.Trim().Split(' ');

                    if (strTime.Length > 0)
                    {
                        txtTreatmentDate.Text = strTime[0];
                    }
                    if (strTime.Length > 1)
                    {
                        txtTreatmentTime.Text = strTime[1];
                    }
                    if (strTime.Length > 2)
                    {
                        for (int intCount = 0; intCount < drpAM.Items.Count; intCount++)
                        {
                            if (drpAM.Items[intCount].Value == strTime[2])
                            {
                                drpAM.SelectedValue = strTime[2];
                            }
                        }
                    }

                }

                ViewState["InstructionID"] = lblInstructionID.Text;


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      NursingOrder.DeleteeDiag_Click");
                TextFileWriting(ex.Message.ToString());
            }



        }

        protected void DeletegInst_Click(object sender, EventArgs e)
        {
            try
            {
                ImageButton btnDel = new ImageButton();
                btnDel = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnDel.Parent.Parent;


                Label lblInstructionID = (Label)gvScanCard.Cells[0].FindControl("lblInstructionID");

                objCom = new CommonBAL();
                string Criteria = " 1=1 ";
                Criteria += " AND EPC_BRANCH_ID	 = '" + Convert.ToString(Session["Branch_ID"]) + "'";
                Criteria += " AND EPC_ID = '" + Convert.ToString(Session["EMR_ID"]) + "'";
                Criteria += " AND EPC_INSTRUCTION_ID = '" + lblInstructionID.Text + "'";
                objCom.fnDeleteTableData("EMR_PT_INSTRUCTION", Criteria);
                BindNursingtInstruction();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      NursingOrder.DeletegInst_Click");
                TextFileWriting(ex.Message.ToString());
            }



        }


        protected void btnSaveTemp_Click(object sender, EventArgs e)
        {
            try
            {
                string TemplateCode = "";
                CommonBAL objCom = new CommonBAL();
                objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
                objCom.Type = "NURSE_INSTRUCTION";
                objCom.Description = txtTemplateName.Value;

                objCom.TemplateData = Instruction.Value;

                objCom.AllDr = "0";

                objCom.DR_ID = Convert.ToString(Session["HPV_DR_ID"]);
                objCom.DEP_ID = Convert.ToString(Session["HPV_DEP_NAME"]);
                TemplateCode = objCom.TemplatesAdd();

                BindTemplate();


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.btnSaveTemp_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void TempSelect_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btnDel = new LinkButton();
                btnDel = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnDel.Parent.Parent;

                Label lblTempCode = (Label)gvScanCard.Cells[0].FindControl("lblTempCode");
                Label lblTempData = (Label)gvScanCard.Cells[0].FindControl("lblTempData");

                Instruction.Value = lblTempData.Text;



            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Others.TempSelect_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void gvDeleteTemp1_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }

            try
            {
                ImageButton btnDel = new ImageButton();
                btnDel = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnDel.Parent.Parent;

                Label lblTempCode = (Label)gvScanCard.Cells[0].FindControl("lblTempCode");
                Label lblTempData = (Label)gvScanCard.Cells[0].FindControl("lblTempData");


                CommonBAL objCom = new CommonBAL();

                string Criteria = " 1=1  and ET_TYPE='NURSE_INSTRUCTION'";
                Criteria += " AND ET_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' ";
                Criteria += " AND ET_CODE='" + lblTempCode.Text + "'";

                objCom.fnDeleteTableData("EMR_TEMPLATES", Criteria);


                BindTemplate();


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Others.gvDeleteTemp1_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;

        }

    }
}