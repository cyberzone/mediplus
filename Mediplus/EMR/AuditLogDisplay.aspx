﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AuditLogDisplay.aspx.cs" Inherits="Mediplus.EMR.AuditLogDisplay" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Aidit Log</title>
     <script src="Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <link href="Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="Styles/style.css" rel="Stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
       <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>
            <table>
                <tr>
                    <td class="PageHeader">Audit Log
                    </td>
                </tr>
            </table>
           <div style="padding-top: 0px; width: 98%; height: 375px; overflow: auto; border: thin; border-color: #CCCCCC; border-style: groove;">
                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                    <ContentTemplate>
                        <asp:GridView ID="gvAuditLog" runat="server"  
                            AllowSorting="True" AutoGenerateColumns="False" OnSorting="gvPTList_Sorting"
                            EnableModelValidation="True" Width="100%"   GridLines="None">
                            <HeaderStyle CssClass="GridHeader_Blue" />
                            <RowStyle CssClass="GridRow" />
                            <Columns>
                                  <asp:TemplateField HeaderText="Date" SortExpression="EAL_CREATED_DATE">
                                    <ItemTemplate>
                                       
                                            <asp:Label ID="lblCreatedDate" CssClass="label" runat="server" Text='<%# Bind("CreatedDate") %>'></asp:Label>
                                        
                                    </ItemTemplate>

                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="EMR ID" SortExpression="EAL_EMR_ID">
                                    <ItemTemplate>
                                         
                                            <asp:Label ID="lblemr_id" CssClass="label" runat="server" Text='<%# Bind("EAL_EMR_ID") %>'></asp:Label>
                                     
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Screen" SortExpression="EAL_SCREEN_NAME">
                                    <ItemTemplate>
                                         
                                            <asp:Label ID="lblScreenName" CssClass="label" runat="server" Text='<%# Bind("EAL_SCREEN_NAME") %>'></asp:Label>
                                     
                                    </ItemTemplate>

                                </asp:TemplateField>
                              
                                <asp:TemplateField HeaderText="Action" SortExpression="EAL_ACTION">
                                    <ItemTemplate>
                                      
                                            <asp:Label ID="lblAction" CssClass="label" runat="server" Text='<%# Bind("EAL_ACTION") %>'></asp:Label>
                                    

                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Remarks" SortExpression="EAL_REMARKS">
                                    <ItemTemplate>
                                       
                                            <asp:Label ID="lblRemarks" CssClass="label" runat="server" Text='<%# Bind("EAL_REMARKS") %>'></asp:Label>
                                     
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="User" SortExpression="CreatedUserName">
                                    <ItemTemplate>
                                      
                                            <asp:Label ID="lblMobile" CssClass="label" runat="server" Text='<%# Bind("CreatedUserName") %>'></asp:Label>
                                        
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 
                                
                            </Columns>

                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>

            </div>
    </div>
    </form>
</body>
</html>
