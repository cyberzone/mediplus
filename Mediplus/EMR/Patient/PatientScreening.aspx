﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master"  AutoEventWireup="true" CodeBehind="PatientScreening.aspx.cs" Inherits=" Mediplus.EMR.Patient.PatientScreening" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/EMR/Patient/VitalSign.ascx" TagPrefix="UC1" TagName="Vital" %>
<%@ Register Src="~/EMR/Patient/PainScore.ascx" TagPrefix="UC2" TagName="PainScore" %>
<%@ Register Src="~/EMR/Patient/PainAssessment.ascx" TagPrefix="UC3" TagName="PainAssessment" %>
<%@ Register Src="~/EMR/Patient/PainReAssessment.ascx" TagPrefix="UC3" TagName="PainReAssessment" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

       <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />
<script src="../../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-1.4.1.js" type="text/javascript"></script>
        <link href="../Content/themes/base/jquery-ui.css" />

      
     <script src="../../Scripts/AccordionScript.js"></script>
     <link rel="stylesheet" href="../../Styles/Accordionstyles.css">


    <style type="text/css">
        .box-header
        {
            border-bottom: 4px solid #f4fbfd;
            height: 70px;
        }

        .box-title
        {
            padding-bottom: 5px;
            border-bottom: 4px solid #92c500;
            float: left;
            font-size: 1.2em;
            color: #2078c0;
        }

        h1, h2, h3, h4, h5, h6
        {
            font-size: 100%;
            font-weight: normal;
            font-family: "Segoe UI", Arial, Helvetica, sans-serif;
        }
    </style>

    <script type="text/javascript">
        function ShowMessage() {
            $("#myMessage").show();
            setTimeout(function () {
                var selectedEffect = 'blind';
                var options = {};
                $("#myMessage").hide();
            }, 2000);
            return true;
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div id="divAuditDtls" runat="server" visible="false" style="text-align: right; width: 80%;" class="lblCaption1">
        Creaded By :
                                  <asp:Label ID="lblCreatedBy" runat="server" CssClass="label"></asp:Label>
        &nbsp;-&nbsp;
                            <asp:Label ID="lblCreatedDate" runat="server" CssClass="label"></asp:Label>
        &nbsp;&nbsp;&nbsp;&nbsp;
        Modified By
                                 <asp:Label ID="lblModifiedBy" runat="server" CssClass="label"></asp:Label>
        &nbsp;-&nbsp;
                             <asp:Label ID="lblModifiedDate" runat="server" CssClass="label"></asp:Label>
    </div>


    <div style="padding-left: 60%; width: 100%;">
        <div id="myMessage" style="display: none; border: groove; height: 30px; width: 200px; background-color: gray; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #fff; padding: 20px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute;">
            Saved Successfully
        </div>
    </div>
 
   <table width="100%">
        <tr>
            <td style="text-align: left; width: 50%;">
                
                 <h3 class="box-title">
        <asp:Label ID="lblHeader" runat="server" Text="Patient Screening"></asp:Label>
    </h3>
            </td>
            <td style="text-align: right; width: 50%;" class="lblCaption1" >
                 <div id="divTemplate" runat="server" visible="false">
                Template
                   <asp:DropDownList ID="drpTemplate" CssClass="label" runat="server" Width="155px" BorderColor="#cccccc" AutoPostBack="true" OnSelectedIndexChanged="drpTemplate_SelectedIndexChanged">
                            </asp:DropDownList>

                            <asp:LinkButton ID="lnkSaveTemp" runat="server" CssClass="label orange" Text="Save" Enabled="false" Width="50px" 
                                Style="width:50px;border-radius:5px 5px 5px 5px; vertical-align: central;text-align:center;"></asp:LinkButton>
                            <asp:Button ID="btnDeleteTemp" runat="server" Text="Delete" OnClick="btnDeleteTemp_Click" style="width:50px;border-radius:5px 5px 5px 5px" CssClass="orange" />
</div>
            </td>
        </tr>
    </table>
            <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="AjaxTabStyle" Width="80%"  >
                <asp:TabPanel runat="server" ID="TabPanelGeneral" HeaderText="Vital Sign" Width="100%">
                    <ContentTemplate>
                        <UC1:Vital ID="Vital" runat="server"></UC1:Vital>
                    </ContentTemplate>
                </asp:TabPanel>
                <asp:TabPanel runat="server" ID="TabPanel1" HeaderText="Pain Score" Width="100%">
                    <ContentTemplate>
                        <UC2:PainScore ID="PainScore" runat="server"></UC2:PainScore>
                    </ContentTemplate>
                </asp:TabPanel>
                <asp:TabPanel runat="server" ID="tabPainAssess" HeaderText="Pain Assessment" Width="100%" Visible="false">
                    <ContentTemplate>
                        <UC3:PainAssessment ID="PainAssess" runat="server"></UC3:PainAssessment>
                    </ContentTemplate>
                </asp:TabPanel>

                <asp:TabPanel runat="server" ID="tabPainReAssess" HeaderText="Pain ReAssessment" Width="100%" Visible="false">
                    <ContentTemplate>
                        <UC3:PainReAssessment ID="PainReAssess" runat="server"></UC3:PainReAssessment>
                    </ContentTemplate>
                </asp:TabPanel>

            </asp:TabContainer>
   
    
    <asp:ModalPopupExtender ID="ModalPopupExtender5" runat="server" TargetControlID="lnkSaveTemp"
    PopupControlID="pnlSaveTemplate" BackgroundCssClass="modalBackground" CancelControlID="btnRemClose"
    PopupDragHandleControlID="pnlSaveTemplate">
</asp:ModalPopupExtender>
<asp:Panel ID="pnlSaveTemplate" runat="server" Height="250px" Width="550px" CssClass="modalPopup">
    <table cellpadding="0" cellspacing="0" width="100%" style="">
        <tr>
            <td align="right">
                <asp:Button ID="btnRemClose" runat="server" Text=" X " Font-Bold="true" CssClass="button blue small" />
            </td>
        </tr>
        <tr>
            <td class="lblCaption" style="font-weight: bold;">Save Template Dialog
            </td>
        </tr>

        <tr>
            <td class="lblCaption1" style="height: 100px; vertical-align: top;">
                <fieldset style="height: 50px;">
                    <legend class="lblCaption1" style="font-weight: bold;">Save Template</legend>
                    <table>
                        <tr>
                            <td>Template Name : 
                            </td>
                            <td>
                                <input type="text" id="txtTemplateName" name="txtTemplateName" runat="server" class="label" />
                            </td>
                        </tr>
                        <tr>
                            <td>Other Doctors :
                            </td>
                            <td>
                                <input type="checkbox" id="chkOtherDr" runat="server" title="Apply" />
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
        <tr>
            <td style="text-align: right;">
                <asp:Button ID="btnSaveTemp" runat="server" Text="Save Template" OnClick="btnSaveTemp_Click" CssClass="button blue small" />
                <asp:Button ID="btlCloseTemp" runat="server" Text="Close" CssClass="button blue small" />

            </td>
        </tr>

    </table>
</asp:Panel>
</asp:Content>
