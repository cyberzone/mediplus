﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_BAL;

namespace Mediplus.EMR.Patient
{
    public partial class ClinicalNotes : System.Web.UI.Page
    {
        public string getURL()
        {
            return Request.Url.GetLeftPart(UriPartial.Authority) + Page.ResolveUrl("~/");
        }
        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        public void BindEMR()
        {

            string Criteria = " 1=1  AND EPM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND EPM_ID = '" + Convert.ToString(Session["EMR_ID"]) + "'";    //BAL.GlobalValues.HomeCare_ID  + "' ";

            DataSet DS = new DataSet();
            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();
            DS = objEmrPTMast.GetEMR_PTMaster(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("EPM_CLINICAL_NOTES") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_CLINICAL_NOTES"]) != "")
                {
                    txtClinicalNotes.Text = Server.HtmlDecode(Convert.ToString(DS.Tables[0].Rows[0]["EPM_CLINICAL_NOTES"]));
                }
                else
                    txtClinicalNotes.Text = "";

            }

        }

        void BindClinicalNotes()
        {
            string Criteria = " 1=1  AND ECN_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND ECN_EMR_ID = '" + Convert.ToString(Session["EMR_ID"]) + "'";    //BAL.GlobalValues.HomeCare_ID  + "' ";

            DataSet DS = new DataSet();
            EMR_ClinicalNotes obj = new EMR_ClinicalNotes();
            DS = obj.EMRClinicalNotesGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                btnSave.Text = "Add";
                lblClinicalNotes1.Text = "";

                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    if (Convert.ToString(DR["ECN_ID"]) == "1")
                    {
                        if (DR.IsNull("ECN_CLINICAL_NOTES") == false && Convert.ToString(DR["ECN_CLINICAL_NOTES"]) != "")
                        {
                            lblClinicalNotes1.Text = Server.HtmlDecode(Convert.ToString(DR["ECN_CLINICAL_NOTES"]));
                            btnModify1.Visible = true;
                            div1.Visible = true;
                        }

                    }

                    if (Convert.ToString(DR["ECN_ID"]) == "2")
                    {
                        if (DR.IsNull("ECN_CLINICAL_NOTES") == false && Convert.ToString(DR["ECN_CLINICAL_NOTES"]) != "")
                        {
                            lblClinicalNotes2.Text = Server.HtmlDecode(Convert.ToString(DR["ECN_CLINICAL_NOTES"]));
                            btnModify2.Visible = true;
                            div2.Visible = true;
                        }

                    }


                    if (Convert.ToString(DR["ECN_ID"]) == "3")
                    {
                        if (DR.IsNull("ECN_CLINICAL_NOTES") == false && Convert.ToString(DR["ECN_CLINICAL_NOTES"]) != "")
                        {
                            lblClinicalNotes3.Text = Server.HtmlDecode(Convert.ToString(DR["ECN_CLINICAL_NOTES"]));
                            btnModify3.Visible = true;
                            div3.Visible = true;
                        }

                    }
                    if (Convert.ToString(DR["ECN_ID"]) == "4")
                    {
                        if (DR.IsNull("ECN_CLINICAL_NOTES") == false && Convert.ToString(DR["ECN_CLINICAL_NOTES"]) != "")
                        {
                            lblClinicalNotes4.Text = Server.HtmlDecode(Convert.ToString(DR["ECN_CLINICAL_NOTES"]));
                            btnModify4.Visible = true;
                            div4.Visible = true;
                        }

                    }

                    if (Convert.ToString(DR["ECN_ID"]) == "5")
                    {
                        if (DR.IsNull("ECN_CLINICAL_NOTES") == false && Convert.ToString(DR["ECN_CLINICAL_NOTES"]) != "")
                        {
                            lblClinicalNotes5.Text = Server.HtmlDecode(Convert.ToString(DR["ECN_CLINICAL_NOTES"]));
                            btnModify5.Visible = true;
                            div5.Visible = true;
                        }

                    }


                }


            }
            else
            {
                ViewState["ECN_ID"] = "";
                btnModify.Visible = false;
            }
        }

        void BindTemplate()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1  and ET_TYPE='CLINICALNOTES'";
            Criteria += " AND ET_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' ";

            Criteria += " AND ( ET_DR_CODE='" + Convert.ToString(Session["HPV_DR_ID"]) + "' OR ET_Apply=1 )";

            if (GlobalValues.FileDescription.ToUpper() != "MAMPILLY")
            {
                Criteria += " AND ET_DEP_ID='" + Convert.ToString(Session["HPV_DEP_NAME"]) + "'";
            }

            DS = objCom.TemplatesGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvTemplates.DataSource = DS;
                gvTemplates.DataBind();
            }
            else
            {
                gvTemplates.DataBind();
            }

            // objCom.BindMasterDropdown(Criteria, out drpDossage);

        }
        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../../Default.aspx?NoSession=1"); }

            if (!IsPostBack)
            {
                if (Convert.ToString(Session["EPM_STATUS"]).ToUpper() == "Y" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                {
                    btnSave.Visible = false;
                }
                //HIDE THE SAVE OPTION FOR NEXT DAY OF THE VISIT
                if (Convert.ToString(Session["EMRCurrentDateDiff"]) != "0" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                {
                    btnSave.Visible = false;
                }
                ViewState["ECN_ID"] = "";
                BindTemplate();
                //BindEMR();
                BindClinicalNotes();
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

            try
            {

                string tempStr = "";
                tempStr = Server.HtmlEncode(txtClinicalNotes.Text);

                /*
                string Criteria = " 1=1  AND EPM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
                Criteria += " AND EPM_ID = '" + Convert.ToString(Session["EMR_ID"]) + "'";


                string FieldNameWithValues = " EPM_CLINICAL_NOTES='" + tempStr + "' ";

                CommonBAL objCom = new CommonBAL();
                objCom.fnUpdateTableData(FieldNameWithValues, "EMR_PT_MASTER", Criteria);
                */


                EMR_ClinicalNotes obj = new EMR_ClinicalNotes();
                obj.ECN_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                obj.ECN_EMR_ID = Convert.ToString(Session["EMR_ID"]);
                obj.ECN_ID = "0";
                obj.ECN_PT_ID = Convert.ToString(Session["EMR_PT_ID"]);
                obj.ECN_CLINICAL_NOTES = tempStr;
                obj.UserID = Convert.ToString(Session["User_ID"]);
                obj.Mode = "A";
                obj.EMRClinicalNotesAdd();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      CriticalNotes.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }

            Response.Redirect("ClinicalNotes.aspx");


        }

        protected void btnModify_Click(object sender, EventArgs e)
        {

            try
            {

                string tempStr = "";
                tempStr = Server.HtmlEncode(txtClinicalNotes.Text);

                /*
                string Criteria = " 1=1  AND EPM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
                Criteria += " AND EPM_ID = '" + Convert.ToString(Session["EMR_ID"]) + "'";


                string FieldNameWithValues = " EPM_CLINICAL_NOTES='" + tempStr + "' ";

                CommonBAL objCom = new CommonBAL();
                objCom.fnUpdateTableData(FieldNameWithValues, "EMR_PT_MASTER", Criteria);
                */


                EMR_ClinicalNotes obj = new EMR_ClinicalNotes();
                obj.ECN_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                obj.ECN_EMR_ID = Convert.ToString(Session["EMR_ID"]);

                obj.ECN_ID = Convert.ToString(ViewState["ECN_ID"]);

                obj.ECN_PT_ID = Convert.ToString(Session["EMR_PT_ID"]);
                obj.ECN_CLINICAL_NOTES = tempStr;
                obj.UserID = Convert.ToString(Session["User_ID"]);
                obj.Mode = "M";
                obj.EMRClinicalNotesAdd();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      CriticalNotes.btnModify_Click");
                TextFileWriting(ex.Message.ToString());
            }

            Response.Redirect("ClinicalNotes.aspx");


        }


        protected void btnModify1_Click(object sender, EventArgs e)
        {

            try
            {

                txtClinicalNotes.Text = lblClinicalNotes1.Text;
                ViewState["ECN_ID"] = "1";

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      CriticalNotes.btnModify1_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }


        protected void btnModify2_Click(object sender, EventArgs e)
        {

            try
            {

                txtClinicalNotes.Text = lblClinicalNotes2.Text;
                ViewState["ECN_ID"] = "2";

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      CriticalNotes.btnModify2_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }


        protected void btnModify3_Click(object sender, EventArgs e)
        {

            try
            {

                txtClinicalNotes.Text = lblClinicalNotes3.Text;
                ViewState["ECN_ID"] = "3";

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      CriticalNotes.btnModify3_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }


        protected void btnModify4_Click(object sender, EventArgs e)
        {

            try
            {

                txtClinicalNotes.Text = lblClinicalNotes4.Text;
                ViewState["ECN_ID"] = "4";

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      CriticalNotes.btnModify4_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }

        protected void btnModify5_Click(object sender, EventArgs e)
        {

            try
            {

                txtClinicalNotes.Text = lblClinicalNotes5.Text;
                ViewState["ECN_ID"] = "5";

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      CriticalNotes.btnModify4_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }



        protected void btnSaveTemp_Click(object sender, EventArgs e)
        {
            try
            {
                string TemplateCode = "";
                CommonBAL objCom = new CommonBAL();
                objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
                objCom.Type = hidCreateTempType.Value;// "TREATMENTPLAN";
                objCom.Description = txtTemplateName.Value;

                objCom.TemplateData = Server.HtmlEncode(txtClinicalNotes.Text);

                objCom.AllDr = "0";

                objCom.DR_ID = Convert.ToString(Session["HPV_DR_ID"]);
                objCom.DEP_ID = Convert.ToString(Session["HPV_DEP_NAME"]);
                TemplateCode = objCom.TemplatesAdd();

                BindTemplate();


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.btnSaveTemp_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void TempSelect_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btnDel = new LinkButton();
                btnDel = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnDel.Parent.Parent;

                Label lblTempCode = (Label)gvScanCard.Cells[0].FindControl("lblTempCode");
                Label lblTempData = (Label)gvScanCard.Cells[0].FindControl("lblTempData");
                // txtTreatmentPlan.Text = lblTempData.Text;

                txtClinicalNotes.Text = Server.HtmlDecode(lblTempData.Text);




            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Others.TempSelect_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        #endregion

        protected void btnPasteNotes_Click(object sender, EventArgs e)
        {
            txtClinicalNotes.Text = Server.HtmlDecode(Convert.ToString(Session["CopyClinicalNotes"]));
        }


    }
}