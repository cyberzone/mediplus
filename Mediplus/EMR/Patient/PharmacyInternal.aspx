﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="PharmacyInternal.aspx.cs" Inherits=" Mediplus.EMR.Patient.PharmacyInternal" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

     <link href="../../Styles/Style.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Datagrid.css" rel="stylesheet" type="text/css" />

     
     
       
     <script src="../../Scripts/AccordionScript.js"></script>
     <link rel="stylesheet" href="../../Styles/Accordionstyles.css">
    <style type="text/css">
        .box-header
        {
            border-bottom: 4px solid #f4fbfd;
            height: 70px;
        }

        .box-title
        {
            padding-bottom: 5px;
            border-bottom: 4px solid #92c500;
            float: left;
            font-size: 1.1em;
            color: #2078c0;
        }

        h1, h2, h3, h4, h5, h6
        {
            font-size: 100%;
            font-weight: normal;
            font-family: "Segoe UI", Arial, Helvetica, sans-serif;
        }
    </style>

    <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divwidth
        {
            width: 400px !important;
        }

            #divwidth div
            {
                width: 400px !important;
            }


        #divDiagExt
        {
            width: 400px !important;
        }

            #divDiagExt div
            {
                width: 400px !important;
            }

        #divComp
        {
            width: 400px !important;
        }

            #divComp div
            {
                width: 400px !important;
            }


        #divDr
        {
            width: 400px !important;
        }

            #divDr div
            {
                width: 400px !important;
            }

        #divOrdClin
        {
            width: 400px !important;
        }

            #divOrdClin div
            {
                width: 400px !important;
            }
    </style>


    <script language="javascript" type="text/javascript">

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }


        function ServNameSelected() {
            if (document.getElementById('<%=txtServName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtServName.ClientID%>').value;
                var Data1 = Data.split('~');

                if (Data1.length > 1) {
                    document.getElementById('<%=txtServCode.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtServName.ClientID%>').value = Data1[1];
                }
            }

            return true;
        }

        function ShowAuditLog() {

            var win = window.open('../AuditLogDisplay.aspx?ScreenID=OP_PHY_INTERNAL&DataDisplay=ByEMR_ID', 'AuditLogDisplay', 'menubar=no,left=100,top=80,height=550,width=1075,scrollbars=1')

        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <table cellpadding="0" cellspacing="0" width="80%">
        <tr>
            <td style="width: 90%; text-align: right;">
                <div id="divAuditDtls" runat="server" visible="false" style="text-align: right; width: 100%;" class="lblCaption1">
                    Creaded By :
                                                  <asp:Label ID="lblCreatedBy" runat="server" CssClass="label"></asp:Label>
                    &nbsp;-&nbsp;
                                            <asp:Label ID="lblCreatedDate" runat="server" CssClass="label"></asp:Label>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                        Modified By
                                                 <asp:Label ID="lblModifiedBy" runat="server" CssClass="label"></asp:Label>
                    &nbsp;-&nbsp;
                                             <asp:Label ID="lblModifiedDate" runat="server" CssClass="label"></asp:Label>
                    &nbsp;&nbsp;
                </div>
            </td>
            <td style="width: 10%; text-align: right;">

                <a href="javascript:ShowAuditLog();" style="text-decoration: none;" class="lblCaption">Audit Log </a>


            </td>
        </tr>
    </table>
    <div style="padding-left: 60%; width: 99%;">
        <div id="myMessage" style="display: none; border: groove; height: 30px; width: 200px; background-color: gray; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #fff; padding: 20px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute;">
            Saved Successfully
        </div>
    </div>

    <h3 class="box-title">
        <asp:Label ID="lblHeader" runat="server" Text="Pharmacy Internal "></asp:Label>
    </h3>
    <br />
    <br />
    <br />
    <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="850px">
        <tr>
            <td>
                <asp:UpdatePanel ID="updatePanel17" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>

    </table>
    <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="AjaxTabStyle" Width="99%">
        <asp:TabPanel runat="server" ID="TabPanel1" HeaderText="Medication" Width="100%">
            <ContentTemplate>
                <table cellpadding="5" cellspacing="5" border="0" width="100%">
                    <tr>
                        <td class="lblCaption1" style="width: 100px;">Date & Time
                        </td>
                        <td>
                            <asp:UpdatePanel ID="updatePanel3" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtDate" runat="server" Width="70px" CssClass="TextBoxStyle" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                    <asp:TextBox ID="txtTime" runat="server" Width="50px" CssClass="TextBoxStyle" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                                    <asp:CalendarExtender ID="Calendarextender2" runat="server"
                                        Enabled="True" TargetControlID="txtDate" Format="dd/MM/yyyy">
                                    </asp:CalendarExtender>
                                    <asp:MaskedEditExtender ID="MaskedEditExtender3" runat="server" Enabled="true" TargetControlID="txtTime" Mask="99:99" MaskType="Time"></asp:MaskedEditExtender>
                                    <asp:DropDownList ID="drpAM" runat="server" CssClass="TextBoxStyle" Height="22px">
                                        <asp:ListItem Text="AM" Value="AM" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="PM" Value="PM"></asp:ListItem>
                                    </asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td class="lblCaption1" style="width: 100px;"></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="lblCaption1">Medication
                        </td>
                        <td colspan="3">
                            <asp:UpdatePanel runat="server" ID="updatePanel1">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtServCode" runat="server" Width="100px" CssClass="TextBoxStyle"   Enabled="false"></asp:TextBox>
                                    <asp:TextBox ID="txtServName" runat="server" Width="665px" CssClass="TextBoxStyle"   onblur="return ServNameSelected()"></asp:TextBox>

                                    <div id="divwidth" style="visibility: hidden;"></div>
                                    <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="Server" TargetControlID="txtServCode" MinimumPrefixLength="1" ServiceMethod="GetServicessList"
                                        CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionInterval="10" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidth">
                                    </asp:AutoCompleteExtender>
                                    <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="Server" TargetControlID="txtServName" MinimumPrefixLength="1" ServiceMethod="GetServicessList"
                                        CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionInterval="10" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidth">
                                    </asp:AutoCompleteExtender>
                                </ContentTemplate>
                            </asp:UpdatePanel>

                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1">Dosage
                        </td>
                        <td>
                            <asp:UpdatePanel runat="server" ID="updatePanel8">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtDossage1" runat="server" CssClass="TextBoxStyle" Width="150px" TextMode="MultiLine" Height="30px" Style="resize: none;"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>

                        </td>
                        <td class="lblCaption1">Route
                        </td>
                        <td>
                            <asp:UpdatePanel runat="server" ID="updatePanel11">
                                <ContentTemplate>
                                    <asp:DropDownList ID="drpRoute" CssClass="TextBoxStyle" runat="server" Width="200px" BorderColor="#cccccc">
                                    </asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1">Duration
                        </td>
                        <td>
                            <asp:UpdatePanel runat="server" ID="updatePanel7">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtDuration" runat="server" CssClass="TextBoxStyle" Width="55px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                    <span class="lblCaption1">Type</span>
                                    <asp:DropDownList ID="drpDurationType" CssClass="TextBoxStyle" runat="server" Width="60px" BorderColor="#cccccc">
                                        <asp:ListItem Text="Day(s)" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="Week(s)" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="Month(s)" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Year(s)" Value="2"></asp:ListItem>
                                    </asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td class="lblCaption1">frequency
                        </td>
                        <td colspan="3">
                            <asp:UpdatePanel runat="server" ID="updatePanel12">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtFreqyency" runat="server" Width="48px" Height="19px" Text="" CssClass="TextBoxStyle" MaxLength="4"   onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                    <asp:DropDownList ID="drpFreqType" CssClass="TextBoxStyle" runat="server" Width="100px" BorderColor="#cccccc">
                                        <asp:ListItem Text="Per Day" Value="04" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="Per Hour" Value="05"></asp:ListItem>
                                        <asp:ListItem Text="Per Week" Value="06"></asp:ListItem>
                                        <asp:ListItem Text="Stat" Value="07"></asp:ListItem>
                                        <asp:ListItem Text="" Value="8"></asp:ListItem>
                                    </asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                     
                    <tr>
                        <td class="lblCaption1">Remarks
                        </td>
                        <td colspan="3">
                            <asp:UpdatePanel runat="server" ID="updatePanel2">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtRemarks" runat="server" CssClass="TextBoxStyle" Width="770px" TextMode="MultiLine" Height="50px" Style="resize: none;"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1" style="width: 100px;">Date & Time
                        </td>
                        <td>
                            <asp:UpdatePanel ID="updatePanel18" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtMedicGivenDate" runat="server" Width="70px" CssClass="TextBoxStyle" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                    <asp:TextBox ID="txtMedicGivenTime" runat="server" Width="50px" CssClass="TextBoxStyle" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                                    <asp:CalendarExtender ID="Calendarextender4" runat="server" Enabled="True" TargetControlID="txtMedicGivenDate" Format="dd/MM/yyyy">
                                    </asp:CalendarExtender>
                                    <asp:MaskedEditExtender ID="MaskedEditExtender4" runat="server" Enabled="true" TargetControlID="txtMedicGivenTime" Mask="99:99" MaskType="Time"></asp:MaskedEditExtender>
                                    <asp:DropDownList ID="drpMedicGivenAM" runat="server" CssClass="TextBoxStyle" Height="22px">
                                        <asp:ListItem Text="AM" Value="AM" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="PM" Value="PM"></asp:ListItem>
                                    </asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td class="lblCaption1" style="width: 100px;">Name  
                        </td>
                        <td>
                            <asp:UpdatePanel ID="updatePanel19" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList ID="drpMedicNurse" runat="server" CssClass="TextBoxStyle" Width="205px"></asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="3">
                            <asp:UpdatePanel runat="server" ID="updatePanel4">
                                <ContentTemplate>
                                    <asp:Button ID="btnAddPhar" runat="server" CssClass="red" Width="50px" Text="Add" OnClick="btnAddPhar_Click" Style="width: 50px; border-radius: 5px 5px 5px 5px" />

                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>

                    </tr>
                </table>

                <asp:UpdatePanel runat="server" ID="updatePanel5">
                    <ContentTemplate>
                        <asp:GridView ID="gvPharmacy" runat="server" AllowSorting="True" AutoGenerateColumns="False" AllowPaging="true" PageSize="30"
                            EnableModelValidation="True">
                            <HeaderStyle CssClass="GridHeader_Blue" BorderStyle="Solid" BorderWidth="1px" BorderColor="#cccccc" />
                            <RowStyle CssClass="GridRow" />
                            <AlternatingRowStyle CssClass="GridAlterRow" />

                            <Columns>
                                <asp:TemplateField HeaderText="" HeaderStyle-Width="5%">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="DeleteeDiag" runat="server" ToolTip="Delete" ImageUrl="~/Images/icon_delete.jpg" Height="18" Width="18"
                                            OnClick="DeleteeDiag_Click" />&nbsp;&nbsp;
                                                
                                    </ItemTemplate>

                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Date & Time" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="120px">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkPhyDate" runat="server" OnClick="PhySelect_Click">
                                            <asp:Label ID="lblPhyDate" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPPI_DATEDesc") %>'></asp:Label>&nbsp
                                            <asp:Label ID="lblPhyTime" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPPI_DATETimeDesc") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="Code" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkPhyCode" runat="server" OnClick="PhySelect_Click">
                                            <asp:Label ID="lblPhyCode" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPPI_PHY_CODE") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Description" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkPhyDesc" runat="server" OnClick="PhySelect_Click">
                                            <asp:Label ID="lblPhyDesc" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPPI_PHY_NAME") %>'></asp:Label>

                                        </asp:LinkButton>

                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Dosage" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkDosage" runat="server" OnClick="PhySelect_Click">
                                            <asp:Label ID="lblPhyDosage" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPPI_DOSAGE") %>'></asp:Label>


                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Route" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkPhyRoute" runat="server" OnClick="PhySelect_Click">
                                            <asp:Label ID="lblPhyRoute" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPPI_ROUTE") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblPhyRouteDesc" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPPI_ROUTEDesc") %>'></asp:Label>

                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Frequency" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkFrequency" runat="server" OnClick="PhySelect_Click">
                                            <asp:Label ID="lblPhyFrequency" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPPI_FREQUENCY") %>' Visible="false"></asp:Label>&nbsp;
                                                            <asp:Label ID="lblPhyFrequencyTypeDesc" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPPI_FREQUENCYTYPEDesc") %>'></asp:Label>
                                            <asp:Label ID="lblPhyFrequencyType" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPPI_FREQUENCYTYPE") %>' Visible="false"></asp:Label>

                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Duration" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkPhyDuration" runat="server" OnClick="PhySelect_Click">
                                            <asp:Label ID="lblPhyDuration" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPPI_DURATION") %>'></asp:Label>&nbsp;
                                                    <asp:Label ID="lblPhyDurationTypeDesc" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPPI_DURATION_TYPEDesc") %>'></asp:Label>
                                            <asp:Label ID="lblPhyDurationType" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPPI_DURATION_TYPE") %>' Visible="false"></asp:Label>

                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                  
                                <asp:TemplateField HeaderText="Remarks" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkPhyRemarks" runat="server" OnClick="PhySelect_Click">
                                            <asp:Label ID="lblPhyRemarks" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPPI_REMARKS") %>'></asp:Label>&nbsp;
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Given Time" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkPhyGivenDate" runat="server" OnClick="PhySelect_Click">
                                            <asp:Label ID="lblPhyGivenDate" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPPI_GIVEN_DATEDesc") %>'></asp:Label>&nbsp;
                                            <asp:Label ID="lblPhyGivenTime" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPPI_GIVEN_DATETimeDesc") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Given By" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="120px">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkPhyGivenBy" runat="server" OnClick="PhySelect_Click">
                                            <asp:Label ID="lblPhyGivenBy" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPPI_GIVENBY") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblPhyGivenByName" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPPI_GIVENBY_NAME") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="Modified Date" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="120px" Visible="false" >
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkPhyModifiedDate" runat="server" OnClick="PhySelect_Click">
                                            <asp:Label ID="lblPhyModifiedDate" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("ModifiedDate") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="User" HeaderStyle-HorizontalAlign="Left" Visible="false" >
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkPhyUser" runat="server" OnClick="PhySelect_Click">
                                            <asp:Label ID="lblPhyUser" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("ModifiedUserName") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <br />
                <br />
                <span class="lblCaption1">History</span><br />
                <div style="padding-top: 0px; width: 100%; height: 300px; overflow: auto; border: thin; border-color: #f6f6f6; border-style: groove;">
                     <asp:UpdatePanel runat="server" ID="updatePanel21">
                    <ContentTemplate>
                    <asp:GridView ID="gvPharmacyHistory" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        EnableModelValidation="True" GridLines="None">
                        <HeaderStyle CssClass="GridHeader" BorderStyle="Solid" BorderWidth="1px" BorderColor="#cccccc" />
                        <RowStyle CssClass="GridRow" />
                        <AlternatingRowStyle CssClass="GridAlterRow" />

                        <Columns>
                            <asp:TemplateField HeaderText="Date & Time" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="120px">
                                <ItemTemplate>
                                    <asp:Label ID="lblHistPhyDate" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPPI_DATEDesc") %>'></asp:Label>
                                    <asp:Label ID="lblHistPhyTime" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPPI_DATETimeDesc") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Code" HeaderStyle-HorizontalAlign="Left">
                                <ItemTemplate>

                                    <asp:Label ID="lblHisPhyCode" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPPI_PHY_CODE") %>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Description" HeaderStyle-HorizontalAlign="Left">
                                <ItemTemplate>

                                    <asp:Label ID="lblHisPhyDesc" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPPI_PHY_NAME") %>'></asp:Label>


                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Dosage" HeaderStyle-HorizontalAlign="Left">
                                <ItemTemplate>

                                    <asp:Label ID="lblHisPhyDosage" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPPI_DOSAGE") %>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Route" HeaderStyle-HorizontalAlign="Left">
                                <ItemTemplate>

                                    <asp:Label ID="lblHisPhyRoute" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPPI_ROUTE") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblHisPhyRouteDesc" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPPI_ROUTEDesc") %>'></asp:Label>


                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Frequency" HeaderStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:Label ID="lblHisPhyFrequencyTypeDesc" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPPI_FREQUENCYTYPEDesc") %>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Duration" HeaderStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:Label ID="llblHisPhyDurationTypeDesc" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPPI_DURATION_TYPEDesc") %>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Remarks" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                       
                                            <asp:Label ID="lblHisPhyRemarks" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPPI_REMARKS") %>'></asp:Label>&nbsp;
                                        
                                    </ItemTemplate>
                                </asp:TemplateField>
                             <asp:TemplateField HeaderText="Given Time" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        
                                            <asp:Label ID="lblHisPhyGivenDate" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPPI_GIVEN_DATEDesc") %>'></asp:Label>&nbsp;
                                            <asp:Label ID="lblHisPhyGivenTime" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPPI_GIVEN_DATETimeDesc") %>'></asp:Label>
                                         
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Given By" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="120px">
                                    <ItemTemplate>
                                            <asp:Label ID="lblHisPhyGivenByName" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPPI_GIVENBY_NAME") %>'></asp:Label>
                                        
                                    </ItemTemplate>
                                </asp:TemplateField>
                            <asp:TemplateField HeaderText="Date" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="120px" Visible="false">
                                <ItemTemplate>

                                    <asp:Label ID="lblHisPhyDate" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("ModifiedDate") %>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="User" HeaderStyle-HorizontalAlign="Left" Visible="false">
                                <ItemTemplate>

                                    <asp:Label ID="lblUser" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("ModifiedUserName") %>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>
                           </ContentTemplate>
                </asp:UpdatePanel>
                </div>

            </ContentTemplate>
        </asp:TabPanel>

        <asp:TabPanel runat="server" ID="TabPanel3" HeaderText="Non Medication" Width="100%">
            <ContentTemplate>
                <table cellpadding="5" cellspacing="5" border="0" width="100%">
                    <tr>
                        <td class="lblCaption1" style="width: 100px;">Date & Time
                        </td>
                        <td>
                            <asp:UpdatePanel ID="updatePanel13" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtNonMedicDate" runat="server" Width="70px" CssClass="TextBoxStyle" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                    <asp:TextBox ID="txtNonMedicTime" runat="server" Width="50px" CssClass="TextBoxStyle" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                                    <asp:CalendarExtender ID="Calendarextender1" runat="server" Enabled="True" TargetControlID="txtNonMedicDate" Format="dd/MM/yyyy">
                                    </asp:CalendarExtender>
                                    <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" Enabled="true" TargetControlID="txtNonMedicTime" Mask="99:99" MaskType="Time"></asp:MaskedEditExtender>
                                    <asp:DropDownList ID="drpNonMedicAM" runat="server" CssClass="TextBoxStyle" Height="22px">
                                        <asp:ListItem Text="AM" Value="AM" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="PM" Value="PM"></asp:ListItem>
                                    </asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td class="lblCaption1" style="width: 100px;"></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="lblCaption1" style="width: 100px; vertical-align: top;">Description
                        </td>
                        <td colspan="3">
                            <asp:UpdatePanel runat="server" ID="updatePanel6">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtNonMedication" runat="server" CssClass="TextBoxStyle" Width="770px" TextMode="MultiLine" Height="70px" Style="resize: none;"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1" style="width: 100px; vertical-align: top;">Remarks
                        </td>
                        <td colspan="3">
                            <asp:UpdatePanel runat="server" ID="updatePanel14">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtNonMedicRemarks" runat="server" CssClass="TextBoxStyle" Width="770px" TextMode="MultiLine" Height="50px" Style="resize: none;"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1" style="width: 100px;">Date & Time
                        </td>
                        <td>
                            <asp:UpdatePanel ID="updatePanel15" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtNonMedicGivenDate" runat="server" Width="70px" CssClass="TextBoxStyle" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                    <asp:TextBox ID="txtNonMedicGivenTime" runat="server" Width="50px" CssClass="TextBoxStyle" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                                    <asp:CalendarExtender ID="Calendarextender3" runat="server" Enabled="True" TargetControlID="txtNonMedicGivenDate" Format="dd/MM/yyyy">
                                    </asp:CalendarExtender>
                                    <asp:MaskedEditExtender ID="MaskedEditExtender2" runat="server" Enabled="true" TargetControlID="txtNonMedicGivenTime" Mask="99:99" MaskType="Time"></asp:MaskedEditExtender>
                                    <asp:DropDownList ID="drpNonMedicGivenAM" runat="server" CssClass="TextBoxStyle" Height="22px">
                                        <asp:ListItem Text="AM" Value="AM" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="PM" Value="PM"></asp:ListItem>
                                    </asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td class="lblCaption1" style="width: 100px;">Name  
                        </td>
                        <td>
                            <asp:UpdatePanel ID="updatePanel20" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList ID="drpNonMedicNurse" runat="server" CssClass="TextBoxStyle" Width="205px"></asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <asp:UpdatePanel runat="server" ID="updatePanel9">
                                <ContentTemplate>
                                    <asp:Button ID="btnNonMedicSave" runat="server" CssClass="red" Width="70px" Text="Add" OnClick="btnNonMedicSave_Click" Style="width: 50px; border-radius: 5px 5px 5px 5px" />

                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>

                    </tr>
                </table>
                <asp:UpdatePanel runat="server" ID="updatePanel16">
                    <ContentTemplate>
                        <asp:GridView ID="gvNonMedic" runat="server" AllowSorting="True" AutoGenerateColumns="False" AllowPaging="true" PageSize="30"
                            EnableModelValidation="True">
                            <HeaderStyle CssClass="GridHeader_Blue" BorderStyle="Solid" BorderWidth="1px" BorderColor="#cccccc" />
                            <RowStyle CssClass="GridRow" />
                            <AlternatingRowStyle CssClass="GridAlterRow" />

                            <Columns>
                                <asp:TemplateField HeaderText="" HeaderStyle-Width="5%">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="DeleteNonPhy" runat="server" ToolTip="Delete" ImageUrl="~/Images/icon_delete.jpg" Height="18" Width="18"
                                            OnClick="DeleteNonPhy_Click" />&nbsp;&nbsp;
                                                
                                    </ItemTemplate>

                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Date & Time" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="120px">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkNonPhyDate" runat="server" OnClick="NonPhySelect_Click">
                                            <asp:Label ID="lblNonPhyID" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPNPI_ID") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblNonPhyDate" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPNPI_DATEDesc") %>'></asp:Label>&nbsp
                                            <asp:Label ID="lblNonPhyTime" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPNPI_DATETimeDesc") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>



                                <asp:TemplateField HeaderText="Description" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkNonPhyDesc" runat="server" OnClick="NonPhySelect_Click">
                                            <asp:Label ID="lblNonPhyDesc" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPNPI_DESCRIPTION") %>'></asp:Label>

                                        </asp:LinkButton>

                                    </ItemTemplate>
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="Remarks" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkNonPhyRemarks" runat="server" OnClick="NonPhySelect_Click">
                                            <asp:Label ID="lblNonPhyRemarks" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPNPI_REMARKS") %>'></asp:Label>&nbsp;
                                                
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Given Time" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkNonPhyGivenDate" runat="server" OnClick="NonPhySelect_Click">
                                            <asp:Label ID="lblNonPhyGivenDate" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPNPI_GIVEN_DATEDesc") %>'></asp:Label>&nbsp;
                                            <asp:Label ID="lblNonPhyGivenTime" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPNPI_GIVEN_DATETimeDesc") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Given By" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="120px">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkNonPhyGivenBy" runat="server" OnClick="NonPhySelect_Click">
                                            <asp:Label ID="lblNonPhyGivenBy" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPNPI_GIVENBY") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblNonPhyGivenByName" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPNPI_GIVENBY_NAME") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </ContentTemplate>

                </asp:UpdatePanel>
                <br />
                <br />
                <span class="lblCaption1">History</span><br />
                <div style="padding-top: 0px; width: 100%; height: 300px; overflow: auto; border: thin; border-color: #f6f6f6; border-style: groove;">
                    <asp:UpdatePanel runat="server" ID="updatePanel10">
                        <ContentTemplate>
                            <asp:GridView ID="gvNonMedicHistory" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                EnableModelValidation="True" GridLines="None">
                                <HeaderStyle CssClass="GridHeader" BorderStyle="Solid" BorderWidth="1px" BorderColor="#cccccc" />
                                <RowStyle CssClass="GridRow" />
                                <AlternatingRowStyle CssClass="GridAlterRow" />

                                <Columns>
                                    <asp:TemplateField HeaderText="Date & Time" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="120px">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkHistNonPhyDate" runat="server" OnClick="PhySelect_Click">
                                                <asp:Label ID="lblHistNonPhyDate" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPNPI_DATEDesc") %>'></asp:Label>&nbsp
                                            <asp:Label ID="lblHistNonPhyTime" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPNPI_DATETimeDesc") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>



                                    <asp:TemplateField HeaderText="Description" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkHistNonPhyDesc" runat="server" OnClick="PhySelect_Click">
                                                <asp:Label ID="lblHistNonPhyDesc" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPNPI_DESCRIPTION") %>'></asp:Label>

                                            </asp:LinkButton>

                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Remarks" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkHistNonPhyRemarks" runat="server" OnClick="PhySelect_Click">
                                                <asp:Label ID="lblHistNonPhyRemarks" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPNPI_REMARKS") %>'></asp:Label>&nbsp;
                                                
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Given Time" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkHistNonPhyGivenDate" runat="server" OnClick="PhySelect_Click">
                                                <asp:Label ID="lblHistNonPhyGivenDate" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPNPI_GIVEN_DATEDesc") %>'></asp:Label>&nbsp;
                                            <asp:Label ID="lblHistNonPhyGivenTime" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPNPI_GIVEN_DATETimeDesc") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Given By" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="120px">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkHistNonPhyGivenBy" runat="server" OnClick="PhySelect_Click">
                                                <asp:Label ID="lblHistNonPhyGivenBy" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EPNPI_GIVENBY_NAME") %>'></asp:Label>

                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </ContentTemplate>
        </asp:TabPanel>

    </asp:TabContainer>
    <br />
    <br />
</asp:Content>
