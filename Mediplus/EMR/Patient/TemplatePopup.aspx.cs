﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_BAL;



namespace Mediplus.EMR.Patient
{
    public partial class TemplatePopup : System.Web.UI.Page
    {
        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["TemplateType"] = Request.QueryString["TemplateType"];
                ViewState["TemplateData"] = Request.QueryString["TemplateData"];


            }
        }

        protected void btnSaveTemp_Click(object sender, EventArgs e)
        {
            try
            {
                
                string TemplateCode = "";
                CommonBAL objCom = new CommonBAL();
                objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
                objCom.Type = Convert.ToString(ViewState["TemplateType"]);// "TREATMENTPLAN";
                objCom.Description = txtTemplateName.Value;
                objCom.TemplateData =Convert.ToString(ViewState["TemplateData"]);

                if (chkOtherDr.Checked == true)
                {
                    objCom.AllDr = "1";
                }
                else
                {
                    objCom.AllDr = "0";
                }

                objCom.DR_ID = Convert.ToString(Session["HPV_DR_ID"]);
                objCom.DEP_ID = Convert.ToString(Session["HPV_DEP_NAME"]);
                TemplateCode = objCom.TemplatesAdd();

               


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      TemplatePopup.btnSaveTemp_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        
    }
}