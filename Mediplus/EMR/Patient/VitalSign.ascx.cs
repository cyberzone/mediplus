﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using EMR_BAL;
using System.IO;

namespace Mediplus.EMR.Patient
{
    public partial class VitalSign : System.Web.UI.UserControl
    {

        public String Gender = "MALE";
        EMR_PTVitalsBAL objVit = new EMR_PTVitalsBAL();
        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        public void BindVital()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND EPV_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND EPV_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
            DataSet DS = new DataSet();
            objVit = new EMR_PTVitalsBAL();
            DS = objVit.EMRPTVitalsGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                ViewState["EPV_SL_NO"] = Convert.ToString(DS.Tables[0].Rows[0]["EPV_SL_NO"]);
                txtWeight.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPV_WEIGHT"]);
                txtHeight.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPV_HEIGHT"]);
                txtBMI.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPV_BMI"]);
                txtTemperatureF.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPV_TEMPERATURE"]);
                txtTemperatureC.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPV_TEMPERATURE_C"]);
                txtPulse.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPV_PULSE"]);
                txtRespiration.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPV_RESPIRATION"]);
                txtBpSystolic.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPV_BP_SYSTOLIC"]);
                txtBpDiastolic.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPV_BP_DIASTOLIC"]);


                txtAbdominalGirth.Value = Convert.ToString(DS.Tables[0].Rows[0]["EMR_PT_ABDOMIANL_GIRTH"]);
                txtHeadCircumference.Value = Convert.ToString(DS.Tables[0].Rows[0]["EMR_PT_HEADCIRC"]);
                txtChestCircumference.Value = Convert.ToString(DS.Tables[0].Rows[0]["EMR_PT_CHESTCIRC"]);
                txtSp02.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPV_SPO2"]);
                txtCapillaryBloodSugar.Value = Convert.ToString(DS.Tables[0].Rows[0]["EMR_PT_CAPILLARY_BLOOD_SUGAR"]);
                txtJaundiceMeter.Value = Convert.ToString(DS.Tables[0].Rows[0]["EMR_PT_JAUNDICE_METER"]);
                txtEcgReport.Value = Convert.ToString(DS.Tables[0].Rows[0]["EMR_PT_ECG_REPORT"]);
                txtOthers.Value = Convert.ToString(DS.Tables[0].Rows[0]["EMR_PT_OTHERS"]);

                txtLMPDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["EPV_LMP_DATE"]);
                txtEDDDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["EPV_EDD_DATE"]);

                if (Convert.ToString(DS.Tables[0].Rows[0]["EPV_IS_PREGNANT"]) == "1")
                {
                    chkIsPregnant.Checked = true;
                }



                if (DS.Tables[0].Rows[0].IsNull("EPV_IS_LACTATING") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPV_IS_LACTATING"]) != "")
                {
                    chkIsLactating.Checked = Convert.ToBoolean(DS.Tables[0].Rows[0]["EPV_IS_LACTATING"]);

                }




                txtHemoglobin.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPV_HEMOGLOBIN"]);
                txtBirthWeight.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPV_BIRTH_WEIGHT"]);


                if (txtTemperatureF.Value != "")
                {
                    //var temp = Convert.ToDecimal(txtTemperatureF.Value);
                    //var tempInF = Math.Round(((100 / (212 - 32)) * (temp - 32)) * 100) / 100;

                    //txtTemperatureC.Value = Convert.ToString(tempInF);
                    //   ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "FtoC('');", true);
                    // ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientScript", "FtoC();", true);
                }

            }

        }

        public void BindVitalGrid()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND EPV_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND EPV_PT_ID ='" + Convert.ToString(Session["EMR_PT_ID"]) + "'";

            // Criteria += " AND EPV_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
            DataSet DS = new DataSet();
            objVit = new EMR_PTVitalsBAL();
            DS = objVit.EMRPTVitalsGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvVital.DataSource = DS;
                gvVital.DataBind();
            }
            else
            {
                gvVital.DataBind();
            }
        }

        public string fnVitalSave()
        {

            objVit = new EMR_PTVitalsBAL();

            objVit.EPV_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
            objVit.EPV_ID = Convert.ToString(ViewState["EPV_ID"]);// Convert.ToString(Session["EMR_ID"]);
            objVit.EPV_WEIGHT = txtWeight.Value.Trim();
            objVit.EPV_HEIGHT = txtHeight.Value.Trim();
            objVit.EPV_TEMPERATURE = txtTemperatureF.Value.Trim();
            objVit.EPV_PULSE = txtPulse.Value.Trim();
            objVit.EPV_RESPIRATION = txtRespiration.Value.Trim();
            objVit.EPV_BP_SYSTOLIC = txtBpSystolic.Value.Trim();
            objVit.EPV_BP_DIASTOLIC = txtBpDiastolic.Value.Trim();
            // objVit.EPV_DATE", EPV_DATE);


            objVit.EPV_PT_ID = Convert.ToString(Session["EMR_PT_ID"]);
            objVit.EMR_PT_ABDOMIANL_GIRTH = txtAbdominalGirth.Value.Trim();
            objVit.EMR_PT_HEADCIRC = txtHeadCircumference.Value.Trim();
            objVit.EMR_PT_CHESTCIRC = txtChestCircumference.Value.Trim();
            objVit.EPV_SPO2 = txtSp02.Value.Trim();
            objVit.EMR_PT_CAPILLARY_BLOOD_SUGAR = txtCapillaryBloodSugar.Value.Trim();
            objVit.EMR_PT_JAUNDICE_METER = txtJaundiceMeter.Value.Trim();
            objVit.EMR_PT_ECG_REPORT = txtEcgReport.Value.Trim();
            objVit.EMR_PT_OTHERS = txtOthers.Value.Trim();
            objVit.EPV_TEMPLATE_CODE = "";

            objVit.EPV_LMP_DATE = txtLMPDate.Text.Trim();
            objVit.EPV_EDD_DATE = txtEDDDate.Text.Trim();

            if (chkIsPregnant.Checked == true)
            {
                objVit.EPV_IS_PREGNANT = "1";
            }
            else
            {
                objVit.EPV_IS_PREGNANT = "0";
            }

            if (chkIsLactating.Checked == true)
            {
                objVit.EPV_IS_LACTATING = "1";
            }
            else
            {
                objVit.EPV_IS_LACTATING = "0";
            }


            objVit.EPV_PT_WAISTCIRC = "";
            objVit.EPV_PT_HIPCIRC = "";
            objVit.EPV_PT_WAISTHIP_RATIO = "";
            objVit.EPV_HEMOGLOBIN = txtHemoglobin.Value.Trim();
            objVit.EPV_BIRTH_WEIGHT = txtBirthWeight.Value.Trim();
            objVit.EPV_SL_NO = Convert.ToString(ViewState["EPV_SL_NO"]);
            objVit.UserID = Convert.ToString(Session["User_ID"]);
            objVit.EMRPTVitalsAdd();
            BindVitalGrid();

            return "";


        }

        void ModifyVital()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND EPV_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND EPV_ID='" + Convert.ToString(ViewState["EPV_ID"]) + "'";
            Criteria += " AND EPV_SL_NO='" + ViewState["EPV_SL_NO"] + "'";

            DataSet DS = new DataSet();
            objVit = new EMR_PTVitalsBAL();
            DS = objVit.EMRPTVitalsGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {

                txtWeight.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPV_WEIGHT"]);
                txtHeight.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPV_HEIGHT"]);
                txtBMI.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPV_BMI"]);
                txtTemperatureF.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPV_TEMPERATURE"]);
                txtTemperatureC.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPV_TEMPERATURE_C"]);
                txtPulse.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPV_PULSE"]);
                txtRespiration.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPV_RESPIRATION"]);
                txtBpSystolic.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPV_BP_SYSTOLIC"]);
                txtBpDiastolic.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPV_BP_DIASTOLIC"]);


                txtAbdominalGirth.Value = Convert.ToString(DS.Tables[0].Rows[0]["EMR_PT_ABDOMIANL_GIRTH"]);
                txtHeadCircumference.Value = Convert.ToString(DS.Tables[0].Rows[0]["EMR_PT_HEADCIRC"]);
                txtChestCircumference.Value = Convert.ToString(DS.Tables[0].Rows[0]["EMR_PT_CHESTCIRC"]);
                txtSp02.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPV_SPO2"]);
                txtCapillaryBloodSugar.Value = Convert.ToString(DS.Tables[0].Rows[0]["EMR_PT_CAPILLARY_BLOOD_SUGAR"]);
                txtJaundiceMeter.Value = Convert.ToString(DS.Tables[0].Rows[0]["EMR_PT_JAUNDICE_METER"]);
                txtEcgReport.Value = Convert.ToString(DS.Tables[0].Rows[0]["EMR_PT_ECG_REPORT"]);
                txtOthers.Value = Convert.ToString(DS.Tables[0].Rows[0]["EMR_PT_OTHERS"]);

                txtLMPDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["EPV_LMP_DATE"]);
                txtEDDDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["EPV_EDD_DATE"]);

                if (Convert.ToString(DS.Tables[0].Rows[0]["EPV_IS_PREGNANT"]) == "1")
                {
                    chkIsPregnant.Checked = true;
                }

                txtHemoglobin.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPV_HEMOGLOBIN"]);
                txtBirthWeight.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPV_BIRTH_WEIGHT"]);


                if (txtTemperatureF.Value != "")
                {
                    //var temp = Convert.ToDecimal(txtTemperatureF.Value);
                    //var tempInF = Math.Round(((100 / (212 - 32)) * (temp - 32)) * 100) / 100;

                    //txtTemperatureC.Value = Convert.ToString(tempInF);
                    //   ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "FtoC('');", true);
                    // ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "clientScript", "FtoC();", true);
                }

            }

        }


        void BindTemplate()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1  and ET_TYPE='VitalSign'  ";
            Criteria += " AND ET_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' ";

            Criteria += " AND ( ET_DR_CODE='" + Convert.ToString(Session["HPV_DR_ID"]) + "' OR ET_Apply=1 )";

            if (GlobalValues.FileDescription.ToUpper() != "MAMPILLY")
            {
                Criteria += " AND ET_DEP_ID='" + Convert.ToString(Session["HPV_DEP_NAME"]) + "'";
            }
            drpTemplate.Items.Clear();

            DS = objCom.TemplatesGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                drpTemplate.DataSource = DS;
                drpTemplate.DataTextField = "ET_NAME";
                drpTemplate.DataValueField = "ET_CODE";
                drpTemplate.DataBind();
            }
            drpTemplate.Items.Insert(0, "Select Template");
            drpTemplate.Items[0].Value = "";

            // objCom.BindMasterDropdown(Criteria, out drpDossage);

        }

        void Clear()
        {

            txtWeight.Value = "";
            txtHeight.Value = "";
            txtTemperatureF.Value = "";
            txtPulse.Value = "";
            txtRespiration.Value = "";
            txtBpSystolic.Value = "";
            txtBpDiastolic.Value = "";

            txtAbdominalGirth.Value = "";
            txtHeadCircumference.Value = "";
            txtChestCircumference.Value = "";
            txtSp02.Value = "";
            txtCapillaryBloodSugar.Value = "";
            txtJaundiceMeter.Value = "";
            txtEcgReport.Value = "";
            txtOthers.Value.Trim();

            chkIsPregnant.Checked = false;
            txtLMPDate.Text = "";
            txtEDDDate.Text = "";
            txtHemoglobin.Value = "";
            objVit.EPV_BIRTH_WEIGHT = "";

            txtBMI.Value = "";
            txtTemperatureC.Value = "";
            txtOthers.Value = "";

            ViewState["EPV_SL_NO"] = "";
            ViewState["EPV_ID"] = "";

        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                Gender = Convert.ToString(Session["HPM_SEX"]).ToUpper() != null ? Convert.ToString(Session["HPM_SEX"]).ToUpper() : "MALE";

                ViewState["EPV_ID"] = Convert.ToString(Session["EMR_ID"]);
                if (GlobalValues.FileDescription == "HOLISTIC")
                {



                    trCapillary.Visible = false;
                    trECG.Visible = false;
                    trJaundice.Visible = false;
                }

                if (Convert.ToString(Session["EMR_SAVE_VITAL_MULTIPLE"]) == "0")
                {
                    BindVital();
                    btnSave.Visible = true;
                }
                else
                {
                    btnSaveVital.Visible = true;
                }

                //if (GlobalValues.FileDescription.ToUpper() != "HOLISTIC")
                //{
                if (Convert.ToString(Session["EPM_STATUS"]).ToUpper() == "Y" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                {
                    btnSaveVital.Visible = false;
                    btnSave.Visible = false;
                }
                // }
                //HIDE THE SAVE OPTION FOR NEXT DAY OF THE VISIT
                if (Convert.ToString(Session["EMRCurrentDateDiff"]) != "0" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                {
                    btnSaveVital.Visible = false;
                    btnSave.Visible = false;

                }
                BindTemplate();
                BindVitalGrid();

                if (GlobalValues.FileDescription == "MAMOON")
                {
                    divTempF.Visible = false;
                    trECG.Visible = false;

                }


            }
        }

        protected void btnSaveVital_Click(object sender, EventArgs e)
        {
            fnVitalSave();
            Clear();
            BindVital();
            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowMessage()", true);
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {

            fnVitalSave();

            BindVital();

            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowMessage()", true);
        }

        protected void Select_Click(object sender, EventArgs e)
        {
            LinkButton btnDel = new LinkButton();
            btnDel = (LinkButton)sender;

            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnDel.Parent.Parent;

            Label lblSLNo = (Label)gvScanCard.Cells[0].FindControl("lblSLNo");
            Label lblEPV_ID = (Label)gvScanCard.Cells[0].FindControl("lblEPV_ID");
            ViewState["EPV_SL_NO"] = lblSLNo.Text;
            ViewState["EPV_ID"] = lblEPV_ID.Text;

            ModifyVital();


        }


        protected void btnSaveTemp_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }

            try
            {
                string TemplateCode = "";
                CommonBAL objCom = new CommonBAL();
                objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
                objCom.Type = "VitalSign";
                objCom.Description = txtTemplateName.Value;
                objCom.TemplateData = "";
                if (chkOtherDr.Checked == true)
                {
                    objCom.AllDr = "1";
                }
                else
                {
                    objCom.AllDr = "0";
                }
                objCom.DR_ID = Convert.ToString(Session["HPV_DR_ID"]);
                objCom.DEP_ID = Convert.ToString(Session["HPV_DEP_NAME"]);
                TemplateCode = objCom.TemplatesAdd();


                EMR_PTVitalsBAL objPhar = new EMR_PTVitalsBAL();
                objPhar.EPV_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objPhar.EPV_ID = Convert.ToString(Session["EMR_ID"]);
                objPhar.TemplateCode = TemplateCode;
                objPhar.VitalsTemplateAdd();
                BindTemplate();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      VitalSign.btnSaveTemp_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;
        }

        protected void btnDeleteTemp_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }

            try
            {

                CommonBAL objCom = new CommonBAL();

                string Criteria = "EPV_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND EPV_TEMPLATE_CODE='" + drpTemplate.SelectedValue + "'";
                objCom.fnDeleteTableData("EMR_PT_VITALS_TEMPLATES", Criteria);



                string Criteria1 = "ET_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND ET_CODE='" + drpTemplate.SelectedValue + "' and ET_TYPE='VitalSign'";
                objCom.fnDeleteTableData("EMR_TEMPLATES", Criteria1);


                BindTemplate();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.btnDeleteTemp_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;
        }

        protected void drpTemplate_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {


                if (drpTemplate.SelectedIndex != 0)
                {
                    EMR_PTVitalsBAL objPhar = new EMR_PTVitalsBAL();
                    objPhar.EPV_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                    objPhar.EPV_ID = Convert.ToString(Session["EMR_ID"]);
                    objPhar.EPV_PT_ID = Convert.ToString(Session["EMR_PT_ID"]);
                    objPhar.EPV_DATE = Convert.ToString(Session["HPV_DATE"]);

                    objPhar.TemplateCode = drpTemplate.SelectedValue;
                    objPhar.VitalsAddFromTemplate();

                    if (Convert.ToString(Session["EMR_SAVE_VITAL_MULTIPLE"]) == "0")
                    {
                        BindVital();
                    }
                    BindVitalGrid();
                }



            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.drpTemplate_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;
        }

    }
}