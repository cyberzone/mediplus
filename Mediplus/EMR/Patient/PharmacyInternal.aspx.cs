﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_BAL;

namespace Mediplus.EMR.Patient
{
    public partial class PharmacyInternal : System.Web.UI.Page
    {
        static string strSessionDeptId;
        CommonBAL objCom = new CommonBAL();
        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindData()
        {

            DataSet ds = new DataSet();
            EMR_PTPharmacyInternal objCom = new EMR_PTPharmacyInternal();

            string Criteria = " 1=1 ";
            Criteria += " AND EPPI_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND EPPI_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
            ds = objCom.PharmacyInternalGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvPharmacy.DataSource = ds;
                gvPharmacy.DataBind();

            }
            else
            {
                gvPharmacy.DataBind();
            }
        }

        void BindHistoryData()
        {

            DataSet ds = new DataSet();
            EMR_PTPharmacyInternal objCom = new EMR_PTPharmacyInternal();

            string Criteria = " 1=1 ";
            Criteria += " AND EPPI_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            //  Criteria += " AND EPPI_ID <>'" + Convert.ToString(Session["EMR_ID"]) + "'";

            Criteria += " AND EPPI_ID in ( SELECT EPM_ID FROM EMR_PT_MASTER WHERE    EPM_PT_ID='" + Convert.ToString(Session["EMR_PT_ID"]) + "' )";
            ds = objCom.PharmacyInternalGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvPharmacyHistory.DataSource = ds;
                gvPharmacyHistory.DataBind();

            }
            else
            {
                gvPharmacyHistory.DataBind();
            }
        }


        void BindRoute()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 AND EPR_ACTIVE=1 ";
            Criteria += " AND EPR_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' ";
            DS = objCom.EMRRouteGet(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                drpRoute.DataSource = DS;
                drpRoute.DataTextField = "EPR_NAME";
                drpRoute.DataValueField = "EPR_CODE";
                drpRoute.DataBind();
            }
            drpRoute.Items.Insert(0, "Select Route");
            drpRoute.Items[0].Value = "";

            // objCom.BindMasterDropdown(Criteria, out drpDossage);

        }

        void BindNurse()
        {
            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();

            string Criteria = " 1=1  AND HSFM_SF_STATUS='Present' AND HSFM_CATEGORY='Nurse' ";
            Criteria += "  AND HSFM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' ";

            DS = objCom.GetStaffMaster(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                drpMedicNurse.DataSource = DS;
                drpMedicNurse.DataTextField = "FullName";
                drpMedicNurse.DataValueField = "HSFM_STAFF_ID";
                drpMedicNurse.DataBind();

                drpNonMedicNurse.DataSource = DS;
                drpNonMedicNurse.DataTextField = "FullName";
                drpNonMedicNurse.DataValueField = "HSFM_STAFF_ID";
                drpNonMedicNurse.DataBind();

            }
            drpMedicNurse.Items.Insert(0, "--- Select ---");
            drpMedicNurse.Items[0].Value = "";

            drpNonMedicNurse.Items.Insert(0, "--- Select ---");
            drpNonMedicNurse.Items[0].Value = "";

            if (Convert.ToString(Session["User_Category"]).ToUpper() == "NURSE")
            {



                for (int intCount = 0; intCount < drpMedicNurse.Items.Count; intCount++)
                {
                    if (drpMedicNurse.Items[intCount].Value == Convert.ToString(Session["User_Code"]))
                    {
                        drpMedicNurse.SelectedValue = Convert.ToString(Session["User_Code"]);
                        drpNonMedicNurse.SelectedValue = Convert.ToString(Session["User_Code"]);
                        goto FordrpMedicNurse;
                    }

                }

            FordrpMedicNurse: ;



            }

        }

        void ClearPhar()
        {
            txtServCode.Text = "";
            txtServName.Text = "";
            txtDossage1.Text = "";

            if (drpRoute.Items.Count > 0)
                drpRoute.SelectedIndex = 0;

            txtFreqyency.Text = "";
           
            drpFreqType.SelectedIndex = 0;


            txtDuration.Text = "";
            if (drpDurationType.Items.Count > 0)
                drpDurationType.SelectedIndex = 0;


            txtRemarks.Text = "";

            string strDate = "", strTime = "", strAM = "";
            strDate = objCom.fnGetDate("dd/MM/yyyy");
            strTime = objCom.fnGetDate("hh:mm");
            strAM = objCom.fnGetDate("tt");

            txtDate.Text = strDate;
            txtTime.Text = strTime;
            drpAM.SelectedValue = strAM;

        }

        void BindAuditDtls()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1   and   EPM_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
            EMR_PTMasterBAL OBJptm = new EMR_PTMasterBAL();
            DS = OBJptm.GetEMR_PTMaster(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                lblCreatedBy.Text = Convert.ToString(DS.Tables[0].Rows[0]["EPM_CRETAED_USER"]);
                lblCreatedDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["CreatedDate"]);
                lblModifiedBy.Text = Convert.ToString(DS.Tables[0].Rows[0]["EPM_MODIFIED_USER"]);
                lblModifiedDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["ModifiedDate"]);
            }
        }

        void UpdateAuditDtls()
        {
            CommonBAL objCom = new CommonBAL();
            objCom.EMRPTMasterUserDtlsUpdate(Convert.ToString(Session["EMR_ID"]), Convert.ToString(Session["User_ID"]));
        }

        void BindEMRData()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND EPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND EPM_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";


            EMR_PTMasterBAL objPTM = new EMR_PTMasterBAL();
            DataSet DS = new DataSet();

            DS = objPTM.GetEMR_PTMaster(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                txtNonMedication.Text = Convert.ToString(DS.Tables[0].Rows[0]["EPM_NONMEDICATION"]);
            }

        }

        void BindNonMedicGrid()
        {
            string Criteria = " 1=1  ";
            Criteria += " AND EPNPI_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND EPNPI_PT_ID='" + Convert.ToString(Session["EMR_PT_ID"]) + "'";
            Criteria += " AND EPNPI_EMR_ID ='" + Convert.ToString(Session["EMR_ID"]) + "'";

            EMR_PTPharmacyInternal objCom = new EMR_PTPharmacyInternal();
            DataSet DS = new DataSet();

            DS = objCom.NonPharmacyInternalGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                gvNonMedic.DataSource = DS;
                gvNonMedic.DataBind();
            }

        }

        void BindNonMedicHistoryGrid()
        {
            string Criteria = " 1=1  ";
            Criteria += " AND EPNPI_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND EPNPI_PT_ID='" + Convert.ToString(Session["EMR_PT_ID"]) + "'";
            // Criteria += " AND EPNPI_EMR_ID ='" + Convert.ToString(Session["EMR_ID"]) + "'";

            EMR_PTPharmacyInternal objCom = new EMR_PTPharmacyInternal();
            DataSet DS = new DataSet();

            DS = objCom.NonPharmacyInternalGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                gvNonMedicHistory.DataSource = DS;
                gvNonMedicHistory.DataBind();
            }

        }


        void ClearNonPhar()
        {
            txtNonMedication.Text = "";
            txtNonMedicRemarks.Text = "";

            ViewState["EPNPI_ID"] = "";

        }

        #endregion

        #region AutoExt
        [System.Web.Services.WebMethod]
        public static string[] GetServicessList(string prefixText)
        {
            DataSet ds = new DataSet();
            CommonBAL dbo = new CommonBAL();

            string[] Data;


            ds = dbo.HaadServicessListGet("Pharmacy", prefixText, "", strSessionDeptId);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = ds.Tables[0].Rows[i]["Code"].ToString() + "~" + ds.Tables[0].Rows[i]["Description"].ToString();


                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../../Default.aspx?NoSession=1"); }
            lblStatus.Text = "";
            try
            {
                if (!IsPostBack)
                {
                    if (GlobalValues.FileDescription == "SUMMERLAND")
                    {
                        lblHeader.Text = "Nursing Order";
                    }

                    string strDate = "", strTime = "", strAM = "";
                    strDate = objCom.fnGetDate("dd/MM/yyyy");
                    strTime = objCom.fnGetDate("hh:mm");
                    strAM = objCom.fnGetDate("tt");

                    txtDate.Text = strDate;
                    txtTime.Text = strTime;
                    drpAM.SelectedValue = strAM;


                    txtNonMedicDate.Text = strDate;
                    txtNonMedicTime.Text = strTime;
                    drpNonMedicAM.SelectedValue = strAM;



                    txtMedicGivenDate.Text = strDate;
                    txtMedicGivenTime.Text = strTime;
                    drpMedicGivenAM.SelectedValue = strAM;

                    txtNonMedicGivenDate.Text = strDate;
                    txtNonMedicGivenTime.Text = strTime;
                    drpNonMedicGivenAM.SelectedValue = strAM;


                    strSessionDeptId = Convert.ToString(Session["User_DeptID"]);
                    BindRoute();
                    BindData();
                    BindHistoryData();
                    BindEMRData();
                    BindNurse();

                    BindNonMedicGrid();
                    BindNonMedicHistoryGrid();
                    if (Convert.ToString(Session["User_Category"]).ToUpper() == "NURSE")
                    {
                        txtDate.Enabled = false;
                        txtTime.Enabled = false;
                        drpAM.Enabled = false;
                        txtServCode.Enabled = false;
                        txtServName.Enabled = false;
                        txtDossage1.Enabled = false;
                        drpRoute.Enabled = false;

                        txtDuration.Enabled = false;
                        drpDurationType.Enabled = false;
                        txtFreqyency.Enabled = false;
                       
                        drpFreqType.Enabled = false;

                        if (gvPharmacy.Rows.Count > 0)
                        {
                            gvPharmacy.Columns[0].Visible = false;
                        }




                        txtNonMedicDate.Enabled = false;
                        txtNonMedicTime.Enabled = false;
                        drpNonMedicAM.Enabled = false;
                        txtNonMedication.Enabled = false;

                        if (gvNonMedic.Rows.Count > 0)
                        {
                            gvNonMedic.Columns[0].Visible = false;
                        }


                    }
                    else
                    {
                        txtRemarks.Enabled = false;
                        txtMedicGivenDate.Enabled = false;
                        txtMedicGivenTime.Enabled = false;
                        drpMedicGivenAM.Enabled = false;
                        drpMedicNurse.Enabled = false;



                        txtNonMedicRemarks.Enabled = false;
                        txtNonMedicGivenDate.Enabled = false;
                        txtNonMedicGivenTime.Enabled = false;
                        drpNonMedicGivenAM.Enabled = false;
                        drpNonMedicNurse.Enabled = false;

                    }

                    if (Convert.ToString(Session["EMR_DISPLAY_AUDIT"]) == "1")
                    {
                        BindAuditDtls();
                        divAuditDtls.Visible = true;
                    }
                    if (Convert.ToString(Session["EPM_STATUS"]).ToUpper() == "Y" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                    {
                        btnAddPhar.Visible = false;
                        if (gvPharmacyHistory.Rows.Count > 0)
                        {
                            gvPharmacy.Columns[0].Visible = false;
                        }
                    }
                    //HIDE THE SAVE OPTION FOR NEXT DAY OF THE VISIT
                    if (Convert.ToString(Session["EMRCurrentDateDiff"]) != "0" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                    {
                        btnAddPhar.Visible = false;
                        if (gvPharmacyHistory.Rows.Count > 0)
                        {
                            gvPharmacy.Columns[0].Visible = false;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "       PharmacyInternal.Index.Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnAddPhar_Click(object sender, EventArgs e)
        {
            try
            {

                if (txtServCode.Text == "")
                {
                    lblStatus.Text = "Please Enter Medication";

                    goto FunEnd;
                }

                if (Convert.ToString(Session["User_Category"]).ToUpper() == "NURSE")
                {
                    if (drpMedicNurse.SelectedIndex == 0)
                    {
                        lblStatus.Text = "Please Select Name";

                        goto FunEnd;
                    }

                }

                EMR_PTPharmacyInternal objDiag = new EMR_PTPharmacyInternal();
                objDiag.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objDiag.EPPI_ID = Convert.ToString(Session["EMR_ID"]);
                objDiag.EPPI_DATE = txtDate.Text.Trim() + " " + txtTime.Text.Trim() + " " + drpAM.SelectedValue; // drpHour.SelectedValue + ":" + drpMin.SelectedValue + ":00";
                objDiag.EPPI_PHY_CODE = txtServCode.Text;
                objDiag.EPPI_PHY_NAME = txtServName.Text.Replace("'", "''");

                objDiag.EPPI_DOSAGE = txtDossage1.Text;
                objDiag.EPPI_ROUTE = drpRoute.SelectedValue;
                objDiag.EPPI_FREQUENCY = txtFreqyency.Text.Trim();
                objDiag.EPPI_FREQUENCYTYPE = drpFreqType.SelectedValue;
                objDiag.EPPI_DURATION = txtDuration.Text.Trim();
                objDiag.EPPI_DURATION_TYPE = drpDurationType.SelectedValue;
                objDiag.EPPI_REMARKS = txtRemarks.Text;
              
                objDiag.UserID = Convert.ToString(Session["User_ID"]);


                if (Convert.ToString(Session["User_Category"]).ToUpper() != "NURSE")
                {
                    objDiag.PharmacyInternalAdd();
                }
                else
                {

                    string FieldNameWithValues = "EPPI_REMARKS='" + txtRemarks.Text + "'";
                    FieldNameWithValues += ",EPPI_GIVEN_DATE=CONVERT(DATETIME,'" + txtMedicGivenDate.Text.Trim() + " " + txtMedicGivenTime.Text.Trim() + " " + drpMedicGivenAM.SelectedValue + "',103)";
                    FieldNameWithValues += ",EPPI_GIVENBY='" + drpMedicNurse.SelectedValue + "'";
                    FieldNameWithValues += ",EPPI_GIVENBY_NAME='" + drpMedicNurse.SelectedItem.Text.Trim() + "'";
                    FieldNameWithValues += ",EPPI_MODIFIED_DATE=getdate() ,EPPI_MODIFIED_USER='" + Convert.ToString(Session["User_ID"]) + "'";


                    string Criteria = " 1=1 ";
                    Criteria += " AND EPPI_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
                    Criteria += " AND EPPI_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
                    Criteria += " AND EPPI_PHY_CODE ='" + txtServCode.Text + "'";

                    objCom.fnUpdateTableData(FieldNameWithValues, "EMR_PT_PHARMACY_INTERNAL", Criteria);
                }



                BindData();
                ClearPhar();
                BindHistoryData();
                if (Convert.ToString(Session["EMR_DISPLAY_AUDIT"]) == "1")
                {
                    if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                    {
                        UpdateAuditDtls();
                    }
                }

                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowMessage()", true);

            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PharmacyInternal.btnAddPhar_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void DeleteeDiag_Click(object sender, EventArgs e)
        {
            try
            {
                ImageButton btnDel = new ImageButton();
                btnDel = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnDel.Parent.Parent;

                Label lblPhyCode = (Label)gvScanCard.Cells[0].FindControl("lblPhyCode");


                EMR_PTPharmacyInternal objDiag = new EMR_PTPharmacyInternal();
                objDiag.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objDiag.EPPI_ID = Convert.ToString(Session["EMR_ID"]);
                objDiag.EPPI_PHY_CODE = lblPhyCode.Text;
                objDiag.UserID = Convert.ToString(Session["User_ID"]);

                objDiag.PharmacyInternalDelete();


                BindData();
                ClearPhar();
                BindHistoryData();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.DeletePhar_Click");
                TextFileWriting(ex.Message.ToString());
            }



        }

        protected void PhySelect_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btnDel = new LinkButton();
                btnDel = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnDel.Parent.Parent;

                Label lblPhyCode = (Label)gvScanCard.Cells[0].FindControl("lblPhyCode");
                Label lblPhyDesc = (Label)gvScanCard.Cells[0].FindControl("lblPhyDesc");

                Label lblPhyDuration = (Label)gvScanCard.Cells[0].FindControl("lblPhyDuration");
                Label lblPhyDurationType = (Label)gvScanCard.Cells[0].FindControl("lblPhyDurationType");

                Label lblPhyDosage = (Label)gvScanCard.Cells[0].FindControl("lblPhyDosage");


                Label lblPhyRoute = (Label)gvScanCard.Cells[0].FindControl("lblPhyRoute");



                Label lblPhyFrequency = (Label)gvScanCard.Cells[0].FindControl("lblPhyFrequency");
                Label lblPhyFrequencyType = (Label)gvScanCard.Cells[0].FindControl("lblPhyFrequencyType");


                Label lblPhyDate = (Label)gvScanCard.Cells[0].FindControl("lblPhyDate");
                Label lblPhyTime = (Label)gvScanCard.Cells[0].FindControl("lblPhyTime");
                Label lblPhyType = (Label)gvScanCard.Cells[0].FindControl("lblPhyType");
                Label lblPhyRemarks = (Label)gvScanCard.Cells[0].FindControl("lblPhyRemarks");

                Label lblPhyGivenDate = (Label)gvScanCard.Cells[0].FindControl("lblPhyGivenDate");
                Label lblPhyGivenTime = (Label)gvScanCard.Cells[0].FindControl("lblPhyGivenTime");
                Label lblPhyGivenBy = (Label)gvScanCard.Cells[0].FindControl("lblPhyGivenBy");
                

                txtDate.Text = lblPhyDate.Text;


                //string strArrivalDate = lblPhyTime.Text;
                //string[] arrArrivalDater = strArrivalDate.Split(':');
                //if (arrArrivalDater.Length > 1)
                //{
                //    drpHour.SelectedValue = arrArrivalDater[0];
                //    drpMin.SelectedValue = arrArrivalDater[1];

                //}

                string strTime = lblPhyTime.Text;

                string[] arrTime = strTime.Split(' ');
                if (arrTime.Length > 0)
                {
                    txtTime.Text = arrTime[0];
                    if (arrTime.Length > 1)
                    {
                        drpAM.SelectedValue = arrTime[1];
                    }

                }



                txtServCode.Text = lblPhyCode.Text;
                txtServName.Text = lblPhyDesc.Text;

                txtDuration.Text = lblPhyDuration.Text;
                drpDurationType.SelectedValue = lblPhyDurationType.Text;

                txtDossage1.Text = lblPhyDosage.Text;

                //  drpDossage.SelectedValue = lblPhyDosage.Text;


                drpRoute.SelectedValue = lblPhyRoute.Text;


                txtFreqyency.Text = lblPhyFrequency.Text;
                drpFreqType.SelectedValue = lblPhyFrequencyType.Text;
                

                // txtRefil.Text = lblPhyRefil.Text;
                // txtFromDate.Text = lblPhyStDate.Text;
                // txtToDate.Text = lblPhyEndDate.Text;

                //  chkLifeLong.Checked = Convert.ToBoolean(lblPhyLifeLong.Text);


                txtRemarks.Text = lblPhyRemarks.Text;

                if (lblPhyGivenDate.Text.Trim() != "")
                {
                    txtMedicGivenDate.Text = lblPhyGivenDate.Text;
                }

                if (lblPhyGivenTime.Text != "")
                {
                    string strNonTime = lblPhyGivenTime.Text;

                    string[] arrNonTime = strNonTime.Split(' ');
                    if (arrNonTime.Length > 0)
                    {
                        txtMedicGivenTime.Text = arrNonTime[0];
                        if (arrNonTime.Length > 1)
                        {
                            drpMedicGivenAM.SelectedValue = arrNonTime[1];
                        }

                    }

                }

                if (lblPhyGivenBy.Text != "")
                {
                    drpMedicNurse.SelectedValue = lblPhyGivenBy.Text;
                }
                else
                {
                    if (Convert.ToString(Session["User_Category"]).ToUpper() == "NURSE")
                    {
                        for (int intCount = 0; intCount < drpMedicNurse.Items.Count; intCount++)
                        {
                            if (drpMedicNurse.Items[intCount].Value == Convert.ToString(Session["User_Code"]))
                            {
                                drpMedicNurse.SelectedValue = Convert.ToString(Session["User_Code"]);
                                goto FordrpMedicNurse;
                            }

                        }

                    FordrpMedicNurse: ;
                    }
                }

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PharmacyInternal.PhySelect_Click");
                TextFileWriting(ex.Message.ToString());
            }



        }

        protected void btnNonMedicSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToString(Session["User_Category"]).ToUpper() == "NURSE")
                {
                    if (drpNonMedicNurse.SelectedIndex == 0)
                    {
                        lblStatus.Text = "Please Select Name";

                        goto FunEnd;
                    }

                }

                EMR_PTPharmacyInternal objDiag = new EMR_PTPharmacyInternal();
                objDiag.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objDiag.EMR_ID = Convert.ToString(Session["EMR_ID"]);
                objDiag.PT_ID = Convert.ToString(Session["EMR_PT_ID"]);
                objDiag.EPNPI_ID = Convert.ToString(ViewState["EPNPI_ID"]);
                objDiag.EPNPI_DATE = txtNonMedicDate.Text.Trim() + " " + txtNonMedicTime.Text.Trim() + " " + drpNonMedicAM.SelectedValue; // drpHour.SelectedValue + ":" + drpMin.SelectedValue + ":00";
                objDiag.EPNPI_DESCRIPTION = txtNonMedication.Text.Replace("'", "''");
                objDiag.UserID = Convert.ToString(Session["User_ID"]);

                if (Convert.ToString(Session["User_Category"]).ToUpper() != "NURSE")
                {
                    objDiag.NonPharmacyInternalAdd();
                }
                else
                {

                    string FieldNameWithValues = "EPNPI_REMARKS='" + txtNonMedicRemarks.Text + "' ";
                    FieldNameWithValues += " ,EPNPI_GIVEN_DATE=CONVERT(DATETIME,'" + txtNonMedicGivenDate.Text.Trim() + " " + txtNonMedicGivenTime.Text.Trim() + " " + drpNonMedicGivenAM.SelectedValue + "',103)";
                    FieldNameWithValues += " ,EPNPI_GIVENBY='" + drpNonMedicNurse.SelectedValue + "'";
                    FieldNameWithValues += " ,EPNPI_GIVENBY_NAME='" + drpNonMedicNurse.SelectedItem.Text.Trim() + "'";
                    FieldNameWithValues += " ,EPNPI_MODIFIED_DATE=getdate() ,EPNPI_MODIFIED_USER='" + Convert.ToString(Session["User_ID"]) + "'";


                    string Criteria = " 1=1 ";
                    Criteria += " AND EPNPI_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
                    Criteria += " AND EPNPI_EMR_ID=" + Convert.ToString(Session["EMR_ID"]);
                    Criteria += " AND EPNPI_ID =" + Convert.ToString(ViewState["EPNPI_ID"]);

                    objCom.fnUpdateTableData(FieldNameWithValues, "EMR_PT_NONPHARMACY_INTERNAL", Criteria);
                }

                BindNonMedicGrid();
                BindNonMedicHistoryGrid();
                ClearNonPhar();

                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowMessage()", true);

            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PharmacyInternal.btnNonMedicSave_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void DeleteNonPhy_Click(object sender, EventArgs e)
        {
            try
            {
                ImageButton btnDel = new ImageButton();
                btnDel = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnDel.Parent.Parent;

                Label lblNonPhyID = (Label)gvScanCard.Cells[0].FindControl("lblNonPhyID");


                string Criteria = " 1=1 ";
                Criteria += " AND EPNPI_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
                Criteria += " AND EPNPI_EMR_ID=" + Convert.ToString(Session["EMR_ID"]);
                Criteria += " AND EPNPI_ID =" + lblNonPhyID.Text;

                objCom.fnDeleteTableData("EMR_PT_NONPHARMACY_INTERNAL", Criteria);


                BindNonMedicGrid();
                BindNonMedicHistoryGrid();
                ClearNonPhar();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.DeletePhar_Click");
                TextFileWriting(ex.Message.ToString());
            }



        }

        protected void NonPhySelect_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btnDel = new LinkButton();
                btnDel = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnDel.Parent.Parent;


                Label lblNonPhyID = (Label)gvScanCard.Cells[0].FindControl("lblNonPhyID");

                Label lblNonPhyDate = (Label)gvScanCard.Cells[0].FindControl("lblNonPhyDate");
                Label lblNonPhyTime = (Label)gvScanCard.Cells[0].FindControl("lblNonPhyTime");
                Label lblNonPhyDesc = (Label)gvScanCard.Cells[0].FindControl("lblNonPhyDesc");
                Label lblNonPhyRemarks = (Label)gvScanCard.Cells[0].FindControl("lblNonPhyRemarks");

                Label lblNonPhyGivenDate = (Label)gvScanCard.Cells[0].FindControl("lblNonPhyGivenDate");
                Label lblNonPhyGivenTime = (Label)gvScanCard.Cells[0].FindControl("lblNonPhyGivenTime");
                Label lblNonPhyGivenBy = (Label)gvScanCard.Cells[0].FindControl("lblNonPhyGivenBy");


                ViewState["EPNPI_ID"] = lblNonPhyID.Text;
                txtNonMedicDate.Text = lblNonPhyDate.Text;


                string strTime = lblNonPhyTime.Text;

                string[] arrTime = strTime.Split(' ');
                if (arrTime.Length > 0)
                {
                    txtNonMedicTime.Text = arrTime[0];
                    if (arrTime.Length > 1)
                    {
                        drpNonMedicAM.SelectedValue = arrTime[1];
                    }

                }



                txtNonMedication.Text = lblNonPhyDesc.Text;
                txtNonMedicRemarks.Text = lblNonPhyRemarks.Text;

                if (lblNonPhyGivenDate.Text.Trim() != "")
                {
                    txtNonMedicGivenDate.Text = lblNonPhyGivenDate.Text;
                }

                if (lblNonPhyGivenTime.Text != "")
                {
                    string strNonTime = lblNonPhyGivenTime.Text;

                    string[] arrNonTime = strNonTime.Split(' ');
                    if (arrNonTime.Length > 0)
                    {
                        txtNonMedicGivenTime.Text = arrNonTime[0];
                        if (arrNonTime.Length > 1)
                        {
                            drpNonMedicGivenAM.SelectedValue = arrNonTime[1];
                        }

                    }

                }

                if (lblNonPhyGivenBy.Text != "")
                {
                    drpNonMedicNurse.SelectedValue = lblNonPhyGivenBy.Text;
                }
                else
                {
                    if (Convert.ToString(Session["User_Category"]).ToUpper() == "NURSE")
                    {

                        for (int intCount = 0; intCount < drpMedicNurse.Items.Count; intCount++)
                        {
                            if (drpMedicNurse.Items[intCount].Value == Convert.ToString(Session["User_Code"]))
                            {
                                drpNonMedicNurse.SelectedValue = Convert.ToString(Session["User_Code"]);
                                goto FordrpMedicNurse;
                            }

                        }

                    FordrpMedicNurse: ;
                    }

                }

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PharmacyInternal.PhySelect_Click");
                TextFileWriting(ex.Message.ToString());
            }



        }



        
    }


}