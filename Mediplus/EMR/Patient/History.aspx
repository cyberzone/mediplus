﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" EnableEventValidation="true" AutoEventWireup="true" CodeBehind="History.aspx.cs" Inherits=" Mediplus.EMR.Patient.History" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
    <link href="../../Styles/style.css" rel="Stylesheet" type="text/css" />
    <link href="../../Styles/style.css" rel="Style" type="text/css" />
    <script src="../../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    
    
     <script src="../../Scripts/AccordionScript.js"></script>
     <link rel="stylesheet" href="../../Styles/Accordionstyles.css">

    <link href="../Styles/style.css" rel="stylesheet" type="text/css" />

    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
     <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .box-header
        {
            border-bottom: 4px solid #f4fbfd;
            height: 70px;
        }

        .box-title
        {
            padding-bottom: 5px;
            border-bottom: 4px solid #92c500;
            float: left;
            font-size: 1.5em;
            color: #2078c0;
        }

        h1, h2, h3, h4, h5, h6
        {
            font-size: 100%;
            font-weight: normal;
            font-family: "Segoe UI", Arial, Helvetica, sans-serif;
        }
    </style>

    <script language="javascript" type="text/javascript">
        function BindReportName(selValue) {


            alert(selValue);
            var vcrTemp1 = document.getElementById("<%=hidPrintClicked.ClientID%>");
            vcrTemp1.value = vcrTemp.value;
        }

        function ShowPrescriptionPrintPDF(FileNo, BranchId, EPM_ID) {

            var win = '';
            var Report = "Prescription.rpt"; //"EMRTestReport.rpt";


            var Criteria = " 1=1 ";
            Criteria += ' AND {EMR_PT_MASTER.EPM_PT_ID}=\'' + FileNo + '\'';
            Criteria += ' AND {EMR_PT_MASTER.EPM_BRANCH_ID}=\'' + BranchId + '\'';
            Criteria += ' AND {EMR_PT_MASTER.EPM_ID}=' + EPM_ID;
            win = window.open('../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria, '_new', 'menubar=no,left=100,top=100,height=850,width=1075,scrollbars=1')


            win.focus();


        }

        function ShowUltrasound(ReportType, FileNo, BranchId, EPM_ID) {

            var win = '';
            var Report = "UltrasoundSingle.rpt"; //"EMRTestReport.rpt";


            if (ReportType == 'UltrasoundSingle') {
                Report = "UltrasoundSingle.rpt";
            }
            else if (ReportType == 'UltrasoundTwins') {
                Report = "UltrasoundTwins.rpt";
            }
            else if (ReportType == 'UltrasoundTriplets') {
                Report = "UltrasoundTriplets.rpt";
            }


            var Criteria = " 1=1 ";
            Criteria += ' AND {EMR_PT_MASTER.EPM_PT_ID}=\'' + FileNo + '\'';
            Criteria += ' AND {EMR_PT_MASTER.EPM_BRANCH_ID}=\'' + BranchId + '\'';
            Criteria += ' AND {EMR_PT_MASTER.EPM_ID}=' + EPM_ID;
            win = window.open('../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria, '_new', 'menubar=no,left=100,top=100,height=850,width=1075,scrollbars=1')


            win.focus();


        }

        function ShowPTBalHistory(FileNo, BranchId, EMRID) {
            var Report = "";
            Report = "HmsPtBalHist.rpt";

            var Criteria = " 1=1 ";
            Criteria += ' AND {HMS_PATIENT_BALANCE_HISTORY.HPB_PT_ID}=\'' + FileNo + '\'';
            Criteria += ' AND {HMS_PATIENT_BALANCE_HISTORY.HPB_BRANCH_ID}=\'' + BranchId + '\'';
            // Criteria += ' AND {EMR_PT_MASTER.EPM_ID}=' + EMRID;

            //var win = window.open('../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula={HMS_PATIENT_MASTER.HPM_PT_ID}=\'' + FileNo + '\' and  {HMS_PATIENT_MASTER.HPM_BRANCH_ID}=\'' + BranchId + '\'', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
            var win = window.open('../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria, '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')

            win.focus();

        }

        function ShowOperationNotes(FileNo, BranchId, EMRID) {
            var Report = "";
            Report = "EMR_OperationNotes.rpt";

            var Criteria = " 1=1 ";
            Criteria += ' AND {EMR_PT_MASTER.EPM_PT_ID}=\'' + FileNo + '\'';
            Criteria += ' AND {EMR_PT_MASTER.EPM_BRANCH_ID}=\'' + BranchId + '\'';
            Criteria += ' AND {EMR_PT_MASTER.EPM_ID}=' + EMRID;

            //var win = window.open('../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula={HMS_PATIENT_MASTER.HPM_PT_ID}=\'' + FileNo + '\' and  {HMS_PATIENT_MASTER.HPM_BRANCH_ID}=\'' + BranchId + '\'', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
            var win = window.open('../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria, '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')

            win.focus();

        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  <input type="hidden" id="hidFileNo" runat="server" />
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">History</h3>
        </div>
    </div>

    <fieldset style="width:80%">
        <legend class="lblCaption1" style="font-weight: bold;">List of Previous Visit Histories</legend>
        <input type="hidden" runat="server" id="hidPrintClicked" value="false" />
        <table style="width: 100%" class="gridspacy">
            <tr>
                <td class="lblCaption1" align="right">Select Report Type: 
                    <select id="selReports" runat="server" class="lblCaption1"></select>
                </td>
            </tr>
            <tr>
                <td class="lblCaption1  BoldStyle">
                    <asp:GridView ID="gvEMR_PTMaster" runat="server" AutoGenerateColumns="False" GridLines="None"
                        EnableModelValidation="True" Width="100%">
                        <HeaderStyle CssClass="GridHeader_Blue" Height="20px" Font-Bold="true" />
                        <RowStyle CssClass="GridRow" Height="20px" />
                        <Columns>

                            <asp:TemplateField HeaderText="Emr ID">
                                <ItemTemplate>
                                    <asp:Label ID="lblFileNumber" CssClass="lblFileNumber" runat="server" Text='<%# Bind("FileNumber") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblPatientMasterID" CssClass="lblCaption1" runat="server" Text='<%# Bind("PatientMasterID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Date">
                                <ItemTemplate>

                                    <asp:Label ID="lblgvEMR_PTMaster_Date" CssClass="lblCaption1" runat="server" Text='<%# Bind("DateDesc") %>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Doctor Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblDoctorID" CssClass="lblCaption1" runat="server" Text='<%# Bind("DoctorID") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblBMI" CssClass="lblCaption1" runat="server" Text='<%# Bind("DoctorName") %>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Diagnosis Codes">
                                <ItemTemplate>
                                    <asp:Label ID="lblDiagCode" CssClass="lblCaption1" Width="300px" runat="server" Text='<%# Bind("DiagnosisCodes") %>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Insurance Company">
                                <ItemTemplate>
                                    <asp:Label ID="lblInsName" CssClass="lblCaption1" runat="server" Text='<%# Bind("InsuranceName") %>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField >
                                <ItemTemplate>
                                    <asp:ImageButton ID="imgPrint" runat="server" OnClick="gvbtnPrint_Click" ImageUrl="~/WebReports/Images/printer.png" style="height:25px;width:30px;border:none;" />


                                </ItemTemplate>


                            </asp:TemplateField>
                        </Columns>


                    </asp:GridView>
                </td>
            </tr>
        </table>
    </fieldset>
      
    <table width="80%" runat="server" id="tblTotalPresc" visible="false" >
        <tr>
            <td align="left">
                <asp:Label ID="Label9" runat="server" CssClass="label" Font-Bold="true"
                    Text="Total Prescription of current month :"></asp:Label>
                &nbsp;
                       <asp:Label ID="lblTotalPresc" runat="server" CssClass="label" Font-Bold="true" ></asp:Label>
                &nbsp;&nbsp;

            </td>
        </tr>
    </table>


</asp:Content>
