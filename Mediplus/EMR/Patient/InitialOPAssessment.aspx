﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EMR/PatientHeader.Master" AutoEventWireup="true" CodeBehind="InitialOPAssessment.aspx.cs" Inherits=" Mediplus.EMR.Patient.InitialOPAssessment" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
      <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />
    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>
    <link href="../Content/themes/base/jquery-ui.css" />
    <style type="text/css">
        .box-header
        {
            border-bottom: 4px solid #f4fbfd;
            height: 70px;
        }

        .box-title
        {
            padding-bottom: 5px;
            border-bottom: 4px solid #92c500;
            float: left;
            font-size: 1.1em;
            color: #2078c0;
        }

        h1, h2, h3, h4, h5, h6
        {
            font-size: 100%;
            font-weight: normal;
            font-family: "Segoe UI", Arial, Helvetica, sans-serif;
        }
    </style>

    <script type="text/javascript">
        function ShowMessage() {
            $("#myMessage").show();
            setTimeout(function () {
                var selectedEffect = 'blind';
                var options = {};
                $("#myMessage").hide();
            }, 2000);
            return true;
        }

        function ShowAuditLog() {

            var win = window.open('../AuditLogDisplay.aspx?ScreenID=OP_INITIAL_OP_ASS&DataDisplay=ByEMR_ID', 'AuditLogDisplay', 'menubar=no,left=100,top=80,height=550,width=1075,scrollbars=1')

        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td style="width:90%;text-align:right;">
                    <div id="divAuditDtls" runat="server" visible="false" style="text-align: right; width: 100%;" class="lblCaption1">
                        Creaded By :
                                                  <asp:Label ID="lblCreatedBy" runat="server" CssClass="label"></asp:Label>
                        &nbsp;-&nbsp;
                                            <asp:Label ID="lblCreatedDate" runat="server" CssClass="label"></asp:Label>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        Modified By
                                                 <asp:Label ID="lblModifiedBy" runat="server" CssClass="label"></asp:Label>
                        &nbsp;-&nbsp;
                                             <asp:Label ID="lblModifiedDate" runat="server" CssClass="label"></asp:Label>
                           &nbsp;&nbsp;
                    </div>
            </td>
            <td style="width:10%;text-align:right;">
 
              <a href="javascript:ShowAuditLog();" style="text-decoration: none;" class="lblCaption">Audit Log </a>
   

            </td>
        </tr>
    </table>
  
     
    <div style="padding-left: 60%; width: 100%;">
        <div id="myMessage" style="display: none; border: groove; height: 30px; width: 200px; background-color: gray; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #fff; padding: 20px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute;">
            Saved Successfully
        </div>
    </div>

    <table width="100%">
        <tr>
            <td style="text-align: left; width: 50%;">
                <h3 class="box-title">Integrated Adult initial Out Patient Assessment</h3>
            </td>
            <td style="text-align: right; width: 50%;">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="btnSave" runat="server" CssClass="button orange small" Text="Save Record" OnClick="btnSave_Click" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>

    <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="AjaxTabStyle" Width="100%">
        <asp:TabPanel runat="server" ID="TabPanelGeneral" HeaderText="Assessment Dtls 1" Width="100%">
            <ContentTemplate>
                <table width="100%">

                    <tr>
                        <td class="lblCaption1" style="width: 300px">Functional		 
                        </td>
                        <td class="lblCaption1">
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                                    <asp:RadioButtonList ID="radFunctional" runat="server" CssClass="label" RepeatDirection="Horizontal" Width="250px">
                                        <asp:ListItem Text="Independent" Value="Independent"></asp:ListItem>
                                        <asp:ListItem Text="Assisted" Value="Assisted"></asp:ListItem>
                                        <asp:ListItem Text="Dependent" Value="Dependent"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </ContentTemplate>
                            </asp:UpdatePanel>

                        </td>
                    </tr>

                    <tr>
                        <td class="lblCaption1">Fall Risk Assessment required as per the policy		 
                        </td>
                        <td class="lblCaption1">
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>
                                    <asp:RadioButtonList ID="radAssRequired" runat="server" CssClass="label" RepeatDirection="Horizontal" Width="250px">
                                        <asp:ListItem Text="No" Value="No"></asp:ListItem>
                                        <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                                    </asp:RadioButtonList>

                                </ContentTemplate>
                            </asp:UpdatePanel>

                        </td>
                    </tr>

                </table>
                <fieldset>
                    <legend class="lblCaption1">Nutritional screening  (Please √  appropriate)</legend>
                    <table width="100%">
                        <tr>
                            <td class="lblCaption1" colspan="4"></td>
                        </tr>
                        <tr>
                            <td class="lblCaption1" style="width: 100px">Appetite: 		
                            </td>
                            <td class="lblCaption1">
                                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                    <ContentTemplate>
                                        <asp:RadioButtonList ID="radAppetite" runat="server" CssClass="label" RepeatDirection="Horizontal" Width="255px">
                                            <asp:ListItem Text="Good" Value="Good"></asp:ListItem>
                                            <asp:ListItem Text="Fair" Value="Fair"></asp:ListItem>
                                            <asp:ListItem Text="Poor" Value="Poor"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>

                            </td>
                        </tr>
                        <tr>
                            <td class="lblCaption1">Swallowing: 		
                            </td>
                            <td class="lblCaption1">
                                <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                    <ContentTemplate>
                                        <asp:RadioButtonList ID="radSwallowing" runat="server" CssClass="label" RepeatDirection="Horizontal" Width="205px">
                                            <asp:ListItem Text="Normal" Value="Normal"></asp:ListItem>
                                            <asp:ListItem Text="Abnormal" Value="Abnormal"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>

                            </td>
                        </tr>
                        <tr>
                            <td class="lblCaption1" style="width: 100px">Dentition: 		
                            </td>
                            <td colspan="3" class="lblCaption1">
                                <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                    <ContentTemplate>
                                        <asp:RadioButtonList ID="radDentition" runat="server" CssClass="label" RepeatDirection="Horizontal" Width="255px">
                                            <asp:ListItem Text="Good" Value="Good"></asp:ListItem>
                                            <asp:ListItem Text="Fair" Value="Fair"></asp:ListItem>
                                            <asp:ListItem Text="Poor" Value="Poor"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>

                            </td>


                        </tr>
                        <tr>
                            <td class="lblCaption1" style="width: 100px">Weight Loss 		
                            </td>
                            <td class="lblCaption1">
                                <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                                    <ContentTemplate>
                                        <input type="radio" id="radWeightLossNo" name="radWeightLoss" runat="server" value="No" /><label for="radWeightLossNo" class="lblCaption1">No</label>
                                        <input type="radio" id="radWeightLossYes" name="radWeightLoss" runat="server" value="Yes" /><label for="radWeightLossYes" class="lblCaption1">Yes</label>
                                        (If Yes, &nbsp;<asp:TextBox ID="txtKgLoss" runat="server" CssClass="label" Width="30px"></asp:TextBox>
                                        Kgs in
                 &nbsp;<asp:TextBox ID="txtMonthsLoss" runat="server" CssClass="label" Width="30px"></asp:TextBox>
                                        Months)
                                    </ContentTemplate>
                                </asp:UpdatePanel>

                            </td>
                        </tr>
                        <tr>
                            <td class="lblCaption1" style="width: 100px">Weight gain
                            </td>
                            <td class="lblCaption1">

                                <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                                    <ContentTemplate>

                                        <input type="radio" id="radWeightGainNo" name="radWeightgain" runat="server" value="No" /><label for="radWeightGainNo" class="lblCaption1">No</label>
                                        <input type="radio" id="radWeightGainYes" name="radWeightgain" runat="server" value="Yes" /><label for="radWeightGainYes" class="lblCaption1">Yes</label>
                                        (If Yes, &nbsp;<asp:TextBox ID="txtKgGain" runat="server" CssClass="label" Width="30px"></asp:TextBox>
                                        Kgs in
                 &nbsp;<asp:TextBox ID="txtMonthsGain" runat="server" CssClass="label" Width="30px"></asp:TextBox>
                                        Months) 

                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td class="lblCaption1" style="width: 100px">Chronic illness
                            </td>
                            <td class="lblCaption1" colspan="3">
                                <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                                    <ContentTemplate>
                                        <asp:CheckBoxList ID="radChronicIllness" runat="server" CssClass="label" RepeatDirection="Horizontal" Width="590px">
                                            <asp:ListItem Text="None" Value="None"></asp:ListItem>
                                            <asp:ListItem Text="DM" Value="DM"></asp:ListItem>
                                            <asp:ListItem Text="HTN" Value="HTN"></asp:ListItem>
                                            <asp:ListItem Text="CAD" Value="CAD"></asp:ListItem>
                                            <asp:ListItem Text="Dyslipidemia" Value="Dyslipidemia"></asp:ListItem>
                                            <asp:ListItem Text="Others" Value="Others"></asp:ListItem>
                                        </asp:CheckBoxList>
                                        <asp:TextBox ID="txtChronicIllness" runat="server" CssClass="label" Width="200px"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td class="lblCaption1" style="width: 100px">Gastrostomy
                            </td>
                            <td class="lblCaption1">
                                <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                                    <ContentTemplate>
                                        <asp:RadioButtonList ID="radGastrostomy" runat="server" CssClass="label" RepeatDirection="Horizontal" Width="190px">
                                            <asp:ListItem Text="No" Value="No"></asp:ListItem>
                                            <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td class="lblCaption1" style="width: 100px">NG Tube
                            </td>
                            <td class="lblCaption1">
                                <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                                    <ContentTemplate>
                                        <asp:RadioButtonList ID="radNGTube" runat="server" CssClass="label" RepeatDirection="Horizontal" Width="190px">
                                            <asp:ListItem Text="No" Value="No"></asp:ListItem>
                                            <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>


                        </tr>

                        <tr>
                            <td class="lblCaption1" style="width: 100px">Type of Diet
                            </td>
                            <td class="lblCaption1">
                                <asp:UpdatePanel ID="UpdatePanel12" runat="server">
                                    <ContentTemplate>

                                        <input type="radio" id="radTypeofDietNor" name="radTypeofDiet" runat="server" value="Normal" /><label for="radTypeofDietNor" class="lblCaption1">Normal</label>
                                        <input type="radio" id="radTypeofDietSpe" name="radTypeofDiet" runat="server" value="Special" /><label for="radTypeofDietSpe" class="lblCaption1">Special</label>

                                        <asp:TextBox ID="txtTypeofDiet" runat="server" CssClass="label" Width="200px"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td class="lblCaption1" colspan="4">Psychosocial Assessment (Please √ appropriate)
                            </td>
                        </tr>

                        <tr>
                            <td class="lblCaption1" style="width: 100px">Mood
                            </td>
                            <td class="lblCaption1" colspan="3">
                                <asp:UpdatePanel ID="UpdatePanel13" runat="server">
                                    <ContentTemplate>
                                        <asp:RadioButtonList ID="radMood" runat="server" CssClass="label" RepeatDirection="Horizontal" Width="672px">
                                            <asp:ListItem Text="Calm" Value="Calm"></asp:ListItem>
                                            <asp:ListItem Text="Sad" Value="Sad"></asp:ListItem>
                                            <asp:ListItem Text="Anxious" Value="Anxious"></asp:ListItem>
                                            <asp:ListItem Text="Tearful" Value="Tearful"></asp:ListItem>
                                            <asp:ListItem Text="Combative" Value="Combative"></asp:ListItem>
                                            <asp:ListItem Text="Agitated" Value="Agitated"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td class="lblCaption1" style="width: 100px">Expressed 
                            </td>
                            <td class="lblCaption1" colspan="3">
                                <asp:UpdatePanel ID="UpdatePanel14" runat="server">
                                    <ContentTemplate>
                                        <asp:RadioButtonList ID="radExpressed" runat="server" CssClass="label" RepeatDirection="Horizontal" Width="580px">
                                            <asp:ListItem Text="None" Value="None"></asp:ListItem>
                                            <asp:ListItem Text="Shame" Value="Shame"></asp:ListItem>
                                            <asp:ListItem Text="Guilt" Value="Guilt"></asp:ListItem>
                                            <asp:ListItem Text="Negative Feelings about self" Value="Negative Feelings about self"></asp:ListItem>

                                        </asp:RadioButtonList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td class="lblCaption1" style="width: 100px">Support System
                            </td>
                            <td class="lblCaption1" colspan="3">
                                <asp:UpdatePanel ID="UpdatePanel15" runat="server">
                                    <ContentTemplate>
                                        <asp:RadioButtonList ID="radSupportSystem" runat="server" CssClass="label" RepeatDirection="Horizontal" Width="670px">
                                            <asp:ListItem Text="Parents" Value="Parents"></asp:ListItem>
                                            <asp:ListItem Text="Spouse" Value="Spouse"></asp:ListItem>
                                            <asp:ListItem Text="Children" Value="Children"></asp:ListItem>
                                            <asp:ListItem Text="Relatives" Value="Relatives"></asp:ListItem>
                                            <asp:ListItem Text="Friends" Value="Friends"></asp:ListItem>
                                            <asp:ListItem Text="Stay Alone" Value="Stay Alone"></asp:ListItem>
                                            <asp:ListItem Text="None" Value="None"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>

                            </td>
                        </tr>
                    </table>
                </fieldset>
            </ContentTemplate>
        </asp:TabPanel>

        <asp:TabPanel runat="server" ID="TabPanel1" HeaderText="Assessment Dtls 2" Width="100%">
            <ContentTemplate>
                <fieldset>
                    <legend class="lblCaption1">Current Medications: </legend>
                    <asp:UpdatePanel ID="UpdatePanel16" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtCurrentMedications" runat="server" CssClass="label" Width="100%" Height="70px" TextMode="MultiLine"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </fieldset>
                <fieldset>
                    <legend class="lblCaption1">Specialized Assessment (as per patient main clinical complain) </legend>
                    <table style="width: 100%">
                        <tr>
                            <td class="lblCaption1" style="width: 100px"></td>
                            <td class="lblCaption1">
                                <asp:UpdatePanel ID="UpdatePanel22" runat="server">
                                    <ContentTemplate>
                                        <input type="radio" id="radElderly" name="radEldaly" runat="server" value="Elderly" /><label for="radElderly" class="lblCaption1">Elderly</label>
                                        <input type="radio" id="radFinding" name="radEldaly" runat="server" value="Finding" /><label for="radFinding" class="lblCaption1">finding</label>
                                        <asp:TextBox ID="txtFindingDesc" runat="server" CssClass="label" Width="300px"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>

                            </td>
                        </tr>
                        <tr>
                            <td class="lblCaption1" style="width: 100px"></td>
                            <td class="lblCaption1">
                                <asp:UpdatePanel ID="UpdatePanel23" runat="server">
                                    <ContentTemplate>
                                        <input type="radio" id="radSpecious" name="radSpecious" runat="server" value="Specious Of Abuse" /><label for="radSpecious" class="lblCaption1">Specious  Of Abuse </label>

                                    </ContentTemplate>
                                </asp:UpdatePanel>

                            </td>
                        </tr>
                        <tr>
                            <td class="lblCaption1" style="width: 100px"></td>
                            <td class="lblCaption1">
                                <asp:UpdatePanel ID="UpdatePanel24" runat="server">
                                    <ContentTemplate>
                                        <input type="radio" id="radAntenatal" name="radAntenatal" runat="server" value="Antenatal Patient" /><label for="radAntenatal" class="lblCaption1">Antenatal Patient</label>
                                        <asp:TextBox ID="txtAntenatalDesc" runat="server" CssClass="label" Width="340px"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>

                            </td>
                        </tr>
                        <tr>
                            <td class="lblCaption1" style="width: 100px">Pediatric
                            </td>
                            <td class="lblCaption1">
                                <asp:UpdatePanel ID="UpdatePanel25" runat="server">
                                    <ContentTemplate>
                                        <input type="radio" id="radVaccScheduke" name="radVaccScheduke" runat="server" value="Vaccination has Schedule" /><label for="radVaccScheduke" class="lblCaption1">Vaccination has Schedule</label>
                                        <input type="radio" id="radVaccNotKnown" name="radVaccScheduke" runat="server" value="Not Known" /><label for="radVaccNotKnown" class="lblCaption1">Not Known</label>
                                        <input type="radio" id="radVaccMissing" name="radVaccScheduke" runat="server" value="Missing Vaccination" /><label for="radVaccMissing" class="lblCaption1">Missing Vaccination</label>
                                        <asp:TextBox ID="txtVaccMissingDesc" runat="server" CssClass="label" Width="400px"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>

                            </td>
                        </tr>
                    </table>

                </fieldset>
                <fieldset>
                    <legend class="lblCaption1">Additional assessment is needed for geriatric  patient ( more than 65 Years , disabled patient, depended patient). </legend>
                    <asp:UpdatePanel ID="UpdatePanel17" runat="server">
                        <ContentTemplate>
                            <asp:RadioButtonList ID="radtxtAddAssmentNeeded" runat="server" CssClass="lblCaption1" RepeatDirection="Horizontal" Width="100px">
                                <asp:ListItem Text="No" Value="No"></asp:ListItem>
                                <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                            </asp:RadioButtonList>
                            <asp:TextBox ID="txtAddAssmentNeeded" runat="server" CssClass="label" Width="100%" Height="70px" TextMode="MultiLine"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </fieldset>
                <fieldset>
                    <legend class="lblCaption1">Plan of Care :( The goals to be achieved including short term and long term goals) </legend>
                    <table>
                        <tr>
                            <td class="lblCaption1">Measurable goals 
                            </td>
                            <td class="lblCaption1">Intervention 
                            </td>
                            <td class="lblCaption1">Evaluation 
                            </td>
                        </tr>
                        <tr>
                            <td class="lblCaption1">
                                <asp:UpdatePanel ID="UpdatePanel18" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtMeasurableGoals" runat="server" CssClass="label" Width="100%" Height="70px" TextMode="MultiLine"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td class="lblCaption1">
                                <asp:UpdatePanel ID="UpdatePanel19" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtIntervention" runat="server" CssClass="label" Width="100%" Height="70px" TextMode="MultiLine"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td class="lblCaption1">
                                <asp:UpdatePanel ID="UpdatePanel20" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtEvaluation" runat="server" CssClass="label" Width="100%" Height="70px" TextMode="MultiLine"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </fieldset>
                <fieldset>
                    <legend class="lblCaption1">Physical Therapy or others </legend>
                    <asp:UpdatePanel ID="UpdatePanel21" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtPhysicalTherapy" runat="server" CssClass="label" Width="100%" Height="70px" TextMode="MultiLine"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </fieldset>


            </ContentTemplate>
        </asp:TabPanel>
        <asp:TabPanel runat="server" ID="TabPanel2" HeaderText="History" Width="100%">
            <ContentTemplate>
                  <div style="padding-top: 0px; width: 100%; height: 400px; overflow: auto; border: thin; border-color: #f6f6f6; border-style: groove;">
                <asp:GridView ID="gvAssessmentHistory" runat="server" AllowSorting="True" AutoGenerateColumns="False" AllowPaging="true" PageSize="30"
                    EnableModelValidation="True" GridLines="None">
                    <HeaderStyle CssClass="GridHeader_Blue" Height="20px" Font-Bold="true" />
                        <RowStyle CssClass="GridRow" Height="20px" />

                    <Columns>
                        <asp:TemplateField HeaderText="EMR ID" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnklblEMRID" runat="server" OnClick="Edit_Click">
                                    <asp:Label ID="lblEMRID" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EIOA_ID") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Date" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnklblPhyDate" runat="server" OnClick="Edit_Click">
                                    <asp:Label ID="lblPhyDate" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("ModifiedDate") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="User" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkgvCreatedUser" runat="server" OnClick="Edit_Click">
                                    <asp:Label ID="lblgvCreatedUser" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("ModifiedUserName") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Functional" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnklblFunctional" runat="server" OnClick="Edit_Click">
                                    <asp:Label ID="lblFunctional" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EIOA_FUNCTIONAL") %>'></asp:Label>
                                </asp:LinkButton>


                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Assessment Required" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnklblRequired" runat="server" OnClick="Edit_Click">
                                    <asp:Label ID="lblRequired" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EIOA_ASSESSMENT_REQUIRED") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Appetite" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnklAppetite" runat="server" OnClick="Edit_Click">
                                    <asp:Label ID="lblAppetite" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EIOA_APPETITE") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Swallowing" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkSwallowing" runat="server" OnClick="Edit_Click">
                                    <asp:Label ID="lblSwallowing" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EIOA_SWALLOWING") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Dentition" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkDentition" runat="server" OnClick="Edit_Click">
                                    <asp:Label ID="lblDentition" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("EIOA_DENTITION") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                    </div> 
            </ContentTemplate>
        </asp:TabPanel>

    </asp:TabContainer>
    <br />
</asp:Content>
