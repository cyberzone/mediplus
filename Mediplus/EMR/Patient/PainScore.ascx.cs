﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using EMR_BAL;
using System.IO;


namespace Mediplus.EMR.Patient
{
    public partial class PainScore : System.Web.UI.UserControl
    {
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        public void BindPainScore()
        {

            string Criteria = " 1=1  AND EPM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
           // Criteria += " AND EPM_ID = '" + Convert.ToString(Session["EMR_ID"]) + "'";
            Criteria += " AND ( EPM_ID ='" + Convert.ToString(Session["EMR_ID"]) + "' OR CONVERT(DATE,EPM_DATE,103) = CONVERT(DATE,'" + Convert.ToString(Session["HPV_DATE"]) + "',103) )";

            DataSet ds = new DataSet();
            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();
            ds = objEmrPTMast.GetEMR_PTMaster(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                string strPainScore = "";
                strPainScore = Convert.ToString(ds.Tables[0].Rows[0]["EPM_PAINSCORE"]);

                txtLocation.Value = Convert.ToString(ds.Tables[0].Rows[0]["EPM_PAINSCORE_LOCATION"]);
                txtIntensity.Value = Convert.ToString(ds.Tables[0].Rows[0]["EPM_PAINSCORE_INTENSITY"]);
                txtCharacter.Value = Convert.ToString(ds.Tables[0].Rows[0]["EPM_PAINSCORE_CHARACTER"]);
                txtDuration.Value = Convert.ToString(ds.Tables[0].Rows[0]["EPM_PAINSCORE_DURATION"]);
                txtReferredTo.Value = Convert.ToString(ds.Tables[0].Rows[0]["EPM_PAINSCORE_OTHERS"]);


                if (strPainScore != "")
                {
                    if (strPainScore == "0")
                        PainValue0.Checked = true;
                    if (strPainScore == "1")
                        PainValue1.Checked = true;
                    if (strPainScore == "2")
                        PainValue2.Checked = true;
                    if (strPainScore == "3")
                        PainValue3.Checked = true;
                    if (strPainScore == "4")
                        PainValue4.Checked = true;
                    if (strPainScore == "5")
                        PainValue5.Checked = true;
                    if (strPainScore == "6")
                        PainValue6.Checked = true;
                    if (strPainScore == "7")
                        PainValue7.Checked = true;
                    if (strPainScore == "8")
                        PainValue8.Checked = true;
                    if (strPainScore == "9")
                        PainValue9.Checked = true;
                    if (strPainScore == "10")
                        PainValue10.Checked = true;

                }


            }


        }

        string fnSave()
        {
            string strPainScore = "";
            if (PainValue0.Checked == true)
                strPainScore = "0";
            if (PainValue1.Checked == true)
                strPainScore = "1";
            if (PainValue2.Checked == true)
                strPainScore = "2";
            if (PainValue3.Checked == true)
                strPainScore = "3";
            if (PainValue4.Checked == true)
                strPainScore = "4";
            if (PainValue5.Checked == true)
                strPainScore = "5";
            if (PainValue6.Checked == true)
                strPainScore = "6";
            if (PainValue7.Checked == true)
                strPainScore = "7";
            if (PainValue8.Checked == true)
                strPainScore = "8";
            if (PainValue9.Checked == true)
                strPainScore = "9";
            if (PainValue10.Checked == true)
                strPainScore = "10";

            string Criteria = " 1=1  AND EPM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND EPM_ID = '" + Convert.ToString(Session["EMR_ID"]) + "'";

            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();
            if (strPainScore == "")
            {
                objEmrPTMast.EPM_PAINSCORE = "null";

            }
            else
            {
                objEmrPTMast.EPM_PAINSCORE = strPainScore;
            }
            objEmrPTMast.EPM_PAINSCORE_LOCATION = txtLocation.Value.Trim();
            objEmrPTMast.EPM_PAINSCORE_INTENSITY = txtIntensity.Value.Trim();
            objEmrPTMast.EPM_PAINSCORE_CHARACTER = txtCharacter.Value.Trim();
            objEmrPTMast.EPM_PAINSCORE_DURATION = txtDuration.Value.Trim();
            objEmrPTMast.EPM_PAINSCORE_OTHERS = txtReferredTo.Value.Trim();

            objEmrPTMast.UpdateEMR_PTMasterPainScore(Criteria);

            return "";
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Convert.ToString(Session["EPM_STATUS"]).ToUpper() == "Y" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                {
                    btnSave.Visible = false;
                }
                //HIDE THE SAVE OPTION FOR NEXT DAY OF THE VISIT
                if (Convert.ToString(Session["EMRCurrentDateDiff"]) != "0" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                {
                    btnSave.Visible = false;
                }
                BindPainScore();
                if (GlobalValues.FileDescription == "HOLISTIC")
                {
                    divPainRIghtCont.Visible = false;

                }

            }
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                fnSave();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PainAssessment.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }



        }
    }
}