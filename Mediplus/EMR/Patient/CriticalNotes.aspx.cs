﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using EMR_BAL;


namespace Mediplus.EMR.Patient
{
    public partial class CriticalNotes : System.Web.UI.Page
    {
        CommonBAL objCom = new CommonBAL();

        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        public void BindEMR()
        {

            string Criteria = " 1=1  AND EPM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND EPM_ID = '" + Convert.ToString(Session["EMR_ID"]) + "'";    //BAL.GlobalValues.HomeCare_ID  + "' ";

            DataSet DS = new DataSet();
            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();
            DS = objEmrPTMast.GetEMR_PTMaster(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("EPM_CRITICAL_NOTES") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPM_CRITICAL_NOTES"]) != "")
                    //TreatmentPlan = clsWebReport.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["EPM_TREATMENT_PLAN"]));
                    txtCriticalNotes.Text = Convert.ToString(DS.Tables[0].Rows[0]["EPM_CRITICAL_NOTES"]);
                else
                    txtCriticalNotes.Text = "";



            }

        }


        public void BindCriticalNotesHistory()
        {

            string Criteria = " 1=1  AND EPM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND EPM_PT_ID = '" + Convert.ToString(Session["EMR_PT_ID"]) + "'";
            Criteria += " AND  EPM_CRITICAL_NOTES IS NOT NULL";


            DataSet DS = new DataSet();
            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();
            DS = objEmrPTMast.GetEMR_PTMaster(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvCriticalNotesHistory.DataSource = DS;
                gvCriticalNotesHistory.DataBind();

            }
            else
            {
                gvCriticalNotesHistory.DataBind();
            }

        }


        void BindAuditDtls()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1   and   EPM_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
            EMR_PTMasterBAL OBJptm = new EMR_PTMasterBAL();
            DS = OBJptm.GetEMR_PTMaster(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                lblCreatedBy.Text = Convert.ToString(DS.Tables[0].Rows[0]["EPM_CRETAED_USER"]);
                lblCreatedDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["CreatedDate"]);
                lblModifiedBy.Text = Convert.ToString(DS.Tables[0].Rows[0]["EPM_MODIFIED_USER"]);
                lblModifiedDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["ModifiedDate"]);
            }
        }

        void UpdateAuditDtls()
        {
            CommonBAL objCom = new CommonBAL();
            objCom.EMRPTMasterUserDtlsUpdate(Convert.ToString(Session["EMR_ID"]), Convert.ToString(Session["User_ID"]));
        }



        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../../Default.aspx?NoSession=1"); }

            if (!IsPostBack)
            {
                try
                {

                    if (Convert.ToString(Session["EPM_STATUS"]).ToUpper() == "Y" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                    {
                        txtCriticalNotes.Enabled = false;
                    }
                    //HIDE THE SAVE OPTION FOR NEXT DAY OF THE VISIT
                    if (Convert.ToString(Session["EMRCurrentDateDiff"]) != "0" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                    {
                        txtCriticalNotes.Enabled = false;
                    }

                    if (Convert.ToString(Session["EMR_DISPLAY_AUDIT"]) == "1")
                    {
                        BindAuditDtls();
                        divAuditDtls.Visible = true;
                    }

                    BindEMR();
                    BindCriticalNotesHistory();

                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      CriticalNotes.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }
            }
        }

        protected void txtCriticalNotes_TextChanged(object sender, EventArgs e)
        {
            try
            {
                string Criteria = " 1=1  AND EPM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
                Criteria += " AND EPM_ID = '" + Convert.ToString(Session["EMR_ID"]) + "'";
                EMR_PTMasterBAL objPTM = new EMR_PTMasterBAL();
                objPTM.EPM_CRITICAL_NOTES = txtCriticalNotes.Text.Trim();
                objPTM.UpdateEMR_PTMasterCriticalNotes(Criteria);
                BindCriticalNotesHistory();
                if (Convert.ToString(Session["EMR_DISPLAY_AUDIT"]) == "1")
                {
                    if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                    {
                        UpdateAuditDtls();
                    }
                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      CriticalNotes.txtCriticalNotes_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }

        }
    }
}