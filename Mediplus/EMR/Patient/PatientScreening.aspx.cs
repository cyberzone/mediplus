﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_BAL;

namespace Mediplus.EMR.Patient
{
    public partial class PatientScreening : System.Web.UI.Page
    {
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindVitalAudit()
        {

            string Criteria = " 1=1 ";
            Criteria += " AND EPV_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND EPV_PT_ID ='" + Convert.ToString(Session["EMR_PT_ID"]) + "'";

            // Criteria += " AND EPV_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
            DataSet DS = new DataSet();
            EMR_PTVitalsBAL objVit = new EMR_PTVitalsBAL();
            DS = objVit.EMRPTVitalsGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                lblCreatedBy.Text = Convert.ToString(DS.Tables[0].Rows[0]["CreatedUser"]);
                lblCreatedDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["CreatedDate"]);
                lblModifiedBy.Text = Convert.ToString(DS.Tables[0].Rows[0]["ModifiedUser"]);
                lblModifiedDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["ModifiedDate"]);
            }
        }

        void BindTemplate()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1  and ET_TYPE='PatientScreening'  ";
            Criteria += " AND ET_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' ";

            Criteria += " AND ( ET_DR_CODE='" + Convert.ToString(Session["HPV_DR_ID"]) + "' OR ET_Apply=1 )";

            if (GlobalValues.FileDescription.ToUpper() != "MAMPILLY")
            {
                Criteria += " AND ET_DEP_ID='" + Convert.ToString(Session["HPV_DEP_NAME"]) + "'";
            }
            drpTemplate.Items.Clear();

            DS = objCom.TemplatesGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                drpTemplate.DataSource = DS;
                drpTemplate.DataTextField = "ET_NAME";
                drpTemplate.DataValueField = "ET_CODE";
                drpTemplate.DataBind();
            }
            drpTemplate.Items.Insert(0, "Select Template");
            drpTemplate.Items[0].Value = "";

            // objCom.BindMasterDropdown(Criteria, out drpDossage);

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../../Default.aspx?NoSession=1"); }


            if (!IsPostBack)
            {
                BindTemplate();
                if (Convert.ToString(Session["EMR_DISPLAY_AUDIT"]) == "1")
                {

                    BindVitalAudit();
                    divAuditDtls.Visible = true;
                    tabPainAssess.Visible = true;
                    tabPainReAssess.Visible = true;
                }

            }
        }


        protected void btnSaveTemp_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }

            try
            {
                string TemplateCode = "";
                CommonBAL objCom = new CommonBAL();
                objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
                objCom.Type = "PatientScreening";
                objCom.Description = txtTemplateName.Value;
                objCom.TemplateData = "";
                if (chkOtherDr.Checked == true)
                {
                    objCom.AllDr = "1";
                }
                else
                {
                    objCom.AllDr = "0";
                }
                objCom.DR_ID = Convert.ToString(Session["HPV_DR_ID"]);
                objCom.DEP_ID = Convert.ToString(Session["HPV_DEP_NAME"]);
                TemplateCode = objCom.TemplatesAdd();

                EMR_PTVitalsBAL objPhar = new EMR_PTVitalsBAL();
                objPhar.EPV_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objPhar.EPV_ID = Convert.ToString(Session["EMR_ID"]);
                objPhar.TemplateCode = TemplateCode;
                objPhar.PatientScreeningTemplateAdd();
                BindTemplate();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientScreening.btnSaveTemp_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;
        }

        protected void btnDeleteTemp_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }

            try
            {

                CommonBAL objCom = new CommonBAL();

                string Criteria = "EPP_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND EPP_TEMPLATE_CODE='" + drpTemplate.SelectedValue + "'";
                objCom.fnDeleteTableData("EMR_PT_PROCEDURES_TEMPLATES", Criteria);



                string Criteria1 = "ET_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND ET_CODE='" + drpTemplate.SelectedValue + "' and ET_TYPE='PatientScreening'";
                objCom.fnDeleteTableData("EMR_TEMPLATES", Criteria1);


                BindTemplate();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientScreening.btnDeleteTemp_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;
        }

        protected void drpTemplate_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            try
            {


                if (drpTemplate.SelectedIndex != 0)
                {
                    EMR_PTVitalsBAL objPhar = new EMR_PTVitalsBAL();
                    objPhar.EPV_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                    objPhar.EPV_ID = Convert.ToString(Session["EMR_ID"]);
                    objPhar.EPV_PT_ID = Convert.ToString(Session["EMR_PT_ID"]);
                    objPhar.EPV_DATE = Convert.ToString(Session["HPV_DATE"]);

                    objPhar.TemplateCode = drpTemplate.SelectedValue;
                    objPhar.PatientScreeningAddFromTemplate();
                }



            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientScreening.drpTemplate_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;
        }
       
 

    }
}