﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using EMR_BAL;


namespace Mediplus.EMR.Patient
{
    public partial class History : System.Web.UI.Page
    {
        CommonBAL objCom = new CommonBAL();

        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        public void BindEMR()
        {
            DataSet DS = new DataSet();
            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();
            objEmrPTMast.EPM_PT_ID = Convert.ToString(Session["EMR_PT_ID"]);
            DS = objEmrPTMast.GetHistory();
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvEMR_PTMaster.DataSource = DS;
                gvEMR_PTMaster.DataBind();
            }
            else
            {
                gvEMR_PTMaster.DataBind();
            }

        }

        public void BindMenus()
        {
            DataSet DS = new DataSet();
            objCom = new CommonBAL();
            objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
            objCom.Type = "HIST_PRINT_MENU";
            objCom.DEP_ID = Convert.ToString(Session["HPV_DEP_NAME"]);

            DS = objCom.GetMenus();
            selReports.DataSource = DS;
            selReports.DataTextField = "MenuName";
            selReports.DataValueField = "MenuAction";
            selReports.DataBind();

        }

        void BindLaboratoryResult()
        {
            DataSet DS = new DataSet();
            objCom = new CommonBAL();
            DS = objCom.LaboratoryResultGet(Convert.ToString(Session["Branch_ID"]), hidFileNo.Value);
            if (DS.Tables[0].Rows.Count > 0)
            {
                string TransNo = "", RptType = "";

                TransNo = Convert.ToString(DS.Tables[0].Rows[0]["TransNo"]);
                RptType = Convert.ToString(DS.Tables[0].Rows[0]["ReportType"]);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LabResult", "LabResultReport('" + TransNo + "','" + RptType + "');", true);

            }
        }

        void BindRadiologyResult()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND RTR_PATIENT_ID='" + hidFileNo.Value + "'";
            Criteria += " AND RTR_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            DataSet DS = new DataSet();
            objCom = new CommonBAL();
            DS = objCom.RadiologyResultGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                string TransNo = "";
                TransNo = Convert.ToString(DS.Tables[0].Rows[0]["RTR_TRANS_NO"]);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Claim Report", "RadResultReport('" + TransNo + "');", true);

            }

        }

        void GetTotalPrescription()
        {
            string strFromDate = DateTime.Today.Year + "-" + DateTime.Today.Month + "-01 00:00:000";
            string strToDate = DateTime.Today.Year + "-" + DateTime.Today.Month + "-" + DateTime.DaysInMonth(DateTime.Today.Year, DateTime.Today.Month) + " 23:59:000 ";

            DataSet DS = new DataSet();
            string Criteria = " EPP_ID IN (SELECT  EPM_ID FROM EMR_PT_MASTER WHERE EPM_PT_ID='" + Convert.ToString(Session["EMR_PT_ID"]) + "' ";
            Criteria += " AND EPM_DATE >='" + strFromDate + "' AND  EPM_DATE <='" + strToDate + "')";

            DS = objCom.fnGetFieldValue(" COUNT(*) as TotalPresc ", "EMR_PT_PHARMACY ", Criteria, "");
            lblTotalPresc.Text = "0";
            if (DS.Tables[0].Rows.Count > 0)
            {
                lblTotalPresc.Text = Convert.ToString(DS.Tables[0].Rows[0]["TotalPresc"]);
            }


        }


        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../../Default.aspx?NoSession=1"); }

            try
            {
                if (!IsPostBack)
                {
                    BindEMR();
                    BindMenus();
                    if (GlobalValues.FileDescription.ToUpper() == "HOLISTIC")
                    {
                        tblTotalPresc.Visible = true;
                        GetTotalPrescription();
                    }
                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      History.Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void gvEMR_PTMaster_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //DropDownList gvdrpReports = (DropDownList)e.Row.FindControl("gvdrpReports");
                System.Web.UI.HtmlControls.HtmlSelect selReports = (System.Web.UI.HtmlControls.HtmlSelect)e.Row.FindControl("selReports");




                //selReports.DataSource = BindMenus();
                //selReports.DataTextField = "MenuName";
                //selReports.DataValueField = "MenuAction";
                //selReports.DataBind();

                if (hidPrintClicked.Value != "")
                {
                    //selReports.InnerText = hidPrintClicked.Value;
                }

            }
        }

        protected void gvbtnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                ImageButton btnDel = new ImageButton();
                btnDel = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnDel.Parent.Parent;

                //  System.Web.UI.HtmlControls.HtmlSelect selReports = (System.Web.UI.HtmlControls.HtmlSelect)gvScanCard.Cells[0].FindControl("selReports");

                string strControl = selReports.Value;// hidPrintClicked.Value;// gvdrpReports.SelectedValue;

                Label lblFileNumber = (Label)gvScanCard.Cells[0].FindControl("lblFileNumber");
                Label lblPatientMasterID = (Label)gvScanCard.Cells[0].FindControl("lblPatientMasterID");
                Label lblDoctorID = (Label)gvScanCard.Cells[0].FindControl("lblDoctorID");

                hidFileNo.Value = lblFileNumber.Text;

                string strRptPath = "";
                if (strControl.ToUpper() == "CLINICALSUMMARY" || strControl.ToUpper() == "DENTALSUMMARY")
                {
                    strRptPath = "../WebReports/ClinicalSummary.aspx";
                    string rptcall = @strRptPath + "?EMR_ID=" + lblPatientMasterID.Text + "&EMR_PT_ID=" + lblFileNumber.Text + "&DR_ID=" + lblDoctorID.Text;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ClinucalSum", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1');", true);
                }
                else if (strControl.ToUpper() == "PRESCRIPTION" || strControl.ToUpper() == "CRYSTALREPORT-PRESCRIPTION")
                {
                    if (GlobalValues.FileDescription.ToUpper() == "HOLISTIC")// || GlobalValues.FileDescription.ToUpper() == "MAMPILLY"
                    {

                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ClaimForm", "ShowPrescriptionPrintPDF('" + lblFileNumber.Text + "','" + Convert.ToString(Session["Branch_ID"]) + "','" + lblPatientMasterID.Text + "');", true);
                    }
                    else
                    {
                        strRptPath = "../WebReports/Prescription.aspx";
                        string rptcall = @strRptPath + "?EMR_ID=" + lblPatientMasterID.Text + "&EMR_PT_ID=" + lblFileNumber.Text + "&DR_ID=" + lblDoctorID.Text;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Presc", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1');", true);

                    }
                }

                if (strControl.ToUpper() == "CYCLOREFRACTION")
                {
                    strRptPath = "../WebReports/GlassPrescription.aspx";
                    string rptcall = @strRptPath + "?EMR_ID=" + lblPatientMasterID.Text + "&EMR_PT_ID=" + lblFileNumber.Text + "&DR_ID=" + lblDoctorID.Text;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ClinucalSum", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1');", true);
                }


                else if (strControl.ToUpper() == "RADIOLOGY")
                {
                    strRptPath = "../WebReports/Radiology.aspx";
                    string rptcall = @strRptPath + "?EMR_ID=" + lblPatientMasterID.Text + "&EMR_PT_ID=" + lblFileNumber.Text + "&DR_ID=" + lblDoctorID.Text;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Presc", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1');", true);

                }
                else if (strControl.ToUpper() == "LABORATORY")
                {
                    strRptPath = "../WebReports/Laboratory.aspx";
                    string rptcall = @strRptPath + "?EMR_ID=" + lblPatientMasterID.Text + "&EMR_PT_ID=" + lblFileNumber.Text + "&DR_ID=" + lblDoctorID.Text;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Presc", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1');", true);

                }
                else if (strControl.ToUpper() == "CLINICALNOTES")
                {
                    strRptPath = "../WebReports/ClinicalNotes.aspx";
                    string rptcall = @strRptPath + "?EMR_ID=" + lblPatientMasterID.Text + "&EMR_PT_ID=" + lblFileNumber.Text + "&DR_ID=" + lblDoctorID.Text;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Presc", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=700,width=1075,scrollbars=1');", true);


                }

                else if (strControl.ToUpper() == "CLINICALNOTESALL")
                {
                    strRptPath = "../WebReports/ClinicalNotesAll.aspx";
                    string rptcall = @strRptPath + "?EMR_ID=" + lblPatientMasterID.Text + "&EMR_PT_ID=" + lblFileNumber.Text + "&DR_ID=" + lblDoctorID.Text;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Presc", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=700,width=1075,scrollbars=1');", true);


                }
                else if (strControl.ToUpper() == "LABORATORY") //"WEBREPORTFORM-LABORATORY")
                {
                    BindLaboratoryResult();
                }
                else if (strControl.ToUpper() == "RADIOLOGY") // "WEBREPORTFORM-RADIOLOGY")
                {
                    BindRadiologyResult();
                }
                else if (strControl.ToUpper() == "UltrasoundSingle".ToUpper())
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "UltrasoundSingle", "ShowUltrasound('UltrasoundSingle','" + Convert.ToString(Session["EMR_PT_ID"]) + "','" + Convert.ToString(Session["Branch_ID"]) + "','" + Convert.ToString(Session["EMR_ID"]) + "');", true);

                }
                else if (strControl.ToUpper() == "UltrasoundTwins".ToUpper())
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "UltrasoundTwins", "ShowUltrasound('UltrasoundTwins','" + Convert.ToString(Session["EMR_PT_ID"]) + "','" + Convert.ToString(Session["Branch_ID"]) + "','" + Convert.ToString(Session["EMR_ID"]) + "');", true);

                }
                else if (strControl.ToUpper() == "UltrasoundTriplets".ToUpper())
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "UltrasoundTriplets", "ShowUltrasound('UltrasoundTriplets','" + Convert.ToString(Session["EMR_PT_ID"]) + "','" + Convert.ToString(Session["Branch_ID"]) + "','" + Convert.ToString(Session["EMR_ID"]) + "');", true);

                }
                else if (strControl.ToUpper() == "VISITSUMMAYALL" || strControl.ToUpper() == "VISITSUMMAYALL")
                {
                    strRptPath = "../WebReports/VisitDtlsSummaryAll.aspx";
                    string rptcall = @strRptPath + "?EMR_ID=" + Convert.ToString(Session["EMR_ID"]) + "&EMR_PT_ID=" + Convert.ToString(Session["EMR_PT_ID"]) + "&DR_ID=" + Convert.ToString(Session["HPV_DR_ID"]);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "VisitDtlsSummaryAll", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1');", true);
                }


                else if (strControl.ToUpper() == "PTBalanceHistory".ToUpper())
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "PTBalanceHistory", "ShowPTBalHistory('" + Convert.ToString(Session["EMR_PT_ID"]) + "','" + Convert.ToString(Session["Branch_ID"]) + "','" + Convert.ToString(Session["EMR_ID"]) + "');", true);

                }

                else if (strControl.ToUpper() == "OperationNotes".ToUpper())
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "OperationNotes", "ShowOperationNotes('" + Convert.ToString(Session["EMR_PT_ID"]) + "','" + Convert.ToString(Session["Branch_ID"]) + "','" + Convert.ToString(Session["EMR_ID"]) + "');", true);

                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      History.gvbtnPrint_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }  
        
    }
}