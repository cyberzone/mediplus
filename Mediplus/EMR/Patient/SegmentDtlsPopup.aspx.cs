﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using EMR_BAL;
using System.IO;

namespace Mediplus.EMR.Patient
{
    public partial class SegmentDtlsPopup : System.Web.UI.Page
    {
        public string SegmentDtslValue = "";

        public void SegmentVisitDtlsALLGet(string Criteria)
        {
            SegmentBAL objSeg = new SegmentBAL();

            string strSegmentType = "";
            string[] arrhidSegTypes = hidSegTypes_SavePT_ID.Value.Split('|');
            for (Int32 intSegType = 0; intSegType < arrhidSegTypes.Length; intSegType++)
            {
                strSegmentType += "'" + arrhidSegTypes[intSegType] + "',";
            }
            strSegmentType = strSegmentType.Substring(0, strSegmentType.Length - 1);

            DataSet DS = new DataSet();
            Criteria += " AND (  ESVD_ID='" + Convert.ToString(ViewState["EMR_ID"]) + "' OR ( ESVD_ID='" + Convert.ToString(ViewState["EMR_PT_ID"] ) + "' and ESVD_TYPE in ( " + strSegmentType + " ))) ";

            objSeg = new SegmentBAL();
            DS = objSeg.EMRSegmentVisitDtlsWithFieldNameAlias(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                ViewState["SegmentVisit"] = DS;
            }


        }


        void BindSegmentVisitDtls(string strType, string SegmentHeader, string ESVD_ID, out string strValue)
        {
            strValue = "";
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND ESVD_TYPE = '" + strType + "'";  // 'ROS' 'HIST_PAST';
            Criteria += " AND ESVD_ID = '" + ESVD_ID + "'";
            if (ViewState["SegmentVisit"] != "" && ViewState["SegmentVisit"] != null)
            {

                DS = (DataSet)ViewState["SegmentVisit"];

                if (DS.Tables[0].Rows.Count > 0)
                {
                    strValue = "<table style='width:100%;' >";
                    strValue += "<tr><td colspan='2' style=' border: 1px solid #dcdcdc;height:25px;width:20%;font-weight:bold;'>" + SegmentHeader + "</td></tr>";
                    DataRow[] result = DS.Tables[0].Select(Criteria);
                    foreach (DataRow DR in result)
                    {
                        strValue += "<tr><td style=' border: 1px solid #dcdcdc;height:25px;width:20%;'>" + Convert.ToString(DR["EST_FIELDNAME_ALIAS"]) + "</td>";
                        strValue += "<td style=' border: 1px solid #dcdcdc;height:25px;width:80%;'>" + Convert.ToString(DR["ESVD_VALUE"]) + "</td></tr>";

                    }


                    strValue += "</table>";

                }
            }

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ViewState["EMR_PT_ID"]  = Convert.ToString(Request.QueryString["EMR_PT_ID"]);
            ViewState["EMR_ID"] = Convert.ToString(Request.QueryString["EMR_ID"]);
            string Segment = Convert.ToString(Request.QueryString["SegmentType"]);
            string SegmentHeader = Convert.ToString(Request.QueryString["SegmentHeader"]);
            string Criteria = " 1=1 ";
            SegmentVisitDtlsALLGet(Criteria);


            string strValue = "";
            BindSegmentVisitDtls(Segment,SegmentHeader, Convert.ToString(ViewState["EMR_PT_ID"]), out  strValue);//"ALLERGY"
            SegmentDtslValue = strValue;
        }
    }
}