﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_BAL;
using System.Web.UI.HtmlControls;

namespace Mediplus.EMR.Patient
{
    public partial class PatientDashboard : System.Web.UI.Page
    {


        # region Variable Declaration

        CommonBAL objCom = new CommonBAL();
        private static SegmentBAL _segmentBAL = new SegmentBAL();

        public static string BranchID, EMR_ID, CCData, DR_ID;

        #endregion

        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        public DataSet GetMenus(string MenuType)
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
            objCom.Type = MenuType;
            objCom.DEP_ID = Convert.ToString(Session["HPV_DEP_NAME"]);

            DS = objCom.GetMenus();
            return DS;
        }

        public DataSet GetSegmentMaster(string SegmentType)
        {
            DataSet DS = new DataSet();

            string Criteria = " 1=1 AND ESM_STATUS=1 ";
            Criteria += " AND ESM_TYPE='" + SegmentType + "'";//HIST_PAST'";


            CommonBAL objCom = new CommonBAL();
            DS = _segmentBAL.SegmentMasterGet(Criteria);


            return DS;

        }

        public DataSet SegmentTemplatesGet(string Criteria)
        {
            DataSet DS = new DataSet();

            // string Criteria = " 1=1 ";
            //Criteria += " and EST_TYPE='VISIT_DETAILS_REMARKS'";


            CommonBAL objCom = new CommonBAL();
            DS = _segmentBAL.SegmentTemplatesGet(Criteria);


            return DS;

        }

        public void SegmentVisitDtlsGet(string Criteria, out string strValue, out string strValueYes)
        {
            strValue = "";
            strValueYes = "";
            DataSet DS = new DataSet();


            if (ViewState["SegmentVisit"] != "" && ViewState["SegmentVisit"] != null)
            {

                DS = (DataSet)ViewState["SegmentVisit"];
                DataRow[] result = DS.Tables[0].Select(Criteria);
                foreach (DataRow DR in result)
                {

                    strValue = Convert.ToString(DR["ESVD_VALUE"]);
                    strValueYes = Convert.ToString(DR["ESVD_VALUE_YES"]);
                }
            }



        }

        public void SegmentVisitDtlsALLGet(string Criteria)
        {
            string strSegmentType = "";
            string[] arrhidSegTypes = hidSegTypes_SavePT_ID.Value.Split('|');
            for (Int32 intSegType = 0; intSegType < arrhidSegTypes.Length; intSegType++)
            {
                strSegmentType += "'" + arrhidSegTypes[intSegType] + "',";
            }
            strSegmentType = strSegmentType.Substring(0, strSegmentType.Length - 1);

            DataSet DS = new DataSet();
            Criteria += " AND (  ESVD_ID='" + Convert.ToString(Session["EMR_ID"]) + "' OR ( ESVD_ID='" + Convert.ToString(Session["EMR_PT_ID"]) + "' and ESVD_TYPE in ( " + strSegmentType + " ))) ";

            CommonBAL objCom = new CommonBAL();
            DS = _segmentBAL.SegmentVisitDtlsGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                ViewState["SegmentVisit"] = DS;
            }


        }

        public string BindEMR()
        {
            string strCriticalNotes = "";
            DataSet DS = new DataSet();

            CommonBAL objCom = new CommonBAL();
            string Criteria = " EPH_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

            DS = objCom.fnGetFieldValue("EPH_CC", "EMR_PT_HISTORY", Criteria, "");
            if (DS.Tables[0].Rows.Count > 0)
            {
                ViewState["ChiefComplaints"] = Convert.ToString(DS.Tables[0].Rows[0]["EPH_CC"]);

            }

            return strCriticalNotes;
        }

        DataSet BindTemplateCC()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1  and ET_TYPE='CC'";
            Criteria += " AND ET_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' ";

            Criteria += " AND ( ET_DR_CODE='" + Convert.ToString(Session["HPV_DR_ID"]) + "' OR ET_Apply=1 )";

            if (GlobalValues.FileDescription.ToUpper() != "MAMPILLY")
            {
                Criteria += " AND ET_DEP_ID='" + Convert.ToString(Session["HPV_DEP_NAME"]) + "'";

            }

            DS = objCom.TemplatesGet(Criteria);
            return DS;

            // objCom.BindMasterDropdown(Criteria, out drpDossage);

        }

        string GetTemplateData()
        {

            DropDownList drp = (DropDownList)PlaceHolder1.FindControl("drpTepCC");
            string strData = "";
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1  and ET_TYPE='CC'";
            Criteria += " AND ET_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' ";

            //   Criteria += " AND ( ET_DR_CODE='" + Convert.ToString(Session["HPV_DR_ID"]) + "' OR ET_Apply=1 )";

            if (GlobalValues.FileDescription.ToUpper() != "MAMPILLY")
            {
                Criteria += " AND ET_DEP_ID='" + Convert.ToString(Session["HPV_DEP_NAME"]) + "'";
            }
            Criteria += " AND ET_CODE='" + drp.SelectedValue + "'";
            Criteria += " AND ET_NAME='" + drp.SelectedItem.Text + "'";


            DS = objCom.TemplatesGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                strData = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["ET_TEMPLATE"]));
            }

            return strData;
            // objCom.BindMasterDropdown(Criteria, out drpDossage);

        }

        void DeleteTemplateData()
        {

            DropDownList drp = (DropDownList)PlaceHolder1.FindControl("drpTepCC");
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1  and ET_TYPE='CC'";
            Criteria += " AND ET_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' ";

            Criteria += " AND ET_CODE='" + drp.SelectedValue + "'";
            Criteria += " AND ET_NAME='" + drp.SelectedItem.Text + "'";

            if (drp.SelectedItem.Text != "")
            {
                objCom.fnDeleteTableData("EMR_TEMPLATES", Criteria);
            }


        }

        public void BindVital(string FieldID, out string strValue, out string strValueYes)
        {
            strValue = "";
            strValueYes = "";



            string Criteria = " 1=1 ";
            Criteria += " AND EPV_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND EPV_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
            DataSet DS = new DataSet();
            EMR_PTVitalsBAL objVit = new EMR_PTVitalsBAL();
            DS = objVit.EMRPTVitalsGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {


                // txtHeight.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPV_HEIGHT"]);
                // txtBMI.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPV_BMI"]);
                // txtTemperatureF.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPV_TEMPERATURE"]);

                if (FieldID == "1")
                {
                    strValue = Convert.ToString(DS.Tables[0].Rows[0]["EPV_TEMPERATURE_C"]);
                }
                if (FieldID == "2")
                {
                    strValue = Convert.ToString(DS.Tables[0].Rows[0]["EPV_PULSE"]);
                }
                //  txtRespiration.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPV_RESPIRATION"]);

                if (FieldID == "3")
                {
                    string strBP, strBPSys = "", strBPDis = "";
                    strBPSys = Convert.ToString(DS.Tables[0].Rows[0]["EPV_BP_SYSTOLIC"]);
                    strBPDis = Convert.ToString(DS.Tables[0].Rows[0]["EPV_BP_DIASTOLIC"]);

                    strValue = strBPSys + " / " + strBPDis;

                }

                if (FieldID == "4")
                {
                    strValue = Convert.ToString(DS.Tables[0].Rows[0]["EPV_WEIGHT"]);
                }

                if (strValue != "")
                {
                    strValueYes = "Y";
                }
            }

        }

        void BindAuditDtls()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1   and   EPM_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
            EMR_PTMasterBAL OBJptm = new EMR_PTMasterBAL();
            DS = OBJptm.GetEMR_PTMaster(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                lblCreatedBy.Text = Convert.ToString(DS.Tables[0].Rows[0]["EPM_CRETAED_USER"]);
                lblCreatedDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["CreatedDate"]);
                lblModifiedBy.Text = Convert.ToString(DS.Tables[0].Rows[0]["EPM_MODIFIED_USER"]);
                lblModifiedDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["ModifiedDate"]);
            }
        }

        void UpdateAuditDtls()
        {
            CommonBAL objCom = new CommonBAL();
            objCom.EMRPTMasterUserDtlsUpdate(Convert.ToString(Session["EMR_ID"]), Convert.ToString(Session["User_ID"]));
            BindAuditDtls();
        }
 

        void BindTemplate()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1  and ET_TYPE='Dashboard'  ";
            Criteria += " AND ET_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' ";

            Criteria += " AND ( ET_DR_CODE='" + Convert.ToString(Session["HPV_DR_ID"]) + "' OR ET_Apply=1 )";

            if (GlobalValues.FileDescription.ToUpper() != "MAMPILLY")
            {
                Criteria += " AND ET_DEP_ID='" + Convert.ToString(Session["HPV_DEP_NAME"]) + "'";
            }
            drpTemplate.Items.Clear();

            DS = objCom.TemplatesGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                drpTemplate.DataSource = DS;
                drpTemplate.DataTextField = "ET_NAME";
                drpTemplate.DataValueField = "ET_CODE";
                drpTemplate.DataBind();
            }
            drpTemplate.Items.Insert(0, "Select Template");
            drpTemplate.Items[0].Value = "";

            // objCom.BindMasterDropdown(Criteria, out drpDossage);

        }
        #endregion

        #region AutoExtender
        [System.Web.Services.WebMethod]
        public static string[] GetSegmentValue(string prefixText, int count, string contextKey)
        {
            string SegType = "", SegSubType = "", SegFieldName = "";
            string[] strContKey = contextKey.Split('-');

            if (strContKey.Length > 0)
            {
                SegType = strContKey[0];
                SegSubType = strContKey[1];
                SegFieldName = strContKey[2];
            }

            string[] Data;
            string Criteria = " 1=1 ";

            // if (SegType.ToUpper() != "CC")
            // {
            Criteria += " AND ESVD_TYPE = '" + SegType + "' AND ESVD_SUBTYPE='" + SegSubType + "' AND ESVD_FIELDNAME='" + SegFieldName + "'";
            Criteria += " AND ESVD_VALUE Like '%" + prefixText + "%'";

            if (SegType.ToUpper() == "CC" && GlobalValues.FileDescription.ToUpper() != "SUMC")//GlobalValues.FileDescription.ToUpper() == "ALNOOR" &&
            {
                Criteria += " AND ESVD_ID IN  (SELECT CAST(EPM_ID AS VARCHAR) FROM EMR_PT_MASTER WHERE EPM_DR_CODE='" + DR_ID + "')";
            }

            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            DS = _segmentBAL.SegmentVisitDtlsTopGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = DS.Tables[0].Rows[i]["ESVD_VALUE"].ToString();
                }

                return Data;
            }
            //}
            //else
            //{
            //    Criteria += " AND EPH_CC Like '%" + prefixText + "%'";

            //    Criteria += " AND EPH_ID IN  (SELECT CAST(EPM_ID AS VARCHAR) FROM EMR_PT_MASTER WHERE EPM_DR_CODE='" + DR_ID + "')";


            //    DataSet DS = new DataSet();
            //    CommonBAL objCom = new CommonBAL();
            //    DS = objCom.EMR_PTHistoryGet(Criteria);
            //    if (DS.Tables[0].Rows.Count > 0)
            //    {
            //        Data = new string[DS.Tables[0].Rows.Count];
            //        for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
            //        {
            //            Data[i] = DS.Tables[0].Rows[i]["EPH_CC"].ToString();
            //        }

            //        return Data;
            //    }
            //}
            string[] Data1 = { "" };

            return Data1;
        }




        #endregion


        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../../Default.aspx?NoSession=1"); }


            try
            {

                if (!IsPostBack)
                {
                    // string script = "$(document).ready(function () { $('[id*=btnSave]').click(); });";
                    // ClientScript.RegisterStartupScript(this.GetType(), "load", script, true);

                    BindTemplate();
                    string SelectedTemplate = "";

                    SelectedTemplate = Convert.ToString(Request.QueryString["SelectedTemplate"]);

                    if (SelectedTemplate != "")
                    {
                        drpTemplate.SelectedValue = SelectedTemplate;
                    }


                    if (GlobalValues.FileDescription == "SUMMERLAND")
                    {
                        lblHeader.Text = "Patient History";
                    }


                    if (Convert.ToString(Session["EPM_STATUS"]).ToUpper() == "Y" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                    {
                        btnSave.Visible = false;
                    }
                    //HIDE THE SAVE OPTION FOR NEXT DAY OF THE VISIT
                    if (Convert.ToString(Session["EMRCurrentDateDiff"]) != "0" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                    {
                        btnSave.Visible = false;
                    }

                    BranchID = Convert.ToString(Session["Branch_ID"]);
                    EMR_ID = Convert.ToString(Session["EMR_ID"]);
                    DR_ID = Convert.ToString(Session["HPV_DR_ID"]);
                    BindEMR();
                    string Criteria = " 1=1 ";
                    SegmentVisitDtlsALLGet(Criteria);
                    if (Convert.ToString(Session["User_Category"]).ToUpper() == "NURSE" && Convert.ToString(Session["EPM_STATUS"]) == "Y")
                    {
                        btnSave.Visible = false;

                    }

                    if (GlobalValues.FileDescription.ToUpper() != "HOLISTIC")
                    {
                        if (Convert.ToString(Session["User_Category"]).ToUpper() == "NURSE")
                        {
                            PlaceHolder2.Visible = false;
                        }

                    }

                    if (Convert.ToString(Session["EPM_STATUS"]).ToUpper() == "Y" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                    {
                        drpTemplate.Enabled = false;


                    }
                    //HIDE THE SAVE OPTION FOR NEXT DAY OF THE VISIT
                    if (Convert.ToString(Session["EMRCurrentDateDiff"]) != "0" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                    {
                        drpTemplate.Enabled = false;

                    }

                    if (Convert.ToString(Session["EMR_VISIT_DTLS_EDIT_ALL_USERS"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "DOCTORS" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                    {
                        drpTemplate.Enabled = false;

                    }



                    if (Convert.ToString(Session["EMR_DISPLAY_AUDIT"]) == "1")
                    {
                        BindAuditDtls();
                        divAuditDtls.Visible = true;
                    }

                    if (Convert.ToString(Session["EMR_DISPLAY_TEMPLATE"]) == "1")
                    {
                        divTemplate.Visible = true;
                    }

                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientDashboard.Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            generateDynamicControls();
        }

        public void generateDynamicControls()
        {

            ViewState["control"] = "textbox";
            // DashBoardLeft();

            CreateSegCtrls1();
            CreateSegCtrls2();

        }

        public void CreateSegCtrls1()
        {
            string[] arrhidSegTypes = hidSegTypes.Value.Split('|');
            bool bolSplSubType = false;

            DataSet DSMenu = new DataSet();

            DSMenu = GetMenus("PORTLET_COL1");
            foreach (DataRow DRMenu in DSMenu.Tables[0].Rows)
            {

                HtmlGenericControl trHe1 = new HtmlGenericControl("tr");

                HtmlGenericControl tdHe1 = new HtmlGenericControl("td");
                // HtmlGenericControl tdHe2 = new HtmlGenericControl("td");
                tdHe1.Style.Add("border", "thin");
                tdHe1.Style.Add("border-color", "#F7F2F2");
                tdHe1.Style.Add("border-style", "groove");
                tdHe1.Style.Add("heigh", "50px");
                tdHe1.Style.Add("border-radius", "5px");

                //tdHe1.Style.Add("border-top-left-radius", "8px");
                //tdHe1.Style.Add("border-top-right-radius", "8px");



                Panel pnlHeader = new Panel();
                pnlHeader.ID = "pnl" + Convert.ToString(DRMenu["MenuName"]);
                pnlHeader.BorderStyle = BorderStyle.Groove;
                pnlHeader.BorderWidth = 0;
                pnlHeader.Style.Add("display", "none");
                pnlHeader.Style.Add("width", "99.5%");
                pnlHeader.Style.Add("border", "thin");
                pnlHeader.Style.Add("border-color", "#F7F2F2");//F7F2F2
                pnlHeader.Style.Add("border-style", "groove");
                pnlHeader.Style.Add("padding-bottom", " 20px");
                pnlHeader.Style.Add("border-radius", "5px");

                // pnlHeader.Style.Add("border-bottom-left-radius", "8px");
                // pnlHeader.Style.Add("border-bottom-right-radius", "8px");

                tdHe1.Style.Add("width", "100%");
                //tdHe2.Style.Add("width", "10%");


                Literal lit1 = new Literal();
                lit1.Text = "&nbsp;";

                TextBox lblHeader = new TextBox();
                lblHeader.Text = Convert.ToString(DRMenu["MenuName"]);
                lblHeader.CssClass = "lblCaption";
                lblHeader.Style.Add("font-family", "Segoe UI,arial");
                lblHeader.Style.Add("font-size", "13px");
                lblHeader.Style.Add("width", "95%");
                lblHeader.ReadOnly = true;
                lblHeader.BorderStyle = BorderStyle.None;

                HtmlAnchor Anc1 = new HtmlAnchor();
                Anc1.ID = "anc" + Convert.ToString(DRMenu["MenuName"]);
                Anc1.Title = "+";
                Anc1.InnerText = "+";
                Anc1.Style.Add("color", "#02a0e0");
                Anc1.Style.Add("font-weight", "bold");
                Anc1.Style.Add("font-family", "Arial Black");
                Anc1.Style.Add("font-size", "18px");
                // Anc1.Attributes.Add("onclick", "DivShow('" + pnlHeader.ID + "','" + Anc1.ID + "')");
                Anc1.Style.Add("width", "5%");

                tdHe1.Attributes.Add("onclick", "DivShow('" + pnlHeader.ID + "','" + Anc1.ID + "')");

                tdHe1.Controls.Add(lit1);
                tdHe1.Controls.Add(lblHeader);
                tdHe1.Controls.Add(Anc1);
                tdHe1.Style.Add("text-align", "left");

                trHe1.Controls.Add(tdHe1);


                trHe1.Controls.Add(tdHe1);

                // trHe1.Controls.Add(tdHe2);


                PlaceHolder1.Controls.Add(trHe1);

                HtmlGenericControl trHe2 = new HtmlGenericControl("tr");
                HtmlGenericControl tdHe21 = new HtmlGenericControl("td");


                DataSet DS = new DataSet();
                DS = GetSegmentMaster(Convert.ToString(DRMenu["MenuAction"]));
                // TextFileWriting("GetSegmentMaster  " + Convert.ToString(DS.Tables[0].Rows.Count));
                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    DataSet DS1 = new DataSet();

                    string Criteria1 = "1=1 AND EST_STATUS=1 ";
                    Criteria1 += "  AND (  EST_DEPARTMENT_ID LIKE '%|" + Convert.ToString(Session["HPV_DEP_ID"]) + "|%' OR EST_DEPARTMENT_ID ='' OR EST_DEPARTMENT_ID is null ) ";
                    Criteria1 += "  AND (  EST_DEPARTMENT_ID_HIDE NOT LIKE '%|" + Convert.ToString(Session["HPV_DEP_ID"]) + "|%' OR EST_DEPARTMENT_ID_HIDE ='' OR EST_DEPARTMENT_ID_HIDE is null ) ";
                    Criteria1 += " AND EST_TYPE='" + Convert.ToString(DR["ESM_TYPE"]) + "' AND EST_SUBTYPE='" + Convert.ToString(DR["ESM_SUBTYPE"]) + "'";
                    Criteria1 += " AND EST_FIELDNAME_ALIAS <> '' and EST_FIELDNAME_ALIAS IS NOT NULL";

                    DS1 = SegmentTemplatesGet(Criteria1);
                    bolSplSubType = false;
                    for (Int32 intSegType = 0; intSegType < arrhidSegTypes.Length; intSegType++)
                    {
                        if (Convert.ToString(DR["ESM_TYPE"]) == Convert.ToString(arrhidSegTypes[intSegType]))
                        {
                            bolSplSubType = true;
                        }

                    }

                    // TextFileWriting("Criteria1   " + Criteria1);
                    Int32 j = 1;
                    foreach (DataRow DR1 in DS1.Tables[0].Rows)
                    {
                        string strSegType = "|" + Convert.ToString(DR1["EST_TYPE"]) + "|";

                        string Criteria2 = "1=1 ";

                        string Type = "";
                        Type = Convert.ToString(DR1["EST_TYPE"]);

                        string strESVD_ID = "";
                        string[] arrhidSegTypesPTID = hidSegTypes_SavePT_ID.Value.Split('|');
                        for (Int32 intSegType = 0; intSegType < arrhidSegTypesPTID.Length; intSegType++)
                        {

                            if (Convert.ToString(DR["ESM_TYPE"]).ToUpper() == arrhidSegTypesPTID[intSegType].ToUpper())
                            {
                                strESVD_ID = "EMR_PT_ID";
                                goto forhidSegTypesPTID;
                            }
                        }
                    forhidSegTypesPTID: ;
                        if (strESVD_ID == "EMR_PT_ID")
                        {
                            Criteria2 += " AND ESVD_ID='" + Convert.ToString(Session["EMR_PT_ID"]) + "'";
                        }
                        else
                        {
                            Criteria2 += " AND ESVD_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
                        }


                        //if (Type.ToUpper() == "HIST_PAST" || Type.ToUpper() == "HIST_PERSONAL" || Type.ToUpper() == "HIST_SOCIAL" || Type.ToUpper() == "HIST_FAMILY" || Type.ToUpper() == "HIST_SURGICAL" || Type.ToUpper() == "ALLERGY")   // && SubType.ToUpper() == "IEN"
                        //{
                        //    Criteria2 += " AND ESVD_ID='" + Convert.ToString(Session["EMR_PT_ID"]) + "'";
                        //}
                        //else
                        //{
                        //    Criteria2 += " AND ESVD_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
                        //}

                        Criteria2 += " AND ESVD_TYPE='" + Convert.ToString(DR1["EST_TYPE"]) + "' AND ESVD_SUBTYPE='" + Convert.ToString(DR1["EST_SUBTYPE"]) + "' ";
                        Criteria2 += " AND ESVD_FIELDNAME='" + Convert.ToString(DR1["EST_FIELDNAME"]) + "'";

                        string TemplateType = "";
                        TemplateType = Convert.ToString(DR1["EST_TYPE"]) + "|" + Convert.ToString(DR1["EST_SUBTYPE"]) + "|" + Convert.ToString(DR1["EST_FIELDNAME"]);

                        // TextFileWriting("Criteria2   " + Criteria2);
                        string strValue = "", strValueYes = "";
                        SegmentVisitDtlsGet(Criteria2, out  strValue, out  strValueYes);


                        //HtmlGenericControl tr1 = new HtmlGenericControl("tr");

                        //HtmlGenericControl td1 = new HtmlGenericControl("td");
                        Label lbl = new Label();
                        lbl.ID = "lbl" + j + Convert.ToString(DR1["EST_TYPE"]) + "-" + Convert.ToString(DR1["EST_SUBTYPE"]) + "-" + Convert.ToString(DR1["EST_FIELDNAME"]).Replace("$", "");
                        lbl.Text = Convert.ToString(DR1["EST_FIELDNAME_ALIAS"]);
                        lbl.CssClass = "lblCaption1";



                        RadioButton rad1 = new RadioButton();
                        rad1.ID = "radNormal" + j + Convert.ToString(DR1["EST_TYPE"]) + "-" + Convert.ToString(DR1["EST_SUBTYPE"]) + "-" + Convert.ToString(DR1["EST_FIELDNAME"]).Replace("$", "");
                        rad1.Text = "No";
                        rad1.CssClass = "lblCaption1";
                        rad1.GroupName = "radNormal" + j + Convert.ToString(DR1["EST_TYPE"]) + "-" + Convert.ToString(DR1["EST_SUBTYPE"]) + "-" + Convert.ToString(DR1["EST_FIELDNAME"]).Replace("$", "");


                        RadioButton rad2 = new RadioButton();
                        rad2.ID = "radAbnormal" + j + Convert.ToString(DR1["EST_TYPE"]) + "-" + Convert.ToString(DR1["EST_SUBTYPE"]) + "-" + Convert.ToString(DR1["EST_FIELDNAME"]).Replace("$", "");
                        rad2.Text = "Yes";
                        rad2.CssClass = "lblCaption1";
                        rad2.GroupName = "radNormal" + j + Convert.ToString(DR1["EST_TYPE"]) + "-" + Convert.ToString(DR1["EST_SUBTYPE"]) + "-" + Convert.ToString(DR1["EST_FIELDNAME"]).Replace("$", "");



                        CheckBox chk1 = new CheckBox();
                        chk1.ID = "chkValueYesNo" + j + Convert.ToString(DR1["EST_TYPE"]) + "-" + Convert.ToString(DR1["EST_SUBTYPE"]) + "-" + Convert.ToString(DR1["EST_FIELDNAME"]).Replace("$", "");
                        chk1.Text = "";


                        Panel UpdatePanel10 = new Panel();

                        Literal ltTemp = new Literal();
                        ltTemp.Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";


                        Label lblTemp = new Label();
                        DropDownList drpTepCC = new DropDownList();
                        Button btnTempCCAdd = new Button();
                        Literal ltTempGap = new Literal();
                        Button btnTempCCDelete = new Button();

                        if (Convert.ToString(DR1["EST_TYPE"]).ToUpper() == "CC" && Convert.ToString(DR1["EST_SUBTYPE"]).ToUpper() == "GENERAL" && GlobalValues.FileDescription.ToUpper() != "SUMC")
                        {


                            lblTemp.Text = "Template";
                            lblTemp.CssClass = "lblCaption1";

                            drpTepCC.ID = "drpTepCC";
                            drpTepCC.DataSource = BindTemplateCC();
                            drpTepCC.DataTextField = "ET_NAME";
                            drpTepCC.DataValueField = "ET_CODE";
                            drpTepCC.DataBind();
                            drpTepCC.Items.Insert(0, "Select Template");
                            drpTepCC.Items[0].Value = "";
                            drpTepCC.Style.Add("width", "100px");
                            drpTepCC.CssClass = "TextBoxStyle";

                            drpTepCC.AutoPostBack = true;


                            btnTempCCAdd.Text = "Add";
                            // btnTempCCAdd.CssClass = "button orange small";
                            btnTempCCAdd.Style.Add("width", "50px");
                            //btnTempCCAdd.Style.Add("height", "20px");
                            //btnTempCCAdd.Style.Add("text-align", "left");
                            //btnTempCCAdd.Style.Add("color", "#fff");
                            //btnTempCCAdd.Style.Add("border", "solid 1px #da7c0c");
                            //btnTempCCAdd.Style.Add("background", "#f78d1d");
                            btnTempCCAdd.Style.Add("border-radius", "5px 5px 5px 5px");
                            btnTempCCAdd.CssClass = "gray";

                            ltTempGap.Text = "&nbsp;&nbsp;&nbsp;";

                            btnTempCCDelete.Text = "Delete";
                            // btnTempCCDelete.CssClass = "button orange small";
                            btnTempCCDelete.Style.Add("width", "50px");
                            //btnTempCCDelete.Style.Add("height", "20px");
                            //btnTempCCDelete.Style.Add("color", "#fff");
                            //btnTempCCDelete.Style.Add("border", "solid 1px #da7c0c");
                            //btnTempCCDelete.Style.Add("background", "#f78d1d");
                            btnTempCCDelete.Style.Add("border-radius", "5px 5px 5px 5px");
                            btnTempCCDelete.CssClass = "gray";

                            drpTepCC.SelectedIndexChanged += new EventHandler(drpTepCC_SelectedIndexChanged);

                            btnTempCCDelete.Click += new EventHandler(btnTempCCDelete_Click);

                            UpdatePanel10.Controls.Add(ltTemp);
                            UpdatePanel10.Controls.Add(lblTemp);
                            UpdatePanel10.Controls.Add(drpTepCC);
                            UpdatePanel10.Controls.Add(btnTempCCAdd);
                            UpdatePanel10.Controls.Add(ltTempGap);
                            UpdatePanel10.Controls.Add(btnTempCCDelete);

                        }




                        //td1.Controls.Add(lbl);
                        //tr1.Controls.Add(td1);
                        //PlaceHolder1.Controls.Add(tr1);

                        //HtmlGenericControl tr2 = new HtmlGenericControl("tr1");
                        //HtmlGenericControl td2 = new HtmlGenericControl("td");
                        TextBox txtBox = new TextBox();
                        txtBox.ID = "txt" + j + Convert.ToString(DR1["EST_TYPE"]) + "-" + Convert.ToString(DR1["EST_SUBTYPE"]) + "-" + Convert.ToString(DR1["EST_FIELDNAME"]).Replace("$", "");
                        txtBox.Text = strValue;
                        txtBox.TextMode = TextBoxMode.MultiLine;
                        txtBox.Style.Add("width", "96%");
                        txtBox.Height = 20;
                        txtBox.Style.Add("resize", "none");
                        txtBox.Attributes.Add("ondblclick", "MyScript('" + txtBox.ID + "','" + TemplateType + "')");
                        txtBox.Attributes.Add("autocomplete", "off");
                        txtBox.CssClass = "TextBoxStyle";

                        rad1.Attributes.Add("onChange", "BindSelectedValue('" + txtBox.ID + "','" + rad1.Text + "')");
                        rad2.Attributes.Add("onChange", "BindSelectedValue('" + txtBox.ID + "','" + rad2.Text + "')");
                        //td2.Controls.Add(txtBox);
                        //tr2.Controls.Add(td2);
                        // PlaceHolder1.Controls.Add(tr2);

                        //SegType = Convert.ToString(DR1["EST_TYPE"]);
                        //SegSubType = Convert.ToString(DR1["EST_SUBTYPE"]);
                        //SegFieldName = Convert.ToString(DR1["EST_FIELDNAME"]);

                        AjaxControlToolkit.AutoCompleteExtender autoCompleteExtender = new AjaxControlToolkit.AutoCompleteExtender();
                        autoCompleteExtender.TargetControlID = txtBox.ID;
                        autoCompleteExtender.ServiceMethod = "GetSegmentValue";
                        autoCompleteExtender.ID = j + Convert.ToString(DR1["EST_TYPE"]) + "-" + Convert.ToString(DR1["EST_SUBTYPE"]) + "-" + Convert.ToString(DR1["EST_FIELDNAME"]).Replace("$", "");
                        autoCompleteExtender.CompletionListCssClass = "AutoExtender";
                        autoCompleteExtender.CompletionListItemCssClass = "AutoExtenderList";
                        autoCompleteExtender.CompletionListHighlightedItemCssClass = "AutoExtenderHighlight";
                        // autoCompleteExtender.CompletionListElementID = "divwidth";
                        autoCompleteExtender.ContextKey = Convert.ToString(DR1["EST_TYPE"]) + "-" + Convert.ToString(DR1["EST_SUBTYPE"]) + "-" + Convert.ToString(DR1["EST_FIELDNAME"]).Replace("$", "");
                        autoCompleteExtender.UseContextKey = true;
                        autoCompleteExtender.CompletionInterval = 10;
                        autoCompleteExtender.CompletionSetCount = 15;
                        autoCompleteExtender.EnableCaching = true;
                        autoCompleteExtender.MinimumPrefixLength = 1;

                        if (hidYesNoboxRequired.Value.IndexOf(strSegType) != -1)
                        {
                            if (strValueYes != "")
                            {
                                if (strValueYes.ToUpper() == "N")
                                {
                                    rad1.Checked = true;
                                    chk1.Checked = false;
                                }
                                if (strValueYes.ToUpper() == "Y")
                                {
                                    rad2.Checked = true;
                                    chk1.Checked = true;
                                }

                            }

                        }

                        if (hidCheckboxRequired.Value.IndexOf(strSegType) != -1)
                        {
                            if (strValueYes != "")
                            {
                                if (strValueYes.ToUpper() == "N")
                                {
                                    chk1.Checked = false;
                                }
                                if (strValueYes.ToUpper() == "Y")
                                {
                                    chk1.Checked = true;
                                }

                            }
                            pnlHeader.Controls.Add(chk1);
                        }

                        Literal ltTemplbl = new Literal();
                        ltTemplbl.Text = "&nbsp;";

                        pnlHeader.Controls.Add(ltTemplbl);
                        pnlHeader.Controls.Add(lbl);

                        if (hidYesNoboxRequired.Value.IndexOf(strSegType) != -1)
                        {
                            pnlHeader.Controls.Add(rad1);
                            pnlHeader.Controls.Add(rad2);

                        }
                        if (Convert.ToString(DR1["EST_TYPE"]).ToUpper() == "CC" && Convert.ToString(DR1["EST_SUBTYPE"]).ToUpper() == "GENERAL" && GlobalValues.FileDescription.ToUpper() != "SUMC")
                        {
                            //   pnlHeader.Controls.Add(ltTemp);
                            //   pnlHeader.Controls.Add(lblTemp);
                            //pnlHeader.Controls.Add(drpTepCC);
                            //  pnlHeader.Controls.Add(btnTempCCAdd);
                            // pnlHeader.Controls.Add(ltTempGap);
                            //  pnlHeader.Controls.Add(btnTempCCDelete);
                            pnlHeader.Controls.Add(UpdatePanel10);

                        }



                        Literal ltTemptxt = new Literal();
                        ltTemptxt.Text = "&nbsp;&nbsp;";

                        pnlHeader.Controls.Add(ltTemptxt);
                        pnlHeader.Controls.Add(txtBox);
                        pnlHeader.Controls.Add(autoCompleteExtender);
                        Literal ltllblGap = new Literal();
                        ltllblGap.Text = "<br /><br />";

                        pnlHeader.Controls.Add(ltllblGap);

                        if (Convert.ToString(DR1["EST_TYPE"]).ToUpper() == "CC" && Convert.ToString(DR1["EST_SUBTYPE"]).ToUpper() == "GENERAL" && GlobalValues.FileDescription.ToUpper() != "SUMC")
                        {
                            Literal lt = new Literal();
                            lt.Text = "<br /><br />";
                            pnlHeader.Controls.Add(lt);
                            Panel UpdatePanel3 = new Panel();
                            TextBox txtBoxCC = new TextBox();
                            txtBoxCC.ID = "txtCriticalNotes";// +Convert.ToString(DR1["EST_TYPE"]) + "-" + Convert.ToString(DR1["EST_SUBTYPE"]) + "-" + Convert.ToString(DR1["EST_FIELDNAME"]);
                            txtBoxCC.Text = Convert.ToString(ViewState["ChiefComplaints"]);
                            txtBoxCC.TextMode = TextBoxMode.MultiLine;
                            txtBoxCC.Style.Add("width", "96%");
                            txtBoxCC.Height = 200;
                            txtBoxCC.CssClass = "TextBoxStyle";
                            txtBoxCC.Style.Add("resize", "none");
                            UpdatePanel3.Controls.Add(txtBoxCC);
                            pnlHeader.Controls.Add(UpdatePanel3);

                            //txtBox_TextChanged("txtBox", new EventArgs());

                            txtBox.Attributes.Add("onkeypress", "return BindCC(event,'" + txtBox.ID + "')");
                            btnTempCCAdd.Attributes.Add("onclick", "TemplatePopupShow('" + txtBoxCC.ID + "','CC')");


                        }


                        tdHe21.Controls.Add(pnlHeader);
                        trHe2.Controls.Add(tdHe21);
                        PlaceHolder1.Controls.Add(trHe2);

                        //HtmlGenericControl trHe3 = new HtmlGenericControl("tr");
                        //HtmlGenericControl tdHe3 = new HtmlGenericControl("td");
                        //tdHe3.Style.Add("height","10px");
                        //trHe3.Controls.Add(tdHe3);
                        //PlaceHolder1.Controls.Add(trHe3);


                        //pnlTop.Controls.Add(trHe1);
                        //pnlTop.Controls.Add(trHe2);
                        //tdTop1.Controls.Add(pnlTop);
                        //trTop.Controls.Add(tdTop1);
                        //PlaceHolder1.Controls.Add(trTop);



                        j = j + 1;
                    }


                }

                HtmlGenericControl trHe3 = new HtmlGenericControl("tr");
                HtmlGenericControl tdHe3 = new HtmlGenericControl("td");
                tdHe3.Style.Add("height", "15px");
                trHe3.Controls.Add(tdHe3);
                PlaceHolder1.Controls.Add(trHe3);



            }
        }

        public void CreateSegCtrls2()
        {
            string[] arrhidSegTypes = hidSegTypes.Value.Split('|');
            bool bolSplSubType = false;



            DataSet DSMenu = new DataSet();

            DSMenu = GetMenus("PORTLET_COL2");
            foreach (DataRow DRMenu in DSMenu.Tables[0].Rows)
            {



                HtmlGenericControl trHe1 = new HtmlGenericControl("tr");

                HtmlGenericControl tdHe1 = new HtmlGenericControl("td");
                // HtmlGenericControl tdHe2 = new HtmlGenericControl("td");

                tdHe1.Style.Add("border", "thin");
                tdHe1.Style.Add("border-color", "#F7F2F2");
                tdHe1.Style.Add("border-style", "groove");
                tdHe1.Style.Add("heigh", "50px");
                tdHe1.Style.Add("border-radius", "5");
                tdHe1.Style.Add("vertical-align", "text-top");
                tdHe1.Style.Add("border-radius", "5px");

                Panel pnlHeader = new Panel();
                pnlHeader.ID = "pnl" + Convert.ToString(DRMenu["MenuName"]);
                pnlHeader.BorderStyle = BorderStyle.Groove;
                pnlHeader.BorderWidth = 0;
                pnlHeader.Style.Add("display", "none");
                pnlHeader.Style.Add("width", "99.5%");
                pnlHeader.Style.Add("border", "thin");
                pnlHeader.Style.Add("border-color", "#F7F2F2");
                pnlHeader.Style.Add("border-style", "groove");
                pnlHeader.Style.Add("padding-bottom", " 20px");

                pnlHeader.Style.Add("border-radius", "5px");
                tdHe1.Style.Add("width", "100%");
                //tdHe2.Style.Add("width", "10%");


                Literal lit1 = new Literal();
                lit1.Text = "&nbsp;";



                //tdHe2.Style.Add("text-align", "right");


                TextBox lblHeader = new TextBox();
                lblHeader.Text = Convert.ToString(DRMenu["MenuName"]);
                lblHeader.CssClass = "lblCaption";
                lblHeader.Style.Add("font-family", "Segoe UI,arial");
                lblHeader.Style.Add("font-size", "13px");

                lblHeader.ReadOnly = true;
                lblHeader.BorderStyle = BorderStyle.None;
                if (Convert.ToString(DRMenu["MenuName"]).ToUpper() == "PHYSICAL EXAM")
                {
                    lblHeader.Style.Add("width", "65%");
                }
                else
                {
                    lblHeader.Style.Add("width", "95%");
                }



                HtmlAnchor Anc1 = new HtmlAnchor();
                Anc1.ID = "anc" + Convert.ToString(DRMenu["MenuName"]);
                Anc1.Title = "+";
                Anc1.InnerText = "+";
                Anc1.Style.Add("color", "#02a0e0");
                Anc1.Style.Add("font-weight", "bold");
                Anc1.Style.Add("font-family", "Arial Black");
                Anc1.Style.Add("font-size", "18px");
                // Anc1.Attributes.Add("onclick", "DivShow('" + pnlHeader.ID + "','" + Anc1.ID + "')");
                Anc1.Style.Add("width", "5%");

                tdHe1.Attributes.Add("onclick", "DivShow('" + pnlHeader.ID + "','" + Anc1.ID + "')");

                tdHe1.Controls.Add(lit1);
                tdHe1.Controls.Add(lblHeader);
                if (Convert.ToString(DRMenu["MenuName"]).ToUpper() == "PHYSICAL EXAM")
                {
                    Literal ltTemp = new Literal();
                    ltTemp.Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                    tdHe1.Controls.Add(ltTemp);

                    DropDownList ExaminationType = new DropDownList();
                    ExaminationType.ID = "ExaminationType";
                    if (GlobalValues.FileDescription.ToUpper() == "ALAMAL")
                    {
                        ExaminationType.Items.Insert(0, "1995 Guidelines");
                        ExaminationType.Items[0].Value = "1995";
                        ExaminationType.Items.Insert(1, "1997 Guidelines");
                        ExaminationType.Items[1].Value = "1997";
                    }
                    else
                    {
                        ExaminationType.Items.Insert(0, "1997 Guidelines");
                        ExaminationType.Items[0].Value = "1997";
                        ExaminationType.Items.Insert(1, "1995 Guidelines");
                        ExaminationType.Items[1].Value = "1995";
                    }
                    ExaminationType.CssClass = "TextBoxStyle";
                    tdHe1.Controls.Add(ExaminationType);
                }
                tdHe1.Controls.Add(Anc1);


                trHe1.Controls.Add(tdHe1);
                ///  trHe1.Controls.Add(tdHe2);


                PlaceHolder2.Controls.Add(trHe1);

                HtmlGenericControl trHe2 = new HtmlGenericControl("tr");

                HtmlGenericControl tdHe21 = new HtmlGenericControl("td");



                DataSet DS = new DataSet();
                DS = GetSegmentMaster(Convert.ToString(DRMenu["MenuAction"]));
                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    DataSet DS1 = new DataSet();
                    string Criteria1 = "1=1 AND EST_STATUS=1 ";
                    Criteria1 += "  AND (  EST_DEPARTMENT_ID LIKE '%|" + Convert.ToString(Session["HPV_DEP_ID"]) + "|%' OR EST_DEPARTMENT_ID ='' OR EST_DEPARTMENT_ID is null ) ";
                    Criteria1 += "  AND (  EST_DEPARTMENT_ID_HIDE NOT LIKE '%|" + Convert.ToString(Session["HPV_DEP_ID"]) + "|%' OR EST_DEPARTMENT_ID_HIDE ='' OR EST_DEPARTMENT_ID_HIDE is null ) ";
                    Criteria1 += " AND EST_TYPE='" + Convert.ToString(DR["ESM_TYPE"]) + "' AND EST_SUBTYPE='" + Convert.ToString(DR["ESM_SUBTYPE"]) + "'";
                    Criteria1 += " AND EST_FIELDNAME_ALIAS <> '' and EST_FIELDNAME_ALIAS IS NOT NULL";

                    DS1 = SegmentTemplatesGet(Criteria1);
                    bolSplSubType = false;
                    for (Int32 intSegType = 0; intSegType < arrhidSegTypes.Length; intSegType++)
                    {
                        if (Convert.ToString(DR["ESM_TYPE"]) == Convert.ToString(arrhidSegTypes[intSegType]))
                        {
                            bolSplSubType = true;
                        }

                    }




                    if (DS1.Tables[0].Rows.Count > 0)
                    {



                        Panel pnlSubHeader = new Panel();
                        pnlSubHeader.ID = "pnl" + Convert.ToString(DRMenu["MenuName"]) + "-" + Convert.ToString(DR["ESM_SUBTYPE"]);
                        pnlSubHeader.BorderStyle = BorderStyle.Groove;
                        pnlSubHeader.BorderWidth = 1;
                        pnlSubHeader.Style.Add("display", "none");
                        pnlSubHeader.Style.Add("width", "100%");
                        //  pnlSubHeader.Style.Add("hight", "70px");
                        pnlSubHeader.Style.Add("padding-bottom", "30px");
                     

                        Label lblSubHeader = new Label();
                        lblSubHeader.Text = Convert.ToString(DR["ESM_SUBTYPE_ALIAS"]);
                        lblSubHeader.CssClass = "lblCaption1";
                        lblSubHeader.Style.Add("font-family", "Segoe UI,arial");
                        lblSubHeader.Style.Add("font-size", "13px");
                        lblSubHeader.Height = 28;
                        lblSubHeader.Style.Add("cursor", "pointer");
                        if (GlobalValues.FileDescription.ToUpper() == "HOLISTIC" && Convert.ToString(DRMenu["MenuName"]).ToUpper() == "PHYSICAL EXAM")
                        {
                            lblSubHeader.Style.Add("width", "75%");
                        }
                        else
                        {

                            lblSubHeader.Style.Add("width", "98%");
                        }

                        lblSubHeader.Style.Add("color", "#ffffff");
                       // lblSubHeader.Style.Add("background-image", "url(../Images/ui-bg_flat_75_02a0e0_40x100.PNG)");
                        lblSubHeader.Style.Add("background-color", "#666");


                        //Label lblSubHeaderGap = new Label();
                        //lblSubHeaderGap.Text = "";
                        //lblSubHeaderGap.Height = 3;
                        //lblSubHeaderGap.Style.Add("width", "100%");
                        //lblSubHeaderGap.Style.Add("background-color", "#ffffff");

                        lblSubHeader.Attributes.Add("onclick", "SubHeaderShow('" + pnlSubHeader.ID + "')");


                        CheckBox chkNotApp = new CheckBox();
                        chkNotApp.ID = "chkSubGrpNotApp" + Convert.ToString(DR["ESM_TYPE"]) + "-" + Convert.ToString(DR["ESM_SUBTYPE"]);
                        chkNotApp.Text = "Not Applicable";
                        chkNotApp.CssClass = "lblCaption1";



                        if (bolSplSubType == true)
                        {

                            // pnlHeader.Controls.Add(imgSubHeader);
                            //Literal ltllbl = new Literal();
                            //ltllbl.Text = "&nbsp;";
                            //pnlHeader.Controls.Add(ltllbl);
                            pnlHeader.Controls.Add(lblSubHeader);


                            if (GlobalValues.FileDescription.ToUpper() == "HOLISTIC" && Convert.ToString(DRMenu["MenuName"]).ToUpper() == "PHYSICAL EXAM")
                            {
                                chkNotApp.Checked = true;

                                string CriteriaNA = "1=1 ";
                                string strESVD_ID = "";
                                string[] arrhidSegTypesPTID = hidSegTypes_SavePT_ID.Value.Split('|');
                                for (Int32 intSegType = 0; intSegType < arrhidSegTypesPTID.Length; intSegType++)
                                {

                                    if (Convert.ToString(DR["ESM_TYPE"]).ToUpper() == arrhidSegTypesPTID[intSegType].ToUpper())
                                    {
                                        strESVD_ID = "EMR_PT_ID";
                                        goto forhidSegTypesPTID;
                                    }
                                }
                            forhidSegTypesPTID: ;

                                if (strESVD_ID == "EMR_PT_ID")
                                {
                                    CriteriaNA += " AND ESVD_ID='" + Convert.ToString(Session["EMR_PT_ID"]) + "'";
                                }
                                else
                                {
                                    CriteriaNA += " AND ESVD_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
                                }



                                // CriteriaNA += " AND ESVD_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
                                CriteriaNA += " AND ESVD_TYPE='" + Convert.ToString(DR["ESM_TYPE"]) + "' AND ESVD_SUBTYPE='" + Convert.ToString(DR["ESM_SUBTYPE"]) + "' ";
                                CriteriaNA += " AND ESVD_FIELDNAME IS NULL ";
                                string strNAValue = "", strNAValueYes = "";
                                SegmentVisitDtlsGet(CriteriaNA, out  strNAValue, out  strNAValueYes);


                                // chkNotApp.Height = 27;
                                // chkNotApp.Style.Add("color", "#ffffff");
                                // chkNotApp.Style.Add("background-image", "url(../Images/ui-bg_flat_75_02a0e0_40x100.PNG)");

                                if (strNAValueYes != "")
                                {
                                    if (strNAValueYes.ToUpper() == "N")
                                    {
                                        chkNotApp.Checked = false;
                                    }
                                    if (strNAValueYes.ToUpper() == "Y")
                                    {
                                        chkNotApp.Checked = true;
                                    }

                                }


                                pnlHeader.Controls.Add(chkNotApp);
                            }
                           // pnlHeader.Controls.Add(lblSubHeaderGap);


                        }

                        string strTextBoxIDs = "";

                        Int32 j = 1;
                        foreach (DataRow DR1 in DS1.Tables[0].Rows)
                        {
                            string Criteria2 = "1=1 ";

                            string strESVD_ID = "";
                            string[] arrhidSegTypesPTID = hidSegTypes_SavePT_ID.Value.Split('|');
                            for (Int32 intSegType = 0; intSegType < arrhidSegTypesPTID.Length; intSegType++)
                            {

                                if (Convert.ToString(DR["ESM_TYPE"]).ToUpper() == arrhidSegTypesPTID[intSegType].ToUpper())
                                {
                                    strESVD_ID = "EMR_PT_ID";
                                    goto forhidSegTypesPTID1;
                                }
                            }
                        forhidSegTypesPTID1: ;

                            if (strESVD_ID == "EMR_PT_ID")
                            {
                                Criteria2 += " AND ESVD_ID='" + Convert.ToString(Session["EMR_PT_ID"]) + "'";
                            }
                            else
                            {
                                Criteria2 += " AND ESVD_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
                            }


                            // Criteria2 += " AND ESVD_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
                            Criteria2 += " AND ESVD_TYPE='" + Convert.ToString(DR1["EST_TYPE"]) + "' AND ESVD_SUBTYPE='" + Convert.ToString(DR1["EST_SUBTYPE"]) + "' ";
                            Criteria2 += " AND ESVD_FIELDNAME='" + Convert.ToString(DR1["EST_FIELDNAME"]) + "'";

                            string strSegType = "|" + Convert.ToString(DR1["EST_TYPE"]) + "|";

                            string TemplateType = "";
                            TemplateType = Convert.ToString(DR1["EST_TYPE"]) + "|" + Convert.ToString(DR1["EST_SUBTYPE"]) + "|" + Convert.ToString(DR1["EST_FIELDNAME"]);

                            string strValue = "", strValueYes = "";
                            SegmentVisitDtlsGet(Criteria2, out  strValue, out  strValueYes);

                            if (Convert.ToString(DR1["EST_TYPE"]) == "PE" && Convert.ToString(DR1["EST_SUBTYPE"]) == "CONS")
                            {
                                if (strValue == "")
                                {
                                    if (Convert.ToString(DR1["EST_FIELDNAME"]) == "1" || Convert.ToString(DR1["EST_FIELDNAME"]) == "2" || Convert.ToString(DR1["EST_FIELDNAME"]) == "3" || Convert.ToString(DR1["EST_FIELDNAME"]) == "4")
                                    {

                                        BindVital(Convert.ToString(DR1["EST_FIELDNAME"]), out  strValue, out  strValueYes);
                                    }

                                }
                            }

                            HtmlGenericControl tr1 = new HtmlGenericControl("tr");
                            HtmlGenericControl td1 = new HtmlGenericControl("td");


                            Label lbl = new Label();
                            lbl.ID = "lbl" + j + Convert.ToString(DR1["EST_TYPE"]) + "-" + Convert.ToString(DR1["EST_SUBTYPE"]) + "-" + Convert.ToString(DR1["EST_FIELDNAME"]).Replace("$", "");
                            lbl.Text = Convert.ToString(DR1["EST_FIELDNAME_ALIAS"]);
                            lbl.CssClass = "lblCaption1";
                            lbl.Width = 250;
                            // lbl.Height=50;
                            // lbl.Style.Add("width", "60%");

                            CheckBox chk1 = new CheckBox();
                            chk1.ID = "chkValueYesNo" + j + Convert.ToString(DR1["EST_TYPE"]) + "-" + Convert.ToString(DR1["EST_SUBTYPE"]) + "-" + Convert.ToString(DR1["EST_FIELDNAME"]).Replace("$", "");
                            chk1.Text = "";


                            CheckBox chkNA = new CheckBox();
                            chkNA.ID = "chkNA" + j + Convert.ToString(DR1["EST_TYPE"]) + "-" + Convert.ToString(DR1["EST_SUBTYPE"]) + "-" + Convert.ToString(DR1["EST_FIELDNAME"]).Replace("$", "");
                            chkNA.Text = "Not Applicable";
                            chkNA.CssClass = "lblCaption1";



                            RadioButton rad1 = new RadioButton();
                            rad1.ID = "radNormal" + j + Convert.ToString(DR1["EST_TYPE"]) + "-" + Convert.ToString(DR1["EST_SUBTYPE"]) + "-" + Convert.ToString(DR1["EST_FIELDNAME"]).Replace("$", "");
                            rad1.Text = "Normal";
                            rad1.CssClass = "lblCaption1";
                            rad1.GroupName = "radNormal" + j + Convert.ToString(DR1["EST_TYPE"]) + "-" + Convert.ToString(DR1["EST_SUBTYPE"]) + "-" + Convert.ToString(DR1["EST_FIELDNAME"]).Replace("$", "");

                            if (GlobalValues.FileDescription.ToUpper() == "ALANAMEL")
                            {
                                rad1.Checked = true;
                            }

                            RadioButton rad2 = new RadioButton();
                            rad2.ID = "radAbnormal" + j + Convert.ToString(DR1["EST_TYPE"]) + "-" + Convert.ToString(DR1["EST_SUBTYPE"]) + "-" + Convert.ToString(DR1["EST_FIELDNAME"]).Replace("$", "");
                            rad2.Text = "Abnormal";
                            rad2.CssClass = "lblCaption1";
                            rad2.GroupName = "radNormal" + j + Convert.ToString(DR1["EST_TYPE"]) + "-" + Convert.ToString(DR1["EST_SUBTYPE"]) + "-" + Convert.ToString(DR1["EST_FIELDNAME"]).Replace("$", "");


                            td1.Controls.Add(lbl);
                            tr1.Controls.Add(td1);
                            PlaceHolder2.Controls.Add(tr1);

                            HtmlGenericControl tr2 = new HtmlGenericControl("tr1");
                            HtmlGenericControl td2 = new HtmlGenericControl("td");
                            TextBox txtBox = new TextBox();
                            txtBox.ID = "txt" + j + Convert.ToString(DR1["EST_TYPE"]) + "-" + Convert.ToString(DR1["EST_SUBTYPE"]) + "-" + Convert.ToString(DR1["EST_FIELDNAME"]).Replace("$", "");
                            txtBox.Text = strValue;
                            txtBox.TextMode = TextBoxMode.MultiLine;
                            txtBox.Style.Add("width", "98%");
                            txtBox.Height = 30;
                            txtBox.Style.Add("resize", "none");
                            txtBox.Attributes.Add("ondblclick", "MyScript2('" + txtBox.ID + "','" + TemplateType + "')");
                            txtBox.CssClass = "TextBoxStyle";
                            rad1.Attributes.Add("onChange", "BindSelectedValue('" + txtBox.ID + "','" + rad1.Text + "')");
                            rad2.Attributes.Add("onChange", "BindSelectedValue('" + txtBox.ID + "','" + rad2.Text + "')");
                            AjaxControlToolkit.AutoCompleteExtender autoCompleteExtender = new AjaxControlToolkit.AutoCompleteExtender();
                            autoCompleteExtender.TargetControlID = txtBox.ID;
                            autoCompleteExtender.ServiceMethod = "GetSegmentValue";
                            autoCompleteExtender.ID = j + Convert.ToString(DR1["EST_TYPE"]) + "-" + Convert.ToString(DR1["EST_SUBTYPE"]) + "-" + Convert.ToString(DR1["EST_FIELDNAME"]).Replace("$", "");
                            autoCompleteExtender.CompletionListCssClass = "AutoExtender";
                            autoCompleteExtender.CompletionListItemCssClass = "AutoExtenderList";
                            autoCompleteExtender.CompletionListHighlightedItemCssClass = "AutoExtenderHighlight";
                            // autoCompleteExtender.CompletionListElementID = "divwidth";
                            autoCompleteExtender.ContextKey = Convert.ToString(DR1["EST_TYPE"]) + "-" + Convert.ToString(DR1["EST_SUBTYPE"]) + "-" + Convert.ToString(DR1["EST_FIELDNAME"]).Replace("$", "");
                            autoCompleteExtender.UseContextKey = true;

                            // autoCompleteExtender.ServicePath = "YourAutoCompleteWebService.asmx";
                            autoCompleteExtender.CompletionInterval = 10;
                            autoCompleteExtender.CompletionSetCount = 15;
                            autoCompleteExtender.EnableCaching = true;
                            autoCompleteExtender.MinimumPrefixLength = 1;

                            if (GlobalValues.FileDescription.ToUpper() == "HOLISTIC" && Convert.ToString(DRMenu["MenuName"]).ToUpper() == "PHYSICAL EXAM")
                            {
                                strTextBoxIDs += txtBox.ID + "|";

                                if (chkNotApp.Checked == true)
                                {
                                    txtBox.Enabled = false;
                                }

                            }
                            chkNA.Attributes.Add("onclick", "HideTextBox('" + txtBox.ID + "','" + chkNA.ID + "')");


                            if (hidNormalAbnormalText.Value == "true")
                            {
                                rad1.Attributes.Add("onclick", "BindRadioValue('" + txtBox.ID + "','" + rad1.Text + "')");
                                rad2.Attributes.Add("onclick", "BindRadioValue('" + txtBox.ID + "','" + rad2.Text + "')");
                            }

                            td2.Controls.Add(txtBox);
                            td2.Controls.Add(autoCompleteExtender);
                            tr2.Controls.Add(td2);
                            // PlaceHolder1.Controls.Add(tr2);

                            //Literal ltllblGap = new Literal();
                            //ltllblGap.Text = "<br /><br />";

                            //tr2.Controls.Add(ltllblGap);







                            if (bolSplSubType == true)
                            {


                                if (strValueYes != "")
                                {
                                    if (strValueYes.ToUpper() == "N")
                                    {
                                        rad1.Checked = true;
                                        chk1.Checked = false;
                                    }
                                    if (strValueYes.ToUpper() == "Y")
                                    {
                                        rad2.Checked = true;
                                        chk1.Checked = true;
                                    }

                                }


                                if (hidCheckboxRequired.Value.IndexOf(strSegType) != -1)
                                {
                                    pnlSubHeader.Controls.Add(chk1);

                                }
                                pnlSubHeader.Controls.Add(lbl);

                                /*
                                if (GlobalValues.FileDescription.ToUpper() == "HOLISTIC" && Convert.ToString(DRMenu["MenuName"]).ToUpper() == "PHYSICAL EXAM")
                                {
                                    if (strValue == "NA")
                                    {
                                        chkNA.Checked = true;
                                        txtBox.Enabled = false;
                                    }
                                    pnlSubHeader.Controls.Add(chkNA);
                                }
                                */


                                if (hidCheckboxRequired.Value.IndexOf(strSegType) == -1)
                                {
                                    pnlSubHeader.Controls.Add(rad1);
                                    pnlSubHeader.Controls.Add(rad2);
                                }

                                pnlSubHeader.Controls.Add(txtBox);
                                pnlSubHeader.Controls.Add(autoCompleteExtender);
                                pnlHeader.Controls.Add(pnlSubHeader);
                            }
                            else
                            {
                                if (hidCheckboxRequired.Value.IndexOf(strSegType) != -1)
                                {
                                    if (strValueYes != "")
                                    {
                                        if (strValueYes.ToUpper() == "N")
                                        {
                                            chk1.Checked = false;
                                        }
                                        if (strValueYes.ToUpper() == "Y")
                                        {
                                            chk1.Checked = true;
                                        }

                                    }
                                    pnlHeader.Controls.Add(chk1);
                                }

                                pnlHeader.Controls.Add(lbl);
                                pnlHeader.Controls.Add(txtBox);
                                pnlHeader.Controls.Add(autoCompleteExtender);
                            }


                            tdHe21.Controls.Add(pnlHeader);
                            trHe2.Controls.Add(tdHe21);



                            PlaceHolder2.Controls.Add(trHe2);

                            //HtmlGenericControl trHe3 = new HtmlGenericControl("tr");

                            //HtmlGenericControl tdHe3 = new HtmlGenericControl("td");
                            //tdHe3.Style.Add("height", "1px");
                            //trHe3.Controls.Add(tdHe3);
                            //PlaceHolder2.Controls.Add(trHe3);

                            j = j + 1;
                        }

                        if (strTextBoxIDs != "")
                        {
                            strTextBoxIDs = strTextBoxIDs.Substring(0, strTextBoxIDs.Length - 1);
                            chkNotApp.Attributes.Add("onclick", "HideAllTextBox('" + strTextBoxIDs + "','" + chkNotApp.ID + "')");
                        }

                    }


                }

                HtmlGenericControl trHe3 = new HtmlGenericControl("tr");
                HtmlGenericControl tdHe3 = new HtmlGenericControl("td");
                tdHe3.Style.Add("height", "20px");
                trHe3.Controls.Add(tdHe3);
                PlaceHolder2.Controls.Add(trHe3);
            }
        }

        void fnSave(string Type, string SubType, string FieldName, string Value, string ValueYes, string ValueNA)
        {
            if (ValueYes == "" && ValueNA == "")
            {
                ValueYes = "Y";
            }
            SegmentBAL objSeg = new SegmentBAL();
            objSeg.BranchID = Convert.ToString(Session["Branch_ID"]);

            string strESVD_ID = "";
            string[] arrhidSegTypesPTID = hidSegTypes_SavePT_ID.Value.Split('|');
            for (Int32 intSegType = 0; intSegType < arrhidSegTypesPTID.Length; intSegType++)
            {

                if (Type.ToUpper() == arrhidSegTypesPTID[intSegType].ToUpper())
                {
                    strESVD_ID = "EMR_PT_ID";
                }
            }

            if (strESVD_ID == "EMR_PT_ID")
            {
                objSeg.EMRID = Convert.ToString(Session["EMR_PT_ID"]);
            }
            else
            {
                objSeg.EMRID = Convert.ToString(Session["EMR_ID"]);
            }


            //if (Type.ToUpper() == "HIST_PAST" || Type.ToUpper() == "HIST_PERSONAL" || Type.ToUpper() == "HIST_SOCIAL" || Type.ToUpper() == "HIST_FAMILY" || Type.ToUpper() == "HIST_SURGICAL" || Type.ToUpper() == "ALLERGY")   // && SubType.ToUpper() == "IEN"
            //{
            //    objSeg.EMRID = Convert.ToString(Session["EMR_PT_ID"]);
            //}
            //else
            //{
            //    objSeg.EMRID = Convert.ToString(Session["EMR_ID"]);
            //}


            objSeg.Type = Type;
            objSeg.SubType = SubType;

            objSeg.FieldName = FieldName;
            objSeg.Value = Value;
            objSeg.ValueYes = ValueYes;
            objSeg.TemplateCode = "";
            objSeg.EMRSegmentVisitDtlsAdd();
        FunEnd: ;
        }

        void DeleteSegmentDtlsByEMR_ID(string SegmentType)
        {
            SegmentBAL objSeg = new SegmentBAL();

            string Criteria = " 1=1 ";
            Criteria += " AND  ESVD_BRANCH_ID= '" + Convert.ToString(Session["Branch_ID"]) + "' AND  ESVD_ID= '" + Convert.ToString(Session["EMR_ID"]) + "' AND  ESVD_TYPE= '" + SegmentType + "' ";
            objSeg.EMRSegmentVisitDtlsDelete(Criteria);
        }

        void DeleteSegmentDtlsByPatientID(string SegmentType)
        {
            SegmentBAL objSeg = new SegmentBAL();
            string Criteria = " 1=1 ";
            Criteria += " AND  ESVD_BRANCH_ID= '" + Convert.ToString(Session["Branch_ID"]) + "' AND  ESVD_ID= '" + Convert.ToString(Session["EMR_PT_ID"]) + "' AND  ESVD_TYPE= '" + SegmentType + "' ";

            objSeg.EMRSegmentVisitDtlsDelete(Criteria);
        }

        void SaveLoop(DataSet DSMenu)
        {
            foreach (DataRow DRMenu in DSMenu.Tables[0].Rows)
            {
                string Type = "";
                Type = Convert.ToString(DRMenu["MenuAction"]);

                if (Type.ToUpper() == "HIST_PAST" || Type.ToUpper() == "HIST_PERSONAL" || Type.ToUpper() == "HIST_SOCIAL" || Type.ToUpper() == "HIST_FAMILY" || Type.ToUpper() == "HIST_SURGICAL" || Type.ToUpper() == "ALLERGY")   // && SubType.ToUpper() == "IEN"
                {
                    DeleteSegmentDtlsByPatientID(Type.ToUpper());
                }
                else
                {
                    DeleteSegmentDtlsByEMR_ID(Convert.ToString(DRMenu["MenuAction"]));
                }


                DataSet DS = new DataSet();
                DS = GetSegmentMaster(Convert.ToString(DRMenu["MenuAction"]));
                foreach (DataRow DR in DS.Tables[0].Rows)
                {


                    DataSet DS1 = new DataSet();
                    // string Criteria1 = "1=1 AND EST_STATUS=1 and (EST_DEPARTMENT_ID ='' OR EST_DEPARTMENT_ID LIKE '%" + Convert.ToString(Session["HPV_DEP_NAME"]) + "%') ";
                    //  Criteria1 += " AND EST_TYPE='" + Convert.ToString(DR["ESM_TYPE"]) + "' AND EST_SUBTYPE='" + Convert.ToString(DR["ESM_SUBTYPE"]) + "'";

                    string Criteria1 = "1=1 AND EST_STATUS=1 ";
                    Criteria1 += "  AND (  EST_DEPARTMENT_ID LIKE '%|" + Convert.ToString(Session["HPV_DEP_ID"]) + "|%' OR EST_DEPARTMENT_ID ='' OR EST_DEPARTMENT_ID is null ) ";
                    Criteria1 += "  AND (  EST_DEPARTMENT_ID_HIDE NOT LIKE '%|" + Convert.ToString(Session["HPV_DEP_ID"]) + "|%' OR EST_DEPARTMENT_ID_HIDE ='' OR EST_DEPARTMENT_ID_HIDE is null ) ";
                    Criteria1 += " AND EST_TYPE='" + Convert.ToString(DR["ESM_TYPE"]) + "' AND EST_SUBTYPE='" + Convert.ToString(DR["ESM_SUBTYPE"]) + "'";
                    Criteria1 += " AND EST_FIELDNAME_ALIAS <> '' and EST_FIELDNAME_ALIAS IS NOT NULL";
                    DS1 = SegmentTemplatesGet(Criteria1);

                    if (Convert.ToString(DRMenu["MenuName"]).ToUpper() == "PHYSICAL EXAM" && GlobalValues.FileDescription.ToUpper() == "HOLISTIC")
                    {
                        string chkNotAppValue = "N";
                        CheckBox chkNotApp = (CheckBox)PlaceHolder1.FindControl("chkSubGrpNotApp" + Convert.ToString(DR["ESM_TYPE"]) + "-" + Convert.ToString(DR["ESM_SUBTYPE"]));
                        if (chkNotApp != null)
                        {
                            if (chkNotApp.Checked == true)
                            {
                                chkNotAppValue = "Y";
                            }
                        }

                        if (chkNotAppValue != "")
                        {
                            fnSave(Convert.ToString(DR["ESM_TYPE"]), Convert.ToString(DR["ESM_SUBTYPE"]), "", "NA", chkNotAppValue, "");
                        }



                    }

                    Int32 j = 1;
                    foreach (DataRow DR1 in DS1.Tables[0].Rows)
                    {
                        string Value = "", ValueYes = "", ValueNA = "";
                        TextBox txt = (TextBox)PlaceHolder1.FindControl("txt" + j + Convert.ToString(DR1["EST_TYPE"]) + "-" + Convert.ToString(DR1["EST_SUBTYPE"]) + "-" + Convert.ToString(DR1["EST_FIELDNAME"]).Replace("$", ""));
                        if (txt != null)
                        {

                            Value = txt.Text.Trim();

                        }

                        CheckBox chk1 = (CheckBox)PlaceHolder1.FindControl("chkValueYesNo" + j + Convert.ToString(DR1["EST_TYPE"]) + "-" + Convert.ToString(DR1["EST_SUBTYPE"]) + "-" + Convert.ToString(DR1["EST_FIELDNAME"]).Replace("$", ""));

                        CheckBox chkNA = (CheckBox)PlaceHolder1.FindControl("chkNA" + j + Convert.ToString(DR1["EST_TYPE"]) + "-" + Convert.ToString(DR1["EST_SUBTYPE"]) + "-" + Convert.ToString(DR1["EST_FIELDNAME"]).Replace("$", ""));
                        if (chkNA != null)
                        {
                            if (chkNA.Checked == true)
                            {
                                ValueNA = "Y";
                            }
                        }


                        RadioButton rad1 = (RadioButton)PlaceHolder1.FindControl("radNormal" + j + Convert.ToString(DR1["EST_TYPE"]) + "-" + Convert.ToString(DR1["EST_SUBTYPE"]) + "-" + Convert.ToString(DR1["EST_FIELDNAME"]).Replace("$", ""));
                        RadioButton rad2 = (RadioButton)PlaceHolder1.FindControl("radAbnormal" + j + Convert.ToString(DR1["EST_TYPE"]) + "-" + Convert.ToString(DR1["EST_SUBTYPE"]) + "-" + Convert.ToString(DR1["EST_FIELDNAME"]).Replace("$", ""));
                        if (rad1 != null)
                        {
                            if (rad1.Checked == true)
                            {
                                ValueYes = "N";
                            }
                        }
                        if (rad2 != null)
                        {
                            if (rad2.Checked == true)
                            {
                                ValueYes = "Y";
                            }

                        }
                        if (chk1 != null)
                        {
                            if (chk1.Checked == true)
                            {
                                ValueYes = "Y";
                            }
                        }


                        if (Value != "" || ValueYes != "")
                        {
                            fnSave(Convert.ToString(DR["ESM_TYPE"]), Convert.ToString(DR1["EST_SUBTYPE"]), Convert.ToString(DR1["EST_FIELDNAME"]), Value, ValueYes, ValueNA);
                        }
                        j = j + 1;
                    }

                }

            }
        }

        public void SaveChiefComplaints()
        {
            DataSet DS = new DataSet();
            TextBox txtCriticalNotes = (TextBox)PlaceHolder1.FindControl("txtCriticalNotes");

            CommonBAL objCom = new CommonBAL();
            // string Criteria = " EPH_BRANCH_ID='"+  Convert.ToString(Session["Branch_ID"]) +"' AND  EPH_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";


            //DS = objCom.fnGetFieldValue("EPH_CC", "EMR_PT_HISTORY", Criteria,"");

            //if (DS.Tables[0].Rows.Count > 0)
            //{
            //    string FieldNameWithValues = "  EPH_CC='" + txtCriticalNotes.Text + "'";
            //    objCom.fnUpdateTableData(FieldNameWithValues, "EMR_PT_HISTORY", Criteria);
            //}
            //else
            //{
            //    string FieldNam = " EPH_BRANCH_ID,EPH_ID, EPH_CC ";
            //    string Values = "'" + Convert.ToString(Session["Branch_ID"]) + "','" + Convert.ToString(Session["EMR_ID"]) + "','" + txtCriticalNotes.Text + "'";
            //    objCom.fnInsertTableData(FieldNam, Values, "EMR_PT_HISTORY");
            //}

            objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
            objCom.EMRID = Convert.ToString(Session["EMR_ID"]);
            objCom.EPH_CC = txtCriticalNotes.Text;
            objCom.EMR_PTHistoryAdd();


        }

        void SaveEMRS()
        {
            DropDownList ExaminationType = (DropDownList)PlaceHolder1.FindControl("ExaminationType");


            string Criteria = " 1=1 ";
            Criteria += " AND EPM_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
            EMRSBAL objEMRS = new EMRSBAL();
            objEMRS.BranchID = Convert.ToString(Session["Branch_ID"]);
            objEMRS.EMRID = Convert.ToString(Session["EMR_ID"]);
            if (ExaminationType != null)
            {
                objEMRS.examinationtype = ExaminationType.SelectedValue;
            }
            else
            {
                objEMRS.examinationtype = "1997";
            }

            objEMRS.SaveEMRS();

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                System.Threading.Thread.Sleep(5000);

                DataSet DSMenu = new DataSet();

                DSMenu = GetMenus("PORTLET_COL1");
                SaveLoop(DSMenu);

                DSMenu = GetMenus("PORTLET_COL2");
                SaveLoop(DSMenu);
                if (GlobalValues.FileDescription.ToUpper() != "SUMC")
                {
                    SaveChiefComplaints();
                }
                SaveEMRS();

                if (Convert.ToString(Session["EMR_DISPLAY_AUDIT"]) == "1")
                {
                    if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                    {
                        UpdateAuditDtls();
                    }
                }
                

                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowMessage()", true);

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientDashboard.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void txtBox_TextChanged(object sender, EventArgs e)
        {
            try
            {
                TextBox txtBox = (TextBox)PlaceHolder1.FindControl("txtBox");
                TextBox txtCriticalNotes = (TextBox)PlaceHolder1.FindControl("txtCriticalNotes");


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientDashboard.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void drpTepCC_SelectedIndexChanged(object sender, EventArgs e)
        {
            TextBox txtCC = (TextBox)PlaceHolder1.FindControl("txtCriticalNotes");
            txtCC.Text = GetTemplateData();

            // ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "DivShowCC();", true);


        }

        protected void btnTempCCDelete_Click(object sender, EventArgs e)
        {

            DeleteTemplateData();

        }


        public void DashBoardLeft()
        {
            string[] arrhidSegTypes = hidSegTypes.Value.Split('|');
            bool bolSplSubType = false;

            DataSet DSMenu = new DataSet();
            DSMenu = GetMenus("PORTLET_COL1");
            foreach (DataRow DRMenu in DSMenu.Tables[0].Rows)
            {
                HtmlGenericControl trTop = new HtmlGenericControl("tr");
                HtmlGenericControl tdTop1 = new HtmlGenericControl("td");
                //tdTop1.Style.Add("border", "thin");
                //tdTop1.Style.Add("border-color", "#cccccc");
                //tdTop1.Style.Add("border-style", "groove");
                //tdTop1.Style.Add("heigh", "50px");



                Panel pnlTop = new Panel();
                pnlTop.BorderStyle = BorderStyle.Groove;
                pnlTop.BorderWidth = 1;
                pnlTop.Style.Add("width", "100%");
                pnlTop.Visible = true;
                pnlTop.Height = 50;




                HtmlGenericControl trHe1 = new HtmlGenericControl("tr");

                HtmlGenericControl tdHe1 = new HtmlGenericControl("td");
                HtmlGenericControl tdHe2 = new HtmlGenericControl("td");

                tdHe1.Style.Add("border", "thin");
                tdHe1.Style.Add("border-color", "#cccccc");
                tdHe1.Style.Add("border-style", "groove");
                tdHe1.Style.Add("heigh", "50px");

                tdHe2.Style.Add("border", "thin");
                tdHe2.Style.Add("border-color", "#cccccc");
                tdHe2.Style.Add("border-style", "groove");
                tdHe2.Style.Add("heigh", "50px");



                Panel pnlHeader = new Panel();
                pnlHeader.ID = "pnl" + Convert.ToString(DRMenu["MenuName"]);
                pnlHeader.BorderStyle = BorderStyle.Groove;
                pnlHeader.BorderWidth = 0;
                pnlHeader.Style.Add("display", "none");
                pnlHeader.Style.Add("width", "100%");


                tdHe1.Style.Add("width", "90%");
                tdHe2.Style.Add("width", "10%");

                Label lblHeader = new Label();
                lblHeader.Text = Convert.ToString(DRMenu["MenuName"]);
                lblHeader.CssClass = "lblCaption";
                lblHeader.Style.Add("font-family", "Segoe UI,arial");
                lblHeader.Style.Add("font-size", "13px");


                HtmlAnchor Anc1 = new HtmlAnchor();
                Anc1.ID = "anc" + Convert.ToString(DRMenu["MenuName"]);
                Anc1.Title = "+";
                Anc1.InnerText = "+";
                Anc1.Style.Add("color", "#02a0e0");
                Anc1.Style.Add("font-weight", "bold");
                Anc1.Style.Add("font-family", "Arial Black");
                Anc1.Style.Add("font-size", "18px");
                Anc1.Attributes.Add("onclick", "DivShow('" + pnlHeader.ID + "','" + Anc1.ID + "')");

                tdHe1.Attributes.Add("onclick", "DivShow('" + pnlHeader.ID + "','" + Anc1.ID + "')");


                tdHe1.Controls.Add(lblHeader);
                tdHe2.Controls.Add(Anc1);
                tdHe2.Style.Add("text-align", "right");

                trHe1.Controls.Add(tdHe1);
                trHe1.Controls.Add(tdHe2);


                PlaceHolder1.Controls.Add(trHe1);

                HtmlGenericControl trHe2 = new HtmlGenericControl("tr");
                HtmlGenericControl tdHe21 = new HtmlGenericControl("td");


                DataSet DS = new DataSet();
                DS = GetSegmentMaster(Convert.ToString(DRMenu["MenuAction"]));

                trHe1.Controls.Add(tdHe1);
                trHe1.Controls.Add(tdHe2);
                //  pnlTop.Controls.Add(trHe1);

                tdTop1.Controls.Add(trHe1);
                trTop.Controls.Add(tdTop1);
                PlaceHolder1.Controls.Add(trTop);

            }
        }


        protected void btnSaveTemp_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }

            try
            {
                string TemplateCode = "";
                CommonBAL objCom = new CommonBAL();
                objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
                objCom.Type = "Dashboard";
                objCom.Description = txtTemplateName.Value;
                objCom.TemplateData = "";
                if (chkOtherDr.Checked == true)
                {
                    objCom.AllDr = "1";
                }
                else
                {
                    objCom.AllDr = "0";
                }
                objCom.DR_ID = Convert.ToString(Session["HPV_DR_ID"]);
                objCom.DEP_ID = Convert.ToString(Session["HPV_DEP_NAME"]);
                TemplateCode = objCom.TemplatesAdd();


                SegmentBAL objPro = new SegmentBAL();
                objPro.BranchID = Convert.ToString(Session["Branch_ID"]);
                objPro.EMRID = Convert.ToString(Session["EMR_ID"]);
                objPro.TemplateCode = TemplateCode;
                objPro.SegmentVisitTemplateAdd();
                BindTemplate();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientDashboard.btnSaveTemp_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;
        }

        protected void btnDeleteTemp_Click(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }

            try
            {

                CommonBAL objCom = new CommonBAL();

                string Criteria = "ESVD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND ESVD_TEMPLATE_CODE='" + drpTemplate.SelectedValue + "'";
                objCom.fnDeleteTableData("EMR_SEGMENT_VISIT_TEMPLATES", Criteria);



                string Criteria1 = "ET_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND ET_CODE='" + drpTemplate.SelectedValue + "' and ET_TYPE='Dashboard'";
                objCom.fnDeleteTableData("EMR_TEMPLATES", Criteria1);


                BindTemplate();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientDashboard.btnDeleteTemp_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;
        }

        protected void drpTemplate_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { goto FunEnd; }
            //try
            //{


            if (drpTemplate.SelectedIndex != 0)
            {
                SegmentBAL objPro = new SegmentBAL();
                objPro.BranchID = Convert.ToString(Session["Branch_ID"]);
                objPro.EMRID = Convert.ToString(Session["EMR_ID"]);
                objPro.TemplateCode = drpTemplate.SelectedValue;
                objPro.SegmentVisitAddFromTemplate();

                Response.Redirect("PatientDashboard.aspx?SelectedTemplate=" + drpTemplate.SelectedValue);



            }



            //}
        //catch (Exception ex)
        //{
        //    TextFileWriting("-----------------------------------------------");
        //    TextFileWriting(System.DateTime.Now.ToString() + "      PatientDashboard.drpTemplate_SelectedIndexChanged");
        //    TextFileWriting(ex.Message.ToString());
        //}

        FunEnd: ;
        }
        #endregion
    }
}