﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using EMR_BAL;


namespace Mediplus.EMR.Patient
{
    public partial class Anesthesia : System.Web.UI.Page
    {
        static string strSessionDeptId;

        CommonBAL objCom = new CommonBAL();
        EMR_Anesthesia objAnes = new EMR_Anesthesia();
        DataSet DS = new DataSet();
        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMR_IPLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        DataTable BindHour()
        {
            DataSet DS = new DataSet();
            DS = objCom.HoursGet("false");

            return DS.Tables[0];
        }

        DataTable BindMinutes()
        {
            DataSet DS = new DataSet();
            DS = objCom.MinutesGet("5");

            return DS.Tables[0];
        }

        //void BindTime12Hrs()
        //{
        //    int AppointmentInterval = 05;// Convert.ToInt32(ViewState["AppointmentInterval"]);
        //    int AppointmentStart = 01; //Convert.ToInt32(ViewState["AppointmentStart"]);
        //    int AppointmentEnd = 12;// Convert.ToInt32(ViewState["AppointmentEnd"]);
        //    int index = 0;

        //    for (int i = 1; i <= 12; i++)
        //    {
        //        drpTimeInHour.Items.Insert(index, Convert.ToInt32(1 + index).ToString("D2"));
        //        drpTimeInHour.Items[index].Value = Convert.ToInt32(1 + index).ToString("D2");

        //        drpTimeOutHour.Items.Insert(index, Convert.ToInt32(1 + index).ToString("D2"));
        //        drpTimeOutHour.Items[index].Value = Convert.ToInt32(1 + index).ToString("D2");

        //        drpAnesSTHour.Items.Insert(index, Convert.ToInt32(1 + index).ToString("D2"));
        //        drpAnesSTHour.Items[index].Value = Convert.ToInt32(1 + index).ToString("D2");

        //        drpAnesFinHour.Items.Insert(index, Convert.ToInt32(1 + index).ToString("D2"));
        //        drpAnesFinHour.Items[index].Value = Convert.ToInt32(1 + index).ToString("D2");

        //        drpSurgSTHour.Items.Insert(index, Convert.ToInt32(1 + index).ToString("D2"));
        //        drpSurgSTHour.Items[index].Value = Convert.ToInt32(1 + index).ToString("D2");


        //        drpSurgFinHour.Items.Insert(index, Convert.ToInt32(1 + index).ToString("D2"));
        //        drpSurgFinHour.Items[index].Value = Convert.ToInt32(1 + index).ToString("D2");

        //        drpSigAnestheHour.Items.Insert(index, Convert.ToInt32(1 + index).ToString("D2"));
        //        drpSigAnestheHour.Items[index].Value = Convert.ToInt32(1 + index).ToString("D2");

        //        index = index + 1;

        //    }
        //    index = 1;
        //    int intCount = 0;
        //    Int32 intMin = AppointmentInterval;

        //    drpTimeInMin.Items.Insert(0, Convert.ToString("00"));
        //    drpTimeInMin.Items[0].Value = Convert.ToString("00");


        //    drpTimeOutMin.Items.Insert(0, Convert.ToString("00"));
        //    drpTimeOutMin.Items[0].Value = Convert.ToString("00");

        //    drpAnesSTMin.Items.Insert(0, Convert.ToString("00"));
        //    drpAnesSTMin.Items[0].Value = Convert.ToString("00");


        //    drpAnesFinMin.Items.Insert(0, Convert.ToString("00"));
        //    drpAnesFinMin.Items[0].Value = Convert.ToString("00");


        //    drpSurgSTMin.Items.Insert(0, Convert.ToString("00"));
        //    drpSurgSTMin.Items[0].Value = Convert.ToString("00");



        //    drpSurgFinMin.Items.Insert(0, Convert.ToString("00"));
        //    drpSurgFinMin.Items[0].Value = Convert.ToString("00");

        //    drpSigAnestheMin.Items.Insert(0, Convert.ToString("00"));
        //    drpSigAnestheMin.Items[0].Value = Convert.ToString("00");

        //    for (int j = AppointmentInterval; j < 60; j++)
        //    {

        //        drpTimeInMin.Items.Insert(index, Convert.ToString(j - intCount));
        //        drpTimeInMin.Items[index].Value = Convert.ToString(j - intCount);

        //        drpTimeOutMin.Items.Insert(index, Convert.ToString(j - intCount));
        //        drpTimeOutMin.Items[index].Value = Convert.ToString(j - intCount);

        //        drpAnesSTMin.Items.Insert(index, Convert.ToString(j - intCount));
        //        drpAnesSTMin.Items[index].Value = Convert.ToString(j - intCount);

        //        drpAnesFinMin.Items.Insert(index, Convert.ToString(j - intCount));
        //        drpAnesFinMin.Items[index].Value = Convert.ToString(j - intCount);

        //        drpSurgSTMin.Items.Insert(index, Convert.ToString(j - intCount));
        //        drpSurgSTMin.Items[index].Value = Convert.ToString(j - intCount);

        //        drpSurgFinMin.Items.Insert(index, Convert.ToString(j - intCount));
        //        drpSurgFinMin.Items[index].Value = Convert.ToString(j - intCount);

        //        drpSigAnestheMin.Items.Insert(index, Convert.ToString(j - intCount));
        //        drpSigAnestheMin.Items[index].Value = Convert.ToString(j - intCount);



        //        j = j + AppointmentInterval;
        //        index = index + 1;
        //        intCount = intCount + 1;

        //    }

        //}

        void BindTime()
        {

            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();

            DS = objCom.HoursGet("false");
            if (DS.Tables[0].Rows.Count > 0)
            {

                drpTimeInHour.DataSource = DS;
                drpTimeInHour.DataTextField = "Name";
                drpTimeInHour.DataValueField = "Code";
                drpTimeInHour.DataBind();

                drpTimeOutHour.DataSource = DS;
                drpTimeOutHour.DataTextField = "Name";
                drpTimeOutHour.DataValueField = "Code";
                drpTimeOutHour.DataBind();

                drpAnesSTHour.DataSource = DS;
                drpAnesSTHour.DataTextField = "Name";
                drpAnesSTHour.DataValueField = "Code";
                drpAnesSTHour.DataBind();


                drpAnesFinHour.DataSource = DS;
                drpAnesFinHour.DataTextField = "Name";
                drpAnesFinHour.DataValueField = "Code";
                drpAnesFinHour.DataBind();


                drpSurgSTHour.DataSource = DS;
                drpSurgSTHour.DataTextField = "Name";
                drpSurgSTHour.DataValueField = "Code";
                drpSurgSTHour.DataBind();


                drpSurgFinHour.DataSource = DS;
                drpSurgFinHour.DataTextField = "Name";
                drpSurgFinHour.DataValueField = "Code";
                drpSurgFinHour.DataBind();


                drpSigAnestheHour.DataSource = DS;
                drpSigAnestheHour.DataTextField = "Name";
                drpSigAnestheHour.DataValueField = "Code";
                drpSigAnestheHour.DataBind();
            }

            //drpTimeInHour.Items.Insert(0, "00");
            //drpTimeInHour.Items[0].Value = "00";

            //drpTimeOutHour.Items.Insert(0, "00");
            //drpTimeOutHour.Items[0].Value = "00";

            //drpAnesSTHour.Items.Insert(0, "00");
            //drpAnesSTHour.Items[0].Value = "00";

            //drpAnesFinHour.Items.Insert(0, "00");
            //drpAnesFinHour.Items[0].Value = "00";

            //drpSurgSTHour.Items.Insert(0, "00");
            //drpSurgSTHour.Items[0].Value = "00";


            //drpSurgFinHour.Items.Insert(0, "00");
            //drpSurgFinHour.Items[0].Value = "00";


            //drpSigAnestheHour.Items.Insert(0, "00");
            //drpSigAnestheHour.Items[0].Value = "00";


            DS = new DataSet();

            DS = objCom.MinutesGet("5");
            if (DS.Tables[0].Rows.Count > 0)
            {


                drpTimeInMin.DataSource = DS;
                drpTimeInMin.DataTextField = "Name";
                drpTimeInMin.DataValueField = "Code";
                drpTimeInMin.DataBind();

                drpTimeOutMin.DataSource = DS;
                drpTimeOutMin.DataTextField = "Name";
                drpTimeOutMin.DataValueField = "Code";
                drpTimeOutMin.DataBind();

                drpAnesSTMin.DataSource = DS;
                drpAnesSTMin.DataTextField = "Name";
                drpAnesSTMin.DataValueField = "Code";
                drpAnesSTMin.DataBind();

                drpAnesFinMin.DataSource = DS;
                drpAnesFinMin.DataTextField = "Name";
                drpAnesFinMin.DataValueField = "Code";
                drpAnesFinMin.DataBind();

                drpSurgSTMin.DataSource = DS;
                drpSurgSTMin.DataTextField = "Name";
                drpSurgSTMin.DataValueField = "Code";
                drpSurgSTMin.DataBind();


                drpSurgFinMin.DataSource = DS;
                drpSurgFinMin.DataTextField = "Name";
                drpSurgFinMin.DataValueField = "Code";
                drpSurgFinMin.DataBind();

                drpSigAnestheMin.DataSource = DS;
                drpSigAnestheMin.DataTextField = "Name";
                drpSigAnestheMin.DataValueField = "Code";
                drpSigAnestheMin.DataBind();
            }

            //drpTimeInMin.Items.Insert(0, "00");
            //drpTimeInMin.Items[0].Value = "00";

            //drpTimeOutMin.Items.Insert(0, "00");
            //drpTimeOutMin.Items[0].Value = "00";

            //drpAnesSTMin.Items.Insert(0, "00");
            //drpAnesSTMin.Items[0].Value = "00";


            //drpAnesFinMin.Items.Insert(0, "00");
            //drpAnesFinMin.Items[0].Value = "00";


            //drpSurgSTMin.Items.Insert(0, "00");
            //drpSurgSTMin.Items[0].Value = "00";


            //drpSurgFinMin.Items.Insert(0, "00");
            //drpSurgFinMin.Items[0].Value = "00";

            //drpSigAnestheMin.Items.Insert(0, "00");
            //drpSigAnestheMin.Items[0].Value = "00";



        }

        void BindStaff()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1  AND HSFM_CATEGORY='Doctors' AND HSFM_SF_STATUS = 'Present' ";
            Criteria += " AND HSFM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' ";
            DS = objCom.GetStaffMaster(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {


                drpSurgeon.DataSource = DS;
                drpSurgeon.DataTextField = "FullName";
                drpSurgeon.DataValueField = "HSFM_STAFF_ID";
                drpSurgeon.DataBind();

                drpSurgeon1.DataSource = DS;
                drpSurgeon1.DataTextField = "FullName";
                drpSurgeon1.DataValueField = "HSFM_STAFF_ID";
                drpSurgeon1.DataBind();

                drpSurgeon2.DataSource = DS;
                drpSurgeon2.DataTextField = "FullName";
                drpSurgeon2.DataValueField = "HSFM_STAFF_ID";
                drpSurgeon2.DataBind();


            }

            drpSurgeon.Items.Insert(0, "--- Select ---");
            drpSurgeon.Items[0].Value = "";


            drpSurgeon1.Items.Insert(0, "--- Select ---");
            drpSurgeon1.Items[0].Value = "";

            drpSurgeon2.Items.Insert(0, "--- Select ---");
            drpSurgeon2.Items[0].Value = "";







            // objCom.BindMasterDropdown(Criteria, out drpDossage);

        }

        void BindUsers()
        {
            string Criteria = " 1=1 AND HUM_STATUS='A' ";
            Criteria += " AND HUM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND ( HUM_REMARK IN (SELECT HSFM_STAFF_ID FROM HMS_STAFF_MASTER WHERE HSFM_CATEGORY IN ('Doctors','Nurse') OR HUM_USER_ID ='ADMIN')) ";


            DataSet ds = new DataSet();
            ds = objCom.UserMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpSigAnesthesiologist.DataSource = ds;
                drpSigAnesthesiologist.DataValueField = "HUM_USER_ID";
                drpSigAnesthesiologist.DataTextField = "HUM_USER_NAME";
                drpSigAnesthesiologist.DataBind();

            }
            drpSigAnesthesiologist.Items.Insert(0, "--- Select ---");
            drpSigAnesthesiologist.Items[0].Value = "";


        }


        DataSet GetSegmentData()
        {
            objCom = new CommonBAL();
            string Criteria = " 1=1 AND EAS_ACTIVE=1 AND EAS_BRANCH='" + Convert.ToString(Session["Branch_ID"]) + "'";// AND EAS_TYPE='" + strType + "'";
            // Criteria += " AND  EAS_POSITION='" + Position + "'";

            DataSet DS = new DataSet();

            DS = objCom.fnGetFieldValue(" * ", "EMR_ANESTHESIA_SEGMENT", Criteria, "EAS_ORDER");



            return DS;

        }

        public DataSet GetSegmentMaster(string SegmentType)
        {
            DataSet DS = new DataSet();

            string Criteria = " 1=1 AND EASM_STATUS=1 ";
            Criteria += " and EASM_TYPE='" + SegmentType + "'";


            objAnes = new EMR_Anesthesia();
            DS = objAnes.AnesthesiaSegmentMasterGet(Criteria);


            return DS;

        }

        public void SaveSegmentDtls(string strType, string FieldID, string FieName, string Selected, string Comment)
        {
            objAnes = new EMR_Anesthesia();

            objAnes.BranchID = Convert.ToString(Session["Branch_ID"]);
            objAnes.EMRID = Convert.ToString(Session["EMR_ID"]);
            objAnes.PTID = Convert.ToString(Session["EMR_PT_ID"]);
            objAnes.EASD_TYPE = strType;
            objAnes.EASD_FIELD_ID = FieldID;
            objAnes.EASD_FIELD_NAME = FieName;
            objAnes.EASD_VALUE = Selected;
            objAnes.EASD_COMMENT = Comment;
            objAnes.AnesthesiaSegmentDtlsAdd();
        }

        public void SaveSegment()
        {
            string Position = "";
            DataSet DS = new DataSet();

            DS = GetSegmentData();

            foreach (DataRow DR in DS.Tables[0].Rows)
            {
                Position = Convert.ToString(DR["EAS_POSITION"]);
                objCom = new CommonBAL();

                string CriteriaDel = " 1=1 AND EASD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND EASD_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
                CriteriaDel += " AND EASD_TYPE='" + Convert.ToString(DR["EAS_CODE"]).Trim() + "'";
                objCom.fnDeleteTableData("EMR_ANESTHESIA_SEGMENT_DTLS", CriteriaDel);


                Int32 i = 1;
                DataSet DS1 = new DataSet();

                DS1 = GetSegmentMaster(Convert.ToString(DR["EAS_CODE"]).Trim());



                foreach (DataRow DR1 in DS1.Tables[0].Rows)
                {
                    string strValueYes = "", _Comment = "";
                    if (Convert.ToString(DR1["EASM_CONTROL_TYPE"]).ToUpper() == "CheckBox".ToUpper())
                    {
                        CheckBox chk1 = new CheckBox();

                        if (Position == "LEFT")
                        {
                            chk1 = (CheckBox)plhoPhyAssessmentLeft.FindControl("chk" + Convert.ToString(DR1["EASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["EASM_FIELD_ID"]).Trim());

                        }
                        else
                        {
                            chk1 = (CheckBox)plhoPhyAssessmentRight.FindControl("chk" + Convert.ToString(DR1["EASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["EASM_FIELD_ID"]).Trim());

                        }







                        if (chk1 != null)
                        {
                            if (chk1.Checked == true)
                            {
                                strValueYes = "Y";
                            }
                        }

                    }
                    if (Convert.ToString(DR1["EASM_CONTROL_TYPE"]).ToUpper() == "TextBox".ToUpper() || Convert.ToString(DR1["EASM_CONTROL_TYPE"]).ToUpper() == "AutoTextBox".ToUpper() || Convert.ToString(DR1["EASM_CONTROL_TYPE"]).ToUpper() == "DateTime".ToUpper())
                    {
                        TextBox txt = new TextBox();

                        if (Position == "LEFT")
                        {
                            txt = (TextBox)plhoPhyAssessmentLeft.FindControl("txt" + Convert.ToString(DR1["EASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["EASM_FIELD_ID"]).Trim());
                        }
                        else
                        {
                            txt = (TextBox)plhoPhyAssessmentRight.FindControl("txt" + Convert.ToString(DR1["EASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["EASM_FIELD_ID"]).Trim());

                        }

                        DropDownList drpHour = new DropDownList();
                        DropDownList drpMin = new DropDownList();

                        if (Convert.ToString(DR1["EASM_CONTROL_TYPE"]).ToUpper() == "DateTime".ToUpper())
                        {
                            if (Position == "LEFT")
                            {
                                drpHour = (DropDownList)plhoPhyAssessmentLeft.FindControl("drpHour" + Convert.ToString(DR1["EASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["EASM_FIELD_ID"]).Trim());
                                drpMin = (DropDownList)plhoPhyAssessmentLeft.FindControl("drpMin" + Convert.ToString(DR1["EASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["EASM_FIELD_ID"]).Trim());
                            }
                            else
                            {
                                drpHour = (DropDownList)plhoPhyAssessmentRight.FindControl("drpHour" + Convert.ToString(DR1["EASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["EASM_FIELD_ID"]).Trim());
                                drpMin = (DropDownList)plhoPhyAssessmentRight.FindControl("drpMin" + Convert.ToString(DR1["EASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["EASM_FIELD_ID"]).Trim());

                            }

                        }


                        if (txt.Text != "")
                        {
                            strValueYes = "Y";
                            _Comment = txt.Text;
                        }

                        if (Convert.ToString(DR1["EASM_CONTROL_TYPE"]).ToUpper() == "DateTime".ToUpper())
                        {
                            _Comment = txt.Text.Trim() + " " + drpHour.SelectedValue + ":" + drpMin.SelectedValue + ":00";
                        }
                    }




                    if (Convert.ToString(DR1["EASM_CONTROL_TYPE"]).ToUpper() == "RadioButtonList".ToUpper())
                    {

                        RadioButtonList RadList = new RadioButtonList();



                        if (Position == "LEFT")
                        {

                            RadList = (RadioButtonList)plhoPhyAssessmentLeft.FindControl("radl" + Convert.ToString(DR1["EASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["EASM_FIELD_ID"]).Trim());

                        }
                        else
                        {
                            RadList = (RadioButtonList)plhoPhyAssessmentRight.FindControl("radl" + Convert.ToString(DR1["EASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["EASM_FIELD_ID"]).Trim());

                        }






                        if (RadList != null)
                        {
                            if (RadList.SelectedValue != "")
                            {
                                strValueYes = "Y";
                                _Comment = RadList.SelectedValue;
                            }
                        }



                    }





                    if (Convert.ToString(DR1["EASM_CONTROL_TYPE"]).ToUpper() == "DropDownList".ToUpper())
                    {

                        DropDownList drpList = new DropDownList();



                        if (Position == "LEFT")
                        {

                            drpList = (DropDownList)plhoPhyAssessmentLeft.FindControl("drp" + Convert.ToString(DR1["EASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["EASM_FIELD_ID"]).Trim());

                        }
                        else
                        {
                            drpList = (DropDownList)plhoPhyAssessmentRight.FindControl("drp" + Convert.ToString(DR1["EASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["EASM_FIELD_ID"]).Trim());

                        }







                        if (drpList != null)
                        {
                            if (drpList.SelectedValue != "" && drpList.SelectedValue != "---Select---")
                            {
                                strValueYes = "Y";
                                _Comment = drpList.SelectedValue;
                            }
                        }



                    }



                    if (strValueYes != "")
                    {
                        SaveSegmentDtls(Convert.ToString(DR["EAS_CODE"]).Trim(), Convert.ToString(DR1["EASM_FIELD_ID"]).Trim(), Convert.ToString(DR1["EASM_FIELD_NAME"]).Trim(), strValueYes, _Comment);

                    }

                }
                i = i + 1;
            }
        }


        public void CreateSegCtrls()
        {
            string Position = "";
            DataSet DS = new DataSet();

            DS = GetSegmentData();

            foreach (DataRow DR in DS.Tables[0].Rows)
            {
                Position = Convert.ToString(DR["EAS_POSITION"]);

                Panel myFieldSet = new Panel();

                myFieldSet.GroupingText = Convert.ToString(DR["EAS_NAME"]);
                myFieldSet.Style.Add("text-align", "justify");
                myFieldSet.CssClass = "lblCaption1";
                myFieldSet.Width = 500;
                Int32 i = 1;

                DataSet DS1 = new DataSet();

                DS1 = GetSegmentMaster(Convert.ToString(DR["EAS_CODE"]));

                foreach (DataRow DR1 in DS1.Tables[0].Rows)
                {


                    Boolean _checked = false;
                    string strComment = "";
                    objAnes = new EMR_Anesthesia();
                    string Criteria2 = "1=1 ";
                    Criteria2 += " AND EASD_TYPE='" + Convert.ToString(DR1["EASM_TYPE"]).Trim() + "' AND EASD_FIELD_ID='" + Convert.ToString(DR1["EASM_FIELD_ID"]).Trim() + "' ";
                    Criteria2 += " AND EASD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND EASD_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

                    DataSet DS2 = new DataSet();
                    DS2 = objAnes.AnesthesiaSegmentDtlsGet(Criteria2);

                    if (DS2.Tables[0].Rows.Count > 0)
                    {
                        _checked = true;

                        strComment = Convert.ToString(DS2.Tables[0].Rows[0]["EASD_COMMENT"]).Trim();
                    }


                    if (Convert.ToString(DR1["EASM_CONTROL_TYPE"]).ToUpper() == "CheckBox".ToUpper())
                    {
                        CheckBox chk = new CheckBox();
                        chk.ID = "chk" + Convert.ToString(DR1["EASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["EASM_FIELD_ID"]).Trim();
                        chk.Text = Convert.ToString(DR1["EASM_FIELD_NAME"]).Trim();
                        chk.CssClass = "lblCaption1";

                        chk.Checked = _checked;

                        //tdHe1.Controls.Add(chk);
                        //trHe1.Controls.Add(tdHe1);
                        //myFieldSet.Controls.Add(trHe1);

                        myFieldSet.Controls.Add(chk);
                    }


                    if (Convert.ToString(DR1["EASM_CONTROL_TYPE"]).ToUpper() == "TextBox".ToUpper())
                    {
                        Label lbl = new Label();
                        lbl.CssClass = "lblCaption1";
                        lbl.Text = Convert.ToString(DR1["EASM_FIELD_NAME"]).Trim() + ":";
                        myFieldSet.Controls.Add(lbl);

                        Literal lit1 = new Literal() { Mode = LiteralMode.PassThrough, Text = "<br/>" };
                        myFieldSet.Controls.Add(lit1);

                        TextBox txt = new TextBox();
                        txt.ID = "txt" + Convert.ToString(DR1["EASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["EASM_FIELD_ID"]).Trim();
                        // txt.Text = Convert.ToString(DR1["EASM_FIELD_NAME"]);
                        txt.CssClass = "Label";
                        txt.Style.Add("width", "99%");

                        txt.Text = strComment;

                        if (DR1.IsNull("EASM_TARGET_CTRL") == false)
                        {
                            if (Convert.ToString(DR1["EASM_FIELD_NAME"]).Trim() == "Height" || Convert.ToString(DR1["EASM_FIELD_NAME"]).Trim() == "Weight")
                            {
                                string strCtrlID = Convert.ToString(DR1["EASM_TARGET_CTRL"]).Trim();
                                txt.Attributes.Add("onkeyup", "return BindBMI('txt" + Convert.ToString(DR1["EASM_TYPE"]).Trim() + "_5','txt" + Convert.ToString(DR1["EASM_TYPE"]).Trim() + "_6','txt" + Convert.ToString(DR1["EASM_TYPE"]).Trim() + "_" + strCtrlID + "')");

                            }
                        }
                        myFieldSet.Controls.Add(txt);


                    }


                    if (Convert.ToString(DR1["EASM_CONTROL_TYPE"]).ToUpper() == "AutoTextBox".ToUpper())
                    {
                        Label lbl = new Label();
                        lbl.CssClass = "lblCaption1";
                        lbl.Text = Convert.ToString(DR1["EASM_FIELD_NAME"]).Trim() + ":";
                        myFieldSet.Controls.Add(lbl);

                        Literal lit1 = new Literal() { Mode = LiteralMode.PassThrough, Text = "<br/>" };
                        myFieldSet.Controls.Add(lit1);

                        TextBox txt = new TextBox();
                        txt.ID = "txt" + Convert.ToString(DR1["EASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["EASM_FIELD_ID"]).Trim();
                        txt.Style.Add("width", "99%");
                        txt.Style.Add("height", "20px");
                        txt.TextMode = TextBoxMode.MultiLine;
                        txt.Height = 20;
                        txt.Style.Add("resize", "none");
                        txt.Attributes.Add("autocomplete", "off");
                        txt.CssClass = "Label";

                        if (DR1.IsNull("EASM_HEIGHT") == false)
                        {
                            txt.Height = Convert.ToInt32(DR1["EASM_HEIGHT"]);
                            txt.Style.Add("height", Convert.ToString(DR1["EASM_HEIGHT"] + "px"));
                        }
                        else
                        {
                            txt.Style.Add("height", "50px");
                        }

                        AjaxControlToolkit.AutoCompleteExtender autoCompleteExtender = new AjaxControlToolkit.AutoCompleteExtender();
                        autoCompleteExtender.TargetControlID = txt.ID;


                        if (DR1.IsNull("EASM_DATA_LOAD_FROM") == false && Convert.ToString(DR1["EASM_DATA_LOAD_FROM"]) != "")
                        {
                            if (Convert.ToString(DR1["EASM_DATA_LOAD_FROM"]).Trim().ToUpper() == "DIAGNOSIS")
                            {
                                autoCompleteExtender.ServiceMethod = "GetDiagnosisName";
                            }
                            if (Convert.ToString(DR1["EASM_DATA_LOAD_FROM"]).Trim().ToUpper() == "PROCEDURE")
                            {
                                autoCompleteExtender.ServiceMethod = "GetProcedure";
                            }

                            if (Convert.ToString(DR1["EASM_DATA_LOAD_FROM"]).Trim().ToUpper() == "DOCTOR")
                            {
                                autoCompleteExtender.ServiceMethod = "GetDoctorName";
                            }

                            if (Convert.ToString(DR1["EASM_DATA_LOAD_FROM"]).Trim().ToUpper() == "HISTORY")
                            {
                                autoCompleteExtender.ServiceMethod = "GetSegmentValue";
                            }
                        }
                        else
                        {
                            autoCompleteExtender.ServiceMethod = "GetSegmentValue";
                        }

                        autoCompleteExtender.ID = "aut" + Convert.ToString(DR1["EASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["EASM_FIELD_ID"]).Trim();
                        autoCompleteExtender.CompletionListCssClass = "AutoExtender";
                        autoCompleteExtender.CompletionListItemCssClass = "AutoExtenderList";
                        autoCompleteExtender.CompletionListHighlightedItemCssClass = "AutoExtenderHighlight";
                        // autoCompleteExtender.CompletionListElementID = "divwidth";
                        autoCompleteExtender.ContextKey = Convert.ToString(DR1["EASM_TYPE"]).Trim() + "~" + Convert.ToString(DR1["EASM_FIELD_ID"]).Trim();
                        autoCompleteExtender.UseContextKey = true;

                        // autoCompleteExtender.ServicePath = "YourAutoCompleteWebService.asmx";
                        autoCompleteExtender.CompletionInterval = 10;
                        autoCompleteExtender.CompletionSetCount = 15;
                        autoCompleteExtender.EnableCaching = true;
                        autoCompleteExtender.MinimumPrefixLength = 1;



                        txt.Text = strComment;

                        if (DR1.IsNull("EASM_TARGET_CTRL") == false)
                        {
                            string strCtrlID = Convert.ToString(DR1["EASM_TARGET_CTRL"]).Trim();
                            txt.Attributes.Add("onkeypress", "return BindCC(event,'" + txt.ID + "','txt" + Convert.ToString(DR1["EASM_TYPE"]).Trim() + "_" + strCtrlID + "')");

                            autoCompleteExtender.ContextKey = Convert.ToString(DR1["EASM_TYPE"]).Trim() + "~" + strCtrlID;
                        }


                        myFieldSet.Controls.Add(txt);
                        myFieldSet.Controls.Add(autoCompleteExtender);



                    }

                    if (Convert.ToString(DR1["EASM_CONTROL_TYPE"]).ToUpper() == "DateTime".ToUpper())
                    {
                        Label lbl = new Label();
                        lbl.CssClass = "lblCaption1";
                        lbl.Text = Convert.ToString(DR1["EASM_FIELD_NAME"]).Trim() + ":";
                        lbl.Width = 150;
                        myFieldSet.Controls.Add(lbl);

                        TextBox txt = new TextBox();
                        txt.ID = "txt" + Convert.ToString(DR1["EASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["EASM_FIELD_ID"]).Trim();
                        // txt.Text = Convert.ToString(DR1["EASM_FIELD_NAME"]);
                        txt.CssClass = "lblCaption1";
                        txt.Style.Add("width", "75px");

                        AjaxControlToolkit.CalendarExtender CalendarExtender = new AjaxControlToolkit.CalendarExtender();
                        CalendarExtender.TargetControlID = txt.ID;
                        CalendarExtender.Format = "dd/MM/yyyy";


                        DropDownList drpHour = new DropDownList();
                        drpHour.ID = "drpHour" + Convert.ToString(DR1["EASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["EASM_FIELD_ID"]).Trim();
                        drpHour.CssClass = "lblCaption1";
                        drpHour.Style.Add("width", "48px");

                        drpHour.DataSource = BindHour();
                        drpHour.DataTextField = "Name";
                        drpHour.DataValueField = "Code";
                        drpHour.DataBind();

                        DropDownList drpMin = new DropDownList();
                        drpMin.ID = "drpMin" + Convert.ToString(DR1["EASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["EASM_FIELD_ID"]).Trim();
                        drpMin.CssClass = "lblCaption1";
                        drpMin.Style.Add("width", "48px");

                        drpMin.DataSource = BindMinutes();
                        drpMin.DataTextField = "Name";
                        drpMin.DataValueField = "Code";
                        drpMin.DataBind();





                        string strDateTime = strComment.Trim();
                        string[] arrDateTime = strDateTime.Split(' ');

                        if (arrDateTime.Length > 0)
                        {
                            txt.Text = arrDateTime[0];

                            if (arrDateTime.Length > 1)
                            {
                                string strIA_TIME_IN = arrDateTime[1];

                                string[] arrIA_TIME_IN = strIA_TIME_IN.Split(':');
                                if (arrIA_TIME_IN.Length > 1)
                                {
                                    drpHour.SelectedValue = arrIA_TIME_IN[0];
                                    drpMin.SelectedValue = arrIA_TIME_IN[1];

                                }
                            }

                        }

                        myFieldSet.Controls.Add(txt);
                        myFieldSet.Controls.Add(CalendarExtender);
                        myFieldSet.Controls.Add(drpHour);
                        myFieldSet.Controls.Add(drpMin);


                    }




                    if (Convert.ToString(DR1["EASM_CONTROL_TYPE"]).ToUpper() == "Label".ToUpper())
                    {
                        Label lbl = new Label();
                        lbl.CssClass = "lblCaption1";
                        lbl.Text = Convert.ToString(DR1["EASM_FIELD_NAME"]).Trim();
                        myFieldSet.Controls.Add(lbl);



                    }

                    if (Convert.ToString(DR1["EASM_CONTROL_TYPE"]) == "DropDownList")
                    {

                        Label lbl = new Label();
                        lbl.CssClass = "lblCaption1";
                        lbl.Text = Convert.ToString(DR1["EASM_FIELD_NAME"]).Trim() + ":    ";
                        lbl.Width = 150;
                        myFieldSet.Controls.Add(lbl);

                        DropDownList DrpList = new DropDownList();
                        DrpList.ID = "drp" + Convert.ToString(DR1["EASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["EASM_FIELD_ID"]).Trim();
                        DrpList.Width = 200;

                        string strRadFieldName = Convert.ToString(DR1["EASM_DATA"]);
                        string[] arrRadFieldName = strRadFieldName.Split('|');


                        for (int intRad = 0; intRad <= arrRadFieldName.Length - 1; intRad++)
                        {

                            DrpList.Items.Add(new ListItem(arrRadFieldName[intRad], arrRadFieldName[intRad]));

                        }

                        DrpList.CssClass = "lblCaption1";

                        for (int intRad = 0; intRad <= arrRadFieldName.Length - 1; intRad++)
                        {

                            if (DrpList.Items[intRad].Value == strComment)
                            {
                                DrpList.SelectedValue = strComment;
                            }

                        }

                        myFieldSet.Controls.Add(DrpList);

                    }




                    if (Convert.ToString(DR1["EASM_CONTROL_TYPE"]) == "RadioButtonList")
                    {

                        Label lbl = new Label();
                        lbl.CssClass = "lblCaption1";
                        lbl.Text = Convert.ToString(DR1["EASM_FIELD_NAME"]).Trim() + ":    ";
                        lbl.Width = 150;
                        myFieldSet.Controls.Add(lbl);

                        RadioButtonList RadList = new RadioButtonList();
                        RadList.ID = "radl" + Convert.ToString(DR1["EASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["EASM_FIELD_ID"]).Trim();

                        RadList.RepeatDirection = RepeatDirection.Horizontal;
                        RadList.Width = Convert.ToInt32(DR1["EASM_WIDTH"]);
                        string strRadFieldName = Convert.ToString(DR1["EASM_DATA"]);
                        string[] arrRadFieldName = strRadFieldName.Split('|');


                        for (int intRad = 0; intRad <= arrRadFieldName.Length - 1; intRad++)
                        {

                            RadList.Items.Add(new ListItem(arrRadFieldName[intRad], arrRadFieldName[intRad]));

                        }

                        RadList.CssClass = "lblCaption1";

                        for (int intRad = 0; intRad <= arrRadFieldName.Length - 1; intRad++)
                        {

                            if (RadList.Items[intRad].Value == strComment)
                            {
                                RadList.SelectedValue = strComment;
                            }

                        }

                        myFieldSet.Controls.Add(RadList);

                    }

                    if (Convert.ToString(DR1["EASM_CONTROL_TYPE"]) == "Image")
                    {

                        Label lbl = new Label();
                        lbl.CssClass = "lblCaption1";
                        lbl.Text = Convert.ToString(DR1["EASM_FIELD_NAME"]).Trim() + ":    ";
                        lbl.Width = 150;
                        myFieldSet.Controls.Add(lbl);

                        Image img = new Image();
                        img.ID = "img" + Convert.ToString(DR1["EASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["EASM_FIELD_ID"]).Trim();

                        img.ImageUrl = "../Images/" + Convert.ToString(DR1["EASM_DATA"]).Trim();

                        if (DR1.IsNull("EASM_WIDTH") == false && Convert.ToString(DR1["EASM_WIDTH"]) != "")
                        {
                            img.Width = Convert.ToInt32(DR1["EASM_WIDTH"]);
                        }
                        else
                        {
                            img.Width = 50;
                        }



                        if (DR1.IsNull("EASM_TARGET_CTRL") == false)
                        {
                            string strCtrlID = Convert.ToString(DR1["EASM_TARGET_CTRL"]).Trim();
                            img.Attributes.Add("onClick", "return BindValue('" + Convert.ToString(DR1["EASM_FIELD_NAME"]).Trim() + "','txt" + Convert.ToString(DR1["EASM_TYPE"]).Trim() + "_" + strCtrlID + "')");


                        }

                        myFieldSet.Controls.Add(img);

                    }

                    Literal lit = new Literal() { Mode = LiteralMode.PassThrough, Text = "<br/>" };
                    myFieldSet.Controls.Add(lit);

                }



                if (Position == "LEFT")
                {
                    plhoPhyAssessmentLeft.Controls.Add(myFieldSet);
                }
                else
                {
                    plhoPhyAssessmentRight.Controls.Add(myFieldSet);
                }






                i = i + 1;



            }
        }



        void BindAnesthesia()
        {
            objAnes = new EMR_Anesthesia();
            DS = new DataSet();
            string Criteria = " EA_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "' AND  EA_ID ='" + Convert.ToString(Session["EMR_ID"]) + "'";
            DS = objAnes.AnesthesiaGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    txtAnesDate.Text = Convert.ToString(DR["EA_DATEDesc"]);

                    txtAnesProcedure.Text = Convert.ToString(DR["EA_ANESTHETIC_PROCEDURES"]);
                    txtSurProcedure.Text = Convert.ToString(DR["EA_SURGICAL_PROCEDURES"]);
                    drpSurgeon.SelectedValue = Convert.ToString(DR["EA_SURGEON"]);
                    drpSurgeon1.SelectedValue = Convert.ToString(DR["EA_SURGEON1"]);
                    drpSurgeon2.SelectedValue = Convert.ToString(DR["EA_SURGEON2"]);

                    txtAssistantsDtls.Text = Convert.ToString(DR["EA_ASSISTANT"]);


                    string strIA_TIME_IN = Convert.ToString(DR["EA_TIME_INDesc"]);

                    string[] arrIA_TIME_IN = strIA_TIME_IN.Split(':');
                    if (arrIA_TIME_IN.Length > 1)
                    {
                        drpTimeInHour.SelectedValue = arrIA_TIME_IN[0];
                        drpTimeInMin.SelectedValue = arrIA_TIME_IN[1];


                        string[] arrIA_TIME_IN1 = strIA_TIME_IN.Split(' ');

                        if (arrIA_TIME_IN1.Length > 1)
                        {
                            drpTimeInAM.SelectedValue = arrIA_TIME_IN1[1];
                        }

                    }



                    string strdrpAnesSTMin = Convert.ToString(DR["EA_ANESTHESIA_STARTDesc"]);
                    string[] arrAnesSTMin = strdrpAnesSTMin.Split(':');
                    if (arrAnesSTMin.Length > 1)
                    {
                        drpAnesSTHour.SelectedValue = arrAnesSTMin[0];
                        drpAnesSTMin.SelectedValue = arrAnesSTMin[1];

                        string[] arrAnesSTMin1 = strdrpAnesSTMin.Split(' ');

                        if (arrAnesSTMin1.Length > 1)
                        {
                            drpAnesSTAM.SelectedValue = arrAnesSTMin1[1];
                        }

                    }


                    string strdrpSurgSTMin = Convert.ToString(DR["EA_SURGERY_STARTDesc"]);
                    string[] arrSurgSTMin = strdrpSurgSTMin.Split(':');
                    if (arrSurgSTMin.Length > 1)
                    {
                        drpSurgSTHour.SelectedValue = arrSurgSTMin[0];
                        drpSurgSTMin.SelectedValue = arrSurgSTMin[1];

                        string[] arrSurgSTMin1 = strdrpSurgSTMin.Split(' ');

                        if (arrSurgSTMin1.Length > 1)
                        {
                            drpSurgSTAM.SelectedValue = arrSurgSTMin1[1];
                        }

                    }


                    string strSurgFinMin = Convert.ToString(DR["EA_SURGERY_ENDDesc"]);
                    string[] arrSurgFinMin = strSurgFinMin.Split(':');
                    if (arrSurgFinMin.Length > 1)
                    {
                        drpSurgFinHour.SelectedValue = arrSurgFinMin[0];
                        drpSurgFinMin.SelectedValue = arrSurgFinMin[1];

                        string[] arrSurgFinMin1 = strSurgFinMin.Split(' ');

                        if (arrSurgFinMin1.Length > 1)
                        {
                            drpSurgFinAM.SelectedValue = arrSurgFinMin1[1];
                        }

                    }


                    string strAnesFinHour = Convert.ToString(DR["EA_ANESTHESIA_STOPESDesc"]);
                    string[] arrAnesFinHour = strAnesFinHour.Split(':');
                    if (arrAnesFinHour.Length > 1)
                    {
                        drpAnesFinHour.SelectedValue = arrAnesFinHour[0];
                        drpAnesFinMin.SelectedValue = arrAnesFinHour[1];

                        string[] arrAnesFinHour1 = strAnesFinHour.Split(' ');

                        if (arrAnesFinHour1.Length > 1)
                        {
                            drpAnesFinAM.SelectedValue = arrAnesFinHour1[1];
                        }

                    }


                    string strTimeOutHour = Convert.ToString(DR["EA_PT_OUT_OF_ORDesc"]);
                    string[] arrTimeOutHour = strTimeOutHour.Split(':');
                    if (arrTimeOutHour.Length > 1)
                    {
                        drpTimeOutHour.SelectedValue = arrTimeOutHour[0];
                        drpTimeOutMin.SelectedValue = arrTimeOutHour[1];

                        string[] arrTimeOutHour1 = strTimeOutHour.Split(' ');

                        if (arrTimeOutHour1.Length > 1)
                        {
                            drpTimeOutAM.SelectedValue = arrTimeOutHour1[1];
                        }
                    }



                    drpBP.SelectedValue = Convert.ToString(DR["EA_BP_SCORE"]);
                    drpSPo2.SelectedValue = Convert.ToString(DR["EA_SPO2_SCORE"]);
                    drpConciousness.SelectedValue = Convert.ToString(DR["EA_CONCIOUSNESS_SCORE"]);
                    drpMotorFn.SelectedValue = Convert.ToString(DR["EA_MOTOR_FUN_SCORE"]);
                    drpPainScore.SelectedValue = Convert.ToString(DR["EA_PAIN_SCORE"]);
                    drpRespiration.SelectedValue = Convert.ToString(DR["EA_RESPIRATION_SCORE"]);


                    txtBPValue.Text = Convert.ToString(DR["EA_BP_SCORE"]);
                    txtSPo2Value.Text = Convert.ToString(DR["EA_SPO2_SCORE"]);
                    txtConciousnessValue.Text = Convert.ToString(DR["EA_CONCIOUSNESS_SCORE"]);
                    txtMotorFnValue.Text = Convert.ToString(DR["EA_MOTOR_FUN_SCORE"]);
                    txtPainScoreValue.Text = Convert.ToString(DR["EA_PAIN_SCORE"]);
                    txtRespirationValue.Text = Convert.ToString(DR["EA_RESPIRATION_SCORE"]);
                    //ViewState["EA_CHART_ATTACHMENT"] = Convert.ToString(DR["EA_CHART_ATTACHMENT"]);
                    //Session["EA_CHART_ATTACHMENT"] = Convert.ToString(DR["EA_CHART_ATTACHMENT"]);


                    lblAnesChartFileName.Text = Convert.ToString(DR["EA_ANS_CHART_FILE_NAME"]);

                }

            }
        }

        void SaveAnesthesia()
        {
            //string pathUser = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
            //string pathDownload = pathUser + @"\Downloads\";

            //if (Convert.ToString(ViewState["AttachedFileName"]) != "" && Convert.ToString(ViewState["AttachedFileName"]) != null)
            //{
            //    byte[] bytes = System.IO.File.ReadAllBytes(pathDownload + Convert.ToString(ViewState["AttachedFileName"]));

            //    ViewState["IA_CHART_ATTACHMENT"] = Convert.ToBase64String(bytes);

            //}

            objAnes = new EMR_Anesthesia();
            objAnes.BranchID = Convert.ToString(Session["Branch_ID"]);
            objAnes.EMRID = Convert.ToString(Session["EMR_ID"]);
            objAnes.PTID = Convert.ToString(Session["EMR_PT_ID"]);

            objAnes.EA_DATE = txtAnesDate.Text;

            objAnes.EA_ANESTHETIC_PROCEDURES = txtAnesProcedure.Text;
            objAnes.EA_SURGICAL_PROCEDURES = txtSurProcedure.Text;
            objAnes.EA_SURGEON = drpSurgeon.SelectedValue;
            objAnes.EA_SURGEON1 = drpSurgeon1.SelectedValue;
            objAnes.EA_SURGEON2 = drpSurgeon2.SelectedValue;
            objAnes.EA_ASSISTANT = txtAssistantsDtls.Text;
            objAnes.EA_TIME_IN = txtAnesDate.Text.Trim() + " " + drpTimeInHour.SelectedValue + ":" + drpTimeInMin.SelectedValue + ":00" + " " + drpTimeInAM.SelectedValue;
            objAnes.EA_ANESTHESIA_START = txtAnesDate.Text.Trim() + " " + drpAnesSTHour.SelectedValue + ":" + drpAnesSTMin.SelectedValue + ":00" + " " + drpAnesSTAM.SelectedValue;
            objAnes.EA_SURGERY_START = txtAnesDate.Text.Trim() + " " + drpSurgSTHour.SelectedValue + ":" + drpSurgSTMin.SelectedValue + ":00" + " " + drpSurgSTAM.SelectedValue;
            objAnes.EA_SURGERY_ENDS = txtAnesDate.Text.Trim() + " " + drpSurgFinHour.SelectedValue + ":" + drpSurgFinMin.SelectedValue + ":00" + " " + drpSurgFinAM.SelectedValue;
            objAnes.EA_ANESTHESIA_STOPES = txtAnesDate.Text.Trim() + " " + drpAnesFinHour.SelectedValue + ":" + drpAnesFinMin.SelectedValue + ":00" + " " + drpAnesFinAM.SelectedValue;
            objAnes.EA_PT_OUT_OF_OR = txtAnesDate.Text.Trim() + " " + drpTimeOutHour.SelectedValue + ":" + drpTimeOutMin.SelectedValue + ":00" + " " + drpTimeOutAM.SelectedValue;


            objAnes.EA_BP_SCORE = txtBPValue.Text;
            objAnes.EA_SPO2_SCORE = txtSPo2Value.Text;
            objAnes.EA_CONCIOUSNESS_SCORE = txtConciousnessValue.Text;
            objAnes.EA_MOTOR_FUN_SCORE = txtMotorFnValue.Text;
            objAnes.EA_PAIN_SCORE = txtPainScoreValue.Text;
            objAnes.EA_RESPIRATION_SCORE = txtRespirationValue.Text;
            objAnes.EA_ANS_CHART_FILE_NAME = lblAnesChartFileName.Text;

            //objAnes.EA_CHART_ATTACHMENT = Convert.ToString(ViewState["EA_CHART_ATTACHMENT"]);


            objAnes.UserID = Convert.ToString(Session["User_ID"]);
            objAnes.AnesthesiaAdd();

        }

        Boolean CheckPassword(string UserID, string Password)
        {
            string Criteria = " 1=1 ";
            Criteria += " AND HUM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HUM_USER_ID ='" + UserID + "' AND   HUM_USER_PASS= '" + Password + "'";

            DataSet ds = new DataSet();
            //ds = dbo.UserMasterGet(drpBranch.SelectedValue, drpUsers.SelectedValue , txtPassword.Text);
            ds = objCom.UserMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            return false;
        }


        void SetPermission()
        {
            //HCREG,HCPTENOC
            string Criteria = " 1=1 AND HRT_SCREEN_ID='EMR_ANESTHESIA' ";
            Criteria += " AND  HRT_ROLL_ID='" + Convert.ToString(Session["Roll_Id"]) + "'";

            CommonBAL objCom = new CommonBAL();
            DataSet ds = new DataSet();
            ds = objCom.RollTransGet(Criteria);

            string strPermission = "0";
            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {

                btnSave.Visible = false;
                btnUpload.Visible = false;
                // btnDeleteFav.Visible = false;
                // btnClear.Visible = false;



            }

            if (strPermission == "5")
            {
                //btnDeleteFav.Visible = false;

            }

            if (strPermission == "7")
            {
                btnSave.Visible = false;
                btnUpload.Visible = false;
                //  btnAddFav.Visible = false;
                //btnProcUpdate.Visible = false;

            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Response.Redirect("../Common/PermissionDenied.aspx");
            }
        }
        #endregion

        #region AutoExt
        [System.Web.Services.WebMethod]
        public static string[] GetProcedure(string prefixText)
        {
            DataSet ds = new DataSet();
            CommonBAL dbo = new CommonBAL();

            string[] Data;

            ds = dbo.HaadServicessListGet("Procedure", prefixText, "", strSessionDeptId);

            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["Code"]).Trim() + " ~ " + Convert.ToString(ds.Tables[0].Rows[i]["Description"]).Trim();


                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetDoctorName(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND HSFM_SF_STATUS='Present'";
            // Criteria += " AND HSFM_FNAME Like '%" + prefixText + "%'";
            Criteria += " AND ( HSFM_STAFF_ID Like '%" + prefixText + "%' OR HSFM_FNAME + ' ' +isnull(HSFM_MNAME,'') + ' '  + isnull(HSFM_LNAME,'')   like '%" + prefixText + "%' ) ";

            DataSet ds = new DataSet();
            CommonBAL objCom = new CommonBAL();
            ds = objCom.GetStaffMaster(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["HSFM_STAFF_ID"]) + "~" + Convert.ToString(ds.Tables[0].Rows[i]["FullName"]);
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }


        [System.Web.Services.WebMethod]
        public static string[] GetSegmentValue(string prefixText, int count, string contextKey)
        {
            string SegType = "", FieldID = "";
            string[] strContKey = contextKey.Split('~');

            if (strContKey.Length > 0)
            {
                SegType = strContKey[0];
                FieldID = strContKey[1];
                //SegFieldName = strContKey[2];
            }

            string[] Data;
            string Criteria = " 1=1  ";

            EMR_Anesthesia objAnes = new EMR_Anesthesia();

            Criteria += " AND EASD_TYPE='" + SegType + "' AND EASD_FIELD_ID='" + FieldID + "' ";
            Criteria += " AND EASD_COMMENT Like '%" + prefixText + "%'";

            DataSet DS = new DataSet();
            DS = objAnes.AnesthesiaSegmentDtlsGet(Criteria);




            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = DS.Tables[0].Rows[i]["EASD_COMMENT"].ToString();
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }


        [System.Web.Services.WebMethod]
        public static string[] GetDiagnosisName(string prefixText)
        {

            CommonBAL dbo = new CommonBAL();
            DataSet DS = new DataSet();

            string[] Data;

            string Criteria = " 1=1 AND DIM_STATUS='A' ";

            Criteria += " AND ( DIM_ICD_ID LIKE '" + prefixText + "' OR DIM_ICD_DESCRIPTION   LIKE '" + prefixText + "%')";
            DS = dbo.DrICDMasterGet(Criteria, "30");

            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["DIM_ICD_ID"]) + "~" + Convert.ToString(DS.Tables[0].Rows[i]["DIM_ICD_DESCRIPTION"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }



        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Convert.ToString(Session["User_ID"]) == "" || Convert.ToString(Session["User_ID"]) == null)
            {
                Session["ErrorMsg"] = "Session Expired Please login again";
                Response.Redirect("../../ErrorPage.aspx");
            }

            try
            {
                if (!IsPostBack)
                {
                    //if (Convert.ToString(Session["User_ID"]).ToLower() != "admin")
                    //{
                    //    SetPermission();
                    //}

                    strSessionDeptId = Convert.ToString(Session["User_DeptID"]);
                    CommonBAL objCom = new CommonBAL();
                    string strDate = "";
                    strDate = objCom.fnGetDate("dd/MM/yyyy");
                    txtAnesDate.Text = strDate;
                    txtSigAnestheDate.Text = strDate;

                    BindUsers();
                    BindStaff();
                    BindTime();
                    //   BindTime12Hrs();
                    BindAnesthesia();
                }


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Anesthesia.Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            generateDynamicControls();
        }



        public void generateDynamicControls()
        {

            CreateSegCtrls();



        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (drpSigAnesthesiologist.SelectedIndex == 0)
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Please enter Signature Details')", true);
                    goto FunEnd;
                }

                if (drpSigAnesthesiologist.SelectedIndex != 0)
                {
                    if (CheckPassword(drpSigAnesthesiologist.SelectedValue, txtSigAnesthePass.Text.Trim()) == false)
                    {
                        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Anesthesiologist Password wrong')", true);
                        goto FunEnd;
                    }
                }

                SaveAnesthesia();
                objCom = new CommonBAL();
                string Criteria = " EASD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND EASD_ID=" + Convert.ToString(Session["EMR_ID"]) + " AND  EASD_PT_ID='" + Convert.ToString(Session["EMR_PT_ID"]) + "'";
                objCom.fnDeleteTableData("EMR_ANESTHESIA_SEGMENT_DTLS", Criteria);

                SaveSegment();



                if (drpSigAnesthesiologist.SelectedIndex != 0)
                {
                    string strSurgeonDate = txtSigAnestheDate.Text.Trim() + " " + drpSigAnestheHour.SelectedValue + ":" + drpSigAnestheMin.SelectedValue + ":00";

                    objCom = new CommonBAL();
                    string FieldNameWithValues = " EA_ANESTHESIOLOGIST_SIGN='" + drpSigAnesthesiologist.SelectedValue + "'";
                    FieldNameWithValues += ",EA_ANESTHESIOLOGIST_SIGN_DATE=CONVERT(datetime,'" + strSurgeonDate + "',103) ";

                    string Criteria1 = " EA_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "' AND  EA_ID ='" + Convert.ToString(Session["EMR_ID"]) + "'";

                    objCom.fnUpdateTableData(FieldNameWithValues, "EMR_ANESTHESIA", Criteria1);

                }

                //objCom = new CommonBAL();
                //string FieldNameWithValues2 = " EA_CHART_ATTACHMENT='" + drpSigAnesthesiologist.SelectedValue + "'";


                //string Criteria2 = " EA_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "' AND  EA_ID ='" + Convert.ToString(Session["EMR_ID"]) + "'";

                //objCom.fnUpdateTableData(FieldNameWithValues2, "EMR_ANESTHESIA", Criteria2);


                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowMessage()", true);
            FunEnd: ;

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Anesthesia.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void drpBP_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtBPValue.Text = drpBP.SelectedValue;
        }

        protected void drpMotorFn_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtMotorFnValue.Text = drpMotorFn.SelectedValue;
        }

        protected void drpSPo2_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtSPo2Value.Text = drpSPo2.SelectedValue;
        }

        protected void drpPainScore_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtPainScoreValue.Text = drpPainScore.SelectedValue;
        }

        protected void drpConciousness_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtConciousnessValue.Text = drpConciousness.SelectedValue;
        }

        protected void drpRespiration_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtRespirationValue.Text = drpRespiration.SelectedValue;
        }

        //protected void btnDownloadTemplate_Click(object sender, EventArgs e)
        //{
        //   // string strFileName = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds;

        //    //strFileName = "AnesthesiaChart_" + strFileName + ".xlsx";

        //    string strTimeStamp = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds;



        //    string strFileName = Convert.ToString(Session["EAS_ADMISSION_NO"]) + "_" + strTimeStamp + ".xlsx";

        //    //if(Directory.Exists(@"D:\AnesthesiaChart") == false)
        //    //{
        //    //    Directory.CreateDirectory(@"D:\AnesthesiaChart");
        //    //}
        //    //string strFullPath= @"D:\AnesthesiaChart\" + strFileName  ;
        //    //File.Copy(Server.MapPath(@"..\Downloads\Template_AnesthesiaChart.xlsx"), strFullPath);

        //    ViewState["AttachedFileName"] = strFileName;
        //    ImageEditorPDC.Src = "DisplayAttachments.aspx?FileName=" + strFileName + "&FileType=Physical";

        //}

        protected void btnShowAttachment_Click(object sender, EventArgs e)
        {
            //string strFileName = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds;
            //strFileName = "AnesthesiaChart_" + strFileName + ".xlsx";
            //string pathUser = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
            //string pathDownload = pathUser + @"\Downloads\" + strFileName;
            //ViewState["AttachedFileName"] = strFileName;
            //Byte[] bytes = Convert.FromBase64String(Convert.ToString(ViewState["EA_CHART_ATTACHMENT"]));
            //File.WriteAllBytes(pathDownload, bytes);

            ImageEditorPDC.Src = "DisplayAttachments.aspx?FileName=" + lblAnesChartFileName.Text + "&FileType=Uploaded";
        }

        /*
         protected void btnUploadChart_Click(object sender, EventArgs e)
         {
             string pathUser = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
             string pathDownload = pathUser + @"\Downloads\";
             byte[] bytes = System.IO.File.ReadAllBytes(pathDownload + Convert.ToString(ViewState["AttachedFileName"]));

             string strData;
             strData=  Convert.ToBase64String(bytes);

           
         }
         */

        protected void lnkDownloadTemplate_Click(object sender, EventArgs e)
        {
            string strTimeStamp = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds;

            string strFileName = Convert.ToString(Session["EAS_ADMISSION_NO"]) + "_" + strTimeStamp + ".xlsx";

            lblTemplateName.Text = strFileName;
            ImageEditorPDC.Src = "DisplayAttachments.aspx?FileName=" + strFileName + "&FileType=Template";

        }


        protected void btnUpload_Click(object sender, EventArgs e)
        {
            lblAnesChartFileName.Text = "";

            string SCanPath, FileExt = "";

            SCanPath = Convert.ToString(Session["EMR_ANES_CHART_FILE_PATH"]);

            if (!Directory.Exists(SCanPath))
            {
                Directory.CreateDirectory(SCanPath);
            }
            // string strTimeStamp = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds;

            // FileExt = Path.GetExtension(fileLogo.FileName);

            string AnesChartFileName = fileLogo.FileName;// Convert.ToString(Session["EAS_ADMISSION_NO"]) + "_" + strTimeStamp + FileExt;

            fileLogo.SaveAs(SCanPath + AnesChartFileName);

            lblAnesChartFileName.Text = AnesChartFileName;
        }

        protected void lnkhowAttachment_Click(object sender, EventArgs e)
        {
            ImageEditorPDC.Src = "DisplayAttachments.aspx?FileName=" + lblAnesChartFileName.Text + "&FileType=Uploaded";
        }

    }
}