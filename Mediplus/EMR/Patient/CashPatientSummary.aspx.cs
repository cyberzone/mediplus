﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EMR_BAL;
using System.Data;
using System.IO;

namespace Mediplus.EMR.Patient
{
    public partial class CashPatientSummary : System.Web.UI.Page
    {
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindData(string EMR_ID)
        {
            txtChiefComplaients.Text = "";
            txtAssessment.Text = "";
            txtTreatment.Text = "";
            txtMedication.Text = "";

            string Criteria1 = " 1=1  AND ECS_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria1 += " AND ECS_ID = '" + EMR_ID + "' AND ECS_PT_ID='" + Convert.ToString(Session["EMR_PT_ID"]) + "'";

            DataSet ds1 = new DataSet();
            EMR_PTCashSummary objPTCS = new EMR_PTCashSummary();
            ds1 = objPTCS.EMRPTCashSummaryGet(Criteria1);
            if (ds1.Tables[0].Rows.Count > 0)
            {

                foreach (DataRow DR in ds1.Tables[0].Rows)
                {
                    txtChiefComplaients.Text = Convert.ToString(DR["ECS_CC"]);
                    txtAssessment.Text = Convert.ToString(DR["ECS_ASSESSMENT"]);
                    txtTreatment.Text = Convert.ToString(DR["ECS_TREATMENT"]);
                    txtMedication.Text = Convert.ToString(DR["ECS_MEDICATION"]);

                }
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../../Default.aspx?NoSession=1"); }

            try
            {
                if (!IsPostBack)
                {
                    if (Convert.ToString(Session["EPM_STATUS"]).ToUpper() == "Y" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                    {
                        btnSave.Visible = false;
                    }
                    //HIDE THE SAVE OPTION FOR NEXT DAY OF THE VISIT
                    if (Convert.ToString(Session["EMRCurrentDateDiff"]) != "0" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                    {
                        btnSave.Visible = false;
                    }
                    BindData(Convert.ToString(Session["EMR_ID"]));
                    TreeNode RootNode = OutputDirectory(Convert.ToString(Session["HPV_DEP_NAME"]), null);
                    // TreeNode treeNode = new TreeNode(Convert.ToString(Session["HPV_DEP_NAME"]));
                    MyTree.Nodes.Add(RootNode);


                    if (MyTree.Nodes.Count > 0)
                    {
                        MyTree.Nodes[0].Expand();
                    }
                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "       CashPatientSummary.Index.Page_Load");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                EMR_PTCashSummary objPTCS = new EMR_PTCashSummary();
                objPTCS.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objPTCS.ECS_ID = Convert.ToString(Session["EMR_ID"]);
                objPTCS.ECS_PT_ID = Convert.ToString(Session["EMR_PT_ID"]);
                objPTCS.ECS_CC = txtChiefComplaients.Text.Trim();
                objPTCS.ECS_ASSESSMENT = txtAssessment.Text.Trim();
                objPTCS.ECS_TREATMENT = txtTreatment.Text.Trim();
                objPTCS.ECS_MEDICATION = txtMedication.Text.Trim();
                objPTCS.EMRPTCashSummaryAdd();
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowMessage()", true);
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "       CashPatientSummary.Index.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }


        TreeNode OutputDirectory(String strDeptName, TreeNode parentNode)
        {

            // validate param
            if (strDeptName == null) return null;
            // create a node for this directory
            TreeNode DirNode = new TreeNode(strDeptName);


            string Criteria1 = " 1=1  AND EPM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria1 += " AND EPM_DEP_NAME = '" + strDeptName + "' AND EPM_PT_ID='" + Convert.ToString(Session["EMR_PT_ID"]) + "'";
            Criteria1 += " AND  EPM_ID IN (SELECT  ECS_ID FROM EMR_PT_CASH_SUMMARY WHERE ECS_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "' AND ECS_PT_ID='" + Convert.ToString(Session["EMR_PT_ID"]) + "' )";

            DataSet DS = new DataSet();
            EMR_PTMasterBAL objPTM = new EMR_PTMasterBAL();
            DS = objPTM.GetEMR_PTMaster(Criteria1);
            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    TreeNode child = new TreeNode(Convert.ToString(DR["EPM_DateDesc"]));
                    DirNode.ChildNodes.Add(child);
                    child.ToolTip = Convert.ToString(DR["EPM_ID"]);
                    child.ImageUrl = @"..\Images\Folder.jpg";

                }
            }

            // otherwise add this node to the parent and return the parent
            if (parentNode == null)
            {
                return DirNode;
            }
            else
            {
                parentNode.ChildNodes.Add(DirNode);
                return parentNode;
            }
        }


        protected void MyTree_SelectedNodeChanged(object sender, EventArgs e)
        {
            string Parent1 = "", Parent2 = "";


            //MyTree.SelectedNode.Parent.Text 
            //MyTree.SelectedNode.Parent.Parent.Text 

            if (MyTree.SelectedNode.ChildNodes.Count == 0)
            {
                //  ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowAttachment('" + Convert.ToString(Session["EMR_PT_ID"]) + "','" + MyTree.SelectedNode.Text + "');", true);

                //  imgAttach.ImageUrl = "DisplayAttachments.aspx?PT_ID=" + Convert.ToString(Session["EMR_PT_ID"]) + "&FileName=" + MyTree.SelectedNode.Text;

                string strFileName = MyTree.SelectedNode.Text;
                if (GlobalValues.FileDescription.ToUpper() == "NEWHEART")
                {
                    if (strFileName.LastIndexOf("_") > 0)
                    {
                        strFileName = strFileName.Substring(0, strFileName.LastIndexOf("_"));
                    }
                }

                ViewState["FileName"] = strFileName;

                //   Parent1 = MyTree.SelectedNode.Parent.Parent.Text;//"03-08-2015"
                //  Parent2 = MyTree.SelectedNode.Parent.Text;//"general"


                BindData(MyTree.SelectedNode.ToolTip);




            }


        }
    }
}