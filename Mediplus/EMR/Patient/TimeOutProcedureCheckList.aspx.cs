﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EMR_BAL;
using System.Data;
using System.IO;

namespace Mediplus.EMR.Patient
{
    public partial class TimeOutProcedureCheckList : System.Web.UI.Page
    {
        DataSet DS = new DataSet();
        CommonBAL objCom = new CommonBAL();
        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindData()
        {
            objCom = new CommonBAL();
            DS = new DataSet();
            string Criteria = " 1=1 ";

            Criteria += " AND TOP_TYPE='PART1' ";

            DS = objCom.fnGetFieldValue(" * ", "EMR_TIMEOUT_PROCEDURE_MASTER", Criteria, "TOP_ORDER");
            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    string strValue="", strValueYes="";
                    Criteria = " 1=1 ";
                    Criteria += " and TOPD_FIELD_ID='" + Convert.ToString(DR["TOP_FIELD_ID"]) + "'";
                    BindOPDData(Criteria, out strValue, out strValueYes);
                    if (Convert.ToString(DR["TOP_FIELD_NAME"]).ToUpper() == "NAMEOFPROCEDURE")
                    {
                        NameOfProcedure.Text = strValue;
                    }
                }
            }



          
            DS = new DataSet();

             Criteria = " 1=1 ";
            Criteria += " AND TOP_TYPE='PART2' ";


            DS = objCom.fnGetFieldValue(" * ", "EMR_TIMEOUT_PROCEDURE_MASTER", Criteria, "TOP_ORDER");
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvOPDAssessment.DataSource = DS;
                gvOPDAssessment.DataBind();

            }
            else
            {
                gvOPDAssessment.DataBind();
            }
        }


        void BindOPDData(string Criteria, out string strValue, out string strValueYes)
        {

            strValue = "";
            strValueYes = "";
            DataSet DS = new DataSet();

            if (ViewState["EMR_OPD"] != "" && ViewState["EMR_OPD"] != null)
            {

                DS = (DataSet)ViewState["EMR_OPD"];
                DataRow[] result = DS.Tables[0].Select(Criteria);
                foreach (DataRow DR in result)
                {

                    strValueYes = Convert.ToString(DR["TOPD_FIELD_VALUE"]);
                    strValue = Convert.ToString(DR["TOPD_DETAILS"]);
                }
            }


        }

        void BindOPDDataAll()
        {

            DataSet ds = new DataSet();
            EMR_OPDAssessment objOPD = new EMR_OPDAssessment();

            string Criteria = " 1=1   and TOPD_PT_ID ='" + Convert.ToString(Session["EMR_PT_ID"]) + "' AND TOPD_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
            CommonBAL objCom = new CommonBAL();
            ds = objCom.fnGetFieldValue(" * ", "EMR_TIMEOUT_PROCEDURE_DTLS", Criteria, "TOPD_FIELD_ID");

            if (ds.Tables[0].Rows.Count > 0)
            {
                ViewState["EMR_OPD"] = ds;
            }

        }



        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Convert.ToString(Session["User_ID"]) == "" || Convert.ToString(Session["User_ID"]) == null)
            {
                Session["ErrorMsg"] = "Session Expired Please login again";
                Response.Redirect("../../ErrorPage.aspx");
            }

            try
            {
                if (!IsPostBack)
                {

                    BindOPDDataAll();
                    BindData();

                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "       OPDAssessment.Index.Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void gvOPDAssessment_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblLevelType = (Label)e.Row.FindControl("lblLevelType");
                Label lblFieldID = (Label)e.Row.FindControl("lblFieldID");
                RadioButtonList radPTAnswer = (RadioButtonList)e.Row.FindControl("radPTAnswer");
                TextBox txtDtls = (TextBox)e.Row.FindControl("txtDtls");
                Label lblFieldName = (Label)e.Row.FindControl("lblFieldName");

                if (lblFieldName.Text.ToUpper() == "Nurse".ToUpper())
                {
                    txtDtls.Visible = true;
                }
                else
                {
                    txtDtls.Visible = false;
                }
                if (lblLevelType.Text == "1")
                {
                    radPTAnswer.Visible = false;
                    txtDtls.Visible = false;
                }
                else
                {
                    string Criteria = " 1=1 ";
                    Criteria += " and TOPD_FIELD_ID='" + lblFieldID.Text + "'";
                    string strValue, strValueYes;
                    BindOPDData(Criteria, out strValue, out strValueYes);
                    radPTAnswer.SelectedValue = strValueYes;
                    txtDtls.Text = strValue;

                   
                }

            }
        }


        #endregion


        protected void btnSave1_Click(object sender, EventArgs e)
        {
            try
            {
                DataSet ds = new DataSet();
                CommonBAL objCom = new CommonBAL();
                EMR_TimeOutProcedure objOPD = new EMR_TimeOutProcedure();

                string Criteria = " 1=1 ";
                Criteria += " AND TOP_TYPE='PART1' ";
                ds = objCom.fnGetFieldValue(" * ", "EMR_TIMEOUT_PROCEDURE_MASTER", Criteria, "TOP_ORDER");
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow DR in ds.Tables[0].Rows)
                    {


                        objOPD = new EMR_TimeOutProcedure();
                        objOPD.branchid = Convert.ToString(Session["Branch_ID"]);
                        objOPD.patientmasterid = Convert.ToString(Session["EMR_ID"]);
                        objOPD.fieldid = Convert.ToString(DR["TOP_FIELD_ID"]);
                        objOPD.PatientID = Convert.ToString(Session["EMR_PT_ID"]);

                        if (Convert.ToString(DR["TOP_FIELD_NAME"]).ToUpper() == "NAMEOFPROCEDURE" && NameOfProcedure.Text != "")
                        {
                            objOPD.value = "1";
                            objOPD.comment = NameOfProcedure.Text;
                            objOPD.EMRTimeOutProcedureDtlsAdd();
                        }

                    }

                }

                for (Int32 i = 0; i < gvOPDAssessment.Rows.Count; i++)
                {

                    Label lblLevelType = (Label)gvOPDAssessment.Rows[i].FindControl("lblLevelType");
                    Label lblFieldID = (Label)gvOPDAssessment.Rows[i].FindControl("lblFieldID");
                    RadioButtonList radPTAnswer = (RadioButtonList)gvOPDAssessment.Rows[i].FindControl("radPTAnswer");
                    TextBox txtDtls = (TextBox)gvOPDAssessment.Rows[i].FindControl("txtDtls");

                    if (lblLevelType.Text != "1" && radPTAnswer.SelectedValue != "")
                    {
                        objOPD = new EMR_TimeOutProcedure();
                        objOPD.branchid = Convert.ToString(Session["Branch_ID"]);
                        objOPD.patientmasterid = Convert.ToString(Session["EMR_ID"]);
                        objOPD.fieldid = lblFieldID.Text;
                        objOPD.value = radPTAnswer.SelectedValue;
                        objOPD.comment = txtDtls.Text;
                        objOPD.PatientID = Convert.ToString(Session["EMR_PT_ID"]);
                        objOPD.EMRTimeOutProcedureDtlsAdd();
                    }


                }

                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowMessage()", true);
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "       EducationalForm.Index.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }


    }
}