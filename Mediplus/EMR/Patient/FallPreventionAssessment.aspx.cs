﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EMR_BAL;
using System.Data;
using System.IO;

namespace Mediplus.EMR.Patient
{
    public partial class FallPreventionAssessment : System.Web.UI.Page
    {
        public string AssessmentHistory;
        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindData()
        {


            DataSet ds = new DataSet();
            CommonBAL objCom = new CommonBAL();
            ds = objCom.fnGetFieldValue(" * ", "EMR_FALLPREVENTION_ASSESSMENT_MASTER", "1=1 AND EPA_TYPE='PART1' ", "FPA_ORDER");
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvOPDAssessment.DataSource = ds;
                gvOPDAssessment.DataBind();

            }
            else
            {
                gvOPDAssessment.DataBind();
            }
        }

        void BindData1()
        {


            DataSet ds = new DataSet();
            CommonBAL objCom = new CommonBAL();
            ds = objCom.fnGetFieldValue(" * ", "EMR_FALLPREVENTION_ASSESSMENT_MASTER", "1=1 AND EPA_TYPE='PART2' ", "FPA_ORDER");
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvPart2.DataSource = ds;
                gvPart2.DataBind();

            }
            else
            {
                gvPart2.DataBind();
            }
        }

        void BindOPDData(string Criteria, out string strValue, out string strValueYes)
        {

            strValue = "";
            strValueYes = "";
            DataSet DS = new DataSet();

            if (ViewState["EMR_OPD"] != "" && ViewState["EMR_OPD"] != null)
            {

                DS = (DataSet)ViewState["EMR_OPD"];
                DataRow[] result = DS.Tables[0].Select(Criteria);
                foreach (DataRow DR in result)
                {

                    strValueYes = Convert.ToString(DR["FPAD_FIELD_VALUE"]);
                    strValue = Convert.ToString(DR["FPAD_DETAILS"]);
                }
            }


        }

        void BindOPDDataAll()
        {

            DataSet ds = new DataSet();
            EMR_OPDAssessment objOPD = new EMR_OPDAssessment();

            string Criteria = " 1=1   and FPAD_PT_ID ='" + Convert.ToString(Session["EMR_PT_ID"]) + "' AND FPAD_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
            CommonBAL objCom = new CommonBAL();
            ds = objCom.fnGetFieldValue(" * ", "EMR_FALLPREVENTION_ASESSMENT_DTLS", Criteria, "FPAD_FIELD_ID");

            if (ds.Tables[0].Rows.Count > 0)
            {
                ViewState["EMR_OPD"] = ds;
            }

        }



        void BindNotApplicable()
        {

            DataSet ds = new DataSet();
            EMR_OPDAssessment objOPD = new EMR_OPDAssessment();

            string Criteria = " 1=1   and FPAD_PT_ID ='" + Convert.ToString(Session["EMR_PT_ID"]) + "' AND FPAD_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
            Criteria += " AND FPAD_FIELD_ID =201 ";

            CommonBAL objCom = new CommonBAL();
            ds = objCom.fnGetFieldValue(" * ", "EMR_FALLPREVENTION_ASESSMENT_DTLS", Criteria, "FPAD_FIELD_ID");

            if (ds.Tables[0].Rows.Count > 0)
            {
                if (Convert.ToString(ds.Tables[0].Rows[0]["FPAD_FIELD_VALUE"]) == "Y")
                {
                    chkNotApplicable.Checked = true;
                    chkNotApplicable_CheckedChanged(chkNotApplicable, new EventArgs());
                }
                else
                {
                    chkNotApplicable.Checked = false;
                }
            }

        }

        void CalculateTotalScore()
        {
            Int64 intTotalScore = 0;
            for (int intCurRow = 0; intCurRow < gvOPDAssessment.Rows.Count; intCurRow++)
            {
                TextBox txtDtls = (TextBox)gvOPDAssessment.Rows[intCurRow].FindControl("txtDtls");

                if (txtDtls.Text != "")
                {
                    intTotalScore += Convert.ToInt64(txtDtls.Text);
                }
            }
            TextBox txtTotalScore = (TextBox)gvOPDAssessment.FooterRow.FindControl("txtTotalScore");

            txtTotalScore.Text = Convert.ToString(intTotalScore);


        }

        void BindAuditDtls()
        {
            DataSet DS = new DataSet();
            EMR_FallPreventionAssessment objOPD = new EMR_FallPreventionAssessment();
            string Criteria = " 1=1   AND FPA_PT_ID ='" + Convert.ToString(Session["EMR_PT_ID"]) + "' AND FPA_ID = '" + Convert.ToString(Session["EMR_ID"]) + "'";

            DS = objOPD.EMRFallPreventionAsseGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                lblCreatedBy.Text = Convert.ToString(DS.Tables[0].Rows[0]["CreatedUserName"]);
                lblCreatedDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["CreatedDate"]);
                lblModifiedBy.Text = Convert.ToString(DS.Tables[0].Rows[0]["ModifiedUserName"]);
                lblModifiedDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["ModifiedDate"]);
            }
        }

        void UpdateAuditDtls()
        {
            CommonBAL objCom = new CommonBAL();
            objCom.EMRPTMasterUserDtlsUpdate(Convert.ToString(Session["EMR_ID"]), Convert.ToString(Session["User_ID"]));
        }


        //NOT IN USE
        void BindAssessmentDtlHistory(string Criteria, out string strValue, out string strValueYes)
        {

            strValue = "";
            strValueYes = "";
            DataSet DS = new DataSet();

            if (ViewState["EMR_OPD_HISTORY"] != "" && ViewState["EMR_OPD_HISTORY"] != null)
            {

                DS = (DataSet)ViewState["EMR_OPD_HISTORY"];
                DataRow[] result = DS.Tables[0].Select(Criteria);
                foreach (DataRow DR in result)
                {

                    strValueYes = Convert.ToString(DR["FPAD_FIELD_VALUE"]);
                    strValue = Convert.ToString(DR["FPAD_DETAILS"]);
                }
            }


        }

        //NOT IN USE
        void BindAssessmentDtlHistoryAll()
        {

            DataSet ds = new DataSet();
            EMR_OPDAssessment objOPD = new EMR_OPDAssessment();

            string Criteria = " 1=1   and FPAD_PT_ID ='" + Convert.ToString(Session["EMR_PT_ID"]) + "' AND FPAD_ID != '" + Convert.ToString(Session["EMR_ID"]) + "'";
            CommonBAL objCom = new CommonBAL();
            ds = objCom.fnGetFieldValue(" * ", "EMR_FALLPREVENTION_ASESSMENT_DTLS", Criteria, "FPAD_FIELD_ID");

            if (ds.Tables[0].Rows.Count > 0)
            {
                ViewState["EMR_OPD_HISTORY"] = ds;
            }

        }
        //NOT IN USE
        void BindAssessmentHistory()
        {
            CommonBAL objCom = new CommonBAL();
            DataSet DSDtls = new DataSet();
            string CriteriaDtls = " 1=1   AND FPAD_PT_ID ='" + Convert.ToString(Session["EMR_PT_ID"]) + "' AND FPAD_ID != '" + Convert.ToString(Session["EMR_ID"]) + "'";
            DSDtls = objCom.fnGetFieldValue(" DISTINCT FPAD_ID  ", "EMR_FALLPREVENTION_ASESSMENT_DTLS", CriteriaDtls, "FPAD_ID");

            if (DSDtls.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DRDtls in DSDtls.Tables[0].Rows)
                {
                    string strHeader = "";
                    DataSet DS = new DataSet();
                    objCom = new CommonBAL();
                    DS = objCom.fnGetFieldValue(" * ", "EMR_FALLPREVENTION_ASSESSMENT_MASTER", "1=1 AND EPA_TYPE='PART1' ", "FPA_ORDER");
                    if (DS.Tables[0].Rows.Count > 0)
                    {
                        string Criteria = " 1=1 ";
                        Criteria += " AND FPAD_ID=" + Convert.ToString(DRDtls["FPAD_ID"]);
                        Criteria += " AND FPAD_FIELD_ID=203";
                        string strUser, strValueUserYes;
                        BindAssessmentDtlHistory(Criteria, out strUser, out strValueUserYes);

                        Criteria = " 1=1 ";
                        Criteria += " AND FPAD_ID=" + Convert.ToString(DRDtls["FPAD_ID"]);
                        Criteria += " AND FPAD_FIELD_ID=204";
                        string strDate, strDateYes;
                        BindAssessmentDtlHistory(Criteria, out strDate, out strDateYes);

                        strHeader += "<table cellpadding='0' cellspacing='0' style='width:100%;' >";
                        strHeader += "<tr><td style='height:10px;font-weight:bold;' class='lblCaption1' >User Name: " + strUser + "</td>";
                        strHeader += "<td style='height:10px;font-weight:bold;' class='lblCaption1' >Date: " + strDate + "</td>";

                        strHeader += "</tr>";
                        strHeader += "<tr>";
                        strHeader += "<td   style=' border: 1px solid #dcdcdc;height:25px;width:500px;font-weight:bold;' class='lblCaption1 BoldStyle BorderStyle' colspan='2' >Description</td>";
                        strHeader += "<td   style=' border: 1px solid #dcdcdc;height:25px;width:50px;font-weight:bold;' class='lblCaption1 BoldStyle BorderStyle' colspan='2' >Scale</td>";
                        strHeader += "<td style=' border: 1px solid #dcdcdc;height:25px;width:50px;font-weight:bold;' class='lblCaption1 BoldStyle BorderStyle' colspan='2' >Scoring</td>";

                        strHeader += "</tr>";
                        Int32 intTotalScore = 0;


                        foreach (DataRow DR in DS.Tables[0].Rows)
                        {
                            Criteria = " 1=1  AND FPAD_ID=" + Convert.ToString(DRDtls["FPAD_ID"]);
                            Criteria += " AND FPAD_FIELD_ID='" + Convert.ToString(DR["FPA_FIELD_ID"]) + "'";
                            string strValue, strValueYes;
                            BindAssessmentDtlHistory(Criteria, out strValue, out strValueYes);

                            string strValueYesDesc = "";
                            if (strValueYes == "Y")
                            {
                                strValueYes = "Yes";
                            }
                            else if (strValueYes == "N")
                            {
                                strValueYes = "No";
                            }
                            strHeader += "<tr>";
                            strHeader += "<td style=' border: 1px solid #dcdcdc;height:25px;width:500px;' class='lblCaption1  BorderStyle' colspan='2' >" + Convert.ToString(DR["FPA_FIELD_NAME"]) + "</td>";
                            strHeader += "<td style=' border: 1px solid #dcdcdc;height:25px;width:50px;' class='lblCaption1  BorderStyle' colspan='2' >" + strValueYesDesc + "</td>";
                            strHeader += "<td style=' border: 1px solid #dcdcdc;height:25px;width:50px;text-align:right;padding-right:5px;' class='lblCaption1  BorderStyle' colspan='2' >" + strValue + "</td>";

                            strHeader += "</tr>";
                            if (strValue != "")
                            {
                                intTotalScore = intTotalScore + Convert.ToInt32(strValue);
                            }
                        }
                        strHeader += "<tr><td style='height:10px;font-weight:bold;text-align:right;padding-right:5px;' class='lblCaption1' colspan='6' >TotalScore: " + Convert.ToString(intTotalScore) + "</td>";

                        strHeader += "</tr>";
                        strHeader += "<tr><td style='height:10px;' ></td></tr>";
                        strHeader += "</table";
                        AssessmentHistory += strHeader;
                    }
                }
            }
        }

        public void BindPreventionAsse()
        {
            DataSet DS = new DataSet();
            EMR_FallPreventionAssessment objOPD = new EMR_FallPreventionAssessment();

            string Criteria = " 1=1   AND FPA_PT_ID ='" + Convert.ToString(Session["EMR_PT_ID"]) + "' AND FPA_ID = '" + Convert.ToString(Session["EMR_ID"]) + "'";

            DS = objOPD.EMRFallPreventionAsseGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("FPA_NOT_APPLICABLE") == false && Convert.ToString(DS.Tables[0].Rows[0]["FPA_NOT_APPLICABLE"]) !="")
                {
                    chkNotApplicable.Checked = Convert.ToBoolean(DS.Tables[0].Rows[0]["FPA_NOT_APPLICABLE"]);
                    chkNotApplicable_CheckedChanged(chkNotApplicable, new EventArgs());
                }
            }
            

        }
        public void BindallPreventionAsse()
        {
            DataSet DS = new DataSet();
            EMR_FallPreventionAssessment objOPD = new EMR_FallPreventionAssessment();

            string Criteria = " 1=1   AND FPA_PT_ID ='" + Convert.ToString(Session["EMR_PT_ID"]) + "' AND FPA_ID != '" + Convert.ToString(Session["EMR_ID"]) + "'";

            DS = objOPD.EMRFallPreventionAsseGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvAssessment.DataSource = DS;
                gvAssessment.DataBind();
            }
            else
            {
                gvAssessment.DataBind();
            }

        }
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Convert.ToString(Session["User_ID"]) == "" || Convert.ToString(Session["User_ID"]) == null)
            {
                Session["ErrorMsg"] = "Session Expired Please login again";
                Response.Redirect("../../ErrorPage.aspx");
            }

            try
            {
                if (!IsPostBack)
                {
                    if (Convert.ToString(Session["EPM_STATUS"]).ToUpper() == "Y" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                    {
                        btnSave.Visible = false;
                    }
                    //HIDE THE SAVE OPTION FOR NEXT DAY OF THE VISIT
                    if (Convert.ToString(Session["EMRCurrentDateDiff"]) != "0" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                    {
                        btnSave.Visible = false;
                    }
                    if (Convert.ToString(Session["EMR_DISPLAY_AUDIT"]) == "1")
                    {
                        BindAuditDtls();
                        divAuditDtls.Visible = true;
                    }

                    BindOPDDataAll();
                    BindData();
                    BindData1();
                    CalculateTotalScore();

                   // BindNotApplicable();

                    //BindAssessmentDtlHistoryAll();
                    //BindAssessmentHistory();
                    BindPreventionAsse();
                    BindallPreventionAsse();
                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "       OPDAssessment.Index.Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void gvOPDAssessment_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                Label lblDescription = (Label)e.Row.FindControl("lblDescription");
                Label lblLevelType = (Label)e.Row.FindControl("lblLevelType");
                Label lblFieldID = (Label)e.Row.FindControl("lblFieldID");
                RadioButtonList radPTAnswer = (RadioButtonList)e.Row.FindControl("radPTAnswer");
                TextBox txtDtls = (TextBox)e.Row.FindControl("txtDtls");

                if (lblLevelType.Text == "1")
                {
                    radPTAnswer.Visible = false;
                    txtDtls.Visible = false;
                    lblDescription.Font.Bold = true;
                }
                else
                {
                    string Criteria = " 1=1 ";
                    Criteria += " and FPAD_FIELD_ID='" + lblFieldID.Text + "'";
                    string strValue, strValueYes;
                    BindOPDData(Criteria, out strValue, out strValueYes);
                    radPTAnswer.SelectedValue = strValueYes;
                    txtDtls.Text = strValue;
                }

            }
        }

        protected void gvPart2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblLevelType1 = (Label)e.Row.FindControl("lblLevelType1");
                Label lblFieldID1 = (Label)e.Row.FindControl("lblFieldID1");
                CheckBox chkPTAnswer = (CheckBox)e.Row.FindControl("chkPTAnswer");


                if (lblLevelType1.Text == "1")
                {
                    chkPTAnswer.Visible = false;

                }
                else
                {
                    string Criteria = " 1=1 ";
                    Criteria += " and FPAD_FIELD_ID='" + lblFieldID1.Text + "'";
                    string strValue, strValueYes;
                    BindOPDData(Criteria, out strValue, out strValueYes);

                    if (strValueYes.ToUpper() == "Y")
                    {
                        chkPTAnswer.Checked = true;
                    }
                    else
                    {
                        chkPTAnswer.Checked = false;
                    }

                }

            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                TextBox txtTotalScore = (TextBox)gvOPDAssessment.FooterRow.FindControl("txtTotalScore");
                EMR_FallPreventionAssessment objOPD = new EMR_FallPreventionAssessment();


                objOPD = new EMR_FallPreventionAssessment();
                objOPD.BranchID = Convert.ToString(Session["Branch_ID"]);
                objOPD.PatientmasterID = Convert.ToString(Session["EMR_ID"]);
                objOPD.PatientID = Convert.ToString(Session["EMR_PT_ID"]);
                objOPD.FPA_NOT_APPLICABLE = Convert.ToString(chkNotApplicable.Checked);
                objOPD.FPA_TOTAL_SCORE = txtTotalScore.Text;
                objOPD.UserID = Convert.ToString(Session["User_ID"]);

                objOPD.EMRFallPreventionAsseAdd();



                if (chkNotApplicable.Checked == false)
                {


                    for (Int32 i = 0; i < gvOPDAssessment.Rows.Count; i++)
                    {

                        Label lblLevelType = (Label)gvOPDAssessment.Rows[i].FindControl("lblLevelType");
                        Label lblFieldID = (Label)gvOPDAssessment.Rows[i].FindControl("lblFieldID");
                        RadioButtonList radPTAnswer = (RadioButtonList)gvOPDAssessment.Rows[i].FindControl("radPTAnswer");
                        TextBox txtDtls = (TextBox)gvOPDAssessment.Rows[i].FindControl("txtDtls");

                        if (lblLevelType.Text != "1" && radPTAnswer.SelectedValue != "")
                        {
                            objOPD = new EMR_FallPreventionAssessment();
                            objOPD.BranchID = Convert.ToString(Session["Branch_ID"]);
                            objOPD.PatientmasterID = Convert.ToString(Session["EMR_ID"]);
                            objOPD.fieldid = lblFieldID.Text;
                            objOPD.value = radPTAnswer.SelectedValue;
                            objOPD.comment = txtDtls.Text;
                            objOPD.PatientID = Convert.ToString(Session["EMR_PT_ID"]);
                            if (radPTAnswer.SelectedValue == "Y" || radPTAnswer.SelectedValue == "N")
                            {
                                objOPD.EMRFallPreventionAsseDtlsAdd();
                            }
                        }


                    }

                    for (Int32 i = 0; i < gvPart2.Rows.Count; i++)
                    {

                        Label lblLevelType1 = (Label)gvPart2.Rows[i].FindControl("lblLevelType1");
                        Label lblFieldID1 = (Label)gvPart2.Rows[i].FindControl("lblFieldID1");
                        CheckBox chkPTAnswer = (CheckBox)gvPart2.Rows[i].FindControl("chkPTAnswer");


                        if (lblLevelType1.Text != "1")
                        {
                            objOPD = new EMR_FallPreventionAssessment();
                            objOPD.BranchID = Convert.ToString(Session["Branch_ID"]);
                            objOPD.PatientmasterID = Convert.ToString(Session["EMR_ID"]);
                            objOPD.fieldid = lblFieldID1.Text;
                            if (chkPTAnswer.Checked == true)
                            {
                                objOPD.value = "Y";
                            }
                            else
                            {
                                objOPD.value = "N";
                            }
                            objOPD.comment = "";
                            objOPD.PatientID = Convert.ToString(Session["EMR_PT_ID"]);

                            objOPD.EMRFallPreventionAsseDtlsAdd();

                        }


                    }


                    //objOPD.BranchID = Convert.ToString(Session["Branch_ID"]);
                    //objOPD.PatientmasterID = Convert.ToString(Session["EMR_ID"]);
                    //objOPD.fieldid = "202";//"Total Score"
                    //objOPD.value = "Y";
                    //objOPD.comment = txtTotalScore.Text;
                    //objOPD.PatientID = Convert.ToString(Session["EMR_PT_ID"]);
                    //objOPD.EMRFallPreventionAsseDtlsAdd();

                    //objOPD.BranchID = Convert.ToString(Session["Branch_ID"]);
                    //objOPD.PatientmasterID = Convert.ToString(Session["EMR_ID"]);
                    //objOPD.fieldid = "203";//Created User
                    //objOPD.value = "Y";
                    //objOPD.comment = Convert.ToString(Session["User_Name"]);
                    //objOPD.PatientID = Convert.ToString(Session["EMR_PT_ID"]);
                    //objOPD.EMRFallPreventionAsseDtlsAdd();

                    //DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                    //string strDate = strFromDate.ToString("dd/MM/yyyy");

                    //objOPD.BranchID = Convert.ToString(Session["Branch_ID"]);
                    //objOPD.PatientmasterID = Convert.ToString(Session["EMR_ID"]);
                    //objOPD.fieldid = "204";//Created User
                    //objOPD.value = "Y";
                    //objOPD.comment = strDate;
                    //objOPD.PatientID = Convert.ToString(Session["EMR_PT_ID"]);
                    //objOPD.EMRFallPreventionAsseDtlsAdd();

                }
                else
                {
                    CommonBAL objCom = new CommonBAL();
                    string Criteria = " 1=1   and FPAD_PT_ID ='" + Convert.ToString(Session["EMR_PT_ID"]) + "' AND FPAD_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
                    objCom.fnDeleteTableData("EMR_FALLPREVENTION_ASESSMENT_DTLS", Criteria);

                }

                objOPD.BranchID = Convert.ToString(Session["Branch_ID"]);
                objOPD.PatientmasterID = Convert.ToString(Session["EMR_ID"]);
                objOPD.fieldid = "201";
                if (chkNotApplicable.Checked == true)
                {
                    objOPD.value = "Y";
                }
                else
                {
                    objOPD.value = "N";
                }
                objOPD.comment = "NotApplicable";
                objOPD.PatientID = Convert.ToString(Session["EMR_PT_ID"]);
                objOPD.EMRFallPreventionAsseDtlsAdd();

                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowMessage()", true);
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "       EducationalForm.Index.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void radPTAnswer_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GridViewRow currentRow = (GridViewRow)((RadioButtonList)sender).Parent.Parent.Parent.Parent;
                Int32 R = currentRow.RowIndex;
                Label lblValue = (Label)gvOPDAssessment.Rows[R].FindControl("lblValue");
                RadioButtonList radPTAnswer = (RadioButtonList)gvOPDAssessment.Rows[R].FindControl("radPTAnswer");

                TextBox txtDtls = (TextBox)gvOPDAssessment.Rows[R].FindControl("txtDtls");

                if (radPTAnswer.SelectedValue == "Y")
                {
                    txtDtls.Text = lblValue.Text;
                }
                else
                {
                    txtDtls.Text = "";
                }
                CalculateTotalScore();
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eAuthApproval.gvInvoiceTrans_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void chkNotApplicable_CheckedChanged(object sender, EventArgs e)
        {

            for (Int32 i = 0; i < gvOPDAssessment.Rows.Count; i++)
            {


                RadioButtonList radPTAnswer = (RadioButtonList)gvOPDAssessment.Rows[i].FindControl("radPTAnswer");
                TextBox txtDtls = (TextBox)gvOPDAssessment.Rows[i].FindControl("txtDtls");
                TextBox txtTotalScore = (TextBox)gvOPDAssessment.FooterRow.FindControl("txtTotalScore");
                if (chkNotApplicable.Checked == true)
                {
                    radPTAnswer.Enabled = false;

                    txtDtls.Text = "";
                    txtDtls.Enabled = false;
                    txtTotalScore.Text = "";
                    txtTotalScore.Enabled = false;
                    radPTAnswer.SelectedIndex = -1;


                }
                else
                {
                    radPTAnswer.Enabled = true;
                    txtDtls.Enabled = true;
                    txtTotalScore.Enabled = true;
                }

            }

            for (Int32 i = 0; i < gvPart2.Rows.Count; i++)
            {


                CheckBox chkPTAnswer = (CheckBox)gvPart2.Rows[i].FindControl("chkPTAnswer");
                if (chkNotApplicable.Checked == true)
                {
                    chkPTAnswer.Enabled = false;
                    chkPTAnswer.Checked = false;
                }
                else
                {
                    chkPTAnswer.Enabled = true;
                }



            }
        }

        protected void Edit_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;


                Label lblPatientMasterID = (Label)gvScanCard.Cells[0].FindControl("lblPatientMasterID");
               

                string strRptPath = "";
                strRptPath = "../WebReports/SHReports/FallPreventionAsses.aspx";
                string rptcall = @strRptPath + "?EMR_ID=" + lblPatientMasterID.Text + "&EMR_PT_ID=" + Convert.ToString(Session["EMR_PT_ID"]) + "&DR_ID=";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Presc", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=700,width=1075,scrollbars=1');", true);


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "       Edit_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }
        #endregion

        

    }
}