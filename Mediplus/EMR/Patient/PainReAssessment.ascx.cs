﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using EMR_BAL;
using System.IO;


namespace Mediplus.EMR.Patient
{
    public partial class PainReAssessment : System.Web.UI.UserControl
    {
        EMR_PTPainAssessment objPainAss = new EMR_PTPainAssessment();
        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        public void BindgvPainAssGrid()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND EPA_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND EPA_PT_ID ='" + Convert.ToString(Session["EMR_PT_ID"]) + "'";

            DataSet DS = new DataSet();
            objPainAss = new EMR_PTPainAssessment();
            DS = objPainAss.PTPainReAssessmentGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvPainAss.DataSource = DS;
                gvPainAss.DataBind();
            }
            else
            {
                gvPainAss.DataBind();
            }
        }

        public void BindgvPainAssData()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND EPA_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND EPA_PT_ID ='" + Convert.ToString(Session["EMR_PT_ID"]) + "'";
            Criteria += " AND ( EPA_ID ='" + Convert.ToString(Session["EMR_ID"]) + "' OR CONVERT(DATE,EPA_DATE,103) = CONVERT(DATE,'" + Convert.ToString(Session["HPV_DATE"]) + "',103) )";

            DataSet DS = new DataSet();
            objPainAss = new EMR_PTPainAssessment();
            DS = objPainAss.PTPainReAssessmentGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                txtDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["EPA_DATEDesc"]);

                DateTime dtInvoice = Convert.ToDateTime(DS.Tables[0].Rows[0]["EPA_DATE"]);
                txtTime.Text = dtInvoice.ToString("hh:mm:ss");


                txtPaintLocation.Text = Convert.ToString(DS.Tables[0].Rows[0]["EPA_PAIN_LOCATION"]);

                if (DS.Tables[0].Rows[0].IsNull("EPA_ONSET") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPA_ONSET"]) != "" && Convert.ToString(DS.Tables[0].Rows[0]["EPA_ONSET"]) != "0")
                    drpOnSet.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["EPA_ONSET"]);

                if (DS.Tables[0].Rows[0].IsNull("EPA_RHYTHM") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPA_RHYTHM"]) != "" && Convert.ToString(DS.Tables[0].Rows[0]["EPA_RHYTHM"]) != "0")
                    drpRhythm.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["EPA_RHYTHM"]);

                txtIntensity.Text = Convert.ToString(DS.Tables[0].Rows[0]["EPA_PAIN_INTENSITY"]);
                txtPainScale.Text = Convert.ToString(DS.Tables[0].Rows[0]["EPA_PAIN_SCALE_USED"]);
                txtAssociatedSymptoms.Text = Convert.ToString(DS.Tables[0].Rows[0]["EPA_ASSOCIATED_SYMPTOMS"]);
                txtPharmacologicalMana.Text = Convert.ToString(DS.Tables[0].Rows[0]["EPA_PHARMACOLOGICAL_MANG"]);

                txtNonPharmacologicalMana.Text = Convert.ToString(DS.Tables[0].Rows[0]["EPA_NONPHARMACOLOGICAL_MANG"]);
                txtComment.Text = Convert.ToString(DS.Tables[0].Rows[0]["EPA_COMMENTS"]);

                if (DS.Tables[0].Rows[0].IsNull("EPA_PAIN_CHARACTER") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPA_PAIN_CHARACTER"]) != "")
                    drpPainCharacter.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["EPA_PAIN_CHARACTER"]);


                if (DS.Tables[0].Rows[0].IsNull("EPA_NOT_APPLICABLE") == false && Convert.ToString(DS.Tables[0].Rows[0]["EPA_NOT_APPLICABLE"]) != "")
                {
                    chkNotApplicable.Checked = Convert.ToBoolean(DS.Tables[0].Rows[0]["EPA_NOT_APPLICABLE"]);

                    if (chkNotApplicable.Checked == true)
                    {
                        chkNotApplicable_CheckedChanged(chkNotApplicable, new EventArgs());
                    }
                }

            }

        }

        public string fnPTReAssessmentSave()
        {

            objPainAss = new EMR_PTPainAssessment();

            objPainAss.EPA_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
            objPainAss.EPA_ID = Convert.ToString(Session["EMR_ID"]);
            objPainAss.EPA_PT_ID = Convert.ToString(Session["EMR_PT_ID"]);
            objPainAss.EPA_DATE = txtDate.Text.Trim() + " " + txtTime.Text.Trim();
            objPainAss.EPA_PAIN_LOCATION = txtPaintLocation.Text.Trim();
            if (drpOnSet.SelectedIndex != 0)
            {
                objPainAss.EPA_ONSET = drpOnSet.SelectedValue;
            }
            else
            {
                objPainAss.EPA_ONSET = "";
            }
            if (drpRhythm.SelectedIndex != 0)
            {

                objPainAss.EPA_RHYTHM = drpRhythm.SelectedValue;
            }
            else
            {
                objPainAss.EPA_RHYTHM = "";
            }
            objPainAss.EPA_PAIN_INTENSITY = txtIntensity.Text;
            objPainAss.EPA_PAIN_SCALE_USED = txtPainScale.Text;
            objPainAss.EPA_ASSOCIATED_SYMPTOMS = txtAssociatedSymptoms.Text;
            objPainAss.EPA_PHARMACOLOGICAL_MANG = txtPharmacologicalMana.Text;
            objPainAss.EPA_NONPHARMACOLOGICAL_MANG = txtNonPharmacologicalMana.Text;
            objPainAss.EPA_PAIN_CHARACTER = drpPainCharacter.SelectedValue;
            objPainAss.EPA_COMMENTS = txtComment.Text;
            objPainAss.EPA_NOT_APPLICABLE = Convert.ToString(chkNotApplicable.Checked);
            objPainAss.UserID = Convert.ToString(Session["User_ID"]);
            objPainAss.PTPainReAssessmentAdd();
            BindgvPainAssGrid();

            return "";
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Convert.ToString(Session["User_ID"]) == "" || Convert.ToString(Session["User_ID"]) == null)
            {
                Session["ErrorMsg"] = "Session Expired Please login again";
                Response.Redirect("../../ErrorPage.aspx");
            }


            if (!IsPostBack)
            {
                if (Convert.ToString(Session["EPM_STATUS"]).ToUpper() == "Y" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                {
                    btnSave.Visible = false;
                }
                //HIDE THE SAVE OPTION FOR NEXT DAY OF THE VISIT
                if (Convert.ToString(Session["EMRCurrentDateDiff"]) != "0" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                {
                    btnSave.Visible = false;
                }
                CommonBAL objCom = new CommonBAL();
                string strDate = "", strTime = ""; ;
                strDate = objCom.fnGetDate("dd/MM/yyyy");
                strTime = objCom.fnGetDate("hh:mm:ss");

                txtDate.Text = strDate;
                txtTime.Text = strTime;
                //if (GlobalValues.FileDescription.ToUpper() == "HOLISTIC")
                //{
                    BindgvPainAssData();
                    BindgvPainAssGrid();
               // }

            }
        }



        protected void chkNotApplicable_CheckedChanged(object sender, EventArgs e)
        {

            if (chkNotApplicable.Checked == true)
            {
                CommonBAL objCom = new CommonBAL();
                string strDate = "", strTime = ""; ;
                strDate = objCom.fnGetDate("dd/MM/yyyy");
                strTime = objCom.fnGetDate("hh:mm:ss");

                txtDate.Text = strDate;
                txtTime.Text = strTime;

                txtPaintLocation.Text = "";
                drpOnSet.SelectedIndex = 0;
                drpRhythm.SelectedIndex = 0;
                txtIntensity.Text = "";
                txtPainScale.Text = "";
                txtAssociatedSymptoms.Text = "";
                txtPharmacologicalMana.Text = "";
                txtNonPharmacologicalMana.Text = "";
                drpPainCharacter.SelectedIndex = 0;
                txtComment.Text = "";

                txtDate.Enabled = false;
                txtTime.Enabled = false;
                txtPaintLocation.Enabled = false;
                drpOnSet.Enabled = false;
                drpRhythm.Enabled = false;
                txtIntensity.Enabled = false;
                txtPainScale.Enabled = false;
                txtAssociatedSymptoms.Enabled = false;
                txtPharmacologicalMana.Enabled = false;
                txtNonPharmacologicalMana.Enabled = false;
                drpPainCharacter.Enabled = false;
                txtComment.Enabled = false;
            }
            else
            {
                CommonBAL objCom = new CommonBAL();
                string strDate = "", strTime = ""; ;
                strDate = objCom.fnGetDate("dd/MM/yyyy");
                strTime = objCom.fnGetDate("hh:mm:ss");

                txtDate.Text = strDate;
                txtTime.Text = strTime;

                txtDate.Enabled = true;
                txtTime.Enabled = true;
                txtPaintLocation.Enabled = true;
                drpOnSet.Enabled = true;
                drpRhythm.Enabled = true;
                txtIntensity.Enabled = true;
                txtPainScale.Enabled = true;
                txtAssociatedSymptoms.Enabled = true;
                txtPharmacologicalMana.Enabled = true;
                txtNonPharmacologicalMana.Enabled = true;
                drpPainCharacter.Enabled = true;
                txtComment.Enabled = true;

            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                fnPTReAssessmentSave();
                BindgvPainAssData();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PainReAssessment.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

    }
}