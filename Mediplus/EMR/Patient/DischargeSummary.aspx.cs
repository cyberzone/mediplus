﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_BAL;

namespace Mediplus.EMR.Patient
{
    public partial class DischargeSummary : System.Web.UI.Page
    {
        CommonBAL objCom = new CommonBAL();

        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindTime()
        {
            int AppointmentInterval = 15;
            int AppointmentStart = 0;
            int AppointmentEnd = 23;
            int index = 0;

            for (int i = AppointmentStart; i <= AppointmentEnd; i++)
            {
                string strHour = Convert.ToString(index);
                if (index <= 9)
                {
                    strHour = "0" + Convert.ToString(index);
                }

                drpAdmissionHour.Items.Insert(index, Convert.ToString(strHour));
                drpAdmissionHour.Items[index].Value = Convert.ToString(strHour);
                drpDisHour.Items.Insert(index, Convert.ToString(strHour));
                drpDisHour.Items[index].Value = Convert.ToString(strHour);


                index = index + 1;

            }
            index = 1;
            int intCount = 0;
            Int32 intMin = AppointmentInterval;




            drpAdmissionMin.Items.Insert(0, Convert.ToString("00"));
            drpAdmissionMin.Items[0].Value = Convert.ToString("00");


            drpDisMin.Items.Insert(0, Convert.ToString("00"));
            drpDisMin.Items[0].Value = Convert.ToString("00");


            for (int j = AppointmentInterval; j < 60; j++)
            {

                drpAdmissionMin.Items.Insert(index, Convert.ToString(j - intCount));
                drpAdmissionMin.Items[index].Value = Convert.ToString(j - intCount);

                drpDisMin.Items.Insert(index, Convert.ToString(j - intCount));
                drpDisMin.Items[index].Value = Convert.ToString(j - intCount);


                j = j + AppointmentInterval;
                index = index + 1;
                intCount = intCount + 1;

            }

        }

        void GetStaff()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND HSFM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            DS = objCom.GetStaffMaster(Criteria);



            if (DS.Tables[0].Rows.Count > 0)
            {
                AttendingPhysician.DataSource = DS;
                AttendingPhysician.DataValueField = "HSFM_STAFF_ID";
                AttendingPhysician.DataTextField = "FullName";
                AttendingPhysician.DataBind();


                Dictatedby.DataSource = DS;
                Dictatedby.DataValueField = "HSFM_STAFF_ID";
                Dictatedby.DataTextField = "FullName";
                Dictatedby.DataBind();

                PrimaryCarePhysician.DataSource = DS;
                PrimaryCarePhysician.DataValueField = "HSFM_STAFF_ID";
                PrimaryCarePhysician.DataTextField = "FullName";
                PrimaryCarePhysician.DataBind();

                ReferringPhysician.DataSource = DS;
                ReferringPhysician.DataValueField = "HSFM_STAFF_ID";
                ReferringPhysician.DataTextField = "FullName";
                ReferringPhysician.DataBind();

                ConsultingPhysician.DataSource = DS;
                ConsultingPhysician.DataValueField = "HSFM_STAFF_ID";
                ConsultingPhysician.DataTextField = "FullName";
                ConsultingPhysician.DataBind();


            }

            AttendingPhysician.Items.Insert(0, "--- Select ---");
            AttendingPhysician.Items[0].Value = "";


            Dictatedby.Items.Insert(0, "--- Select ---");
            Dictatedby.Items[0].Value = "";

            PrimaryCarePhysician.Items.Insert(0, "--- Select ---");
            PrimaryCarePhysician.Items[0].Value = "";


            ReferringPhysician.Items.Insert(0, "--- Select ---");
            ReferringPhysician.Items[0].Value = "";

            ConsultingPhysician.Items.Insert(0, "--- Select ---");
            ConsultingPhysician.Items[0].Value = "";


        }

        void BindPatientDetails()
        {
            DataSet DS = new DataSet();
            objCom = new CommonBAL();
            string Criteria = " 1=1 ";
            Criteria += " AND HPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND HPM_PT_ID='" + Convert.ToString(Session["EMR_PT_ID"]) + "'";
            DS = objCom.PatientMasterGet(Criteria);
            {

                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    ViewState["FileNo"] = Convert.ToString(DR["HPM_PT_ID"]);
                    ViewState["FullName"] = Convert.ToString(DR["FullName"]);

                }
            }

        }



        void BindDischargeSummary()
        {

            DataSet DS = new DataSet();
            EMR_PTDischargeSummary objDis = new EMR_PTDischargeSummary();
            string Criteria = " 1=1 ";
            Criteria += " and EPDS_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

            DS = objDis.DischargesummaryGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {


                txtAdmissionDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["AdmissionDateDesc"]);

                string strArrivalDate = Convert.ToString(DS.Tables[0].Rows[0]["AdmissionDateTimeDesc"]);
                string[] arrArrivalDater = strArrivalDate.Split(':');
                if (arrArrivalDater.Length > 1)
                {
                    drpAdmissionHour.SelectedValue = arrArrivalDater[0];
                    drpAdmissionMin.SelectedValue = arrArrivalDater[1];

                }


                txtDischargeDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["DischargeDateDesc"]);

                string strDisDate = Convert.ToString(DS.Tables[0].Rows[0]["DischargeDateTimeDesc"]);
                string[] arrDisater = strDisDate.Split(':');
                if (arrDisater.Length > 1)
                {
                    drpDisHour.SelectedValue = arrDisater[0];
                    drpDisMin.SelectedValue = arrDisater[1];

                }


                AttendingPhysician.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPDS_ATTENDING_PHYSICIAN"]);
                Dictatedby.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPDS_DICTATEDBY"]);
                PrimaryCarePhysician.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPDS_PRIMARYCARE_PHYSICIAN"]);
                ReferringPhysician.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPDS_REFERRING_PHYSICIAN"]);
                ConsultingPhysician.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPDS_CONSULTING_PHYSICIAN"]);
                ConditionOnDischarge.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPDS_DISCHARGE_CONDITION"]);
                FinalDiagnosis.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPDS_FINAL_DIAGNOSIS"]);
                HistoryofPresentIllness.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPDS_HISTORY_PRESENT_ILLNESS"]);
                Laboratory.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPDS_LABORATORY"]);
                HospitalCourse.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPDS_HOSPITAL_COURSE"]);
                DischargeMedications.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPDS_DISCHARGE_MEDICATIONS"]);
                DischargeInstruction.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPDS_DISCHARGE_INSTRUCTIONS"]);
                FollowUpAppointments.Value = Convert.ToString(DS.Tables[0].Rows[0]["EPDS_FOLLOWUP_APPOINTMENTS"]);
            }
        }

        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Convert.ToString(Session["User_ID"]) == "" || Convert.ToString(Session["User_ID"]) == null)
            {
                Session["ErrorMsg"] = "Session Expired Please login again";
                Response.Redirect("../../ErrorPage.aspx");
            }

            if (!IsPostBack)
            {
                BindTime();
                GetStaff();
                BindPatientDetails();
                string strDate = "", strTime = ""; ;
                strDate = objCom.fnGetDate("dd/MM/yyyy");
                strTime = objCom.fnGetDate("hh:mm:ss");

                txtAdmissionDate.Text = strDate;
                txtDischargeDate.Text = strDate;

                BindDischargeSummary();

            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                EMR_PTDischargeSummary objDis = new EMR_PTDischargeSummary();

                objDis.BranchID = Convert.ToString(Session["Branch_ID"]);
                objDis.EMRID = Convert.ToString(Session["EMR_ID"]);
                objDis.PTID = Convert.ToString(Session["EMR_PT_ID"]);

                objDis.PT_NAME = Convert.ToString(ViewState["FullName"]);
                objDis.EPDS_ADMISSION_DATE = txtAdmissionDate.Text.Trim() + " " + drpAdmissionHour.SelectedValue + ":" + drpAdmissionMin.SelectedValue + ":00";
                objDis.EPDS_DISCHARGE_DATE = txtDischargeDate.Text.Trim() + " " + drpDisHour.SelectedValue + ":" + drpDisMin.SelectedValue + ":00";
                objDis.EPDS_ATTENDING_PHYSICIAN = AttendingPhysician.Value;
                objDis.EPDS_DICTATEDBY = Dictatedby.Value;
                objDis.EPDS_PRIMARYCARE_PHYSICIAN = PrimaryCarePhysician.Value;
                objDis.EPDS_REFERRING_PHYSICIAN = ReferringPhysician.Value;
                objDis.EPDS_CONSULTING_PHYSICIAN = ConsultingPhysician.Value;
                objDis.EPDS_DISCHARGE_CONDITION = ConditionOnDischarge.Value;
                objDis.EPDS_FINAL_DIAGNOSIS = FinalDiagnosis.Value;
                objDis.EPDS_HISTORY_PRESENT_ILLNESS = HistoryofPresentIllness.Value;
                objDis.EPDS_LABORATORY = Laboratory.Value;
                objDis.EPDS_HOSPITAL_COURSE = HospitalCourse.Value;
                objDis.EPDS_DISCHARGE_MEDICATIONS = DischargeMedications.Value;
                objDis.EPDS_DISCHARGE_INSTRUCTIONS = DischargeInstruction.Value;
                objDis.EPDS_FOLLOWUP_APPOINTMENTS = FollowUpAppointments.Value;

                objDis.UserID = Convert.ToString(Session["User_ID"]);
                objDis.DischargesummaryAdd();



                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowMessage()", true);
            FunEnd: ;

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      DischargeSummary.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }
    }
}