﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EMR/PatientHeader.Master" AutoEventWireup="true" CodeBehind="TimeOutProcedureCheckList.aspx.cs" Inherits="Mediplus.EMR.Patient.TimeOutProcedureCheckList" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />
    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>
    <link href="../Content/themes/base/jquery-ui.css" />
    <style type="text/css">
        .box-header
        {
            border-bottom: 4px solid #f4fbfd;
            height: 70px;
        }

        .box-title
        {
            padding-bottom: 5px;
            border-bottom: 4px solid #92c500;
            float: left;
            font-size: 1.1em;
            color: #2078c0;
        }

        h1, h2, h3, h4, h5, h6
        {
            font-size: 100%;
            font-weight: normal;
            font-family: "Segoe UI", Arial, Helvetica, sans-serif;
        }
    </style>

    <script type="text/javascript">
        function ShowMessage() {
            $("#myMessage").show();
            setTimeout(function () {
                var selectedEffect = 'blind';
                var options = {};
                $("#myMessage").hide();
            }, 2000);
            return true;
        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 
            <h3 class="box-title">Time out Procedure Checklist </h3>
       
    <div style="padding-left: 60%; width: 100%;">
        <div id="myMessage" style="display: none; border: groove; height: 30px; width: 200px; background-color: gray; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #fff; padding: 20px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute;">
            Saved Successfully
        </div>
    </div>

    <table style="width: 100%;">
        <tr>
            <td align="right">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>

                        <asp:Button ID="btnSave1" runat="server" CssClass="button orange" Width="100px" OnClick="btnSave1_Click" Text="Save Record" />
                    </ContentTemplate>
                </asp:UpdatePanel>

            </td>
        </tr>
    </table>
    <table style="width:100%;">
        <tr>
            <td class="lblCaption1">
               Name of Procedure
            </td>
            <td>
                <asp:TextBox ID="NameOfProcedure" runat="server" CssClass="lblCaption1" Width ="200px"  ></asp:TextBox>
            </td>
        </tr>
    </table>
    <div style="padding-top: 0px; width: 100%; height: 500px; overflow: auto; border: thin; border-color: #CCCCCC; border-style: groove;">

        <asp:GridView ID="gvOPDAssessment" runat="server" AllowSorting="True" AutoGenerateColumns="False"
            EnableModelValidation="True" Width="99%" GridLines="None" OnRowDataBound="gvOPDAssessment_RowDataBound">
            <HeaderStyle CssClass="GridHeader_Blue" />
            <RowStyle CssClass="GridRow" />
            <AlternatingRowStyle CssClass="GridAlterRow" />
            <Columns>
                <asp:TemplateField HeaderText="Description" HeaderStyle-Width="50%">
                    <ItemTemplate>
                        <asp:Label ID="lblLevelType" CssClass="lblCaption1" runat="server" Text='<%# Bind("TOP_LEVEL") %>' Visible="false"></asp:Label>
                        <asp:Label ID="lblFieldID" CssClass="lblCaption1" runat="server" Text='<%# Bind("TOP_FIELD_ID") %>' Visible="false"></asp:Label>

                        <asp:Label ID="lblFieldName" CssClass="lblCaption1" runat="server" Text='<%# Bind("TOP_FIELD_NAME") %>' Width="100%"></asp:Label>

                    </ItemTemplate>

                </asp:TemplateField>
                <asp:TemplateField HeaderText="Patient's Answer" HeaderStyle-Width="20%">
                    <ItemTemplate>
                        <asp:RadioButtonList ID="radPTAnswer" runat="server" CssClass="lblCaption1" RepeatDirection="Horizontal" Width="180px">
                            <asp:ListItem Text="Yes" Value="Y"> </asp:ListItem>
                            <asp:ListItem Text="No" Value="N"> </asp:ListItem>
                            <asp:ListItem Text="N/A" Value="D"> </asp:ListItem>
                        </asp:RadioButtonList>
                    </ItemTemplate>

                </asp:TemplateField>
                <asp:TemplateField HeaderText="Details" HeaderStyle-Width="30%" >
                    <ItemTemplate>
                        <asp:TextBox ID="txtDtls" runat="server" CssClass="lblCaption1" TextMode="MultiLine" Height="50px" Width="95%" style="resize:none;" ></asp:TextBox>
                    </ItemTemplate>

                </asp:TemplateField>
            </Columns>

        </asp:GridView>
    </div>

   


    <style type="text/css">
        .style1
        {
            width: 20px;
            height: 32px;
        }

        .modalBackground
        {
            background-color: black;
            filter: alpha(opacity=40);
            opacity: 0.5;
        }

        .modalPopup
        {
            background-color: #ffffff;
            border-width: 3px;
            border-style: solid;
            border-color: Gray;
            padding: 3px;
            width: 250px;
            top: -1000px;
            position: absolute;
            padding: 0px10px10px10px;
        }
    </style>

</asp:Content>