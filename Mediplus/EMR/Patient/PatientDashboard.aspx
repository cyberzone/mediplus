﻿<%@ Page Title=""  Language="C#"   MasterPageFile="~/Site2.Master"  AutoEventWireup="true" CodeBehind="PatientDashboard.aspx.cs" Inherits=" Mediplus.EMR.Patient.PatientDashboard" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="../../Styles/style.css" rel="Stylesheet" type="text/css" />
    <link href="../../Styles/style.css" rel="Style" type="text/css" />
    <script src="../../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    
    
     <script src="../../Scripts/AccordionScript.js"></script>
     <link rel="stylesheet" href="../../Styles/Accordionstyles.css">

    <link href="../Styles/style.css" rel="stylesheet" type="text/css" />

    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
     <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />



    <link href="../Content/themes/base/jquery-ui.css" />

    <style type="text/css">
        .box-header
        {
            border-bottom: 4px solid #f4fbfd;
            height: 70px;
        }

        .box-title
        {
            padding-bottom: 5px;
            border-bottom: 4px solid #92c500;
            float: left;
            font-size: 1.2em;
            color: #2078c0;
        }

        h1, h2, h3, h4, h5, h6
        {
            font-size: 100%;
            font-weight: normal;
            font-family: "Segoe UI", Arial, Helvetica, sans-serif;
        }
    </style>

    <style type="text/css">
        .AutoExtender
        {
            font-family: "Segoe UI",arial, "lucida grande", "lucida sans unicode", verdana, tahoma, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: #005c7b;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #02a0e0;
            cursor: pointer;
        }

        #divwidth
        {
            width: 400px !important;
        }

            #divwidth div
            {
                width: 400px !important;
            }
    </style>

     <script language="javascript" type="text/javascript">

         function DivShow(pnlID, AncID) {
             if (document.getElementById("ContentPlaceHolder1_" + AncID).innerHTML == '+') {
                 document.getElementById("ContentPlaceHolder1_" + AncID).innerHTML = '-';
                 document.getElementById("ContentPlaceHolder1_" + pnlID).style.display = 'block';
             }
             else {
                 document.getElementById("ContentPlaceHolder1_" + AncID).innerHTML = '+';
                 document.getElementById("ContentPlaceHolder1_" + pnlID).style.display = 'none';
             }


         }

         function DivShowCC() {
             document.getElementById("ContentPlaceHolder1_pnlChief Complaints").style.display = 'block';

         }

         function TemplatePopupShow(strValue, TemplateType) {
             // alert(strValue);
             var CtrlName = "ContentPlaceHolder1_" + strValue;
             var Data = document.getElementById("ContentPlaceHolder1_" + strValue).value;
             //  

             var win = window.open("TemplatePopup.aspx?CtrlName=" + CtrlName + "&TemplateType=" + TemplateType + "&TemplateData=" + Data, "newwin1", "top=200,left=270,height=300,width=500,toolbar=no,scrollbars=yes,menubar=no;");
             win.focus();
         }


         function MyScript(strValue, TemplateType) {
             // alert(strValue);
             var CtrlName = "ContentPlaceHolder1_" + strValue;
             // alert(document.getElementById("ContentPlaceHolder1_" + strValue).value);

             var win = window.open("PatientDashboardPopup.aspx?CtrlName=" + CtrlName + "&TemplateType=" + TemplateType, "newwin1", "top=200,left=270,height=570,width=350,toolbar=no,scrollbars=yes,menubar=no;");
             win.focus();
         }

         function MyScript2(strValue, TemplateType) {
             var CtrlName = "ContentPlaceHolder1_" + strValue;
             var win = window.open("PatientDashboardPopup.aspx?CtrlName=" + CtrlName + "&TemplateType=" + TemplateType, "newwin1", "top=200,left=270,height=570,width=350,toolbar=no,scrollbars=yes,menubar=no;");
             win.focus();
         }

         function BindSelectedValue(ControlID, strValue) {
             //alert(strValue);
             document.getElementById("ContentPlaceHolder1_" + ControlID).value = strValue;

         }

         function BindTemplate(TempData, CtrlName) {
             document.getElementById(CtrlName).value = TempData
         }


         function BindRadioValue(strValue, RadValue) {
             var CtrlName = "ContentPlaceHolder1_" + strValue;
             document.getElementById("ContentPlaceHolder1_" + strValue).value = RadValue;
         }
         function HideTextBox(strValue, ChkValue) {

             if (document.getElementById("ContentPlaceHolder1_" + ChkValue).checked == true) {
                 document.getElementById("ContentPlaceHolder1_" + strValue).disabled = true;
                 document.getElementById("ContentPlaceHolder1_" + strValue).value = 'NA';

             }
             else {
                 document.getElementById("ContentPlaceHolder1_" + strValue).disabled = false;
                 document.getElementById("ContentPlaceHolder1_" + strValue).value = '';
             }
         }

         function HideAllTextBox(TextBoxIDs, chkNotAppID) {
             // alert(chkNotAppID);
             var arrTextBoxIDs = TextBoxIDs.split("|");
             for (i = 0; i < arrTextBoxIDs.length; i++) {

                 var vtxtID = "";
                 vtxtID = arrTextBoxIDs[i]

                 if (document.getElementById("ContentPlaceHolder1_" + chkNotAppID).checked == true) {
                     document.getElementById("ContentPlaceHolder1_" + vtxtID).disabled = true;
                     //alert("ContentPlaceHolder1_" + vtxtID);
                 }
                 else {
                     document.getElementById("ContentPlaceHolder1_" + vtxtID).disabled = false;

                     // alert("ContentPlaceHolder1_" + vtxtID);
                 }
             }
         }



         function SubHeaderShow(SeltedPanelId) {
             var i = 0;

             var strhidSelSegSubType = document.getElementById('<%=hidSelSegSubType.ClientID %>').value;


            if (document.getElementById("ContentPlaceHolder1_" + SeltedPanelId).style.display == 'none') {
                document.getElementById("ContentPlaceHolder1_" + SeltedPanelId).style.display = 'block';
            }
            else {
                document.getElementById("ContentPlaceHolder1_" + SeltedPanelId).style.display = 'none';
            }
            if (strhidSelSegSubType != "" && strhidSelSegSubType != SeltedPanelId) {
                document.getElementById("ContentPlaceHolder1_" + strhidSelSegSubType).style.display = 'none';
            }

            document.getElementById('<%=hidSelSegSubType.ClientID %>').value = SeltedPanelId;

        }


        function BindCC(e, CC_ID) {
            if (e.keyCode == 13) {

                var ChiefComp = document.getElementById("ContentPlaceHolder1_txtCriticalNotes").value + document.getElementById("ContentPlaceHolder1_" + CC_ID).value + "\n";
                document.getElementById("ContentPlaceHolder1_txtCriticalNotes").value = ChiefComp;
                document.getElementById("ContentPlaceHolder1_" + CC_ID).value = '';


                return false;
            }
        }

    </script>


    <script type="text/javascript">
        function ShowMessage() {
            $("#myMessage").show();
            setTimeout(function () {
                var selectedEffect = 'blind';
                var options = {};
                $("#myMessage").hide();
            }, 2000);
            return true;
        }

        function ShowAuditLog() {

            var win = window.open('../AuditLogDisplay.aspx?ScreenID=OP_DASHBOARD&DataDisplay=ByEMR_ID', 'AuditLogDisplay', 'menubar=no,left=100,top=80,height=550,width=1075,scrollbars=1')

        }

    </script>
     <style type="text/css">
    .modal
    {
        position: fixed;
        top: 0;
        left: 0;
        background-color: black;
        z-index: 99;
        opacity: 0.8;
        filter: alpha(opacity=80);
        -moz-opacity: 0.8;
        min-height: 100%;
        width: 100%;
    }
    .loading
    {
        font-family: Arial;
        font-size: 10pt;
        border: 5px solid #67CFF5;
        width: 200px;
        height: 100px;
        display: none;
        position: fixed;
        background-color: White;
        z-index: 999;
    }
</style>
 

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <input type="hidden" id="hidSegTypes" runat="server" value="ROS|PE|GYNAC_ANTENATAL_ASSESSMENT|GYNAC_PRENATAL_ASSESSMENT|GYNAC_POSTPARTUM_ASSESSMENT|THERAPY_TREATMENT_PLANS|EXTRA_ORAL_EXAMINATION|INTRA_ORAL_EXAMINATION|PREVIOUS_DELIVERIES" />
    <input type="hidden" id="hidSegTypes_SavePT_ID" runat="server" value="HIST_PAST|HIST_PERSONAL|HIST_SOCIAL|HIST_FAMILY|HIST_SURGICAL|ALLERGY|PREVIOUS_DELIVERIES" />

    <input type="hidden" id="hidSelSegSubType" runat="server" value="" />
    <input type="hidden" id="hidNormalAbnormalText" runat="server" value="false" />
    <input type="hidden" id="hidCheckboxRequired" runat="server" value="|LOCAL_EXAM|PE|NEONATAL_HISTORY|MENTAL_ASSESSMENT|THERAPY_PROGRESS_NOTES|PRECAUTIONS|THERAPY_TREATMENT_PLANS|THERAPY_DETAILED_ASSESSMENT|THERAPY_PROBLEMS|DENTAL_HISTORY|PSYCHOLOGICAL_STATUS|GYNAC_ANTENATAL_ASSESSMENT|PREVIOUS_DELIVERIES|" />

    <input type="hidden" id="hidYesNoboxRequired" runat="server" value="|HIST_PAST|HIST_FAMILY|HIST_SURGICAL|HIST_SOCIAL|ALLERGY|HIST_VACC|" />

     <table cellpadding="0" cellspacing="0" width="80%">
        <tr>
            <td style="width:90%;text-align:right;">
                    <div id="divAuditDtls" runat="server" visible="false" style="text-align: right; width: 100%;" class="lblCaption1">
                        Creaded By :
                                                  <asp:Label ID="lblCreatedBy" runat="server" CssClass="label"></asp:Label>
                        &nbsp;-&nbsp;
                                            <asp:Label ID="lblCreatedDate" runat="server" CssClass="label"></asp:Label>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        Modified By
                                                 <asp:Label ID="lblModifiedBy" runat="server" CssClass="label"></asp:Label>
                        &nbsp;-&nbsp;
                                             <asp:Label ID="lblModifiedDate" runat="server" CssClass="label"></asp:Label>
                           &nbsp;&nbsp;
                    </div>
            </td>
            <td style="width:10%;text-align:right;">
 
              <a href="javascript:ShowAuditLog();" style="text-decoration: none;" class="lblCaption">Audit Log </a>
   

            </td>
        </tr>
    </table>
    
 

    <div style="padding-left: 60%; width: 100%;">
        <div id="myMessage" style="display: none; border: groove; height: 30px; width: 200px; background-color: gray; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #fff; padding: 20px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute;">
            Saved Successfully
        </div>
    </div>

    <table width="80%">
        <tr>
            <td style="text-align: left; width: 30%;">
                
                 <h3 class="box-title">
        <asp:Label ID="lblHeader" runat="server" Text="Patient Dashboard"></asp:Label>
    </h3>
            </td>
            <td style="text-align: left; width: 50%;" class="lblCaption" >
                <div id="divTemplate" runat="server" visible="false">
                 Template
                   <asp:DropDownList ID="drpTemplate" CssClass="TextBoxStyle" runat="server" Width="155px" BorderColor="#cccccc" AutoPostBack="true" OnSelectedIndexChanged="drpTemplate_SelectedIndexChanged">
                            </asp:DropDownList>
                
                            <asp:LinkButton ID="lnkSaveTemp" runat="server" CssClass="label gray" Text="Save" Enabled="false" Width="50px" 
                                Style="width:50px;border-radius:5px 5px 5px 5px; vertical-align: central;text-align:center;"></asp:LinkButton>
                            <asp:Button ID="btnDeleteTemp" runat="server" Text="Delete" OnClick="btnDeleteTemp_Click" style="width:50px;border-radius:5px 5px 5px 5px"  CssClass="label gray" />
                </div>
            </td>
            <td style="text-align: right; width: 20%;">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="btnSave" runat="server" CssClass="button red small" Text="Save Record" OnClick="btnSave_Click" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>

    <div id="divwidth" style="visibility: hidden;"></div>
    <table cellpadding="5" cellspacing="5" style="width: 80%;">

        <tr>
            <td style="width: 50%; vertical-align: top;">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
                        </td>
                    </tr>
                </table>
            </td>

            <td style="width: 50%; vertical-align: top;">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <asp:PlaceHolder ID="PlaceHolder2" runat="server"></asp:PlaceHolder>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

          <asp:ModalPopupExtender ID="ModalPopupExtender5" runat="server" TargetControlID="lnkSaveTemp"
    PopupControlID="pnlSaveTemplate" BackgroundCssClass="modalBackground" CancelControlID="btnRemClose"
    PopupDragHandleControlID="pnlSaveTemplate">
</asp:ModalPopupExtender>
<asp:Panel ID="pnlSaveTemplate" runat="server" Height="250px" Width="550px" CssClass="modalPopup" Visible="false">
    <table cellpadding="0" cellspacing="0" width="100%" style="">
        <tr>
            <td align="right">
                <asp:Button ID="btnRemClose" runat="server" Text=" X " Font-Bold="true" CssClass="button blue small" />
            </td>
        </tr>
        <tr>
            <td class="lblCaption" style="font-weight: bold;">Save Template Dialog
            </td>
        </tr>

        <tr>
            <td class="lblCaption1" style="height: 100px; vertical-align: top;">
                <fieldset style="height: 50px;">
                    <legend class="lblCaption1" style="font-weight: bold;">Save Template</legend>
                    <table>
                        <tr>
                            <td>Template Name : 
                            </td>
                            <td>
                                <input type="text" id="txtTemplateName" name="txtTemplateName" runat="server" class="label" />
                            </td>
                        </tr>
                        <tr>
                            <td>Other Doctors :
                            </td>
                            <td>
                                <input type="checkbox" id="chkOtherDr" runat="server" title="Apply" />
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
        <tr>
            <td style="text-align: right;">
                <asp:Button ID="btnSaveTemp" runat="server" Text="Save Template" OnClick="btnSaveTemp_Click" CssClass="button blue small" />
                <asp:Button ID="btlCloseTemp" runat="server" Text="Close" CssClass="button blue small" />

            </td>
        </tr>

    </table>
</asp:Panel>

    <br />
    <br />


</asp:Content>
