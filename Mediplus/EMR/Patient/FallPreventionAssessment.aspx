﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EMR/PatientHeader.Master" AutoEventWireup="true" CodeBehind="FallPreventionAssessment.aspx.cs" Inherits=" Mediplus.EMR.Patient.FallPreventionAssessment" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />
    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>
    <link href="../Content/themes/base/jquery-ui.css" />
    <style type="text/css">
        .box-header
        {
            border-bottom: 4px solid #f4fbfd;
            height: 70px;
        }

        .box-title
        {
            padding-bottom: 5px;
            border-bottom: 4px solid #92c500;
            float: left;
            font-size: 1.1em;
            color: #2078c0;
        }

        h1, h2, h3, h4, h5, h6
        {
            font-size: 100%;
            font-weight: normal;
            font-family: "Segoe UI", Arial, Helvetica, sans-serif;
        }
    </style>

    <style type="text/css">
        .BoldStyle
        {
            font-weight: bold;
        }

        .BorderStyle
        {
            border: 1px solid #dcdcdc;
            height: 20px;
        }
    </style>

    <script type="text/javascript">
        function ShowMessage() {
            $("#myMessage").show();
            setTimeout(function () {
                var selectedEffect = 'blind';
                var options = {};
                $("#myMessage").hide();
            }, 2000);
            return true;
        }

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }

        function ShowAuditLog() {

            var win = window.open('../AuditLogDisplay.aspx?ScreenID=OP_FALL_PREVEN_ASS&DataDisplay=ByEMR_ID', 'AuditLogDisplay', 'menubar=no,left=100,top=80,height=550,width=1075,scrollbars=1')

        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td style="width:90%;text-align:right;">
                    <div id="divAuditDtls" runat="server" visible="false" style="text-align: right; width: 100%;" class="lblCaption1">
                        Creaded By :
                                                  <asp:Label ID="lblCreatedBy" runat="server" CssClass="label"></asp:Label>
                        &nbsp;-&nbsp;
                                            <asp:Label ID="lblCreatedDate" runat="server" CssClass="label"></asp:Label>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        Modified By
                                                 <asp:Label ID="lblModifiedBy" runat="server" CssClass="label"></asp:Label>
                        &nbsp;-&nbsp;
                                             <asp:Label ID="lblModifiedDate" runat="server" CssClass="label"></asp:Label>
                           &nbsp;&nbsp;
                    </div>
            </td>
            <td style="width:10%;text-align:right;">
 
              <a href="javascript:ShowAuditLog();" style="text-decoration: none;" class="lblCaption">Audit Log </a>
   

            </td>
        </tr>
    </table>
    <div style="padding-left: 60%; width: 100%;">
        <div id="myMessage" style="display: none; border: groove; height: 30px; width: 200px; background-color: gray; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #fff; padding: 20px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute;">
            Saved Successfully
        </div>
    </div>

    <table width="100%">
        <tr>
            <td style="text-align: left; width: 50%;">
                <h3 class="box-title">Fall Prevention Assessment</h3>
            </td>
            <td style="text-align: right; width: 50%;">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="btnSave" runat="server" CssClass="button orange small" Text="Save Record" OnClick="btnSave_Click" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="AjaxTabStyle" Width="100%">
        <asp:TabPanel runat="server" ID="TabPanelGeneral" HeaderText="Fall Prevention Assessment" Width="100%">
            <ContentTemplate>
                <table width="100%">
                    <tr>
                        <td style="text-align: left; width: 50%;">
                            <asp:CheckBox ID="chkNotApplicable" runat="server" CssClass="lblCaption1" Text="Not Applicable" AutoPostBack="true" OnCheckedChanged="chkNotApplicable_CheckedChanged" />
                        </td>
                    </tr>
                </table>

                <div style="padding-top: 0px; width: 100%; height: auto; overflow: auto; border: thin; border-color: #CCCCCC; border-style: groove;">

                    <asp:GridView ID="gvOPDAssessment" runat="server" AllowSorting="True" AutoGenerateColumns="False" ShowFooter="true"
                        EnableModelValidation="True" Width="99%" GridLines="None" OnRowDataBound="gvOPDAssessment_RowDataBound">
                        <HeaderStyle CssClass="GridHeader_Blue" />
                        <FooterStyle CssClass="GridHeader_Blue" />
                        <RowStyle CssClass="GridRow" />
                        <AlternatingRowStyle CssClass="GridAlterRow" />
                        <Columns>
                            <asp:TemplateField HeaderText="Description" HeaderStyle-Width="50%">
                                <ItemTemplate>
                                    <asp:Label ID="lblLevelType" CssClass="lblCaption1" runat="server" Text='<%# Bind("FPA_LEVEL") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblFieldID" CssClass="lblCaption1" runat="server" Text='<%# Bind("FPA_FIELD_ID") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblValue" CssClass="lblCaption1" runat="server" Text='<%# Bind("FPA_VALUE") %>' Width="100%" Visible="false"></asp:Label>
                                    <asp:Label ID="lblDescription" CssClass="lblCaption1" runat="server" Text='<%# Bind("FPA_FIELD_NAME") %>' Width="100%"></asp:Label>



                                </ItemTemplate>

                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Scale" HeaderStyle-Width="25%">
                                <ItemTemplate>
                                    <asp:UpdatePanel runat="server" ID="UpId"
                                        UpdateMode="Conditional" ChildrenAsTriggers="true">
                                        <ContentTemplate>
                                            <asp:RadioButtonList ID="radPTAnswer" runat="server" CssClass="lblCaption1" RepeatDirection="Horizontal" Width="200px" AutoPostBack="true" OnSelectedIndexChanged="radPTAnswer_SelectedIndexChanged">
                                                <asp:ListItem Text="No" Value="N"> </asp:ListItem>
                                                <asp:ListItem Text="Yes" Value="Y"> </asp:ListItem>
                                            </asp:RadioButtonList>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="radPTAnswer" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <span class="lblCaption1" style="font-weight: bold; color: white;">Total Score </span>
                                </FooterTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Scoring" HeaderStyle-Width="25%">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtDtls" runat="server" CssClass="lblCaption1" Width="50px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtTotalScore" runat="server" CssClass="lblCaption1" Width="50px" ReadOnly="true"></asp:TextBox>
                                </FooterTemplate>
                            </asp:TemplateField>
                        </Columns>

                    </asp:GridView>
                </div>
                <br />
                <span class="lblCaption1">Fall prevention interventions: check as appropriate for the patient :</span>
                <div style="padding-top: 0px; width: 100%; height: auto; overflow: auto; border: thin; border-color: #CCCCCC; border-style: groove;">

                    <asp:GridView ID="gvPart2" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        EnableModelValidation="True" Width="99%" GridLines="None" OnRowDataBound="gvPart2_RowDataBound">
                        <HeaderStyle CssClass="GridHeader_Blue" />
                        <RowStyle CssClass="GridRow" />
                        <AlternatingRowStyle CssClass="GridAlterRow" />
                        <Columns>
                            <asp:TemplateField HeaderText="" HeaderStyle-Width="5%">
                                <ItemTemplate>

                                    <asp:CheckBox ID="chkPTAnswer" runat="server" />
                                </ItemTemplate>

                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Description" HeaderStyle-Width="85%">
                                <ItemTemplate>
                                    <asp:Label ID="lblLevelType1" CssClass="lblCaption1" runat="server" Text='<%# Bind("FPA_LEVEL") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblFieldID1" CssClass="lblCaption1" runat="server" Text='<%# Bind("FPA_FIELD_ID") %>' Visible="false"></asp:Label>

                                    <asp:Label ID="Label2" CssClass="lblCaption1" runat="server" Text='<%# Bind("FPA_FIELD_NAME") %>' Width="100%"></asp:Label>

                                </ItemTemplate>

                            </asp:TemplateField>


                        </Columns>

                    </asp:GridView>
                </div>
            </ContentTemplate>
        </asp:TabPanel>
        <asp:TabPanel runat="server" ID="TabPanel1" HeaderText="History" Width="100%">
            <ContentTemplate>
                <div style="padding-top: 0px; width: 100%; height: 400px; overflow: auto; border: thin; border-color: #f6f6f6; border-style: groove;">
                      <asp:GridView ID="gvAssessment" runat="server" AutoGenerateColumns="False" GridLines="None"
                        EnableModelValidation="True" Width="100%">
                        <HeaderStyle CssClass="GridHeader_Blue" Height="20px" Font-Bold="true" />
                        <RowStyle CssClass="GridRow" Height="20px" />
                        <Columns>

                            <asp:TemplateField HeaderText="Emr ID">
                                <ItemTemplate>
                                      <asp:LinkButton ID="lnkFileNumbe" runat="server" OnClick="Edit_Click">
                                    <asp:Label ID="lblFileNumber" CssClass="lblFileNumber" runat="server" Text='<%# Bind("FPA_PT_ID") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblPatientMasterID" CssClass="lblCaption1" runat="server" Text='<%# Bind("FPA_ID") %>'></asp:Label>
                                     </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Date">
                                <ItemTemplate>
                                     <asp:LinkButton ID="lnkDate" runat="server" OnClick="Edit_Click">
                                    <asp:Label ID="lblgvEMR_PTMaster_Date" CssClass="lblCaption1" runat="server" Text='<%# Bind("ModifiedDate") %>'></asp:Label>
                                         </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="User">
                                <ItemTemplate>
                                     <asp:LinkButton ID="lnkUser" runat="server" OnClick="Edit_Click">
                                    <asp:Label ID="lblgvCreatedUser" CssClass="lblCaption1" runat="server" Text='<%# Bind("ModifiedUserName") %>'></asp:Label>
                                         </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Total Score">
                                <ItemTemplate>
                                     <asp:LinkButton ID="lnklblgvScore" runat="server" OnClick="Edit_Click">
                                    <asp:Label ID="lblgvScore" CssClass="lblCaption1" runat="server" Text='<%# Bind("FPA_TOTAL_SCORE") %>'></asp:Label>
                                         </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Not Applicable">
                                <ItemTemplate>
                                     <asp:LinkButton ID="lnkgvNotApplicable" runat="server" OnClick="Edit_Click">
                                    <asp:Label ID="lblgvNotApplicable" CssClass="lblCaption1" runat="server" Text='<%# Bind("FPA_NOT_APPLICABLEDesc") %>'></asp:Label>
                                         </asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>


                    </asp:GridView>
                </div>
            </ContentTemplate>
        </asp:TabPanel>
    </asp:TabContainer>
    <style type="text/css">
        .style1
        {
            width: 20px;
            height: 32px;
        }

        .modalBackground
        {
            background-color: black;
            filter: alpha(opacity=40);
            opacity: 0.5;
        }

        .modalPopup
        {
            background-color: #ffffff;
            border-width: 3px;
            border-style: solid;
            border-color: Gray;
            padding: 3px;
            width: 250px;
            top: -1000px;
            position: absolute;
            padding: 0px10px10px10px;
        }
    </style>
</asp:Content>
