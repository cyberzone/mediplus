﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_BAL;


namespace Mediplus.EMR.Patient
{
    public partial class InitialOPAssessment : System.Web.UI.Page
    {
        EMR_InitialOPAssessment objIniti = new EMR_InitialOPAssessment();


        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(this.Page.Header.Title + "." + strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindData()
        {



            DataSet ds = new DataSet();
            EMR_OPDAssessment objOPD = new EMR_OPDAssessment();

            string Criteria = " 1=1   and EIOA_PT_ID ='" + Convert.ToString(Session["EMR_PT_ID"]) + "' AND EIOA_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
            CommonBAL objCom = new CommonBAL();
            ds = objCom.fnGetFieldValue(" * ", "EMR_INITIAL_OPASSESSMENT", Criteria, "EIOA_ID");

            if (ds.Tables[0].Rows.Count > 0)
            {

                foreach (DataRow DR in ds.Tables[0].Rows)
                {

                    radFunctional.SelectedValue = Convert.ToString(DR["EIOA_FUNCTIONAL"]);
                    radAssRequired.SelectedValue = Convert.ToString(DR["EIOA_ASSESSMENT_REQUIRED"]);
                    radAppetite.SelectedValue = Convert.ToString(DR["EIOA_APPETITE"]);
                    radSwallowing.SelectedValue = Convert.ToString(DR["EIOA_SWALLOWING"]);
                    radDentition.SelectedValue = Convert.ToString(DR["EIOA_DENTITION"]);

                    string strWaitLoss = Convert.ToString(DR["EIOA_WEIGHT_LOSS"]);
                    string[] arrWaitLoss = strWaitLoss.Split('|');

                    if (arrWaitLoss.Length > 0)
                    {


                        if (radWeightLossNo.Value == arrWaitLoss[0])
                        {
                            radWeightLossNo.Checked = true;
                        }
                        else if (radWeightLossYes.Value == arrWaitLoss[0])
                        {
                            radWeightLossYes.Checked = true;
                        }
                        if (arrWaitLoss.Length > 1)
                        {
                            txtKgLoss.Text = arrWaitLoss[1];
                        }
                        if (arrWaitLoss.Length > 2)
                        {
                            txtMonthsLoss.Text = arrWaitLoss[2];
                        }
                    }

                    string strWaitgain = Convert.ToString(DR["EIOA_WEIGHT_GAIN"]);
                    string[] arrWaitgain = strWaitgain.Split('|');

                    if (arrWaitgain.Length > 0)
                    {

                        if (radWeightGainNo.Value == arrWaitgain[0])
                        {
                            radWeightGainNo.Checked = true;
                        }
                        else if (radWeightGainYes.Value == arrWaitgain[0])
                        {
                            radWeightGainYes.Checked = true;
                        }

                        if (arrWaitgain.Length > 1)
                        {
                            txtKgGain.Text = arrWaitgain[1];
                        }
                        if (arrWaitgain.Length > 2)
                        {
                            txtMonthsGain.Text = arrWaitgain[2];
                        }
                    }

                    string strChronicIllness = Convert.ToString(DR["EIOA_CHRONIC_ILLNESS"]);
                    string[] arrChronicIllness = strChronicIllness.Split('|');

                    if (arrChronicIllness.Length > 0)
                    {
                        string strSelectedData = arrChronicIllness[0];
                        string[] arrSelectedData = strSelectedData.Split('~');

                        if (arrSelectedData.Length > 0)
                        {
                            for (Int32 i = 0; i < radChronicIllness.Items.Count; i++)
                            {
                                for (Int32 j = 0; j < arrSelectedData.Length - 1; j++)
                                {
                                    if (radChronicIllness.Items[i].Text == arrSelectedData[j])
                                    {
                                        radChronicIllness.Items[i].Selected = true;
                                        goto EndDataLoop;
                                    }
                                }
                            EndDataLoop: ;
                            }
                        }

                        if (arrChronicIllness.Length > 1)
                        {
                            txtChronicIllness.Text = arrChronicIllness[1];

                        }
                    }



                    radGastrostomy.SelectedValue = Convert.ToString(DR["EIOA_GASTROSTOMY"]);
                    radNGTube.SelectedValue = Convert.ToString(DR["EIOA_NG_TUBE"]);

                    string strTypeofDiet = Convert.ToString(DR["EIOA_DIET_TYPE"]);
                    string[] arrTypeofDiet = strTypeofDiet.Split('|');

                    if (arrTypeofDiet.Length > 0)
                    {
                        // radTypeofDiet.SelectedValue = arrTypeofDiet[0];

                        if (radTypeofDietNor.Value == arrTypeofDiet[0])
                        {
                            radTypeofDietNor.Checked = true;
                        }
                        else if (radTypeofDietSpe.Value == arrTypeofDiet[0])
                        {
                            radTypeofDietSpe.Checked = true;
                        }

                        if (arrTypeofDiet.Length > 1)
                        {
                            txtTypeofDiet.Text = arrTypeofDiet[1];
                        }
                    }



                    radMood.SelectedValue = Convert.ToString(DR["EIOA_MOOD"]);
                    radExpressed.SelectedValue = Convert.ToString(DR["EIOA_EXPRESSED"]);
                    radSupportSystem.SelectedValue = Convert.ToString(DR["EIOA_SUPPORT_SYSTEM"]);
                    txtCurrentMedications.Text = Convert.ToString(DR["EIOA_CURRENT_MEDICATIONS"]);

                    string strAddAssmentNeeded = Convert.ToString(DR["EIOA_ADDITIONAL_ASSESSMENT"]);
                    string[] arrAddAssmentNeeded = strAddAssmentNeeded.Split('|');

                    if (arrAddAssmentNeeded.Length > 0)
                    {
                        radtxtAddAssmentNeeded.SelectedValue = arrAddAssmentNeeded[0];
                        txtAddAssmentNeeded.Text = arrAddAssmentNeeded[1];

                    }


                    txtMeasurableGoals.Text = Convert.ToString(DR["EIOA_MEASURABLE_GOALS"]);
                    txtIntervention.Text = Convert.ToString(DR["EIOA_INTERVENTION"]);
                    txtEvaluation.Text = Convert.ToString(DR["EIOA_EVALUATION"]);
                    txtPhysicalTherapy.Text = Convert.ToString(DR["EIOA_PHYSICAL_THERAPY"]);





                    string strSpecType = Convert.ToString(DR["EIOA_SPECIALIZED_TYPE"]);
                    string[] arrSpecType = strSpecType.Split('|');

                    if (arrSpecType.Length > 0)
                    {
                        if (radElderly.Value == arrSpecType[0])
                        {
                            radElderly.Checked = true;
                        }
                        else if (radFinding.Value == arrSpecType[0])
                        {
                            radFinding.Checked = true;
                        }
                        if (arrSpecType.Length > 1)
                        {
                            txtFindingDesc.Text = arrSpecType[1];
                        }

                    }


                    if (radSpecious.Value == Convert.ToString(DR["EIOA_SPECIOUS_ABUSE"]))
                    {
                        radSpecious.Checked = true;
                    }




                    string strAntenatal = Convert.ToString(DR["EIOA_ANTENATAL_PT"]);
                    string[] arrAntenatal = strAntenatal.Split('|');

                    if (arrAntenatal.Length > 0)
                    {
                        if (radAntenatal.Value == arrAntenatal[0])
                        {
                            radAntenatal.Checked = true;
                        }
                        if (arrAntenatal.Length > 1)
                        {
                            txtAntenatalDesc.Text = arrAntenatal[1];
                        }
                    }



                    string strVacc = Convert.ToString(DR["EIOA_VACCINATION"]);
                    string[] arrVacc = strVacc.Split('|');

                    if (arrAntenatal.Length > 0)
                    {
                        if (radVaccScheduke.Value == arrVacc[0])
                        {
                            radVaccScheduke.Checked = true;
                        }
                        else if (radVaccNotKnown.Value == arrVacc[0])
                        {
                            radVaccNotKnown.Checked = true;
                        }
                        else if (radVaccMissing.Value == arrVacc[0])
                        {
                            radVaccMissing.Checked = true;
                        }
                        if (arrVacc.Length > 1)
                        {
                            txtVaccMissingDesc.Text = arrVacc[1];
                        }

                    }
                }
            }




        }

        void BindAuditDtls()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1   and EIOA_PT_ID ='" + Convert.ToString(Session["EMR_PT_ID"]) + "' AND EIOA_ID ='" + Convert.ToString(Session["EMR_ID"]) + "'";
            objIniti = new EMR_InitialOPAssessment();

            DS = objIniti.InitialOPAssessmentGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                lblCreatedBy.Text = Convert.ToString(DS.Tables[0].Rows[0]["CreatedUserName"]);
                lblCreatedDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["CreatedDate"]);
                lblModifiedBy.Text = Convert.ToString(DS.Tables[0].Rows[0]["ModifiedUserName"]);
                lblModifiedDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["ModifiedDate"]);
            }
        }

        void BindAssessmentHistor()
        {

            DataSet DS = new DataSet();
            //EMR_OPDAssessment objOPD = new EMR_OPDAssessment();

            string Criteria = " 1=1   and EIOA_PT_ID ='" + Convert.ToString(Session["EMR_PT_ID"]) + "' AND EIOA_ID !='" + Convert.ToString(Session["EMR_ID"]) + "'";
            objIniti = new EMR_InitialOPAssessment();

            DS = objIniti.InitialOPAssessmentGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                gvAssessmentHistory.DataSource = DS;
                gvAssessmentHistory.DataBind();
            }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Convert.ToString(Session["User_ID"]) == "" || Convert.ToString(Session["User_ID"]) == null)
            {
                Session["ErrorMsg"] = "Session Expired Please login again";
                Response.Redirect("../../ErrorPage.aspx");
            }

            try
            {
                this.Page.Title = "InitialOPAssessment";
                if (!IsPostBack)
                {
                    if (Convert.ToString(Session["EPM_STATUS"]).ToUpper() == "Y" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                    {
                        btnSave.Visible = false;
                    }
                    //HIDE THE SAVE OPTION FOR NEXT DAY OF THE VISIT
                    if (Convert.ToString(Session["EMRCurrentDateDiff"]) != "0" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                    {
                        btnSave.Visible = false;
                    }
                    BindData();
                    BindAssessmentHistor();
                    if (Convert.ToString(Session["EMR_DISPLAY_AUDIT"]) == "1")
                    {
                        BindAuditDtls();
                        divAuditDtls.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                objIniti = new EMR_InitialOPAssessment();

                objIniti.branchid = Convert.ToString(Session["Branch_ID"]);
                objIniti.patientmasterid = Convert.ToString(Session["EMR_ID"]);
                objIniti.PatientID = Convert.ToString(Session["EMR_PT_ID"]);
                objIniti.EIOA_FUNCTIONAL = radFunctional.SelectedValue;
                objIniti.EIOA_ASSESSMENT_REQUIRED = radAssRequired.SelectedValue;
                objIniti.EIOA_APPETITE = radAppetite.SelectedValue;
                objIniti.EIOA_SWALLOWING = radSwallowing.SelectedValue;
                objIniti.EIOA_DENTITION = radDentition.SelectedValue;

                string strWeightLoss = "";
                if (radWeightLossNo.Checked == true)
                {
                    strWeightLoss = radWeightLossNo.Value;
                }
                else if (radWeightLossYes.Checked == true)
                {
                    strWeightLoss = radWeightLossYes.Value;
                }
                objIniti.EIOA_WEIGHT_LOSS = strWeightLoss + "|" + txtKgLoss.Text + "|" + txtMonthsLoss.Text;


                string strWeightGain = "";
                if (radWeightGainNo.Checked == true)
                {
                    strWeightGain = radWeightGainNo.Value;
                }
                else if (radWeightGainYes.Checked == true)
                {
                    strWeightGain = radWeightGainYes.Value;
                }

                objIniti.EIOA_WEIGHT_GAIN = strWeightGain + "|" + txtKgGain.Text + "|" + txtMonthsGain.Text;


                string strChronic = "";

                for (Int32 i = 0; i < radChronicIllness.Items.Count; i++)
                {
                    if (radChronicIllness.Items[i].Selected == true)
                    {
                        strChronic += radChronicIllness.Items[i].Text + "~";
                    }
                }
                objIniti.EIOA_CHRONIC_ILLNESS = strChronic + "|" + txtChronicIllness.Text;



                objIniti.EIOA_GASTROSTOMY = radGastrostomy.SelectedValue;
                objIniti.EIOA_NG_TUBE = radNGTube.SelectedValue;

                string strTypeofDietNor = "";
                if (radTypeofDietNor.Checked == true)
                {
                    strTypeofDietNor = radTypeofDietNor.Value;
                }
                else if (radTypeofDietSpe.Checked == true)
                {
                    strTypeofDietNor = radTypeofDietSpe.Value;
                }


                objIniti.EIOA_DIET_TYPE = strTypeofDietNor + "|" + txtTypeofDiet.Text;
                objIniti.EIOA_MOOD = radMood.SelectedValue;
                objIniti.EIOA_EXPRESSED = radExpressed.SelectedValue;
                objIniti.EIOA_SUPPORT_SYSTEM = radSupportSystem.SelectedValue;
                objIniti.EIOA_CURRENT_MEDICATIONS = txtCurrentMedications.Text;
                objIniti.EIOA_ADDITIONAL_ASSESSMENT = radtxtAddAssmentNeeded.SelectedValue + "|" + txtAddAssmentNeeded.Text;
                objIniti.EIOA_MEASURABLE_GOALS = txtMeasurableGoals.Text;
                objIniti.EIOA_INTERVENTION = txtIntervention.Text;
                objIniti.EIOA_EVALUATION = txtEvaluation.Text;
                objIniti.EIOA_PHYSICAL_THERAPY = txtPhysicalTherapy.Text;

                string strSpecType = "";
                if (radElderly.Checked == true)
                {
                    strSpecType = radElderly.Value;
                }
                else if (radFinding.Checked == true)
                {
                    strSpecType = radFinding.Value;
                }
                objIniti.EIOA_SPECIALIZED_TYPE = strSpecType + "|" + txtFindingDesc.Text;

                if (radSpecious.Checked == true)
                {
                    objIniti.EIOA_SPECIOUS_ABUSE = radSpecious.Value;
                }
                else
                {
                    objIniti.EIOA_SPECIOUS_ABUSE = "";
                }


                string strAntenatal = "";
                if (radAntenatal.Checked == true)
                {
                    strAntenatal = radAntenatal.Value;
                }

                objIniti.EIOA_ANTENATAL_PT = strAntenatal + "|" + txtAntenatalDesc.Text;

                string strVacc = "";
                if (radVaccScheduke.Checked == true)
                {
                    strVacc = radVaccScheduke.Value;
                }
                else if (radVaccNotKnown.Checked == true)
                {
                    strVacc = radVaccNotKnown.Value;
                }
                else if (radVaccMissing.Checked == true)
                {
                    strVacc = radVaccMissing.Value;
                }


                objIniti.EIOA_VACCINATION = strVacc + "|" + txtVaccMissingDesc.Text;

                objIniti.UserID = Convert.ToString(Session["User_ID"]);

                objIniti.InitialOPAssessmentAdd();

                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowMessage()", true);
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnSave1_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void Edit_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;


                Label lblEMRID = (Label)gvScanCard.Cells[0].FindControl("lblEMRID");


                string strRptPath = "";
                strRptPath = "../WebReports/SHReports/InitialOPAssessment.aspx";
                string rptcall = @strRptPath + "?EMR_ID=" + Convert.ToString(Session["EMR_ID"]) + "&EMR_PT_ID=" + Convert.ToString(Session["EMR_PT_ID"]) + "&DR_ID=" + lblEMRID.Text;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Presc", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=700,width=1075,scrollbars=1');", true);


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "       Edit_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }

      
    }
}