﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PainScore.ascx.cs" Inherits=" Mediplus.EMR.Patient.PainScore" %>
<link href="../Styles/Maincontrols.css"  rel="stylesheet" type="text/html" />
<link href="../Styles/style.css" rel="stylesheet" type="text/html" />

<style type="text/css">
    #pain-score-tab .table.grid tbody td {
        text-align: left;
    }

    #pain-score-content {
        position: relative;
    }

    #pain-score-left-content {
        width:450px;
    }

    #pain-score-left-content .table tbody tr td {
        padding:2px 5px;
    }

   

    .pain-smilie
    {
        padding:5px;
        width:48px;
        height:48px;
    }

    tr.pain-selected img.pain-smilie
    {
        border:1px solid #02a0e0;
        border-radius:3px;
    }
</style>
 <table width="100%">
        <tr>
              <td style="text-align: right; width: 100%;">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="btnSave" runat="server" CssClass="button orange small" Text="Save Record" OnClick="btnSave_Click" />
                    </ContentTemplate>
                </asp:UpdatePanel>

            </td>
        </tr>
    </table>
 <div id="pain-score-content">
        <div id="pain-score-left-content">
            <fieldset>
                <legend class="lblCaption1" style="font-weight:bold;">Pain Score</legend>
                <table class="table spacy">
                    <tbody>
                        <tr>
                            <td><label for="PainValue0" class="lblCaption1" >No Pain</label></td>
                            <td class="lblCaption1" >
                                <input type="radio" name="PainValue" id="PainValue0" value="0" class="lblCaption1" runat="server" /> 0<br />
                                <input type="radio" name="PainValue" id="PainValue1" value="1"  class="lblCaption1"  runat="server"/> 1
                            </td>
                            <td><img src="../Images/smilie-1.png" alt="Pain Score" class="pain-smilie"/></td>
                        </tr>
                        <tr>
                            <td><label for="PainValue2" class="lblCaption1" >Mild annoying pain</label></td>
                            <td class="lblCaption1" >
                                <input type="radio" name="PainValue" id="PainValue2" value="2"  class="lblCaption1" runat="server" /> 2<br />
                                <input type="radio" name="PainValue" id="PainValue3" value="3"  class="lblCaption1"  runat="server"/> 3
                            </td>
                            <td><img src="../images/smilie-2.png" alt="Pain Score" class="pain-smilie" /></td>
                        </tr>
                        <tr>
                            <td><label for="PainValue4" class="lblCaption1" >Nagging,uncomfortable troublesome pain</label></td>
                            <td class="lblCaption1" >
                                <input type="radio" name="PainValue" id="PainValue4" value="4" class="lblCaption1"  runat="server"/> 4<br />
                                <input type="radio" name="PainValue" id="PainValue5" value="5" class="lblCaption1"  runat="server"/> 5
                            </td>
                            <td><img src="../images/smilie-3.png" alt="Pain Score" class="pain-smilie" /></td>
                        </tr>
                        <tr>
                            <td><label for="PainValue6" class="lblCaption1" >Distressing, miserable Pain</label></td>
                            <td class="lblCaption1" >
                                <input type="radio" name="PainValue" id="PainValue6" value="6" class="lblCaption1"  runat="server"/> 6<br />
                                <input type="radio" name="PainValue" id="PainValue7" value="7" class="lblCaption1" runat="server" /> 7
                            </td>
                            <td><img src="../images/smilie-4.png" alt="Pain Score" class="pain-smilie" /></td>
                        </tr>
                        <tr>
                            <td><label for="PainValue8" class="lblCaption1" >Intense, dreadful horrible pain</label></td>
                            <td class="lblCaption1" >
                                <input type="radio" name="PainValue" id="PainValue8" value="8" class="lblCaption1"  runat="server"/> 8<br />
                                <input type="radio" name="PainValue" id="PainValue9" value="9" class="lblCaption1" runat="server"/> 9
                            </td>
                            <td><img src="../images/smilie-5.png" alt="Pain Score" class="pain-smilie" /></td>
                        </tr>
                        <tr>
                            <td><label for="PainValue10" class="lblCaption1" >Worst possible, unbearable, excruciating Pain</label></td>
                            <td class="lblCaption1" >
                                <input type="radio" name="PainValue" id="PainValue10" value="10" class="lblCaption1"  runat="server"/> 10<br />
                            </td>
                            <td><img src="../images/smilie-6.png" alt="Pain Score" class="pain-smilie" /></td>
                        </tr>
                    </tbody>
                </table>
            </fieldset>
        </div>
      <div id="divPainRIghtCont" runat="server" style="  position:absolute;top:0;right:0;width: 300px" >
            <fieldset class="fieldset">
                <legend class="lblCaption1" style="font-weight:bold;">Pain Details</legend>
                <table class="table spacy">
                        <tr class="pain-segment-fields">
                            <td class="lblCaption1" > <label for="Location">Location</label></td>
                            <td><input type="text" class="pain-input" id="txtLocation" name="txtLocation" runat="server" /></td>
                        </tr>
                        <tr class="pain-segment-fields">
                            <td class="lblCaption1" > <label for="txtIntensity">Intensity</label></td>
                            <td><input type="text" class="pain-input" id="txtIntensity" name="txtIntensity" runat="server" /></td>
                        </tr>
                        <tr class="pain-segment-fields">
                            <td class="lblCaption1" > <label for="txtIntensity">Character</label></td>
                            <td><input type="text" class="pain-input" id="txtCharacter" name="txtCharacter" runat="server" /></td>
                        </tr>
                        <tr class="pain-segment-fields">
                            <td class="lblCaption1" > <label for="txtIntensity">Duration</label></td>
                            <td><input type="text" class="pain-input" id="txtDuration" name="txtDuration"  runat="server"/></td>
                        </tr>
                        <tr class="pain-segment-fields">
                            <td class="lblCaption1" >Referred to<label for="paininput"></label></td>
                            <td><input type="text" class="pain-input" id="txtReferredTo" name="txtReferredTo"  runat="server"/></td>
                        </tr>
                                         
                   
                </table>
            </fieldset>
        </div>
     </div>
