﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PainAssessment.ascx.cs" Inherits=" Mediplus.EMR.Patient.PainAssessment" %>
<link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/html" />
<link href="../Styles/style.css" rel="stylesheet" type="text/html" />

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

 <script language="javascript" type="text/javascript">
     function ShowAuditLogReAssess() {

         var win = window.open('../AuditLogDisplay.aspx?ScreenID=OP_PAIN_ASSESS&DataDisplay=ByEMR_ID', 'AuditLogDisplay', 'menubar=no,left=100,top=80,height=550,width=1075,scrollbars=1')

     }
    </script>


  <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
               <td style="width:100%;text-align:right;">
 
              <a href="javascript:ShowAuditLogReAssess();" style="text-decoration: none;" class="lblCaption">Audit Log </a>
   

            </td>
        </tr>
</table>
  <table width="100%">
        <tr>
              <td style="text-align: right; width: 100%;">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="btnSave" runat="server" CssClass="button orange small" Text="Save Record" OnClick="btnSave_Click" />
                    </ContentTemplate>
                </asp:UpdatePanel>

            </td>
        </tr>
    </table>
<fieldset>
    <legend class="lblCaption1" style="font-weight: bold;">Pain Assessment</legend>
       
    <table width="100%">
        <tr>
            <td style="text-align: left; width: 50%;">
                <asp:CheckBox ID="chkNotApplicable" runat="server" CssClass="lblCaption1"  Text ="Not Applicable" AutoPostBack="true"  OnCheckedChanged="chkNotApplicable_CheckedChanged" />
            </td>
        </tr>
    </table>

    <table style="width: 100%;">
        <tr>
            <td class="lblCaption1">Date & Time
            </td>
            <td colspan="3">
                <asp:TextBox ID="txtDate" runat="server" Width="90px" Height="22px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                <asp:TextBox ID="txtTime" runat="server" ReadOnly="true" Width="50px" Height="22px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" MaxLength="10"></asp:TextBox>
                <asp:CalendarExtender ID="Calendarextender3" runat="server"
                    Enabled="True" TargetControlID="txtDate" Format="dd/MM/yyyy">
                </asp:CalendarExtender>
            </td>
        </tr>
        <tr>
            <td class="lblCaption1">Pain Location 

            </td>
            <td>
                <asp:TextBox ID="txtPaintLocation" runat="server" CssClass="label" Width="150px"></asp:TextBox>
            </td>
            <td class="lblCaption1">On Set 

            </td>
            <td>
                <asp:DropDownList ID="drpOnSet" runat="server" CssClass="label" Width="155px">
                    <asp:ListItem Selected="True" Text="--- Select ---" Value="0"></asp:ListItem>
                    <asp:ListItem Text="Sudden" Value="Sudden"></asp:ListItem>
                    <asp:ListItem Value="Gradual" Text="Gradual"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="lblCaption1">Rhythm
            </td>
            <td>
                <asp:DropDownList ID="drpRhythm" runat="server" CssClass="label" Width="155px">
                    <asp:ListItem Selected="True" Text="--- Select ---" Value="0"></asp:ListItem>
                    <asp:ListItem Text="Continuous" Value="Continuous"></asp:ListItem>
                    <asp:ListItem Value="Intermittent " Text="Intermittent "></asp:ListItem>
                    <asp:ListItem Value="Episodic" Text="Episodic"></asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="lblCaption1">Intensity of pain</td>
            <td>
                <asp:TextBox ID="txtIntensity" runat="server" CssClass="label" Width="150px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="lblCaption1">Pain scale used</td>
            <td>
                <asp:TextBox ID="txtPainScale" runat="server" CssClass="label" Width="150px"></asp:TextBox>
            </td>
            <td class="lblCaption1">Associated symptoms</td>
            <td>
                <asp:TextBox ID="txtAssociatedSymptoms" runat="server" CssClass="label" Width="150px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="lblCaption1">Pharmacological Management </td>
            <td>
                <asp:TextBox ID="txtPharmacologicalMana" runat="server" CssClass="label" Width="150px"></asp:TextBox>
            </td>
            <td class="lblCaption1">Non-Pharmacological Management 
            </td>
            <td>
                <asp:TextBox ID="txtNonPharmacologicalMana" runat="server" CssClass="label" Width="150px"></asp:TextBox>
            </td>
        </tr>

        <tr>
            <td class="lblCaption1">Pain Character
            </td>
            <td colspan="3">
                <asp:DropDownList ID="drpPainCharacter" runat="server" CssClass="label" Width="155px">
                    <asp:ListItem Selected="True" Text="--- Select ---" Value=""></asp:ListItem>
                    <asp:ListItem Text="Aching" Value="Aching"></asp:ListItem>
                    <asp:ListItem Value="Burining" Text="Burining"></asp:ListItem>
                    <asp:ListItem Value="Cramping" Text="Cramping"></asp:ListItem>

                    <asp:ListItem Value="Crushing" Text="Crushing"></asp:ListItem>
                    <asp:ListItem Value="Dull" Text="Dull"></asp:ListItem>
                    <asp:ListItem Value="Numbness" Text="Numbness"></asp:ListItem>
                </asp:DropDownList>
            </td>


        </tr>
        <tr>
            <td class="lblCaption1">Comments and Action </td>
            <td colspan="3">
                <asp:TextBox ID="txtComment" runat="server" CssClass="label" TextMode="MultiLine" Height="50px" Width="100%" style="resize:none;"></asp:TextBox>
            </td>
        </tr>
    </table>
   
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:GridView ID="gvPainAss" runat="server" AutoGenerateColumns="False"
                EnableModelValidation="True" Width="100%" GridLines="none">
                <HeaderStyle CssClass="GridHeader_Blue" Font-Bold="true" />
                <RowStyle CssClass="GridRow" />
                <Columns>
                    <asp:TemplateField HeaderText="Date">
                        <ItemTemplate>
                            <asp:Label ID="lblDate" CssClass="GridRow" runat="server" Text='<%# Bind("EPA_DATEDesc") %>'></asp:Label>

                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Location">
                        <ItemTemplate>
                            <asp:Label ID="lblLocation" CssClass="GridRow" runat="server" Text='<%# Bind("EPA_PAIN_LOCATION") %>'></asp:Label>


                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Height">
                        <ItemTemplate>
                            <asp:Label ID="lblOnSet" CssClass="GridRow" runat="server" Text='<%# Bind("EPA_ONSET") %>'></asp:Label>

                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Rhythm">
                        <ItemTemplate>
                            <asp:Label ID="lblRhythm" CssClass="GridRow" runat="server" Text='<%# Bind("EPA_RHYTHM") %>'></asp:Label>

                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Intensity of pain">
                        <ItemTemplate>
                            <asp:Label ID="lblPulse" CssClass="GridRow" runat="server" Text='<%# Bind("EPA_PAIN_INTENSITY") %>'></asp:Label>

                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Pain scale used">
                        <ItemTemplate>
                            <asp:Label ID="lblRespiration" CssClass="GridRow" runat="server" Text='<%# Bind("EPA_PAIN_SCALE_USED") %>'></asp:Label>

                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Associated symptoms">
                        <ItemTemplate>
                            <asp:Label ID="Label1" CssClass="GridRow" runat="server" Text='<%# Bind("EPA_ASSOCIATED_SYMPTOMS") %>'></asp:Label>

                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Pharmacological Manag.">
                        <ItemTemplate>
                            <asp:Label ID="Label2" CssClass="GridRow" runat="server" Text='<%# Bind("EPA_PHARMACOLOGICAL_MANG") %>'></asp:Label>

                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Non-Pharmacological Manag.">
                        <ItemTemplate>
                            <asp:Label ID="Label23" CssClass="GridRow" runat="server" Text='<%# Bind("EPA_NONPHARMACOLOGICAL_MANG") %>'></asp:Label>

                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Comments">
                        <ItemTemplate>
                            <asp:Label ID="Label23" CssClass="GridRow" runat="server" Text='<%# Bind("EPA_COMMENTS") %>'></asp:Label>

                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Pain Character">
                        <ItemTemplate>
                            <asp:Label ID="lblPainCharacter" CssClass="GridRow" runat="server" Text='<%# Bind("EPA_PAIN_CHARACTER") %>'></asp:Label>

                        </ItemTemplate>
                    </asp:TemplateField>


                </Columns>


            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
</fieldset>