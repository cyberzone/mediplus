﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TemplatePopup.aspx.cs" Inherits=" Mediplus.EMR.Patient.TemplatePopup" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">

        function passvalue(TempData) {

            var CtrlName = document.getElementById('hidCtrlName').value;

            window.opener.BindTemplate(TempData, CtrlName);
            window.close();

        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <asp:Panel ID="pnlSaveTemplate" runat="server" Height="250px" Width="450px" CssClass="modalPopup">
    <table cellpadding="0" cellspacing="0" width="100%" style="">
        <tr>
            <td class="lblCaption" style="font-weight: bold;">Template List
            </td>
        </tr>

        <tr>
            <td class="lblCaption1" style="height: 100px; vertical-align: top;">
                <fieldset style="height: 100px;">
                    <legend class="lblCaption1" style="font-weight: bold;">Save Template</legend>
                    <table>
                        <tr>
                            <td>Template Name : 
                            </td>
                            <td>
                                <input type="text" id="txtTemplateName" name="txtTemplateName" runat="server" class="label" />
                              
                            </td>
                        </tr>
                        <tr>
                            <td>Other Doctors : 
                            </td>
                            <td>
                               <asp:CheckBox id="chkOtherDr" runat="server" Text="Apply"  />
                            </td>
                        </tr>
                        
                    </table>
                </fieldset>
            </td>
        </tr>
        <tr>
            <td style="text-align: right;">
                <asp:Button ID="btnSaveTemp" runat="server" Text="Save Template" OnClick="btnSaveTemp_Click" CssClass="button blue small" />
                <asp:Button ID="btlCloseTemp" runat="server" Text="Close" CssClass="button blue small"  OnClientClick=" window.close();" />

            </td>
        </tr>

    </table>
</asp:Panel>
    </div>
    </form>
</body>
</html>
