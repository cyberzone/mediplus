﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using EMR_BAL;


namespace Mediplus.EMR.Patient
{
    public partial class PatientDashboardPopup : System.Web.UI.Page
    {
        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindTemplate()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();

            string[] arrTemplateType = Convert.ToString(ViewState["TemplateType"]).Split('|');

            string strType = "";
            if (arrTemplateType.Length > 1)
            {
                strType = arrTemplateType[0];
            }


            string Criteria = " 1=1  AND (  ET_TYPE='" + Convert.ToString(ViewState["TemplateType"]) + "' OR ET_TYPE='" + strType + "')";

            Criteria += " AND ET_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' ";

            Criteria += " AND ( ET_DR_CODE='" + Convert.ToString(Session["HPV_DR_ID"]) + "' OR ET_Apply=1 )";

            if (GlobalValues.FileDescription.ToUpper() != "MAMPILLY")
            {
                Criteria += " AND ( ET_DEP_ID='" + Convert.ToString(Session["HPV_DEP_NAME"]) + "' OR  ET_DEP_ID='' OR  ET_DEP_ID IS NULL )";
            }

            DS = objCom.TemplatesGet(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                gvTemplates.DataSource = DS;
                gvTemplates.DataBind();
            }

            else
            {
                gvTemplates.DataBind();
            }

            // objCom.BindMasterDropdown(Criteria, out drpDossage);

        }


        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    hidCtrlName.Value = Convert.ToString(Request.QueryString["CtrlName"]);
                    ViewState["TemplateType"] = Convert.ToString(Request.QueryString["TemplateType"]);

                    BindTemplate();

                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      PatientDashboardPopup.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }

            }
        }

        protected void btnOK_Click(object sender, EventArgs e)
        {
            try
            {
                string strData = "";
                for(Int32 i=0;i < gvTemplates.Rows.Count ;i++)
                {
                    
                    Label lblTempCode = (Label)gvTemplates.Rows[i].Cells[0].FindControl("lblTempCode");
                    Label lblTempData = (Label)gvTemplates.Rows[i].Cells[0].FindControl("lblTempData");
                    CheckBox chk1 = (CheckBox)gvTemplates.Rows[i].Cells[0].FindControl("chk1");
                    if (chk1.Checked == true)
                    {
                        if (strData != "")
                        {
                            strData += "," + lblTempData.Text;
                        }
                        else
                        {
                            strData += lblTempData.Text;
                        }
                    }

                }

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LabResult", "passvalue('" + strData + "');", true);


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientDashboardPopup.btnOK_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }
    }
}