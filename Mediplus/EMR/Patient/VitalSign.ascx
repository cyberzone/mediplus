﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VitalSign.ascx.cs" Inherits=" Mediplus.EMR.Patient.VitalSign" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<link href="../../Styles/Maincontrols.css"  rel="stylesheet" type="text/html" />
<link href="../../Styles/style.css" rel="stylesheet" type="text/html" />


    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.7.1.js" type="text/javascript"></script>

<style type="text/css">
    #vital-signs-top-content {
        position:relative;
    }
    #vital-signs-top-left-content {
        width:600px;
    }
    #vital-signs-top-right-content {
        position:absolute;
        right:10px;
        top:0;
        width:260px;
    }
    textarea#EcgReport, textarea#Others { width:100%;height:80px; }


</style>

 <style type="text/css">
        .box-header
        {
            border-bottom: 4px solid #f4fbfd;
            height: 33px;
        }

            .box-title
            {
                padding-bottom: 5px;
                border-bottom: 4px solid #92c500;
                float: left;
                font-size: 1.5em;
                color: #2078c0;
            }
            h1, h2, h3, h4, h5, h6 {
                font-size: 100%;
                font-weight: normal;
                font-family: "Segoe UI", Arial, Helvetica, sans-serif;
                }
    </style>

     <style type="text/css">
    .style1
    {
        width: 20px;
        height: 32px;
    }

    .modalBackground
    {
        background-color: black;
        filter: alpha(opacity=40);
        opacity: 0.5;
    }

    .modalPopup
    {
        background-color: #ffffff;
        border-width: 3px;
        border-style: solid;
        border-color: Gray;
        padding: 3px;
        width: 250px;
        top: -1000px;
        position: absolute;
        padding: 0px10px10px10px;
    }
</style>
<script language="javascript" type="text/javascript">

    function OnlyNumeric(evt) {
        var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
        if (chCode >= 48 && chCode <= 57 ||
             chCode == 46) {
            return true;
        }
        else

            return false;
    }


    function SaveVitalVal() {
        var label;





    }

    </script>

    <script language="javascript" type="text/javascript">
        function GetFtoC() {
            var temp = document.getElementById("<%=txtTemperatureF.ClientID%>").value;
            var tempInF = Math.round(((100 / (212 - 32)) * (temp - 32)) * 100) / 100;
            return tempInF
        }


        function FtoC() {
            var temp = document.getElementById("<%=txtTemperatureF.ClientID%>").value;
            var tempInF = Math.round(((100 / (212 - 32)) * (temp - 32)) * 100) / 100;
            document.getElementById("<%=txtTemperatureC.ClientID%>").value = tempInF;
            if (temp == 0 || temp == "") {
                document.getElementById("<%=txtTemperatureC.ClientID%>").value = '';
            }
        }
        function CtoF() {
            var celc = document.getElementById("<%=txtTemperatureC.ClientID%>").value;
            var tempInC = Math.round((((212 - 32) / 100) * celc + 32) * 100) / 100;
            document.getElementById("<%=txtTemperatureF.ClientID%>").value = tempInC;
            if (celc == 0 || celc == "") {
                document.getElementById("<%=txtTemperatureF.ClientID%>").value = '';
            }
        }

        function BMI(Wt, Ht) {
            if (Wt == 0 || Ht == 0)
                return 0;
            return ((parseFloat(Wt) / parseFloat(Ht) / parseFloat(Ht)) * 10000).toFixed(2)
        }

        function SetCalculation() {
            var wt = document.getElementById("<%=txtWeight.ClientID%>").value;
            var ht = document.getElementById("<%=txtHeight.ClientID%>").value;

            var varBmi = BMI(wt, ht)
            document.getElementById("<%=txtBMI.ClientID%>").value = varBmi;
            Calculation1(varBmi);
        }

        function Calculation1(BMIValue) {
            var vGender = '<%=Gender%>';
            var BMITABLE = jQuery.parseJSON('[{"Gender":"Male","BMIfrom":0,"BMIto":18.5,"BMIresult":"Under Weight"},' +
                '{"Gender":"Male","BMIfrom":18.6,"BMIto":24.9,"BMIresult":"Normal"},' +
                '{"Gender":"Male","BMIfrom":25,"BMIto":29.9,"BMIresult":"Over weight"},' +
                '{"Gender":"Male","BMIfrom":30,"BMIto":30000,"BMIresult":"Obese"},' +
                '{"Gender":"Male","BMIfrom":35,"BMIto":39.9,"BMIresult":"Extreme Obesity"},' +
                '{"Gender":"Male","BMIfrom":40,"BMIto":12000,"BMIresult":"Morbid Obesity"},' +
                '{"Gender":"Female","BMIfrom":0,"BMIto":18.9,"BMIresult":"UnderWeight"},' +
                '{"Gender":"Female","BMIfrom":19.0,"BMIto":24.9,"BMIresult":"Normal"},' +
                '{"Gender":"Female","BMIfrom":25,"BMIto":29.9,"BMIresult":"Over weight"},' +
                '{"Gender":"Female","BMIfrom":30,"BMIto":30000,"BMIresult":"Obese"},' +
                '{"Gender":"Female","BMIfrom":35,"BMIto":39.9,"BMIresult":"Extreme Obesity"},' +
                '{"Gender":"Female","BMIfrom":40,"BMIto":12000,"BMIresult":"Morbid Obesity"}]');

            // alert(JSON.stringify(BMITABLE));
            $.each(BMITABLE, function (i, item) {
                var gender = vGender == undefined ? 'Male' : vGender;


                if ((BMIValue > item.BMIfrom) && (BMIValue <= item.BMIto) && item.Gender.toLowerCase() == gender.toLowerCase()) {
                    // alert(item.BMIresult);
                    $("#BMIMessage").text(item.BMIresult)
                    return;
                }
            });
        }

        function ShowAuditLog() {

            var win = window.open('../AuditLogDisplay.aspx?ScreenID=OP_VITAL_SIGN&DataDisplay=ByEMR_ID', 'AuditLogDisplay', 'menubar=no,left=100,top=80,height=550,width=1075,scrollbars=1')

        }


        function addDays(date, days) {
            var result = new Date(date);
            result.setDate(result.getDate() + days);
            result.setMonth(result.getMonth() + 10);


            return result;
        }


        function CalculateEDD() {
            var vLMP = document.getElementById("<%=txtLMPDate.ClientID%>").value;

            var arrvLMP = vLMP.split("/");
            var strDate = arrvLMP[2] + '/' + arrvLMP[1] + '/' + arrvLMP[0]


            var vEDD = addDays(strDate, 7);


            var vMonth = vEDD.getMonth()
            if (vMonth == 0) {
                vMonth = 12;
            }

            var vEDDFormated = vEDD.getDate() + "/" + vMonth + '/' + vEDD.getFullYear()

            if (document.getElementById("<%=chkIsPregnant.ClientID%>").checked == true) {

                document.getElementById("<%=txtEDDDate.ClientID%>").value = vEDDFormated;
            }

            //  alert(vEDD.getDate() + "/" + vEDD.getMonth() + '/' + vEDD.getFullYear());
        }
    </script>

<script type="text/javascript">
    jQuery(document).ready(function ($) {
        // onLoad starts
        onLoad();
        // $("#txtWeight, #txtHeight").keyup(SetCalculation);


        function onLoad() {

            // $("#BMIMessage").text("test");
            // alert($("#BMIMessage").val());
            SetCalculation();
        }


    });

</script>

<table cellpadding="0" cellspacing="0" width="100%">
        <tr>
               <td style="width:100%;text-align:right;">
 
              <a href="javascript:ShowAuditLog();" style="text-decoration: none;" class="lblCaption">Audit Log </a>
   

            </td>
        </tr>
</table>
<table width="100%">
        <tr>
            <td style="text-align: right; width: 50%;display:none;" class="lblCaption1"  >
                Template
                   <asp:DropDownList ID="drpTemplate" CssClass="TextBoxStyle" runat="server" Width="155px"   AutoPostBack="true" OnSelectedIndexChanged="drpTemplate_SelectedIndexChanged">
                            </asp:DropDownList>

                            <asp:LinkButton ID="lnkSaveTemp" runat="server" CssClass="label orange" Text="Save" Enabled="false" Width="50px" 
                                Style="width:50px;border-radius:5px 5px 5px 5px; vertical-align: central;text-align:center;"></asp:LinkButton>
                            <asp:Button ID="btnDeleteTemp" runat="server" Text="Delete" OnClick="btnDeleteTemp_Click" style="width:50px;border-radius:5px 5px 5px 5px" CssClass="gray" />

            </td>

              <td style="text-align: right; width: 100%;">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="btnSave" runat="server" CssClass="button red small" Visible="false" Text="Save Record" OnClick="btnSave_Click" />
                    </ContentTemplate>
                </asp:UpdatePanel>

            </td>
        </tr>
    </table>

<table  style="width:100%;">
    <tr>
        <td style="vertical-align:top;">
  <table class="table" border="0" style="padding:3px 0px 3px 0px;width:100%" >
                <tr>
                    <td class="lblCaption1"><label for="txtWeight">Weight</label></td>
                    <td colspan="3" class="lblCaption1">
                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                            <ContentTemplate>
                                 <input id="txtWeight" runat="server" class="TextBoxStyle"  style="width:70px;" onkeypress="return OnlyNumeric(event);" onkeyup="SetCalculation();" />&nbsp;Kg 
                            </ContentTemplate>
                        </asp:UpdatePanel>
                </td>
                </tr>
                <tr>
                    <td class="lblCaption1"><label for="txtHeight">Height</label></td>
                    <td class="lblCaption1">
                          <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                            <ContentTemplate>
                        <input id="txtHeight" runat="server" class="TextBoxStyle"  style="width:70px;" onkeypress="return OnlyNumeric(event);" onkeyup="SetCalculation();" />&nbsp;Cms 
                            </ContentTemplate>
                           </asp:UpdatePanel>            
                                
                    </td>
                    <td class="lblCaption1"><label for="txtBMI">BMI</label></td>
                    <td class="lblCaption1">
                          <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                            <ContentTemplate>
                           <input id="txtBMI" runat="server" class="TextBoxStyle"  style="width:70px;" value="0" /><span id="BMIMessage"></span>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                 
                <tr>
                     
                       
                     <td class="lblCaption1"><label for="txtLMPDate">LMPDate</label></td>
                    <td  class="lblCaption1"> 
                        <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                            <ContentTemplate>
                         <asp:TextBox ID="txtLMPDate" runat="server" Width="70px" MaxLength="10" CssClass="TextBoxStyle" onkeypress="return OnlyNumeric(event);" onchange="CalculateEDD()"></asp:TextBox>
                            <asp:CalendarExtender ID="TextBox1_CalendarExtender" runat="server"
                                Enabled="True" TargetControlID="txtLMPDate" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                            <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" Enabled="true" TargetControlID="txtLMPDate" Mask="99/99/9999" MaskType="Date" ></asp:MaskedEditExtender>

                       <label for="IsPregnant">Pregnant</label> <input type="checkbox" name="chkIsPregnant" id="chkIsPregnant" runat="server" value="1" onchange="CalculateEDD()" />
                       <label for="IsLactating">Lactating</label> <input type="checkbox" name="chkIsLactating" id="chkIsLactating" runat="server" value="1" />
                             </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    
                       <td class="lblCaption1">
                           EDD
                       </td>
                     <td>
                          <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                            <ContentTemplate>
                             <asp:TextBox ID="txtEDDDate" runat="server" Width="70px" MaxLength="10" class="TextBoxStyle"  onkeypress="return OnlyNumeric(event);"  ></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender1" runat="server"
                                Enabled="True" TargetControlID="txtEDDDate" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                            <asp:MaskedEditExtender ID="MaskedEditExtender2" runat="server" Enabled="true" TargetControlID="txtEDDDate" Mask="99/99/9999" MaskType="Date" ></asp:MaskedEditExtender>
                                </ContentTemplate>
                          </asp:UpdatePanel>

                    </td>
                    
                   
                </tr>
                <tr>
                   
                          <td class="lblCaption1"><label for="txtTemperatureF">Hemoglobin</label></td>
                        <td class="lblCaption1">
                             <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                            <ContentTemplate>
                            <input id="txtHemoglobin" runat="server" class="TextBoxStyle"  style="width:70px;"  onkeypress="return OnlyNumeric(event);" />&nbsp;(Unit is ‘g%’) 
                                </ContentTemplate>
                                 </asp:UpdatePanel>
                                
                                </td>


                </tr>
                 <tr id="trCapillary" runat="server" >
                     <td class="lblCaption1"><label for="HeadCircumference">Head Circumference</label></td>
                    <td class="lblCaption1">
                         <asp:UpdatePanel ID="UpdatePanel17" runat="server">
                            <ContentTemplate>
                        <input type="text" name="txtHeadCircumference" id="txtHeadCircumference" style="width:70px"  runat="server"  class="TextBoxStyle" onkeypress="return OnlyNumeric(event);"  /> cm
                                 </ContentTemplate>
                            </asp:UpdatePanel>
                                </td>
                    <td class="lblCaption1"><label for="ChestCircumference">Birth Weight</label></td>
                    <td class="lblCaption1" colspan="3">
                         <asp:UpdatePanel ID="UpdatePanel18" runat="server">
                            <ContentTemplate>
                        <input type="text" name="txtBirthWeight" id="txtBirthWeight" style="width:70px"  runat="server" class="TextBoxStyle"  onkeypress="return OnlyNumeric(event);" /> Kg
                                 </ContentTemplate>
                            </asp:UpdatePanel>
                                </td>

                </tr>
            </table>
    </td>
        <td style="vertical-align:top;">

 <table class="table">
          
                    <tr>
                    <td class="lblCaption1"><label for="txtTemperatureF">Temperature</label></td>
                    <td class="lblCaption1">
                         <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                            <ContentTemplate>
                        <input id="txtTemperatureC" runat="server" class="TextBoxStyle"  style="width:70px;"  onkeypress="return OnlyNumeric(event);" onkeyup="CtoF();"/>&deg;C
                    <div id="divTempF" runat="server" >
                        <input id="txtTemperatureF" runat="server" class="TextBoxStyle"  style="width:70px;"  onkeypress="return OnlyNumeric(event);" onkeyup="FtoC();"/>&nbsp;F 
                    </div>
                                  </ContentTemplate>
                        </asp:UpdatePanel>
                                </td>
                     
                </tr>
        
                <tr>
                    <td class="lblCaption1"><label for="txtWeight">Pulse</label></td>
                    <td class="lblCaption1">
                         <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                            <ContentTemplate>
                        <input id="txtPulse" runat="server" class="TextBoxStyle"  style="width:70px;" />&nbsp;Beats/Min
                                  </ContentTemplate>
                        </asp:UpdatePanel>
                                </td>

                    </tr>
                    <tr>
                        <td  class="lblCaption1"><label for="Respiration">Respiration</label></td>
                        <td  class="lblCaption1">
                             <asp:UpdatePanel ID="UpdatePanel12" runat="server">
                            <ContentTemplate>
                            <input type="text" name="txtRespiration" id="txtRespiration" runat="server" class="TextBoxStyle" onkeypress="return OnlyNumeric(event);"  style="width:70px;"  /> /m
                                </ContentTemplate>
                            </asp:UpdatePanel>
                                </td>
                    </tr>
                    <tr>
                        <td  class="lblCaption1"><label for="BpSystolic">BP:Systolic</label></td>
                        <td  class="lblCaption1">
                             <asp:UpdatePanel ID="UpdatePanel13" runat="server">
                            <ContentTemplate>
                            <input type="text" name="txtBpSystolic" id="txtBpSystolic" runat="server" class="TextBoxStyle"  onkeypress="return OnlyNumeric(event);"   style="width:70px;"  /> mm/hg
                                  </ContentTemplate>
                            </asp:UpdatePanel>
                                
                                </td>
                    </tr>
                    <tr>
                        <td  class="lblCaption1"><label for="BpDiastolic">BP:Diastolic</label></td>
                        <td  class="lblCaption1">
                             <asp:UpdatePanel ID="UpdatePanel14" runat="server">
                            <ContentTemplate>
                            <input type="text" name="txtBpDiastolic" id="txtBpDiastolic" runat="server" class="TextBoxStyle"  onkeypress="return OnlyNumeric(event);"  style="width:70px;"  /> mm/hg
                                  </ContentTemplate>
                            </asp:UpdatePanel>
                                </td>
                    </tr>
                    <tr>
                        <td  class="lblCaption1"><label for="Sp02">SP02</label></td>
                        <td  class="lblCaption1">
                             <asp:UpdatePanel ID="UpdatePanel15" runat="server">
                            <ContentTemplate>
                            <input type="text" name="txtSp02" id="txtSp02" runat="server"    class="TextBoxStyle"  onkeypress="return OnlyNumeric(event);" style="width:70px;" />
                                  </ContentTemplate>
                            </asp:UpdatePanel>
                                </td>
                    </tr>
                    <tr>
                        <td  class="lblCaption1"><label for="Sp02">Capillary Blood Sugar</label></td>
                        <td  class="lblCaption1">
                             <asp:UpdatePanel ID="UpdatePanel16" runat="server">
                            <ContentTemplate>
                            <input type="text" name="txtCapillaryBloodSugar" id="txtCapillaryBloodSugar"  runat="server"  class="TextBoxStyle" onkeypress="return OnlyNumeric(event);" style="width:70px;"  /> mg/dl
                                  </ContentTemplate>
                            </asp:UpdatePanel>
                                
                                </td>
                    </tr>
               
            </table>

        </td>
    </tr>
</table>
    
<table class="table">
          
                
                <tr id="trECG" runat="server" >
                    <td class="lblCaption1"><label for="EcgReport">ECG Report</label></td>
                    <td colspan="4">
                          <asp:UpdatePanel ID="UpdatePanel19" runat="server">
                            <ContentTemplate>
                        <textarea id="txtEcgReport" name="txtEcgReport" class="TextBoxStyle" runat="server" style="width:100%;height:50px;"></textarea>
                                   </ContentTemplate>
                            </asp:UpdatePanel>
                                </td>
                </tr>
                <tr>
                    <td class="lblCaption1"> <label for="Others">Others</label></td>
                    <td colspan="4">
                          <asp:UpdatePanel ID="UpdatePanel20" runat="server">
                            <ContentTemplate>
                        <textarea name="txtOthers" id="txtOthers" runat="server" class="TextBoxStyle"  style="width:100%;height:50px;"></textarea>
                                   </ContentTemplate>
                            </asp:UpdatePanel>
                                </td>
                </tr>
                <tr id="trJaundice" runat="server">
                   
                    <td class="lblCaption1"><label for="JaundiceMeter">Jaundice Meter</label></td>
                    <td>
                         <asp:UpdatePanel ID="UpdatePanel21" runat="server">
                            <ContentTemplate>
                        <input type="text" name="txtJaundiceMeter" id="txtJaundiceMeter"   runat="server" class="TextBoxStyle"  onkeypress="return OnlyNumeric(event);" />
                                   </ContentTemplate>
                            </asp:UpdatePanel>
                                </td>
                    <td class="lblCaption1"> <label for="AbdominalGirth">Abdominal Girth</label></td>
                    <td class="lblCaption1">
                         <asp:UpdatePanel ID="UpdatePanel22" runat="server">
                            <ContentTemplate>
                        <input type="text" name="txtAbdominalGirth" id="txtAbdominalGirth" runat="server" class="TextBoxStyle" onkeypress="return OnlyNumeric(event);"  /> cm
                                   </ContentTemplate>
                            </asp:UpdatePanel>
                                </td>
                    <td class="lblCaption1"><label for="ChestCircumference">Chest Circumference</label></td>
                    <td class="lblCaption1">
                         <asp:UpdatePanel ID="UpdatePanel23" runat="server">
                            <ContentTemplate>
                        <input type="text" name="txtChestCircumference" id="txtChestCircumference"  runat="server" class="TextBoxStyle"  onkeypress="return OnlyNumeric(event);" /> cm
                                   </ContentTemplate>
                            </asp:UpdatePanel>
                                
                                </td>
                    
                </tr>
           
        </table>

<table>
     <tr>
            <td colspan="4">
                 <asp:UpdatePanel ID="UpdatePanel24" runat="server">
                    <ContentTemplate>
                <asp:Button ID="btnSaveVital" runat="server" CssClass="button red small" Width="70px" Visible="false"  OnClick="btnSaveVital_Click" OnClientClick="return SaveVitalVal();" Text="Add" />
                        </ContentTemplate>
                 </asp:UpdatePanel>
            </td>
        </tr>
 </table>
<div id="content-panel">
      <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
    <asp:gridview id="gvVital" runat="server" autogeneratecolumns="False" 
                            enablemodelvalidation="True" width="100%" gridlines="none">
                                 <HeaderStyle CssClass="GridHeader_Blue"  Font-Bold="true"  />
                                <RowStyle CssClass="GridRow" />
                                <Columns>
                              <asp:TemplateField HeaderText="Date">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkDate" runat="server"  OnClick="Select_Click" >
                                            <asp:Label ID="lblSLNo" CssClass="GridRow"   runat="server" Text='<%# Bind("EPV_SL_NO") %>' Visible="false"  ></asp:Label>
                                            <asp:Label ID="lblEPV_ID" CssClass="GridRow"   runat="server" Text='<%# Bind("EPV_ID") %>' Visible="false"  ></asp:Label>
                                            <asp:Label ID="lblDate" CssClass="GridRow"   runat="server" Text='<%# Bind("CreatedDate") %>'  ></asp:Label>
                                          </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="User">
                                    <ItemTemplate>
                                           <asp:LinkButton ID="lnkUserID" runat="server"  OnClick="Select_Click" >
                                            <asp:Label ID="lblUserID" CssClass="GridRow"   runat="server" Text='<%# Bind("CreatedUserName") %>'  ></asp:Label>
                                   </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Weight">
                                    <ItemTemplate>
                                           <asp:LinkButton ID="lnkWeight" runat="server"  OnClick="Select_Click" >
                                            <asp:Label ID="lblWeight" CssClass="GridRow"   runat="server" Text='<%# Bind("EPV_WEIGHT") %>'  ></asp:Label>
                                      &nbsp;Kg
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                  <asp:TemplateField HeaderText="Height">
                                    <ItemTemplate>
                                         
                                            <asp:Label ID="lblHeight" CssClass="GridRow"   runat="server" Text='<%# Bind("EPV_HEIGHT") %>'  ></asp:Label>
                                       &nbsp; cm
                                             
                                    </ItemTemplate>
                                </asp:TemplateField>
                                  
                                 <asp:TemplateField HeaderText="Temp.">
                                    <ItemTemplate>
                                          
                                            <asp:Label ID="lblTemperatureC" CssClass="GridRow"   runat="server" Text='<%# Bind("EPV_TEMPERATURE_C") %>'  ></asp:Label>
                                            <asp:Label ID="lblTemperatureF" CssClass="GridRow"   runat="server" Text='<%# Bind("EPV_TEMPERATURE") %>'  Visible="false" ></asp:Label>
                                       &nbsp; C
                                             
                                    </ItemTemplate>
                                </asp:TemplateField>
                            
                                <asp:TemplateField HeaderText="Pulse">
                                    <ItemTemplate>
                                         
                                            <asp:Label ID="lblPulse" CssClass="GridRow"   runat="server" Text='<%# Bind("EPV_PULSE") %>'  ></asp:Label>
                                         &nbsp;Beats/Min
                                            
                                    </ItemTemplate>
                                </asp:TemplateField>
                               <asp:TemplateField HeaderText="LMP">
                                    <ItemTemplate>
                                        
                                            <asp:Label ID="lblLMP" CssClass="GridRow"   runat="server" Text='<%# Bind("EPV_LMP_DATE") %>'  ></asp:Label>
                                         
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="EDD">
                                    <ItemTemplate>
                                        
                                            <asp:Label ID="lblEDD" CssClass="GridRow"   runat="server" Text='<%# Bind("EPV_EDD_DATE") %>'  ></asp:Label>
                                         
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Respir.">
                                    <ItemTemplate>
                                         
                                            <asp:Label ID="lblRespiration" CssClass="GridRow"   runat="server" Text='<%# Bind("EPV_RESPIRATION") %>'  ></asp:Label>
                                         &nbsp; /m
                                             
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="BP">
                                    <ItemTemplate>
                                         
                                            <asp:Label ID="lblBPSys" CssClass="GridRow"   runat="server" Text='<%# Bind("EPV_BP_SYSTOLIC") %>'  ></asp:Label>
                                           /  
                                            <asp:Label ID="lblBPDiastolic" CssClass="GridRow"   runat="server" Text='<%# Bind("EPV_BP_DIASTOLIC") %>'  ></asp:Label>
                                        &nbsp;  mm/Hg
                                              
                                    </ItemTemplate>
                                </asp:TemplateField>
                             
                                 <asp:TemplateField HeaderText="SPO2">
                                    <ItemTemplate>
                                         
                                            <asp:Label ID="lblSPO2" CssClass="GridRow"   runat="server" Text='<%# Bind("EPV_SPO2") %>'  ></asp:Label>
                                       
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Blood Sugar">
                                    <ItemTemplate>
                                         
                                            <asp:Label ID="lblCapillary" CssClass="GridRow"   runat="server" Text='<%# Bind("EMR_PT_CAPILLARY_BLOOD_SUGAR") %>'  ></asp:Label>
                                      &nbsp;  mg/dL
                                             
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Hemoglobin">
                                    <ItemTemplate>
                                         
                                            <asp:Label ID="lblHemoglobin" CssClass="GridRow"   runat="server" Text='<%# Bind("EPV_HEMOGLOBIN") %>'  ></asp:Label>
                                    
                                    </ItemTemplate>
                                </asp:TemplateField>
                               
                                <asp:TemplateField HeaderText="Weight" Visible="false">
                                    <ItemTemplate>
                                         
                                            <asp:Label ID="lblBirthWeight" CssClass="GridRow"   runat="server" Text='<%# Bind("EPV_BIRTH_WEIGHT") %>'  ></asp:Label>
                                      &nbsp;Kg
                                        
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Others">
                                    <ItemTemplate>
                                         
                                            <asp:Label ID="lblOthers" CssClass="GridRow"   runat="server" Text='<%# Bind("EMR_PT_OTHERS") %>'  ></asp:Label>
                                             
                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                              

    </asp:gridview>
                  </ContentTemplate>
          </asp:UpdatePanel>

    
    <asp:ModalPopupExtender ID="ModalPopupExtender5" runat="server" TargetControlID="lnkSaveTemp"
    PopupControlID="pnlSaveTemplate" BackgroundCssClass="modalBackground" CancelControlID="btnRemClose"
    PopupDragHandleControlID="pnlSaveTemplate">
</asp:ModalPopupExtender>
    <asp:Panel ID="pnlSaveTemplate" runat="server" Height="250px" Width="550px" CssClass="modalPopup" >
    <table cellpadding="0" cellspacing="0" width="100%" style="">
        <tr>
            <td align="right">
                <asp:Button ID="btnRemClose" runat="server" Text=" X " Font-Bold="true" CssClass="button blue small" />
            </td>
        </tr>
        <tr>
            <td class="lblCaption" style="font-weight: bold;">Save Template Dialog
            </td>
        </tr>

        <tr>
            <td class="lblCaption1" style="height: 100px; vertical-align: top;">
                <fieldset style="height: 50px;">
                    <legend class="lblCaption1" style="font-weight: bold;">Save Template</legend>
                    <table>
                        <tr>
                            <td>Template Name : 
                            </td>
                            <td>
                                <input type="text" id="txtTemplateName" name="txtTemplateName" runat="server" class="label" />
                            </td>
                        </tr>
                        <tr>
                            <td>Other Doctors :
                            </td>
                            <td>
                                <input type="checkbox" id="chkOtherDr" runat="server" title="Apply" />
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
        <tr>
            <td style="text-align: right;">
                <asp:Button ID="btnSaveTemp" runat="server" Text="Save Template" OnClick="btnSaveTemp_Click" CssClass="button blue small" />
                <asp:Button ID="btlCloseTemp" runat="server" Text="Close" CssClass="button blue small" />

            </td>
        </tr>

    </table>
</asp:Panel>
    </div>