﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EMR/PatientHeader.Master" AutoEventWireup="true" CodeBehind="Anesthesia.aspx.cs" Inherits=" Mediplus.EMR.Patient.Anesthesia" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
      <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />
    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <script src="../Validation.js" type="text/javascript"></script>

    <style type="text/css">
        .box-header
        {
            border-bottom: 4px solid #f4fbfd;
            height: 70px;
        }

        .box-title
        {
            padding-bottom: 5px;
            border-bottom: 4px solid #92c500;
            float: left;
            font-size: 1.2em;
            color: #2078c0;
        }

        h1, h2, h3, h4, h5, h6
        {
            font-size: 100%;
            font-weight: normal;
            font-family: "Segoe UI", Arial, Helvetica, sans-serif;
        }
    </style>

    <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divwidthProc
        {
            width: 400px !important;
        }

            #divwidthProc div
            {
                width: 400px !important;
            }
    </style>
    <script type="text/javascript">
        function ShowMessage() {
            $("#myMessage").show();
            setTimeout(function () {
                var selectedEffect = 'blind';
                var options = {};
                $("#myMessage").hide();
            }, 2000);
            return true;
        }

        function ShowErrorMessage(vMessage) {

            document.getElementById("divMessage").style.display = 'block';
            document.getElementById("<%=lblMessage.ClientID%>").innerHTML = vMessage;
        }

        function HideErrorMessage() {

            document.getElementById("divMessage").style.display = 'none';
            document.getElementById("<%=lblMessage.ClientID%>").innerHTML = '';
        }
    </script>
    <script type="text/javascript">

        function BindAnesProcedure() {
            var ChiefComp = document.getElementById("<%=txtAnesProcedure.ClientID%>").value + document.getElementById("<%=txtAnesProc.ClientID%>").value + "\n";
            if (document.getElementById("<%=txtAnesProc.ClientID%>").value != "") {
                document.getElementById("<%=txtAnesProcedure.ClientID%>").value = ChiefComp;
            }
            document.getElementById("<%=txtAnesProc.ClientID%>").value = '';
            return false;
        }
        function BindSurgProcedure() {
            var ChiefComp = document.getElementById("<%=txtSurProcedure.ClientID%>").value + document.getElementById("<%=txtSurProc.ClientID%>").value + "\n";
            if (document.getElementById("<%=txtSurProc.ClientID%>").value != "") {
                document.getElementById("<%=txtSurProcedure.ClientID%>").value = ChiefComp;
            }
            document.getElementById("<%=txtSurProc.ClientID%>").value = '';
            return false;
        }


        function BindAssistants() {
            var ChiefComp = document.getElementById("<%=txtAssistantsDtls.ClientID%>").value + document.getElementById("<%=txtAssistants.ClientID%>").value + "\n";
            if (document.getElementById("<%=txtAssistants.ClientID%>").value != "") {
                document.getElementById("<%=txtAssistantsDtls.ClientID%>").value = ChiefComp;
         }
         document.getElementById("<%=txtAssistants.ClientID%>").value = '';
            return false;
        }


        function BindCC(e, CC_ID, TargetID) {
            if (e.keyCode == 13) {

                var ChiefComp = document.getElementById(TargetID).value + document.getElementById(CC_ID).value + "\n";
                document.getElementById(TargetID).value = ChiefComp;
                document.getElementById(CC_ID).value = '';



                return false;
            }
        }


        function BindValue(vValue, TargetID) {

            document.getElementById(TargetID).value = vValue;
            return false;

        }

        function BMI(Wt, Ht) {
            if (Wt == 0 || Ht == 0)
                return 0;
            return ((parseFloat(Wt) / parseFloat(Ht) / parseFloat(Ht)) * 10000).toFixed(2)
        }

        function BindBMI(He_ID, Wt_ID, TargetID) {
            var wt = document.getElementById("ContentPlaceHolder1_TabContainer1_TabPanel1_" + Wt_ID).value;
            var ht = document.getElementById("ContentPlaceHolder1_TabContainer1_TabPanel1_" + He_ID).value;

            var varBmi = BMI(wt, ht)
            document.getElementById("ContentPlaceHolder1_TabContainer1_TabPanel1_" + TargetID).value = varBmi;
            Calculation1(varBmi);
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <input type="hidden" id="hidPermission" runat="server" value="9" />

    <div style="padding-left: 60%; width: 100%;">
        <div id="myMessage" style="display: none; border: groove; height: 30px; width: 200px; background-color: gray; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #fff; padding: 20px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute;">
            Saved Successfully 
                
        </div>
    </div>

    <div style="padding-left: 60%; width: 100%;">
        <div id="divMessage" style="display: none; border: groove; height: 50px; width: 300px; background-color: brown; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #fff; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute;">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="right" valign="top">

                        <input type="button" id="btnMsgClose" class="ButtonStyle" style="background-color: White; color: black; border: none;" value=" X " onclick="HideErrorMessage()" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblMessage" runat="server" CssClass="label"></asp:Label>
                    </td>
                </tr>
            </table>

        </div>
    </div>
    <table width="100%">
        <tr>
            <td style="text-align: left; width: 50%;">
                <h3 class="box-title">Anesthesia Record</h3>
            </td>
            <td style="text-align: right; width: 50%;">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="btnSave" runat="server" CssClass="button orange small" Text="Save Record" OnClick="btnSave_Click" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>

    <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="AjaxTabStyle" Width="100%">
        <asp:TabPanel runat="server" ID="TabPanel4" HeaderText="General" Width="100%">
            <ContentTemplate>
                <table style="width: 100%" cellpadding="3" cellspacing="3">
                    <tr>
                        <td class="lblCaption1" style="width: 200px;">Date 
                        </td>
                        <td colspan="3">
                            <asp:UpdatePanel ID="updatePanel2" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtAnesDate" runat="server" Width="70px" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                    <asp:CalendarExtender ID="Calendarextender1" runat="server"
                                        Enabled="True" TargetControlID="txtAnesDate" Format="dd/MM/yyyy">
                                    </asp:CalendarExtender>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>

                    </tr>
                    <tr>
                        <td class="lblCaption1" style="width: 200px;">Time In
                        </td>
                        <td colspan="3" class="lblCaption1">
                            <asp:UpdatePanel ID="updatePanel6" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList ID="drpTimeInHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="45px"></asp:DropDownList>
                                    <asp:DropDownList ID="drpTimeInMin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="45px"></asp:DropDownList>
                                     <asp:DropDownList ID="drpTimeInAM" runat="server" CssClass="label" style="font-size:10px"   BorderWidth="1px"  BorderColor="#cccccc" Width="45px" >
                                            <asp:ListItem Value="AM">AM</asp:ListItem>
                                            <asp:ListItem Value="PM">PM</asp:ListItem>
                                        </asp:DropDownList>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Surgery Ends &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; 
                                    <asp:DropDownList ID="drpSurgFinHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="45px"></asp:DropDownList>
                                    <asp:DropDownList ID="drpSurgFinMin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="45px"></asp:DropDownList>
                                       <asp:DropDownList ID="drpSurgFinAM" runat="server" CssClass="label" style="font-size:10px"   BorderWidth="1px"  BorderColor="#cccccc" Width="45px" >
                                            <asp:ListItem Value="AM">AM</asp:ListItem>
                                            <asp:ListItem Value="PM">PM</asp:ListItem>
                                        </asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>

                    </tr>
                    <tr>
                        <td class="lblCaption1" style="width: 200px;">Anesthesia Start
                        </td>
                        <td colspan="3" class="lblCaption1">
                            <asp:UpdatePanel ID="updatePanel12" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList ID="drpAnesSTHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="45px"></asp:DropDownList>
                                    <asp:DropDownList ID="drpAnesSTMin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="45px"></asp:DropDownList>
                                     <asp:DropDownList ID="drpAnesSTAM" runat="server" CssClass="label" style="font-size:10px"   BorderWidth="1px"  BorderColor="#cccccc" Width="45px" >
                                            <asp:ListItem Value="AM">AM</asp:ListItem>
                                            <asp:ListItem Value="PM">PM</asp:ListItem>
                                        </asp:DropDownList>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Anesthesia Stopes &nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:DropDownList ID="drpAnesFinHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="45px"></asp:DropDownList>
                                    <asp:DropDownList ID="drpAnesFinMin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="45px"></asp:DropDownList>
                                     <asp:DropDownList ID="drpAnesFinAM" runat="server" CssClass="label" style="font-size:10px"   BorderWidth="1px"  BorderColor="#cccccc" Width="45px" >
                                            <asp:ListItem Value="AM">AM</asp:ListItem>
                                            <asp:ListItem Value="PM">PM</asp:ListItem>
                                        </asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>

                    </tr>
                    <tr>
                        <td class="lblCaption1" style="width: 200px;">Surgery Start
                        </td>
                        <td colspan="3" class="lblCaption1">
                            <asp:UpdatePanel ID="updatePanel4" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList ID="drpSurgSTHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="45px"></asp:DropDownList>
                                    <asp:DropDownList ID="drpSurgSTMin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="45px"></asp:DropDownList>
                                      <asp:DropDownList ID="drpSurgSTAM" runat="server" CssClass="label" style="font-size:10px"   BorderWidth="1px"  BorderColor="#cccccc" Width="45px" >
                                            <asp:ListItem Value="AM">AM</asp:ListItem>
                                            <asp:ListItem Value="PM">PM</asp:ListItem>
                                        </asp:DropDownList>
                                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Patient out of the OR
                                    <asp:DropDownList ID="drpTimeOutHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="45px"></asp:DropDownList>
                                    <asp:DropDownList ID="drpTimeOutMin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="45px"></asp:DropDownList>
                                 <asp:DropDownList ID="drpTimeOutAM" runat="server" CssClass="label" style="font-size:10px"   BorderWidth="1px"  BorderColor="#cccccc" Width="45px" >
                                            <asp:ListItem Value="AM">AM</asp:ListItem>
                                            <asp:ListItem Value="PM">PM</asp:ListItem>
                                        </asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>

                    </tr>
                    <tr>
                        <td class="lblCaption1" valign="top">Anesthetic procedure
                        </td>
                        <td valign="top" colspan="3">
                            <asp:UpdatePanel ID="updatePanel18" runat="server">
                                <ContentTemplate>
                                    <div id="divwidthProc" style="visibility: hidden;"></div>
                                    <asp:TextBox ID="txtAnesProc" runat="server" CssClass="label" Width="60%"></asp:TextBox>
                                    <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="Server" TargetControlID="txtAnesProc" MinimumPrefixLength="1" ServiceMethod="GetProcedure"
                                        CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionInterval="10" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidthPDiag">
                                    </asp:AutoCompleteExtender>
                                    <input type="button" id="btnAnesProcAdd" value="Add" class="button orange small" onclick="BindAnesProcedure()" />
                                    <asp:TextBox ID="txtAnesProcedure" runat="server" Width="70%" Height="100px" CssClass="label" TextMode="MultiLine" Style="resize: none;"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1" valign="top">Surgical procedure
                        </td>
                        <td valign="top" colspan="3">
                            <asp:UpdatePanel ID="updatePanel3" runat="server">
                                <ContentTemplate>
                                    <div id="div1" style="visibility: hidden;"></div>
                                    <asp:TextBox ID="txtSurProc" runat="server" CssClass="label" Width="60%"></asp:TextBox>
                                    <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="Server" TargetControlID="txtSurProc" MinimumPrefixLength="1" ServiceMethod="GetProcedure"
                                        CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionInterval="10" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidthPDiag">
                                    </asp:AutoCompleteExtender>
                                    <input type="button" id="Button1" value="Add" class="button orange small" onclick="BindSurgProcedure()" />
                                    <asp:TextBox ID="txtSurProcedure" runat="server" Width="70%" Height="100px" CssClass="label" TextMode="MultiLine" Style="resize: none;"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1">Surgeon 
                        </td>
                        <td colspan="3">
                            <asp:UpdatePanel runat="server" ID="updatePanel5">
                                <ContentTemplate>
                                    <asp:DropDownList ID="drpSurgeon" runat="server" CssClass="label" Width="250px"></asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1">Surgeon 1
                        </td>
                        <td>
                            <asp:UpdatePanel runat="server" ID="updatePanel22">
                                <ContentTemplate>
                                    <asp:DropDownList ID="drpSurgeon1" runat="server" CssClass="label" Width="250px"></asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td class="lblCaption1">Surgeon 2
                        </td>
                        <td>
                            <asp:UpdatePanel runat="server" ID="updatePanel23">
                                <ContentTemplate>
                                    <asp:DropDownList ID="drpSurgeon2" runat="server" CssClass="label" Width="250px"></asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>

                    <tr>
                        <td class="lblCaption1" valign="top">Assistants
                        </td>
                        <td class="auto-style28" colspan="3">
                            <asp:UpdatePanel runat="server" ID="updatePanel8">
                                <ContentTemplate>
                                    <div id="divDr" style="visibility: hidden;"></div>
                                    <asp:TextBox ID="txtAssistants" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#CCCCCC" Width="60%" MaxLength="50"></asp:TextBox>
                                    <asp:AutoCompleteExtender ID="AutoCompleteExtender4" runat="Server" TargetControlID="txtAssistants" MinimumPrefixLength="1" ServiceMethod="GetDoctorName"
                                        CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divDr">
                                    </asp:AutoCompleteExtender>
                                    <input type="button" id="btnAssistantsAdd" value="Add" class="button orange small" onclick="BindAssistants()" />
                                    <asp:TextBox ID="txtAssistantsDtls" runat="server" Width="70%" Height="100px" CssClass="label" TextMode="MultiLine" Style="resize: none;"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>


                </table>
            </ContentTemplate>
        </asp:TabPanel>
        <asp:TabPanel runat="server" ID="TabPanel1" HeaderText="Anesthesia" Width="100%">
            <ContentTemplate>
                <table style="width: 100%">
                    <tr>
                        <td style="width: 50%; vertical-align: top;">
                            <asp:PlaceHolder ID="plhoPhyAssessmentLeft" runat="server"></asp:PlaceHolder>
                        </td>
                        <td style="width: 50%; vertical-align: top;">
                            <asp:PlaceHolder ID="plhoPhyAssessmentRight" runat="server"></asp:PlaceHolder>
                        </td>
                    </tr>
                </table>

            </ContentTemplate>
        </asp:TabPanel>
        <asp:TabPanel runat="server" ID="TabPanel2" HeaderText="Anesthesia Chart" Width="100%" Visible="false">
            <ContentTemplate>
                <table width="100%">
                    <tr>
                        <td style="width: 50%; vertical-align: top;">
                            <fieldset style="height: 100px;">
                                <table width="100%">
                                    <tr>
                                        <td class="lblCaption1">
                                            <asp:LinkButton ID="lnkDownloadTemplate" runat="server" Text="Download Template" CssClass="lblCaption1" Font-Bold="true" OnClick="lnkDownloadTemplate_Click"></asp:LinkButton>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="lblCaption1">Template File Name&nbsp;&nbsp;:&nbsp;
                                             <asp:Label ID="lblTemplateName" runat="server" CssClass="lblCaption1"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </td>

                        <td style="width: 50%; vertical-align: top;">
                            <fieldset style="height: 100px;">
                                <table width="100%">

                                    <tr>
                                        <td class="lblCaption1">Select File &nbsp;&nbsp;:&nbsp;
                                <asp:FileUpload ID="fileLogo" runat="server" Width="300px" CssClass="ButtonStyle" />
                                            <asp:Button ID="btnUpload" runat="server" Text="Upload" CssClass="button orange small" OnClick="btnUpload_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="lblCaption1">Uploaded File Name&nbsp;&nbsp;:&nbsp;
                            <asp:Label ID="lblAnesChartFileName" runat="server" CssClass="lblCaption1"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 10px;"></td>
                                    </tr>
                                    <tr>
                                        <td class="lblCaption1">
                                            <asp:LinkButton ID="lnkhowAttachment" runat="server" Text="Show Attachment" CssClass="lblCaption1" Font-Bold="true" OnClick="lnkhowAttachment_Click"></asp:LinkButton>

                                        </td>
                                    </tr>

                                </table>
                            </fieldset>
                        </td>

                    </tr>
                </table>




                <iframe id="ImageEditorPDC" runat="server" style="width: 100%; height: 600px; border: none;"></iframe>
            </ContentTemplate>
        </asp:TabPanel>

        <asp:TabPanel runat="server" ID="TabPanel3" HeaderText="Patient Status Score & Signature" Width="100%">
            <ContentTemplate>
                <table width="100%">
                    <tr>
                        <td class="lblCaption1" style="font-weight: bold;">The immediate post anesthesia status ot the patient at the time of the transfer from OR  and score
                        </td>
                    </tr>
                </table>
                <fieldset>
                    <legend class="lblCaption1">Modified Aldre's Scoring
                    </legend>

                    <table width="100%" cellpadding="3" cellspacing="3">
                        <tr>
                            <td class="lblCaption1"></td>
                            <td></td>
                            <td class="lblCaption1">Score
                            </td>
                            <td></td>

                            <td></td>

                            <td class="lblCaption1">Score
                            </td>
                        </tr>
                        <tr>
                            <td class="lblCaption1">B/P 
                            </td>
                            <td>
                                <asp:UpdatePanel runat="server" ID="updatePanel7">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="drpBP" runat="server" CssClass="lblCaption1" Width="250px" AutoPostBack="true" OnSelectedIndexChanged="drpBP_SelectedIndexChanged">
                                            <asp:ListItem Text="---Select---" Value=""></asp:ListItem>
                                            <asp:ListItem Text="±20%" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="˃±20%" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="˃±21%" Value="0"></asp:ListItem>
                                        </asp:DropDownList>

                                    </ContentTemplate>

                                </asp:UpdatePanel>

                            </td>
                            <td>
                                <asp:UpdatePanel runat="server" ID="updatePanel9">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtBPValue" runat="server" Width="50px" ReadOnly="true"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td class="lblCaption1">Motor function
                            </td>
                            <td>
                                <asp:UpdatePanel runat="server" ID="updatePanel10">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="drpMotorFn" runat="server" CssClass="lblCaption1" Width="250px" AutoPostBack="true" OnSelectedIndexChanged="drpMotorFn_SelectedIndexChanged">
                                            <asp:ListItem Text="---Select---" Value=""></asp:ListItem>
                                            <asp:ListItem Text="Moving all limbs" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="Moving only two limbs on command" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Unable to move with painful stimuli" Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td>
                                <asp:UpdatePanel runat="server" ID="updatePanel11">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtMotorFnValue" runat="server" Width="50px" ReadOnly="true"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td class="lblCaption1">SPo2
                            </td>
                            <td>
                                <asp:UpdatePanel runat="server" ID="updatePanel13">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="drpSPo2" runat="server" CssClass="lblCaption1" Width="250px" AutoPostBack="true" OnSelectedIndexChanged="drpSPo2_SelectedIndexChanged">
                                            <asp:ListItem Text="---Select---" Value=""></asp:ListItem>
                                            <asp:ListItem Text="˃95% room air" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="˃95% with O2" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="<95% with O2" Value="0"></asp:ListItem>
                                        </asp:DropDownList>

                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td>
                                <asp:UpdatePanel runat="server" ID="updatePanel14">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtSPo2Value" runat="server" Width="50px" ReadOnly="true"></asp:TextBox>

                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td class="lblCaption1">Pain Score
                            </td>
                            <td>
                                <asp:UpdatePanel runat="server" ID="updatePanel15">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="drpPainScore" runat="server" CssClass="lblCaption1" Width="250px" AutoPostBack="true" OnSelectedIndexChanged="drpPainScore_SelectedIndexChanged">
                                            <asp:ListItem Text="---Select---" Value=""></asp:ListItem>
                                            <asp:ListItem Text="Score 0-3" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="Score 4-7" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Score 8-10" Value="0"></asp:ListItem>
                                        </asp:DropDownList>

                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td>
                                <asp:UpdatePanel runat="server" ID="updatePanel16">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtPainScoreValue" runat="server" Width="50px" ReadOnly="true"></asp:TextBox>

                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td class="lblCaption1">Conciousness
                            </td>
                            <td>
                                <asp:UpdatePanel runat="server" ID="updatePanel17">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="drpConciousness" runat="server" CssClass="lblCaption1" Width="250px" AutoPostBack="true" OnSelectedIndexChanged="drpConciousness_SelectedIndexChanged">
                                            <asp:ListItem Text="---Select---" Value=""></asp:ListItem>
                                            <asp:ListItem Text="Normal" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="Mild sedation" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Deep sedation" Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td>
                                <asp:UpdatePanel runat="server" ID="updatePanel19">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtConciousnessValue" runat="server" Width="50px" ReadOnly="true"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td class="lblCaption1">Respiration
                            </td>
                            <td>
                                <asp:UpdatePanel runat="server" ID="updatePanel20">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="drpRespiration" runat="server" CssClass="lblCaption1" Width="250px" AutoPostBack="true" OnSelectedIndexChanged="drpRespiration_SelectedIndexChanged">
                                            <asp:ListItem Text="---Select---" Value=""></asp:ListItem>
                                            <asp:ListItem Text="Able to have deep breathing/coughfing   " Value="2"></asp:ListItem>
                                            <asp:ListItem Text="Dyspnea corrected on prop up" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Unable to maintain airway even on Tonsill position" Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td>
                                <asp:UpdatePanel runat="server" ID="updatePanel21">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtRespirationValue" runat="server" Width="50px" ReadOnly="true"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </fieldset>

                <table width="100%">

                    <tr>
                        <td style="width: 50%">
                            <fieldset>
                                <legend class="lblCaption1">Anesthesiologist Signature </legend>
                                <table width="100%">
                                    <tr>

                                        <td class="lblCaption1" style="width: 150px">Name  
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="drpSigAnesthesiologist" runat="server" CssClass="label" Width="205px"></asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="lblCaption1" style="width: 150px">Password
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtSigAnesthePass" runat="server" CssClass="label" TextMode="Password" Width="200px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="lblCaption1">Date & Time
                                        </td>
                                        <td>
                                            <asp:UpdatePanel ID="updatePanel54" runat="server">
                                                <ContentTemplate>
                                                    <asp:TextBox ID="txtSigAnestheDate" runat="server" Width="70px" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                    <asp:CalendarExtender ID="CalendarextenderSigAnes" runat="server"
                                                        Enabled="True" TargetControlID="txtSigAnestheDate" Format="dd/MM/yyyy">
                                                    </asp:CalendarExtender>
                                                    <asp:DropDownList ID="drpSigAnestheHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                                                    <asp:DropDownList ID="drpSigAnestheMin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                                                     <asp:DropDownList ID="drpSigAnestheAM" runat="server" CssClass="label" style="font-size:10px"   BorderWidth="1px"  BorderColor="#cccccc" Width="45px" >
                                            <asp:ListItem Value="AM">AM</asp:ListItem>
                                            <asp:ListItem Value="PM">PM</asp:ListItem>
                                        </asp:DropDownList>

                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>

                                </table>


                            </fieldset>
                        </td>
                        <td style="width: 50%"></td>
                    </tr>
                </table>

            </ContentTemplate>
        </asp:TabPanel>
    </asp:TabContainer>


</asp:Content>
