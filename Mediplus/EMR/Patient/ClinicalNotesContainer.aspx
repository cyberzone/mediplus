﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="ClinicalNotesContainer.aspx.cs" Inherits=" Mediplus.EMR.Patient.ClinicalNotesContainer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

 
     <script src="../../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

     <link href="../../Styles/Style.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Datagrid.css" rel="stylesheet" type="text/css" />

     
     
       
     <script src="../../Scripts/AccordionScript.js"></script>
     <link rel="stylesheet" href="../../Styles/Accordionstyles.css">
    <style type="text/css">
        .box-header
        {
            border-bottom: 4px solid #f4fbfd;
            height: 70px;
        }

        .box-title
        {
            padding-bottom: 5px;
            border-bottom: 4px solid #92c500;
            float: left;
            font-size: 1.2em;
            color: #2078c0;
        }

        h1, h2, h3, h4, h5, h6
        {
            font-size: 100%;
            font-weight: normal;
            font-family: "Segoe UI", Arial, Helvetica, sans-serif;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Clinical Notes</h3>
        </div>
    </div>

    <br />
  <br />
    <iframe id="ifrm" src="ClinicalNotes.aspx" height="900px" width="100%" ></iframe>
</asp:Content>
