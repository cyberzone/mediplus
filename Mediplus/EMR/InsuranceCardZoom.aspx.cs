﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Mediplus.EMR
{
    public partial class InsuranceCardZoom : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                imgFront.ImageUrl = "DisplayCardImage.aspx";
                imgBack.ImageUrl = "DisplayCardImageBack.aspx";
            }
        }
    }
}