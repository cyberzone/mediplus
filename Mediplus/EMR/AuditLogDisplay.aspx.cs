﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_BAL;


namespace Mediplus.EMR
{
    public partial class AuditLogDisplay : System.Web.UI.Page
    {
        DataSet ds = new DataSet();
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine("AuditLogDisplay " + strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }


        void BindGrid()
        {
            string Criteria = " 1=1 ";
            //if (txtSearch.Text.Trim() != "")
            //{

            //    Criteria += " AND ( HPM_PT_FNAME like '%" + txtSearch.Text.Trim() + "%' OR  HPM_PT_MNAME like '%" + txtSearch.Text.Trim() + "%' " +
            //    "  OR   HPM_PT_LNAME like '%" + txtSearch.Text.Trim() + "%' OR   HPM_PT_ID like '%" + txtSearch.Text.Trim() + "%' " +
            //    "  OR   HPM_IQAMA_NO like '%" + txtSearch.Text.Trim() + "%' OR   HPM_MOBILE like '%" + txtSearch.Text.Trim() + "%' " +
            //    "  OR   HPM_PHONE1 like '%" + txtSearch.Text.Trim() + "%' OR   HPM_PHONE2 like '%" + txtSearch.Text.Trim() + "%' " +
            //    "  OR   HPM_PHONE3 like '%" + txtSearch.Text.Trim() + "%' OR   HPM_POBOX like '%" + txtSearch.Text.Trim() + "%' " +
            //    "  OR   HPM_NATIONALITY like '%" + txtSearch.Text.Trim() + "%' OR  HPM_POBOX like '%" + txtSearch.Text.Trim() + "%' " +
            //    "  OR   HPM_POLICY_NO like '%" + txtSearch.Text.Trim() + "%' OR   HPM_POBOX like '%" + txtSearch.Text.Trim() + "%' " +
            //    "  OR   HPM_BILL_CODE like '%" + txtSearch.Text.Trim() + "%' OR   HPM_INS_COMP_ID like '%" + txtSearch.Text.Trim() + "%' )";

            //}



            Criteria += " AND EAL_SCREEN_ID='" + ViewState["ScreenID"] + "'";

            if (Convert.ToString(ViewState["DataDisplay"]).ToUpper() == "ByEMR_ID".ToUpper())
            {
                Criteria += " AND EAL_EMR_ID='" + Session["EMR_ID"] + "'";
            }

            CommonBAL com = new CommonBAL();
            ds = com.EMRAuditLogGet(Criteria);
            //SORTING CODING - START
            DataView DV = new DataView();
            DV.Table = ds.Tables[0];
            DV.Sort = Convert.ToString(ViewState["SortOrder"]);
            //SORTING CODING - END


            gvAuditLog.Visible = false;
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvAuditLog.Visible = true;
                gvAuditLog.DataSource = DV;
                gvAuditLog.DataBind();


            }
            else
            {


            }


            ds.Clear();
            ds.Dispose();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            try
            {

                if (!IsPostBack)
                {
                    ViewState["ScreenID"] = Request.QueryString["ScreenID"].ToString();
                    ViewState["DataDisplay"] = Request.QueryString["DataDisplay"].ToString();

                    BindGrid();

                }

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void gvPTList_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                String strSort = Convert.ToString(ViewState["SortOrder"]);

                if (strSort == e.SortExpression + " Asc")
                {
                    strSort = e.SortExpression + " Desc";
                }
                else
                {
                    strSort = e.SortExpression + " Asc";
                }

                ViewState["SortOrder"] = strSort;
                BindGrid();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "gvPTList_Sorting");
                TextFileWriting(ex.Message.ToString());
            }


        }
    }
}