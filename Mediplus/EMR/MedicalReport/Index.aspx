﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="Mediplus.EMR.MedicalReport.Index" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<%@ Register Src="~/EMR/MedicalReport/MedicalReport.ascx" TagPrefix="UC1" TagName="MedicalReport" %>
<%@ Register Src="~/EMR/MedicalReport/Leave.ascx" TagPrefix="UC1" TagName="Leave" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="../Styles/style.css" rel="stylesheet" type="text/css" />

    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />

    <link href="../Styles/MenuStyle.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .box-header
        {
            border-bottom: 4px solid #f4fbfd;
            height: 70px;
        }

        .box-title
        {
            padding-bottom: 5px;
            border-bottom: 4px solid #92c500;
            float: left;
            font-size: 1.5em;
            color: #2078c0;
        }

        h1, h2, h3, h4, h5, h6
        {
            font-size: 100%;
            font-weight: normal;
            font-family: "Segoe UI", Arial, Helvetica, sans-serif;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ToolkitScriptManager>
        <div>
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Reports </h3>
                </div>
            </div>
            <br />
            <br />
            <br />
            <div style="padding: 5px; border: thin; border-color: #777; border-style: groove;">
                <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="AjaxTabStyle" Width="100%">
                    <asp:TabPanel runat="server" ID="TabPanelGeneral" HeaderText="Medical Report" Width="100%" >
                        <ContentTemplate>
                            <UC1:MedicalReport ID="MedicalReport" runat="server" />
                        </ContentTemplate>
                    </asp:TabPanel>
                    <asp:TabPanel runat="server" ID="TabPanel1" HeaderText="Leave" Width="100%">
                        <ContentTemplate>
                            <UC1:Leave ID="Leave" runat="server" />
                        </ContentTemplate>
                    </asp:TabPanel>

                </asp:TabContainer>
            </div>
        </div>
    </form>
</body>
</html>
