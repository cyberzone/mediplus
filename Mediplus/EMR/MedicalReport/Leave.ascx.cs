﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_BAL;


namespace Mediplus.EMR.MedicalReport
{
    public partial class Leave : System.Web.UI.UserControl
    {
        # region Variable Declaration

        CommonBAL objCom = new CommonBAL();
        EMR_PTMasterBAL objEMR_PTMast = new EMR_PTMasterBAL();


        #endregion

        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void PatientDataBind()
        {

            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HPM_PT_ID = '" + txtFileNo.Text + "'";
            Criteria += " AND HPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            ds = objCom.PatientMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                txtFileNo.Text = ds.Tables[0].Rows[0]["HPM_PT_ID"].ToString();
                txtFullName.Text = ds.Tables[0].Rows[0]["FullName"].ToString();


            }

        }


        void BindLeaveReportGrid()
        {
            string Criteria = " 1=1 ";


            string strStartDate = txtFromDate1.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            if (txtFromDate1.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),EPLR_CREATED_DATE,101),101) >= '" + strForStartDate + "'";
            }



            string strTotDate = txtToDate1.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }


            if (txtToDate1.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),EPLR_CREATED_DATE,101),101) <= '" + strForToDate + "'";
            }

            if (txtSrcFileNo1.Text.Trim() != "")
            {
                Criteria += " AND EPLR_PT_ID like '%" + txtSrcFileNo1.Text.Trim() + "%'";

            }

            if (txtSrcName1.Text.Trim() != "")
            {
                Criteria += " AND HPM_PT_NAME LIKE '%" + txtSrcName1.Text.Trim() + "%'";
            }

            if (txtSrcMobile1.Text.Trim() != "")
            {
                Criteria += " AND HPM_MOBILE= '" + txtSrcMobile1.Text.Trim() + "'";
            }



            DataSet DS = new DataSet();
            EMR_PTMedicalReport objMR = new EMR_PTMedicalReport();
            DS = objMR.PTLeaveReportGet(Criteria);

            gvLeaveReport.Visible = false;
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvLeaveReport.Visible = true;
                gvLeaveReport.DataSource = DS;
                gvLeaveReport.DataBind();

            }
            else
            {
                gvLeaveReport.DataBind();
            }


        }

        void Clear()
        {
            radLeaveType.SelectedIndex = 0;
            txtFileNo.Text = "";
            txtFullName.Text = "";
            txtDiagnosis.Text = "";
            txtTreatmentDesc.Text = "";
            DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());

            txtFromDate.Text = strFromDate.ToString("dd/MM/yyyy");
            txtToDate.Text = strFromDate.ToString("dd/MM/yyyy");

            ViewState["EPLR_ID"] = "";


        }

        #endregion


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["EPLR_ID"] = "";
                if (Convert.ToString(Session["User_ID"]).ToLower() != "admin")
                {
                    //SetPermission();
                }
                try
                {
                    DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());

                    txtFromDate.Text = strFromDate.ToString("dd/MM/yyyy");
                    txtToDate.Text = strFromDate.ToString("dd/MM/yyyy");

                    txtFromDate1.Text = strFromDate.AddDays(-9).ToString("dd/MM/yyyy");
                    txtToDate1.Text = strFromDate.ToString("dd/MM/yyyy");



                    BindLeaveReportGrid();

                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      Leave.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }

            }
        }

        protected void txtFileNo_TextChanged(object sender, EventArgs e)
        {
            try
            {

                PatientDataBind();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      HomeCareRegistration.txtFileNo_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

            try
            {


                EMR_PTMedicalReport objMR = new EMR_PTMedicalReport();
                objMR.BranchID = Convert.ToString(Session["Branch_ID"]);
                objMR.PatientID = txtFileNo.Text.Trim();
                objMR.EPLR_ID = Convert.ToString(ViewState["EPLR_ID"]);
                objMR.leaveType = radLeaveType.SelectedValue;
                objMR.Diagnosis = txtDiagnosis.Text.Trim().Replace("'", "''");
                objMR.Treatment = txtTreatmentDesc.Text.Trim().Replace("'", "''");
                objMR.FromDate = txtFromDate.Text.Trim();
                objMR.ToDate = txtToDate.Text.Trim();

                objMR.UserID = Convert.ToString(Session["User_ID"]);
                objMR.PTLeaveReportAdd();
                BindLeaveReportGrid();

                Clear();
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowMessage()", true);
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Leave.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnMRRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                BindLeaveReportGrid();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Leave.btnMRRefresh_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void Edit_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btnDel = new LinkButton();
                btnDel = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnDel.Parent.Parent;




                Label lblEPLR_ID = (Label)gvScanCard.Cells[0].FindControl("lblEPLR_ID");
                Label lblFileNo = (Label)gvScanCard.Cells[0].FindControl("lblFileNo");
                Label lblDate = (Label)gvScanCard.Cells[0].FindControl("lblDate");
                Label lblPTName = (Label)gvScanCard.Cells[0].FindControl("lblPTName");
                Label lblFromDate = (Label)gvScanCard.Cells[0].FindControl("lblFromDate");
                Label lblToDat = (Label)gvScanCard.Cells[0].FindControl("lblToDat");
                Label lblDiag = (Label)gvScanCard.Cells[0].FindControl("lblDiag");
                Label lblTreatment = (Label)gvScanCard.Cells[0].FindControl("lblTreatment");

                Label lblLeaveType = (Label)gvScanCard.Cells[0].FindControl("lblLeaveType");

                ViewState["EPLR_ID"] = lblEPLR_ID.Text;
                txtFileNo.Text = lblFileNo.Text.Trim();
                txtFullName.Text = lblPTName.Text.Trim();
                txtDiagnosis.Text = lblDiag.Text.Trim();
                txtTreatmentDesc.Text = lblTreatment.Text.Trim();

                txtFromDate.Text = lblFromDate.Text.Trim();
                txtToDate.Text = lblToDat.Text.Trim();

                radLeaveType.SelectedValue = lblLeaveType.Text.Trim();

                TabContainer1.ActiveTab = TabPanelGeneral;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Leave.gvbtnPrint_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void gvbtnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btnDel = new LinkButton();
                btnDel = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnDel.Parent.Parent;




                Label lblEPLR_ID = (Label)gvScanCard.Cells[0].FindControl("lblEPLR_ID");
                Label lblFileNo = (Label)gvScanCard.Cells[0].FindControl("lblFileNo");
                string strRptPath = "";
                if (GlobalValues.FileDescription.ToUpper() == "MAMPILLY" || GlobalValues.FileDescription.ToUpper() == "ALTAIF")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LeaveReportrpt", "ShowLeavePrintPDF('" + lblFileNo.Text + "','" + Convert.ToString(Session["Branch_ID"]) + "','" + lblEPLR_ID.Text + "','" + drpReportType.SelectedValue + "');", true);
                }
                else
                {
                    strRptPath = "../WebReports/LeaveReport.aspx";
                    string rptcall = @strRptPath + "?EPLR_ID=" + lblEPLR_ID.Text + "&FileNo=" + lblFileNo.Text + "&DR_ID=" + Convert.ToString(Session["User_Code"]);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "LeaveReport", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1');", true);
                }


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Leave.gvbtnPrint_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void gvLeaveReport_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvLeaveReport.PageIndex = e.NewPageIndex;
                BindLeaveReportGrid();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Leave.gvMedicalReport_PageIndexChanging");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                Clear();


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Leave.btnClear_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }
    }
}