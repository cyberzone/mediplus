﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_BAL;

namespace Mediplus.EMR.MedicalReport
{
    public partial class MedicalReport : System.Web.UI.UserControl
    {
        # region Variable Declaration

        CommonBAL objCom = new CommonBAL();
        EMR_PTMasterBAL objEMR_PTMast = new EMR_PTMasterBAL();


        #endregion

        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }


        void BindEMRGrid()
        {
            string Criteria = " 1=1 ";


            string strStartDate = txtFromDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            if (txtFromDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),HPV_DATE,101),101) >= '" + strForStartDate + "'";
            }



            string strTotDate = txtToDate.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }


            if (txtToDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),HPV_DATE,101),101) <= '" + strForToDate + "'";
            }

            if (txtSrcFileNo.Text.Trim() != "")
            {
                Criteria += " AND HPV_PT_ID like '%" + txtSrcFileNo.Text.Trim() + "%'";

            }

            if (txtSrcName.Text.Trim() != "")
            {
                Criteria += " AND HPV_PT_NAME LIKE '%" + txtSrcName.Text.Trim() + "%'";
            }

            if (txtSrcMobile.Text.Trim() != "")
            {
                Criteria += " AND HPV_MOBILE= '" + txtSrcMobile.Text.Trim() + "'";
            }



            if (Convert.ToString(Session["User_Category"]).ToLower() != "nurse" && Convert.ToString(Session["User_Category"]).ToLower() != "others")
            {
                Criteria += " AND HPV_DR_ID='" + Convert.ToString(Session["User_Code"]) + "'";
            }

            if (Convert.ToString(Session["User_Category"]).ToUpper() == "NURSE")
            {
                Criteria += " AND HPV_EMR_ID IN (SELECT DISTINCT  HPV_EMR_ID  FROM HMS_PATIENT_VISIT INNER JOIN EMR_PT_INSTRUCTION ON CAST(HPV_EMR_ID AS VARCHAR)  =CAST(EPC_ID AS VARCHAR) WHERE " + Criteria + ")";
            }


            DataSet DS = new DataSet();

            //if (Convert.ToString(Session["User_Category"]).ToUpper() == "NURSE")
            //{
            //    DS = objCom.PatientVisitNursingOrderGet(Criteria);
            //}
            //else
            //{
            DS = objCom.PatientVisitGet(Criteria);
            //  }
            //SORTING CODING - START
            DataView DV = new DataView();
            DV.Table = DS.Tables[0];
            DV.Sort = Convert.ToString(ViewState["SortOrder"]);
            //SORTING CODING - END



            gvGridView.Visible = false;
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvGridView.Visible = true;
                gvGridView.DataSource = DV;
                gvGridView.DataBind();

            }

            if (Convert.ToString(Session["User_Category"]).ToUpper() == "NURSE" || Convert.ToString(Session["User_Category"]).ToUpper() == "OTHERS")
            {
                gvGridView.Columns[10].Visible = true;
            }


        }

        void BindMedicalReportGrid()
        {
            string Criteria = " 1=1 ";


            string strStartDate = txtFromDate1.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            if (txtFromDate1.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),EPMR_CREATED_DATE,101),101) >= '" + strForStartDate + "'";
            }



            string strTotDate = txtToDate1.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }


            if (txtToDate1.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),EPMR_CREATED_DATE,101),101) <= '" + strForToDate + "'";
            }

            if (txtSrcFileNo1.Text.Trim() != "")
            {
                Criteria += " AND EPMR_PT_ID like '%" + txtSrcFileNo1.Text.Trim() + "%'";

            }

            if (txtSrcName1.Text.Trim() != "")
            {
                Criteria += " AND HPM_PT_NAME LIKE '%" + txtSrcName1.Text.Trim() + "%'";
            }

            if (txtSrcMobile1.Text.Trim() != "")
            {
                Criteria += " AND HPM_MOBILE= '" + txtSrcMobile1.Text.Trim() + "'";
            }



            DataSet DS = new DataSet();
            EMR_PTMedicalReport objMR = new EMR_PTMedicalReport();
            DS = objMR.PTMedicalReportGet(Criteria);

            gvMedicalReport.Visible = false;
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvMedicalReport.Visible = true;
                gvMedicalReport.DataSource = DS;
                gvMedicalReport.DataBind();

            }
            else
            {
                gvMedicalReport.DataBind();
            }


        }

        void Clear()
        {
            txtInvstRemarks.Text = "";
            txtRecommendation.Text = "";
            ViewState["EMR_ID"] = "";
            ViewState["EMR_PT_ID"] = "";


            if (Convert.ToString(ViewState["SelectIndex"]) != "")
            {
                if (gvGridView.Rows.Count > 0)
                    gvGridView.Rows[Convert.ToInt32(ViewState["SelectIndex"])].BackColor = System.Drawing.Color.White;
            }
            ViewState["SelectIndex"] = "";


        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Convert.ToString(Session["User_ID"]).ToLower() != "admin")
                {
                    //SetPermission();
                }
                try
                {
                    DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                    txtFromDate.Text = strFromDate.ToString("dd/MM/yyyy");
                    txtToDate.Text = strFromDate.ToString("dd/MM/yyyy");

                    txtFromDate1.Text = strFromDate.AddDays(-9).ToString("dd/MM/yyyy");
                    txtToDate1.Text = strFromDate.ToString("dd/MM/yyyy");


                    BindEMRGrid();
                    BindMedicalReportGrid();

                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      MedicalReport.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }

            }
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                BindEMRGrid();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      MedicalReport.btnRefresh_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void gvGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvGridView.PageIndex = e.NewPageIndex;
                BindEMRGrid();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      MedicalReport.gvGridView_PageIndexChanging");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void gvGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                String strSort = Convert.ToString(ViewState["SortOrder"]);

                if (strSort == e.SortExpression + " Asc")
                {
                    strSort = e.SortExpression + " Desc";
                }
                else
                {
                    strSort = e.SortExpression + " Asc";
                }

                ViewState["SortOrder"] = strSort;
                BindEMRGrid();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      MedicalReport.gvGridView_Sorting");
                TextFileWriting(ex.Message.ToString());
            }


        }


        protected void Edit_Click(object sender, EventArgs e)
        {

            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;

            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

            ViewState["SelectIndex"] = gvScanCard.RowIndex;
            gvGridView.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");


            Label lblPatientId = (Label)gvScanCard.Cells[0].FindControl("lblPatientId");
            Label lblSeqNo = (Label)gvScanCard.Cells[0].FindControl("lblSeqNo");
            Label lblEMR_ID = (Label)gvScanCard.Cells[0].FindControl("lblEMR_ID");
            Label lblDrID = (Label)gvScanCard.Cells[0].FindControl("lblDrID");
            Label lblDeptName = (Label)gvScanCard.Cells[0].FindControl("lblDeptName");
            Label lblVisitType = (Label)gvScanCard.Cells[0].FindControl("lblVisitType");
            Label lblCompID = (Label)gvScanCard.Cells[0].FindControl("lblCompID");


            ViewState["EMR_ID"] = lblEMR_ID.Text;
            ViewState["EMR_PT_ID"] = lblPatientId.Text;



        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToString(ViewState["SelectIndex"]) == "")
                {
                    /// lblStatus.Text = " Please select one data from above list";
                    //lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                EMR_PTMedicalReport objMR = new EMR_PTMedicalReport();
                objMR.BranchID = Convert.ToString(Session["Branch_ID"]);
                objMR.PatientID = Convert.ToString(ViewState["EMR_PT_ID"]);
                objMR.Remarks = txtInvstRemarks.Text.Trim().Replace("'", "''");
                objMR.Recommend = txtRecommendation.Text.Trim().Replace("'", "''");
                objMR.EMR_ID = Convert.ToString(ViewState["EMR_ID"]);
                objMR.UserID = Convert.ToString(Session["User_ID"]);
                objMR.PTMedicalReportAdd();
                BindMedicalReportGrid();

                Clear();
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowMessage()", true);
            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      MedicalReport.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void btnMRRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                BindMedicalReportGrid();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      MedicalReport.btnMRRefresh_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void gvbtnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btnDel = new LinkButton();
                btnDel = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnDel.Parent.Parent;




                Label lblEMRID = (Label)gvScanCard.Cells[0].FindControl("lblEMRID");
                Label lblFileNo = (Label)gvScanCard.Cells[0].FindControl("lblFileNo");
                string strRptPath = "";

                strRptPath = "../WebReports/MedicalReport.aspx";
                string rptcall = @strRptPath + "?EMR_ID=" + lblEMRID.Text + "&EMR_PT_ID=" + lblFileNo.Text + "&DR_ID=" + Convert.ToString(Session["User_Code"]);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ClinucalSum", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1');", true);



            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      MedicalReport.gvbtnPrint_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void gvMedicalReport_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvMedicalReport.PageIndex = e.NewPageIndex;
                BindMedicalReportGrid();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      MedicalReport.gvMedicalReport_PageIndexChanging");
                TextFileWriting(ex.Message.ToString());
            }
        }

    }
}