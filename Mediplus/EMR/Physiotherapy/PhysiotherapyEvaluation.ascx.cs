﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_BAL;

namespace Mediplus.EMR.Physiotherapy
{
    public partial class PhysiotherapyEvaluation : System.Web.UI.UserControl
    {
        static string strSessionDeptId;

        CommonBAL objCom = new CommonBAL();
        EMR_Physiotherapy objAnes = new EMR_Physiotherapy();
        DataSet DS = new DataSet();
        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMR_Log.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine("Physiotherapy." + strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        DataTable BindHour()
        {
            DataSet DS = new DataSet();
            DS = objCom.HoursGet("true");

            return DS.Tables[0];
        }

        DataTable BindMinutes()
        {
            DataSet DS = new DataSet();
            DS = objCom.MinutesGet("5");

            return DS.Tables[0];
        }

        DataSet GetSegmentData()
        {
            objCom = new CommonBAL();
            string Criteria = " 1=1 AND EPS_ACTIVE=1 AND EPS_BRANCH='" + Convert.ToString(Session["Branch_ID"]) + "'";// AND EPS_TYPE='" + strType + "'";
            // Criteria += " AND  EPS_POSITION='" + Position + "'";

            DataSet DS = new DataSet();

            DS = objCom.fnGetFieldValue(" * ", "EMR_PHYSIOTHERAPY_SEGMENT", Criteria, "EPS_ORDER");



            return DS;

        }

        public DataSet GetSegmentMaster(string SegmentType)
        {
            objCom = new CommonBAL();
            string Criteria = " 1=1 AND EPSM_STATUS=1 ";
            Criteria += " and EPSM_TYPE='" + SegmentType + "'";



            DataSet DS = new DataSet();

            DS = objCom.fnGetFieldValue(" * ", "EMR_PHYSIOTHERAPY_SEGMENT_MASTER", Criteria, "EPSM_ORDER");



            return DS;




        }

        public void SaveSegmentDtls(string strType, string FieldID, string FieName, string Selected, string Comment)
        {
            objAnes = new EMR_Physiotherapy();

            objAnes.BranchID = Convert.ToString(Session["Branch_ID"]);
            objAnes.EMRID = Convert.ToString(Session["EMR_ID"]);
            objAnes.PTID = Convert.ToString(Session["EMR_PT_ID"]);
            objAnes.EPSD_TYPE = strType;
            objAnes.EPSD_FIELD_ID = FieldID;
            objAnes.EPSD_FIELD_NAME = FieName;
            objAnes.EPSD_VALUE = Selected;
            objAnes.EPSD_COMMENT = Comment;
            objAnes.EPSD_ENTRY_FROM = "IP";
            objAnes.PhysiotherapySegmentDtlsAdd();
        }

        public void SavePhyEvalution()
        {
            string Position = "";

            DataSet DS = new DataSet();

            DS = GetSegmentData();

            foreach (DataRow DR in DS.Tables[0].Rows)
            {
                Position = Convert.ToString(DR["EPS_POSITION"]);

                objCom = new CommonBAL();

                string CriteriaDel = " 1=1 AND EPSD_ENTRY_FROM='IP' AND EPSD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND EPSD_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
                CriteriaDel += " AND EPSD_TYPE='" + Convert.ToString(DR["EPS_CODE"]).Trim() + "'";
                objCom.fnDeleteTableData("EMR_PHYSIOTHERAPY_SEGMENT_DTLS", CriteriaDel);


                Int32 i = 1;
                DataSet DS1 = new DataSet();

                DS1 = GetSegmentMaster(Convert.ToString(DR["EPS_CODE"]).Trim());



                foreach (DataRow DR1 in DS1.Tables[0].Rows)
                {
                    string strValueYes = "", _Comment = "";
                    if (Convert.ToString(DR1["EPSM_CONTROL_TYPE"]).ToUpper() == "CheckBox".ToUpper())
                    {
                        CheckBox chk1 = new CheckBox();
                        //  if (strType == "EMR_PRE_ANESTHESIA_HISTORY")
                        // {
                        if (Position == "LEFT")
                        {
                            chk1 = (CheckBox)plhoPhyAssessmentLeft.FindControl("chk" + Convert.ToString(DR1["EPSM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["EPSM_FIELD_ID"]).Trim());

                        }
                        else if (Position == "MIDDLE")
                        {

                            chk1 = (CheckBox)plhoPhyAssessmentMiddle.FindControl("chk" + Convert.ToString(DR1["EPSM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["EPSM_FIELD_ID"]).Trim());

                        }
                        else if (Position == "RIGHT")
                        {
                            chk1 = (CheckBox)plhoPhyAssessmentRight.FindControl("chk" + Convert.ToString(DR1["EPSM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["EPSM_FIELD_ID"]).Trim());

                        }






                        if (chk1 != null)
                        {
                            if (chk1.Checked == true)
                            {
                                strValueYes = "Y";
                            }
                        }

                    }
                    if (Convert.ToString(DR1["EPSM_CONTROL_TYPE"]).ToUpper() == "TextBox".ToUpper() || Convert.ToString(DR1["EPSM_CONTROL_TYPE"]).ToUpper() == "AutoTextBox".ToUpper() || Convert.ToString(DR1["EPSM_CONTROL_TYPE"]).ToUpper() == "Date".ToUpper() || Convert.ToString(DR1["EPSM_CONTROL_TYPE"]).ToUpper() == "DateTime".ToUpper())
                    {
                        TextBox txt = new TextBox();
                        //if (strType == "EMR_PRE_ANESTHESIA_HISTORY")
                        //{
                        if (Position == "LEFT")
                        {
                            txt = (TextBox)plhoPhyAssessmentLeft.FindControl("txt" + Convert.ToString(DR1["EPSM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["EPSM_FIELD_ID"]).Trim());
                        }
                        else if (Position == "MIDDLE")
                        {
                            txt = (TextBox)plhoPhyAssessmentMiddle.FindControl("txt" + Convert.ToString(DR1["EPSM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["EPSM_FIELD_ID"]).Trim());

                        }
                        else if (Position == "RIGHT")
                        {
                            txt = (TextBox)plhoPhyAssessmentRight.FindControl("txt" + Convert.ToString(DR1["EPSM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["EPSM_FIELD_ID"]).Trim());

                        }
                        DropDownList drpHour = new DropDownList();
                        DropDownList drpMin = new DropDownList();

                        if (Convert.ToString(DR1["EPSM_CONTROL_TYPE"]).ToUpper() == "DateTime".ToUpper())
                        {
                            if (Position == "LEFT")
                            {
                                drpHour = (DropDownList)plhoPhyAssessmentLeft.FindControl("drpHour" + Convert.ToString(DR1["EPSM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["EPSM_FIELD_ID"]).Trim());
                                drpMin = (DropDownList)plhoPhyAssessmentLeft.FindControl("drpMin" + Convert.ToString(DR1["EPSM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["EPSM_FIELD_ID"]).Trim());
                            }

                            else if (Position == "MIDDLE")
                            {
                                drpHour = (DropDownList)plhoPhyAssessmentMiddle.FindControl("drpHour" + Convert.ToString(DR1["EPSM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["EPSM_FIELD_ID"]).Trim());
                                drpMin = (DropDownList)plhoPhyAssessmentMiddle.FindControl("drpMin" + Convert.ToString(DR1["EPSM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["EPSM_FIELD_ID"]).Trim());
                            }

                            else if (Position == "RIGHT")
                            {
                                drpHour = (DropDownList)plhoPhyAssessmentRight.FindControl("drpHour" + Convert.ToString(DR1["EPSM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["EPSM_FIELD_ID"]).Trim());
                                drpMin = (DropDownList)plhoPhyAssessmentRight.FindControl("drpMin" + Convert.ToString(DR1["EPSM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["EPSM_FIELD_ID"]).Trim());

                            }

                        }


                        if (txt.Text != "")
                        {
                            strValueYes = "Y";
                            _Comment = txt.Text;
                        }

                        if (Convert.ToString(DR1["EPSM_CONTROL_TYPE"]).ToUpper() == "DateTime".ToUpper())
                        {
                            _Comment = txt.Text.Trim() + " " + drpHour.SelectedValue + ":" + drpMin.SelectedValue + ":00";
                        }
                    }




                    if (Convert.ToString(DR1["EPSM_CONTROL_TYPE"]).ToUpper() == "RadioButtonList".ToUpper())
                    {

                        RadioButtonList RadList = new RadioButtonList();


                        // if (strType == "EMR_PRE_ANESTHESIA_HISTORY")
                        // {
                        if (Position == "LEFT")
                        {

                            RadList = (RadioButtonList)plhoPhyAssessmentLeft.FindControl("radl" + Convert.ToString(DR1["EPSM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["EPSM_FIELD_ID"]).Trim());

                        }
                        else if (Position == "MIDDLE")
                        {
                            RadList = (RadioButtonList)plhoPhyAssessmentMiddle.FindControl("radl" + Convert.ToString(DR1["EPSM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["EPSM_FIELD_ID"]).Trim());

                        }
                        else if (Position == "RIGHT")
                        {
                            RadList = (RadioButtonList)plhoPhyAssessmentRight.FindControl("radl" + Convert.ToString(DR1["EPSM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["EPSM_FIELD_ID"]).Trim());

                        }
                        // }






                        if (RadList != null)
                        {
                            if (RadList.SelectedValue != "")
                            {
                                strValueYes = "Y";
                                _Comment = RadList.SelectedValue;
                            }
                        }



                    }





                    if (Convert.ToString(DR1["EPSM_CONTROL_TYPE"]).ToUpper() == "DropDownList".ToUpper())
                    {

                        DropDownList drpList = new DropDownList();


                        // if (strType == "EMR_PRE_ANESTHESIA_HISTORY")
                        // {
                        if (Position == "LEFT")
                        {

                            drpList = (DropDownList)plhoPhyAssessmentLeft.FindControl("drp" + Convert.ToString(DR1["EPSM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["EPSM_FIELD_ID"]).Trim());

                        }
                        else if (Position == "MIDDLE")
                        {
                            drpList = (DropDownList)plhoPhyAssessmentMiddle.FindControl("drp" + Convert.ToString(DR1["EPSM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["EPSM_FIELD_ID"]).Trim());

                        }
                        else if (Position == "RIGHT")
                        {
                            drpList = (DropDownList)plhoPhyAssessmentRight.FindControl("drp" + Convert.ToString(DR1["EPSM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["EPSM_FIELD_ID"]).Trim());

                        }

                        // }






                        if (drpList != null)
                        {
                            if (drpList.SelectedValue != "" && drpList.SelectedValue != "---Select---")
                            {
                                strValueYes = "Y";
                                _Comment = drpList.SelectedValue;
                            }
                        }



                    }



                    if (strValueYes != "")
                    {
                        SaveSegmentDtls(Convert.ToString(DR["EPS_CODE"]).Trim(), Convert.ToString(DR1["EPSM_FIELD_ID"]).Trim(), Convert.ToString(DR1["EPSM_FIELD_NAME"]).Trim(), strValueYes, _Comment);

                    }

                }
                i = i + 1;
            }
        }


        public void CreateSegCtrls()
        {
            string Position = "";
            DataSet DS = new DataSet();

            DS = GetSegmentData();

            foreach (DataRow DR in DS.Tables[0].Rows)
            {
                Position = Convert.ToString(DR["EPS_POSITION"]);

                Panel myFieldSet = new Panel();

                myFieldSet.GroupingText = Convert.ToString(DR["EPS_NAME"]);
                myFieldSet.Style.Add("text-align", "justify");
                myFieldSet.CssClass = "lblCaption1";
                //if (DS.Tables[0].Rows[0].IsNull("EPS_WIDTH") == false)
                //{
                //    myFieldSet.Width = Convert.ToInt32(DS.Tables[0].Rows[0]["EPS_WIDTH"]);
                //    myFieldSet.Style.Add("width", Convert.ToString(DS.Tables[0].Rows[0]["EPS_WIDTH"] + "px"));
                //}
                //else
                //{
                //    myFieldSet.Width = 500;
                //    myFieldSet.Style.Add("width", "500PX");
                //}

                myFieldSet.Style.Add("width", "99%");
                Int32 i = 1;

                DataSet DS1 = new DataSet();

                DS1 = GetSegmentMaster(Convert.ToString(DR["EPS_CODE"]));

                foreach (DataRow DR1 in DS1.Tables[0].Rows)
                {


                    Boolean _checked = false;
                    string strComment = "";
                    objAnes = new EMR_Physiotherapy();
                    string Criteria2 = "1=1 AND EPSD_ENTRY_FROM='IP' ";
                    Criteria2 += " AND EPSD_TYPE='" + Convert.ToString(DR1["EPSM_TYPE"]).Trim() + "' AND EPSD_FIELD_ID='" + Convert.ToString(DR1["EPSM_FIELD_ID"]).Trim() + "' ";
                    Criteria2 += " AND EPSD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND EPSD_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

                    DataSet DS2 = new DataSet();
                    DS2 = objAnes.PhysiotherapySegmentDtlsGet(Criteria2);

                    if (DS2.Tables[0].Rows.Count > 0)
                    {
                        _checked = true;

                        strComment = Convert.ToString(DS2.Tables[0].Rows[0]["EPSD_COMMENT"]).Trim();
                    }


                    if (Convert.ToString(DR1["EPSM_CONTROL_TYPE"]).ToUpper() == "CheckBox".ToUpper())
                    {
                        CheckBox chk = new CheckBox();
                        chk.ID = "chk" + Convert.ToString(DR1["EPSM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["EPSM_FIELD_ID"]).Trim();
                        chk.Text = Convert.ToString(DR1["EPSM_FIELD_NAME"]).Trim();
                        chk.CssClass = "lblCaption1";

                        chk.Checked = _checked;

                        //tdHe1.Controls.Add(chk);
                        //trHe1.Controls.Add(tdHe1);
                        //myFieldSet.Controls.Add(trHe1);

                        myFieldSet.Controls.Add(chk);
                    }


                    if (Convert.ToString(DR1["EPSM_CONTROL_TYPE"]).ToUpper() == "TextBox".ToUpper())
                    {
                        if (Convert.ToString(DR1["EPSM_FIELD_NAME"]).Trim() != "")
                        {

                            Label lbl = new Label();
                            lbl.CssClass = "lblCaption1";
                            lbl.Text = Convert.ToString(DR1["EPSM_FIELD_NAME"]).Trim() + ":";
                            myFieldSet.Controls.Add(lbl);

                            Literal lit1 = new Literal() { Mode = LiteralMode.PassThrough, Text = "<br/>" };
                            myFieldSet.Controls.Add(lit1);

                        }



                        TextBox txt = new TextBox();
                        txt.ID = "txt" + Convert.ToString(DR1["EPSM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["EPSM_FIELD_ID"]).Trim();
                        // txt.Text = Convert.ToString(DR1["EPSM_FIELD_NAME"]);
                        txt.CssClass = "Label";

                        if (DR1.IsNull("EPSM_WIDTH") == false)
                        {
                            txt.Width = Convert.ToInt32(DR1["EPSM_WIDTH"]);
                            txt.Style.Add("width", Convert.ToString(DR1["EPSM_WIDTH"] + "px"));
                        }
                        else
                        {
                            txt.Style.Add("width", "99%");
                        }

                        txt.Text = strComment;



                        if (DR1.IsNull("EPSM_TARGET_CTRL") == false)
                        {
                            if (Convert.ToString(DR1["EPSM_FIELD_NAME"]).Trim() == "Height" || Convert.ToString(DR1["EPSM_FIELD_NAME"]).Trim() == "Weight")
                            {
                                string strCtrlID = Convert.ToString(DR1["EPSM_TARGET_CTRL"]).Trim();
                                txt.Attributes.Add("onkeyup", "return BindBMI('txt" + Convert.ToString(DR1["EPSM_TYPE"]).Trim() + "_1','txt" + Convert.ToString(DR1["EPSM_TYPE"]).Trim() + "_2','txt" + Convert.ToString(DR1["EPSM_TYPE"]).Trim() + "_" + strCtrlID + "')");

                            }
                        }
                        myFieldSet.Controls.Add(txt);


                    }



                    if (Convert.ToString(DR1["EPSM_CONTROL_TYPE"]).ToUpper() == "AutoTextBox".ToUpper())
                    {
                        if (Convert.ToString(DR1["EPSM_FIELD_NAME"]).Trim() != "")
                        {
                            Label lbl = new Label();
                            lbl.CssClass = "lblCaption1";
                            lbl.Text = Convert.ToString(DR1["EPSM_FIELD_NAME"]).Trim() + ":";
                            myFieldSet.Controls.Add(lbl);
                        }
                        Literal lit1 = new Literal() { Mode = LiteralMode.PassThrough, Text = "<br/>" };
                        myFieldSet.Controls.Add(lit1);

                        TextBox txt = new TextBox();
                        txt.ID = "txt" + Convert.ToString(DR1["EPSM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["EPSM_FIELD_ID"]).Trim();

                        txt.TextMode = TextBoxMode.MultiLine;
                        txt.Height = 20;
                        txt.Style.Add("resize", "none");
                        txt.Attributes.Add("autocomplete", "off");
                        txt.CssClass = "Label";

                        if (DR1.IsNull("EPSM_WIDTH") == false)
                        {
                            txt.Width = Convert.ToInt32(DR1["EPSM_WIDTH"]);
                            txt.Style.Add("width", Convert.ToString(DR1["EPSM_WIDTH"] + "px"));
                        }
                        else
                        {
                            txt.Style.Add("width", "99%");
                        }

                        if (DR1.IsNull("EPSM_HEIGHT") == false)
                        {
                            txt.Height = Convert.ToInt32(DR1["EPSM_HEIGHT"]);
                            txt.Style.Add("height", Convert.ToString(DR1["EPSM_HEIGHT"] + "px"));
                        }
                        else
                        {
                            txt.Style.Add("height", "20px");
                        }

                        AjaxControlToolkit.AutoCompleteExtender autoCompleteExtender = new AjaxControlToolkit.AutoCompleteExtender();
                        autoCompleteExtender.TargetControlID = txt.ID;


                        if (DR1.IsNull("EPSM_DATA_LOAD_FROM") == false && Convert.ToString(DR1["EPSM_DATA_LOAD_FROM"]) != "")
                        {
                            if (Convert.ToString(DR1["EPSM_DATA_LOAD_FROM"]).Trim().ToUpper() == "DIAGNOSIS")
                            {
                                autoCompleteExtender.ServiceMethod = "GetDiagnosisName";
                            }
                            if (Convert.ToString(DR1["EPSM_DATA_LOAD_FROM"]).Trim().ToUpper() == "PROCEDURE")
                            {
                                autoCompleteExtender.ServiceMethod = "GetProcedure";
                            }

                            if (Convert.ToString(DR1["EPSM_DATA_LOAD_FROM"]).Trim().ToUpper() == "DOCTOR")
                            {
                                autoCompleteExtender.ServiceMethod = "GetDoctorName";
                            }

                            if (Convert.ToString(DR1["EPSM_DATA_LOAD_FROM"]).Trim().ToUpper() == "HISTORY")
                            {
                                autoCompleteExtender.ServiceMethod = "GetSegmentValue";
                            }
                        }
                        else
                        {
                            autoCompleteExtender.ServiceMethod = "GetSegmentValue";
                        }

                        autoCompleteExtender.ID = "aut" + Convert.ToString(DR1["EPSM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["EPSM_FIELD_ID"]).Trim();
                        autoCompleteExtender.CompletionListCssClass = "AutoExtender";
                        autoCompleteExtender.CompletionListItemCssClass = "AutoExtenderList";
                        autoCompleteExtender.CompletionListHighlightedItemCssClass = "AutoExtenderHighlight";
                        // autoCompleteExtender.CompletionListElementID = "divwidth";
                        autoCompleteExtender.ContextKey = Convert.ToString(DR1["EPSM_TYPE"]).Trim() + "~" + Convert.ToString(DR1["EPSM_FIELD_ID"]).Trim();
                        autoCompleteExtender.UseContextKey = true;

                        // autoCompleteExtender.ServicePath = "YourAutoCompleteWebService.asmx";
                        autoCompleteExtender.CompletionInterval = 10;
                        autoCompleteExtender.CompletionSetCount = 15;
                        autoCompleteExtender.EnableCaching = true;
                        autoCompleteExtender.MinimumPrefixLength = 1;



                        txt.Text = strComment;

                        if (DR1.IsNull("EPSM_TARGET_CTRL") == false)
                        {
                            string strCtrlID = Convert.ToString(DR1["EPSM_TARGET_CTRL"]).Trim();
                            txt.Attributes.Add("onkeypress", "return BindCC(event,'" + txt.ID + "','txt" + Convert.ToString(DR1["EPSM_TYPE"]).Trim() + "_" + strCtrlID + "')");

                            autoCompleteExtender.ContextKey = Convert.ToString(DR1["EPSM_TYPE"]).Trim() + "~" + strCtrlID;
                        }


                        myFieldSet.Controls.Add(txt);
                        myFieldSet.Controls.Add(autoCompleteExtender);



                    }



                    if (Convert.ToString(DR1["EPSM_CONTROL_TYPE"]).ToUpper() == "Date".ToUpper())
                    {
                        Label lbl = new Label();
                        lbl.CssClass = "lblCaption1";
                        lbl.Text = Convert.ToString(DR1["EPSM_FIELD_NAME"]).Trim() + ":";
                        lbl.Width = 100;
                        myFieldSet.Controls.Add(lbl);

                        TextBox txt = new TextBox();
                        txt.ID = "txt" + Convert.ToString(DR1["EPSM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["EPSM_FIELD_ID"]).Trim();
                        // txt.Text = Convert.ToString(DR1["EPSM_FIELD_NAME"]);
                        txt.CssClass = "lblCaption1";
                        txt.Style.Add("width", "75px");

                        AjaxControlToolkit.CalendarExtender CalendarExtender = new AjaxControlToolkit.CalendarExtender();
                        CalendarExtender.TargetControlID = txt.ID;
                        CalendarExtender.Format = "dd/MM/yyyy";


                        txt.Text = strComment;

                        myFieldSet.Controls.Add(txt);
                        myFieldSet.Controls.Add(CalendarExtender);


                    }

                    if (Convert.ToString(DR1["EPSM_CONTROL_TYPE"]).ToUpper() == "DateTime".ToUpper())
                    {
                        Label lbl = new Label();
                        lbl.CssClass = "lblCaption1";
                        lbl.Text = Convert.ToString(DR1["EPSM_FIELD_NAME"]).Trim() + ":";
                        lbl.Width = 100;
                        myFieldSet.Controls.Add(lbl);

                        TextBox txt = new TextBox();
                        txt.ID = "txt" + Convert.ToString(DR1["EPSM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["EPSM_FIELD_ID"]).Trim();
                        // txt.Text = Convert.ToString(DR1["EPSM_FIELD_NAME"]);
                        txt.CssClass = "lblCaption1";
                        txt.Style.Add("width", "72px");

                        AjaxControlToolkit.CalendarExtender CalendarExtender = new AjaxControlToolkit.CalendarExtender();
                        CalendarExtender.TargetControlID = txt.ID;
                        CalendarExtender.Format = "dd/MM/yyyy";


                        DropDownList drpHour = new DropDownList();
                        drpHour.ID = "drpHour" + Convert.ToString(DR1["EPSM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["EPSM_FIELD_ID"]).Trim();
                        drpHour.CssClass = "lblCaption1";
                        drpHour.Style.Add("width", "45px");

                        drpHour.DataSource = BindHour();
                        drpHour.DataTextField = "Name";
                        drpHour.DataValueField = "Code";
                        drpHour.DataBind();

                        DropDownList drpMin = new DropDownList();
                        drpMin.ID = "drpMin" + Convert.ToString(DR1["EPSM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["EPSM_FIELD_ID"]).Trim();
                        drpMin.CssClass = "lblCaption1";
                        drpMin.Style.Add("width", "45px");

                        drpMin.DataSource = BindMinutes();
                        drpMin.DataTextField = "Name";
                        drpMin.DataValueField = "Code";
                        drpMin.DataBind();





                        string strDateTime = strComment.Trim();
                        string[] arrDateTime = strDateTime.Split(' ');

                        if (arrDateTime.Length > 0)
                        {
                            txt.Text = arrDateTime[0];

                            if (arrDateTime.Length > 1)
                            {
                                string strIA_TIME_IN = arrDateTime[1];

                                string[] arrIA_TIME_IN = strIA_TIME_IN.Split(':');
                                if (arrIA_TIME_IN.Length > 1)
                                {
                                    drpHour.SelectedValue = arrIA_TIME_IN[0];
                                    drpMin.SelectedValue = arrIA_TIME_IN[1];

                                }
                            }

                        }

                        myFieldSet.Controls.Add(txt);
                        myFieldSet.Controls.Add(CalendarExtender);
                        myFieldSet.Controls.Add(drpHour);
                        myFieldSet.Controls.Add(drpMin);


                    }



                    if (Convert.ToString(DR1["EPSM_CONTROL_TYPE"]).ToUpper() == "Label".ToUpper())
                    {
                        Label lbl = new Label();
                        lbl.CssClass = "lblCaption1";
                        lbl.Text = Convert.ToString(DR1["EPSM_FIELD_NAME"]).Trim();
                        myFieldSet.Controls.Add(lbl);



                    }

                    if (Convert.ToString(DR1["EPSM_CONTROL_TYPE"]) == "DropDownList")
                    {

                        Label lbl = new Label();
                        lbl.CssClass = "lblCaption1";
                        lbl.Text = Convert.ToString(DR1["EPSM_FIELD_NAME"]).Trim() + ":    ";
                        lbl.Width = 150;
                        myFieldSet.Controls.Add(lbl);

                        DropDownList DrpList = new DropDownList();
                        DrpList.ID = "drp" + Convert.ToString(DR1["EPSM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["EPSM_FIELD_ID"]).Trim();


                        if (DR1.IsNull("EPSM_WIDTH") == false)
                        {
                            DrpList.Width = Convert.ToInt32(DR1["EPSM_WIDTH"]);
                            DrpList.Style.Add("width", Convert.ToString(DR1["EPSM_WIDTH"] + "px"));
                        }
                        else
                        {
                            DrpList.Width = 200;
                        }

                        string strRadFieldName = Convert.ToString(DR1["EPSM_DATA"]);
                        string[] arrRadFieldName = strRadFieldName.Split('|');


                        for (int intRad = 0; intRad <= arrRadFieldName.Length - 1; intRad++)
                        {

                            DrpList.Items.Add(new ListItem(arrRadFieldName[intRad], arrRadFieldName[intRad]));

                        }

                        DrpList.CssClass = "lblCaption1";

                        for (int intRad = 0; intRad <= arrRadFieldName.Length - 1; intRad++)
                        {

                            if (DrpList.Items[intRad].Value == strComment)
                            {
                                DrpList.SelectedValue = strComment;
                            }

                        }

                        myFieldSet.Controls.Add(DrpList);

                    }




                    if (Convert.ToString(DR1["EPSM_CONTROL_TYPE"]) == "RadioButtonList")
                    {

                        Label lbl = new Label();
                        lbl.CssClass = "lblCaption1";
                        lbl.Text = Convert.ToString(DR1["EPSM_FIELD_NAME"]).Trim() + ":    ";
                        lbl.Width = 150;
                        myFieldSet.Controls.Add(lbl);

                        RadioButtonList RadList = new RadioButtonList();
                        RadList.ID = "radl" + Convert.ToString(DR1["EPSM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["EPSM_FIELD_ID"]).Trim();

                        RadList.RepeatDirection = RepeatDirection.Horizontal;
                        if (DR1.IsNull("EPSM_WIDTH") == false && Convert.ToString(DR1["EPSM_WIDTH"]) != "")
                        {
                            RadList.Width = Convert.ToInt32(DR1["EPSM_WIDTH"]);
                        }
                        else
                        {
                            RadList.Width = 200;
                        }
                        string strRadFieldName = Convert.ToString(DR1["EPSM_DATA"]);
                        string[] arrRadFieldName = strRadFieldName.Split('|');


                        for (int intRad = 0; intRad <= arrRadFieldName.Length - 1; intRad++)
                        {

                            RadList.Items.Add(new ListItem(arrRadFieldName[intRad], arrRadFieldName[intRad]));

                        }

                        RadList.CssClass = "lblCaption1";

                        for (int intRad = 0; intRad <= arrRadFieldName.Length - 1; intRad++)
                        {

                            if (RadList.Items[intRad].Value == strComment)
                            {
                                RadList.SelectedValue = strComment;
                            }

                        }

                        myFieldSet.Controls.Add(RadList);

                    }


                    if (Convert.ToString(DR1["EPSM_CONTROL_TYPE"]) == "Image")
                    {

                        Label lbl = new Label();
                        lbl.CssClass = "lblCaption1";
                        lbl.Text = Convert.ToString(DR1["EPSM_FIELD_NAME"]).Trim() + ":    ";
                        lbl.Width = 150;
                        myFieldSet.Controls.Add(lbl);

                        Image img = new Image();
                        img.ID = "img" + Convert.ToString(DR1["EPSM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["EPSM_FIELD_ID"]).Trim();

                        img.ImageUrl = "../Images/" + Convert.ToString(DR1["EPSM_DATA"]).Trim();

                        if (DR1.IsNull("EPSM_WIDTH") == false && Convert.ToString(DR1["EPSM_WIDTH"]) != "")
                        {
                            img.Width = Convert.ToInt32(DR1["EPSM_WIDTH"]);
                        }
                        else
                        {
                            img.Width = 50;
                        }



                        if (DR1.IsNull("EPSM_TARGET_CTRL") == false)
                        {
                            string strCtrlID = Convert.ToString(DR1["EPSM_TARGET_CTRL"]).Trim();
                            img.Attributes.Add("onClick", "return BindValue('" + Convert.ToString(DR1["EPSM_FIELD_NAME"]).Trim() + "','txt" + Convert.ToString(DR1["EPSM_TYPE"]).Trim() + "_" + strCtrlID + "')");


                        }

                        myFieldSet.Controls.Add(img);

                    }






                    Literal lit = new Literal() { Mode = LiteralMode.PassThrough, Text = "<br/>" };
                    myFieldSet.Controls.Add(lit);



                }


                if (Position == "LEFT")
                {
                    plhoPhyAssessmentLeft.Controls.Add(myFieldSet);
                }
                else if (Position == "MIDDLE")
                {
                    plhoPhyAssessmentMiddle.Controls.Add(myFieldSet);
                }
                else if (Position == "RIGHT")
                {
                    plhoPhyAssessmentRight.Controls.Add(myFieldSet);
                }





                i = i + 1;



            }
        }


        Boolean CheckPassword(string UserID, string Password)
        {
            string Criteria = " 1=1 ";
            Criteria += " AND HUM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HUM_USER_ID ='" + UserID + "' AND   HUM_USER_PASS= '" + Password + "'";

            DataSet ds = new DataSet();
            //ds = dbo.UserMasterGet(drpBranch.SelectedValue, drpUsers.SelectedValue , txtPassword.Text);
            ds = objCom.UserMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            return false;
        }



        #endregion

        #region AutoExt
        [System.Web.Services.WebMethod]
        public static string[] GetProcedure(string prefixText)
        {
            DataSet ds = new DataSet();
            CommonBAL dbo = new CommonBAL();

            string[] Data;

            ds = dbo.HaadServicessListGet("Procedure", prefixText, "", strSessionDeptId);

            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["Code"]).Trim() + " ~ " + Convert.ToString(ds.Tables[0].Rows[i]["Description"]).Trim();


                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetDoctorName(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND HSFM_SF_STATUS='Present'";
            // Criteria += " AND HSFM_FNAME Like '%" + prefixText + "%'";
            Criteria += " AND ( HSFM_STAFF_ID Like '%" + prefixText + "%' OR HSFM_FNAME + ' ' +isnull(HSFM_MNAME,'') + ' '  + isnull(HSFM_LNAME,'')   like '%" + prefixText + "%' ) ";

            DataSet ds = new DataSet();
            CommonBAL objCom = new CommonBAL();
            ds = objCom.GetStaffMaster(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["HSFM_STAFF_ID"]) + "~" + Convert.ToString(ds.Tables[0].Rows[i]["FullName"]);
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }


        [System.Web.Services.WebMethod]
        public static string[] GetSegmentValue(string prefixText, int count, string contextKey)
        {
            string SegType = "", FieldID = "";
            string[] strContKey = contextKey.Split('~');

            if (strContKey.Length > 0)
            {
                SegType = strContKey[0];
                FieldID = strContKey[1];
                //SegFieldName = strContKey[2];
            }

            string[] Data;
            string Criteria = " 1=1  AND EPSD_ENTRY_FROM='IP' ";

            EMR_Physiotherapy objAnes = new EMR_Physiotherapy();

            Criteria += " AND EPSD_TYPE='" + SegType + "' AND EPSD_FIELD_ID='" + FieldID + "' ";
            Criteria += " AND EPSD_COMMENT Like '%" + prefixText + "%'";

            DataSet DS = new DataSet();
            DS = objAnes.PhysiotherapySegmentDtlsGet(Criteria);




            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = DS.Tables[0].Rows[i]["EPSD_COMMENT"].ToString();
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }


        [System.Web.Services.WebMethod]
        public static string[] GetDiagnosisName(string prefixText)
        {

            CommonBAL dbo = new CommonBAL();
            DataSet DS = new DataSet();

            string[] Data;

            string Criteria = " 1=1 AND DIM_STATUS='A' ";

            Criteria += " AND ( DIM_ICD_ID LIKE '" + prefixText + "' OR DIM_ICD_DESCRIPTION   LIKE '" + prefixText + "%')";
            DS = dbo.DrICDMasterGet(Criteria, "30");

            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["DIM_ICD_ID"]) + "~" + Convert.ToString(DS.Tables[0].Rows[i]["DIM_ICD_DESCRIPTION"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                if (!IsPostBack)
                {


                    strSessionDeptId = Convert.ToString(Session["User_DeptID"]);
                    CommonBAL objCom = new CommonBAL();
                    string strDate = "";
                    strDate = objCom.fnGetDate("dd/MM/yyyy");

                }


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Anesthesia.Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            generateDynamicControls();
        }



        public void generateDynamicControls()
        {
            CreateSegCtrls();
        }
    }
}