﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using EMR_BAL;

namespace Mediplus.EMR.Physiotherapy
{
    public partial class Index : System.Web.UI.Page
    {
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }
            if (!IsPostBack)
            {
                //if (Convert.ToString(Session["EPM_STATUS"]).ToUpper() == "Y" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                //{
                //    btnSave.Visible = false;
                //}
                ////HIDE THE SAVE OPTION FOR NEXT DAY OF THE VISIT
                //if (Convert.ToString(Session["EMRCurrentDateDiff"]) != "0" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                //{
                //    btnSave.Visible = false;
                //}

                //GetMenus();
                //if (drpSegment.Items.Count > 0)
                //{
                //    pnlPeriodonticChart.SegmentType = drpSegment.SelectedValue;
                //}
            }
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                //  pnlPhyEvalution.Segment = drpSegment.SelectedValue;
              pnlPhyEvalution.SavePhyEvalution();
                //  PhyTherapyChart.SaveVPhyTherapyChart();



                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowMessage()", true);
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "       Pediatric.Index.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }
    }
}