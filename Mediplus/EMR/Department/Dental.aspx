﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EMR/PatientHeader.Master" AutoEventWireup="true" CodeBehind="Dental.aspx.cs" Inherits="Mediplus.EMR.Department.Dental" %>

<%@ Register Src="~/EMR/Department/Treatment.ascx" TagPrefix="UC1" TagName="Treatment" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <UC1:Treatment id="pnlTreatment" runat="server" ></UC1:Treatment>
</asp:Content>
