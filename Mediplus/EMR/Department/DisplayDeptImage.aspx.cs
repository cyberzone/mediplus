﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_BAL;

namespace Mediplus.EMR.Department
{
    public partial class DisplayDeptImage : System.Web.UI.Page
    {
        CommonBAL objCom = new CommonBAL();

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindScreenCustomization()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";

            Criteria += " AND SCREENNAME='EMR_DEPT' ";

            DS = new DataSet();
            DS = objCom.ScreenCustomizationGet(Criteria);
            ViewState["DEPARTMENTAL_IMAGE_PATH"] = "0";



            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    if (Convert.ToString(DR["SEGMENT"]) == "DEPARTMENTAL_IMAGE_PATH")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            ViewState["DEPARTMENTAL_IMAGE_PATH"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }



                }
            }



        }


        void BindScanFile()
        {
            string DentalPath = "", strFileNo = "", strEPMDate = "";
            string PBMI_FileName = "";
            string PBMI_FullPath = "";


            strFileNo = Convert.ToString(ViewState["EMR_PT_ID"]).Replace("/", "_"); ;
            strEPMDate = Convert.ToString(ViewState["EPM_DATE"]).Replace("/", "");
            DentalPath = Convert.ToString(ViewState["DEPARTMENTAL_IMAGE_PATH"]);
            PBMI_FileName = strFileNo + "@" + strEPMDate + "@" + Convert.ToString(ViewState["EMR_ID"]) + "@" + Convert.ToString(ViewState["SegmentType"]) + ".jpg";

            // 00028@23122014@11@DCF


            PBMI_FullPath = "../Images/EmptyUserImage.jpg";// @DentalPath + Convert.ToString(ViewState["SegmentType"]) + @"\" + strFileNo + @"\notsaved.jpg";// +PBMI_FileName;

            Response.ContentType = "image/JPEG";
            Response.WriteFile(PBMI_FullPath);
            Response.End();
        }


        void DisplayImage()
        {
            try
            {

                string DentalPath = "", strFileNo = "", strEPMDate = "";
                string PBMI_FileName = "";
                string PBMI_FullPath = "";


                strFileNo = Convert.ToString(ViewState["EMR_PT_ID"]).Replace("/", "_"); ;
                strEPMDate = Convert.ToString(ViewState["EPM_DATE"]).Replace("/", "");
                DentalPath = Convert.ToString(ViewState["DEPARTMENTAL_IMAGE_PATH"]);
                PBMI_FileName = strFileNo + "@" + strEPMDate + "@" + Convert.ToString(ViewState["EMR_ID"]) + "@" + Convert.ToString(ViewState["SegmentType"]) + ".jpg";

                // 00028@23122014@11@DCF

                if (File.Exists(@DentalPath + Convert.ToString(ViewState["SegmentType"]) + @"\" + strFileNo + @"\" + PBMI_FileName))
                {
                    PBMI_FullPath = @DentalPath + Convert.ToString(ViewState["SegmentType"]) + @"\" + strFileNo + @"\" + PBMI_FileName;
                }
                else
                {
                    PBMI_FullPath = @DentalPath + Convert.ToString(ViewState["SegmentType"]) + @"\notsaved.jpg";
                }

                //TextFileWriting(PBMI_FullPath);

                FileStream fs = new FileStream(PBMI_FullPath,FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(fs);
                Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                br.Close();
                fs.Close();

                //Write the file to response Stream
                Response.Buffer = true;
                Response.Charset = "";
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.ContentType = "image/JPEG";
                Response.AddHeader("content-disposition", "attachment;filename=" + PBMI_FileName);
                Response.BinaryWrite(bytes);
                Response.Flush();
                Response.End();
            }
            catch
            {
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            ViewState["EMR_ID"] = Request.QueryString["EMR_ID"];
            ViewState["EMR_PT_ID"] = Request.QueryString["EMR_PT_ID"];
            ViewState["EPM_DATE"] = Request.QueryString["EPM_DATE"];
            ViewState["SegmentType"] = Request.QueryString["SegmentType"];
            BindScreenCustomization();
            //BindScanFile();
            DisplayImage();
        }
    }
}