﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using EMR_BAL;
using System.Net;


namespace Mediplus.EMR.Department
{
    public partial class OpenDepartmentalImage : System.Web.UI.Page
    {
        CommonBAL objCom = new CommonBAL();
        String SegmentType = "";

        void BindScreenCustomization()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";

            Criteria += " AND SCREENNAME='EMR_DEPT'";
            DS = new DataSet();
            DS = objCom.ScreenCustomizationGet(Criteria);
            ViewState["DEPARTMENTAL_IMAGE_PATH"] = "0";



            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    if (Convert.ToString(DR["SEGMENT"]) == "DEPARTMENTAL_IMAGE_PATH")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            ViewState["DEPARTMENTAL_IMAGE_PATH"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }



                }
            }


        }

        public void SavePediatricImage()
        {
            string DentalPath = "", strFileNo = "", strEPMDate = "";
            string PBMI_FileName = "";
            string PBMI_FullPath = "";


            strFileNo = Convert.ToString(Session["EMR_PT_ID"]).Replace("/", "_"); ;
            strEPMDate = Convert.ToString(Session["EPM_DATE"]).Replace("/", "");
            DentalPath = Convert.ToString(ViewState["DEPARTMENTAL_IMAGE_PATH"]);


            if (!Directory.Exists(@DentalPath + SegmentType))
            {
                Directory.CreateDirectory(@DentalPath + SegmentType);
            }


            if (!Directory.Exists(@DentalPath + strFileNo))
            {
                Directory.CreateDirectory(@DentalPath + strFileNo);
            }




            PBMI_FileName = strFileNo + "@" + strEPMDate + "@" + Convert.ToString(Session["EMR_ID"]) + "@" + SegmentType + ".jpg";

            // 00028@23122014@11@DCF


            PBMI_FullPath = @DentalPath + SegmentType + @"\" + strFileNo + @"\" + PBMI_FileName;

            //  System.Drawing.Image im = System.Drawing.Image.FromFile(editableImage.Src);
            // im.Save(@PBMI_FullPath);


            //          InputImages = System.IO.Directory.GetFiles(DEPARTMENTAL_IMAGE_PATH + ImageType + "\\" + SelectedPatient.FileNumber.Replace("/", "_"), "*.jpg").ToList();



        }

        void BindImage(string SegmentType)
        {
            string DentalPath = "", strFileNo = "", strEPMDate = "";

            strFileNo = Convert.ToString(Session["EMR_PT_ID"]).Replace("/", "_"); ;
            strEPMDate = Convert.ToString(Session["EPM_DATE"]).Replace("/", "");
            DentalPath = Convert.ToString(ViewState["DEPARTMENTAL_IMAGE_PATH"]);
            editableImage.Src = "DisplayDeptImage.aspx?EMR_PT_ID=" + strFileNo + "&EPM_DATE=" + strEPMDate + "&SegmentType=" + SegmentType; //   @DentalPath + SegmentType + "\notsaved.jpg";


        }



        protected void Page_Load(object sender, EventArgs e)
        {
            SegmentType = (String)(Request.Params["ImageType"]);

            BindScreenCustomization();
            BindImage(SegmentType);
            // SavePediatricImage();

            if (Convert.ToString(Session["EPM_STATUS"]).ToUpper() == "Y" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
            {
                btnAdd.Visible = false;
            }

            //HIDE THE SAVE OPTION FOR NEXT DAY OF THE VISIT
            if (Convert.ToString(Session["EMRCurrentDateDiff"]) != "0" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
            {
                   btnAdd.Visible = false;
            }

        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            string DentalPath = "", strFileNo = "", strEPMDate = "";
            string PBMI_FileName = "";
            string PBMI_FullPath = "";


            strFileNo = Convert.ToString(Session["EMR_PT_ID"]).Replace("/", "_"); ;
            strEPMDate = Convert.ToString(Session["EPM_DATE"]).Replace("/", "");
            DentalPath = Convert.ToString(ViewState["DEPARTMENTAL_IMAGE_PATH"]);


            if (!Directory.Exists(@DentalPath + @"\" + SegmentType))
            {
                Directory.CreateDirectory(@DentalPath + @"\" + SegmentType);
            }


            if (!Directory.Exists(@DentalPath + @"\" + SegmentType + @"\" + strFileNo))
            {
                Directory.CreateDirectory(@DentalPath + @"\" + SegmentType + @"\" + strFileNo);
            }




            PBMI_FileName = strFileNo + "@" + strEPMDate + "@" + Convert.ToString(Session["EMR_ID"]) + "@" + SegmentType + ".jpg";

            // 00028@23122014@11@DCF


            PBMI_FullPath = @DentalPath + SegmentType + @"\" + strFileNo + @"\" + PBMI_FileName;


            using (FileStream fs = new FileStream(PBMI_FullPath, FileMode.Create))
            {
                using (BinaryWriter bw = new BinaryWriter(fs))
                {
                    byte[] data = Convert.FromBase64String(hidImgData.Value);
                    bw.Write(data);
                    bw.Close();

                }
            }

            BindImage(SegmentType);
        }



    }
}