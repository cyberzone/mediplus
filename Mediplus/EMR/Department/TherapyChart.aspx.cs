﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Mediplus.EMR.Department
{
    public partial class TherapyChart : System.Web.UI.Page
    {
        public string SegmentType;

        public void BindPediatricImage()
        {

            ImageEditorPDC.Src = "OpenDepartmentalImage.aspx?ImageType=" + SegmentType;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../../Default.aspx?NoSession=1"); }

            if (!IsPostBack)
            {
                SegmentType = "THERAPYC";
                BindPediatricImage();

            }
        }
       
    }
}