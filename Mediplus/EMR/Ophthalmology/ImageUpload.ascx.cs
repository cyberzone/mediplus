﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_BAL;
using System.Web.UI.HtmlControls;

namespace Mediplus.EMR.Ophthalmology
{
    public partial class ImageUpload : System.Web.UI.UserControl
    {
        DataSet DS;
        CommonBAL objCom = new CommonBAL();

        

        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("~/EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindScreenCustomization()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";

            Criteria += " AND SCREENNAME='EMR_DEPT'";
            DS = new DataSet();
            DS = objCom.ScreenCustomizationGet(Criteria);
            ViewState["DEPARTMENTAL_IMAGE_PATH"] = "0";



            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    if (Convert.ToString(DR["SEGMENT"]) == "DEPARTMENTAL_IMAGE_PATH")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            ViewState["DEPARTMENTAL_IMAGE_PATH"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }



                }
            }



        }


        void BindMultipleImages()
        {
          

            string DentalPath = "", strFileNo = "", strEPMDate = "";
            string PBMI_FileName = "";
            string SegmentType1 = "EYER";


            strFileNo = Convert.ToString(Session["EMR_PT_ID"]).Replace("/", "_"); ;
            strEPMDate = Convert.ToString(Session["EPM_DATE"]).Replace("/", "");
            DentalPath = Convert.ToString(ViewState["DEPARTMENTAL_IMAGE_PATH"]);



            for (int i = 1; i <= 10; i++)
            {

                PBMI_FileName = strFileNo + "@" + strEPMDate + "@" + Convert.ToString(Session["EMR_ID"]) + "@" + SegmentType1 + "@" + Convert.ToString(i) + ".jpg";

                if (System.IO.File.Exists(@DentalPath + SegmentType1 + @"\" + strFileNo + @"\" + PBMI_FileName))
                {
                    HtmlGenericControl tr1 = new HtmlGenericControl("tr");
                    HtmlGenericControl td1 = new HtmlGenericControl("td");

                    Image img = new Image();
                    img.CssClass = "ImageStyle";
                    img.ImageUrl = "DisplayScandImage.aspx?EMR_PT_ID=" + strFileNo + "&EPM_DATE=" + strEPMDate + "&SegmentType=EYER&ID=" + Convert.ToString(i);

                    td1.Controls.Add(img);
                    tr1.Controls.Add(td1);
                  //  PlaceHolder1.Controls.Add(tr1);

                }
            }
        }
        #endregion




        protected void Page_Load(object sender, EventArgs e)
        {
            if (Convert.ToString(Session["User_ID"]) == "" || Convert.ToString(Session["User_ID"]) == null)
            {
                Session["ErrorMsg"] = "Session Expired Please login again";
                Response.Redirect("../../ErrorPage.aspx");
            }
            lblStatus.Text = "";

            if (!IsPostBack)
            {

                try
                {
                    BindScreenCustomization();
                  
                    //BindMultipleImages();
                    string strFileNo = Convert.ToString(Session["EMR_PT_ID"]).Replace("/", "_"); 
                     string SegmentType1 = "EYER";
                    string   DentalPath = Convert.ToString(ViewState["DEPARTMENTAL_IMAGE_PATH"]);
                    ctrlDisplayImage.EMRScanPath= Convert.ToString(ViewState["DEPARTMENTAL_IMAGE_PATH"]);
                    ctrlDisplayImage.RootPath = @DentalPath + @"\" + SegmentType1 ;
                    
                }

                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      ImageUpload.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }


            }
        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                if (fileLogo.FileName == "")
                {
                    lblStatus.Text = "Please select the file";
                    goto FundEnd;
                }

                string DentalPath = "", strFileNo = "", strEPMDate = "";
                string PBMI_FileName = "";
                string PBMI_FullPath = "";
                string SegmentType1 = "EYER";


                strFileNo = Convert.ToString(Session["EMR_PT_ID"]).Replace("/", "_"); ;
                strEPMDate = Convert.ToString(Session["EPM_DATE"]).Replace("/", "");
                DentalPath = Convert.ToString(ViewState["DEPARTMENTAL_IMAGE_PATH"]);

                string strVisitDate = Convert.ToString(Session["HPV_DATE"]);
                string[] arrDate = strVisitDate.Split('/');

              string   DateFolder = arrDate[0] + "-" + arrDate[1] + "-" + arrDate[2];

                if (!Directory.Exists(@DentalPath + @"\" + SegmentType1))
                {
                    Directory.CreateDirectory(@DentalPath + @"\" + SegmentType1);
                }


                if (!Directory.Exists(@DentalPath + @"\" + SegmentType1 + @"\" + strFileNo))
                {
                    Directory.CreateDirectory(@DentalPath + @"\" + SegmentType1 + @"\" + strFileNo);
                }

                if (!Directory.Exists(@DentalPath + @"\" + SegmentType1 + @"\" + strFileNo + @"\" + DateFolder))
                {
                    Directory.CreateDirectory(@DentalPath + @"\" + SegmentType1 + @"\" + strFileNo + @"\" + DateFolder);
                }

                for (int i = 1; i <= 100; i++)
                {

                    PBMI_FileName = txtFileName.Text.Trim()+"@"+ strFileNo + "@" + strEPMDate + "@" + Convert.ToString(Session["EMR_ID"]) + "@" + Convert.ToString(i) + ".jpg";

                    if (!System.IO.File.Exists(@DentalPath + SegmentType1 + @"\" + strFileNo + @"\" + DateFolder + @"\" + PBMI_FileName))
                     {
                         goto EndFor;
                     }
                }
                EndFor:;
               

                // 00028@23122014@11@DCF


                PBMI_FullPath = @DentalPath + SegmentType1 + @"\" + strFileNo + @"\" + DateFolder  +@"\" + PBMI_FileName;

                fileLogo.SaveAs(PBMI_FullPath);

              //ctrlDisplayImage.papulateMenuTree();

            FundEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      ImageUpload.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }

            Response.Redirect("Index.aspx");

        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            
        }
    }
}