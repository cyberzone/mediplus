﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Media;
using System.Drawing;
using System.Drawing.Imaging;
using EMR_BAL;

namespace Mediplus.EMR.Ophthalmology
{
    public partial class DisplayScanImage : System.Web.UI.UserControl
    {
        DataSet DS;
        CommonBAL objCom = new CommonBAL();
        public string RootPath { get; set; }
        public string EMRScanPath { get; set; }

        #region Methods

        void BindScreenCustomization()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";

            Criteria += " AND SCREENNAME='EMR_DEPT'";
            DS = new DataSet();
            DS = objCom.ScreenCustomizationGet(Criteria);
            ViewState["DEPARTMENTAL_IMAGE_PATH"] = "0";



            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    if (Convert.ToString(DR["SEGMENT"]) == "DEPARTMENTAL_IMAGE_PATH")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            ViewState["DEPARTMENTAL_IMAGE_PATH"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }



                }
            }

        }

        public void papulateMenuTree()
        {
            string SCanPath, FileNoFolder = "";
            SCanPath = RootPath;// Convert.ToString(ViewState["EMR_SCAN_PATH"]);
            FileNoFolder = Convert.ToString(Session["EMR_PT_ID"]).Replace("/", "_");

            System.IO.DirectoryInfo RootDir = new System.IO.DirectoryInfo(SCanPath + "\\" + FileNoFolder);

            if (System.IO.Directory.Exists(SCanPath + "\\" + FileNoFolder) == true)
            {
                // output the directory into a node
                TreeNode RootNode = OutputDirectory(RootDir, null);
                // add the output to the tree

                MyTree.Nodes.Add(RootNode);
            }

        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindScreenCustomization();


                papulateMenuTree();
            }

        }

        TreeNode OutputDirectory(System.IO.DirectoryInfo directory, TreeNode parentNode)
        {


            // validate param
            if (directory == null) return null;
            // create a node for this directory
            TreeNode DirNode = new TreeNode(directory.Name);
            // get subdirectories of the current directory
            System.IO.DirectoryInfo[] SubDirectories = directory.GetDirectories();
            // OutputDirectory(SubDirectories[0], "Directories");
            // output each subdirectory


            for (int DirectoryCount = 0; DirectoryCount < SubDirectories.Length; DirectoryCount++)
            {
                OutputDirectory(SubDirectories[DirectoryCount], DirNode);
            }


            System.IO.FileInfo[] Files = directory.GetFiles();
            for (int FileCount = 0; FileCount < Files.Length; FileCount++)
            {
                DirNode.ChildNodes.Add(new TreeNode(Files[FileCount].Name));
            }        // if the parent node is null, return this node


            // otherwise add this node to the parent and return the parent
            if (parentNode == null)
            {
                return DirNode;
            }
            else
            {
                parentNode.ChildNodes.Add(DirNode);
                return parentNode;
            }
        }

        protected void MyTree_SelectedNodeChanged(object sender, EventArgs e)
        {
            //MyTree.SelectedNode.Parent.Text 
            //MyTree.SelectedNode.Parent.Parent.Text 

            if (MyTree.SelectedNode.ChildNodes.Count == 0)
            {
                //  ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowAttachment('" + Convert.ToString(Session["EMR_PT_ID"]) + "','" + MyTree.SelectedNode.Text + "');", true);

                //  imgAttach.ImageUrl = "DisplayAttachments.aspx?PT_ID=" + Convert.ToString(Session["EMR_PT_ID"]) + "&FileName=" + MyTree.SelectedNode.Text;

                ViewState["FileName"] = MyTree.SelectedNode.Text;
                ImageEditorPDC.Src = "DisplayScandImage.aspx?PT_ID=" + Convert.ToString(Session["EMR_PT_ID"]) + "&FolderName=" + MyTree.SelectedNode.Parent.Value + "&FileName=" + MyTree.SelectedNode.Text + "&SegmentType=EYER";
            }

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            string ScanPath = Convert.ToString(ViewState["DEPARTMENTAL_IMAGE_PATH"]);
            string PT_ID = Convert.ToString(Session["EMR_PT_ID"]);
            string FolderName = MyTree.SelectedNode.Parent.Value;
            string ScanFileName = MyTree.SelectedNode.Text;
            string ScanSourcePath = @ScanPath + @"\EYER" + @"\" + PT_ID + @"\" + FolderName + @"\" + ScanFileName;

            if (System.IO.File.Exists(ScanSourcePath) == true)
            {
                System.IO.File.Delete(ScanSourcePath);
            }

           //  papulateMenuTree();
             Response.Redirect("Index.aspx");
        }
    }
}