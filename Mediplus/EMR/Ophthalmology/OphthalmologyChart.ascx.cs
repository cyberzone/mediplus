﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Mediplus.EMR.Ophthalmology
{
    public partial class OphthalmologyChart : System.Web.UI.UserControl
    {
        public string SegmentType;


        public void BindPediatricImage()
        {

            ImageEditorPDC.Src = "../Department/OpenDepartmentalImage.aspx?ImageType=" + SegmentType;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindPediatricImage();

            }
        }
    }
}