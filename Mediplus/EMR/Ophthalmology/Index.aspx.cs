﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EMR_BAL;
using System.Data;
using System.IO;

namespace Mediplus.EMR.Ophthalmology
{
    public partial class Index : System.Web.UI.Page
    {
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        public void GetMenus()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
            objCom.Type = "OPTH_CHARTS";
            objCom.DEP_ID = Convert.ToString(Session["HPV_DEP_NAME"]);

            DS = objCom.GetMenus();

            drpSegment.DataSource = DS;
            drpSegment.DataTextField = "MenuAction";
            drpSegment.DataValueField = "MenuAction";
            drpSegment.DataBind();


        }
        [System.Web.Services.WebMethod]
        public static string[] GetFieldR(string prefixText)
        {
            string[] Data;



            EMR_Ophthalmology objOphth = new EMR_Ophthalmology();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND EOPE_R_FIELD Like '%" + prefixText + "%'";

            DS = objOphth.EMR_OpthalExaminationGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["EOPE_R_FIELD"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetFieldL(string prefixText)
        {
            string[] Data;



            EMR_Ophthalmology objOphth = new EMR_Ophthalmology();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND EOPE_L_FIELD Like '%" + prefixText + "%'";

            DS = objOphth.EMR_OpthalExaminationGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["EOPE_L_FIELD"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetIOP_R(string prefixText)
        {
            string[] Data;



            EMR_Ophthalmology objOphth = new EMR_Ophthalmology();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND EOPE_R_IOP Like '%" + prefixText + "%'";

            DS = objOphth.EMR_OpthalExaminationGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["EOPE_R_IOP"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetIOP_L(string prefixText)
        {
            string[] Data;



            EMR_Ophthalmology objOphth = new EMR_Ophthalmology();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND EOPE_L_IOP Like '%" + prefixText + "%'";

            DS = objOphth.EMR_OpthalExaminationGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["EOPE_L_IOP"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetFOM_R(string prefixText)
        {
            string[] Data;



            EMR_Ophthalmology objOphth = new EMR_Ophthalmology();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND EOPE_R_EOM Like '%" + prefixText + "%'";

            DS = objOphth.EMR_OpthalExaminationGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["EOPE_R_EOM"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetFOM_L(string prefixText)
        {
            string[] Data;



            EMR_Ophthalmology objOphth = new EMR_Ophthalmology();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND EOPE_L_EOM Like '%" + prefixText + "%'";

            DS = objOphth.EMR_OpthalExaminationGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["EOPE_L_EOM"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetLids_R(string prefixText)
        {
            string[] Data;



            EMR_Ophthalmology objOphth = new EMR_Ophthalmology();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND EOPE_R_LIDS Like '%" + prefixText + "%'";

            DS = objOphth.EMR_OpthalExaminationGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["EOPE_R_LIDS"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetLids_L(string prefixText)
        {
            string[] Data;



            EMR_Ophthalmology objOphth = new EMR_Ophthalmology();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND EOPE_L_LIDS Like '%" + prefixText + "%'";

            DS = objOphth.EMR_OpthalExaminationGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["EOPE_L_LIDS"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetConjunctiva_R(string prefixText)
        {
            string[] Data;



            EMR_Ophthalmology objOphth = new EMR_Ophthalmology();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND EOPE_R_CONJUNCTIVA Like '%" + prefixText + "%'";

            DS = objOphth.EMR_OpthalExaminationGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["EOPE_R_CONJUNCTIVA"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetConjunctiva_L(string prefixText)
        {
            string[] Data;



            EMR_Ophthalmology objOphth = new EMR_Ophthalmology();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND EOPE_L_CONJUNCTIVA Like '%" + prefixText + "%'";

            DS = objOphth.EMR_OpthalExaminationGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["EOPE_L_CONJUNCTIVA"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetSclera_R(string prefixText)
        {
            string[] Data;



            EMR_Ophthalmology objOphth = new EMR_Ophthalmology();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND EOPE_R_SCLERA Like '%" + prefixText + "%'";

            DS = objOphth.EMR_OpthalExaminationGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["EOPE_R_SCLERA"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetSclera_L(string prefixText)
        {
            string[] Data;



            EMR_Ophthalmology objOphth = new EMR_Ophthalmology();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND EOPE_L_SCLERA Like '%" + prefixText + "%'";

            DS = objOphth.EMR_OpthalExaminationGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["EOPE_L_SCLERA"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }



        [System.Web.Services.WebMethod]
        public static string[] GetCornea_R(string prefixText)
        {
            string[] Data;



            EMR_Ophthalmology objOphth = new EMR_Ophthalmology();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND EOPE_R_CORNEA Like '%" + prefixText + "%'";

            DS = objOphth.EMR_OpthalExaminationGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["EOPE_R_CORNEA"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetCornea_l(string prefixText)
        {
            string[] Data;



            EMR_Ophthalmology objOphth = new EMR_Ophthalmology();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND EOPE_L_CORNEA Like '%" + prefixText + "%'";

            DS = objOphth.EMR_OpthalExaminationGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["EOPE_L_CORNEA"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetAC_R(string prefixText)
        {
            string[] Data;



            EMR_Ophthalmology objOphth = new EMR_Ophthalmology();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND EOPE_R_AC Like '%" + prefixText + "%'";

            DS = objOphth.EMR_OpthalExaminationGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["EOPE_R_AC"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetAC_L(string prefixText)
        {
            string[] Data;



            EMR_Ophthalmology objOphth = new EMR_Ophthalmology();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND EOPE_L_AC Like '%" + prefixText + "%'";

            DS = objOphth.EMR_OpthalExaminationGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["EOPE_L_AC"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetAngle_R(string prefixText)
        {
            string[] Data;



            EMR_Ophthalmology objOphth = new EMR_Ophthalmology();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND EOPE_R_ANGLE Like '%" + prefixText + "%'";

            DS = objOphth.EMR_OpthalExaminationGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["EOPE_R_ANGLE"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetAngle_L(string prefixText)
        {
            string[] Data;



            EMR_Ophthalmology objOphth = new EMR_Ophthalmology();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND EOPE_L_ANGLE Like '%" + prefixText + "%'";

            DS = objOphth.EMR_OpthalExaminationGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["EOPE_L_ANGLE"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }


        [System.Web.Services.WebMethod]
        public static string[] GetIris_R(string prefixText)
        {
            string[] Data;



            EMR_Ophthalmology objOphth = new EMR_Ophthalmology();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND EOPE_R_IRIS Like '%" + prefixText + "%'";

            DS = objOphth.EMR_OpthalExaminationGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["EOPE_R_IRIS"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetIris_L(string prefixText)
        {
            string[] Data;



            EMR_Ophthalmology objOphth = new EMR_Ophthalmology();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND EOPE_L_IRIS Like '%" + prefixText + "%'";

            DS = objOphth.EMR_OpthalExaminationGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["EOPE_L_IRIS"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }


        [System.Web.Services.WebMethod]
        public static string[] GetLens_R(string prefixText)
        {
            string[] Data;



            EMR_Ophthalmology objOphth = new EMR_Ophthalmology();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND EOPE_R_LENS Like '%" + prefixText + "%'";

            DS = objOphth.EMR_OpthalExaminationGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["EOPE_R_LENS"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetLens_L(string prefixText)
        {
            string[] Data;



            EMR_Ophthalmology objOphth = new EMR_Ophthalmology();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND EOPE_L_LENS Like '%" + prefixText + "%'";

            DS = objOphth.EMR_OpthalExaminationGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["EOPE_L_LENS"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetVitreous_R(string prefixText)
        {
            string[] Data;



            EMR_Ophthalmology objOphth = new EMR_Ophthalmology();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND EOPE_R_VITREOUS Like '%" + prefixText + "%'";

            DS = objOphth.EMR_OpthalExaminationGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["EOPE_R_VITREOUS"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetVitreous_L(string prefixText)
        {
            string[] Data;



            EMR_Ophthalmology objOphth = new EMR_Ophthalmology();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND EOPE_L_VITREOUS Like '%" + prefixText + "%'";

            DS = objOphth.EMR_OpthalExaminationGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["EOPE_L_VITREOUS"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }


        [System.Web.Services.WebMethod]
        public static string[] GetOpticNerve_R(string prefixText)
        {
            string[] Data;



            EMR_Ophthalmology objOphth = new EMR_Ophthalmology();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND EOPE_R_OPTICNERVE Like '%" + prefixText + "%'";

            DS = objOphth.EMR_OpthalExaminationGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["EOPE_R_OPTICNERVE"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetOpticNerve_L(string prefixText)
        {
            string[] Data;



            EMR_Ophthalmology objOphth = new EMR_Ophthalmology();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND EOPE_L_OPTICNERVE Like '%" + prefixText + "%'";

            DS = objOphth.EMR_OpthalExaminationGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["EOPE_L_OPTICNERVE"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetChoroid_R(string prefixText)
        {
            string[] Data;



            EMR_Ophthalmology objOphth = new EMR_Ophthalmology();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND EOPE_R_CHOROID Like '%" + prefixText + "%'";

            DS = objOphth.EMR_OpthalExaminationGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["EOPE_R_CHOROID"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetChoroid_L(string prefixText)
        {
            string[] Data;



            EMR_Ophthalmology objOphth = new EMR_Ophthalmology();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND EOPE_L_CHOROID Like '%" + prefixText + "%'";

            DS = objOphth.EMR_OpthalExaminationGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["EOPE_L_CHOROID"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetReinalNPDR_R(string prefixText)
        {
            string[] Data;



            EMR_Ophthalmology objOphth = new EMR_Ophthalmology();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND EOPE_R_NPDR Like '%" + prefixText + "%'";

            DS = objOphth.EMR_OpthalExaminationGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["EOPE_R_NPDR"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetReinalNPDR_L(string prefixText)
        {
            string[] Data;



            EMR_Ophthalmology objOphth = new EMR_Ophthalmology();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND EOPE_L_NPDR Like '%" + prefixText + "%'";

            DS = objOphth.EMR_OpthalExaminationGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["EOPE_L_NPDR"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GettReinalCSME_R(string prefixText)
        {
            string[] Data;



            EMR_Ophthalmology objOphth = new EMR_Ophthalmology();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND EOPE_R_CSME Like '%" + prefixText + "%'";

            DS = objOphth.EMR_OpthalExaminationGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["EOPE_R_CSME"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GettReinalCSME_L(string prefixText)
        {
            string[] Data;



            EMR_Ophthalmology objOphth = new EMR_Ophthalmology();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND EOPE_L_CSME Like '%" + prefixText + "%'";

            DS = objOphth.EMR_OpthalExaminationGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["EOPE_L_CSME"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }


        [System.Web.Services.WebMethod]
        public static string[] GetGonioscopy_R(string prefixText)
        {
            string[] Data;



            EMR_Ophthalmology objOphth = new EMR_Ophthalmology();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND EOPE_R_GONIOSCOPY Like '%" + prefixText + "%'";

            DS = objOphth.EMR_OpthalExaminationGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["EOPE_R_GONIOSCOPY"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetGonioscopy_L(string prefixText)
        {
            string[] Data;



            EMR_Ophthalmology objOphth = new EMR_Ophthalmology();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND EOPE_L_GONIOSCOPY Like '%" + prefixText + "%'";

            DS = objOphth.EMR_OpthalExaminationGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["EOPE_L_GONIOSCOPY"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }


        [System.Web.Services.WebMethod]
        public static string[] GetPupil_R(string prefixText)
        {
            string[] Data;



            EMR_Ophthalmology objOphth = new EMR_Ophthalmology();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND EOPE_R_PUPIL Like '%" + prefixText + "%'";

            DS = objOphth.EMR_OpthalExaminationGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["EOPE_R_PUPIL"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetPupil_L(string prefixText)
        {
            string[] Data;



            EMR_Ophthalmology objOphth = new EMR_Ophthalmology();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND EOPE_L_PUPIL Like '%" + prefixText + "%'";

            DS = objOphth.EMR_OpthalExaminationGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["EOPE_L_PUPIL"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }


        [System.Web.Services.WebMethod]
        public static string[] GettRefractionRemarks(string prefixText)
        {
            string[] Data;

            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND EOP_REMARKS Like '%" + prefixText + "%'";

            DS = objCom.fnGetFieldValue("EOP_REMARKS", "EMR_OPTHALMOLOGY_POWERDETAILS", Criteria, "EOP_REMARKS");
            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["EOP_REMARKS"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../../Default.aspx?NoSession=1"); }

            if (!IsPostBack)
            {

                if (Convert.ToString(Session["EPM_STATUS"]).ToUpper() == "Y" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                {
                    btnSave.Visible = false;
                }
                //HIDE THE SAVE OPTION FOR NEXT DAY OF THE VISIT
                if (Convert.ToString(Session["EMRCurrentDateDiff"]) != "0" && Convert.ToString(Session["EMR_EDIT_DATA_AFTER_COMPLETE"]) == "0" && Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF")
                {
                    btnSave.Visible = false;
                }
                GetMenus();
                if (drpSegment.Items.Count > 0)
                {
                    pnlOphthalmologyChart.SegmentType = drpSegment.SelectedValue;
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                pnlOphthalExamination.SaveOphthExam();
                pnlOpticals.SaveOpticals();




                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowMessage()", true);
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "       Pediatric.Index.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void drpSegment_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                pnlOphthalmologyChart.SegmentType = drpSegment.SelectedValue;
                pnlOphthalmologyChart.BindPediatricImage();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "       Pediatric.drpSegment_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }


    }
}