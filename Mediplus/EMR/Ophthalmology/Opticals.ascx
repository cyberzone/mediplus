﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Opticals.ascx.cs" Inherits="Mediplus.EMR.Ophthalmology.Opticals" %>
<%@ register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>


 

<link href="../Styles/style.css" rel="stylesheet" type="text/css" />

<link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
<link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />

<link href="../Styles/MenuStyle.css" rel="stylesheet" type="text/css" />
   <script src="../../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-1.7.1.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery.caret.1.02.min.js" type="text/javascript"></script>

 


 

<script>
    $(function () {
        $(".spinner").spinner({ step: 0.01 });
        var step = $(".spinner").spinner("option", "step");
        $(".spinner").spinner("option", "step", 0.01);
    });
</script>
<style>
    .align
    {
        text-align: center !important;
    }

    .spinner
    {
        width: 40px;
    }

    #Refractionrighteye
    {
        border: 1px solid #dcdcdc;
    }

    #Refractionlefteye
    {
        border: 1px solid #dcdcdc;
    }

    #CycloRefractionRight
    {
        border: 1px solid #dcdcdc;
    }

    #CycloRefractionLeft
    {
        border: 1px solid #dcdcdc;
    }
</style>

<script  language="javascript"  type="text/javascript" >
    function OnlyNumeric1(evt) {
        var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
        if (chCode >= 48 && chCode <= 57 ||
             chCode == 46 || chCode == 45) {
            return true;
        }
        else {
            return false;
        }
    }

    function Increase(vName,Action) {
        //alert(document.getElementById(vName).value);

        var vData;
        var Result;
        if (Action == "+") {
            Result = parseFloat(0.01);
        }
        else {
            Result = parseFloat(-0.01);
        }

        if (vName == "txtrdsph") {
            vData = document.getElementById('<%=txtrdsph.ClientID %>').value;
            if (vData != '') {

                if (Action == "+") {
                    Result = parseFloat(vData) + parseFloat(0.01);
                }
                else {
                    Result = parseFloat(vData) - parseFloat(0.01);
                }

            }
            document.getElementById('<%=txtrdsph.ClientID %>').value = Result.toFixed(2)

        }
        else if (vName == "txtrdcyl") {
            vData = document.getElementById('<%=txtrdcyl.ClientID %>').value;
            if (vData != '') {
                if (Action == "+") {
                    Result = parseFloat(vData) + parseFloat(0.01);
                }
                else {
                    Result = parseFloat(vData) - parseFloat(0.01);
                }

            }
            document.getElementById('<%=txtrdcyl.ClientID %>').value = Result.toFixed(2)
        }
        else if (vName == "txtrdaxis") {
            vData = document.getElementById('<%=txtrdaxis.ClientID %>').value;
            if (vData != '') {
                if (Action == "+") {
                    Result = parseFloat(vData) + parseFloat(0.01);
                }
                else {
                    Result = parseFloat(vData) - parseFloat(0.01);
                }

            }
            document.getElementById('<%=txtrdaxis.ClientID %>').value = Result.toFixed(2)
        }
        else if (vName == "txtldsph") {
            vData = document.getElementById('<%=txtldsph.ClientID %>').value;
            if (vData != '') {
                if (Action == "+") {
                    Result = parseFloat(vData) + parseFloat(0.01);
                }
                else {
                    Result = parseFloat(vData) - parseFloat(0.01);
                }

            }
            document.getElementById('<%=txtldsph.ClientID %>').value = Result.toFixed(2)
        }
        else if (vName == "txtldcyl") {
            vData = document.getElementById('<%=txtldcyl.ClientID %>').value;
            if (vData != '') {
                if (Action == "+") {
                    Result = parseFloat(vData) + parseFloat(0.01);
                }
                else {
                    Result = parseFloat(vData) - parseFloat(0.01);
                }

            }
            document.getElementById('<%=txtldcyl.ClientID %>').value = Result.toFixed(2)
        }
        else if (vName == "txtldaxis") {
            vData = document.getElementById('<%=txtldaxis.ClientID %>').value;
            if (vData != '') {
                if (Action == "+") {
                    Result = parseFloat(vData) + parseFloat(0.01);
                }
                else {
                    Result = parseFloat(vData) - parseFloat(0.01);
                }

            }
            document.getElementById('<%=txtldaxis.ClientID %>').value = Result.toFixed(2)
         }
        else if (vName == "txtrnsph") {
            vData = document.getElementById('<%=txtrnsph.ClientID %>').value;
            if (vData != '') {
                if (Action == "+") {
                    Result = parseFloat(vData) + parseFloat(0.01);
                }
                else {
                    Result = parseFloat(vData) - parseFloat(0.01);
                }

            }
            document.getElementById('<%=txtrnsph.ClientID %>').value = Result.toFixed(2)
        }
        else if (vName == "txtrncyl") {
            vData = document.getElementById('<%=txtrncyl.ClientID %>').value;
            if (vData != '') {
                if (Action == "+") {
                    Result = parseFloat(vData) + parseFloat(0.01);
                }
                else {
                    Result = parseFloat(vData) - parseFloat(0.01);
                }

            }
            document.getElementById('<%=txtrncyl.ClientID %>').value = Result.toFixed(2)
        }
        else if (vName == "txtrnaxis") {
            vData = document.getElementById('<%=txtrnaxis.ClientID %>').value;
            if (vData != '') {
                if (Action == "+") {
                    Result = parseFloat(vData) + parseFloat(0.01);
                }
                else {
                    Result = parseFloat(vData) - parseFloat(0.01);
                }

            }
            document.getElementById('<%=txtrnaxis.ClientID %>').value = Result.toFixed(2)
         }
        else if (vName == "txtlnsph") {
            vData = document.getElementById('<%=txtlnsph.ClientID %>').value;
            if (vData != '') {
                if (Action == "+") {
                    Result = parseFloat(vData) + parseFloat(0.01);
                }
                else {
                    Result = parseFloat(vData) - parseFloat(0.01);
                }

            }
            document.getElementById('<%=txtlnsph.ClientID %>').value = Result.toFixed(2)
        }
        else if (vName == "txtlncyl") {
            vData = document.getElementById('<%=txtlncyl.ClientID %>').value;
            if (vData != '') {
                if (Action == "+") {
                    Result = parseFloat(vData) + parseFloat(0.01);
                }
                else {
                    Result = parseFloat(vData) - parseFloat(0.01);
                }

            }
            document.getElementById('<%=txtlncyl.ClientID %>').value = Result.toFixed(2)
        }
        else if (vName == "txtlnaxis") {
            vData = document.getElementById('<%=txtlnaxis.ClientID %>').value;
            if (vData != '') {
                if (Action == "+") {
                    Result = parseFloat(vData) + parseFloat(0.01);
                }
                else {
                    Result = parseFloat(vData) - parseFloat(0.01);
                }

            }
            document.getElementById('<%=txtlnaxis.ClientID %>').value = Result.toFixed(2)
        }


    }

    function Decrease(vName) {

        var vData = document.getElementById(vName).value;
        if (vData != '') {
            var Result = '';
            Result = parseFloat(vData) - parseFloat(0.01);
            document.getElementById(vName).value = Result.toFixed(2)

        }
        else {
            Result = parseFloat(0) - parseFloat(0.01);
            document.getElementById(vName).value = Result.toFixed(2)
        }



    }
</script>

<div>
    <div id="Refractionrighteye">
        <table class="table grid spacy"  border="1" style=" border: thin; border-color:#dcdcdc; border-style: groove;" cellpadding="0" cellspacing="0" >
           
                <tr>
                    <td></td>
                    <td colspan="3" class="GridHeader_Gray"  ><span class="GridHeader_Gray" style="font-weight:bold;">RIGHT EYE</span></td>
                    <td colspan="3" class="GridHeader_Gray"  ><span class="GridHeader_Gray" style="font-weight:bold;">LEFT EYE</span></td>
                </tr>
                
                <tr>
                    <td></td>
                    <td class="GridHeader_Gray" ><span class="GridHeader_Gray" style="font-weight:bold;">Sph </span></td>
                    <td class="GridHeader_Gray" ><span class="GridHeader_Gray" style="font-weight:bold;">Cyl </span></td>
                    <td class="GridHeader_Gray" ><span class="GridHeader_Gray" style="font-weight:bold;">Axis </span></td>
                    <td class="GridHeader_Gray" ><span class="GridHeader_Gray" style="font-weight:bold;">Sph </span></td>
                    <td class="GridHeader_Gray" > <span class="GridHeader_Gray" style="font-weight:bold;">Cyl </span></td>
                    <td class="GridHeader_Gray" >  <span class="GridHeader_Gray" style="font-weight:bold;">Axis </span></td>

                </tr>
             
           
                <tr>
                    <td class="lblCaption1"  style="text-align:left;" >Distance:</td>
                    <td >
                         <table>
                            <tr>
                                <td style="text-align: right;">
                                    <input type="text"  id="txtrdsph"  runat="server"  onkeypress="return OnlyNumeric1(event);" class="lblCaption1" style="width:50px;height:20px;" />
                                </td>
                                <td style="text-align: left;">
                                    <img id="img1" src="../Images/sort_asc.png" onclick="Increase('txtrdsph','+');" /><br />
                                    <img id="img2" src="../Images/sort_desc.png" onclick="Increase('txtrdsph','-');" />
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                          <table>
                            <tr>
                                <td style="text-align: right;">
                                    <input type="text"  id="txtrdcyl" runat="server"    onkeypress="return OnlyNumeric1(event);" class="lblCaption1" style="width:50px;height:20px;"/>
                                </td>
                                <td style="text-align: left;">
                                    <img id="img3" src="../Images/sort_asc.png" onclick="Increase('txtrdcyl','+');" /><br />
                                    <img id="img4" src="../Images/sort_desc.png" onclick="Increase('txtrdcyl','-');" />
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                         <table>
                            <tr>
                                <td style="text-align: right;">
                                    <input type="text"  id="txtrdaxis"  runat="server"  onkeypress="return OnlyNumeric1(event);" class="lblCaption1" style="width:50px;height:20px;"  />
                                </td>
                                <td style="text-align: left;">
                                    <img id="img5" src="../Images/sort_asc.png" onclick="Increase('txtrdaxis','+');" /><br />
                                    <img id="img6" src="../Images/sort_desc.png" onclick="Increase('txtrdaxis','-');" />
                                </td>
                            </tr>
                        </table>

                    </td>
                    <td>
                        <table>
                            <tr>
                                <td style="text-align: right;">
                                    <input type="text"  id="txtldsph" runat="server"  onkeypress="return OnlyNumeric1(event);" class="lblCaption1" style="width:50px;height:20px;"  />
                                </td>
                                <td style="text-align: left;">
                                    <img id="img7" src="../Images/sort_asc.png" onclick="Increase('txtldsph','+');" /><br />
                                    <img id="img8" src="../Images/sort_desc.png" onclick="Increase('txtldsph','-');" />
                                </td>
                            </tr>
                        </table>

                    </td>
                    <td>
                      
                         <table>
                            <tr>
                                <td style="text-align: right;">
                                    <input type="text"  id="txtldcyl" runat="server"  onkeypress="return OnlyNumeric1(event);" class="lblCaption1" style="width:50px;height:20px;"  />
                                </td>
                                <td style="text-align: left;">
                                    <img id="img9" src="../Images/sort_asc.png" onclick="Increase('txtldcyl','+');" /><br />
                                    <img id="img10" src="../Images/sort_desc.png" onclick="Increase('txtldcyl','-');" />
                                </td>
                            </tr>
                        </table>

                    </td>
                    <td>
                         <table>
                            <tr>
                                <td style="text-align: right;">
                                    <input type="text"  id="txtldaxis" runat="server"  onkeypress="return OnlyNumeric1(event);" class="lblCaption1" style="width:50px;height:20px;"  />
                                </td>
                                <td style="text-align: left;">
                                    <img id="img11" src="../Images/sort_asc.png" onclick="Increase('txtldaxis','+');" /><br />
                                    <img id="img12" src="../Images/sort_desc.png" onclick="Increase('txtldaxis','-');" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1"  style="text-align:left;">Near:</td>
                    <td>
                          <table>
                            <tr>
                                <td style="text-align: right;">
                                    <input type="text"  id="txtrnsph" runat="server"  onkeypress="return OnlyNumeric1(event);" class="lblCaption1" style="width:50px;height:20px;" />
                                </td>
                                <td style="text-align: left;">
                                    <img id="img13" src="../Images/sort_asc.png" onclick="Increase('txtrnsph','+');" /><br />
                                    <img id="img14" src="../Images/sort_desc.png" onclick="Increase('txtrnsph','-');" />
                                </td>
                            </tr>
                        </table>

                    </td>
                    <td>
                         <table>
                            <tr>
                                <td style="text-align: right;">
                                    <input type="text"  id="txtrncyl"  runat="server" onkeypress="return OnlyNumeric1(event);" class="lblCaption1" style="width:50px;height:20px;"  />
                                </td>
                                <td style="text-align: left;">
                                    <img id="img15" src="../Images/sort_asc.png" onclick="Increase('txtrncyl','+');" /><br />
                                    <img id="img16" src="../Images/sort_desc.png" onclick="Increase('txtrncyl','-');" />
                                </td>
                            </tr>
                        </table>

                    </td>
                    <td>
                         <table>
                            <tr>
                                <td style="text-align: right;">
                                    <input type="text"  id="txtrnaxis" runat="server"  onkeypress="return OnlyNumeric1(event);" class="lblCaption1" style="width:50px;height:20px;"  />
                                </td>
                                <td style="text-align: left;">
                                    <img id="img17" src="../Images/sort_asc.png" onclick="Increase('txtrnaxis','+');" /><br />
                                    <img id="img18" src="../Images/sort_desc.png" onclick="Increase('txtrnaxis','-');" />
                                </td>
                            </tr>
                        </table>

                    </td>
                    <td>
                         <table>
                            <tr>
                                <td style="text-align: right;">
                                    <input type="text"  id="txtlnsph" runat="server"  onkeypress="return OnlyNumeric1(event);" class="lblCaption1" style="width:50px;height:20px;"  />
                                </td>
                                <td style="text-align: left;">
                                    <img id="img19" src="../Images/sort_asc.png" onclick="Increase('txtlnsph','+');" /><br />
                                    <img id="img20" src="../Images/sort_desc.png" onclick="Increase('txtlnsph','-');" />
                                </td>
                            </tr>
                        </table>

                    </td>
                    <td>
                         <table>
                            <tr>
                                <td style="text-align: right;">
                                    <input type="text"  id="txtlncyl" runat="server"  onkeypress="return OnlyNumeric1(event);" class="lblCaption1" style="width:50px;height:20px;" />
                                </td>
                                <td style="text-align: left;">
                                    <img id="img21" src="../Images/sort_asc.png" onclick="Increase('txtlncyl','+');" /><br />
                                    <img id="img22" src="../Images/sort_desc.png" onclick="Increase('txtlncyl','-');"/>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td>
                       <table>
                            <tr>
                                <td style="text-align: right;">
                                    <input type="text"  id="txtlnaxis" runat="server"  onkeypress="return OnlyNumeric1(event);" class="lblCaption1" style="width:50px;height:20px;"  />
                                </td>
                                <td style="text-align: left;">
                                    <img id="img23" src="../Images/sort_asc.png" onclick="Increase('txtlnaxis','+');" /><br />
                                    <img id="img24" src="../Images/sort_desc.png" onclick="Increase('txtlnaxis','-');" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            
        </table>
    </div>
    <div style="padding: 10px 0 0 0;">&nbsp;</div>
    <div id="remarksdiv">
        <span class="lblCaption1" style="font-weight:bold;">Remarks</span>:
       
         <asp:TextBox ID="txtRemarks" runat="server"  style="width: 100%" class="lblCaption1"  Height="50px" TextMode="MultiLine" ></asp:TextBox>
        <asp:autocompleteextender id="AutoCompleteExtender1" runat="Server" targetcontrolid="txtRemarks" minimumprefixlength="1" ServicePath="~/Ophthalmology/Index.aspx"  servicemethod="GettRefractionRemarks"
                        completionlistcssclass="AutoExtender" completionlistitemcssclass="AutoExtenderList" completionlisthighlighteditemcssclass="AutoExtenderHighlight" CompletionInterval="10" CompletionSetCount="15" ></asp:autocompleteextender>

    </div>

</div>
