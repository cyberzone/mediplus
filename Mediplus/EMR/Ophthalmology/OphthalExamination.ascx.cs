﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_BAL;

namespace Mediplus.EMR.Ophthalmology
{
    public partial class OphthalExamination : System.Web.UI.UserControl
    {
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindData()
        {


            EMR_Ophthalmology objOphth = new EMR_Ophthalmology();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND EOPE_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND EOPE_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

            DS = objOphth.EMR_OpthalExaminationGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {

                txtVisualAcuity_UCVA_R.Value = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_UCVA"]);
                txtVisualAcuity_PH_R.Value = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_PH"]);
                txtVisualAcuity_BCVA_R.Value = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_BCVA"]);
                txtField_R.Text = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_FIELD"]);
                txtIOP_R.Text = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_IOP"]);
                txtEOM_R.Text = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_EOM"]);
                txtLids_R.Text = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_LIDS"]);
                txtConjunctiva_R.Text = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_CONJUNCTIVA"]);
                txtSclera_R.Text = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_SCLERA"]);
                txtCornea_R.Text = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_CORNEA"]);
                txtAC_R.Text = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_AC"]);
                txtAngle_R.Text = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_ANGLE"]);
                txtIris_R.Text = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_IRIS"]);
                txtLens_R.Text = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_LENS"]);
                txtVitreous_R.Text = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_VITREOUS"]);
                txtOpticNerve_R.Text = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_OPTICNERVE"]);
                txtChoroid_R.Text = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_CHOROID"]);

                txtVisualAcuity_UCVA_L.Value = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_UCVA"]);
                txtVisualAcuity_PH_L.Value = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_PH"]);
                txtVisualAcuity_BCVA_L.Value = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_BCVA"]);
                txtField_L.Text = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_FIELD"]);
                txtIOP_L.Text = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_IOP"]);
                txtEOM_L.Text = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_EOM"]);
                txtLids_L.Text = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_LIDS"]);
                txtConjunctiva_L.Text = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_CONJUNCTIVA"]);
                txtSclera_L.Text = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_SCLERA"]);
                txtCornea_L.Text = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_CORNEA"]);
                txtAC_L.Text = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_AC"]);
                txtAngle_L.Text = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_ANGLE"]);
                txtIris_L.Text = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_IRIS"]);
                txtLens_L.Text = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_LENS"]);
                txtVitreous_L.Text = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_VITREOUS"]);
                txtOpticNerve_L.Text = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_OPTICNERVE"]);
                txtChoroid_L.Text = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_CHOROID"]);


                txtOCT.Value = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_OCT"]);
                txtFA.Value = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_FA"]);
                txtField.Value = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_FIELD"]);

                txtReinalNPDR_R.Text = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_NPDR"]);
                txtReinalCSME_R.Text = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_CSME"]);
                txtReinalNPDR_L.Text = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_NPDR"]);
                txtReinalCSME_L.Text = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_CSME"]);


                // txtVisualAcuity_BCVA_R_PG.Value = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_BCVA_PG"]);
                txtVisualAcuity_COLORVISION_R.Value = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_COLORVISION"]);
                // txtVisualAcuity_BCVA_L_PG.Value = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_BCVA_PG"]);
                txtVisualAcuity_COLORVISION_L.Value = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_COLORVISION"]);






                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_FIELD_STATUS"]).ToLower() == "normal")
                {
                    radField_R_Normal.Checked = true;
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_FIELD_STATUS"]).ToLower() == "abnormal")
                {
                    radField_R_Abnormal.Checked = true;
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_IOP_STATUS"]).ToLower() == "normal")
                {
                    radIOP_R_Normal.Checked = true;
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_IOP_STATUS"]).ToLower() == "abnormal")
                {
                    radIOP_R_Abnormal.Checked = true;
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_EOM_STATUS"]).ToLower() == "normal")
                {
                    radEOM_R_Normal.Checked = true;
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_EOM_STATUS"]).ToLower() == "abnormal")
                {
                    radEOM_R_Abnormal.Checked = true;
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_LIDS_STATUS"]).ToLower() == "normal")
                {
                    radLids_R_Normal.Checked = true;
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_LIDS_STATUS"]).ToLower() == "abnormal")
                {
                    radLids_R_Abnormal.Checked = true;
                }


                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_CONJUNCTIVA_STATUS"]).ToLower() == "normal")
                {
                    radConjunctiva_R_Normal.Checked = true;
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_CONJUNCTIVA_STATUS"]).ToLower() == "abnormal")
                {
                    radConjunctiva_R_Abnormal.Checked = true;
                }


                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_SCLERA_STATUS"]).ToLower() == "normal")
                {
                    radSclera_R_Normal.Checked = true;
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_SCLERA_STATUS"]).ToLower() == "abnormal")
                {
                    radSclera_R_Abnormal.Checked = true;
                }


                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_CORNEA_STATUS"]).ToLower() == "normal")
                {
                    radCornea_R_Normal.Checked = true;
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_CORNEA_STATUS"]).ToLower() == "abnormal")
                {
                    radCornea_R_Abnormal.Checked = true;
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_AC_STATUS"]).ToLower() == "normal")
                {
                    radAC_R_Normal.Checked = true;
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_AC_STATUS"]).ToLower() == "abnormal")
                {
                    radAC_R_Abnormal.Checked = true;
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_ANGLE_STATUS"]).ToLower() == "normal")
                {
                    radAngle_R_Normal.Checked = true;
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_ANGLE_STATUS"]).ToLower() == "abnormal")
                {
                    radAngle_R_Abnormal.Checked = true;
                }


                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_IRIS_STATUS"]).ToLower() == "normal")
                {
                    radIris_R_Normal.Checked = true;
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_IRIS_STATUS"]).ToLower() == "abnormal")
                {
                    radIris_R_Abnormal.Checked = true;
                }



                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_LENS_STATUS"]).ToLower() == "normal")
                {
                    radLens_R_Normal.Checked = true;
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_LENS_STATUS"]).ToLower() == "abnormal")
                {
                    radLens_R_Abnormal.Checked = true;
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_VITREOUS_STATUS"]).ToLower() == "normal")
                {
                    radVitreous_R_Normal.Checked = true;
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_VITREOUS_STATUS"]).ToLower() == "abnormal")
                {
                    radVitreous_R_Abnormal.Checked = true;
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_OPTICNERVE_STATUS"]).ToLower() == "normal")
                {
                    radOpticNerve_R_Normal.Checked = true;
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_OPTICNERVE_STATUS"]).ToLower() == "abnormal")
                {
                    radOpticNerve_R_Abnormal.Checked = true;
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_CHOROID_STATUS"]).ToLower() == "normal")
                {
                    radChoroid_R_Normal.Checked = true;
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_CHOROID_STATUS"]).ToLower() == "abnormal")
                {
                    radChoroid_R_Abnormal.Checked = true;
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_CHOROID_STATUS"]).ToLower() == "normal")
                {
                    radChoroid_R_Normal.Checked = true;
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_CHOROID_STATUS"]).ToLower() == "abnormal")
                {
                    radChoroid_R_Abnormal.Checked = true;
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_NPDR_STATUS"]).ToLower() == "normal")
                {
                    radReinalNPDR_R_Normal.Checked = true;
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_NPDR_STATUS"]).ToLower() == "abnormal")
                {
                    radReinalNPDR_R_Abnormal.Checked = true;
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_CSME_STATUS"]).ToLower() == "normal")
                {
                    radReinalCSME_R_Normal.Checked = true;
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_CSME_STATUS"]).ToLower() == "abnormal")
                {
                    radReinalCSME_R_Abnormal.Checked = true;
                }







                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_FIELD_STATUS"]).ToLower() == "normal")
                {
                    radField_L_Normal.Checked = true;
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_FIELD_STATUS"]).ToLower() == "abnormal")
                {
                    radField_L_Abnormal.Checked = true;
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_IOP_STATUS"]).ToLower() == "normal")
                {
                    radIOP_L_Normal.Checked = true;
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_IOP_STATUS"]).ToLower() == "abnormal")
                {
                    radIOP_L_Abnormal.Checked = true;
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_EOM_STATUS"]).ToLower() == "normal")
                {
                    radEOM_L_Normal.Checked = true;
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_EOM_STATUS"]).ToLower() == "abnormal")
                {
                    radEOM_L_Abnormal.Checked = true;
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_LIDS_STATUS"]).ToLower() == "normal")
                {
                    radLids_L_Normal.Checked = true;
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_LIDS_STATUS"]).ToLower() == "abnormal")
                {
                    radLids_L_Abnormal.Checked = true;
                }


                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_CONJUNCTIVA_STATUS"]).ToLower() == "normal")
                {
                    radConjunctiva_L_Normal.Checked = true;
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_CONJUNCTIVA_STATUS"]).ToLower() == "abnormal")
                {
                    radConjunctiva_L_Abnormal.Checked = true;
                }


                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_SCLERA_STATUS"]).ToLower() == "normal")
                {
                    radSclera_L_Normal.Checked = true;
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_SCLERA_STATUS"]).ToLower() == "abnormal")
                {
                    radSclera_L_Abnormal.Checked = true;
                }


                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_CORNEA_STATUS"]).ToLower() == "normal")
                {
                    radCornea_L_Normal.Checked = true;
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_CORNEA_STATUS"]).ToLower() == "abnormal")
                {
                    radCornea_L_Abnormal.Checked = true;
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_AC_STATUS"]).ToLower() == "normal")
                {
                    radAC_L_Normal.Checked = true;
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_AC_STATUS"]).ToLower() == "abnormal")
                {
                    radAC_L_Abnormal.Checked = true;
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_ANGLE_STATUS"]).ToLower() == "normal")
                {
                    radAngle_L_Normal.Checked = true;
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_ANGLE_STATUS"]).ToLower() == "abnormal")
                {
                    radAngle_L_Abnormal.Checked = true;
                }


                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_IRIS_STATUS"]).ToLower() == "normal")
                {
                    radIris_L_Normal.Checked = true;
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_IRIS_STATUS"]).ToLower() == "abnormal")
                {
                    radIris_L_Abnormal.Checked = true;
                }



                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_LENS_STATUS"]).ToLower() == "normal")
                {
                    radLens_L_Normal.Checked = true;
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_LENS_STATUS"]).ToLower() == "abnormal")
                {
                    radLens_L_Abnormal.Checked = true;
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_VITREOUS_STATUS"]).ToLower() == "normal")
                {
                    radVitreous_L_Normal.Checked = true;
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_VITREOUS_STATUS"]).ToLower() == "abnormal")
                {
                    radVitreous_L_Abnormal.Checked = true;
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_OPTICNERVE_STATUS"]).ToLower() == "normal")
                {
                    radOpticNerve_L_Normal.Checked = true;
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_OPTICNERVE_STATUS"]).ToLower() == "abnormal")
                {
                    radOpticNerve_L_Abnormal.Checked = true;
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_CHOROID_STATUS"]).ToLower() == "normal")
                {
                    radChoroid_L_Normal.Checked = true;
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_CHOROID_STATUS"]).ToLower() == "abnormal")
                {
                    radChoroid_L_Abnormal.Checked = true;
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_CHOROID_STATUS"]).ToLower() == "normal")
                {
                    radChoroid_L_Normal.Checked = true;
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_CHOROID_STATUS"]).ToLower() == "abnormal")
                {
                    radChoroid_L_Abnormal.Checked = true;
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_NPDR_STATUS"]).ToLower() == "normal")
                {
                    radReinalNPDR_L_Normal.Checked = true;
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_NPDR_STATUS"]).ToLower() == "abnormal")
                {
                    radReinalNPDR_L_Abnormal.Checked = true;
                }

                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_CSME_STATUS"]).ToLower() == "normal")
                {
                    radReinalCSME_L_Normal.Checked = true;
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_CSME_STATUS"]).ToLower() == "abnormal")
                {
                    radReinalCSME_L_Abnormal.Checked = true;
                }




                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_BCVA_TYPE"]).ToLower() == "with glass")
                {
                    radVisualAcuity_BCVA_R_Glass.Checked = true;
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_BCVA_TYPE"]).ToLower() == "with contact lense")
                {
                    radVisualAcuity_BCVA_R_ContLen.Checked = true;
                }




                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_BCVA_TYPE"]).ToLower() == "with glass")
                {
                    radVisualAcuity_BCVA_L_Glass.Checked = true;
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_BCVA_TYPE"]).ToLower() == "with contact lense")
                {
                    radVisualAcuity_BCVA_L_ContLen.Checked = true;
                }






                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_NPDR_TYPE"]).ToLower() == "dilated")
                {
                    radReinalNPDR_R_Dilated.Checked = true;
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_NPDR_TYPE"]).ToLower() == "undilated")
                {
                    radReinalNPDR_R_Undilated.Checked = true;
                }


                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_NPDR_TYPE"]).ToLower() == "dilated")
                {
                    radReinalNPDR_L_Dilated.Checked = true;
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_NPDR_TYPE"]).ToLower() == "undilated")
                {
                    radReinalNPDR_L_Undilated.Checked = true;
                }

                txtGonioscopy_R.Text = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_GONIOSCOPY"]);
                txtGonioscopy_L.Text = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_GONIOSCOPY"]);


                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_GONIOSCOPY_STATUS"]).ToLower() == "normal")
                {
                    radGonioscopy_R_Normal.Checked = true;
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_GONIOSCOPY_STATUS"]).ToLower() == "abnormal")
                {
                    radGonioscopy_R_Abnormal.Checked = true;
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_GONIOSCOPY_STATUS"]).ToLower() == "normal")
                {
                    radGonioscopy_L_Normal.Checked = true;
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_GONIOSCOPY_STATUS"]).ToLower() == "abnormal")
                {
                    radGonioscopy_L_Abnormal.Checked = true;
                }



                txtPupil_R.Text = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_PUPIL"]);
                txtPupil_L.Text = Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_PUPIL"]);


                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_PUPIL_STATUS"]).ToLower() == "normal")
                {
                    radPupil_R_Normal.Checked = true;
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_R_PUPIL_STATUS"]).ToLower() == "abnormal")
                {
                    radPupil_R_Abnormal.Checked = true;
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_PUPIL_STATUS"]).ToLower() == "normal")
                {
                    radPupil_L_Normal.Checked = true;
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["EOPE_L_PUPIL_STATUS"]).ToLower() == "abnormal")
                {
                    radPupil_L_Abnormal.Checked = true;
                }

            }
        }

        public void SaveOphthExam()
        {
            EMR_Ophthalmology objOphth = new EMR_Ophthalmology();
            objOphth.branchid = Convert.ToString(Session["Branch_ID"]);
            objOphth.patientmasterid = Convert.ToString(Session["EMR_ID"]);

            objOphth.EOPE_R_UCVA = txtVisualAcuity_UCVA_R.Value;
            objOphth.EOPE_R_PH = txtVisualAcuity_PH_R.Value;
            objOphth.EOPE_R_BCVA = txtVisualAcuity_BCVA_R.Value;
            objOphth.EOPE_R_FIELD = txtField_R.Text;
            objOphth.EOPE_R_IOP = txtIOP_R.Text;
            objOphth.EOPE_R_EOM = txtEOM_R.Text;
            objOphth.EOPE_R_LIDS = txtLids_R.Text;
            objOphth.EOPE_R_CONJUNCTIVA = txtConjunctiva_R.Text;
            objOphth.EOPE_R_SCLERA = txtSclera_R.Text;
            objOphth.EOPE_R_CORNEA = txtCornea_R.Text;
            objOphth.EOPE_R_AC = txtAC_R.Text;
            objOphth.EOPE_R_ANGLE = txtAngle_R.Text;
            objOphth.EOPE_R_IRIS = txtIris_R.Text;
            objOphth.EOPE_R_LENS = txtLens_R.Text;
            objOphth.EOPE_R_VITREOUS = txtVitreous_R.Text;
            objOphth.EOPE_R_OPTICNERVE = txtOpticNerve_R.Text;
            objOphth.EOPE_R_CHOROID = txtChoroid_R.Text;
            objOphth.EOPE_L_UCVA = txtVisualAcuity_UCVA_L.Value;
            objOphth.EOPE_L_PH = txtVisualAcuity_PH_L.Value;
            objOphth.EOPE_L_BCVA = txtVisualAcuity_BCVA_L.Value;


            if (radVisualAcuity_BCVA_R_Glass.Checked == true)
            {
                objOphth.EOPE_R_BCVA_TYPE = "With Glass";
            }
            if (radVisualAcuity_BCVA_R_ContLen.Checked == true)
            {
                objOphth.EOPE_R_BCVA_TYPE = "With Contact Lense";
            }


            if (radVisualAcuity_BCVA_L_Glass.Checked == true)
            {
                objOphth.EOPE_L_BCVA_TYPE = "With Glass";
            }
            if (radVisualAcuity_BCVA_L_ContLen.Checked == true)
            {
                objOphth.EOPE_L_BCVA_TYPE = "With Contact Lense";
            }



            objOphth.EOPE_L_FIELD = txtField_L.Text;
            objOphth.EOPE_L_IOP = txtIOP_L.Text;
            objOphth.EOPE_L_EOM = txtEOM_L.Text;
            objOphth.EOPE_L_LIDS = txtLids_L.Text;
            objOphth.EOPE_L_CONJUNCTIVA = txtConjunctiva_L.Text;
            objOphth.EOPE_L_SCLERA = txtSclera_L.Text;
            objOphth.EOPE_L_CORNEA = txtCornea_L.Text;
            objOphth.EOPE_L_AC = txtAC_L.Text;
            objOphth.EOPE_L_ANGLE = txtAngle_L.Text;
            objOphth.EOPE_L_IRIS = txtIris_L.Text;
            objOphth.EOPE_L_LENS = txtLens_L.Text;
            objOphth.EOPE_L_VITREOUS = txtVitreous_L.Text;
            objOphth.EOPE_L_OPTICNERVE = txtOpticNerve_L.Text;
            objOphth.EOPE_L_CHOROID = txtChoroid_L.Text;

            objOphth.EOPE_OCT = txtOCT.Value;
            objOphth.EOPE_FA = txtFA.Value;
            objOphth.EOPE_FIELD = txtField.Value;

            objOphth.EOPE_R_NPDR = txtReinalNPDR_R.Text;
            objOphth.EOPE_R_CSME = txtReinalCSME_R.Text;
            objOphth.EOPE_L_NPDR = txtReinalNPDR_L.Text;
            objOphth.EOPE_L_CSME = txtReinalCSME_L.Text;


            //objOphth.EOPE_R_BCVA_PG = txtVisualAcuity_BCVA_R_PG.Value;
            objOphth.EOPE_R_COLORVISION = txtVisualAcuity_COLORVISION_R.Value;
            // objOphth.EOPE_L_BCVA_PG = txtVisualAcuity_BCVA_L_PG.Value;
            objOphth.EOPE_L_COLORVISION = txtVisualAcuity_COLORVISION_L.Value;

            if (radField_R_Normal.Checked == true)
            {
                objOphth.EOPE_R_FIELD_STATUS = "Normal";
            }
            if (radField_R_Abnormal.Checked == true)
            {
                objOphth.EOPE_R_FIELD_STATUS = "Abnormal";
            }

            if (radIOP_R_Normal.Checked == true)
            {
                objOphth.EOPE_R_IOP_STATUS = "Normal";
            }
            if (radIOP_R_Abnormal.Checked == true)
            {
                objOphth.EOPE_R_IOP_STATUS = "Abnormal";
            }

            if (radEOM_R_Normal.Checked == true)
            {
                objOphth.EOPE_R_EOM_STATUS = "Normal";
            }
            if (radEOM_R_Abnormal.Checked == true)
            {
                objOphth.EOPE_R_EOM_STATUS = "Abnormal";
            }

            if (radLids_R_Normal.Checked == true)
            {
                objOphth.EOPE_R_LIDS_STATUS = "Normal";
            }
            if (radLids_R_Abnormal.Checked == true)
            {
                objOphth.EOPE_R_LIDS_STATUS = "Abnormal";
            }

            if (radConjunctiva_R_Normal.Checked == true)
            {
                objOphth.EOPE_R_CONJUNCTIVA_STATUS = "Normal";
            }
            if (radConjunctiva_R_Abnormal.Checked == true)
            {
                objOphth.EOPE_R_CONJUNCTIVA_STATUS = "Abnormal";
            }

            if (radSclera_R_Normal.Checked == true)
            {
                objOphth.EOPE_R_SCLERA_STATUS = "Normal";
            }
            if (radSclera_R_Abnormal.Checked == true)
            {
                objOphth.EOPE_R_SCLERA_STATUS = "Abnormal";
            }

            if (radCornea_R_Normal.Checked == true)
            {
                objOphth.EOPE_R_CORNEA_STATUS = "Normal";
            }
            if (radCornea_R_Abnormal.Checked == true)
            {
                objOphth.EOPE_R_CORNEA_STATUS = "Abnormal";
            }


            if (radAC_R_Normal.Checked == true)
            {
                objOphth.EOPE_R_AC_STATUS = "Normal";
            }
            if (radAC_R_Abnormal.Checked == true)
            {
                objOphth.EOPE_R_AC_STATUS = "Abnormal";
            }


            if (radAngle_R_Normal.Checked == true)
            {
                objOphth.EOPE_R_ANGLE_STATUS = "Normal";
            }
            if (radAngle_R_Abnormal.Checked == true)
            {
                objOphth.EOPE_R_ANGLE_STATUS = "Abnormal";
            }


            if (radIris_R_Normal.Checked == true)
            {
                objOphth.EOPE_R_IRIS_STATUS = "Normal";
            }
            if (radIris_R_Abnormal.Checked == true)
            {
                objOphth.EOPE_R_IRIS_STATUS = "Abnormal";
            }

            if (radLens_R_Normal.Checked == true)
            {
                objOphth.EOPE_R_LENS_STATUS = "Normal";
            }
            if (radLens_R_Abnormal.Checked == true)
            {
                objOphth.EOPE_R_LENS_STATUS = "Abnormal";
            }

            if (radVitreous_R_Normal.Checked == true)
            {
                objOphth.EOPE_R_VITREOUS_STATUS = "Normal";
            }
            if (radVitreous_R_Abnormal.Checked == true)
            {
                objOphth.EOPE_R_VITREOUS_STATUS = "Abnormal";
            }



            if (radOpticNerve_R_Normal.Checked == true)
            {
                objOphth.EOPE_R_OPTICNERVE_STATUS = "Normal";
            }
            if (radOpticNerve_R_Abnormal.Checked == true)
            {
                objOphth.EOPE_R_OPTICNERVE_STATUS = "Abnormal";
            }


            if (radChoroid_R_Normal.Checked == true)
            {
                objOphth.EOPE_R_CHOROID_STATUS = "Normal";
            }
            if (radChoroid_R_Abnormal.Checked == true)
            {
                objOphth.EOPE_R_CHOROID_STATUS = "Abnormal";
            }


            if (radReinalNPDR_R_Normal.Checked == true)
            {
                objOphth.EOPE_R_NPDR_STATUS = "Normal";
            }
            if (radReinalNPDR_R_Abnormal.Checked == true)
            {
                objOphth.EOPE_R_NPDR_STATUS = "Abnormal";
            }

            if (radReinalCSME_R_Normal.Checked == true)
            {
                objOphth.EOPE_R_CSME_STATUS = "Normal";
            }
            if (radReinalCSME_R_Abnormal.Checked == true)
            {
                objOphth.EOPE_R_CSME_STATUS = "Abnormal";
            }



            if (radField_L_Normal.Checked == true)
            {
                objOphth.EOPE_L_FIELD_STATUS = "Normal";
            }
            if (radField_L_Abnormal.Checked == true)
            {
                objOphth.EOPE_L_FIELD_STATUS = "Abnormal";
            }

            if (radIOP_L_Normal.Checked == true)
            {
                objOphth.EOPE_L_IOP_STATUS = "Normal";
            }
            if (radIOP_L_Abnormal.Checked == true)
            {
                objOphth.EOPE_L_IOP_STATUS = "Abnormal";
            }

            if (radEOM_L_Normal.Checked == true)
            {
                objOphth.EOPE_L_EOM_STATUS = "Normal";
            }
            if (radEOM_L_Abnormal.Checked == true)
            {
                objOphth.EOPE_L_EOM_STATUS = "Abnormal";
            }

            if (radLids_L_Normal.Checked == true)
            {
                objOphth.EOPE_L_LIDS_STATUS = "Normal";
            }
            if (radLids_L_Abnormal.Checked == true)
            {
                objOphth.EOPE_L_EOM_STATUS = "Abnormal";
            }

            if (radConjunctiva_L_Normal.Checked == true)
            {
                objOphth.EOPE_L_CONJUNCTIVA_STATUS = "Normal";
            }
            if (radConjunctiva_L_Abnormal.Checked == true)
            {
                objOphth.EOPE_L_CONJUNCTIVA_STATUS = "Abnormal";
            }

            if (radSclera_L_Normal.Checked == true)
            {
                objOphth.EOPE_L_SCLERA_STATUS = "Normal";
            }
            if (radSclera_L_Abnormal.Checked == true)
            {
                objOphth.EOPE_L_SCLERA_STATUS = "Abnormal";
            }

            if (radCornea_L_Normal.Checked == true)
            {
                objOphth.EOPE_L_CORNEA_STATUS = "Normal";
            }
            if (radCornea_L_Abnormal.Checked == true)
            {
                objOphth.EOPE_L_CORNEA_STATUS = "Abnormal";
            }


            if (radAC_L_Normal.Checked == true)
            {
                objOphth.EOPE_L_AC_STATUS = "Normal";
            }
            if (radAC_L_Abnormal.Checked == true)
            {
                objOphth.EOPE_L_AC_STATUS = "Abnormal";
            }


            if (radAngle_L_Normal.Checked == true)
            {
                objOphth.EOPE_L_ANGLE_STATUS = "Normal";
            }
            if (radAngle_L_Abnormal.Checked == true)
            {
                objOphth.EOPE_L_ANGLE_STATUS = "Abnormal";
            }


            if (radIris_L_Normal.Checked == true)
            {
                objOphth.EOPE_L_IRIS_STATUS = "Normal";
            }
            if (radIris_L_Abnormal.Checked == true)
            {
                objOphth.EOPE_L_IRIS_STATUS = "Abnormal";
            }

            if (radLens_L_Normal.Checked == true)
            {
                objOphth.EOPE_L_LENS_STATUS = "Normal";
            }
            if (radLens_L_Abnormal.Checked == true)
            {
                objOphth.EOPE_L_LENS_STATUS = "Abnormal";
            }

            if (radVitreous_L_Normal.Checked == true)
            {
                objOphth.EOPE_L_VITREOUS_STATUS = "Normal";
            }
            if (radVitreous_L_Abnormal.Checked == true)
            {
                objOphth.EOPE_L_VITREOUS_STATUS = "Abnormal";
            }



            if (radOpticNerve_L_Normal.Checked == true)
            {
                objOphth.EOPE_L_OPTICNERVE_STATUS = "Normal";
            }
            if (radOpticNerve_L_Abnormal.Checked == true)
            {
                objOphth.EOPE_L_OPTICNERVE_STATUS = "Abnormal";
            }


            if (radChoroid_L_Normal.Checked == true)
            {
                objOphth.EOPE_L_CHOROID_STATUS = "Normal";
            }
            if (radChoroid_L_Abnormal.Checked == true)
            {
                objOphth.EOPE_L_CHOROID_STATUS = "Abnormal";
            }


            if (radReinalNPDR_L_Normal.Checked == true)
            {
                objOphth.EOPE_L_NPDR_STATUS = "Normal";
            }
            if (radReinalNPDR_L_Abnormal.Checked == true)
            {
                objOphth.EOPE_L_NPDR_STATUS = "Abnormal";
            }

            if (radReinalCSME_L_Normal.Checked == true)
            {
                objOphth.EOPE_L_CSME_STATUS = "Normal";
            }
            if (radReinalCSME_L_Abnormal.Checked == true)
            {
                objOphth.EOPE_L_CSME_STATUS = "Abnormal";
            }





            if (radReinalNPDR_R_Dilated.Checked == true)
            {
                objOphth.EOPE_R_NPDR_TYPE = "Dilated";
            }
            if (radReinalNPDR_R_Undilated.Checked == true)
            {
                objOphth.EOPE_R_NPDR_TYPE = "Undilated";
            }

            if (radReinalNPDR_L_Dilated.Checked == true)
            {
                objOphth.EOPE_L_NPDR_TYPE = "Dilated";
            }
            if (radReinalNPDR_L_Undilated.Checked == true)
            {
                objOphth.EOPE_L_NPDR_TYPE = "Undilated";
            }



            objOphth.EOPE_R_GONIOSCOPY = txtGonioscopy_R.Text;
            objOphth.EOPE_L_GONIOSCOPY = txtGonioscopy_L.Text;


            if (radGonioscopy_R_Normal.Checked == true)
            {
                objOphth.EOPE_R_GONIOSCOPY_STATUS = "Normal";
            }
            if (radGonioscopy_R_Abnormal.Checked == true)
            {
                objOphth.EOPE_R_GONIOSCOPY_STATUS = "Abnormal";
            }


            if (radGonioscopy_L_Normal.Checked == true)
            {
                objOphth.EOPE_L_GONIOSCOPY_STATUS = "Normal";
            }
            if (radGonioscopy_L_Abnormal.Checked == true)
            {
                objOphth.EOPE_L_GONIOSCOPY_STATUS = "Abnormal";
            }



            objOphth.EOPE_R_PUPIL = txtPupil_R.Text;
            objOphth.EOPE_L_PUPIL = txtPupil_L.Text;


            if (radPupil_R_Normal.Checked == true)
            {
                objOphth.EOPE_R_PUPIL_STATUS = "Normal";
            }
            if (radPupil_R_Abnormal.Checked == true)
            {
                objOphth.EOPE_R_PUPIL_STATUS = "Abnormal";
            }


            if (radPupil_L_Normal.Checked == true)
            {
                objOphth.EOPE_L_PUPIL_STATUS = "Normal";
            }
            if (radPupil_L_Abnormal.Checked == true)
            {
                objOphth.EOPE_L_PUPIL_STATUS = "Abnormal";
            }


            objOphth.EMR_OpthalExaminationAdd();



        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    if (GlobalValues.FileDescription.ToUpper() == "MAMPILLY")
                    {
                        lblNPDR.Visible = false;
                        lblCSME.Visible = false;

                        txtReinalCSME_R.Visible = false;
                        txtReinalCSME_L.Visible = false;

                        radReinalCSME_R_Normal.Visible = false;
                        radReinalCSME_R_Abnormal.Visible = false;
                        radReinalCSME_L_Normal.Visible = false;
                        radReinalCSME_L_Abnormal.Visible = false;


                    }
                    BindData();
                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "       Ophthalmology.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }


            }

        }

        protected void chkAllNormal_CheckedChanged(object sender, EventArgs e)
        {
            if (chkAllNormal.Checked == true)
            {
                radField_R_Normal.Checked = true;
                radField_L_Normal.Checked = true;

                radIOP_R_Normal.Checked = true;
                radIOP_L_Normal.Checked = true;

                radEOM_R_Normal.Checked = true;
                radEOM_L_Normal.Checked = true;

                radLids_R_Normal.Checked = true;
                radLids_L_Normal.Checked = true;

                radConjunctiva_R_Normal.Checked = true;
                radConjunctiva_L_Normal.Checked = true;

                radSclera_R_Normal.Checked = true;
                radSclera_L_Normal.Checked = true;

                radCornea_R_Normal.Checked = true;
                radCornea_L_Normal.Checked = true;

                radAC_R_Normal.Checked = true;
                radAC_L_Normal.Checked = true;

                radAngle_R_Normal.Checked = true;
                radAngle_L_Normal.Checked = true;

                radIris_R_Normal.Checked = true;
                radIris_L_Normal.Checked = true;

                radLens_R_Normal.Checked = true;
                radLens_L_Normal.Checked = true;

                radVitreous_R_Normal.Checked = true;
                radVitreous_L_Normal.Checked = true;

                radOpticNerve_R_Normal.Checked = true;
                radOpticNerve_L_Normal.Checked = true;

                radChoroid_R_Normal.Checked = true;
                radChoroid_L_Normal.Checked = true;

                radGonioscopy_R_Normal.Checked = true;
                radGonioscopy_L_Normal.Checked = true;

                radPupil_R_Normal.Checked = true;
                radPupil_L_Normal.Checked = true;

                radReinalNPDR_R_Normal.Checked = true;
                radReinalNPDR_L_Normal.Checked = true;

                radReinalCSME_R_Normal.Checked = true;
                radReinalCSME_L_Normal.Checked = true;


            }
            else
            {

                radField_R_Normal.Checked = false;
                radField_L_Normal.Checked = false;

                radIOP_R_Normal.Checked = false;
                radIOP_L_Normal.Checked = false;

                radEOM_R_Normal.Checked = false;
                radEOM_L_Normal.Checked = false;

                radLids_R_Normal.Checked = false;
                radLids_L_Normal.Checked = false;

                radConjunctiva_R_Normal.Checked = false;
                radConjunctiva_L_Normal.Checked = false;

                radSclera_R_Normal.Checked = false;
                radSclera_L_Normal.Checked = false;

                radCornea_R_Normal.Checked = false;
                radCornea_L_Normal.Checked = false;

                radAC_R_Normal.Checked = false;
                radAC_L_Normal.Checked = false;

                radAngle_R_Normal.Checked = false;
                radAngle_L_Normal.Checked = false;

                radIris_R_Normal.Checked = false;
                radIris_L_Normal.Checked = false;

                radLens_R_Normal.Checked = false;
                radLens_L_Normal.Checked = false;

                radVitreous_R_Normal.Checked = false;
                radVitreous_L_Normal.Checked = false;

                radOpticNerve_R_Normal.Checked = false;
                radOpticNerve_L_Normal.Checked = false;

                radChoroid_R_Normal.Checked = false;
                radChoroid_L_Normal.Checked = false;

                radGonioscopy_R_Normal.Checked = false;
                radGonioscopy_L_Normal.Checked = false;

                radPupil_R_Normal.Checked = false;
                radPupil_L_Normal.Checked = false;

                radReinalNPDR_R_Normal.Checked = false;
                radReinalNPDR_L_Normal.Checked = false;

                radReinalCSME_R_Normal.Checked = false;
                radReinalCSME_L_Normal.Checked = false;
            }

        }
    }
}