﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EMR_BAL;
using System.Data;
using System.IO;


namespace Mediplus.EMR.Ophthalmology
{
    public partial class Opticals : System.Web.UI.UserControl
    {

        public string rdsph, rdcyl, rdaxis, ldsph, ldcyl, ldaxis, rnsph, rncyl, rnaxis, lnsph, lncyl, lnaxis, remarks;



        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindData()
        {
            EMR_Ophthalmology objOphth = new EMR_Ophthalmology();
            DataSet DS = new DataSet();

            DS = objOphth.WEMR_spS_GetRefraction(Convert.ToString(Session["Branch_ID"]), Convert.ToString(Session["EMR_ID"]));
            if (DS.Tables[0].Rows.Count > 0)
            {

                txtrdsph.Value = Convert.ToString(DS.Tables[0].Rows[0]["R_D_Sph"]);
                txtrdcyl.Value = Convert.ToString(DS.Tables[0].Rows[0]["R_D_Cyl"]);
                txtrdaxis.Value = Convert.ToString(DS.Tables[0].Rows[0]["R_D_Axis"]);
                txtldsph.Value = Convert.ToString(DS.Tables[0].Rows[0]["L_D_Sph"]);
                txtldcyl.Value = Convert.ToString(DS.Tables[0].Rows[0]["L_D_Cyl"]);
                txtldaxis.Value = Convert.ToString(DS.Tables[0].Rows[0]["L_D_Axis"]);
                txtrnsph.Value = Convert.ToString(DS.Tables[0].Rows[0]["R_N_Sph"]);
                txtrncyl.Value = Convert.ToString(DS.Tables[0].Rows[0]["R_N_Cyl"]);
                txtrnaxis.Value = Convert.ToString(DS.Tables[0].Rows[0]["R_N_Axis"]);
                txtlnsph.Value = Convert.ToString(DS.Tables[0].Rows[0]["L_N_Sph"]);
                txtlncyl.Value = Convert.ToString(DS.Tables[0].Rows[0]["L_N_Cyl"]);
                txtlnaxis.Value = Convert.ToString(DS.Tables[0].Rows[0]["L_N_Axis"]);
                txtRemarks.Text = Convert.ToString(DS.Tables[0].Rows[0]["Remarks"]);
            }
        }

        public void SaveOpticals()
        {
            EMR_Ophthalmology objOphth = new EMR_Ophthalmology();
           objOphth.rdsph= txtrdsph.Value;
           objOphth.rdcyl=txtrdcyl.Value;
           objOphth.rdaxis = txtrdaxis.Value;

           objOphth.rdprism="";
           objOphth.rdva="";
           objOphth.radd="";
           objOphth.rnsph = txtrnsph.Value;
           objOphth.rncyl = txtrncyl.Value;
           objOphth.rnaxis = txtrnaxis.Value;
           objOphth.rnprism="";
           objOphth.rnva="";

           objOphth.ldsph= txtldsph.Value;
           objOphth.ldcyl= txtldcyl.Value;
           objOphth.ldaxis= txtldaxis.Value;

           objOphth.ldprism="";
           objOphth.ldva="";
           objOphth.ladd="";

           objOphth.lnsph= txtlnsph.Value ;
           objOphth.lncyl= txtlncyl.Value ;
           objOphth.lnaxis= txtlnaxis.Value ;

           objOphth.lnprism="";
           objOphth.lnva="";
           objOphth.crdsph="";
           objOphth.crdcyl="";
           objOphth.crdaxis="";
           objOphth.crdprism="";
           objOphth.crdva="";
           objOphth.cldsph="";
           objOphth.cldcyl="";
           objOphth.cldaxis="";
           objOphth.cldprism="";
           objOphth.cldva="";

           objOphth.remarks =  txtRemarks.Text;
           objOphth.branchid = Convert.ToString(Session["Branch_ID"]);
           objOphth.patientmasterid = Convert.ToString(Session["EMR_ID"]);
           objOphth.WEMR_spI_SaveRefraction();


        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {

                    BindData();
                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "       Opticals.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }


            }
        }
    }
}