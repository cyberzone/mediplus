﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_BAL;

namespace Mediplus.EMR.Ophthalmology
{
    public partial class DisplayScandImage : System.Web.UI.Page
    {
        CommonBAL objCom = new CommonBAL();

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }
        void BindScreenCustomization()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";

            Criteria += " AND SCREENNAME='EMR_DEPT'";
            DS = new DataSet();
            DS = objCom.ScreenCustomizationGet(Criteria);
            ViewState["DEPARTMENTAL_IMAGE_PATH"] = "0";



            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    if (Convert.ToString(DR["SEGMENT"]) == "DEPARTMENTAL_IMAGE_PATH")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            ViewState["DEPARTMENTAL_IMAGE_PATH"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }



                }
            }



        }

        void DisplayImage()
        {
            try
            {

                string DentalPath = "", strPTID = "",SegmentType="", FolderName="";
                string PBMI_FileName = "";
                string PBMI_FullPath = "";


                strPTID = Convert.ToString(ViewState["PT_ID"]).Replace("/", "_"); ;
                SegmentType = Convert.ToString(ViewState["SegmentType"]);
                FolderName = Convert.ToString(ViewState["FolderName"]);
                PBMI_FileName = Convert.ToString(ViewState["FileName"]);
                DentalPath = Convert.ToString(ViewState["DEPARTMENTAL_IMAGE_PATH"]);
 
                // 00028@23122014@11@DCF

                if (File.Exists(@DentalPath + SegmentType + @"\" + strPTID + @"\" + FolderName + @"\" + PBMI_FileName))
                {
                    PBMI_FullPath = @DentalPath + SegmentType + @"\" + strPTID + @"\" + FolderName + @"\" + PBMI_FileName;

                    Response.ContentType = "image/JPEG";
                    Response.WriteFile(PBMI_FullPath);
                    Response.End();
                }


                //if (System.IO.File.Exists(PBMI_FullPath) == true)
                //{
                //    if (FileExtension.ToLower() == ".pdf")
                //    {
                //        Response.ContentType = "Application/pdf";
                //    }
                //    else if (FileExtension.ToLower() == ".docx" || FileExtension.ToLower() == ".doc")
                //    {
                //        Response.ContentType = "application/ms-word";
                //    }
                //    else if (FileExtension.ToLower() == ".xlsx" || FileExtension.ToLower() == ".xls")
                //    {
                //        Response.ContentType = "application/vnd.xls";
                //    }
                //    else if (FileExtension.ToLower() == ".jpg")
                //    {
                //        Response.ContentType = "image/JPEG";
                //    }
                //    else if (FileExtension.ToLower() == ".png")
                //    {
                //        Response.ContentType = "image/png";
                //    }
                //    else if (FileExtension.ToLower() == ".bmp")
                //    {
                //        Response.ContentType = "image/bmp";
                //    }
                //    else if (FileExtension.ToLower() == ".gif")
                //    {
                //        Response.ContentType = "image/gif";
                //    }



                    Response.WriteFile(PBMI_FullPath);
                    Response.End();


                //}
            }
            catch
            {
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ViewState["PT_ID"] = Request.QueryString["PT_ID"];
          
            ViewState["SegmentType"] = Request.QueryString["SegmentType"];
            ViewState["FolderName"] = Request.QueryString["FolderName"];
            ViewState["FileName"] = Request.QueryString["FileName"];

            BindScreenCustomization();
            DisplayImage();
        }
    }
}