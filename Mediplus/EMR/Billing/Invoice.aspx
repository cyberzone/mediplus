﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="Invoice.aspx.cs" Inherits="Mediplus.EMR.Billing.Invoice" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
      <link href="../../Styles/style.css" rel="Stylesheet" type="text/css" />
    <link href="../../Styles/style.css" rel="Style" type="text/css" />
    <script src="../../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    
    
     <script src="../../Scripts/AccordionScript.js"></script>
     <link rel="stylesheet" href="../../Styles/Accordionstyles.css">

    <link href="../Styles/style.css" rel="stylesheet" type="text/css" />

    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
     <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    
    <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divwidth
        {
            width: 400px !important;
        }

            #divwidth div
            {
                width: 400px !important;
            }


        #divDiagExt
        {
            width: 400px !important;
        }

            #divDiagExt div
            {
                width: 400px !important;
            }

        #divComp
        {
            width: 400px !important;
        }

            #divComp div
            {
                width: 400px !important;
            }


        #divDr
        {
            width: 400px !important;
        }

            #divDr div
            {
                width: 400px !important;
            }

        #divOrdClin
        {
            width: 400px !important;
        }

            #divOrdClin div
            {
                width: 400px !important;
            }
    </style>


    <script language="javascript" type="text/javascript">

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }

        function PatientPopup1(CtrlName, strValue) {
            var win = window.open("../Registration/Firstname_Zoom.aspx?PageName=Invoice&CtrlName=" + CtrlName + "&Value=" + strValue, "newwin", "top=200,left=100,height=650,width=1100,toolbar=no,scrollbars==yes,menubar=no");
            win.focus();

            return true;

        }

        function PTPopup(PtId) {

            if (PtId != "") {
                var win = window.open("../Registration/Patient_Registration.aspx?PatientId=" + PtId, "newwin1", "top=200,left=100,height=700,width=950,toolbar=no,scrollbars=" + scroll + ",menubar=no");
                win.focus();
            }
        }



        function ServicePopup(CtrlName, strValue) {
            var strPermission = document.getElementById("<%=hidPermission.ClientID%>").value;
            var strSubCompID = document.getElementById('<%=txtSubCompanyID.ClientID%>').value;

            if (strPermission == "3" || strPermission == "5" || strPermission == "9") {
                var win = "";

                if (strSubCompID != "") {
                    win = window.open("../Masters/AgrementServiceLookup.aspx?PageName=Invoice&CtrlName=" + CtrlName + "&Value=" + strValue + "&COMP_ID=" + strSubCompID, "newwin", "height=570,width=950,toolbar=no,scrollbars==yes,menubar=no");

                }
                else {
                    win = window.open("../Masters/ServiceMasterLookup.aspx?PageName=Invoice&CtrlName=" + CtrlName + "&Value=" + strValue, "newwin", "height=570,width=950,toolbar=no,scrollbars==yes,menubar=no");
                }
                win.focus();
            }

            return true;

        }
        function CheckInvType(msg1, msg2) {
            var isDelCompany = window.confirm('This patient is registered as ' + msg1 + ' Do you want Invoice this Patient as ' + msg2);

            return true

        }

        function CompIdSelected() {
            if (document.getElementById('<%=txtSubCompanyID.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtSubCompanyID.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtSubCompanyID.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtCompanyName.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }

        function CompNameSelected() {
            if (document.getElementById('<%=txtCompanyName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtCompanyName.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtSubCompanyID.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtCompanyName.ClientID%>').value = Name.trim();
                }
            }

            return true;
        }

        function DRIdSelected() {
            if (document.getElementById('<%=txtDoctorID.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtDoctorID.ClientID%>').value;
                var Data1 = Data.split('~');
                if (Data1.length > 1) {
                    document.getElementById('<%=txtDoctorID.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtDoctorName.ClientID%>').value = Data1[1];
                }
            }
            return true;
        }


        function DRNameSelected() {
            if (document.getElementById('<%=txtDoctorName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtDoctorName.ClientID%>').value;
                var Data1 = Data.split('~');
                if (Data1.length > 1) {
                    document.getElementById('<%=txtDoctorID.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtDoctorName.ClientID%>').value = Data1[1];
                }
            }

            return true;
        }



        function OrdClinIdSelected() {
            if (document.getElementById('<%=txtOrdClinCode.ClientID%>').value != "") {
                 var Data = document.getElementById('<%=txtOrdClinCode.ClientID%>').value;
                 var Data1 = Data.split('~');
                 if (Data1.length > 1) {
                     document.getElementById('<%=txtOrdClinCode.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtOrdClinName.ClientID%>').value = Data1[1];
                }
            }
            return true;
        }


        function OrdClinNameSelected() {
            if (document.getElementById('<%=txtOrdClinName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtOrdClinName.ClientID%>').value;
                var Data1 = Data.split('~');
                if (Data1.length > 1) {
                    document.getElementById('<%=txtOrdClinCode.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtOrdClinName.ClientID%>').value = Data1[1];
                }
            }

            return true;
        }


        function ServIdSelected() {
            if (document.getElementById('<%=txtServCode.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtServCode.ClientID%>').value;
                var Data1 = Data.split('~');
                if (Data1.length > 1) {
                    document.getElementById('<%=txtServCode.ClientID%>').value = Data1[0];
                     document.getElementById('<%=txtServName.ClientID%>').value = Data1[1];
                 }
             }
             return true;
         }


         function ServNameSelected() {
             if (document.getElementById('<%=txtServName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtServName.ClientID%>').value;
                var Data1 = Data.split('~');

                if (Data1.length > 1) {
                    document.getElementById('<%=txtServCode.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtServName.ClientID%>').value = Data1[1];
                }
            }

            return true;
        }


        function DiagIdSelected() {
            if (document.getElementById('<%=txtDiagCode.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtDiagCode.ClientID%>').value;
                var Data1 = Data.split('~');
                if (Data1.length > 1) {
                    document.getElementById('<%=txtDiagCode.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtDiagName.ClientID%>').value = Data1[1];
                }
            }
            return true;
        }


        function DiagNameSelected() {
            if (document.getElementById('<%=txtDiagName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtDiagName.ClientID%>').value;
                var Data1 = Data.split('~');

                if (Data1.length > 1) {
                    document.getElementById('<%=txtDiagCode.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtDiagName.ClientID%>').value = Data1[1];
                }
            }

            return true;
        }

        function RefDRNameSelected() {
            if (document.getElementById('<%=txtRefDrName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtRefDrName.ClientID%>').value;
                var Data1 = Data.split('~');
                if (Data1.length > 1) {
                    document.getElementById('<%=txtRefDrId.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtRefDrName.ClientID%>').value = Data1[1];
                }
            }

            return true;
        }
        function ConfirmYes() {

            document.getElementById('<%=hidMsgConfirm.ClientID%>').value = "true";

        }

        function Confirm() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("Patient Policy is expired. So you cannot claim to this invoice. Do you want to invoice?")) {
                confirm_value.value = "Yes";
            } else {
                confirm_value.value = "No";
            }
            document.forms[0].appendChild(confirm_value);
        }


        function ServiceAddVal() {

            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";


            var strtxtServCode = document.getElementById('<%=txtServCode.ClientID%>').value
            if (/\S+/.test(strtxtServCode) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Service Code";
                document.getElementById('<%=txtServCode.ClientID%>').focus;
                return false;
            }


            var strtxtServName = document.getElementById('<%=txtServName.ClientID%>').value
            if (/\S+/.test(strtxtServName) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Service Name";
                document.getElementById('<%=txtServName.ClientID%>').focus;
                return false;
            }



        }

        function DiagAddVal() {

            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";


            var strtxtServCode = document.getElementById('<%=txtDiagCode.ClientID%>').value
            if (/\S+/.test(strtxtServCode) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Diagnosis  Code";
                document.getElementById('<%=txtDiagCode.ClientID%>').focus;
                return false;
            }

            var strtxtServName = document.getElementById('<%=txtDiagName.ClientID%>').value
            if (/\S+/.test(strtxtServName) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Service Name";
                document.getElementById('<%=txtDiagName.ClientID%>').focus;
                return false;
            }

        }

        function DeleteInvoiceVal() {
            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";


            var strCompCode = document.getElementById('<%=txtInvoiceNo.ClientID%>').value
            if (/\S+/.test(strCompCode) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Invoice No";
                document.getElementById('<%=txtInvoiceNo.ClientID%>').focus;
                return false;
            }

            var isDelCompany = window.confirm('Do you want to delete Invoice Information?');

            if (isDelCompany == true) {
                return true
            }
            else {
                return false;
            }
            return true
        }



        function InvSaveVal() {

            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";


            var strtxtInvoiceNo = document.getElementById('<%=txtInvoiceNo.ClientID%>').value
            if (/\S+/.test(strtxtInvoiceNo) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Invoice No";
                document.getElementById('<%=txtInvoiceNo.ClientID%>').focus;
                return false;
            }

            var strtxtInvDate = document.getElementById('<%=txtInvDate.ClientID%>').value
            if (/\S+/.test(strtxtInvDate) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Date";
                document.getElementById('<%=txtInvDate.ClientID%>').focus;
                return false;
            }


            var strtxtInvTime = document.getElementById('<%=txtInvTime.ClientID%>').value
            if (/\S+/.test(strtxtInvTime) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Time";
                document.getElementById('<%=txtInvTime.ClientID%>').focus;
                return false;
            }

            var strtxtFileNo = document.getElementById('<%=txtFileNo.ClientID%>').value
            if (/\S+/.test(strtxtFileNo) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter File No";
                document.getElementById('<%=txtFileNo.ClientID%>').focus;
                return false;
            }


            var strtxtName = document.getElementById('<%=txtName.ClientID%>').value
            if (/\S+/.test(strtxtName) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Name";
                document.getElementById('<%=txtName.ClientID%>').focus;
                return false;
            }






            if (document.getElementById('<%=drpInvType.ClientID%>').value == "Credit") {


                var strtxtSubCompanyID = document.getElementById('<%=txtSubCompanyID.ClientID%>').value
                if (/\S+/.test(strtxtSubCompanyID) == false) {
                    document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Company ID";
                    document.getElementById('<%=txtSubCompanyID.ClientID%>').focus;
                    return false;
                }


                var strtxtCompanyName = document.getElementById('<%=txtCompanyName.ClientID%>').value
                if (/\S+/.test(strtxtCompanyName) == false) {
                    document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Company Name";
                    document.getElementById('<%=txtSubCompanyID.ClientID%>').focus;
                    return false;
                }

                var strtxtPolicyNo = document.getElementById('<%=txtPolicyNo.ClientID%>').value
                if (/\S+/.test(strtxtPolicyNo) == false) {
                    document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Policy No";
                    document.getElementById('<%=txtPolicyNo.ClientID%>').focus;
                    return false;
                }


                var strtxtPolicyExp = document.getElementById('<%=txtPolicyExp.ClientID%>').value
                if (/\S+/.test(strtxtPolicyExp) == false) {
                    document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Policy Expiry";
                    document.getElementById('<%=txtPolicyExp.ClientID%>').focus;
                    return false;
                }




            }


            var strtxtDoctorID = document.getElementById('<%=txtDoctorID.ClientID%>').value
            if (/\S+/.test(strtxtDoctorID) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Doctor Code";
                document.getElementById('<%=txtDoctorID.ClientID%>').focus;
                return false;
            }

            var strtxtDoctorName = document.getElementById('<%=txtDoctorName.ClientID%>').value
            if (/\S+/.test(strtxtDoctorName) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Doctor Name";
                document.getElementById('<%=txtDoctorName.ClientID%>').focus;
                return false;
            }

            var PaidAmount = document.getElementById('<%=TxtRcvdAmt.ClientID%>').value;
            var PaidAmtCheck = document.getElementById('<%=hidPaidAmtCheck.ClientID%>').value;
            var PaidAmtChange = '<%=Convert.ToString(ViewState["HSOM_INVOICE_PAIDAMT_CHANGE"]) %>';
            var NewFlag = '<%=Convert.ToString(ViewState["NewFlag"]) %>';


            if (PaidAmtChange == "Y" && NewFlag == "False") {
                if (PaidAmount != PaidAmtCheck) {

                    var isMsgConfirm = window.confirm('Paid Amount changed, Do you want to Continue? ');
                    document.getElementById('<%=hidMsgConfirm.ClientID%>').value = isMsgConfirm;
                }

            }

        }


        function BindServiceDtls(ServCode, ServName, HaadCode, CtrlName) {

            document.getElementById("<%=txtServCode.ClientID%>").value = ServCode;
            document.getElementById("<%=txtServName.ClientID%>").value = ServName;




        }

        function ShowData() {
            alert("<%=strSubCompanyID %>");

        }

        function ShowSOServicePopup() {

            document.getElementById("divSOServicePopup").style.display = 'block';


        }

        function HideSOServicePopup() {

            document.getElementById("divSOServicePopup").style.display = 'none';

        }

    </script>
    <script language="javascript" type="text/javascript">

        function ShowPrintCrBill(InvoiceNo, ReportName) {
            var BranchID = '<%=Convert.ToString(Session["Branch_ID"]) %>';

            var Criteria = " 1=1 "
            Criteria += ' AND {HMS_INVOICE_MASTER.HIM_BRANCH_ID}=\'' + BranchID + '\'  AND {HMS_INVOICE_MASTER.HIM_INVOICE_ID}=\'' + InvoiceNo + '\'';


            // var win = window.open('../CReports/ReportViewer.aspx?ReportName=' + ReportName + '&SelectionFormula=' + Criteria, 'PrintCrBill', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
            // win.focus();

        }

        function ShowPrintInvoice(BranchID, InvoiceNo, ReportName) {

            var Criteria = " 1=1 "
            Criteria += ' AND {HMS_INVOICE_MASTER.HIM_BRANCH_ID}=\'' + BranchID + '\'  AND {HMS_INVOICE_MASTER.HIM_INVOICE_ID}=\'' + InvoiceNo + '\'';

            var win = window.open('../../CReports/ReportViewer.aspx?ReportName=' + ReportName + '&SelectionFormula=' + Criteria, 'PrintInvoice', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')

           // var win = window.open('../CReports/ReportViewer.aspx?ReportName=' + ReportName + '&SelectionFormula=' + Criteria, 'PrintInvoice', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
            win.focus();

        }




        function ShowMergeReport(BranchID, InvoiceNo, InvDate, FileNo, DrId, SubCompId, PolicyNo, NoMergePrint) {

            var arrFromDate = InvDate.split('/');
            var Date1;
            if (arrFromDate.length > 1) {

                Date1 = arrFromDate[2] + "-" + arrFromDate[1] + "-" + arrFromDate[0];
            }



            var Report = "HmsMergeCreditBill.rpt";
            var Criteria = " 1=1 "
            Criteria += ' AND  {HMS_INVOICE_MASTER.HIM_PT_ID}= \'' + FileNo + '\''

            Criteria += ' AND {HMS_INVOICE_MASTER.HIM_BRANCH_ID}=\'' + BranchID + '\'';

            Criteria += ' AND  {HMS_INVOICE_MASTER.HIM_DR_CODE}= \'' + DrId + '\''



            if (FileNo == "OPDCR" || FileNo == "OPD") {

                Criteria += ' AND  {HMS_INVOICE_MASTER.HIM_POLICY_NO}= \'' + PolicyNo + '\''
            }

            Criteria += ' AND  {HMS_INVOICE_MASTER.HIM_SUB_INS_CODE}=\'' + SubCompId + '\''

            Criteria += ' AND  {HMS_INVOICE_MASTER.HIM_DATE}=date(\'' + Date1 + '\')'



            if (NoMergePrint == 'Y') {

                Criteria += '  AND {HMS_INVOICE_MASTER.HIM_INVOICE_ID}=\'' + InvoiceNo + '\''
            }


            //var win = window.open('../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria, 'PrintMergeInv', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
            //win.focus();


        }

    </script>
    <script type="text/javascript">
        function ShowMessage() {
            $("#myMessage").show();
            setTimeout(function () {
                var selectedEffect = 'blind';
                var options = {};
                $("#myMessage").hide();
            }, 2000);
            return true;
        }


    </script>
    <script language="javascript" type="text/javascript">
        function clientShowing(sender) {
            //sender is reference to the timepicker object
        }
        function clientShown(sender) {
            //sender is reference to the timepicker object
        }
        function clientHiding(sender) {
            //sender is reference to the timepicker object
        }
        function clientHidden(sender) {
            //sender is reference to the timepicker object
        }
        function selectionChanged(sender) {
            alert(sender._selectedTime);
        }
</script>




</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <input type="hidden" id="hidPermission" runat="server" value="9" />
    <input type="hidden" runat="server" id="hidMsgConfirm" />

    <input type="hidden" id="hidCompleteProc" runat="server" />
<div style="padding-top: 0px; width: 80%; height: 220px; border: thin; border-color: #cccccc; border-style: groove;">
      
        <div style="text-align:left;float:left;vertical-align:top;">
                <asp:label id="lblStatus" runat="server" forecolor="red" font-bold="true" style="letter-spacing: 1px;" cssclass="label"></asp:label>
    &nbsp;&nbsp;<asp:label id="lblBalanceMsg" runat="server" forecolor="red" font-bold="true" style="letter-spacing: 1px;" cssclass="label"></asp:label>
                
</div>

     <div style="padding-left: 10%; width: 100%;">
            <div id="myMessage" style="display: none; border: groove; height: 30px; width: 200px; background-color: gray; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #fff; padding: 20px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute;">
                Saved Successfully
            </div>
        </div>
        <br />
       <div style="text-align:Right;float:right;vertical-align:top;">
                            <asp:Button ID="btnSave" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 100px;" CssClass="button red small" OnClick="btnSave_Click" Text="Save" OnClientClick="return InvSaveVal();" />
                            <asp:Button ID="btnDelete" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 100px;" CssClass="button red small" OnClick="btnDelete_Click" Text="Delete" OnClientClick="return DeleteInvoiceVal();" Visible="false" />
                            <asp:Button ID="btnClear" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 100px;" CssClass="button gray small" OnClick="btnClear_Click" Text="Clear" />

       </div>
    
        <input type="hidden" runat="server" id="hidVisitID" />
        <input type="hidden" runat="server" id="hidCompanyID" />
        <input type="hidden" runat="server" id="hidCmpRefCode" />
        <input type="hidden" runat="server" id="hidInsType" />
        <input type="hidden" runat="server" id="hidIDNo" />
        <input type="hidden" runat="server" id="hidInsTrtnt" />

        <input type="hidden" runat="server" id="hidPTVisitType" />
        <input type="hidden" runat="server" id="hidUpdateCodeType" />
        <input type="hidden" runat="server" id="hidCompAgrmtType" />
        <input type="hidden" runat="server" id="hidCoInsType" />
        <input type="hidden" runat="server" id="hidReceipt" />
        <input type="hidden" runat="server" id="hidType" />

        <input type="hidden" runat="server" id="hidccText" />
        <input type="hidden" runat="server" id="hidmasterinvoicenumber" />

        <input type="hidden" runat="server" id="hidPaidAmtCheck" />


        <table width="100%" border="0">
            <tr>
                <td class="lblCaption1" style="width: 70px">Invoice Type
                </td>
                <td style="width: 100px">
                    <asp:DropDownList ID="drpInvType" runat="server" CssClass="label" Width="95%" BorderWidth="1px" BorderColor="#cccccc">
                        <asp:ListItem Value="Cash">Cash</asp:ListItem>
                        <asp:ListItem Value="Credit" Selected="True">Credit</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td class="lblCaption1" style="width: 70px;">Inv. No
                     <asp:Button ID="btnNew" runat="server" Style="padding-left: 2px; padding-right: 2px; width: 30px;" CssClass="button gray small"
                    OnClick="btnNew_Click" Text="New" />
                </td>
                <td style="width: 100px">
                    <asp:TextBox ID="txtInvoiceNo" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Enabled="false" Width="95%" Height="22PX" MaxLength="10" AutoPostBack="true" OnTextChanged="txtInvoiceNo_TextChanged"></asp:TextBox>

                </td>

                <td class="lblCaption1" style="width: 70px">Date & Time
                </td>
                <td style="width: 150px">
                    <asp:TextBox ID="txtInvDate" runat="server" Width="48%" Height="22px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" MaxLength="10" onkeypress="return OnlyNumeric(event);" AutoPostBack="true" OnTextChanged="txtInvDate_TextChanged"></asp:TextBox>
                    <asp:TextBox ID="txtInvTime" runat="server" ReadOnly="true" Width="40%" Height="22px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" MaxLength="10"></asp:TextBox>
                    <asp:CalendarExtender ID="Calendarextender3" runat="server"
                        Enabled="True" TargetControlID="txtInvDate" Format="dd/MM/yyyy">
                    </asp:CalendarExtender>
                 

                </td>
                <td class="lblCaption1" style="width: 70px">
                    <asp:Label ID="lblInsCard" runat="server" Style="letter-spacing: 1px;" CssClass="lblCaption1" Text="Ins.Card#"></asp:Label>

                </td>
                <td style="width: 100px">
                    <asp:TextBox ID="txtIdNo" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="95%"></asp:TextBox>
                    <asp:DropDownList ID="drpStatus" CssClass="label" runat="server" Width="150px" Visible="false">
                        <asp:ListItem Value="Open">Open</asp:ListItem>
                        <asp:ListItem Value="Void">Void</asp:ListItem>
                        <asp:ListItem Value="Hold">Hold</asp:ListItem>
                    </asp:DropDownList>

                    <asp:TextBox ID="txtJobnumber" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="50%" Visible="false"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="lblCaption1">Token No
                </td>
                <td>
                    <asp:TextBox ID="txtTokenNo" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="95%"></asp:TextBox>

                </td>
                <td class="lblCaption1">File No
                </td>
                <td>
                    <asp:TextBox ID="txtFileNo" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="95%" Enabled="false" AutoPostBack="true" OnTextChanged="txtFileNo_TextChanged" ondblclick="return PatientPopup1('FileNo',this.value);"></asp:TextBox>

                </td>
                <td class="lblCaption1">Name
                </td>
                <td>
                    <asp:TextBox ID="txtName" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="95%"></asp:TextBox>

                </td>
                <td class="lblCaption1">PT Comp.
                </td>
                <td>
                    <asp:TextBox ID="txtPTComp" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="95%"></asp:TextBox>

                </td>
            </tr>

            <tr>
                <td class="lblCaption1">Comp. ID 

                </td>
                <td>
                    <asp:TextBox ID="txtSubCompanyID" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="95%" Enabled="false"></asp:TextBox>

                </td>
                <td class="lblCaption1">Ins. Name

                </td>
                <td colspan="3">
                    <asp:TextBox ID="txtSubCompanyName" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="48%" Enabled="false"></asp:TextBox>

                    <asp:TextBox ID="txtCompanyName" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="48%" Enabled="false"></asp:TextBox>


                </td>
                <td class="lblCaption1">Policy Type
                </td>
                <td>


                    <asp:TextBox ID="txtPlanType" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="95%"></asp:TextBox>

                </td>

            </tr>
            <tr>
                <td class="lblCaption1">Policy No 

                </td>
                <td colspan="2" class="lblCaption1">
                    <asp:TextBox ID="txtPolicyNo" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="80%" Enabled="false"></asp:TextBox>

                    Exp.

                </td>
                <td>
                    <asp:TextBox ID="txtPolicyExp" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="95%" Enabled="false"></asp:TextBox>

                </td>
                <td class="lblCaption1">Treat. Type
                </td>
                <td>
                    <asp:DropDownList ID="drpTreatmentType" CssClass="label" runat="server" Width="95%">
                    </asp:DropDownList>
                    <asp:TextBox ID="txtICdCode" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="50px" Visible="false"></asp:TextBox>
                    <asp:TextBox ID="txtICDDesc" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="150px" Visible="false"></asp:TextBox>

                </td>
                <td class="lblCaption1">Claim No 

                </td>
                <td>
                    <asp:TextBox ID="TxtVoucherNo" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="95%"></asp:TextBox>

                </td>
            </tr>
            <tr>
                <td class="lblCaption1">Doctor 
                </td>
                <td class="auto-style28" colspan="3">

                    <div id="divDr" style="visibility: hidden;"></div>
                    <asp:TextBox ID="txtDoctorID" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#CCCCCC" Width="20%" AutoPostBack="true" OnTextChanged="txtDoctorID_TextChanged" onblur="return DRIdSelected()"></asp:TextBox>
                    <asp:TextBox ID="txtDoctorName" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#CCCCCC" Width="75%" MaxLength="50" onblur="return DRNameSelected()"></asp:TextBox>
                    <asp:AutoCompleteExtender ID="AutoCompleteExtender3" runat="Server" TargetControlID="txtDoctorID" MinimumPrefixLength="1" ServiceMethod="GetDoctorId"
                        CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divDr">
                    </asp:AutoCompleteExtender>
                    <asp:AutoCompleteExtender ID="AutoCompleteExtender4" runat="Server" TargetControlID="txtDoctorName" MinimumPrefixLength="1" ServiceMethod="GetDoctorName"
                        CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divDr">
                    </asp:AutoCompleteExtender>

                </td>

                <td class="lblCaption1">Ref Dr.

                </td>
                <td colspan="3">
                    <asp:TextBox ID="txtRefDrId" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="20%" Visible="true"></asp:TextBox>

                    <asp:TextBox ID="txtRefDrName" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="75%" onblur="return RefDRNameSelected()" AutoPostBack="true" OnTextChanged="txtRefDrId_TextChanged"></asp:TextBox>
                    <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="Server" TargetControlID="txtRefDrName" MinimumPrefixLength="1" ServiceMethod="GetRefDoctorName"
                        CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight">
                    </asp:AutoCompleteExtender>

                </td>

            </tr>
            <tr>
                <td class="lblCaption1">Ord. Clinician 
                </td>
                <td class="auto-style28" colspan="3">

                    <div id="divOrdClin" style="visibility: hidden;"></div>
                    <asp:TextBox ID="txtOrdClinCode" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#CCCCCC" Width="20%" AutoPostBack="true" OnTextChanged="txtOrdClinCode_TextChanged" onblur="return OrdClinIdSelected()"></asp:TextBox>
                    <asp:TextBox ID="txtOrdClinName" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#CCCCCC" Width="75%" MaxLength="50" onblur="return OrdClinNameSelected()"></asp:TextBox>
                    <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="Server" TargetControlID="txtOrdClinCode" MinimumPrefixLength="1" ServiceMethod="GetDoctorId"
                        CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divOrdClin">
                    </asp:AutoCompleteExtender>
                    <asp:AutoCompleteExtender ID="AutoCompleteExtender9" runat="Server" TargetControlID="txtOrdClinName" MinimumPrefixLength="1" ServiceMethod="GetDoctorName"
                        CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divOrdClin">
                    </asp:AutoCompleteExtender>

                </td>

                <td class="lblCaption1">Remarks

                </td>
                <td>
                    <asp:TextBox ID="txtRemarks" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="95%" Visible="true"></asp:TextBox>
                    <asp:DropDownList ID="drpRemarks" CssClass="label" runat="server" Width="150px" Visible="false">
                    </asp:DropDownList>
                    <asp:DropDownList ID="drpCommissionType" CssClass="label" runat="server" Width="150px" Visible="false"></asp:DropDownList>

                </td>
            </tr>
             <tr>
                 <td class="lblCaption1">Payer ID

                </td>
                <td>
                    <asp:textbox id="txtPayerID" runat="server" cssclass="TextBoxStyle" width="80px" visible="true" ReadOnly="true"  ></asp:textbox>
                 </td>
                 <td class="lblCaption1">Reciver ID

                </td>
                <td>
                    <asp:textbox id="txtReciverID" runat="server" cssclass="TextBoxStyle" width="100px" visible="true" ReadOnly="true"  ></asp:textbox>
                 </td>

                <td colspan="3">

                </td>
                <td class="lblCaption1">
                    <asp:CheckBox ID="chkOutsidePT" Text="Outside Patient" runat="server" CssClass="lblCaption1" />
                </td>
                
            </tr>
        </table>
    </div>
    
  <div style="padding: 5px; width: 80%; border: thin; border-color: #cccccc; border-style: groove;">

         <asp:tabcontainer id="TabContainer1" runat="server" activetabindex="0" cssclass="AjaxTabStyle" width="100%" Height="550px">

          <asp:TabPanel runat="server" ID="tabService" HeaderText="Services" Width="100%">
                <ContentTemplate>
                    <table>
                        <tr>
                            <td class="lblCaption">Code <span style="color: red;">* </span>
                            </td>
                            <td class="lblCaption">
                                <a href="javascript:ShowData();" style="text-decoration: none;" class="lblCaption">Description </a>
                            </td>
                            <td>
                                <asp:UpdatePanel runat="server" ID="updatePanel5">
                                    <ContentTemplate>
                                        <asp:Button ID="btnSO_Map" runat="server" Visible="false" OnClick="btnSO_Map_Click" Text="SO. Map" CssClass="button gray small" Style="padding-left: 2px; padding-right: 2px; width: 60px;" OnClientClick="ShowSOServicePopup()" />

                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>


                        </tr>
                        <tr>
                            <td style="width: 105px;">
                                <input type="hidden" id="hidHaadID" runat="server" />
                                <input type="hidden" id="hidCatID" runat="server" />
                                <input type="hidden" id="hidCatType" runat="server" />
                                <asp:TextBox ID="txtServCode" runat="server" Width="100px" Height="22px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" AutoPostBack="true" OnTextChanged="txtServCode_TextChanged" onblur="return ServIdSelected()" ondblclick="return ServicePopup('txtServCode',this.value);"></asp:TextBox>
                            </td>
                            <td style="width: 405px;">
                                <asp:TextBox ID="txtServName" runat="server" Width="400px" Height="22px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" onblur="return ServNameSelected()" ondblclick="return ServicePopup('txtServName',this.value);"></asp:TextBox>

                                <div id="divwidth" style="visibility: hidden;"></div>
                                <asp:AutoCompleteExtender ID="AutoCompleteExtender5" runat="Server" TargetControlID="txtServCode" MinimumPrefixLength="1" ServiceMethod="GetServiceID"
                                    CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidth">
                                </asp:AutoCompleteExtender>
                                <asp:AutoCompleteExtender ID="AutoCompleteExtender6" runat="Server" TargetControlID="txtServName" MinimumPrefixLength="1" ServiceMethod="GetServiceName"
                                    CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidth">
                                </asp:AutoCompleteExtender>


                            </td>

                            <td>
                                <asp:Button ID="btnServiceAdd" runat="server" Style="padding-left: 2px; padding-right: 2px; width: 50px;" CssClass="button gray small"
                                    OnClick="btnServiceAdd_Click" Text="Add" OnClientClick="return ServiceAddVal();" />
                                <asp:Button ID="btnImportSO" runat="server" OnClick="btnImportSO_Click" Text="Import Sales Order Trans." CssClass="button gray small" Style="padding-left: 2px; padding-right: 2px; width: 150px;" />



                            </td>
                        </tr>

                    </table>
                    <div id="divSOServicePopup" style="display: none; border: groove; height: 450px; width: 600px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #eee9e9; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute; left: 400px; top:10px;">
                        <table cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td style="vertical-align: top;"></td>
                                <td align="right" style="vertical-align: top;">

                                    <input type="button" id="btnMsgClose" class="ButtonStyle" style="color: black; border: none;" value=" X " onclick="HideSOServicePopup()" />
                                </td>
                            </tr>
                        </table>
                        <asp:TabContainer ID="TabContainer2" runat="server" ActiveTabIndex="0" CssClass="ajax__tab_yuitabview-theme" Width="100%">
                            <asp:TabPanel runat="server" ID="TabPanelGeneral" HeaderText="Mapped" Width="100%">
                                <ContentTemplate>
                                    <div style="padding-top: 0px; width: 100%; height: 330px; overflow: auto; border: thin; border-color: #f6f6f6; border-style: groove;">
                                        <table cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td colspan="2">
                                                    <asp:UpdatePanel runat="server" ID="updatePanel13">
                                                        <ContentTemplate>

                                                            <asp:GridView ID="gvSOServicePopup" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                EnableModelValidation="True" GridLines="None" Width="100%">
                                                                <HeaderStyle CssClass="GridHeader" BorderStyle="Solid" BorderWidth="1px" BorderColor="#cccccc" />
                                                                <RowStyle CssClass="GridRow" />

                                                                <Columns>
                                                                    <asp:TemplateField ItemStyle-VerticalAlign="middle" ItemStyle-HorizontalAlign="Center">
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="chkInvSOSelect" runat="server" CssClass="label" Checked="true"  />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>


                                                                    <asp:TemplateField HeaderText="Code" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px">
                                                                        <ItemTemplate>

                                                                            <asp:Label ID="lblgvSOServiceID" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HSM_SERV_ID") %>'></asp:Label>

                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="Description" HeaderStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>

                                                                            <asp:Label ID="lblgvSOServiceName" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HSM_NAME") %>'></asp:Label>

                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="HAAD Code" HeaderStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>

                                                                            <asp:Label ID="lblgvSOHAADCode" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HSM_HAAD_CODE") %>'></asp:Label>

                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Amount" HeaderStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>

                                                                            <asp:Label ID="lblgvSOAmount" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HAS_COMP_FEE") %>'></asp:Label>

                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>

                                            </tr>

                                        </table>
                                    </div>
                                </ContentTemplate>
                            </asp:TabPanel>
                            <asp:TabPanel runat="server" ID="TabPanel2" HeaderText="Not Mapped" Width="100%">
                                <ContentTemplate>
                                      <div style="padding-top: 0px; width: 100%; height: 330px; overflow: auto; border: thin; border-color: #f6f6f6; border-style: groove;">
                                        <table cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td colspan="2">
                                                    <asp:UpdatePanel runat="server" ID="updatePanel14">
                                                        <ContentTemplate>

                                                            <asp:GridView ID="gvSOServicePopupSM" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                                EnableModelValidation="True" GridLines="None" Width="100%">
                                                                <HeaderStyle CssClass="GridHeader" BorderStyle="Solid" BorderWidth="1px" BorderColor="#cccccc" />
                                                                <RowStyle CssClass="GridRow" />

                                                                <Columns>
                                                                    <asp:TemplateField ItemStyle-VerticalAlign="middle" ItemStyle-HorizontalAlign="Center">
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="chkInvSOSelectSM" runat="server" CssClass="label" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>


                                                                    <asp:TemplateField HeaderText="Code" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px">
                                                                        <ItemTemplate>

                                                                            <asp:Label ID="lblgvSOServiceIDSM" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HSM_SERV_ID") %>'></asp:Label>

                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="Description" HeaderStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>

                                                                            <asp:Label ID="lblgvSOServiceNameSM" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HSM_NAME") %>'></asp:Label>

                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="HAAD Code" HeaderStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>

                                                                            <asp:Label ID="lblgvSOHAADCodeSM" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HSM_HAAD_CODE") %>'></asp:Label>

                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Amount" HeaderStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>

                                                                            <asp:Label ID="lblgvSOAmountSM" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HSM_FEE") %>'></asp:Label>

                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>

                                            </tr>

                                        </table>
                                    </div>
                                </ContentTemplate>
                            </asp:TabPanel>
                        </asp:TabContainer>
                        <asp:Button ID="btnMapSOData" runat="server" OnClick="btnMapSOData_Click" Text="Load SO Data" CssClass="button gray small" Style="padding-left: 2px; padding-right: 2px; width: 100px;" />


                    </div>


                    <div style="padding-top: 0px; width: 99%; height: 250px; overflow: auto; border: thin; border-color: #CCCCCC; border-style: groove;">
                        <table width="100%">
                            <tr>
                                <td>
                                    <asp:GridView ID="gvInvoiceTrans" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                        EnableModelValidation="True" Width="99%" gridline="none" OnRowDataBound="gvInvTran_RowDataBound">
                                        <HeaderStyle CssClass="GridHeader" />
                                        <RowStyle CssClass="GridRow" />

                                        <Columns>
                                            <asp:TemplateField HeaderText="Code" HeaderStyle-Width="110px">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblgvServCost" Width="100px" CssClass="label" runat="server" Text='<%# Bind("HIT_SERVCOST") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblgvAdjDedAmt" Width="100px" CssClass="label" runat="server" Text='<%# Bind("HIT_ADJ_DEDUCT_AMOUNT") %>' Visible="false"></asp:Label>
                                                    <asp:UpdatePanel runat="server" ID="UpdatePanel6"
                                                        UpdateMode="Conditional" ChildrenAsTriggers="true">
                                                        <ContentTemplate>


                                                            <asp:TextBox ID="txtgvServCode" Width="100px" CssClass="label" Style="border: none;" runat="server" Text='<%# Bind("HIT_SERV_CODE") %>' Height="30px" Enabled="false" AutoPostBack="false" OnTextChanged="txtgvServCode_TextChanged"></asp:TextBox>


                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="txtgvServCode" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </ItemTemplate>

                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Description">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtServDesc" Width="300px" CssClass="label" Style="border: none;" runat="server" Text='<%# Bind("HIT_Description") %>' Height="30px" Enabled="false"></asp:TextBox>



                                                </ItemTemplate>

                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Fee">
                                                <ItemTemplate>
                                                    <asp:UpdatePanel runat="server" ID="UpdatePanel12"
                                                        UpdateMode="Conditional" ChildrenAsTriggers="true">
                                                        <ContentTemplate>
                                                            <asp:TextBox ID="txtFee" runat="server" Style="text-align: right; padding-right: 5px; border: none;" Width="70px" Height="30px" Text='<%# Bind("HIT_FEE") %>' AutoPostBack="true" OnTextChanged="gvInvoiceTrans_TextChanged" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="txtFee" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </ItemTemplate>

                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="D.%" HeaderStyle-Width="50px" ItemStyle-VerticalAlign="Top">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDiscType" CssClass="GridRow" runat="server" Text='<%# Bind("HIT_DISC_TYPE") %>' Visible="false"></asp:Label>



                                                    <asp:UpdatePanel runat="server" ID="UpdatePanel4"
                                                        UpdateMode="Conditional" ChildrenAsTriggers="true">
                                                        <ContentTemplate>
                                                            <asp:DropDownList ID="drpgvDiscType" CssClass="LabelStyle" Style="border: none;" runat="server" Width="50px" Height="30px" AutoPostBack="true" OnSelectedIndexChanged="drpgvDiscType_SelectedIndexChanged">
                                                                <asp:ListItem Value="$">$</asp:ListItem>
                                                                <asp:ListItem Value="%">%</asp:ListItem>
                                                            </asp:DropDownList>

                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="drpgvDiscType" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="D.Amt" HeaderStyle-Width="70px">
                                                <ItemTemplate>

                                                    <asp:UpdatePanel runat="server" ID="UpdatePanel5"
                                                        UpdateMode="Conditional" ChildrenAsTriggers="true">
                                                        <ContentTemplate>

                                                            <asp:TextBox ID="txtDiscAmt" runat="server" CssClass="label" Style="text-align: right; padding-right: 5px; border: none;" Width="70px" Height="30px" Text='<%# Bind("HIT_DISC_AMT") %>' AutoPostBack="true" OnTextChanged="gvInvoiceTrans_TextChanged" onkeypress="return OnlyNumeric(event);"></asp:TextBox>


                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="txtDiscAmt" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>


                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Qty" HeaderStyle-Width="30px">
                                                <ItemTemplate>
                                                    <asp:UpdatePanel runat="server" ID="UpId"
                                                        UpdateMode="Conditional" ChildrenAsTriggers="true">
                                                        <ContentTemplate>
                                                            <asp:TextBox ID="txtgvQty" runat="server" CssClass="label" Style="text-align: right; padding-right: 5px; border: none;" Width="30px" Height="30px" Text='<%# Bind("HIT_QTY") %>' AutoPostBack="true" OnTextChanged="gvInvoiceTrans_TextChanged" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="txtgvQty" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>

                                                </ItemTemplate>

                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Amount" HeaderStyle-Width="30px">
                                                <ItemTemplate>

                                                    <asp:TextBox ID="txtHitAmount" CssClass="label" runat="server" Style="text-align: right; padding-right: 5px; border: none;" Width="70px" Height="30px" ReadOnly="true" Text='<%# Bind("HIT_AMOUNT") %>'></asp:TextBox>

                                                </ItemTemplate>

                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Ded." HeaderStyle-Width="70px">
                                                <ItemTemplate>
                                                    <asp:UpdatePanel runat="server" ID="UpdatePanel2"
                                                        UpdateMode="Conditional" ChildrenAsTriggers="true">
                                                        <ContentTemplate>

                                                            <asp:TextBox ID="txtDeduct" runat="server" CssClass="label" Style="text-align: right; padding-right: 5px; border: none;" Width="70px" Height="30px" Text='<%# Bind("HIT_DEDUCT") %>' AutoPostBack="true" OnTextChanged="gvInvoiceTrans_TextChanged" onkeypress="return OnlyNumeric(event);"></asp:TextBox>


                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="txtDeduct" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>


                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Co-%" HeaderStyle-Width="50px" ItemStyle-VerticalAlign="Top">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCoInsType" CssClass="GridRow" runat="server" Text='<%# Bind("HIT_CO_INS_TYPE") %>' Visible="false"></asp:Label>
                                                    <asp:UpdatePanel runat="server" ID="UpdatePanel3"
                                                        UpdateMode="Conditional" ChildrenAsTriggers="true">
                                                        <ContentTemplate>
                                                            <asp:DropDownList ID="drpCoInsType" CssClass="LabelStyle" runat="server" Style="border: none;" Width="50px" Height="30px" AutoPostBack="true" OnSelectedIndexChanged="drpCoInsType_SelectedIndexChanged">
                                                                <asp:ListItem Value="$">$</asp:ListItem>
                                                                <asp:ListItem Value="%">%</asp:ListItem>
                                                            </asp:DropDownList>

                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="drpCoInsType" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Co-Ins" HeaderStyle-Width="70px">
                                                <ItemTemplate>
                                                    <asp:UpdatePanel runat="server" ID="UpdatePanel1"
                                                        UpdateMode="Conditional" ChildrenAsTriggers="true">
                                                        <ContentTemplate>
                                                            <asp:TextBox ID="txtCoInsAmt" runat="server" CssClass="label" Style="text-align: right; padding-right: 5px; border: none;" Width="70px" Height="30px" Text='<%# Bind("HIT_CO_INS_AMT")%>' AutoPostBack="true" OnTextChanged="gvInvoiceTrans_TextChanged" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="txtCoInsAmt" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>


                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Co-Total" HeaderStyle-Width="70px">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtgvCoInsTotal" runat="server" CssClass="label" Style="text-align: right; padding-right: 5px; border: none;" Width="70px" Height="30px" Enabled="false" Text='<%# Bind("HIT_CO_TOTAL") %>'></asp:TextBox>

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Comp. Type" HeaderStyle-Width="70px">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtgvCompType" runat="server" CssClass="label" Style="text-align: right; padding-right: 5px; border: none;" Width="70px" Height="30px" Enabled="false" Text='<%# Bind("HIT_COMP_TYPE") %>'></asp:TextBox>

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Comp. Amount" HeaderStyle-Width="70px">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtgvCompAmount" runat="server" CssClass="label" Style="text-align: right; padding-right: 5px; border: none;" Width="70px" Height="30px" Enabled="false" Text='<%# Bind("HIT_COMP_AMT") %>'></asp:TextBox>

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Comp Total" HeaderStyle-Width="70px">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtgvCompTotal" runat="server" CssClass="label" Style="text-align: right; padding-right: 5px; border: none;" Width="70px" Height="30px" Enabled="false" Text='<%# Bind("HIT_COMP_TOTAL") %>'></asp:TextBox>

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Dr.Code" HeaderStyle-Width="100">
                                                <ItemTemplate>
                                                    <asp:UpdatePanel runat="server" ID="UpdatePanel7"
                                                        UpdateMode="Conditional" ChildrenAsTriggers="true">
                                                        <ContentTemplate>
                                                            <asp:TextBox ID="txtgvDrCode" runat="server" CssClass="label" Style="border: none;" Width="100px" Height="30px" Text='<%# Bind("HIT_DR_CODE") %>' AutoPostBack="true" OnTextChanged="txtgvDrCode_TextChanged"></asp:TextBox>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="txtgvDrCode" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </ItemTemplate>

                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Dr.Name" HeaderStyle-Width="150">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtgvDrName" runat="server" CssClass="label" Style="border: none;" Width="150px" Height="30px" Text='<%# Bind("HIT_DR_NAME") %>'></asp:TextBox>
                                                </ItemTemplate>

                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="Serv. Category">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtgvCatID" runat="server" CssClass="label" Style="border: none;" Width="100px" Height="30px" Text='<%# Bind("HIT_CAT_ID") %>'></asp:TextBox>
                                                    <asp:Label ID="lblgvCatID" Width="100px" CssClass="label" runat="server" Text='<%# Bind("HIT_CAT_ID") %>' Visible="false"></asp:Label>
                                                </ItemTemplate>

                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Serv. Treatment Type" ShowHeader="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblServTreatType" Width="100px" CssClass="label" runat="server" Visible="false"></asp:Label>
                                                </ItemTemplate>

                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Covered" ShowHeader="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCoverd" Width="100px" CssClass="label" runat="server" Text='<%# Bind("HIT_COVERED") %>' Visible="false"></asp:Label>
                                                </ItemTemplate>

                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Serv. Type" ShowHeader="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblServType" Width="100px" CssClass="label" runat="server" Text='<%# Bind("HIT_SERV_TYPE") %>' Visible="false"></asp:Label>
                                                </ItemTemplate>

                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="RefNo." ShowHeader="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblgvRefNo" Width="100px" CssClass="label" runat="server" Text='<%# Bind("HIT_REFNO") %>' Visible="false"></asp:Label>
                                                </ItemTemplate>

                                            </asp:TemplateField>




                                            <asp:TemplateField HeaderText="TeethNo." ShowHeader="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblgvTeethNo" Width="100px" CssClass="label" runat="server" Text='<%# Bind("HIT_TEETHNO") %>' Visible="false"></asp:Label>
                                                </ItemTemplate>

                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Haad Code">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtHaadCode" runat="server" CssClass="label" Style="border: none;" Width="70px" Height="30px" Text='<%# Bind("HIT_HAAD_CODE") %>'></asp:TextBox>

                                                </ItemTemplate>

                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Haad Desc">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtHaadDesc" runat="server" CssClass="label" Style="border: none;" Width="150px" Height="30px"></asp:TextBox>

                                                </ItemTemplate>

                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Haad Type">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtTypeValue" runat="server" CssClass="label" Style="text-align: center; border: none;" Width="70px" Height="30px" ReadOnly="true" Text='<%# Bind("HIT_TYPE_VALUE") %>'></asp:TextBox>

                                                </ItemTemplate>

                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Cost Price" Visible="False">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtgvCostPrice" runat="server" CssClass="label" Style="text-align: right; padding-right: 5px; border: none;" Width="70px" Height="30px" Enabled="false" Text='<%# Bind("HIT_COSTPRICE") %>'></asp:TextBox>
                                                </ItemTemplate>

                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Authorization ID" HeaderStyle-Width="100px">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtgvAuthID" runat="server" CssClass="label" Style="border: none;" Width="100px" Height="30px" Text='<%# Bind("HIT_AUTHORIZATIONID") %>'></asp:TextBox>
                                                </ItemTemplate>

                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Start Date" HeaderStyle-Width="100px">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtgcStartDt" runat="server" CssClass="label" Style="border: none;" Width="100px" Height="30px" Text='<%# Bind("HIT_STARTDATEDesc") %>'></asp:TextBox>
                                                </ItemTemplate>

                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Observ.Type" HeaderStyle-Width="70px">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtObsType" runat="server" CssClass="label" Style="border: none;" Width="100px" Height="30px" Text='<%# Bind("HIO_TYPE") %>' ReadOnly="true"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Observ.Code" HeaderStyle-Width="70px">
                                                <ItemTemplate>
                                                    <asp:UpdatePanel runat="server" ID="UpdatePanel8"
                                                        UpdateMode="Conditional" ChildrenAsTriggers="true">
                                                        <ContentTemplate>
                                                            <asp:TextBox ID="txtObsCode" runat="server" CssClass="label" Style="border: none;" Width="70px" Height="30px" Text='<%# Bind("HIO_CODE") %>' AutoPostBack="true" OnTextChanged="txtObsCode_TextChanged"></asp:TextBox>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="txtObsCode" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Observ.Description" HeaderStyle-Width="70px">
                                                <ItemTemplate>
                                                    <asp:UpdatePanel runat="server" ID="UpdatePanel9"
                                                        UpdateMode="Conditional" ChildrenAsTriggers="true">
                                                        <ContentTemplate>
                                                            <asp:TextBox ID="txtObsDesc" runat="server" CssClass="label" Style="border: none;" Width="120px" Height="30px" Text='<%# Bind("HIO_DESCRIPTION") %>' AutoPostBack="true" OnTextChanged="txtObsCode_TextChanged"></asp:TextBox>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="txtObsDesc" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Observ.Value" HeaderStyle-Width="70px">
                                                <ItemTemplate>
                                                    <asp:UpdatePanel runat="server" ID="UpdatePanel10"
                                                        UpdateMode="Conditional" ChildrenAsTriggers="true">
                                                        <ContentTemplate>
                                                            <asp:TextBox ID="txtObsValue" runat="server" CssClass="label" Style="border: none;" Width="70px" Height="30px" Text='<%# Bind("HIO_VALUE") %>' AutoPostBack="true" OnTextChanged="txtObsCode_TextChanged"></asp:TextBox>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="txtObsValue" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Observ.ValueType" HeaderStyle-Width="100px">
                                                <ItemTemplate>
                                                    <asp:UpdatePanel runat="server" ID="UpdatePanel11"
                                                        UpdateMode="Conditional" ChildrenAsTriggers="true">
                                                        <ContentTemplate>
                                                            <asp:TextBox ID="txtObsValueType" runat="server" CssClass="label" Style="border: none;" Width="100px" Height="30px" Text='<%# Bind("HIO_VALUETYPE") %>' AutoPostBack="true" OnTextChanged="txtObsCode_TextChanged"></asp:TextBox>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="txtObsValueType" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>


                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Delete">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="DeleteDiag" runat="server" ToolTip="Delete" ImageUrl="~/Images/icon_delete.jpg" Width="16px" Height="16px"
                                                        OnClick="DeletegvInv_Click" />&nbsp;&nbsp;
                                                
                                                </ItemTemplate>
                                                <HeaderStyle Width="50px" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>

                                        </Columns>
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </div>

                    <div id="divTotalAmount" runat="server" style="padding-top: 0px; width: 800px; overflow: auto; border: thin; border-color: #CCCCCC; border-style: groove;">
                        <table width="100%" border="0">
                            <tr>
                                <td class="lblCaption1" style="width: 90px;">Bill Amt(1)
                                </td>
                                <td>
                                    <asp:TextBox ID="txtBillAmt1" runat="server" Height="22px" Style="text-align: right;" Enabled="false" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="80px" Text="0.00" MaxLength="10" AutoPostBack="true" OnTextChanged="txtInvoiceNo_TextChanged"></asp:TextBox>

                                </td>
                                <td class="lblCaption1" style="width: 80px;">Bill Amt(2)
                                </td>
                                <td>
                                    <asp:TextBox ID="txtBillAmt2" runat="server" Height="22px" Style="text-align: right;" Enabled="false" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="70px" Text="0.00" MaxLength="10" AutoPostBack="true" OnTextChanged="txtInvoiceNo_TextChanged"></asp:TextBox>

                                </td>
                                <td class="lblCaption1" style="width: 80px;">Bill Amt(3)
                                </td>
                                <td>
                                    <asp:TextBox ID="txtBillAmt3" runat="server" Height="22px" Style="text-align: right;" Enabled="false" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="70px" Text="0.00" MaxLength="10" AutoPostBack="true" OnTextChanged="txtInvoiceNo_TextChanged"></asp:TextBox>

                                </td>
                                <td class="lblCaption1" style="width: 60px;">Claim Amt
                                </td>
                                <td>
                                    <asp:TextBox ID="txtClaimAmt" runat="server" Height="22px" Style="text-align: right;" Enabled="false" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="70px" Text="0.00" MaxLength="10" AutoPostBack="true" OnTextChanged="txtInvoiceNo_TextChanged"></asp:TextBox>

                                </td>
                                <td class="lblCaption1" style="width: 70px;">Bill Amt(4)
                                </td>
                                <td>
                                    <asp:TextBox ID="txtBillAmt4" runat="server" Height="22px" Style="text-align: right;" Enabled="false" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="70px" Text="0.00" MaxLength="10" AutoPostBack="true" OnTextChanged="txtInvoiceNo_TextChanged"></asp:TextBox>

                                </td>
                            </tr>
                            <tr>
                                <td class="lblCaption1" style="width: 90px;">Hosp.Disc
                                </td>
                                <td>
                                    <asp:DropDownList ID="drpHospDiscType" CssClass="LabelStyle" Font-Size="10px" runat="server" Width="35px" Height="27px">
                                        <asp:ListItem Value="$">$</asp:ListItem>
                                        <asp:ListItem Value="%">%</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:TextBox ID="txtHospDisc" runat="server" Height="22px" Style="text-align: right;" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="40px" MaxLength="10" Text="0.00" AutoPostBack="true" OnTextChanged="txtHospDisc_TextChanged" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                    <asp:TextBox ID="txtHospDiscAmt" runat="server" Height="22px" Style="text-align: right;" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" MaxLength="10" Text="0.00" Visible="false"></asp:TextBox>

                                </td>
                                <td class="lblCaption1" style="width: 80px;">Deductible
                                </td>
                                <td>
                                    <asp:TextBox ID="txtDeductible" runat="server" Height="22px" Enabled="false" Style="text-align: right;" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="70px" Text="0.00" MaxLength="10"></asp:TextBox>

                                </td>
                                <td class="lblCaption1" style="width: 80px;">Co-Ins.Total
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCoInsTotal" runat="server" Height="22px" Enabled="false" Style="text-align: right;" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Text="0.00" Width="70px" MaxLength="10"></asp:TextBox>

                                </td>
                                <td class="lblCaption1" style="width: 60px;">Spl.Disc
                                </td>
                                <td>
                                    <asp:TextBox ID="txtSplDisc" runat="server" Height="22px" Style="text-align: right;" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="70px" MaxLength="10" Text="0.00" AutoPostBack="true" OnTextChanged="txtSplDisc_TextChanged" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                                </td>
                                <td class="lblCaption1" style="width: 70px;">PT Credit
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPTCredit" runat="server" Height="22px" Style="text-align: right;" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="70px" MaxLength="10" Text="0.00" AutoPostBack="true" OnTextChanged="txtSplDisc_TextChanged" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                                </td>
                            </tr>
                            <tr>
                                <td colspan="1">
                                    <asp:CheckBox ID="chkPrintCrBill" runat="server" CssClass="lblCaption1" Text="Print Cr. Bill" Checked="false" />

                                </td>
                                <td>
                                    <asp:CheckBox ID="chkOnlySave" runat="server" CssClass="lblCaption1" Text="Only Save" Checked="true" />

                                </td>
                                <td class="lblCaption1" style="width: 80px;">Print Type
                                </td>
                                <td>
                                    <asp:DropDownList ID="drpPrintType" CssClass="LabelStyle" runat="server" Width="70px">
                                        <asp:ListItem Value="" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="Gross">Gross</asp:ListItem>
                                        <asp:ListItem Value="Net">Net</asp:ListItem>
                                        <asp:ListItem Value="Print1">Print1</asp:ListItem>
                                        <asp:ListItem Value="Print2">Print2</asp:ListItem>
                                        <asp:ListItem Value="Print3">Print3</asp:ListItem>
                                    </asp:DropDownList>


                                </td>

                                <td>
                                    <asp:Button ID="btnEditPTPopup" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 70px;" CssClass="button gray small" Height="25px" Text="Edit Patient" OnClick="btnEditPTPopup_Click" Visible="false" />
                                </td>
                                <td colspan="2">&nbsp;<asp:Button ID="btnPrntDeductible" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 70px;" CssClass="button gray small" Visible="true" OnClick="btnPrntDeductible_Click" Text="Deductible" />

                                </td>
                                <td>
                                    <asp:Button ID="btnPrint" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 70px;" CssClass="button gray small" Visible="true" OnClick="btnPrint_Click" Text="Print" OnClientClick="return InvSaveVal();" />

                                </td>
                                <td class="lblCaption1" style="width: 70px;">Paid Amt
                                </td>
                                <td>
                                    <asp:TextBox ID="TxtRcvdAmt" runat="server" Height="22px" Style="text-align: right;" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="70px" MaxLength="10" Text="0.00" AutoPostBack="false" OnTextChanged="TxtRcvdAmt_TextChanged" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                                    <asp:TextBox ID="txtPaidAmount" runat="server" Height="22px" Style="text-align: right;" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="70px" MaxLength="10" Text="0.00" Visible="false" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:CheckBox ID="chkNoMergePrint" runat="server" CssClass="lblCaption1" Text="No Merge Print" Checked="false" Visible="false" />&nbsp;&nbsp;

                                </td>
                            </tr>
                        </table>
                    </div>
                    <div id="divPayType" runat="server" style="padding-top: 0px; width: 800PX; height: auto; border: thin; border-color: #cccccc; border-style: groove;">

                        <asp:Label ID="Label1" runat="server" CssClass="lblCaption1" Text="Payment" Style="font-weight: bold;"></asp:Label>
                        &nbsp;
                <table style="width: 100%;">
                    <tr>
                        <td class="lblCaption1" style="width: 80px;">Pay.Type</td>
                        <td style="width: 105px;">
                            <asp:DropDownList ID="drpPayType" runat="server" CssClass="label" Width="100px" BorderWidth="1px" BorderColor="#cccccc" AutoPostBack="true" OnSelectedIndexChanged="drpPayType_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td class="lblCaption1" style="width: 70px;">Cash Amt.
                        </td>
                        <td>
                            <asp:TextBox ID="txtCashAmt" runat="server" Width="70px" Height="22px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Text="0.00" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                        </td>
                    </tr>
                </table>
                        <div id="divCC" runat="server" visible="false">
                            <table style="width: 100%;">
                                <tr>
                                    <td class="lblCaption1" style="width: 80px;">Type</td>
                                    <td style="width: 105px;">
                                        <asp:DropDownList ID="drpccType" runat="server" CssClass="label" Width="100px" BorderWidth="1px" BorderColor="#cccccc">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="lblCaption1" style="width: 70px;">C.C Amt.
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtCCAmt" runat="server" Width="70px" Height="22px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" AutoPostBack="true" OnTextChanged="txtCCAmt_TextChanged" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                    </td>
                                    <td class="lblCaption1">C.C No.
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtCCNo" runat="server" Width="70px" Height="22px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc"></asp:TextBox>
                                    </td>
                                    <td class="lblCaption1">Holder
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtCCHolder" runat="server" Width="100px" Height="22px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc"></asp:TextBox>
                                    </td>
                                    <td class="lblCaption1">Ref No.
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtCCRefNo" runat="server" Width="70px" Height="22px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div id="divCheque" runat="server" visible="false">
                            <table style="width: 100%;">
                                <tr>
                                    <td class="lblCaption1" style="width: 80px;">Cheq. No.
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtDcheqno" runat="server" Width="100px" Height="22px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc"></asp:TextBox>
                                    </td>
                                    <td class="lblCaption1" style="width: 70px;">Cheq. Amt.
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtChqAmt" runat="server" Width="70px" Height="22px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" AutoPostBack="true" OnTextChanged="txtChqAmt_TextChanged" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                    </td>
                                    <td class="lblCaption1">Cheq. Date
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtDcheqdate" runat="server" Width="70px" Height="22px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                        <asp:CalendarExtender ID="Calendarextender4" runat="server"
                                            Enabled="True" TargetControlID="txtDcheqdate" Format="dd/MM/yyyy">
                                        </asp:CalendarExtender>

                                    </td>
                                    <td class="lblCaption1">Bank
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="drpBank" CssClass="label" runat="server" Width="250px"></asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </div>


                    </div>

                    <div id="divAdvane" runat="server" visible="false">
                        <fieldset style="width: 800px;">
                            <legend class="lblCaption1">Advance Details</legend>
                            <table style="width: 100%;">
                                <tr>
                                    <td class="lblCaption1">
                                        <asp:ListBox ID="lstAdvDtls" runat="server" CssClass="label" Height="50px" Width="250px"></asp:ListBox>
                                        Avail. Adv
                             <asp:TextBox ID="txtAvailAdv" runat="server" Width="100px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Visible="false" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                        Advance Amt.
                             <asp:TextBox ID="txtAdvAmt" runat="server" Width="100px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Visible="false" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                                    </td>

                                </tr>
                            </table>
                        </fieldset>
                    </div>
                    <div id="divMergeInvoiceNo" runat="server" visible="false">
                        <table style="width: 300px;">

                            <tr>
                                <td class="lblCaption1">Merge Inv. No
                                </td>
                                <td>
                                    <asp:TextBox ID="txtMergeInvoices" runat="server" Width="100px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc"></asp:TextBox>

                                </td>
                            </tr>
                        </table>

                    </div>
                    <div id="divMultiCurrency" runat="server" visible="false">
                        <fieldset style="width: 800px;">
                            <legend class="lblCaption1">Multi Currency</legend>
                            <table style="width: 100%;">
                                <tr>
                                    <td class="lblCaption1">Cur. Type
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="drpCurType" runat="server" CssClass="label" Width="120px" BorderWidth="1px" BorderColor="#cccccc">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="lblCaption1">Rcvd. Amt
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtRcvdAmount" runat="server" Width="100px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                                    </td>
                                    <td class="lblCaption1">Conv. Rate
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtConvRate" runat="server" Width="100px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" AutoPostBack="true" OnTextChanged="txtConvRate_TextChanged"></asp:TextBox>
                                    </td>

                                    <td class="lblCaption1">Cur. Value
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtcurValue" runat="server" Width="100px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc"></asp:TextBox>
                                    </td>
                                    <td class="lblCaption1">Bal. Amt.
                                    </td>
                                    <td>
                                        <asp:Label ID="LblMBalAmt" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label" onkeypress="return OnlyNumeric(event);"></asp:Label>
                                    </td>

                                </tr>
                            </table>
                        </fieldset>
                    </div>
                    <div style="padding-top: 0px; width: 800PX; height: 20px; border: thin; border-color: #cccccc; border-style: groove; background-color: #efefef;">
                        <table width="100%">
                            <tr>
                                <td class="lblCaption1" style="width: 500px;">Last Invoice No - Date:
                            &nbsp;
                                    <asp:Label ID="lblLstInvNoDate" runat="server" CssClass="lblCaption1"></asp:Label>
                                    &nbsp;
                           First Invoice Date:
                                   <asp:Label ID="lblFstInvDate" runat="server" CssClass="lblCaption1"></asp:Label>
                                </td>

                                <td class="lblCaption1">Last Modified User:
                            &nbsp;
                                    <asp:Label ID="lblModifiedUser" runat="server" CssClass="lblCaption1"></asp:Label>
                                </td>

                                <td class="lblCaption1">Created User :
                            &nbsp;
                                    <asp:Label ID="lblCreatedUser" runat="server" CssClass="lblCaption1"></asp:Label>
                                </td>



                            </tr>

                        </table>
                    </div>
                    <table style="width: 100%;">
                        <tr>
                            <td class="lblCaption1" style="width: 150px; height: 40px;">PriorAuthorization ID</td>
                            <td style="width: 200px;">
                                <asp:TextBox ID="txtPriorAuthorizationID" runat="server" Width="183px" Height="22px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc"></asp:TextBox>

                            </td>
                            <td>
                                <asp:CheckBox ID="ChkTopupCard" runat="server" CssClass="lblCaption1" Text="Topup Card" Checked="false" Enabled="false" />
                                <asp:TextBox ID="TxtTopupAmt" runat="server" Width="100px" Height="22px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Text="0.00" Enabled="false" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox ID="chkReadyforEclaim" runat="server" CssClass="lblCaption1" Text="Ready for Eclaim" Checked="true" Visible="false" />
                            </td>

                        </tr>
                    </table>


                    <asp:LinkButton ID="lnkVisitMessage" runat="server" CssClass="LabelStyle" Text="" Enabled="false"
                        ForeColor="Blue"></asp:LinkButton>
                    <asp:ModalPopupExtender ID="ModPopExtVisits" runat="server" TargetControlID="lnkVisitMessage"
                        PopupControlID="pnlVisit" BackgroundCssClass="modalBackground" CancelControlID="btnVisitClose"
                        PopupDragHandleControlID="pnlVisit">
                    </asp:ModalPopupExtender>
                    <asp:Panel ID="pnlVisit" runat="server" Height="250px" Width="680px" CssClass="modalPopup">
                        <table cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td align="right">
                                    <asp:Button ID="btnVisitClose" runat="server" Text=" X " Font-Bold="true" CssClass="ButtonStyle" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="height: 205px;">


                                    <div style="padding-top: 0px; width: 650px; height: 200px; overflow: auto; border-color: #e3f7ef; border-style: groove;">

                                        <asp:GridView ID="gvVisit" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
                                            EnableModelValidation="True" Width="99%" PageSize="50">
                                            <HeaderStyle CssClass="GridHeader" />
                                            <RowStyle CssClass="GridRow" />
                                            <AlternatingRowStyle CssClass="GridAlterRow" />
                                            <Columns>


                                                <asp:TemplateField HeaderText="File No">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="LinkButton1" runat="server" OnClick="SelectVisit_Click">
                                                            <asp:Label ID="lblVisitID" runat="server" Text='<%# Bind("HPV_SEQNO") %>' Visible="false"></asp:Label>
                                                            <asp:Label ID="lblVisitEMR_ID" runat="server" Text='<%# Bind("HPV_EMR_ID") %>' Visible="false"></asp:Label>
                                                            <asp:Label ID="lblPTId" runat="server" Text='<%# Bind("HPV_PT_ID") %>'></asp:Label>
                                                        </asp:LinkButton>
                                                    </ItemTemplate>

                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Name">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="LinkButton2" runat="server" OnClick="SelectVisit_Click">
                                                            <asp:Label ID="lblPTName" runat="server" Text='<%# Bind("HPV_PT_NAME") %>'></asp:Label>
                                                        </asp:LinkButton>
                                                    </ItemTemplate>

                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Dr.Coe">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="LinkButton3" runat="server" OnClick="SelectVisit_Click">
                                                            <asp:Label ID="lblgvhpvRefDrId" runat="server" Text='<%# Bind("HPV_REFERRAL_TO") %>' Visible="false"></asp:Label>
                                                            <asp:Label ID="lblDrID" runat="server" Text='<%# Bind("HPV_DR_ID") %>'></asp:Label>
                                                        </asp:LinkButton>
                                                    </ItemTemplate>

                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Doctor Name">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="LinkButton4" runat="server" OnClick="SelectVisit_Click">
                                                            <asp:Label ID="lblDRName" runat="server" Text='<%# Bind("HPV_DR_NAME") %>'></asp:Label>
                                                        </asp:LinkButton>
                                                    </ItemTemplate>

                                                </asp:TemplateField>

                                            </Columns>

                                        </asp:GridView>


                                    </div>


                                </td>
                            </tr>

                        </table>
                    </asp:Panel>
                </ContentTemplate>

            </asp:TabPanel>

            <asp:TabPanel runat="server" ID="TabPanel13" HeaderText="Diagnosis" Width="100%">
                <ContentTemplate>
                    <table width="100%">
                        <tr>
                            <td style="width: 100px; height: 30px;" class="lblCaption1">Type
                            </td>
                            <td style="width: 275px;">
                                <asp:UpdatePanel ID="UpdatePanel37" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="drpClmType" runat="server" CssClass="label" Width="270px">
                                            <asp:ListItem Value="1" Selected="True">No Bed + No Emergency Room (OP)</asp:ListItem>
                                            <asp:ListItem Value="2">No Bed + Emergency Room (OP)</asp:ListItem>
                                            <asp:ListItem Value="3">InPatient Bed + No Emergency Room (IP)</asp:ListItem>
                                            <asp:ListItem Value="4">InPatient Bed + Emergency Room (IP)</asp:ListItem>
                                            <asp:ListItem Value="5">Daycase Bed + No Emergency Room (Day Care)</asp:ListItem>
                                            <asp:ListItem Value="6">Daycase Bed + Emergency Room (Day Care)</asp:ListItem>
                                            <asp:ListItem Value="7">National Screening</asp:ListItem>
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>

                            </td>
                            <td style="height: 40px; width: 70px;" class="lblCaption1">Start Date  <span style="color: red;">* </span>
                            </td>
                            <td style="width: 130px;">
                                <asp:UpdatePanel ID="UpdatePanel33" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtClmStartDate" runat="server" Width="75px" Height="22PX" MaxLength="10" BorderWidth="1px" BorderColor="#cccccc" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                        <asp:CalendarExtender ID="CalendarExtender2" runat="server"
                                            Enabled="True" TargetControlID="txtClmStartDate" Format="dd/MM/yyyy">
                                        </asp:CalendarExtender>
                                        <asp:MaskedEditExtender ID="MaskedEditExtender5" runat="server" Enabled="true" TargetControlID="txtClmStartDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                                        <asp:TextBox ID="txtClmFromTime" runat="server" Width="50px" Height="22PX" MaxLength="10" BorderWidth="1px" BorderColor="#cccccc" onkeypress="return OnlyNumeric(event);"></asp:TextBox>


                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td class="lblCaption1" style="width: 70px;">End Date  <span style="color: red;">* </span>
                            </td>
                            <td style="width: 130px;">
                                <asp:UpdatePanel ID="UpdatePanel34" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtClmEndDate" runat="server" Width="75px" Height="22PX" MaxLength="10" BorderWidth="1px" BorderColor="#cccccc" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                        <asp:CalendarExtender ID="CalendarExtender1" runat="server"
                                            Enabled="True" TargetControlID="txtClmEndDate" Format="dd/MM/yyyy">
                                        </asp:CalendarExtender>
                                        <asp:MaskedEditExtender ID="MaskedEditExtender6" runat="server" Enabled="true" TargetControlID="txtClmEndDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                                        <asp:TextBox ID="txtClmToTime" runat="server" Width="50px" Height="22PX" MaxLength="10" BorderWidth="1px" BorderColor="#cccccc" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>


                        </tr>
                        <tr>
                            <td style="height: 30px;" class="lblCaption1">Start Type
                            </td>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel35" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="drpClmStartType" runat="server" CssClass="label" Width="270px">
                                            <asp:ListItem Value="1" Selected="True">Elective</asp:ListItem>
                                            <asp:ListItem Value="2">Emergency</asp:ListItem>
                                            <asp:ListItem Value="3">Transfer</asp:ListItem>
                                            <asp:ListItem Value="4">Live Birth</asp:ListItem>
                                            <asp:ListItem Value="5">Still Birth</asp:ListItem>
                                            <asp:ListItem Value="6">Dead on Arrival</asp:ListItem>
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td class="lblCaption1">End Type 
                            </td>
                            <td colspan="3">
                                <asp:UpdatePanel ID="UpdatePanel36" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="drpClmEndType" runat="server" CssClass="label" Width="200px">
                                            <asp:ListItem Value="1" Selected="True">Discharged with approval</asp:ListItem>
                                            <asp:ListItem Value="2">Discharged against advice</asp:ListItem>
                                            <asp:ListItem Value="3">Discharged absent without leave</asp:ListItem>
                                            <asp:ListItem Value="4">Transfer to another facility</asp:ListItem>
                                            <asp:ListItem Value="5">Deceased</asp:ListItem>

                                        </asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>

                            </td>
                        </tr>

                    </table>
                    <table width="700px">
                        <tr>
                            <td class="lblCaption1" style="width: 105px;">Type <span style="color: red;">* </span>
                            </td>
                            <td class="lblCaption1" style="width: 105px;">Code <span style="color: red;">* </span>
                            </td>
                            <td class="lblCaption1">Description
                            </td>



                        </tr>
                        <tr>
                            <td>
                                <asp:DropDownList ID="drpDiagType" runat="server" CssClass="label" Width="100px" BorderWidth="1px" BorderColor="#cccccc">
                                    <asp:ListItem Value="Principal" Selected="True">Principal</asp:ListItem>
                                    <asp:ListItem Value="Secondary">Secondary</asp:ListItem>
                                    <asp:ListItem Value="Admitting">Admitting</asp:ListItem>

                                </asp:DropDownList>
                            </td>
                            <td>

                                <asp:TextBox ID="txtDiagCode" runat="server" Width="100px" Height="22px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" onblur="return DiagIdSelected()"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="txtDiagName" runat="server" Width="400px" Height="22px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" onblur="return DiagNameSelected()"></asp:TextBox>

                                <div id="divDiagExt" style="visibility: hidden;"></div>
                                <asp:AutoCompleteExtender ID="AutoCompleteExtender7" runat="Server" TargetControlID="txtDiagCode" MinimumPrefixLength="1" ServiceMethod="GetDiagnosisID"
                                    CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divDiagExt">
                                </asp:AutoCompleteExtender>
                                <asp:AutoCompleteExtender ID="AutoCompleteExtender8" runat="Server" TargetControlID="txtDiagName" MinimumPrefixLength="1" ServiceMethod="GetDiagnosisName"
                                    CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divDiagExt">
                                </asp:AutoCompleteExtender>


                            </td>

                            <td>
                                <asp:Button ID="btnInvDiagAdd" runat="server" Style="padding-left: 2px; padding-right: 2px; width: 50px;" CssClass="button gray small"
                                    OnClick="btnInvDiagAdd_Click" Text="Add" OnClientClick="return DiagAddVal();" />

                            </td>
                        </tr>

                    </table>
                    <table width="98%">
                        <tr>
                            <td>
                                <asp:GridView ID="gvInvoiceClaims" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                    EnableModelValidation="True" Width="95%" gridline="none">
                                    <HeaderStyle CssClass="GridHeader" />
                                    <RowStyle CssClass="GridRow" />

                                    <Columns>
                                        <asp:TemplateField HeaderText="Code" HeaderStyle-Width="110px">
                                            <ItemTemplate>

                                                <asp:Label ID="lblICDCode" Width="100px" CssClass="label" runat="server" Text='<%# Bind("HIC_ICD_CODE") %>'></asp:Label>

                                            </ItemTemplate>

                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Description">
                                            <ItemTemplate>

                                                <asp:Label ID="lblICDDesc" Width="400px" CssClass="label" runat="server" Text='<%# Bind("HIC_ICD_DESC") %>'></asp:Label>

                                            </ItemTemplate>

                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Type">
                                            <ItemTemplate>

                                                <asp:Label ID="lblICDType" Width="70px" CssClass="label" runat="server" Text='<%# Bind("HIC_ICD_TYPE") %>'></asp:Label>

                                            </ItemTemplate>

                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Delete">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="ImageButton1" runat="server" ToolTip="Delete" ImageUrl="~/Images/icon_delete.jpg" Width="16px" Height="16px"
                                                    OnClick="DeletegvDiag_Click" />&nbsp;&nbsp;
                                                
                                            </ItemTemplate>
                                            <HeaderStyle Width="50px" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>





                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:TabPanel>


        </asp:TabContainer>
        <asp:LinkButton ID="lnkMessage" runat="server" CssClass="LabelStyle" Text="" Enabled="false"
            ForeColor="Blue"></asp:LinkButton>
        <asp:ModalPopupExtender ID="MPExtMessage" runat="server" TargetControlID="lnkMessage"
            PopupControlID="pnlPrevPrice" BackgroundCssClass="modalBackground" CancelControlID="btnClose"
            PopupDragHandleControlID="pnlPrevPrice">
        </asp:ModalPopupExtender>
        <asp:Panel ID="pnlPrevPrice" runat="server" Height="250px" Width="550px" CssClass="modalPopup">
            <table cellpadding="0" cellspacing="0" width="100%" style="background-color: maroon;">
                <tr>
                    <td align="right">
                        <asp:Button ID="btnClose" runat="server" Text=" X " Font-Bold="true" CssClass="ButtonStyle" />
                    </td>
                </tr>
                <tr>
                    <td align="center" style="height: 230px; color: #ffffff; font-weight: bold;">
                        <%=strMessage %>
                    </td>
                </tr>
            </table>
        </asp:Panel>
       
    </div>
    <br />
    <br />
    <style type="text/css">
        .style1
        {
            width: 20px;
            height: 32px;
        }

        .modalBackground
        {
            background-color: black;
            filter: alpha(opacity=40);
            opacity: 0.5;
        }

        .modalPopup
        {
            background-color: #ffffff;
            border-width: 3px;
            border-style: solid;
            border-color: Gray;
            padding: 3px;
            width: 250px;
            top: -1000px;
            position: absolute;
            padding: 0px10px10px10px;
        }
    </style>

</asp:Content>
