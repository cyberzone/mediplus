﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using Mediplus_BAL;
using System.Collections;
using System.Text;

namespace Mediplus.HMS.eAuthorization
{
    public partial class eAuthApproval : System.Web.UI.Page
    {
        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }


        void BindEMRPTMaster()
        {


            string Criteria = " 1=1 ";

            if (chkEMR.Checked == true)
            {
                Criteria += " AND EPM_INS_CODE <> ''  AND EPM_INS_CODE IS NOT NULL AND EPM_ID IS NOT NULL  ";
            }
            else
            {
                Criteria += " AND HPM_INS_COMP_ID <> ''  AND HPM_INS_COMP_ID IS NOT NULL AND HPM_PT_TYPE='CR'  ";
            }




            //Criteria += " AND HPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND HPM_BRANCH_ID='" + hidBranchID.Value + "'";

            if (txtId.Text.Trim() != "")
            {
                if (chkEMR.Checked == true)
                {
                    Criteria += " AND EPM_ID  = '" + txtId.Text.Trim() + "' ";
                }

            }


            if (txtSrcName.Text.Trim() != "")
            {



                if (chkEMR.Checked == true)
                {
                    Criteria += " AND EPM_PT_NAME  like '%" + txtSrcName.Text.Trim() + "%' ";
                }
                else
                {
                    Criteria += " AND HPM_PT_FNAME + ' ' +ISNULL(HPM_PT_MNAME,'') + ' '  + ISNULL(HPM_PT_LNAME,'')   LIKE '%" + txtSrcName.Text + "%' ";
                }
            }



            if (txtSrcFileNo.Text.Trim() != "")
            {


                if (chkEMR.Checked == true)
                {
                    Criteria += " AND EPM_PT_ID  = '" + txtSrcFileNo.Text.Trim() + "' ";
                }
                else
                {
                    Criteria += " AND HPM_PT_ID  = '" + txtSrcFileNo.Text.Trim() + "' ";
                }

            }


            if (drpDoctor.SelectedIndex != 0)
            {

                if (chkEMR.Checked == true)
                {
                    Criteria += " AND EPM_DR_CODE  = '" + drpDoctor.SelectedValue + "' ";
                }
                else
                {
                    Criteria += " AND HPM_DR_ID  = '" + drpDoctor.SelectedValue + "' ";
                }

            }



            string strStartDate = txtFromDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            if (txtFromDate.Text != "")
            {
                if (chkEMR.Checked == true)
                {
                    Criteria += " AND   CONVERT(datetime,convert(varchar(10),EPM_DATE,101),101) >= '" + strForStartDate + "'";
                }
                else
                {
                    Criteria += " AND   CONVERT(datetime,convert(varchar(10),HPM_MODIFIED_DATE,101),101) >= '" + strForStartDate + "'";
                }
            }


            string strTotDate = txtToDate.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }


            if (txtToDate.Text != "")
            {
                if (chkEMR.Checked == true)
                {
                    Criteria += " AND   CONVERT(datetime,convert(varchar(10),EPM_DATE,101),101) <= '" + strForToDate + "'";
                }
                else
                {
                    Criteria += " AND   CONVERT(datetime,convert(varchar(10),HPM_MODIFIED_DATE,101),101) <= '" + strForToDate + "'";
                }
            }

            if (chkEMR.Checked == true)
            {
                Criteria += " AND  EPM_ID NOT IN (SELECT ISNULL(HAR_EMR_ID,0) FROM HMS_AUTHORIZATION_REQUESTS WHERE HAR_TYPE='A' ) ";
            }

            DataSet DS = new DataSet();


            if (chkEMR.Checked == true)
            {
                eAuthorizationBAL objeAuth = new eAuthorizationBAL();
                DS = objeAuth.AuthorizationPTEMRDtlsGet(Criteria);

            }
            else
            {
                dboperations objDB = new dboperations();
                DS = objDB.retun_patient_details(Criteria);

                DataTable DT = new DataTable();
                DT = DS.Tables[0];

                DataColumn EPM_ID = new DataColumn();
                EPM_ID.ColumnName = "EPM_ID";

                DataColumn EPM_DATEDesc = new DataColumn();
                EPM_DATEDesc.ColumnName = "EPM_DATEDesc";

                DT.Columns.Add(EPM_ID);
                DT.Columns.Add(EPM_DATEDesc);




            }

            if (DS.Tables[0].Rows.Count > 0)
            {
                gvPTMaster.DataSource = DS;
                gvPTMaster.DataBind();

            }
            else
            {
                lblStatus.Text = " No Data";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                gvPTMaster.DataBind();
            }

        }

        void BindCompany()
        {

            //string Criteria = " 1=1 AND HCM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            string Criteria = " 1=1 AND HCM_BRANCH_ID='" + hidBranchID.Value + "'";


            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_inccmp_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpCompany.DataSource = ds;
                drpCompany.DataValueField = "HCM_COMP_ID";
                drpCompany.DataTextField = "HCM_NAME";
                drpCompany.DataBind();

            }
            drpCompany.Items.Insert(0, "--- Select---");
            drpCompany.Items[0].Value = "0";


        }

        void BindDoctor()
        {

            // string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND HSFM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND HSFM_BRANCH_ID='" + hidBranchID.Value + "'";

            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_doctor_detailss(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpDoctor.DataSource = ds;
                drpDoctor.DataValueField = "HSFM_STAFF_ID";
                drpDoctor.DataTextField = "FullName";
                drpDoctor.DataBind();

                drpeAuthDoctor.DataSource = ds;
                drpeAuthDoctor.DataValueField = "HSFM_MOH_NO";
                drpeAuthDoctor.DataTextField = "FullName";
                drpeAuthDoctor.DataBind();



            }
            drpDoctor.Items.Insert(0, "--- All ---");
            drpDoctor.Items[0].Value = "0";

            drpeAuthDoctor.Items.Insert(0, "--- All ---");
            drpeAuthDoctor.Items[0].Value = "0";


        }

        void BindPatientHistory()
        {

            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " EPM_ID='" + hidEmrID.Value + "'  AND EPM_CC IS NOT NULL ";

            DS = objCom.fnGetFieldValue("TOP 1 EPM_CC", "EMR_PT_MASTER", Criteria, "");
            if (DS.Tables[0].Rows.Count > 0)
            {

                txtCC.Text = Convert.ToString(DS.Tables[0].Rows[0]["EPM_CC"]);

            }

        }

        void BuildDiagnosis()
        {
            DataTable dt = new DataTable();

            DataColumn EPD_ID = new DataColumn();
            EPD_ID.ColumnName = "EPD_ID";

            DataColumn EPD_DIAG_CODE = new DataColumn();
            EPD_DIAG_CODE.ColumnName = "EPD_DIAG_CODE";

            DataColumn EPD_DIAG_NAME = new DataColumn();
            EPD_DIAG_NAME.ColumnName = "EPD_DIAG_NAME";


            dt.Columns.Add(EPD_ID);
            dt.Columns.Add(EPD_DIAG_CODE);
            dt.Columns.Add(EPD_DIAG_NAME);


            ViewState["Diagnosis"] = dt;
        }

        void BindDiagnosis()
        {
            string Criteria = " 1=1 ";
            // Criteria += " AND EPD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND EPD_BRANCH_ID='" + hidBranchID.Value + "'";
            Criteria += " AND EPD_ID='" + hidEmrID.Value + "'";

            DataSet DS = new DataSet();
            EMR_BAL.EMR_PTDiagnosis objDiag = new EMR_BAL.EMR_PTDiagnosis();
            DS = objDiag.DiagnosisGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                DataTable DT = new DataTable();
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    DT = (DataTable)ViewState["Diagnosis"];

                    DataRow objrow;
                    objrow = DT.NewRow();
                    objrow["EPD_ID"] = Convert.ToString(DS.Tables[0].Rows[i]["EPD_ID"]);
                    objrow["EPD_DIAG_CODE"] = Convert.ToString(DS.Tables[0].Rows[i]["EPD_DIAG_CODE"]);
                    objrow["EPD_DIAG_NAME"] = Convert.ToString(DS.Tables[0].Rows[i]["EPD_DIAG_NAME"]);
                    DT.Rows.Add(objrow);
                }

                ViewState["Diagnosis"] = DT;
            }


        }

        void BindTempDiagnosis()
        {
            DataTable DT = new DataTable();

            DT = (DataTable)ViewState["Diagnosis"];
            if (DT.Rows.Count > 0)
            {
                gvDiagnosis.DataSource = DT;
                gvDiagnosis.DataBind();
            }
            else
            {
                gvDiagnosis.DataBind();
            }

        }


        void BuildActivities()
        {
            DataTable dt = new DataTable();

            DataColumn EMR_ID = new DataColumn();
            EMR_ID.ColumnName = "EMR_ID";

            DataColumn SERV_CODE = new DataColumn();
            SERV_CODE.ColumnName = "SERV_CODE";

            DataColumn SERV_NAME = new DataColumn();
            SERV_NAME.ColumnName = "SERV_NAME";

            DataColumn QTY = new DataColumn();
            QTY.ColumnName = "QTY";

            DataColumn HAA_NET = new DataColumn();
            HAA_NET.ColumnName = "HAA_NET";

            DataColumn HAA_ACTIVITY_START = new DataColumn();
            HAA_ACTIVITY_START.ColumnName = "HAA_ACTIVITY_START";

            DataColumn HAA_ACTIVITY_TYPE = new DataColumn();
            HAA_ACTIVITY_TYPE.ColumnName = "HAA_ACTIVITY_TYPE";

            DataColumn DURATION = new DataColumn();
            DURATION.ColumnName = "DURATION";

            DataColumn DURATION_TYPE = new DataColumn();
            DURATION_TYPE.ColumnName = "DURATION_TYPE";

            DataColumn SERV_ID = new DataColumn();
            SERV_ID.ColumnName = "SERV_ID";


            dt.Columns.Add(EMR_ID);
            dt.Columns.Add(SERV_CODE);
            dt.Columns.Add(SERV_NAME);
            dt.Columns.Add(QTY);
            dt.Columns.Add(HAA_ACTIVITY_START);
            dt.Columns.Add(HAA_ACTIVITY_TYPE);
            dt.Columns.Add(HAA_NET);
            dt.Columns.Add(DURATION);
            dt.Columns.Add(DURATION_TYPE);
            dt.Columns.Add(SERV_ID);



            ViewState["Activities"] = dt;
        }

        void BindActivities()
        {
            string strActivityDate = "";
            string Criteria = " 1=1 ";
            Criteria += " AND EPM_ID='" + hidEmrID.Value + "'";

            EMR_BAL.EMR_PTMasterBAL objEMR = new EMR_BAL.EMR_PTMasterBAL();

            DataSet DSEmrPT = new DataSet();

            DSEmrPT = objEMR.GetEMR_PTMaster(Criteria);

            if (DSEmrPT.Tables[0].Rows.Count > 0)
            {
                strActivityDate = Convert.ToString(DSEmrPT.Tables[0].Rows[0]["EPM_START_DATEDesc"]);

                string[]  strDate= strActivityDate.Split(' ');

                if (strDate.Length > 0)
                {
                    txtActivityDate.Text = strDate[0];

                    if (strDate.Length > 1)
                    {

                    }
                }

            }


            Criteria = " 1=1 ";
            // Criteria += " AND  TREATMENT_TYPE='" + drpTreatmentType.SelectedValue + "'";
            Criteria += " AND EMR_ID='" + hidEmrID.Value + "'";
            DataSet DS = new DataSet();

            eAuthorizationBAL objeAuth = new eAuthorizationBAL();
            DS = objeAuth.eAuthorizationServiceGet(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                DataTable DT = new DataTable();
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    string strHaadCode = "", strServType = "", strServID = "";
                    strHaadCode = Convert.ToString(DS.Tables[0].Rows[i]["SERV_CODE"]);
                    strServID = Convert.ToString(DS.Tables[0].Rows[i]["SERV_ID"]); //hidServID.Value.Trim();

                    Decimal decServPrice = 0;

                    if (strHaadCode != "")
                    {
                        GetHaadServDtls(strHaadCode, out  strServType);
                        //  GetCompanyAgrDtls(strServID, lblInsCode.Text, out  decServPrice);
                        GetCompanyAgrDtls(strServID, drpCompany.SelectedValue, out  decServPrice);

                    }
                    DT = (DataTable)ViewState["Activities"];
                    DataRow objrow;
                    objrow = DT.NewRow();
                    objrow["EMR_ID"] = Convert.ToString(DS.Tables[0].Rows[i]["EMR_ID"]);
                    objrow["SERV_CODE"] = Convert.ToString(DS.Tables[0].Rows[i]["SERV_CODE"]);
                    objrow["SERV_NAME"] = Convert.ToString(DS.Tables[0].Rows[i]["SERV_NAME"]);
                    objrow["QTY"] = Convert.ToString(DS.Tables[0].Rows[i]["QTY"]);

                    decimal decQty = 1, decPrice = 0;
                    if (txtQty.Text.Trim() != "")
                    {
                        decQty = Convert.ToDecimal(txtQty.Text.Trim());
                    }
                    decPrice = decQty * decServPrice;


                    objrow["HAA_NET"] = decPrice;
                    objrow["HAA_ACTIVITY_START"] = txtActivityDate.Text.Trim() + " " + drpActivityHour.SelectedValue + ":" + drpActivityMin.SelectedValue;
                    objrow["HAA_ACTIVITY_TYPE"] = strServType;


                    objrow["DURATION"] = Convert.ToString(DS.Tables[0].Rows[i]["DURATION"]);
                    objrow["DURATION_TYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["DURATION_TYPE"]);



                    DT.Rows.Add(objrow);
                }
                ViewState["Activities"] = DT;

            }

        }

        void BindTempAuthorizationService()
        {
            DataTable DT = new DataTable();

            DT = (DataTable)ViewState["Activities"];
            if (DT.Rows.Count > 0)
            {
                gvActivity.DataSource = DT;
                gvActivity.DataBind();
            }
            else
            {
                gvActivity.DataBind();
            }

        }


        void BuildAttachments()
        {
            DataTable dt = new DataTable();

            DataColumn HAAT_ID = new DataColumn();
            HAAT_ID.ColumnName = "HAAT_ID";

            DataColumn HAAT_FILE_NAME = new DataColumn();
            HAAT_FILE_NAME.ColumnName = "HAAT_FILE_NAME";

            DataColumn HAAT_FILE_NAME_ACTUAL = new DataColumn();
            HAAT_FILE_NAME_ACTUAL.ColumnName = "HAAT_FILE_NAME_ACTUAL";

            dt.Columns.Add(HAAT_ID);
            dt.Columns.Add(HAAT_FILE_NAME);
            dt.Columns.Add(HAAT_FILE_NAME_ACTUAL);

            ViewState["Attachments"] = dt;
        }

        //void BinddAttachments()
        //{
        //    string Criteria = " 1=1 ";
        //    Criteria += " AND HAAT_ID='" + hidEmrID.Value + "'";

        //    DataSet DS = new DataSet();
        //    clsWebReport objWebrpt = new clsWebReport();
        //    DS = objWebrpt.DiagnosisGet(Criteria);
        //    if (DS.Tables[0].Rows.Count > 0)
        //    {
        //        DataTable DT = new DataTable();
        //        for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
        //        {
        //            DT = (DataTable)ViewState["Attachments"];

        //            DataRow objrow;
        //            objrow = DT.NewRow();
        //            objrow["HAAT_ID"] = Convert.ToString(DS.Tables[0].Rows[i]["HAAT_ID"]);
        //            objrow["HAAT_FILE_NAME"] = Convert.ToString(DS.Tables[0].Rows[i]["HAAT_FILE_NAME"]);
        //            DT.Rows.Add(objrow);
        //        }

        //        ViewState["Attachments"] = DT;
        //    }


        //}

        void BindTempAttachments()
        {
            DataTable DT = new DataTable();

            DT = (DataTable)ViewState["Attachments"];
            if (DT.Rows.Count > 0)
            {
                gvAttachments.DataSource = DT;
                gvAttachments.DataBind();
            }
            else
            {
                gvAttachments.DataBind();
            }

        }


        void GetDiagnosisName()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND  DIM_ICD_ID  ='" + txtDiagCode.Text.Trim() + "'";

            DataSet DS = new DataSet();
            dboperations dbo = new dboperations();
            DS = dbo.DrICDMasterGet(Criteria, "10");

            txtDiagName.Text = "";
            if (DS.Tables[0].Rows.Count > 0)
            {
                txtDiagName.Text = Convert.ToString(DS.Tables[0].Rows[0]["DIM_ICD_DESCRIPTION"]);
            }

        }

        void GetDiagnosisCode()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND  DIM_ICD_DESCRIPTION   ='" + txtDiagName.Text.Trim() + "'";

            DataSet DS = new DataSet();
            dboperations dbo = new dboperations();
            DS = dbo.DrICDMasterGet(Criteria, "10");

            txtDiagCode.Text = "";
            if (DS.Tables[0].Rows.Count > 0)
            {
                txtDiagCode.Text = Convert.ToString(DS.Tables[0].Rows[0]["DIM_ICD_ID"]);
            }

        }

        void GetServiceMasterName(string ServCode, out string ServID, out string ServName)
        {
            ServID = "";
            ServName = "";

            string Criteria = " 1=1 AND HSM_STATUS='A' ";
            Criteria += " AND  HSM_HAAD_CODE  ='" + ServCode + "'";

            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();
            DS = dbo.ServiceMasterGet(Criteria);
            txtServName.Text = "";
            if (DS.Tables[0].Rows.Count > 0)
            {
                ServID = Convert.ToString(DS.Tables[0].Rows[0]["HSM_SERV_ID"]);
                ServName = Convert.ToString(DS.Tables[0].Rows[0]["HSM_NAME"]);
            }

        }

        void GetServiceMasterCode()
        {
            string Criteria = " 1=1 AND HSM_STATUS='A' ";
            Criteria += " AND HSM_NAME    ='" + txtServName.Text.Trim() + "'";

            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();
            DS = dbo.ServiceMasterGet(Criteria);
            txtServCode.Text = "";
            if (DS.Tables[0].Rows.Count > 0)
            {
                hidServID.Value = Convert.ToString(DS.Tables[0].Rows[0]["HSM_SERV_ID"]);
                txtServCode.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSM_HAAD_CODE"]);
            }

        }


        void GetHaadServDtls(string ServCode, out string ServType)
        {
            ServType = "3";


            DataSet DS = new DataSet();
            dboperations objDBO = new dboperations();

            string Criteria = " 1=1 ";
            Criteria += " AND HHS_CODE='" + ServCode + "'";
            DS = objDBO.HaadServiceGet(Criteria, "", "100");
            if (DS.Tables[0].Rows.Count > 0)
            {
                ServType = Convert.ToString(DS.Tables[0].Rows[0]["HHS_TYPE_VALUE"]);
            }



        }

        void GetCompanyAgrDtls(string ServCode, string CompanyID, out decimal ServPrice)
        {

            ServPrice = 0;

            DataSet DS = new DataSet();
            dboperations objDBO = new dboperations();

            string Criteria = " 1=1 ";
            Criteria += " AND HAS_SERV_ID='" + ServCode + "' AND HAS_COMP_ID='" + CompanyID + "'";
            DS = objDBO.AgrementServiceGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("HAS_NETAMOUNT") == false)
                {
                    ServPrice = Convert.ToDecimal(DS.Tables[0].Rows[0]["HAS_NETAMOUNT"]);
                }
            }



        }



        void Clear()
        {
            txtOrderDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");

            txtActivityDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
            if (drpActivityHour.Items.Count > 0)
                drpActivityHour.SelectedIndex = 0;
            if (drpActivityMin.Items.Count > 0)
                drpActivityMin.SelectedIndex = 0;

            TabContainer2.ActiveTab = TabPanelDiagnosis;
            drpTreatmentType.SelectedIndex = 0;
            txtDoctor.Text = "";
            tblEncounter.Visible = false;
            txtEncounterSt.Text = "";
            txtEncounterEnd.Text = "";
            drpSTHour.SelectedIndex = 0;
            drpSTMin.SelectedIndex = 0;
            drpFIHour.SelectedIndex = 0;
            drpFIMin.SelectedIndex = 0;

            drpCompany.SelectedIndex = 0;
            txtMemberID.Text = "";
            lblPTName.Text = "";
            lblFileNo.Text = "";

            hidEmrID.Value = "";
            hidPTID.Value = "";
            hidPayerID.Value = "";
            hidReciverID.Value = "";

            BuildDiagnosis();
            BuildActivities();
            BuildAttachments();

            ClearActivity();

            txtCC.Text = "";
            txtManagementPlan.Text = "";
            gvDiagnosis.DataBind();
            gvActivity.DataBind();
            gvAttachments.DataBind();

            if (Convert.ToString(ViewState["SelectIndex"]) != "")
            {
                if (gvPTMaster.Rows.Count > 0)
                    gvPTMaster.Rows[Convert.ToInt32(ViewState["SelectIndex"])].BackColor = System.Drawing.Color.White;
            }
            ViewState["SelectIndex"] = "";

        }

        void ClearActivity()
        {
            hidServID.Value = "";
            txtServCode.Text = "";
            txtServName.Text = "";
            txtQty.Text = "1";
            txtType.Text = "";
            txtToothNo.Text = "";

            lblType.Visible = false;
            txtType.Visible = false;

            lblPrice.Visible = false;
            txtNet.Visible = false;

            ViewState["ActivitySelectIndex"] = "";
        }

        void BuildeAuthDiagnosis()
        {
            DataTable dt = new DataTable();

            DataColumn EPD_ID = new DataColumn();
            EPD_ID.ColumnName = "EPD_ID";

            DataColumn EPD_DIAG_CODE = new DataColumn();
            EPD_DIAG_CODE.ColumnName = "EPD_DIAG_CODE";

            DataColumn EPD_DIAG_NAME = new DataColumn();
            EPD_DIAG_NAME.ColumnName = "EPD_DIAG_NAME";


            dt.Columns.Add(EPD_ID);
            dt.Columns.Add(EPD_DIAG_CODE);
            dt.Columns.Add(EPD_DIAG_NAME);


            ViewState["eAuthDiagnosis"] = dt;
        }

        void BuildeAuthActivities()
        {
            DataTable dt = new DataTable();

            DataColumn EMR_ID = new DataColumn();
            EMR_ID.ColumnName = "EMR_ID";

            DataColumn SERV_CODE = new DataColumn();
            SERV_CODE.ColumnName = "SERV_CODE";

            DataColumn SERV_NAME = new DataColumn();
            SERV_NAME.ColumnName = "SERV_NAME";

            DataColumn QTY = new DataColumn();
            QTY.ColumnName = "QTY";


            DataColumn HAA_NET = new DataColumn();
            HAA_NET.ColumnName = "HAA_NET";

            DataColumn HAA_ACTIVITY_START = new DataColumn();
            HAA_ACTIVITY_START.ColumnName = "HAA_ACTIVITY_START";


            DataColumn HAA_ACTIVITY_TYPE = new DataColumn();
            HAA_ACTIVITY_TYPE.ColumnName = "HAA_ACTIVITY_TYPE";




            DataColumn HAA_LIST = new DataColumn();
            HAA_LIST.ColumnName = "HAA_LIST";

            DataColumn DURATION = new DataColumn();
            DURATION.ColumnName = "DURATION";

            DataColumn DURATION_TYPE = new DataColumn();
            DURATION_TYPE.ColumnName = "DURATION_TYPE";

            DataColumn HAA_APPROVED_QTY = new DataColumn();
            HAA_APPROVED_QTY.ColumnName = "HAA_APPROVED_QTY";

            DataColumn DENIAL_CODE = new DataColumn();
            DENIAL_CODE.ColumnName = "DENIAL_CODE";

            DataColumn SERV_ID = new DataColumn();
            SERV_ID.ColumnName = "SERV_ID";



            dt.Columns.Add(EMR_ID);
            dt.Columns.Add(SERV_CODE);
            dt.Columns.Add(SERV_NAME);
            dt.Columns.Add(QTY);
            dt.Columns.Add(HAA_NET);
            dt.Columns.Add(HAA_ACTIVITY_START);
            dt.Columns.Add(HAA_ACTIVITY_TYPE);
            dt.Columns.Add(HAA_LIST);
            dt.Columns.Add(DURATION);
            dt.Columns.Add(DURATION_TYPE);
            dt.Columns.Add(HAA_APPROVED_QTY);
            dt.Columns.Add(SERV_ID);
            dt.Columns.Add(DENIAL_CODE);

            ViewState["eAuthActivities"] = dt;
        }





        void BindeAuthorizationGrid()
        {
            string Criteria = " 1=1 AND  HAR_TYPE	='A' ";
            // Criteria += " AND  TREATMENT_TYPE='" + drpTreatmentType.SelectedValue + "'";
            if (txteAuthHAR_ID.Text.Trim() != "")
            {
                Criteria += " AND HAR_ID  like '%" + txteAuthHAR_ID.Text.Trim() + "%' ";

            }

            if (txteAuthPTD.Text.Trim() != "")
            {
                Criteria += " AND HAR_PT_ID  = '" + txteAuthPTD.Text.Trim() + "' ";

            }

            if (drpeAuthStatus.SelectedIndex != 0)
            {
                Criteria += " AND HAR_STATUS  = " + drpeAuthStatus.Text.Trim();

            }


            string strStartDate = txteAuthFrmDt.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            if (txteAuthFrmDt.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),HAR_CREATED_DATE,101),101) >= '" + strForStartDate + "'";
            }


            string strTotDate = txteAuthToDt.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }

            if (txteAuthToDt.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),HAR_CREATED_DATE,101),101) <= '" + strForToDate + "'";
            }

            DataSet DS = new DataSet();
            eAuthorizationBAL objeAuth = new eAuthorizationBAL();
            DS = objeAuth.AuthorizationRequestGet(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                gvAuth.DataSource = DS;
                gvAuth.DataBind();
            }
            else
            {
                lblStatus.Text = " No Data";
                lblStatus.ForeColor = System.Drawing.Color.Red;

                gvAuth.DataBind();
            }


        }

        void BindeAuthDiagnosis()
        {
            string Criteria = " 1=1 ";

            Criteria += " AND  HAD_HAR_ID='" + hidHAR_ID.Value + "'";

            DataSet DS = new DataSet();
            eAuthorizationBAL objeAuth = new eAuthorizationBAL();
            DS = objeAuth.AuthorizationDiagnosisGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                DataTable DT = new DataTable();
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    DT = (DataTable)ViewState["eAuthDiagnosis"];

                    DataRow objrow;
                    objrow = DT.NewRow();
                    objrow["EPD_ID"] = Convert.ToString(DS.Tables[0].Rows[i]["HAD_EMR_ID"]);
                    objrow["EPD_DIAG_CODE"] = Convert.ToString(DS.Tables[0].Rows[i]["HAD_DIAG_CODE"]);
                    objrow["EPD_DIAG_NAME"] = Convert.ToString(DS.Tables[0].Rows[i]["HAD_DIAG_NAME"]);
                    DT.Rows.Add(objrow);
                }

                ViewState["eAuthDiagnosis"] = DT;
            }


        }

        void BindTempeAuthDiagnosis()
        {
            DataTable DT = new DataTable();

            DT = (DataTable)ViewState["eAuthDiagnosis"];
            if (DT.Rows.Count > 0)
            {
                gveAuthDiagnosis.DataSource = DT;
                gveAuthDiagnosis.DataBind();

            }
            else
            {
                gveAuthDiagnosis.DataBind();
            }

        }

        void BindeAuthActivities()
        {
            string Criteria = " 1=1 ";
            // Criteria += " AND  TREATMENT_TYPE='" + drpTreatmentType.SelectedValue + "'";
            Criteria += " AND HAA_HAR_ID='" + hidHAR_ID.Value + "'";
            DataSet DS = new DataSet();

            eAuthorizationBAL objeAuth = new eAuthorizationBAL();
            DS = objeAuth.AuthorizationActivitiesGet(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                string strObsValue = "", strObsCC = "", strObsMangPlan = "";
                strObsValue = Convert.ToString(DS.Tables[0].Rows[0]["HAA_OBS_VALUE"]);

                string[] arrObsValue;
                arrObsValue = strObsValue.Split(':');

                if (arrObsValue.Length > 1)
                {
                    strObsCC = arrObsValue[1];
                    strObsCC = strObsCC.Replace(", Management Plans", "");
                    strObsMangPlan = arrObsValue[2];


                }



                txteAuthCC.Text = strObsCC;
                txteAuthManagementPlan.Text = strObsMangPlan;

                DataTable DT = new DataTable();
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    DT = (DataTable)ViewState["eAuthActivities"];
                    DataRow objrow;
                    objrow = DT.NewRow();
                    objrow["EMR_ID"] = Convert.ToString(DS.Tables[0].Rows[i]["HAA_EMR_ID"]);
                    objrow["SERV_CODE"] = Convert.ToString(DS.Tables[0].Rows[i]["HAA_SERV_CODE"]);
                    objrow["SERV_NAME"] = Convert.ToString(DS.Tables[0].Rows[i]["HAA_SERV_NAME"]);
                    objrow["QTY"] = Convert.ToString(DS.Tables[0].Rows[i]["QTY"]);
                    objrow["HAA_NET"] = Convert.ToString(DS.Tables[0].Rows[i]["HAA_NET"]);
                    DateTime dtrActivityDate;
                    if (DS.Tables[0].Rows[i].IsNull("HAA_ACTIVITY_START") == false)
                    {
                        dtrActivityDate = Convert.ToDateTime(DS.Tables[0].Rows[i]["HAA_ACTIVITY_START"]);
                        objrow["HAA_ACTIVITY_START"] = dtrActivityDate.ToString("dd/MM/yyyy HH:mm");
                    }
                    else
                    {
                        objrow["HAA_ACTIVITY_START"] = "";
                    }
                    objrow["HAA_ACTIVITY_TYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["HAA_ACTIVITY_TYPE"]);
                    objrow["DURATION"] = Convert.ToString(DS.Tables[0].Rows[i]["HAA_OBS_VALUE"]);
                    objrow["DURATION_TYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["HAA_OBS_VALUE_TYPE"]);
                    objrow["HAA_APPROVED_QTY"] = Convert.ToString(DS.Tables[0].Rows[i]["HAA_APPROVED_QTY"]);
                    objrow["DENIAL_CODE"] = Convert.ToString(DS.Tables[0].Rows[i]["HAA_DENIAL_CODE"]);

                    DT.Rows.Add(objrow);

                }
                ViewState["eAuthActivities"] = DT;

            }

        }

        void BindeAuthTempeActivities()
        {
            DataTable DT = new DataTable();

            DT = (DataTable)ViewState["eAuthActivities"];
            if (DT.Rows.Count > 0)
            {
                gveAuthActivity.DataSource = DT;
                gveAuthActivity.DataBind();
            }
            else
            {
                gveAuthActivity.DataBind();
            }

        }





        void BuildeAuthAttachments()
        {
            DataTable dt = new DataTable();

            DataColumn HAAT_ID = new DataColumn();
            HAAT_ID.ColumnName = "HAAT_ID";

            DataColumn HAAT_FILE_NAME = new DataColumn();
            HAAT_FILE_NAME.ColumnName = "HAAT_FILE_NAME";

            DataColumn HAAT_FILE_NAME_ACTUAL = new DataColumn();
            HAAT_FILE_NAME_ACTUAL.ColumnName = "HAAT_FILE_NAME_ACTUAL";

            DataColumn IsNewFile = new DataColumn();
            IsNewFile.ColumnName = "IsNewFile";


            dt.Columns.Add(HAAT_ID);
            dt.Columns.Add(HAAT_FILE_NAME);
            dt.Columns.Add(HAAT_FILE_NAME_ACTUAL);
            dt.Columns.Add(IsNewFile);

            ViewState["eAuthAttachments"] = dt;
        }

        void BindeAuthAttachments()
        {
            // DataTable DT = new DataTable();

            //  DT = (DataTable)ViewState["eAuthActivities"];
            string Criteria = " 1=1 ";
            // Criteria += " AND  TREATMENT_TYPE='" + drpTreatmentType.SelectedValue + "'";
            Criteria += " AND HAAT_HAR_ID='" + hidHAR_ID.Value + "'";
            DataSet DS = new DataSet();

            eAuthorizationBAL objeAuth = new eAuthorizationBAL();
            DS = objeAuth.AuthorizationAttachmentsGet(Criteria);

            DataTable DT = new DataTable();
            if (DS.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    DT = (DataTable)ViewState["eAuthAttachments"];
                    DataRow objrow;
                    objrow = DT.NewRow();
                    objrow["HAAT_ID"] = Convert.ToString(DS.Tables[0].Rows[i]["HAAT_ID"]);
                    objrow["HAAT_FILE_NAME"] = Convert.ToString(DS.Tables[0].Rows[i]["HAAT_FILE_NAME"]);
                    objrow["HAAT_FILE_NAME_ACTUAL"] = Convert.ToString(DS.Tables[0].Rows[i]["HAAT_FILE_NAME_ACTUAL"]);
                    objrow["IsNewFile"] = "0";

                    DT.Rows.Add(objrow);

                }
                ViewState["eAuthAttachments"] = DT;
            }

        }

        void BindeAuthTempAttachments()
        {
            DataTable DT = new DataTable();

            DT = (DataTable)ViewState["eAuthAttachments"];
            if (DT.Rows.Count > 0)
            {
                gveAuthAttachments.DataSource = DT;
                gveAuthAttachments.DataBind();
            }
            else
            {
                gveAuthAttachments.DataBind();
            }

        }


        void CleareAuth()
        {
            if (drpeAuthTreatmentType.Items.Count > 0)
                drpeAuthTreatmentType.SelectedIndex = 0;

            txteAuthOrderDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
            txteAuthActivityDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
            if (drpeAuthActivityHour.Items.Count > 0)
                drpeAuthActivityHour.SelectedIndex = 0;
            if (drpeAuthActivityMin.Items.Count > 0)
                drpeAuthActivityMin.SelectedIndex = 0;
            TabContainer3.ActiveTab = TabeAuthPanelDiagnosis;
            hidHAR_ID.Value = "";
            hideAuthEmrID.Value = "";
            hideAuthClinicianID.Value = "";

            txtResubMemberID.Text = "";
            txtResubDoctor.Text = "";
            BuildeAuthDiagnosis();
            BuildeAuthActivities();
            BuildeAuthAttachments();

            txteAuthCC.Text = "";
            txteAuthManagementPlan.Text = "";
            txtResubComment.Text = "";
            drpResubType.SelectedIndex = 0;

            gveAuthDiagnosis.DataBind();
            gveAuthActivity.DataBind();
            gveAuthAttachments.DataBind();

            if (Convert.ToString(ViewState["eAuthSelectIndex"]) != "")
            {
                if (gvAuth.Rows.Count > 0)
                    gvAuth.Rows[Convert.ToInt32(ViewState["eAuthSelectIndex"])].BackColor = System.Drawing.Color.White;
            }
            ViewState["eAuthSelectIndex"] = "";
            ViewState["eAuthAttachDeleted"] = "";


        }

        void CleareAuthActivity()
        {
            hideAuthServID.Value = "";
            txteAuthServCode.Text = "";
            txteAuthServName.Text = "";

            txteAuthQty.Text = "1";
            txteAuthType.Text = "";
            txteAuthNet.Text = "";

            lbleAuthType.Visible = false;
            txteAuthType.Visible = false;

            lbleAuthPrice.Visible = false;
            txteAuthNet.Visible = false;


            ViewState["eAuthActivitySelectIndex"] = "";
        }


        [System.Web.Services.WebMethod]
        public static string[] GetStaffName(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 ";
            // Criteria += " AND HSFM_FNAME Like '%" + prefixText + "%'";
            Criteria += " AND HSFM_STAFF_ID +' '  + HSFM_FNAME + ' ' +isnull(HSFM_MNAME,'') + ' '  + isnull(HSFM_LNAME,'')   like '%" + prefixText + "%' ";

            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_doctor_detailss(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["HSFM_STAFF_ID"]).Trim() + " ~ " + Convert.ToString(ds.Tables[0].Rows[i]["HSFM_MOH_NO"]).Trim() + " ~ " + Convert.ToString(ds.Tables[0].Rows[i]["FullName"]).Trim();
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }


        void BindTime()
        {
            int AppointmentInterval = 15;
            int AppointmentStart = 0;
            int AppointmentEnd = 23;
            int index = 0;

            for (int i = AppointmentStart; i <= AppointmentEnd; i++)
            {
                string strHour = Convert.ToString(index);
                if (index <= 9)
                {
                    strHour = "0" + Convert.ToString(index);
                }

                drpActivityHour.Items.Insert(index, Convert.ToString(strHour));
                drpActivityHour.Items[index].Value = Convert.ToString(strHour);

                drpeAuthActivityHour.Items.Insert(index, Convert.ToString(strHour));
                drpeAuthActivityHour.Items[index].Value = Convert.ToString(strHour);



                drpSTHour.Items.Insert(index, Convert.ToString(strHour));
                drpSTHour.Items[index].Value = Convert.ToString(strHour);

                drpFIHour.Items.Insert(index, Convert.ToString(strHour));
                drpFIHour.Items[index].Value = Convert.ToString(strHour);

                index = index + 1;

            }
            index = 1;
            int intCount = 0;
            Int32 intMin = AppointmentInterval;


            drpActivityMin.Items.Insert(0, Convert.ToString("00"));
            drpActivityMin.Items[0].Value = Convert.ToString("00");

            drpeAuthActivityMin.Items.Insert(0, Convert.ToString("00"));
            drpeAuthActivityMin.Items[0].Value = Convert.ToString("00");

            drpSTMin.Items.Insert(0, Convert.ToString("00"));
            drpSTMin.Items[0].Value = Convert.ToString("00");

            drpFIMin.Items.Insert(0, Convert.ToString("00"));
            drpFIMin.Items[0].Value = Convert.ToString("00");

            for (int j = AppointmentInterval; j < 60; j++)
            {
                drpActivityMin.Items.Insert(index, Convert.ToString(j - intCount));
                drpActivityMin.Items[index].Value = Convert.ToString(j - intCount);

                drpeAuthActivityMin.Items.Insert(index, Convert.ToString(j - intCount));
                drpeAuthActivityMin.Items[index].Value = Convert.ToString(j - intCount);

                drpSTMin.Items.Insert(index, Convert.ToString(j - intCount));
                drpSTMin.Items[index].Value = Convert.ToString(j - intCount);

                drpFIMin.Items.Insert(index, Convert.ToString(j - intCount));
                drpFIMin.Items[index].Value = Convert.ToString(j - intCount);


                j = j + AppointmentInterval;
                index = index + 1;
                intCount = intCount + 1;

            }

        }

        void GetCompanyDtls(string InsCode, out string strPayerID, out string strReceiverID)
        {
            strPayerID = "";
            strReceiverID = "";

            DataSet DS = new DataSet();
            CompanyMasterBAL objeComp = new CompanyMasterBAL();

            string Criteria = " 1=1 ";
            Criteria += " and HCM_COMP_ID='" + InsCode + "'";

            DS = objeComp.GetCompanyMaster(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("HCM_PAYERID") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCM_PAYERID"]) != "")
                {
                    strPayerID = Convert.ToString(DS.Tables[0].Rows[0]["HCM_PAYERID"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("HCM_FIELD5") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCM_FIELD5"]) != "")
                {
                    strReceiverID = Convert.ToString(DS.Tables[0].Rows[0]["HCM_FIELD5"]);
                }

            }

        }

        string GetReciverID(string InsCode)
        {
            string strReceiverID = "";

            DataSet DS = new DataSet();
            CompanyMasterBAL objeComp = new CompanyMasterBAL();

            string Criteria = " 1=1 ";
            Criteria += " and HCM_COMP_ID='" + InsCode + "'";

            DS = objeComp.GetCompanyMaster(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("HCM_FIELD5") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCM_FIELD5"]) != "")
                {
                    strReceiverID = Convert.ToString(DS.Tables[0].Rows[0]["HCM_FIELD5"]);
                }

            }
            return strReceiverID;
        }

        void XMLFileWrite()
        {

            eAuthorizationBAL objeAuth;
            DataSet DS;
            DS = new DataSet();

            objeAuth = new eAuthorizationBAL();
            string Criteria = " 1=1 ";
            string strStatus = GlobalValues.Authorization_SubmitStatus + "," + GlobalValues.Authorization_ReSubmitStatus + "," + GlobalValues.Authorization_CancelSubmitStatus;
            // Criteria += " AND HAR_STATUS in (" + strStatus + ")";
            Criteria += " AND  HAR_ID= " + hidHAR_ID.Value;

            DS = objeAuth.AuthorizationRequestGet(Criteria);
            Console.WriteLine(" Ready for eAuthorization total count is " + DS.Tables[0].Rows.Count);
            if (DS.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    StringBuilder strData = new StringBuilder();


                    strData.Append("<?xml version='1.0' encoding='utf-8' ?>");
                    strData.Append(@"<Prior.Request xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:noNamespaceSchemaLocation='https://www.haad.ae/DataDictionary/CommonTypes/PriorRequest.xsd'> ");


                    string strAuthorizationID, strInsCode = "", strReceiverID = "", strPayerID = "", strDate = "", strTime = "", strRequestType = "", strFileType = ""
                        , strIDPayer = "", strMemberId = "", strEncounterType = "", strResubType = "", strResubComment = "", strClinicianID = ""
                        , strEncounterDtDate = "", strEncounterDtTime = "", strEncounterEndDate = "", strEncounterEndTime = "";

                    Int32 intStatus = 0;


                    strDate = DateTime.Now.ToString("dd/MM/yyyy HH:mm");
                    string[] arrDate;
                    arrDate = strDate.Split(' ');
                    string strDate1 = arrDate[0];
                    strTime = arrDate[1];

                    string[] arrDate1 = strDate1.Split('/');
                    string strTimeStamp = "";
                    strTimeStamp = arrDate1[2] + arrDate1[1] + arrDate1[0] + strTime.Replace(":", "");

                    string strHAR_ID = "", strHAR_EMR_ID = "";

                    strHAR_ID = Convert.ToString(DS.Tables[0].Rows[i]["HAR_ID"]);
                    strHAR_EMR_ID = Convert.ToString(DS.Tables[0].Rows[i]["HAR_EMR_ID"]);
                    strInsCode = Convert.ToString(DS.Tables[0].Rows[i]["HAR_INS_CODE"]);//THIS USER FOR TAKE THE RECIVER ID
                    strReceiverID = GetReciverID(strInsCode);
                    strPayerID = Convert.ToString(DS.Tables[0].Rows[i]["HAR_PAYER_ID"]);

                    intStatus = Convert.ToInt32(DS.Tables[0].Rows[i]["HAR_STATUS"]);
                    strIDPayer = Convert.ToString(DS.Tables[0].Rows[i]["HAR_ID_PAYER"]);

                    strMemberId = Convert.ToString(DS.Tables[0].Rows[i]["HAR_MEMBER_ID"]);
                    strEncounterType = Convert.ToString(DS.Tables[0].Rows[i]["HAR_ENCOUNTER_TYPE"]);

                    strClinicianID = Convert.ToString(DS.Tables[0].Rows[i]["HAR_CLINICIAN_ID"]);

                    strEncounterDtDate = Convert.ToString(DS.Tables[0].Rows[i]["HAR_ENCOUNTER_STARTDesc"]);
                    strEncounterDtTime = Convert.ToString(DS.Tables[0].Rows[i]["HAR_ENCOUNTER_STARTTime"]);
                    strEncounterEndDate = Convert.ToString(DS.Tables[0].Rows[i]["HAR_ENCOUNTER_ENDDesc"]);
                    strEncounterEndTime = Convert.ToString(DS.Tables[0].Rows[i]["HAR_ENCOUNTER_ENDTime"]);

                    strAuthorizationID = GlobalValues.FacilityID + " " + strPayerID + " " + strTimeStamp + strHAR_ID;


                    if (intStatus == GlobalValues.Authorization_SubmitStatus)
                    {
                        strRequestType = "Authorization";
                        strFileType = "Submission";
                    }

                    if (intStatus == GlobalValues.Authorization_ReSubmitStatus)
                    {
                        strRequestType = "Authorization";
                        strFileType = "Resubmission";
                    }
                    else if (intStatus == GlobalValues.Authorization_CancelSubmitStatus)
                    {
                        strRequestType = "Cancellation";
                        strFileType = "Cancellation";
                    }




                    strData.Append("<Header>");
                    strData.Append("<SenderID>" + GlobalValues.FacilityID + "</SenderID>");
                    strData.Append("<ReceiverID>" + strReceiverID + "</ReceiverID>");
                    strData.Append("<TransactionDate>" + strDate + "</TransactionDate>");
                    strData.Append("<RecordCount>1</RecordCount>");
                    strData.Append("<DispositionFlag>" + GlobalValues.Shafafiya_DispositionFlag + "</DispositionFlag>");
                    strData.Append("</Header>");
                    strData.Append("<Authorization>");
                    strData.Append("<Type>" + strRequestType + "</Type>");
                    strData.Append("<ID>" + strAuthorizationID + "</ID>");


                    if (strFileType == "Resubmission" || strFileType == "Cancellation")
                    {
                        strData.Append("<IDPayer>" + strIDPayer + "</IDPayer>");
                    }


                    strData.Append("<MemberID>" + strMemberId + "</MemberID>");
                    strData.Append("<PayerID>" + strPayerID + "</PayerID>");
                    strData.Append("<EmiratesIDNumber>999-9999-9999999-9</EmiratesIDNumber>");
                    strData.Append("<DateOrdered>" + strDate1 + "</DateOrdered>");

                    if (strFileType == "Submission" || strFileType == "Resubmission")
                    {

                        if (strEncounterType == "3" || strEncounterType == "4")
                        {
                            strData.Append("<Encounter>");
                            strData.Append("<FacilityID>" + GlobalValues.FacilityID + "</FacilityID>");
                            strData.Append("<Type>" + strEncounterType + "</Type>");
                            strData.Append("<Start>" + strEncounterDtDate + " " + strEncounterDtTime + "</Start>");
                            strData.Append("<End>" + strEncounterEndDate + " " + strEncounterEndTime + "</End>");
                            strData.Append("</Encounter>");
                        }
                        else
                        {
                            strData.Append("<Encounter>");
                            strData.Append("<FacilityID>" + GlobalValues.FacilityID + "</FacilityID>");
                            strData.Append("<Type>" + strEncounterType + "</Type>");
                            strData.Append("</Encounter>");

                        }

                    }

                    if (strFileType == "Resubmission")
                    {
                        strData.Append("<Resubmission>");
                        strData.Append("<Type>" + strResubType + "</Type>");
                        strData.Append("<Comment>" + strResubComment + "</Comment>");
                        strData.Append("</Resubmission>");
                    }


                    DataSet DSDiag = new DataSet();
                    objeAuth = new eAuthorizationBAL();
                    string strDiagCriteria = " 1=1 ";
                    strDiagCriteria += " AND HAD_HAR_ID =" + strHAR_ID;
                    DSDiag = objeAuth.AuthorizationDiagnosisGet(strDiagCriteria);

                    if (DSDiag.Tables[0].Rows.Count > 0)
                    {
                        for (int d = 0; d < DSDiag.Tables[0].Rows.Count; d++)
                        {
                            string strDiagCode = "";

                            strDiagCode = Convert.ToString(DSDiag.Tables[0].Rows[d]["HAD_DIAG_CODE"]);
                            strData.Append("<Diagnosis>");
                            if (d == 0)
                            {
                                strData.Append("<Type>Principal</Type>");
                            }
                            else
                            {
                                strData.Append("<Type>Secondary</Type>");
                            }
                            strData.Append("<Code>" + strDiagCode + "</Code>");
                            strData.Append("</Diagnosis>");

                        }

                    }

                    DataSet DSActivity = new DataSet();
                    objeAuth = new eAuthorizationBAL();
                    string strActivCriteria = " 1=1 ";
                    strActivCriteria += " AND HAA_HAR_ID =" + strHAR_ID;
                    DSActivity = objeAuth.AuthorizationActivitiesGet(strActivCriteria);

                    if (DSActivity.Tables[0].Rows.Count > 0)
                    {
                        for (int a = 0; a < DSActivity.Tables[0].Rows.Count; a++)
                        {
                            string HAA_ACTIVITY_TYPE = "", HAA_SERV_CODE = "", HAA_QTY = "1.0", HAA_NET = "0.0", HAA_OBS_TYPE = "", HAA_OBS_CODE = ""
                                , HAA_OBS_VALUE = "", HAA_OBS_VALUE_TYPE = "";

                            HAA_ACTIVITY_TYPE = Convert.ToString(DSActivity.Tables[0].Rows[a]["HAA_ACTIVITY_TYPE"]);
                            HAA_SERV_CODE = Convert.ToString(DSActivity.Tables[0].Rows[a]["HAA_SERV_CODE"]);
                            HAA_QTY = Convert.ToString(DSActivity.Tables[0].Rows[a]["HAA_QTY"]);
                            HAA_NET = Convert.ToString(DSActivity.Tables[0].Rows[a]["HAA_NET"]);

                            HAA_OBS_TYPE = Convert.ToString(DSActivity.Tables[0].Rows[a]["HAA_OBS_TYPE"]);
                            HAA_OBS_CODE = Convert.ToString(DSActivity.Tables[0].Rows[a]["HAA_OBS_CODE"]);
                            if (DSActivity.Tables[0].Rows[a].IsNull("HAA_OBS_VALUE") == false)
                                HAA_OBS_VALUE = Convert.ToString(DSActivity.Tables[0].Rows[a]["HAA_OBS_VALUE"]);
                            HAA_OBS_VALUE_TYPE = Convert.ToString(DSActivity.Tables[0].Rows[a]["HAA_OBS_VALUE_TYPE"]);


                            if (a == 0)
                            {
                                DataSet DSAttach = new DataSet();
                                objeAuth = new eAuthorizationBAL();
                                string strAttachCriteria = " 1=1 ";
                                strAttachCriteria += " AND HAAT_HAR_ID =" + strHAR_ID;
                                DSAttach = objeAuth.AuthorizationAttachmentsGet(strAttachCriteria);


                                strData.Append("<Activity>");
                                strData.Append("<ID>" + Convert.ToString(a + 1) + "</ID>");
                                strData.Append("<Start>" + strDate + "</Start>");
                                strData.Append("<Type>" + HAA_ACTIVITY_TYPE + "</Type>");
                                strData.Append("<Code>" + HAA_SERV_CODE + "</Code>");
                                strData.Append("<Quantity>" + HAA_QTY + "</Quantity>");
                                strData.Append("<Net>" + HAA_NET + "</Net>");
                                strData.Append("<Clinician>" + strClinicianID + "</Clinician>");

                                if (HAA_ACTIVITY_TYPE != "6")
                                {
                                    strData.Append("<Observation>");
                                    strData.Append("<Type>" + HAA_OBS_TYPE + "</Type>");
                                    strData.Append("<Code>" + HAA_OBS_CODE + "</Code>");
                                    strData.Append("<Value>" + HAA_OBS_VALUE.Replace("int& ext", "") + "</Value>");
                                    strData.Append("<ValueType>" + HAA_OBS_VALUE_TYPE + "</ValueType>");
                                    strData.Append("</Observation>");
                                }
                                else
                                {
                                    if (HAA_OBS_VALUE != "")
                                    {
                                        strData.Append("<Observation>");
                                        strData.Append("<Type>" + HAA_OBS_TYPE + "</Type>");
                                        strData.Append("<Code>" + HAA_OBS_VALUE.Replace("int& ext", "") + "</Code>");
                                        strData.Append("</Observation>");
                                    }

                                }

                                if (DSAttach.Tables[0].Rows.Count > 0)
                                {
                                    for (int at = 0; at < DSAttach.Tables[0].Rows.Count; at++)
                                    {
                                        strData.Append("<Observation>");
                                        strData.Append("<Type>File</Type>");
                                        strData.Append("<Code>PDF</Code>");
                                        strData.Append("<Value>" + Convert.ToString(DSAttach.Tables[0].Rows[at]["HAAT_ATTACHMENT"]) + "</Value>");
                                        strData.Append("<ValueType>PDF File</ValueType>");
                                        strData.Append("</Observation>");
                                    }
                                }


                                strData.Append("</Activity>");


                            }
                            else
                            {
                                strData.Append("<Activity>");
                                strData.Append("<ID>" + Convert.ToString(a + 1) + "</ID>");
                                strData.Append("<Start>" + strDate + "</Start>");
                                strData.Append("<Type>" + HAA_ACTIVITY_TYPE + "</Type>");
                                strData.Append("<Code>" + HAA_SERV_CODE + "</Code>");
                                strData.Append("<Quantity>" + HAA_QTY + "</Quantity>");
                                strData.Append("<Net>" + HAA_NET + "</Net>");
                                strData.Append("<Clinician>" + strClinicianID + "</Clinician>");

                                if (HAA_ACTIVITY_TYPE == "6" && HAA_OBS_VALUE != "")
                                {
                                    strData.Append("<Observation>");
                                    strData.Append("<Type>" + HAA_OBS_TYPE + "</Type>");
                                    strData.Append("<Code>" + HAA_OBS_VALUE + "</Code>");
                                    strData.Append("</Observation>");

                                }

                                strData.Append("</Activity>");



                            }
                        }

                    }


                    strData.Append("</Authorization></Prior.Request>");

                }

            }









        }


        void SetPermission()
        {
            string strPermission = "0";
            if (Convert.ToString(Session["ROLL_IDS"]) == "" || Convert.ToString(Session["ROLL_IDS"]) == null)
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../../ErrorPage.aspx");
            }
            string Criteria = " 1=1 AND HRT_SCREEN_ID='EAUTH' ";
            Criteria += " AND  HRT_ROLL_ID='" + Convert.ToString(Session["Roll_Id"]) + "'";

            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.RollTransGet(Criteria);


            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "0" || strPermission == "1")
            {
                btnGenerate.Enabled = false;
                btnResubmisionGenerate.Enabled = false;
                btnCancelAuth.Enabled = false;

                btnDiagAdd.Enabled = false;
                btnServiceAdd.Enabled = false;
                btnAttaAdd.Enabled = false;

                btneAuthDiagAdd.Enabled = false;
                btneAuthActivityAdd.Enabled = false;
                btneAuthAttaAdd.Enabled = false;

                btnClear.Enabled = false;

            }


            if (strPermission == "7")
            {
                btnResubmisionGenerate.Enabled = false;
                btnCancelAuth.Enabled = false;

                btnDiagAdd.Enabled = false;
                btnServiceAdd.Enabled = false;
                btnAttaAdd.Enabled = false;

                btneAuthDiagAdd.Enabled = false;
                btneAuthActivityAdd.Enabled = false;
                btneAuthAttaAdd.Enabled = false;

                btnClear.Enabled = false;

            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../../ErrorPage.aspx");
            }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            lblStatus.Text = "";

            if (!IsPostBack)
            {
                if (Convert.ToString(Session["HMS_AUTHENTICATION"]) == "1")
                {
                    if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_ID"]).ToUpper() != "SUPER_ADMIN")
                    {
                        SetPermission();
                    }
                }
                try
                {

                    ViewState["SelectIndex"] = "";
                    ViewState["eAuthSelectIndex"] = "";
                    ViewState["eAuthAttachDeleted"] = "";
                    hidBranchID.Value = Convert.ToString(Session["Branch_ID"]);
                    hidUserID.Value = Convert.ToString(Session["User_ID"]);

                    txtFromDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
                    txtToDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");


                    txteAuthFrmDt.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
                    txteAuthToDt.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
                    txtOrderDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
                    txteAuthOrderDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");

                    txtActivityDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
                    txteAuthActivityDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");

                    BindCompany();
                    BindDoctor();
                    BindTime();
                }
                catch (Exception ex)
                {

                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      eAuthApproval.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }
            }

        }

        protected void drpTreatmentType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (drpTreatmentType.SelectedValue == "3" || drpTreatmentType.SelectedValue == "4")
            {
                tblEncounter.Visible = true;
            }
            else
            {
                tblEncounter.Visible = false;
            }
        }

        protected void chkDental_CheckedChanged(object sender, EventArgs e)
        {

            if (chkDental.Checked == true)
            {
                lblToothNo.Visible = true;
                txtToothNo.Visible = true;

            }
            else
            {
                lblToothNo.Visible = false;
                txtToothNo.Visible = false;

            }
        }


        protected void Select_Click(object sender, EventArgs e)
        {
            try
            {

                if (Convert.ToString(ViewState["SelectIndex"]) != "")
                {
                    gvPTMaster.Rows[Convert.ToInt32(ViewState["SelectIndex"])].BackColor = System.Drawing.Color.White;
                }
                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

                ViewState["SelectIndex"] = gvScanCard.RowIndex;
                gvPTMaster.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");

                Label lblEpmID, lblPTName1, lblPTID, lblMemberID, lblDrCode, lblDrName, lblClinicianID, lblInsCode, lblPayerID;


                lblEpmID = (Label)gvScanCard.Cells[0].FindControl("lblEpmID");
                lblPTName1 = (Label)gvScanCard.Cells[0].FindControl("lblPTName");
                lblPTID = (Label)gvScanCard.Cells[0].FindControl("lblPTID");
                lblMemberID = (Label)gvScanCard.Cells[0].FindControl("lblMemberID");
                lblDrCode = (Label)gvScanCard.Cells[0].FindControl("lblDrCode");
                lblDrName = (Label)gvScanCard.Cells[0].FindControl("lblDrName");

                lblClinicianID = (Label)gvScanCard.Cells[0].FindControl("lblClinicianID");
                lblInsCode = (Label)gvScanCard.Cells[0].FindControl("lblInsCode");
                lblPayerID = (Label)gvScanCard.Cells[0].FindControl("lblPayerID");


                if (lblDrCode.Text != "" && lblDrName.Text != "")
                {
                    txtDoctor.Text = lblDrCode.Text + " ~ " + lblClinicianID.Text + " ~ " + lblDrName.Text;

                }

                if (lblInsCode.Text != "")
                {

                    for (int intCount = 0; intCount < drpCompany.Items.Count; intCount++)
                    {
                        if (drpCompany.Items[intCount].Value == lblInsCode.Text)
                        {
                            drpCompany.SelectedValue = lblInsCode.Text;
                            goto ForLoopCompanyEnd;

                        }
                    }

                }
            ForLoopCompanyEnd: ;

                drpCompany_SelectedIndexChanged(drpCompany, new EventArgs());

                txtMemberID.Text = lblMemberID.Text;
                hidEmrID.Value = lblEpmID.Text;
                hidPTID.Value = lblPTID.Text;

                lblPTName.Text = lblPTName1.Text;
                lblFileNo.Text = lblPTID.Text;




                BuildDiagnosis();
                BuildActivities();
                BuildAttachments();

                BindDiagnosis();
                BindActivities();

                BindTempDiagnosis();
                BindTempAuthorizationService();

            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eAuthApproval.Select_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }


        protected void gvPTMaster_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvPTMaster.PageIndex = e.NewPageIndex;
            BindEMRPTMaster();
        }

        protected void btnFind_Click(object sender, EventArgs e)
        {
            try
            {
                //Clear();

                //if (drpTreatmentType.SelectedValue == "1" || drpTreatmentType.SelectedValue == "3" || drpTreatmentType.SelectedValue == "4")
                //{
                BindEMRPTMaster();

                //}
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eAuthApproval.btnFind_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void drpCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                hidPayerID.Value = "";
                hidReciverID.Value = "";
                string strPayerID = "", strReceiverID = "";
                GetCompanyDtls(drpCompany.SelectedValue, out  strPayerID, out  strReceiverID);
                if (strPayerID == "")
                {
                    lblStatus.Text = " Selected Company Not haveing the PayerID";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;

                }

                if (strReceiverID == "")
                {
                    lblStatus.Text = " Selected Company Not haveing the ReciverID";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;

                }

                hidPayerID.Value = strPayerID;
                hidReciverID.Value = strReceiverID;
            FunEnd: ;

            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eAuthApproval.drpCompany_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void btnDiagAdd_Click(object sender, EventArgs e)
        {

            try
            {
                if (Convert.ToString(ViewState["SelectIndex"]) == "")
                {
                    lblStatus.Text = " Please select one data from above list";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }


                DataTable DT = new DataTable();

                DT = (DataTable)ViewState["Diagnosis"];

                DataRow objrow;
                objrow = DT.NewRow();
                objrow["EPD_ID"] = hidEmrID.Value;
                objrow["EPD_DIAG_CODE"] = txtDiagCode.Text.Trim(); ;
                objrow["EPD_DIAG_NAME"] = txtDiagName.Text.Trim();
                DT.Rows.Add(objrow);


                ViewState["Diagnosis"] = DT;
                BindTempDiagnosis();

                txtDiagCode.Text = "";
                txtDiagName.Text = "";

            FunEnd: ;
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eAuthApproval.btnDiagAdd_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void DeleteDiag_Click(object sender, EventArgs e)
        {
            try
            {
                string strPermission = hidPermission.Value;
                if (strPermission == "7" || strPermission == "9")
                {
                    ImageButton btnEdit = new ImageButton();
                    btnEdit = (ImageButton)sender;

                    GridViewRow gvScanCard;
                    gvScanCard = (GridViewRow)btnEdit.Parent.Parent;


                    if (ViewState["Diagnosis"] != null)
                    {
                        DataTable DT = new DataTable();
                        DT = (DataTable)ViewState["Diagnosis"];
                        DT.Rows.RemoveAt(gvScanCard.RowIndex);
                        DT.AcceptChanges();
                        ViewState["Diagnosis"] = DT;

                    }



                    BindTempDiagnosis();
                }
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eAuthApproval.DeleteDiag_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }

        protected void btnServiceAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToString(ViewState["SelectIndex"]) == "")
                {
                    lblStatus.Text = " Please select one data from above list";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                // Label lblInsCode;
                int intSelected = Convert.ToInt32(ViewState["SelectIndex"]);

                // lblInsCode = (Label)gvPTMaster.Rows[intSelected].Cells[0].FindControl("lblInsCode");

                string strHaadCode = "", strServType = "", strServID = "";
                strHaadCode = txtServCode.Text.Trim();
                strServID = hidServID.Value.Trim();

                Decimal decServPrice = 0;

                if (strHaadCode != "")
                {
                    GetHaadServDtls(strHaadCode, out  strServType);
                    //  GetCompanyAgrDtls(strServID, lblInsCode.Text, out  decServPrice);
                    GetCompanyAgrDtls(strServID, drpCompany.SelectedValue, out  decServPrice);

                }
                //GetCompanyAgrDtls(hidServID.Value.Trim(), lblInsCode.Text, out  decServPrice);


                /*
                if (decServPrice == 0)
                {
                    lblStatus.Text = " Selected service is not availbae in Agrement service or Price is not available";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;

                }
                */
                DataTable DT = new DataTable();

                DT = (DataTable)ViewState["Activities"];
                if (ViewState["ActivitySelectIndex"] == "" || ViewState["ActivitySelectIndex"] == null)
                {
                    DataRow objrow;
                    objrow = DT.NewRow();
                    objrow["EMR_ID"] = hidEmrID.Value;
                    objrow["SERV_CODE"] = txtServCode.Text.Trim();
                    objrow["SERV_NAME"] = txtServName.Text.Trim();
                    objrow["QTY"] = txtQty.Text.Trim();

                    decimal decQty = 1, decPrice = 0;
                    if (txtQty.Text.Trim() != "")
                    {
                        decQty = Convert.ToDecimal(txtQty.Text.Trim());
                    }
                    decPrice = decQty * decServPrice;


                    objrow["HAA_NET"] = decPrice;
                    objrow["HAA_ACTIVITY_START"] = txtActivityDate.Text.Trim() + " " + drpActivityHour.SelectedValue + ":" + drpActivityMin.SelectedValue;
                    objrow["HAA_ACTIVITY_TYPE"] = strServType;

                    objrow["SERV_ID"] = hidServID.Value.Trim();

                    if (chkDental.Checked == true)
                    {
                        objrow["DURATION"] = txtToothNo.Text.Trim();
                    }
                    else
                    {
                        objrow["DURATION"] = "";
                    }

                    DT.Rows.Add(objrow);
                }
                else
                {
                    Int32 R = Convert.ToInt32(ViewState["ActivitySelectIndex"]);
                    DT.Rows[R]["EMR_ID"] = hidEmrID.Value;
                    DT.Rows[R]["SERV_CODE"] = txtServCode.Text.Trim(); ;
                    DT.Rows[R]["SERV_NAME"] = txtServName.Text.Trim();
                    DT.Rows[R]["QTY"] = txtQty.Text.Trim();
                    // DT.Rows[R]["HAA_NET"] = txtNet.Text.Trim(); //decServPrice;


                    decimal decQty = 1, decPrice = 0;
                    if (txtQty.Text.Trim() != "")
                    {
                        decQty = Convert.ToDecimal(txtQty.Text.Trim());
                    }

                    if (txtNet.Text.Trim() != "")
                    {
                        decServPrice = Convert.ToDecimal(txtNet.Text.Trim());
                    }

                    decPrice = decQty * decServPrice;


                    DT.Rows[R]["HAA_NET"] = decPrice;

                    DT.Rows[R]["HAA_ACTIVITY_START"] = txtActivityDate.Text.Trim() + " " + drpActivityHour.SelectedValue + ":" + drpActivityMin.SelectedValue;

                    DT.Rows[R]["HAA_ACTIVITY_TYPE"] = txtType.Text.Trim();

                    DT.Rows[R]["SERV_ID"] = hidServID.Value.Trim();

                    if (chkDental.Checked == true)
                    {
                        DT.Rows[R]["DURATION"] = txtToothNo.Text.Trim();
                    }
                    else
                    {
                        DT.Rows[R]["DURATION"] = "";
                    }

                    DT.AcceptChanges();
                }

                ViewState["Activities"] = DT;
                BindTempAuthorizationService();

                ClearActivity();


            FunEnd: ;
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eAuthApproval.btnServiceAdd_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void SelectActivity_Click(object sender, EventArgs e)
        {
            try
            {
                ClearActivity();

                lblType.Visible = true;
                txtType.Visible = true;

                lblPrice.Visible = true;
                txtNet.Visible = true;
                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
                ViewState["ActivitySelectIndex"] = gvScanCard.RowIndex;

                Label lblServCode, lblServName, lblQty, lblServID, lblObsValue, lblActivityType, lblNet;


                lblServCode = (Label)gvScanCard.Cells[0].FindControl("lblServCode");
                lblServName = (Label)gvScanCard.Cells[0].FindControl("lblServName");
                lblQty = (Label)gvScanCard.Cells[0].FindControl("lblQty");
                lblServID = (Label)gvScanCard.Cells[0].FindControl("lblServID");
                lblObsValue = (Label)gvScanCard.Cells[0].FindControl("lblObsValue");
                lblActivityType = (Label)gvScanCard.Cells[0].FindControl("lblActivityType");
                lblNet = (Label)gvScanCard.Cells[0].FindControl("lblNet");

                txtServCode.Text = lblServCode.Text;
                txtServName.Text = lblServName.Text;
                txtQty.Text = lblQty.Text;
                hidServID.Value = lblServID.Text;
                txtToothNo.Text = lblObsValue.Text;
                txtType.Text = lblActivityType.Text;
                txtNet.Text = lblNet.Text;

            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eAuthApproval.SelectActivity_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }


        protected void DeleteService_Click(object sender, EventArgs e)
        {
            try
            {
                string strPermission = hidPermission.Value;
                if (strPermission == "7" || strPermission == "9")
                {
                    ImageButton btnEdit = new ImageButton();
                    btnEdit = (ImageButton)sender;

                    GridViewRow gvScanCard;
                    gvScanCard = (GridViewRow)btnEdit.Parent.Parent;


                    if (ViewState["Activities"] != null)
                    {
                        DataTable DT = new DataTable();
                        DT = (DataTable)ViewState["Activities"];
                        DT.Rows.RemoveAt(gvScanCard.RowIndex);
                        DT.AcceptChanges();
                        ViewState["Activities"] = DT;

                    }

                    BindTempAuthorizationService();
                }
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eAuthApproval.DeleteService_Click");
                TextFileWriting(ex.Message.ToString());
            }



        }


        protected void btnAttaAdd_Click(object sender, EventArgs e)
        {

            try
            {
                string strName = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds + ".PDF";
                string strPath = GlobalValues.AuthorizationAttachmentPath;


                if (Convert.ToString(ViewState["SelectIndex"]) == "")
                {
                    lblStatus.Text = " Please select one data from above list";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                Int32 intLength;
                intLength = filAttachment.PostedFile.ContentLength;

                if (intLength >= 4194304)
                {
                    lblStatus.Text = " File size not exceed 4 MB";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                filAttachment.SaveAs(strPath + strName);
                DataTable DT = new DataTable();

                DT = (DataTable)ViewState["Attachments"];

                DataRow objrow;
                objrow = DT.NewRow();
                objrow["HAAT_FILE_NAME"] = strName;
                objrow["HAAT_FILE_NAME_ACTUAL"] = filAttachment.FileName;

                DT.Rows.Add(objrow);

                ViewState["Attachments"] = DT;
                BindTempAttachments();


            FunEnd: ;
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eAuthApproval.btnDiagAdd_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void DeleteAtta_Click(object sender, EventArgs e)
        {
            try
            {
                string strPermission = hidPermission.Value;
                if (strPermission == "7" || strPermission == "9")
                {
                    ImageButton btnEdit = new ImageButton();
                    btnEdit = (ImageButton)sender;

                    GridViewRow gvScanCard;
                    gvScanCard = (GridViewRow)btnEdit.Parent.Parent;


                    if (ViewState["Attachments"] != null)
                    {
                        DataTable DT = new DataTable();
                        DT = (DataTable)ViewState["Attachments"];
                        DT.Rows.RemoveAt(gvScanCard.RowIndex);
                        DT.AcceptChanges();
                        ViewState["Attachments"] = DT;

                    }


                    BindTempAttachments();
                }
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eAuthApproval.DeleteAtta_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }





        protected void btnGenerate_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToString(ViewState["SelectIndex"]) == "")
                {
                    lblStatus.Text = " Please select one data from list";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }



                Int32 intLength;
                intLength = filAttachment.PostedFile.ContentLength;

                //below checking for file size more that 4MB
                if (intLength >= 4194304)
                {
                    lblStatus.Text = " File size not exceed 4 MB";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }


                eAuthorizationBAL objeAuth;

                Decimal decTotalAmount = 0;

                DataTable DT;

                Label lblPTName, lblDRCode, lblDrName, lblInsCode, lblInsName, lblMemberID, lblPayerID;
                int intSelected = Convert.ToInt32(ViewState["SelectIndex"]);

                lblPTName = (Label)gvPTMaster.Rows[intSelected].Cells[0].FindControl("lblPTName");
                lblDRCode = (Label)gvPTMaster.Rows[intSelected].Cells[0].FindControl("lblDRCode");
                lblDrName = (Label)gvPTMaster.Rows[intSelected].Cells[0].FindControl("lblDrName");
                lblInsCode = (Label)gvPTMaster.Rows[intSelected].Cells[0].FindControl("lblInsCode");
                lblInsName = (Label)gvPTMaster.Rows[intSelected].Cells[0].FindControl("lblInsName");
                lblMemberID = (Label)gvPTMaster.Rows[intSelected].Cells[0].FindControl("lblMemberID");
                lblPayerID = (Label)gvPTMaster.Rows[intSelected].Cells[0].FindControl("lblPayerID");

                objeAuth = new eAuthorizationBAL();
                objeAuth.HAR_BRANCH_ID = hidBranchID.Value; //Convert.ToString(Session["Branch_ID"]);
                objeAuth.HAR_EMR_ID = hidEmrID.Value;
                objeAuth.HAR_PT_ID = hidPTID.Value;
                objeAuth.HAR_PT_NAME = lblPTName.Text;

                //objeAuth.HAR_DR_CODE = lblDRCode.Text;
                //objeAuth.HAR_DR_NAME = lblDrName.Text;


                string strClinicianID = "";
                string strDoctor = txtDoctor.Text.Trim();

                string[] arrDoctor = strDoctor.Split('~');
                // string[] arrDoctor = strDoctor.Split('/');

                if (arrDoctor.Length > 1)
                {
                    objeAuth.HAR_DR_CODE = arrDoctor[0].Trim();
                    objeAuth.HAR_CLINICIAN_ID = arrDoctor[1].Trim();
                    strClinicianID = arrDoctor[1].Trim();
                    objeAuth.HAR_DR_NAME = arrDoctor[2].Trim();

                }
                else
                {
                    objeAuth.HAR_DR_CODE = "";
                    objeAuth.HAR_DR_NAME = "";
                    objeAuth.HAR_CLINICIAN_ID = "";
                }

                if (drpCompany.SelectedValue == "")
                {
                    lblStatus.Text = " Please select the Company";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                if (objeAuth.HAR_CLINICIAN_ID == "")
                {
                    lblStatus.Text = " Please enter the doctor details";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                if (hidPayerID.Value == "") // if (lblPayerID.Text == "")
                {
                    lblStatus.Text = "Company Not haveing the Payer ID";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }
                if (hidReciverID.Value == "") // if (lblPayerID.Text == "")
                {
                    lblStatus.Text = "Company Not haveing the Reciver ID";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }



                if (txtMemberID.Text.Trim() == "") // if (lblMemberID.Text == "")
                {
                    lblStatus.Text = " Please enter the Member ID";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }


                DT = new DataTable();
                DT = (DataTable)ViewState["Diagnosis"];
                if (DT.Rows.Count <= 0)
                {
                    lblStatus.Text = " Please add  the Diagnosis";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                DT = new DataTable();
                DT = (DataTable)ViewState["Activities"];
                if (DT.Rows.Count <= 0)
                {
                    lblStatus.Text = " Please add the Activities";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }


                objeAuth.HAR_INS_CODE = drpCompany.SelectedValue; //lblInsCode.Text;
                objeAuth.HAR_INS_NAME = drpCompany.SelectedItem.Text; //lblInsName.Text;
                objeAuth.HAR_MEMBER_ID = txtMemberID.Text; //lblMemberID.Text;

                objeAuth.HAR_PAYER_ID = hidPayerID.Value;// lblPayerID.Text;
                objeAuth.HAR_ENCOUNTER_TYPE = drpTreatmentType.SelectedValue;
                //  objeAuth.HAR_TOTAL_AMOUNT = Convert.ToString(decTotalAmount);
                //  objeAuth.HAR_ATTACHMENT = Convert.ToBase64String(filAttachment.FileBytes);
                if (drpTreatmentType.SelectedValue == "3" || drpTreatmentType.SelectedValue == "4")
                {
                    objeAuth.HAR_ENCOUNTER_START = txtEncounterSt.Text.Trim() + " " + drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue;
                    objeAuth.HAR_ENCOUNTER_END = txtEncounterEnd.Text.Trim() + " " + drpFIHour.SelectedValue + ":" + drpFIMin.SelectedValue;
                }
                else
                {
                    objeAuth.HAR_ENCOUNTER_START = "";
                    objeAuth.HAR_ENCOUNTER_END = "";
                }
                objeAuth.HAR_ORDER_DATE = txtOrderDate.Text.Trim();
                objeAuth.HAR_TYPE = "A";
                objeAuth.UserID = hidUserID.Value; //Convert.ToString(Session["User_ID"]);


                string strHAR_ID = "";

                objeAuth.AddAuthorizationRequest(out strHAR_ID);

                // objeAuth.HAR_ID = strHAR_ID;
                //objeAuth.UpdateAttachment(filAttachment.FileBytes);


                DT = new DataTable();
                DT = (DataTable)ViewState["Diagnosis"];
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    objeAuth = new eAuthorizationBAL();
                    objeAuth.HAR_EMR_ID = hidEmrID.Value;
                    objeAuth.HAR_ID = strHAR_ID;
                    objeAuth.HAD_DIAG_CODE = Convert.ToString(DT.Rows[i]["EPD_DIAG_CODE"]);
                    objeAuth.HAD_DIAG_NAME = Convert.ToString(DT.Rows[i]["EPD_DIAG_NAME"]);
                    objeAuth.AddAuthorizationDiag();

                }

                DT = new DataTable();
                DT = (DataTable)ViewState["Activities"];
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    string strHaadCode = "", strServType = "", strObsType = "Text", strServID = "", strServPrice = "0";
                    strHaadCode = Convert.ToString(DT.Rows[i]["SERV_CODE"]);
                    strServID = Convert.ToString(DT.Rows[i]["SERV_ID"]);

                    //Decimal decServPrice = 0;

                    //  if (strHaadCode != "")
                    //  {
                    //    GetHaadServDtls(strHaadCode, out  strServType);
                    //    GetCompanyAgrDtls(strServID, lblInsCode.Text, out  decServPrice);

                    //  }
                    strServType = Convert.ToString(DT.Rows[i]["HAA_ACTIVITY_TYPE"]);


                    if (Convert.ToString(DT.Rows[i]["HAA_NET"]) != "" && Convert.ToString(DT.Rows[i]["HAA_NET"]) != null)
                    {
                        strServPrice = Convert.ToString(DT.Rows[i]["HAA_NET"]);
                    }


                    if (strServType == "6")
                    {
                        strObsType = "Universal Dental";
                    }
                    //else if (strServType == "")
                    //{
                    //    strObsType = "LOINC";
                    //}


                    objeAuth = new eAuthorizationBAL();
                    objeAuth.HAR_EMR_ID = hidEmrID.Value;
                    objeAuth.HAR_ID = strHAR_ID;
                    objeAuth.HAA_SERV_CODE = Convert.ToString(DT.Rows[i]["SERV_CODE"]);
                    objeAuth.HAA_SERV_NAME = Convert.ToString(DT.Rows[i]["SERV_NAME"]);
                    objeAuth.HAA_ACTIVITY_ID = Convert.ToString(i + 1);
                    objeAuth.HAA_ACTIVITY_START = Convert.ToString(DT.Rows[i]["HAA_ACTIVITY_START"]);
                    objeAuth.HAA_ACTIVITY_TYPE = strServType;  // "3";

                    objeAuth.HAA_QTY = Convert.ToString(DT.Rows[i]["QTY"]);
                    objeAuth.HAA_NET = Convert.ToString(strServPrice);// "0.0";
                    objeAuth.HAA_CLINICIAN_ID = strClinicianID;

                    objeAuth.HAA_OBS_TYPE = strObsType;
                    if (strServType == "6")
                    {
                        objeAuth.HAA_OBS_CODE = "Universal Dental";
                        objeAuth.HAA_OBS_VALUE = Convert.ToString(DT.Rows[i]["DURATION"]);
                        objeAuth.HAA_OBS_VALUE_TYPE = ""; // "Days";
                    }
                    else
                    {
                        objeAuth.HAA_OBS_CODE = Convert.ToString(DT.Rows[i]["SERV_CODE"]);// "Duration";
                        objeAuth.HAA_OBS_VALUE = "Chief Complaints:" + txtCC.Text.Trim() + ", Management Plans:" + txtManagementPlan.Text.Trim(); //"1";
                        objeAuth.HAA_OBS_VALUE_TYPE = "Text"; // "Days";
                    }
                    objeAuth.AddAuthorizationActivities();
                    decTotalAmount = decTotalAmount + Convert.ToDecimal(strServPrice);

                }

                /*
                if (filAttachment.FileName != "")
                {
                    string strName = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds + ".PDF";
                   

                    filAttachment.SaveAs(strPath + strName);
                    objeAuth = new eAuthorizationBAL();
                    objeAuth.HAR_ID = strHAR_ID;
                    objeAuth.HAAT_FILE_NAME = strName;
                    objeAuth.HAAT_ATTACHMENT = Convert.ToBase64String(filAttachment.FileBytes);
                    objeAuth.AddAuthorizationAttachments();

                }*/

                DataTable DTAttach = new DataTable();
                DTAttach = (DataTable)ViewState["Attachments"];
                for (int at = 0; at < DTAttach.Rows.Count; at++)
                {
                    string strName = "", strNameActual = "";
                    strName = Convert.ToString(DTAttach.Rows[at]["HAAT_FILE_NAME"]);
                    strNameActual = Convert.ToString(DTAttach.Rows[at]["HAAT_FILE_NAME_ACTUAL"]);

                    string strPath = GlobalValues.AuthorizationAttachmentPath;

                    byte[] bytes = System.IO.File.ReadAllBytes(strPath + strName);
                    objeAuth = new eAuthorizationBAL();
                    objeAuth.HAR_ID = strHAR_ID;
                    objeAuth.HAAT_FILE_NAME = strName;
                    objeAuth.HAAT_FILE_NAME_ACTUAL = strNameActual;
                    objeAuth.HAAT_ATTACHMENT = Convert.ToBase64String(bytes);
                    objeAuth.AddAuthorizationAttachments();

                }



                objeAuth = new eAuthorizationBAL();
                objeAuth.HAR_ID = strHAR_ID;
                objeAuth.HAR_TOTAL_AMOUNT = Convert.ToString(decTotalAmount);
                objeAuth.UpdateAuthorizationTotalAmount();


                BindEMRPTMaster();
                Clear();

                lblStatus.Text = " eAuthorization Generated";
                lblStatus.ForeColor = System.Drawing.Color.Green;

            FunEnd: ;
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eAuthApproval.btnGenerate_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            Clear();
        }

        protected void btneAuthFind_Click(object sender, EventArgs e)
        {
            CleareAuth();
            BindeAuthorizationGrid();
        }

        protected void DeleteAuth_Click(object sender, EventArgs e)
        {
            try
            {
                string strPermission = hidPermission.Value;
                if (strPermission == "7" || strPermission == "9")
                {
                    ImageButton btnEdit = new ImageButton();
                    btnEdit = (ImageButton)sender;

                    GridViewRow gvScanCard;
                    gvScanCard = (GridViewRow)btnEdit.Parent.Parent;


                    Label lblHarID;

                    lblHarID = (Label)gvScanCard.Cells[0].FindControl("lblHarID");
                    eAuthorizationBAL eAuth = new eAuthorizationBAL();
                    eAuth.HAR_ID = lblHarID.Text;
                    eAuth.DeleteAuthorizationRequest();

                    BindeAuthorizationGrid();

                    lblStatus.Text = " eAuthorization Deleted";
                    lblStatus.ForeColor = System.Drawing.Color.Green;
                    ViewState["eAuthSelectIndex"] = "";
                    CleareAuth();
                FunEnd: ;
                }

            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eAuthApproval.DeleteAuth_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void SelectResubmit_Click(object sender, EventArgs e)
        {
            try
            {

                txteAuthCC.Text = "";
                txteAuthManagementPlan.Text = "";
                txtResubComment.Text = "";
                drpResubType.SelectedIndex = 0;


                if (Convert.ToString(ViewState["eAuthSelectIndex"]) != "" && Convert.ToString(ViewState["eAuthSelectIndex"]) != null)
                {
                    gvAuth.Rows[Convert.ToInt32(ViewState["eAuthSelectIndex"])].BackColor = System.Drawing.Color.White;
                }

                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

                ViewState["eAuthSelectIndex"] = gvScanCard.RowIndex;
                gvAuth.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");

                Label lblEmrID, lblHarID, lblStatus, lblResubComment, lblResubType, lblOrderDate, lblTreatType;

                lblEmrID = (Label)gvScanCard.Cells[0].FindControl("lblEmrID");
                lblHarID = (Label)gvScanCard.Cells[0].FindControl("lblHarID");
                lblStatus = (Label)gvScanCard.Cells[0].FindControl("lblStatus");
                lblResubComment = (Label)gvScanCard.Cells[0].FindControl("lblResubComment");
                lblResubType = (Label)gvScanCard.Cells[0].FindControl("lblResubType");
                lblOrderDate = (Label)gvScanCard.Cells[0].FindControl("lblOrderDate");
                lblTreatType = (Label)gvScanCard.Cells[0].FindControl("lblTreatType");

                Label lblResubDrCode = (Label)gvScanCard.Cells[0].FindControl("lblResubDrCode");
                Label lblResubDrName = (Label)gvScanCard.Cells[0].FindControl("lblResubDrName");
                Label lblResubClinicianID = (Label)gvScanCard.Cells[0].FindControl("lblResubClinicianID");

                Label lblResubMemberID = (Label)gvScanCard.Cells[0].FindControl("lblResubMemberID");

                txtResubComment.Text = lblResubComment.Text;

                if (lblResubType.Text != "" && lblResubType.Text != null)
                {
                    drpResubType.SelectedValue = lblResubType.Text;
                }
                if (lblOrderDate.Text != "")
                {
                    txteAuthOrderDate.Text = lblOrderDate.Text;
                }
                else
                {
                    txteAuthOrderDate.Text = "";
                }

                if (lblResubDrCode.Text != "" && lblResubDrName.Text != "")
                {
                    txtResubDoctor.Text = lblResubDrCode.Text + " ~ " + lblResubClinicianID.Text + " ~ " + lblResubDrName.Text;

                }

                txtResubMemberID.Text = lblResubMemberID.Text;




                if (lblTreatType.Text != "")
                {
                    for (int intCount = 0; intCount < drpeAuthTreatmentType.Items.Count; intCount++)
                    {
                        if (drpeAuthTreatmentType.Items[intCount].Value == lblTreatType.Text)
                        {
                            drpeAuthTreatmentType.SelectedValue = lblTreatType.Text;
                            goto ForTreatType;
                        }

                    }
                }
            ForTreatType: ;

                hideAuthEmrID.Value = lblEmrID.Text;
                hidHAR_ID.Value = lblHarID.Text;

                // hideAuthClinicianID.Value=
                BuildeAuthDiagnosis();
                BuildeAuthActivities();
                BuildeAuthAttachments();

                BindeAuthDiagnosis();
                BindTempeAuthDiagnosis();

                BindeAuthActivities();
                BindeAuthTempeActivities();


                BindeAuthAttachments();
                BindeAuthTempAttachments();

            FunEnd: ;


            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eAuthApproval.SelectResubmit_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void btneAuthDiagAdd_Click(object sender, EventArgs e)
        {

            try
            {


                if (Convert.ToString(ViewState["eAuthSelectIndex"]) == "")
                {
                    lblStatus.Text = " Please select one data from above list";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }



                DataTable DT = new DataTable();

                DT = (DataTable)ViewState["eAuthDiagnosis"];

                DataRow objrow;
                objrow = DT.NewRow();
                objrow["EPD_ID"] = hideAuthEmrID.Value;
                objrow["EPD_DIAG_CODE"] = txteAuthDiagCode.Text.Trim(); ;
                objrow["EPD_DIAG_NAME"] = txteAuthDiagName.Text.Trim();
                DT.Rows.Add(objrow);


                ViewState["eAuthDiagnosis"] = DT;
                BindTempeAuthDiagnosis();

                txteAuthDiagCode.Text = "";
                txteAuthDiagName.Text = "";


            FunEnd: ;
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eAuthApproval.btneAuthDiagAdd_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void DeleteeAuthDiag_Click(object sender, EventArgs e)
        {
            try
            {
                string strPermission = hidPermission.Value;
                if (strPermission == "7" || strPermission == "9")
                {
                    ImageButton btnEdit = new ImageButton();
                    btnEdit = (ImageButton)sender;

                    GridViewRow gvScanCard;
                    gvScanCard = (GridViewRow)btnEdit.Parent.Parent;


                    if (ViewState["eAuthDiagnosis"] != null)
                    {
                        DataTable DT = new DataTable();
                        DT = (DataTable)ViewState["eAuthDiagnosis"];
                        DT.Rows.RemoveAt(gvScanCard.RowIndex);
                        DT.AcceptChanges();
                        ViewState["eAuthDiagnosis"] = DT;

                    }



                    BindTempeAuthDiagnosis();
                }
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eAuthApproval.DeleteDiag_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }


        protected void btneAuthActivityAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToString(ViewState["eAuthSelectIndex"]) == "")
                {
                    lblStatus.Text = " Please select one data from above list";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }


                DataTable DT = new DataTable();

                DT = (DataTable)ViewState["eAuthActivities"];

                Label lblInsCode;
                int intSelected = Convert.ToInt32(ViewState["eAuthSelectIndex"]);

                lblInsCode = (Label)gvAuth.Rows[intSelected].Cells[0].FindControl("lblInsCode");


                string strHaadCode = "", strServType = "", strServID = "";
                strHaadCode = txteAuthServCode.Text.Trim();
                strServID = hideAuthServID.Value.Trim();

                if (strServID == "")
                {
                    lblStatus.Text = " Service ID is Empty";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                Decimal decServPrice = 0;

                if (strHaadCode != "")
                {
                    GetHaadServDtls(strHaadCode, out  strServType);
                    GetCompanyAgrDtls(strServID, lblInsCode.Text, out  decServPrice);

                }

                // Decimal decServPrice = 0;

                // GetCompanyAgrDtls(hideAuthServID.Value.Trim(), lblInsCode.Text, out  decServPrice);
                /*
                if (decServPrice == 0)
                {
                    lblStatus.Text = " Selected service is not availbae in Agrement service or Price is not available";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;

                }
                */

                if (ViewState["eAuthActivitySelectIndex"] == "" || ViewState["eAuthActivitySelectIndex"] == null)
                {

                    DataRow objrow;
                    objrow = DT.NewRow();
                    objrow["EMR_ID"] = hideAuthEmrID.Value;
                    objrow["SERV_CODE"] = txteAuthServCode.Text.Trim(); ;
                    objrow["SERV_NAME"] = txteAuthServName.Text.Trim();
                    objrow["QTY"] = txteAuthQty.Text.Trim();

                    decimal decQty = 1, decPrice = 0;
                    if (txteAuthQty.Text.Trim() != "")
                    {
                        decQty = Convert.ToDecimal(txteAuthQty.Text.Trim());
                    }
                    decPrice = decQty * decServPrice;


                    objrow["HAA_NET"] = decPrice;
                    objrow["HAA_ACTIVITY_START"] = txteAuthActivityDate.Text.Trim() + " " + drpeAuthActivityHour.SelectedValue + ":" + drpeAuthActivityMin.SelectedValue;
                    objrow["HAA_ACTIVITY_TYPE"] = strServType;
                    objrow["SERV_ID"] = hideAuthServID.Value.Trim();

                    if (chkeAuthDental.Checked == true)
                    {
                        objrow["DURATION"] = txteAuthToothNo.Text.Trim();
                    }
                    else
                    {
                        objrow["DURATION"] = "";
                    }


                    DT.Rows.Add(objrow);
                }
                else
                {
                    Int32 R = Convert.ToInt32(ViewState["eAuthActivitySelectIndex"]);
                    DT.Rows[R]["EMR_ID"] = hideAuthEmrID.Value;
                    DT.Rows[R]["SERV_CODE"] = txteAuthServCode.Text.Trim(); ;
                    DT.Rows[R]["SERV_NAME"] = txteAuthServName.Text.Trim();
                    DT.Rows[R]["QTY"] = txteAuthQty.Text.Trim();


                    decimal decQty = 1, decPrice = 0;
                    if (txteAuthQty.Text.Trim() != "")
                    {
                        decQty = Convert.ToDecimal(txteAuthQty.Text.Trim());
                    }
                    if (txteAuthNet.Text.Trim() != "")
                    {
                        decPrice = Convert.ToDecimal(txteAuthNet.Text.Trim());
                    }

                    decPrice = decQty * decPrice;

                    DT.Rows[R]["HAA_NET"] = decPrice;

                    DT.Rows[R]["HAA_ACTIVITY_START"] = txteAuthActivityDate.Text.Trim() + " " + drpeAuthActivityHour.SelectedValue + ":" + drpeAuthActivityMin.SelectedValue;
                    DT.Rows[R]["HAA_ACTIVITY_TYPE"] = txteAuthType.Text.Trim();

                    DT.Rows[R]["SERV_ID"] = hideAuthServID.Value.Trim();

                    if (chkeAuthDental.Checked == true)
                    {
                        DT.Rows[R]["DURATION"] = txteAuthToothNo.Text.Trim();
                    }
                    else
                    {
                        DT.Rows[R]["DURATION"] = "";
                    }


                    DT.AcceptChanges();
                }

                ViewState["eAuthActivities"] = DT;
                BindeAuthTempeActivities();

                CleareAuthActivity();

            FunEnd: ;
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eAuthApproval.btneAuthActivityAdd");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void SelecteAuthActivity_Click(object sender, EventArgs e)
        {
            try
            {
                CleareAuthActivity();

                lbleAuthType.Visible = true;
                txteAuthType.Visible = true;
                lbleAuthPrice.Visible = true;
                txteAuthNet.Visible = true;
                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;


                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
                ViewState["eAuthActivitySelectIndex"] = gvScanCard.RowIndex;

                Label lblServCode, lblServName, lblQty, lblObsValue, lblActivityType, lblNet, lblActivityStart;


                lblServCode = (Label)gvScanCard.Cells[0].FindControl("lblServCode");
                lblServName = (Label)gvScanCard.Cells[0].FindControl("lblServName");
                lblQty = (Label)gvScanCard.Cells[0].FindControl("lblQty");

                lblObsValue = (Label)gvScanCard.Cells[0].FindControl("lblObsValue");
                lblActivityType = (Label)gvScanCard.Cells[0].FindControl("lblActivityType");
                lblNet = (Label)gvScanCard.Cells[0].FindControl("lblNet");

                lblActivityStart = (Label)gvScanCard.Cells[0].FindControl("lblActivityStart");

                txteAuthServCode.Text = lblServCode.Text;
                txteAuthServName.Text = lblServName.Text;
                txteAuthQty.Text = lblQty.Text;

                if (chkeAuthDental.Checked == true)
                {
                    txteAuthToothNo.Text = lblObsValue.Text;
                }
                txteAuthType.Text = lblActivityType.Text;
                txteAuthNet.Text = lblNet.Text;

                string strActivityStart = "", strSTHour = "";
                strActivityStart = lblActivityStart.Text;

                string[] arrActivitydt = strActivityStart.Split(' ');

                if (arrActivitydt.Length > 1)
                {
                    txteAuthActivityDate.Text = arrActivitydt[0];
                    strSTHour = arrActivitydt[1];

                }


                string[] arrSTHour = strSTHour.Split(':');
                if (arrSTHour.Length > 1)
                {
                    drpeAuthActivityHour.SelectedValue = arrSTHour[0];
                    drpeAuthActivityMin.SelectedValue = arrSTHour[1];

                }

                string ServID = "", ServName = "";
                string ServCode = txteAuthServCode.Text.Trim();
                GetServiceMasterName(ServCode, out ServID, out  ServName);
                hideAuthServID.Value = ServID;



            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eAuthApproval.SelecteAuthActivity_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void DeleteeAuthActivity_Click(object sender, EventArgs e)
        {
            try
            {
                string strPermission = hidPermission.Value;
                if (strPermission == "7" || strPermission == "9")
                {
                    ImageButton btnEdit = new ImageButton();
                    btnEdit = (ImageButton)sender;

                    GridViewRow gvScanCard;
                    gvScanCard = (GridViewRow)btnEdit.Parent.Parent;


                    if (ViewState["eAuthActivities"] != null)
                    {
                        DataTable DT = new DataTable();
                        DT = (DataTable)ViewState["eAuthActivities"];
                        DT.Rows.RemoveAt(gvScanCard.RowIndex);
                        DT.AcceptChanges();
                        ViewState["eAuthActivities"] = DT;

                    }

                    BindeAuthTempeActivities();
                }
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eAuthApproval.DeleteService_Click");
                TextFileWriting(ex.Message.ToString());
            }



        }

        protected void btneAuthAttaAdd_Click(object sender, EventArgs e)
        {

            try
            {
                string strName = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds + ".PDF";
                string strPath = GlobalValues.AuthorizationAttachmentPath;


                if (Convert.ToString(ViewState["eAuthSelectIndex"]) == "")
                {
                    lblStatus.Text = " Please select one data from above list";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                Int32 intLength;
                intLength = fileAuthAttachment.PostedFile.ContentLength;

                if (intLength >= 4194304)
                {
                    lblStatus.Text = " File size not exceed 4 MB";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                fileAuthAttachment.SaveAs(strPath + strName);
                DataTable DT = new DataTable();

                DT = (DataTable)ViewState["eAuthAttachments"];

                DataRow objrow;
                objrow = DT.NewRow();
                objrow["HAAT_FILE_NAME"] = strName;
                objrow["HAAT_FILE_NAME_ACTUAL"] = fileAuthAttachment.FileName;
                objrow["IsNewFile"] = "1";
                DT.Rows.Add(objrow);

                ViewState["eAuthAttachments"] = DT;
                BindeAuthTempAttachments();


            FunEnd: ;
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eAuthApproval.btnDiagAdd_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void DeleteeAuthAtta_Click(object sender, EventArgs e)
        {
            try
            {
                string strPermission = hidPermission.Value;
                if (strPermission == "7" || strPermission == "9")
                {
                    ImageButton btnEdit = new ImageButton();
                    btnEdit = (ImageButton)sender;

                    GridViewRow gvScanCard;
                    gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

                    Label lblHaat_ID;
                    lblHaat_ID = (Label)gvScanCard.Cells[0].FindControl("lblHaat_ID");

                    if (ViewState["eAuthAttachments"] != null)
                    {

                        if (Convert.ToString(ViewState["eAuthAttachDeleted"]) == "")
                        {
                            ViewState["eAuthAttachDeleted"] = lblHaat_ID.Text;
                        }
                        else
                        {
                            ViewState["eAuthAttachDeleted"] += "," + lblHaat_ID.Text;
                        }


                        DataTable DT = new DataTable();
                        DT = (DataTable)ViewState["eAuthAttachments"];
                        DT.Rows.RemoveAt(gvScanCard.RowIndex);
                        DT.AcceptChanges();
                        ViewState["eAuthAttachments"] = DT;

                    }



                    BindeAuthTempAttachments();
                }
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eAuthApproval.DeleteeAuthAtta_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }

        protected void chkeAuthDental_CheckedChanged(object sender, EventArgs e)
        {

            if (chkeAuthDental.Checked == true)
            {
                lbleAuthToothNo.Visible = true;
                txteAuthToothNo.Visible = true;

            }
            else
            {
                lbleAuthToothNo.Visible = false;
                txteAuthToothNo.Visible = false;

            }
        }

        protected void btnResubmisionGenerate_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable DT;

                if (hidHAR_ID.Value == "")
                {
                    lblStatus.Text = " Please select the Authorization Data";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                //Int32 intLength;
                //intLength = fileAuthAttachment.PostedFile.ContentLength;

                ////below checking for file size more that 4MB
                //if (intLength >= 4194304)
                //{
                //    lblStatus.Text = " File size not exceed 4 MB";
                //    lblStatus.ForeColor = System.Drawing.Color.Red;
                //    goto FunEnd;
                //}


                DT = new DataTable();
                DT = (DataTable)ViewState["eAuthDiagnosis"];
                if (DT.Rows.Count <= 0)
                {
                    lblStatus.Text = " Please add  the Diagnosis";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                DT = new DataTable();
                DT = (DataTable)ViewState["eAuthActivities"];
                if (DT.Rows.Count <= 0)
                {
                    lblStatus.Text = " Please add the Activities";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }


                if (drpResubType.SelectedIndex == 0)
                {
                    lblStatus.Text = " Please select Resubmission Type";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }


                eAuthorizationBAL objeAuth;

                objeAuth = new eAuthorizationBAL();

                objeAuth.HAR_ID = hidHAR_ID.Value;


                ////string strClinicianID = "";

                ////string strDoctor = txtResubDoctor.Text.Trim();
                ////string[] arrDoctor = strDoctor.Split('/');

                ////if (arrDoctor.Length > 1)
                ////{
                ////    objeAuth.HAR_DR_CODE = arrDoctor[0].Trim();

                ////    string strDoctor1 = arrDoctor[1].Trim();
                ////    string[] arrDoctor1 = strDoctor1.Split('~');
                ////    if (arrDoctor1.Length > 1)
                ////    {
                ////        objeAuth.HAR_CLINICIAN_ID = arrDoctor1[0].Trim();
                ////        strClinicianID = arrDoctor1[0].Trim();
                ////        objeAuth.HAR_DR_NAME = arrDoctor1[1].Trim();

                ////    }

                ////}
                ////else
                ////{
                ////    objeAuth.HAR_DR_CODE = "";
                ////    objeAuth.HAR_DR_NAME = "";
                ////    objeAuth.HAR_CLINICIAN_ID = "";
                ////}


                string strClinicianID = "";
                string strDoctor = txtResubDoctor.Text.Trim();

                string[] arrDoctor = strDoctor.Split('~');
                // string[] arrDoctor = strDoctor.Split('/');

                if (arrDoctor.Length > 1)
                {
                    objeAuth.HAR_DR_CODE = arrDoctor[0].Trim();
                    objeAuth.HAR_CLINICIAN_ID = arrDoctor[1].Trim();
                    strClinicianID = arrDoctor[1].Trim();
                    objeAuth.HAR_DR_NAME = arrDoctor[2].Trim();

                }
                else
                {
                    objeAuth.HAR_DR_CODE = "";
                    objeAuth.HAR_DR_NAME = "";
                    objeAuth.HAR_CLINICIAN_ID = "";
                }




                objeAuth.HAR_MEMBER_ID = txtResubMemberID.Text.Trim();


                if (objeAuth.HAR_CLINICIAN_ID == "")
                {
                    lblStatus.Text = " Please enter the doctor details";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }



                if (txtResubMemberID.Text.Trim() == "") // if (lblMemberID.Text == "")
                {
                    lblStatus.Text = " Please enter the Member ID";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                objeAuth.HAR_ENCOUNTER_TYPE = drpeAuthTreatmentType.SelectedValue;
                objeAuth.HAR_RESUB_COMMENTS = txtResubComment.Text.Trim();
                objeAuth.HAR_RESUB_TYPE = drpResubType.SelectedValue;
                objeAuth.HAR_ORDER_DATE = txteAuthOrderDate.Text.Trim();
                objeAuth.HAR_STATUS = "4";
                objeAuth.UserID = hidUserID.Value;// Convert.ToString(Session["User_ID"]);

                objeAuth.ResubmitAuthorization();

                Decimal decTotalAmount = 0;


                Label lblInsCode;
                int intSelected = Convert.ToInt32(ViewState["eAuthSelectIndex"]);
                lblInsCode = (Label)gvAuth.Rows[intSelected].Cells[0].FindControl("lblInsCode");


                DT = new DataTable();
                DT = (DataTable)ViewState["eAuthDiagnosis"];
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    objeAuth = new eAuthorizationBAL();
                    objeAuth.HAR_EMR_ID = hideAuthEmrID.Value;
                    objeAuth.HAR_ID = hidHAR_ID.Value;
                    objeAuth.HAD_DIAG_CODE = Convert.ToString(DT.Rows[i]["EPD_DIAG_CODE"]);
                    objeAuth.HAD_DIAG_NAME = Convert.ToString(DT.Rows[i]["EPD_DIAG_NAME"]);
                    objeAuth.AddAuthorizationDiag();

                }

                DT = new DataTable();
                DT = (DataTable)ViewState["eAuthActivities"];
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    string strServCode = "", strServType = "", strObsType = "Text", strServID, strServPrice = "0";
                    strServCode = Convert.ToString(DT.Rows[i]["SERV_CODE"]);
                    strServID = Convert.ToString(DT.Rows[i]["SERV_ID"]);
                    // Decimal decServPrice = 0;

                    // if (strServCode != "")
                    //     GetHaadServDtls(strServCode, out  strServType);
                    // GetCompanyAgrDtls(strServID, lblInsCode.Text, out  decServPrice);


                    strServType = Convert.ToString(DT.Rows[i]["HAA_ACTIVITY_TYPE"]);


                    if (Convert.ToString(DT.Rows[i]["HAA_NET"]) != "" && Convert.ToString(DT.Rows[i]["HAA_NET"]) != null)
                    {
                        strServPrice = Convert.ToString(DT.Rows[i]["HAA_NET"]);
                    }


                    if (strServType == "6")
                    {
                        strObsType = "Universal Dental";
                    }
                    //else if (strServType == "")
                    //{
                    //    strObsType = "LOINC";
                    //}


                    objeAuth = new eAuthorizationBAL();
                    objeAuth.HAR_EMR_ID = hideAuthEmrID.Value;
                    objeAuth.HAR_ID = hidHAR_ID.Value;
                    objeAuth.HAA_SERV_CODE = Convert.ToString(DT.Rows[i]["SERV_CODE"]);
                    objeAuth.HAA_SERV_NAME = Convert.ToString(DT.Rows[i]["SERV_NAME"]);
                    objeAuth.HAA_ACTIVITY_ID = Convert.ToString(i + 1);
                    objeAuth.HAA_ACTIVITY_START = objeAuth.HAA_ACTIVITY_START = Convert.ToString(DT.Rows[i]["HAA_ACTIVITY_START"]);
                    objeAuth.HAA_ACTIVITY_TYPE = strServType;  // "3";

                    objeAuth.HAA_QTY = Convert.ToString(DT.Rows[i]["QTY"]);
                    objeAuth.HAA_NET = Convert.ToString(strServPrice);// "0.0";
                    objeAuth.HAA_CLINICIAN_ID = hideAuthClinicianID.Value;

                    objeAuth.HAA_OBS_TYPE = strObsType;
                    if (strServType == "6")
                    {
                        objeAuth.HAA_OBS_CODE = "Universal Dental";
                        objeAuth.HAA_OBS_VALUE = Convert.ToString(DT.Rows[i]["DURATION"]);
                        objeAuth.HAA_OBS_VALUE_TYPE = ""; // "Days";
                    }
                    else
                    {
                        objeAuth.HAA_OBS_CODE = Convert.ToString(DT.Rows[i]["SERV_CODE"]);// "Duration";
                        objeAuth.HAA_OBS_VALUE = "Chief Complaints:" + txteAuthCC.Text.Trim() + ", Management Plans:" + txteAuthManagementPlan.Text.Trim(); //"1";
                        objeAuth.HAA_OBS_VALUE_TYPE = "Text"; // "Days";
                    }
                    objeAuth.AddAuthorizationActivities();

                    decTotalAmount = decTotalAmount + Convert.ToDecimal(strServPrice);
                    /*
                    if (fileAuthAttachment.FileName != "")
                    {
                        string strName = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds + ".PDF";
                        string strPath = GlobalValues.AuthorizationAttachmentPath;

                        fileAuthAttachment.SaveAs(strPath + strName);
                        objeAuth = new eAuthorizationBAL();
                        objeAuth.HAR_ID = hidHAR_ID.Value;
                        objeAuth.HAAT_FILE_NAME = strName;
                        objeAuth.HAAT_ATTACHMENT = Convert.ToBase64String(fileAuthAttachment.FileBytes);
                        objeAuth.AddAuthorizationAttachments();

                    }
                    */


                }


                DataTable DTAttach = new DataTable();
                DTAttach = (DataTable)ViewState["eAuthAttachments"];
                for (int at = 0; at < DTAttach.Rows.Count; at++)
                {
                    string strName = "", strNameActual = "", IsNewFile;
                    strName = Convert.ToString(DTAttach.Rows[at]["HAAT_FILE_NAME"]);
                    strNameActual = Convert.ToString(DTAttach.Rows[at]["HAAT_FILE_NAME_ACTUAL"]);
                    IsNewFile = Convert.ToString(DTAttach.Rows[at]["IsNewFile"]);

                    if (IsNewFile == "1")
                    {
                        string strPath = GlobalValues.AuthorizationAttachmentPath;

                        byte[] bytes = System.IO.File.ReadAllBytes(strPath + strName);
                        objeAuth = new eAuthorizationBAL();
                        objeAuth.HAR_ID = hidHAR_ID.Value; ;
                        objeAuth.HAAT_FILE_NAME = strName;
                        objeAuth.HAAT_FILE_NAME_ACTUAL = strNameActual;
                        objeAuth.HAAT_ATTACHMENT = Convert.ToBase64String(bytes);
                        objeAuth.AddAuthorizationAttachments();
                    }

                }

                if (Convert.ToString(ViewState["eAuthAttachDeleted"]) != "")
                {

                    string strHaatId = "";
                    strHaatId = Convert.ToString(ViewState["eAuthAttachDeleted"]);
                    string[] arrHaatId = strHaatId.Split(',');

                    if (arrHaatId.Length >= 1)
                    {

                        for (int intobs = 0; intobs < arrHaatId.Length; intobs++)
                        {
                            if (Convert.ToString(arrHaatId[intobs]) != "")
                            {
                                //strHaatId = strHaatId.Substring(0, strHaatId.Length - 1);
                                objeAuth = new eAuthorizationBAL();

                                objeAuth.HAAT_ID = Convert.ToString(arrHaatId[intobs]);
                                objeAuth.AuthorizationAttachmentsDelete();
                            }

                        }

                    }

                }

                BindeAuthorizationGrid();
                CleareAuth();

                lblStatus.Text = " eAuthorization Generated";
                lblStatus.ForeColor = System.Drawing.Color.Green;
            FunEnd: ;

            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eAuthApproval.btnResubmisionGenerate_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable DT;

                if (hidHAR_ID.Value == "")
                {
                    lblStatus.Text = " Please select the Authorization Data";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                //Int32 intLength;
                //intLength = fileAuthAttachment.PostedFile.ContentLength;

                ////below checking for file size more that 4MB
                //if (intLength >= 4194304)
                //{
                //    lblStatus.Text = " File size not exceed 4 MB";
                //    lblStatus.ForeColor = System.Drawing.Color.Red;
                //    goto FunEnd;
                //}


                DT = new DataTable();
                DT = (DataTable)ViewState["eAuthDiagnosis"];
                if (DT.Rows.Count <= 0)
                {
                    lblStatus.Text = " Please add  the Diagnosis";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                DT = new DataTable();
                DT = (DataTable)ViewState["eAuthActivities"];
                if (DT.Rows.Count <= 0)
                {
                    lblStatus.Text = " Please add the Activities";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                eAuthorizationBAL objeAuth;

                objeAuth = new eAuthorizationBAL();

                objeAuth.HAR_ID = hidHAR_ID.Value;
                string strClinicianID = "";

                string strDoctor = txtResubDoctor.Text.Trim();
                string[] arrDoctor = strDoctor.Split('/');

                if (arrDoctor.Length > 1)
                {
                    objeAuth.HAR_DR_CODE = arrDoctor[0].Trim();

                    string strDoctor1 = arrDoctor[1].Trim();
                    string[] arrDoctor1 = strDoctor1.Split('~');
                    if (arrDoctor1.Length > 1)
                    {
                        objeAuth.HAR_CLINICIAN_ID = arrDoctor1[0].Trim();
                        strClinicianID = arrDoctor1[0].Trim();
                        objeAuth.HAR_DR_NAME = arrDoctor1[1].Trim();

                    }

                }
                else
                {
                    objeAuth.HAR_DR_CODE = "";
                    objeAuth.HAR_DR_NAME = "";
                    objeAuth.HAR_CLINICIAN_ID = "";
                }

                objeAuth.HAR_MEMBER_ID = txtResubMemberID.Text.Trim();

                if (objeAuth.HAR_CLINICIAN_ID == "")
                {
                    lblStatus.Text = " Please enter the doctor details";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }



                if (txtResubMemberID.Text.Trim() == "") // if (lblMemberID.Text == "")
                {
                    lblStatus.Text = " Please enter the Member ID";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }


                objeAuth.HAR_ENCOUNTER_TYPE = drpeAuthTreatmentType.SelectedValue;
                objeAuth.HAR_RESUB_COMMENTS = txtResubComment.Text.Trim();
                objeAuth.HAR_RESUB_TYPE = drpResubType.SelectedValue;
                objeAuth.HAR_ORDER_DATE = txteAuthOrderDate.Text.Trim();

                if (drpResubType.SelectedIndex != 0)
                {
                    objeAuth.HAR_STATUS = "4";
                }
                else
                {
                    objeAuth.HAR_STATUS = "0";
                }

                objeAuth.UserID = hidUserID.Value;// Convert.ToString(Session["User_ID"]);

                objeAuth.ResubmitAuthorization();

                Decimal decTotalAmount = 0;


                Label lblInsCode;
                int intSelected = Convert.ToInt32(ViewState["eAuthSelectIndex"]);
                lblInsCode = (Label)gvAuth.Rows[intSelected].Cells[0].FindControl("lblInsCode");


                DT = new DataTable();
                DT = (DataTable)ViewState["eAuthDiagnosis"];
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    objeAuth = new eAuthorizationBAL();
                    objeAuth.HAR_EMR_ID = hideAuthEmrID.Value;
                    objeAuth.HAR_ID = hidHAR_ID.Value;
                    objeAuth.HAD_DIAG_CODE = Convert.ToString(DT.Rows[i]["EPD_DIAG_CODE"]);
                    objeAuth.HAD_DIAG_NAME = Convert.ToString(DT.Rows[i]["EPD_DIAG_NAME"]);
                    objeAuth.AddAuthorizationDiag();

                }

                DT = new DataTable();
                DT = (DataTable)ViewState["eAuthActivities"];
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    string strServCode = "", strServType = "", strObsType = "Text", strServID, strServPrice = "0";
                    strServCode = Convert.ToString(DT.Rows[i]["SERV_CODE"]);
                    strServID = Convert.ToString(DT.Rows[i]["SERV_ID"]);
                    // Decimal decServPrice = 0;

                    // if (strServCode != "")
                    //     GetHaadServDtls(strServCode, out  strServType);
                    // GetCompanyAgrDtls(strServID, lblInsCode.Text, out  decServPrice);


                    strServType = Convert.ToString(DT.Rows[i]["HAA_ACTIVITY_TYPE"]);


                    if (Convert.ToString(DT.Rows[i]["HAA_NET"]) != "" && Convert.ToString(DT.Rows[i]["HAA_NET"]) != null)
                    {
                        strServPrice = Convert.ToString(DT.Rows[i]["HAA_NET"]);
                    }


                    if (strServType == "6")
                    {
                        strObsType = "Universal Dental";
                    }
                    //else if (strServType == "")
                    //{
                    //    strObsType = "LOINC";
                    //}


                    objeAuth = new eAuthorizationBAL();
                    objeAuth.HAR_EMR_ID = hideAuthEmrID.Value;
                    objeAuth.HAR_ID = hidHAR_ID.Value;
                    objeAuth.HAA_SERV_CODE = Convert.ToString(DT.Rows[i]["SERV_CODE"]);
                    objeAuth.HAA_SERV_NAME = Convert.ToString(DT.Rows[i]["SERV_NAME"]);
                    objeAuth.HAA_ACTIVITY_ID = Convert.ToString(i + 1);
                    objeAuth.HAA_ACTIVITY_START = objeAuth.HAA_ACTIVITY_START = Convert.ToString(DT.Rows[i]["HAA_ACTIVITY_START"]);
                    objeAuth.HAA_ACTIVITY_TYPE = strServType;  // "3";

                    objeAuth.HAA_QTY = Convert.ToString(DT.Rows[i]["QTY"]);
                    objeAuth.HAA_NET = Convert.ToString(strServPrice);// "0.0";
                    objeAuth.HAA_CLINICIAN_ID = hideAuthClinicianID.Value;

                    objeAuth.HAA_OBS_TYPE = strObsType;
                    if (strServType == "6")
                    {
                        objeAuth.HAA_OBS_CODE = "Universal Dental";
                        objeAuth.HAA_OBS_VALUE = Convert.ToString(DT.Rows[i]["DURATION"]);
                        objeAuth.HAA_OBS_VALUE_TYPE = ""; // "Days";
                    }
                    else
                    {
                        objeAuth.HAA_OBS_CODE = Convert.ToString(DT.Rows[i]["SERV_CODE"]);// "Duration";
                        objeAuth.HAA_OBS_VALUE = "Chief Complaints:" + txteAuthCC.Text.Trim() + ", Management Plans:" + txteAuthManagementPlan.Text.Trim(); //"1";
                        objeAuth.HAA_OBS_VALUE_TYPE = "Text"; // "Days";
                    }
                    objeAuth.AddAuthorizationActivities();

                    decTotalAmount = decTotalAmount + Convert.ToDecimal(strServPrice);
                    /*
                    if (fileAuthAttachment.FileName != "")
                    {
                        string strName = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds + ".PDF";
                        string strPath = GlobalValues.AuthorizationAttachmentPath;

                        fileAuthAttachment.SaveAs(strPath + strName);
                        objeAuth = new eAuthorizationBAL();
                        objeAuth.HAR_ID = hidHAR_ID.Value;
                        objeAuth.HAAT_FILE_NAME = strName;
                        objeAuth.HAAT_ATTACHMENT = Convert.ToBase64String(fileAuthAttachment.FileBytes);
                        objeAuth.AddAuthorizationAttachments();

                    }
                    */


                }


                DataTable DTAttach = new DataTable();
                DTAttach = (DataTable)ViewState["eAuthAttachments"];
                for (int at = 0; at < DTAttach.Rows.Count; at++)
                {
                    string strName = "", strNameActual = "", IsNewFile;
                    strName = Convert.ToString(DTAttach.Rows[at]["HAAT_FILE_NAME"]);
                    strNameActual = Convert.ToString(DTAttach.Rows[at]["HAAT_FILE_NAME_ACTUAL"]);
                    IsNewFile = Convert.ToString(DTAttach.Rows[at]["IsNewFile"]);

                    if (IsNewFile == "1")
                    {
                        string strPath = GlobalValues.AuthorizationAttachmentPath;

                        byte[] bytes = System.IO.File.ReadAllBytes(strPath + strName);
                        objeAuth = new eAuthorizationBAL();
                        objeAuth.HAR_ID = hidHAR_ID.Value; ;
                        objeAuth.HAAT_FILE_NAME = strName;
                        objeAuth.HAAT_FILE_NAME_ACTUAL = strNameActual;
                        objeAuth.HAAT_ATTACHMENT = Convert.ToBase64String(bytes);
                        objeAuth.AddAuthorizationAttachments();
                    }

                }

                if (Convert.ToString(ViewState["eAuthAttachDeleted"]) != "")
                {

                    string strHaatId = "";
                    strHaatId = Convert.ToString(ViewState["eAuthAttachDeleted"]);
                    string[] arrHaatId = strHaatId.Split(',');

                    if (arrHaatId.Length >= 1)
                    {

                        for (int intobs = 0; intobs < arrHaatId.Length; intobs++)
                        {
                            if (Convert.ToString(arrHaatId[intobs]) != "")
                            {
                                //strHaatId = strHaatId.Substring(0, strHaatId.Length - 1);
                                objeAuth = new eAuthorizationBAL();

                                objeAuth.HAAT_ID = Convert.ToString(arrHaatId[intobs]);
                                objeAuth.AuthorizationAttachmentsDelete();
                            }

                        }

                    }

                }

                BindeAuthorizationGrid();
                CleareAuth();

                lblStatus.Text = " eAuthorization Generated";
                lblStatus.ForeColor = System.Drawing.Color.Green;
            FunEnd: ;

            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eAuthApproval.btnResubmisionGenerate_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void btnCancelAuth_Click(object sender, EventArgs e)
        {
            try
            {

                if (hidHAR_ID.Value == "")
                {
                    lblStatus.Text = " Please select the Authorization Data";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                eAuthorizationBAL objeAuth = new eAuthorizationBAL();
                objeAuth.HAR_ID = hidHAR_ID.Value;
                objeAuth.CancelAuthorization();



                BindeAuthorizationGrid();
                CleareAuth();

                lblStatus.Text = " eAuthorization Cancelled";
                lblStatus.ForeColor = System.Drawing.Color.Green;

            FunEnd: ;
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eAuthApproval.btnCancelAuth_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void btneAuthReport_Click(object sender, EventArgs e)
        {
            if (hidHAR_ID.Value == "")
            {
                lblStatus.Text = " Please select the Authorization Data";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                goto FunEnd;
            }

            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "eAuthApprovalReport('" + hidHAR_ID.Value + "');", true);

        FunEnd: ;


        }

        protected void txtDiagCode_TextChanged(object sender, EventArgs e)
        {

            GetDiagnosisName();

        }

        protected void txtDiagName_TextChanged(object sender, EventArgs e)
        {

            GetDiagnosisCode();

        }

        protected void txtServCode_TextChanged(object sender, EventArgs e)
        {
            string ServID = "", ServName = "";
            string ServCode = txtServCode.Text.Trim();
            GetServiceMasterName(ServCode, out ServID, out  ServName);
            hidServID.Value = ServID;
            txtServName.Text = ServName;

        }

        protected void txtServName_TextChanged(object sender, EventArgs e)
        {

            GetServiceMasterCode();

        }

        protected void btnGetkXMLFile_Click(object sender, EventArgs e)
        {
            try
            {

                if (Convert.ToString(ViewState["eAuthSelectIndex"]) == "")
                {
                    lblStatus.Text = " Please select one data from above list";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                XMLFileWrite();

            FunEnd: ;
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eAuthApproval.btnGetkXMLFile_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void txteAuthServCode_TextChanged(object sender, EventArgs e)
        {

            string ServID = "", ServName = "";
            string ServCode = txteAuthServCode.Text.Trim();
            GetServiceMasterName(ServCode, out ServID, out  ServName);
            hideAuthServID.Value = ServID;
            txteAuthServName.Text = ServName;

        }



        //protected void DeleteeAuthAtta_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        ImageButton btnEdit = new ImageButton();
        //        btnEdit = (ImageButton)sender;

        //        GridViewRow gvScanCard;
        //        gvScanCard = (GridViewRow)btnEdit.Parent.Parent;


        //        if (ViewState["Attachments"] != null)
        //        {
        //            DataTable DT = new DataTable();
        //            DT = (DataTable)ViewState["Attachments"];
        //            DT.Rows.RemoveAt(gvScanCard.RowIndex);
        //            DT.AcceptChanges();
        //            ViewState["Attachments"] = DT;

        //        }


        //        BindTempAttachments();
        //    }
        //    catch (Exception ex)
        //    {

        //        TextFileWriting("-----------------------------------------------");
        //        TextFileWriting(System.DateTime.Now.ToString() + "      eAuthApproval.DeleteAtta_Click");
        //        TextFileWriting(ex.Message.ToString());
        //    }


        //}

    }
}