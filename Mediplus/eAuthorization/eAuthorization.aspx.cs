﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using Mediplus_BAL;
using System.Collections;
using System.Text;

namespace Mediplus.eAuthorization
{
    public partial class eAuthorization : System.Web.UI.Page
    {
        static string strSessionBranchId;

        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindProvider()
        {

            DataSet ds = new DataSet();
            CommonBAL objphyCom = new CommonBAL();

            string Criteria = " 1=1 AND HAC_STATUS='A' ";
            ds = objphyCom.AuthorizationCredentialGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                drpProvider.DataSource = ds;
                drpProvider.DataValueField = "HAC_PROVIDER_ID";
                drpProvider.DataTextField = "HAC_PROVIDER_NAME";
                drpProvider.DataBind();



            }

            drpProvider.Items.Insert(0, "--- Select ---");
            drpProvider.Items[0].Value = "";




            if (drpProvider.Items.Count == 2)
            {
                drpProvider.SelectedIndex = 1;


            }
        }

        void GetCompanyDtls()
        {
            ViewState["BillToCode"] = "";
            CommonBAL objCom = new CommonBAL();

            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HCM_STATUS = 'A' AND HCM_BRANCH_ID = '" + strSessionBranchId + "' ";
            Criteria += " AND HCM_COMP_ID = '" + txtCompany.Text.Trim() + "'";


            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_inccmp_details(Criteria);
            if (DS.Tables[0].Rows.Count > 00)
            {
                txtCompanyName.Text = Convert.ToString(DS.Tables[0].Rows[0]["CUST_NAME"]);
                txtPayerID.Text = Convert.ToString(DS.Tables[0].Rows[0]["Addr2"]);
                txtReciverID.Text = Convert.ToString(DS.Tables[0].Rows[0]["Addr1"]);
                ViewState["BillToCode"] = Convert.ToString(DS.Tables[0].Rows[0]["billtoacno"]);

                //LogFileWriting(" CompAcNo =" + txtCompany.Text.Trim() + " Bill to Code=" + Convert.ToString(ViewState["BillToCode"]));
            }

        }

        void BuildDiagnosis()
        {
            DataTable dt = new DataTable();

            DataColumn EPD_ID = new DataColumn();
            EPD_ID.ColumnName = "EPD_ID";

            DataColumn EPD_DIAG_CODE = new DataColumn();
            EPD_DIAG_CODE.ColumnName = "EPD_DIAG_CODE";

            DataColumn EPD_DIAG_NAME = new DataColumn();
            EPD_DIAG_NAME.ColumnName = "EPD_DIAG_NAME";

            DataColumn EPD_TYPE = new DataColumn();
            EPD_TYPE.ColumnName = "EPD_TYPE";

            dt.Columns.Add(EPD_ID);
            dt.Columns.Add(EPD_DIAG_CODE);
            dt.Columns.Add(EPD_DIAG_NAME);
            dt.Columns.Add(EPD_TYPE);

            ViewState["eAuthDiagnosis"] = dt;
        }

        void BindDiagnosis()
        {
            string Criteria = " 1=1 ";

            Criteria += " AND HAD_HAR_ID='" + Convert.ToString(ViewState["HAR_ID"]) + "'";

            DataSet DS = new DataSet();
            eAuthorizationBAL objeAuth = new eAuthorizationBAL();
            DS = objeAuth.AuthorizationDiagnosisGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                DataTable DT = new DataTable();
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    DT = (DataTable)ViewState["eAuthDiagnosis"];

                    DataRow objrow;
                    objrow = DT.NewRow();
                    objrow["EPD_ID"] = Convert.ToString(DS.Tables[0].Rows[i]["HAD_EMR_ID"]);
                    objrow["EPD_DIAG_CODE"] = Convert.ToString(DS.Tables[0].Rows[i]["HAD_DIAG_CODE"]);
                    objrow["EPD_DIAG_NAME"] = Convert.ToString(DS.Tables[0].Rows[i]["HAD_DIAG_NAME"]);
                    objrow["EPD_TYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["EPD_TYPE"]);
                    DT.Rows.Add(objrow);
                }

                ViewState["eAuthDiagnosis"] = DT;
            }


        }

        void BindEMRDignosis()
        {
            string Criteria = " 1=1 ";
            // Criteria += " AND EPD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND EPD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND EPD_ID='" +  Convert.ToString( ViewState["EMR_ID"]) + "'";

            DataSet DS = new DataSet();
            EMR_BAL.EMR_PTDiagnosis objDiag = new EMR_BAL.EMR_PTDiagnosis();
            DS = objDiag.DiagnosisGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                DataTable DT = new DataTable();
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    DT = (DataTable)ViewState["eAuthDiagnosis"];

                    DataRow objrow;
                    objrow = DT.NewRow();
                    objrow["EPD_ID"] = Convert.ToString(DS.Tables[0].Rows[i]["EPD_ID"]);
                    objrow["EPD_DIAG_CODE"] = Convert.ToString(DS.Tables[0].Rows[i]["EPD_DIAG_CODE"]);
                    objrow["EPD_DIAG_NAME"] = Convert.ToString(DS.Tables[0].Rows[i]["EPD_DIAG_NAME"]);
                    objrow["EPD_TYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["EPD_TYPE"]);
                    DT.Rows.Add(objrow);
                }

                ViewState["eAuthDiagnosis"] = DT;
            }


        }


        void BindTempDiagnosis()
        {
            DataTable DT = new DataTable();

            DT = (DataTable)ViewState["eAuthDiagnosis"];
            if (DT.Rows.Count > 0)
            {
                gveAuthDiagnosis.DataSource = DT;
                gveAuthDiagnosis.DataBind();
            }
            else
            {
                gveAuthDiagnosis.DataBind();
            }

        }
        #endregion

        #region AutoExtenderFunctions

        [System.Web.Services.WebMethod]
        public static string[] GetCompany(string prefixText)
        {

            string[] Data;
            string Criteria = " 1=1 ";
            Criteria += " AND HCM_STATUS = 'A' AND HCM_BRANCH_ID = '" + strSessionBranchId + "' ";


            Criteria += " AND HCM_COMP_ID Like '%" + prefixText + "%'";


            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_inccmp_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["HCM_COMP_ID"]) + "~" + Convert.ToString(ds.Tables[0].Rows[i]["HCM_NAME"]);


                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetCompanyName(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 ";

            Criteria += " AND HCM_STATUS = 'A' AND HCM_BRANCH_ID = '" + strSessionBranchId + "' ";


            Criteria += " AND HCM_NAME Like '%" + prefixText + "%'";



            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_inccmp_details(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["HCM_COMP_ID"]) + "~" + Convert.ToString(ds.Tables[0].Rows[i]["HCM_NAME"]);
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }



        [System.Web.Services.WebMethod]
        public static string[] GetClinician(string prefixText)
        {

            string[] Data;
            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND  HSFM_SF_STATUS='Present' ";
            Criteria += " AND HSFM_STAFF_ID Like '%" + prefixText + "%'";
            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_doctor_detailss(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["HSFM_STAFF_ID"]) + "~" + Convert.ToString(ds.Tables[0].Rows[i]["FullName"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }


        [System.Web.Services.WebMethod]
        public static string[] GetClinicianName(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND  HSFM_SF_STATUS='Present' ";
            // Criteria += " AND HSFM_FNAME Like '%" + prefixText + "%'";
            Criteria += " AND HSFM_FNAME + ' ' +isnull(HSFM_MNAME,'') + ' '  + isnull(HSFM_LNAME,'')   like '%" + prefixText + "%' ";

            DataSet ds = new DataSet();
            dboperations dbo = new dboperations();
            ds = dbo.retun_doctor_detailss(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["HSFM_STAFF_ID"]) + "~" + Convert.ToString(ds.Tables[0].Rows[i]["FullName"]);
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }




        [System.Web.Services.WebMethod]
        public static string[] GetICDCode(string prefixText)
        {
            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();

            string[] Data;

            string Criteria = " 1=1 AND DIM_STATUS='A' ";

            Criteria += " AND DIM_ICD_ID  like '" + prefixText + "%' ";
            DS = dbo.DrICDMasterGet(Criteria, "30");

            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["DIM_ICD_ID"]) + "~" + Convert.ToString(DS.Tables[0].Rows[i]["DIM_ICD_DESCRIPTION"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetICDName(string prefixText)
        {
            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();

            string[] Data;

            string Criteria = " 1=1 AND DIM_STATUS='A' ";

            Criteria += " AND DIM_ICD_DESCRIPTION   like '" + prefixText + "%' ";
            DS = dbo.DrICDMasterGet(Criteria, "30");

            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["DIM_ICD_ID"]) + "~" + Convert.ToString(DS.Tables[0].Rows[i]["DIM_ICD_DESCRIPTION"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }


        [System.Web.Services.WebMethod]
        public static string[] GetServiceID(string prefixText)
        {

            string[] Data;
            string Criteria = " 1=1 ";
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();

            Criteria += " AND  HSM_STATUS='A' ";

            Criteria += " AND HSM_SERV_ID   like '" + prefixText + "%' ";
            DS = objCom.HMS_SP_ServiceMasterTopGet(Criteria, "30");

            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["HSM_SERV_ID"]) + "~" + Convert.ToString(DS.Tables[0].Rows[i]["HSM_NAME"]);

                }


                return Data;
            }




            string[] Data1 = { "" };

            return Data1;

        }

        [System.Web.Services.WebMethod]
        public static string[] GetServiceName(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 ";
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();

            Criteria += " AND  HSM_STATUS='A' ";

            Criteria += " AND HSM_NAME   like '" + prefixText + "%' ";
            DS = objCom.HMS_SP_ServiceMasterTopGet(Criteria, "30");

            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["HSM_SERV_ID"]) + "~" + Convert.ToString(DS.Tables[0].Rows[i]["HSM_NAME"]);

                }


                return Data;
            }




            string[] Data1 = { "" };

            return Data1;
        }




        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }
            if (!IsPostBack)
            {
                strSessionBranchId = Convert.ToString(Session["Branch_ID"]);

                txtOrderDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");

                ViewState["HAR_ID"] = Convert.ToString(Request.QueryString["HAR_ID"]);


                ViewState["PT_ID"] = Convert.ToString(Request.QueryString["PatientId"]);
                ViewState["PTName"] = Convert.ToString(Request.QueryString["PTName"]);
                ViewState["EMR_ID"] = Convert.ToString(Request.QueryString["EMR_ID"]);
                ViewState["EPM_INS_CODE"] = Convert.ToString(Request.QueryString["COMP_ID"]);
                ViewState["EPM_DR_CODE"] = Convert.ToString(Request.QueryString["DR_ID"]);

                ViewState["HAR_ID"] = Convert.ToString(Request.QueryString["HAR_ID"]);

                txtPTID.Text = Convert.ToString(ViewState["PT_ID"]);
                txtPTName.Text = Convert.ToString(ViewState["PTName"]);

                BindProvider();

                BuildDiagnosis();


                if (Convert.ToString(ViewState["EMR_ID"]) != "")
                {

                   // BindEMRActivities();


                    BindEMRDignosis();
                    BindTempDiagnosis();
                }

            }
        }

        protected void gveAuthDiagnosis_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblDiagType = (Label)e.Row.FindControl("lblDiagType");
                    Button btnColor = (Button)e.Row.FindControl("btnColor");

                    if (lblDiagType.Text == "Principal")
                    {
                        btnColor.BackColor = System.Drawing.ColorTranslator.FromHtml("#ff0000");

                    }
                    else if (lblDiagType.Text == "Secondary")
                    {
                        btnColor.BackColor = System.Drawing.ColorTranslator.FromHtml("#9ACD32");
                    }
                    else if (lblDiagType.Text == "Admitting")
                    {
                        btnColor.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFF00");
                    }
                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      ePrescription.gveAuthDiagnosis_RowDataBound");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnDiagAdd_Click(object sender, EventArgs e)
        {

            try
            {


                //if (Convert.ToString(ViewState["eAuthSelectIndex"]) == "")
                //{
                //    lblStatus.Text = " Please select one data from above list";
                //    lblStatus.ForeColor = System.Drawing.Color.Red;
                //    goto FunEnd;
                //}

                string strDiagCode = "", strDiagName = ""; ;

                if (txtDiagName.Text.Trim() != "")
                {
                    string strDiagNameDtls = txtDiagName.Text.Trim();
                    string[] arrDiagName = strDiagNameDtls.Split('~');

                    if (arrDiagName.Length > 0)
                    {
                        strDiagCode = arrDiagName[0];

                        if (arrDiagName.Length > 1)
                        {
                            strDiagName = arrDiagName[1];

                        }

                    }


                }

                if (strDiagName == "")
                {
                    lblStatus.Text = " Please enter Diseases details";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                DataTable DT = new DataTable();

                DT = (DataTable)ViewState["eAuthDiagnosis"];

                Boolean isPrincipalAvail = false;

                if (drpDiagType.SelectedValue == "Principal")
                {
                    foreach (DataRow DR in DT.Rows)
                    {
                        if (Convert.ToString(DR["EPD_TYPE"]) == "Principal")
                        {
                            isPrincipalAvail = true;
                        }

                    }

                }

                if (isPrincipalAvail == true)
                {
                    drpDiagType.SelectedValue = "Secondary";
                }


                DataRow objrow;
                objrow = DT.NewRow();
                objrow["EPD_ID"] = "";
                objrow["EPD_DIAG_CODE"] = strDiagCode;
                objrow["EPD_DIAG_NAME"] = strDiagName;
                objrow["EPD_TYPE"] = drpDiagType.SelectedValue;
                DT.Rows.Add(objrow);


                ViewState["eAuthDiagnosis"] = DT;
                BindTempDiagnosis();


                //txtDiagCode.Text = "";
                txtDiagName.Text = "";
                drpDiagType.SelectedValue = "Secondary";
                txtDiagName.Focus();
            FunEnd: ;
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      ePrescription.btneAuthDiagAdd_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void DeleteDiag_Click(object sender, EventArgs e)
        {
            try
            {

                ImageButton btnEdit = new ImageButton();
                btnEdit = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;


                if (ViewState["eAuthDiagnosis"] != null)
                {
                    DataTable DT = new DataTable();
                    DT = (DataTable)ViewState["eAuthDiagnosis"];
                    DT.Rows.RemoveAt(gvScanCard.RowIndex);
                    DT.AcceptChanges();
                    ViewState["eAuthDiagnosis"] = DT;

                }



                BindTempDiagnosis();

            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      ePrescription.DeleteDiag_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }


        protected void drpProvider_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void txtCompanyName_TextChanged(object sender, EventArgs e)
        {
            try
            {
                
                GetCompanyDtls();
               

            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      ePrescription.txtCompanyName_TextChanged");
                TextFileWriting(ex.Message.ToString());

            }
        }

        protected void imgBack_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Home.aspx");
        }

    }
}