﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using Mediplus_BAL;
using System.Collections;
using System.Text;

namespace Mediplus.eAuthorization
{
    public partial class Home : System.Web.UI.Page
    {

        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindeAuthorizationGrid()
        {
            string Criteria = " 1=1 AND  HAR_TYPE	='A' ";
            // Criteria += " AND  TREATMENT_TYPE='" + drpTreatmentType.SelectedValue + "'";
            if (txteAuthHAR_ID.Text.Trim() != "")
            {
                Criteria += " AND HAR_ID  like '%" + txteAuthHAR_ID.Text.Trim() + "%' ";

            }

            //if (txteAuthPTD.Text.Trim() != "")
            //{
            //    Criteria += " AND HAR_PT_ID  = '" + txteAuthPTD.Text.Trim() + "' ";

            //}

            //if (drpeAuthStatus.SelectedIndex != 0)
            //{
            //    Criteria += " AND HAR_STATUS  = " + drpeAuthStatus.Text.Trim();

            //}


            string strStartDate = txteAuthFrmDt.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            if (txteAuthFrmDt.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),HAR_CREATED_DATE,101),101) >= '" + strForStartDate + "'";
            }


            string strTotDate = txteAuthToDt.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }

            if (txteAuthToDt.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),HAR_CREATED_DATE,101),101) <= '" + strForToDate + "'";
            }

            DataSet DS = new DataSet();
            eAuthorizationBAL objeAuth = new eAuthorizationBAL();
            DS = objeAuth.AuthorizationRequestGet(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                gvAuth.DataSource = DS;
                gvAuth.DataBind();
            }
            else
            {
                lblStatus.Text = " No Data";
                lblStatus.ForeColor = System.Drawing.Color.Red;

                gvAuth.DataBind();
            }


        }

        void BindVisitDetailsGrid()
        {
            string Criteria = " 1=1 ";//AND HPV_TYPE='OP' 
            Criteria += " AND EPM_INS_CODE IS NOT NULL AND EPM_INS_CODE <>'' ";

            string strStartDate = txtSrcFromDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            if (txtSrcFromDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),EPM_DATE,101),101) >= '" + strForStartDate + "'";
            }



            string strTotDate = txtSrcToDate.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }


            if (txtSrcToDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),EPM_DATE,101),101) <= '" + strForToDate + "'";
            }

            if (txtSrcFileNo.Text.Trim() != "")
            {
                Criteria += " AND EPM_PT_ID like '%" + txtSrcFileNo.Text.Trim() + "%'";

            }



            DataSet DS = new DataSet();
            EMR_BAL.EMR_PTMasterBAL objCom = new EMR_BAL.EMR_PTMasterBAL();
            DS = objCom.GetEMR_PTMaster(Criteria);


            gvEMRVisits.Visible = false;
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvEMRVisits.Visible = true;
                gvEMRVisits.DataSource = DS;
                gvEMRVisits.DataBind();



            }




        }

        void SetPermission()
        {
            string strPermission = "0";
            if (Convert.ToString(Session["ROLL_IDS"]) == "" || Convert.ToString(Session["ROLL_IDS"]) == null)
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../ErrorPage.aspx");
            }
            string Criteria = " 1=1 AND HRT_SCREEN_ID='EAUTH' ";
            Criteria += " AND  HRT_ROLL_ID='" + Convert.ToString(Session["Roll_Id"]) + "'";

            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.RollTransGet(Criteria);


            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "0" || strPermission == "1")
            {
                 

            }


            if (strPermission == "7")
            {
                 

            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../../ErrorPage.aspx");
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }
            if (!IsPostBack)
            {
                txteAuthFrmDt.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
                txteAuthToDt.Text = System.DateTime.Now.ToString("dd/MM/yyyy");



                txtSrcFromDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");
                txtSrcToDate.Text = System.DateTime.Now.ToString("dd/MM/yyyy");



                if (Convert.ToString(Session["SearchFromDate"]) != null && Convert.ToString(Session["SearchFromDate"]) != "")
                {

                    txteAuthFrmDt.Text = Convert.ToString(Session["SearchFromDate"]);
                }


                if (Convert.ToString(Session["SearchToDate"]) != null && Convert.ToString(Session["SearchToDate"]) != "")
                {

                    txteAuthToDt.Text = Convert.ToString(Session["SearchToDate"]);
                }

                if (Convert.ToString(Session["SearchStatus"]) != null && Convert.ToString(Session["SearchStatus"]) != "")
                {
                    drpApprovalStatus.SelectedIndex = Convert.ToInt32(Session["SearchStatus"]);
                }
                BindeAuthorizationGrid();
            }
        }

        protected void btneAuthFind_Click(object sender, EventArgs e)
        {
            try
            {
                BindeAuthorizationGrid();
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Home.btneAuthFind_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnNewRequest_Click(object sender, EventArgs e)
        {
            Response.Redirect("eAuthorization.aspx?MenuName=Insurance");
        }

        protected void Edit_Click(object sender, EventArgs e)
        {



            TextFileWriting(System.DateTime.Now.ToString() + "Edit_Click Started");

            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;

            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
            ViewState["SelectIndex"] = gvScanCard.RowIndex;


            Label lblPatientId = (Label)gvScanCard.Cells[0].FindControl("lblPatientId");
            Label lblPTName = (Label)gvScanCard.Cells[0].FindControl("lblPTName");
            
            Label lblEMR_ID = (Label)gvScanCard.Cells[0].FindControl("lblEMR_ID");
            Label lblCompCode = (Label)gvScanCard.Cells[0].FindControl("lblCompCode");
            Label lblDrID = (Label)gvScanCard.Cells[0].FindControl("lblDrID");

            Response.Redirect("eAuthorization.aspx?MenuName=Insurance&PatientId=" + lblPatientId.Text + "&PTName=" + lblPTName.Text + "&COMP_ID=" + lblCompCode.Text + "&DR_ID=" + lblDrID.Text.Trim() + "&EMR_ID=" + lblEMR_ID.Text);

        }

        protected void SelectResubmit_Click(object sender, EventArgs e)
        {
            try
            {

                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
 

                Label lblEmrID = (Label)gvScanCard.Cells[0].FindControl("lblEmrID");
                Label lblHarID = (Label)gvScanCard.Cells[0].FindControl("lblHarID");


                Response.Redirect("eAuthorization.aspx?MenuName=Insurance&HAR_ID=" + lblHarID.Text.Trim());
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eAuthorization_Home.SelectResubmit_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void btnVisitSrc_Click(object sender, EventArgs e)
        {
            BindVisitDetailsGrid();
        }

        protected void Timer1_Tick(object sender, EventArgs e)
        {
            BindeAuthorizationGrid();
        }

        protected void DeleteAuth_Click(object sender, EventArgs e)
        {
            try
            {
                string strPermission = hidPermission.Value;
                if (strPermission == "7" || strPermission == "9")
                {
                    ImageButton btnEdit = new ImageButton();
                    btnEdit = (ImageButton)sender;

                    GridViewRow gvScanCard;
                    gvScanCard = (GridViewRow)btnEdit.Parent.Parent;


                    Label lblHarID;

                    lblHarID = (Label)gvScanCard.Cells[0].FindControl("lblHarID");
                    eAuthorizationBAL eAuth = new eAuthorizationBAL();
                    eAuth.HAR_ID = lblHarID.Text;
                    eAuth.DeleteAuthorizationRequest();

                    BindeAuthorizationGrid();

                    lblStatus.Text = " eAuthorization Deleted";
                    lblStatus.ForeColor = System.Drawing.Color.Green;
                  
                   
                FunEnd: ;
                }

            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Home.DeleteAuth_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void gvAuth_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {


                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblApprovalStatus = (Label)e.Row.FindControl("lblApprovalStatus");
                    Image imgApprovalStatus = (Image)e.Row.FindControl("imgApprovalStatus");

                    if (lblApprovalStatus.Text == "PENDING")
                    {
                        imgApprovalStatus.ImageUrl = "../Images/Pending.PNG";
                        imgApprovalStatus.ToolTip = "PENDING";
                    }
                    else if (lblApprovalStatus.Text == "APPROVED")
                    {
                        imgApprovalStatus.ImageUrl = "../Images/Approved.PNG";
                        imgApprovalStatus.ToolTip = "APPROVED";
                    }
                    else if (lblApprovalStatus.Text == "PARTIALY APPROVED")
                    {
                        imgApprovalStatus.ImageUrl = "../Images/PartiallyApproved.PNG";
                        imgApprovalStatus.ToolTip = "PARTIALY APPROVED";
                    }
                    else if (lblApprovalStatus.Text == "REJECTED")
                    {
                        imgApprovalStatus.ImageUrl = "../Images/Rejected.PNG";
                        imgApprovalStatus.ToolTip = "REJECTED";
                    }
                    else if (lblApprovalStatus.Text == "DISPENSE")
                    {
                        imgApprovalStatus.ImageUrl = "../Images/Dispensed.PNG";
                        imgApprovalStatus.ToolTip = "DISPENSE";
                    }
                    else if (lblApprovalStatus.Text == "CANCELLED")
                    {
                        imgApprovalStatus.ImageUrl = "../Images/Canceled.PNG";
                        imgApprovalStatus.ToolTip = "CANCELLED";
                    }
                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Home.gvAuth_RowDataBound");
                TextFileWriting(ex.Message.ToString());
            }
        }
    }
}