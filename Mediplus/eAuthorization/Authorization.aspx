﻿<%@ Page Title="" Language="C#"  MasterPageFile="~/Site2.Master"  AutoEventWireup="true" CodeBehind="Authorization.aspx.cs" Inherits="Mediplus.HMS.eAuthorization.Authorization" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title></title>
    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>


    <link href="../Styles/Style.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />

    <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divwidth
        {
            width: 400px !important;
        }

            #divwidth div
            {
                width: 400px !important;
            }

        #divwidth1
        {
            width: 400px !important;
        }

            #divwidth1 div
            {
                width: 400px !important;
            }

        #divDiagExt
        {
            width: 400px !important;
        }

            #divDiagExt div
            {
                width: 400px !important;
            }


        #divDiag1
        {
            width: 400px !important;
        }

            #divDiag1 div
            {
                width: 400px !important;
            }


        #divComp
        {
            width: 400px !important;
        }

            #divComp div
            {
                width: 400px !important;
            }


        #divDr
        {
            width: 400px !important;
        }

            #divDr div
            {
                width: 400px !important;
            }
    </style>

    
<script language="javascript" type="text/javascript">

    function OnlyNumeric(evt) {
        var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
        if (chCode >= 48 && chCode <= 57 ||
             chCode == 46) {
            return true;
        }
        else

            return false;
    }




    function SaveVal() {
        var label;
        label = document.getElementById('<%=lblStatus.ClientID%>');
        label.style.color = 'red';
        document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";



            }

            function ShowPTMasterVal() {

                var label;
                label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";


            if (document.getElementById('<%=txtFromDate.ClientID%>').value != "") {
                if (isDate(document.getElementById('<%=txtFromDate.ClientID%>').value) == false) {
                    document.getElementById('<%=txtFromDate.ClientID%>').focus()
                    return false;
                }
            }

            if (document.getElementById('<%=txtToDate.ClientID%>').value != "") {
                if (isDate(document.getElementById('<%=txtToDate.ClientID%>').value) == false) {
                    document.getElementById('<%=txtToDate.ClientID%>').focus()
                    return false;
                }
            }
        }



        function SubmissionVal() {
            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";


            var TreatmentType = document.getElementById('<%=drpTreatmentType.ClientID%>').value;

            if (document.getElementById('<%=txtOrderDate.ClientID%>').value == "") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Order Date";
                return false;

            }


            if (document.getElementById('<%=txtOrderDate.ClientID%>').value != "") {
                if (isDate(document.getElementById('<%=txtOrderDate.ClientID%>').value) == false) {
                    document.getElementById('<%=txtOrderDate.ClientID%>').focus()
                    return false;
                }

            }


            if (document.getElementById('<%=txtActivityDate.ClientID%>').value == "") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Activity Date";
                return false;

            }


            if (document.getElementById('<%=txtActivityDate.ClientID%>').value != "") {
                if (isDate(document.getElementById('<%=txtActivityDate.ClientID%>').value) == false) {
                    document.getElementById('<%=txtActivityDate.ClientID%>').focus()
                    return false;
                }

            }



            //  if (TreatmentType == '3' || TreatmentType == '4') {
            //      if (document.getElementById('<%=txtEncounterSt.ClientID%>').value == "") {
            //          document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Encounter Start Date";
            //          return false;

            //      }


            //       if (document.getElementById('<%=txtEncounterSt.ClientID%>').value != "") {
            ///           if (isDate(document.getElementById('<%=txtEncounterSt.ClientID%>').value) == false) {
            //               document.getElementById('<%=txtEncounterSt.ClientID%>').focus()
            //               return false;
            //          }

            //       }

            //      if (document.getElementById('<%=txtEncounterEnd.ClientID%>').value == "") {
            ///          document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Encounter End Date";
            //           return false;

            //      }


            //      if (document.getElementById('<%=txtEncounterEnd.ClientID%>').value != "") {
            //         if (isDate(document.getElementById('<%=txtEncounterEnd.ClientID%>').value) == false) {
            //            document.getElementById('<%=txtEncounterEnd.ClientID%>').focus()
            //              return false;
            //         }

            //      }



            //   }


            // DisableClick();




        }

    var c = 0;
    function DisableClick() {
        var objName = 'Button1';
        document.getElementById('<%=btnGenerate.ClientID%>').disabled = true;
        c = c + 1;
        msg = 'Please Wait...(' + c + ')!';
        document.getElementById('<%=btnGenerate.ClientID%>').value = msg;


                var t = setTimeout('DisableClick()', 1000);




            }



            function ResubVal() {

                var label;
                label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';

            if (document.getElementById('<%=txteAuthOrderDate.ClientID%>').value == "") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Order Date";
                return false;

            }


            if (document.getElementById('<%=txteAuthOrderDate.ClientID%>').value != "") {
                if (isDate(document.getElementById('<%=txteAuthOrderDate.ClientID%>').value) == false) {
                    document.getElementById('<%=txteAuthOrderDate.ClientID%>').focus()
                    return false;
                }

            }

        }



        function ShoweAuthVal() {

            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";

            if (document.getElementById('<%=txteAuthFrmDt.ClientID%>').value != "") {
                if (isDate(document.getElementById('<%=txteAuthFrmDt.ClientID%>').value) == false) {
                  document.getElementById('<%=txteAuthFrmDt.ClientID%>').focus()
                    return false;
                }
            }

            if (document.getElementById('<%=txteAuthToDt.ClientID%>').value != "") {
                if (isDate(document.getElementById('<%=txteAuthToDt.ClientID%>').value) == false) {
                  document.getElementById('<%=txteAuthToDt.ClientID%>').focus()
                    return false;
                }
            }
        }

        function PTDiagVal() {

            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
          label.style.color = 'red';
          document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";

          if (document.getElementById('<%=txtDiagCode.ClientID%>').value == "") {
              document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter the Diagnosis Code";
                return false;

            }

        }
        function PTActivityVal() {

            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";

            if (document.getElementById('<%=txtServCode.ClientID%>').value == "") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter the Activity Code";
                return false;

            }

            if (document.getElementById('<%=txtNet.ClientID%>').value == "") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter the Fees";
                return false;

            }



            if (document.getElementById('<%=txtQty.ClientID%>').value == "") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter the Qty";
                return false;

            }

            if (document.getElementById('<%=txtType.ClientID%>').value == "") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter the Type";
                return false;

            }



        }


        function PTeAuthDiagVal() {

            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";

            if (document.getElementById('<%=txteAuthDiagCode.ClientID%>').value == "") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter the Diagnosis Code";
                return false;

            }

        }

        function PTeAuthActivityVal() {

            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";

            if (document.getElementById('<%=txteAuthServCode.ClientID%>').value == "") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter the Activity Code";
                return false;

            }

            if (document.getElementById('<%=txteAuthNet.ClientID%>').value == "") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter the Fees";
                return false;

            }


            if (document.getElementById('<%=txteAuthQty.ClientID%>').value == "") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter the QTY";
                return false;

            }

            if (document.getElementById('<%=txteAuthType.ClientID%>').value == "") {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter the Type";
                return false;

            }



        }

        function DiagIdSelected() {
            if (document.getElementById('<%=txtDiagCode.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtDiagCode.ClientID%>').value;
                var Data1 = Data.split('~');
                if (Data1.length > 1) {
                    document.getElementById('<%=txtDiagCode.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtDiagName.ClientID%>').value = Data1[1];
                }
            }
            return true;
        }


        function DiagNameSelected() {
            if (document.getElementById('<%=txtDiagName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtDiagName.ClientID%>').value;
                var Data1 = Data.split('~');

                if (Data1.length > 1) {
                    document.getElementById('<%=txtDiagCode.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtDiagName.ClientID%>').value = Data1[1];
                }
            }

            return true;
        }

        function ServIdSelected() {
            if (document.getElementById('<%=txtServCode.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtServCode.ClientID%>').value;
                var Data1 = Data.split('~');
                if (Data1.length > 1) {
                    document.getElementById('<%=txtServCode.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtServName.ClientID%>').value = Data1[1];
                }
            }
            return true;
        }


        function ServNameSelected() {
            if (document.getElementById('<%=txtServName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtServName.ClientID%>').value;
                var Data1 = Data.split('~');

                if (Data1.length > 1) {
                    document.getElementById('<%=txtServCode.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtServName.ClientID%>').value = Data1[1];
                }
            }

            return true;
        }

        function eAuthDiagIdSelected() {
            if (document.getElementById('<%=txteAuthDiagCode.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txteAuthDiagCode.ClientID%>').value;
                var Data1 = Data.split('~');
                if (Data1.length > 1) {
                    document.getElementById('<%=txteAuthDiagCode.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txteAuthDiagName.ClientID%>').value = Data1[1];
                }
            }
            return true;
        }


        function eAuthDiagNameSelected() {
            if (document.getElementById('<%=txteAuthDiagName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txteAuthDiagName.ClientID%>').value;
                var Data1 = Data.split('~');

                if (Data1.length > 1) {
                    document.getElementById('<%=txteAuthDiagCode.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txteAuthDiagName.ClientID%>').value = Data1[1];
                }
            }

            return true;
        }


        function eAuthServIdSelected() {
            if (document.getElementById('<%=txteAuthServCode.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txteAuthServCode.ClientID%>').value;
                var Data1 = Data.split('~');
                if (Data1.length > 1) {
                    document.getElementById('<%=txteAuthServCode.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txteAuthServName.ClientID%>').value = Data1[1];
                }
            }
            return true;
        }


        function eAuthServNameSelected() {
            if (document.getElementById('<%=txteAuthServName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txteAuthServName.ClientID%>').value;
                var Data1 = Data.split('~');

                if (Data1.length > 1) {
                    document.getElementById('<%=txteAuthServCode.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txteAuthServName.ClientID%>').value = Data1[1];
                }
            }

            return true;
        }
        function eAuthApprovalReport(strValue) {
            var win = window.open("../WebReport/eAuthApprovalOP.aspx?HAR_ID=" + strValue, "newwin1", "top=80,left=200,height=850,width=1075,toolbar=no,scrollbars=1,menubar=no");
            win.focus();
        }


</script>



<script language="javascript" type="text/javascript">

    function DiagnosisPopup(CtrlName, CategoryType) {
        var win = window.open("../HomeCare/ServicessLookup.aspx?CtrlName=" + CtrlName + "&CategoryType=" + CategoryType, "newwin1", "top=200,left=270,height=500,width=750,toolbar=no,scrollbars=yes,menubar=no");
        win.focus();
        return false;

    }

    function BindServicess(HaadCode, HaadName, CtrlName) {

        if (CtrlName == 'Diagnosis') {
            document.getElementById("<%=txtDiagCode.ClientID%>").value = HaadCode
            document.getElementById("<%=txtDiagName.ClientID%>").value = HaadName

        }

        if (CtrlName == 'eAuthDiagnosis') {
            document.getElementById("<%=txteAuthDiagCode.ClientID%>").value = HaadCode
                document.getElementById("<%=txteAuthDiagName.ClientID%>").value = HaadName

            }



        }


        function ServicePopup(CtrlName, strValue) {
            var win = window.open("../HMS/Masters/ServicessLookup.aspx?PageName=HomeCareReg&CtrlName=" + CtrlName + "&Value=" + strValue, "newwin", "height=570,width=950,toolbar=no,scrollbars==yes,menubar=no");
            win.focus();

            return true;

        }

        function BindServiceDtls(ServCode, ServName, HaadCode, CtrlName) {

            if (CtrlName == 'Activities') {
                document.getElementById("<%=hidServID.ClientID%>").value = ServCode;
                document.getElementById("<%=txtServCode.ClientID%>").value = HaadCode;
                document.getElementById("<%=txtServName.ClientID%>").value = ServName;
            }

            if (CtrlName == 'eAuthActivities') {
                document.getElementById("<%=hideAuthServID.ClientID%>").value = ServCode;
                document.getElementById("<%=txteAuthServCode.ClientID%>").value = HaadCode;
                document.getElementById("<%=txteAuthServName.ClientID%>").value = ServName;
            }




        }



</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


        <input type="hidden" id="hidPermission" runat="server" value="9" />
        <input type="hidden" id="hidEmrID" runat="server" />
        <input type="hidden" id="hidPTID" runat="server" />

        <input type="hidden" id="hidHAR_ID" runat="server" />

        <input type="hidden" id="hideAuthEmrID" runat="server" />
        <input type="hidden" id="hideAuthClinicianID" runat="server" />

        <input type="hidden" id="hidAttachLength" runat="server" />

        <input type="hidden" id="hidBranchID" runat="server" value="MAIN" />
        <input type="hidden" id="hidUserID" runat="server" value="admin" />

        <table>
            <tr>
                <td class="lblCaption1" style="font-size:15px;">Authorization Approval
               
                </td>
            </tr>
        </table>

        <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="100%">
            <tr>

                <td>
                    <asp:UpdatePanel ID="UpdatePanel0" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
        <br />
        <div style="padding: 5px; width: 900px; overflow: auto; border: thin; border-color: #cccccc; border-style: groove;">
            <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="ajax__tab_yuitabview-theme" Width="100%">
                <asp:TabPanel runat="server" ID="TabPanelGeneral" HeaderText="Generate eAuthorization" Width="100%">
                    <ContentTemplate>

                        <table width="100%">
                            <tr>
                                <td class="lblCaption1" style="height: 30px;">Treatment Type
                                </td>
                                <td>
                                    <asp:DropDownList ID="drpTreatmentType" CssClass="label" runat="server" Width="150px" AutoPostBack="True" OnSelectedIndexChanged="drpTreatmentType_SelectedIndexChanged">
                                        <asp:ListItem Text="Outpatient" Value="1" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="Daycare" Value="5"></asp:ListItem>
                                        <asp:ListItem Text="HomeCare" Value="12"></asp:ListItem>
                                        <asp:ListItem Text="Inpatient" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="Inpatient Emergency" Value="4"></asp:ListItem>
                                        <asp:ListItem Text="Daycare Emergency" Value="6"></asp:ListItem>
                                        <asp:ListItem Text="Outpatient Emergency" Value="2"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>

                                <td colspan="4">
                                    <asp:CheckBox ID="chkEMR" runat="server" CssClass="label" Text="Search With EMR"></asp:CheckBox>


                                </td>
                            </tr>
                            <tr>
                                <td class="lblCaption1" style="height: 30px;">ID
                                </td>
                                <td>
                                    <asp:TextBox ID="txtId" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="100px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                </td>
                                <td class="lblCaption1" style="height: 30px;">File No

                                </td>
                                <td>
                                    <asp:TextBox ID="txtSrcFileNo" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="100px" MaxLength="10"></asp:TextBox>
                                </td>
                                <td class="lblCaption1" style="height: 30px;">Name 
                                </td>

                                <td>
                                    <asp:TextBox ID="txtSrcName" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Width="200px"></asp:TextBox>
                                    <asp:Button ID="btnFind" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 60px;" CssClass="button orange small"
                                        OnClick="btnFind_Click" Text="Refresh" OnClientClick="return ShowPTMasterVal();" />
                                </td>


                            </tr>
                            <tr>
                                <td class="lblCaption1" style="height: 30px;">From Date
                                </td>
                                <td>
                                    <asp:TextBox ID="txtFromDate" runat="server" Width="100px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                    <asp:CalendarExtender ID="TextBox1_CalendarExtender" runat="server"
                                        Enabled="True" TargetControlID="txtFromDate" Format="dd/MM/yyyy">
                                    </asp:CalendarExtender>
                                    <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" Enabled="true" TargetControlID="txtFromDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>

                                </td>

                                <td class="lblCaption1" style="height: 30px;">To
                                </td>
                                <td>
                                    <asp:TextBox ID="txtToDate" runat="server" Width="100px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                    <asp:CalendarExtender ID="txtToDate_CalendarExtender" runat="server"
                                        Enabled="True" TargetControlID="txtToDate" Format="dd/MM/yyyy">
                                    </asp:CalendarExtender>
                                    <asp:MaskedEditExtender ID="MaskedEditExtender2" runat="server" Enabled="true" TargetControlID="txtToDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>

                                </td>
                                <td class="lblCaption1" style="height: 30px;">Doctor
                                </td>
                                <td>
                                    <asp:DropDownList ID="drpSrcDoctor" CssClass="label" BorderColor="#e3f7ef" BorderStyle="Groove" runat="server" Width="250px">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                        <div style="padding-top: 0px; width: 100%; height: 250px; overflow: auto; border-color: #CCCCCC; border-style: solid; border-width: thin;">


                            <asp:GridView ID="gvPTMaster" runat="server" AutoGenerateColumns="False" GridLines="None"
                                EnableModelValidation="True" Width="100%" PageSize="200" OnPageIndexChanging="gvPTMaster_PageIndexChanging">
                                <HeaderStyle CssClass="GridHeader_Blue" Font-Bold="true" />
                                <RowStyle CssClass="GridRow" />

                                <Columns>
                                    <asp:TemplateField HeaderText="EMR ID">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkEmergencyNotes" runat="server" OnClick="Select_Click">
                                                <asp:Label ID="lblTreatPlan" CssClass="GridRow" runat="server" Text='<%# Bind("EPM_TREATMENT_PLAN") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="Label1" CssClass="GridRow" runat="server" Width="70px" Text='<%# Bind("EPM_ID") %>'></asp:Label>

                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Date">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkEpmDate" runat="server" OnClick="Select_Click">
                                                <asp:Label ID="Label29" CssClass="GridRow" runat="server" Width="70px" Text='<%# Bind("EPM_DATEDesc") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Company Name">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkInsName" runat="server" OnClick="Select_Click">
                                                <asp:Label ID="lblInsCode" CssClass="GridRow" runat="server" Text='<%# Bind("HPM_INS_COMP_ID") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblInsName" CssClass="GridRow" runat="server" Text='<%# Bind("HPM_INS_COMP_NAME") %>' Width="250px"></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Payer ID">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton13" runat="server" OnClick="Select_Click">
                                                <asp:Label ID="lblPayerID" CssClass="GridRow" runat="server" Text='<%# Bind("HCM_PAYERID") %>' Width="100px"></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="File No">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkPTID" runat="server" OnClick="Select_Click">
                                                <asp:Label ID="lblEpmID" CssClass="GridRow" runat="server" Text='<%# Bind("EPM_ID") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblEpmDate" CssClass="GridRow" runat="server" Text='<%# Bind("EPM_DATEDesc") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblPTID" CssClass="GridRow" runat="server" Text='<%# Bind("HPM_PT_ID") %>' Width="150px"></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Patient Name">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkPTName" runat="server" OnClick="Select_Click">
                                                <asp:Label ID="lblPTName" CssClass="GridRow" runat="server" Text='<%# Bind("FullName") %>' Width="250px"></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Member ID">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkMemberID" runat="server" OnClick="Select_Click">
                                                <asp:Label ID="lblMemberID" CssClass="GridRow" runat="server" Text='<%# Bind("MemberID") %>' Width="150px"></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Dr Name">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkDrName" runat="server" OnClick="Select_Click">
                                                <asp:Label ID="lblDRCode" CssClass="GridRow" runat="server" Text='<%# Bind("HPM_DR_ID") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblDrName" CssClass="GridRow" runat="server" Text='<%# Bind("HPM_DR_NAME") %>' Width="250px"></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Clinician ID">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkClinicianID" runat="server" OnClick="Select_Click">
                                                <asp:Label ID="lblClinicianID" CssClass="GridRow" runat="server" Text='<%# Bind("HSFM_MOH_NO") %>' Width="200px"></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                </Columns>
                                <PagerStyle CssClass="GridHeader" Font-Bold="true" HorizontalAlign="Center" />


                            </asp:GridView>



                        </div>
                        <br />
                        <table style="width: 100%">
                            <tr>
                                <td class="lblCaption1" style="height: 30px;">PT. Name    :
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPTFullName" CssClass="label" runat="server" Width="200px" BorderWidth="1px" BorderColor="#CCCCCC"></asp:TextBox>


                                </td>
                                <td class="lblCaption1" style="height: 30px;">File No  :
                                </td>
                                <td>
                                    <asp:TextBox ID="txtFileNo" CssClass="label" runat="server" Width="200px" BorderWidth="1px" BorderColor="#CCCCCC"></asp:TextBox>
                                </td>


                            </tr>

                            <tr>
                                <td class="lblCaption1" style="height: 30px;">Company  :
                                </td>
                                <td>
                                    <asp:DropDownList ID="drpCompany" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#CCCCCC" class="label" Width="200px" AutoPostBack="true" OnSelectedIndexChanged="drpCompany_SelectedIndexChanged"></asp:DropDownList>
                                </td>
                                <td class="lblCaption1" style="height: 30px;">Member ID   :
                                </td>
                                <td>
                                    <asp:TextBox ID="txtMemberID" runat="server" Width="200px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                                </td>
                            </tr>
                            <tr>
                                <td class="lblCaption1" style="height: 30px; width: 150px;">Payer ID  :
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPayerID" CssClass="label" runat="server" Width="200px" BorderWidth="1px" BorderColor="#CCCCCC"></asp:TextBox>
                                </td>
                                <td class="lblCaption1" style="height: 30px;">Reciver ID :
                                </td>
                                <td>
                                    <asp:TextBox ID="txtReciverID" runat="server" CssClass="label" Width="200px" BorderWidth="1px" BorderColor="#CCCCCC"></asp:TextBox>

                                </td>
                            </tr>
                            <tr>
                                <td class="lblCaption1" style="height: 30px;">Doctor  :
                                </td>
                                <td>
                                    <asp:DropDownList ID="drpDoctor" CssClass="label" BorderColor="#e3f7ef" BorderStyle="Groove" runat="server" Width="200px" AutoPostBack="true" OnSelectedIndexChanged="drpDoctor_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                                <td class="lblCaption1" style="height: 30px;">Clinician ID  :
                                </td>
                                <td>

                                    <asp:TextBox ID="txtClinicianID" CssClass="label" runat="server" Width="200px" BorderWidth="1px" BorderColor="#CCCCCC"></asp:TextBox>

                                </td>
                                </td>
                    </td>
                            </tr>

                            <tr>
                                <td></td>
                                <td></td>
                                <td class="lblCaption1" style="height: 30px;">Order Date  :
                                </td>
                                <td>

                                    <asp:TextBox ID="txtOrderDate" runat="server" Width="200px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                    <asp:CalendarExtender ID="Calendarextender5" runat="server"
                                        Enabled="True" TargetControlID="txtOrderDate" Format="dd/MM/yyyy">
                                    </asp:CalendarExtender>
                                    <asp:MaskedEditExtender ID="MaskedEditExtender7" runat="server" Enabled="true" TargetControlID="txtOrderDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                            </tr>
                        </table>
                        <table style="width: 70%" id="tblEncounter" runat="server" visible="false">
                            <tr>
                                <td class="lblCaption1" style="height: 30px;">Encounter Start : <span style="color: red;">* </span>
                                    <asp:TextBox ID="txtEncounterSt" runat="server" Width="80px" Height="20px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                    <asp:CalendarExtender ID="Calendarextender3" runat="server"
                                        Enabled="True" TargetControlID="txtEncounterSt" Format="dd/MM/yyyy">
                                    </asp:CalendarExtender>

                                    <asp:DropDownList ID="drpSTHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                                    <asp:DropDownList ID="drpSTMin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>

                                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        <asp:MaskedEditExtender ID="MaskedEditExtender5" runat="server" Enabled="true" TargetControlID="txtEncounterSt" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                                    End : <span style="color: red;">* </span>
                                    <asp:TextBox ID="txtEncounterEnd" runat="server" Width="80px" Height="20px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                    <asp:CalendarExtender ID="Calendarextender4" runat="server"
                                        Enabled="True" TargetControlID="txtEncounterEnd" Format="dd/MM/yyyy">
                                    </asp:CalendarExtender>
                                    <asp:MaskedEditExtender ID="MaskedEditExtender6" runat="server" Enabled="true" TargetControlID="txtEncounterEnd" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                                    <asp:DropDownList ID="drpFIHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                                    :
                        <asp:DropDownList ID="drpFIMin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>


                                </td>
                            </tr>

                        </table>
                        <br />
                        <div style="padding: 5px; width: 99%; overflow: auto; border: thin; border-color: #cccccc; border-style: groove;">
                            <asp:TabContainer ID="TabContainer2" runat="server" ActiveTabIndex="0" CssClass="ajax__tab_yuitabview-theme" Width="100%">
                                <asp:TabPanel runat="server" ID="TabPanelDiagnosis" HeaderText="Diagnosis" Width="100%">
                                    <ContentTemplate>
                                        <table style="width: 100%">
                                            <tr>
                                                <td colspan="2" style="width: 100%; vertical-align: text-top;">

                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td class="lblCaption1" style="width: 120PX;">Diagnosis  <span style="color: red;">* </span>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:TextBox ID="txtDiagCode" runat="server" Width="100px" CssClass="label" BorderWidth="1px" BorderColor="#CCCCCC" onblur="return DiagIdSelected()"></asp:TextBox>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtDiagName" runat="server" Width="450px" CssClass="label" BorderWidth="1px" BorderColor="#CCCCCC" onblur="return DiagNameSelected()"></asp:TextBox>

                                                                <div id="divDiagExt" style="visibility: hidden;"></div>
                                                                <asp:AutoCompleteExtender ID="AutoCompleteExtender7" runat="server" TargetControlID="txtDiagCode" MinimumPrefixLength="1" ServiceMethod="GetDiagnosisID"
                                                                    CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divDiagExt" DelimiterCharacters="" Enabled="True" ServicePath="">
                                                                </asp:AutoCompleteExtender>
                                                                <asp:AutoCompleteExtender ID="AutoCompleteExtender8" runat="server" TargetControlID="txtDiagName" MinimumPrefixLength="1" ServiceMethod="GetDiagnosisName"
                                                                    CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divDiagExt" DelimiterCharacters="" Enabled="True" ServicePath="">
                                                                </asp:AutoCompleteExtender>
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="btnDiagAdd" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 60px;" CssClass="button orange small"
                                                                    OnClick="btnDiagAdd_Click" Text="Add" OnClientClick="return PTDiagVal();" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <asp:GridView ID="gvDiagnosis" runat="server" AutoGenerateColumns="False"
                                                        EnableModelValidation="True" Width="100%" PageSize="200">
                                                        <HeaderStyle CssClass="GridHeader" Font-Bold="true" />
                                                        <RowStyle CssClass="GridRow" />
                                                        <Columns>

                                                            <asp:TemplateField HeaderText="Code">
                                                                <ItemTemplate>

                                                                    <asp:Label ID="lblDiagCode" CssClass="GridRow" runat="server" Text='<%# Bind("EPD_DIAG_CODE") %>'></asp:Label>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Name">
                                                                <ItemTemplate>

                                                                    <asp:Label ID="lblDiagName" CssClass="GridRow" runat="server" Text='<%# Bind("EPD_DIAG_NAME") %>'></asp:Label>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Delete">
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="DeleteDiag" runat="server" ToolTip="Delete" ImageUrl="~/Images/icon_delete.png"
                                                                        OnClick="DeleteDiag_Click" />&nbsp;&nbsp;
                                                
                                                                </ItemTemplate>
                                                                <HeaderStyle Width="50px" />
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <PagerStyle CssClass="GridHeader" Font-Bold="true" HorizontalAlign="Center" />

                                                    </asp:GridView>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" style="height: 10px;"></td>
                                            </tr>

                                        </table>
                                    </ContentTemplate>
                                </asp:TabPanel>
                                <asp:TabPanel runat="server" ID="TabPanel3" HeaderText="Activities " Width="100%">
                                    <ContentTemplate>
                                        <input type="hidden" id="hidServID" runat="server" />
                                        <table style="width: 100%">
                                            <tr>
                                                <td colspan="2" style="width: 100%; vertical-align: text-top;">
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td class="lblCaption1" style="width: 120px">Activities  <span style="color: red;">* </span>

                                                            </td>
                                                            <td style="width: 480px"></td>
                                                            <td class="lblCaption1">Fees<span style="color: red;">* </span>
                                                            </td>
                                                            <td class="lblCaption1">Qty: <span style="color: red;">* </span>
                                                            </td>
                                                            <td class="lblCaption1">Type <span style="color: red;">* </span>
                                                            </td>


                                                        </tr>
                                                        <tr>
                                                            <td style="width: 120px">
                                                                <input type="hidden" id="Hidden1" runat="server" />
                                                                <asp:TextBox ID="txtServCode" runat="server" Width="100px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" AutoPostBack="true" OnTextChanged="txtServCode_TextChanged" onblur="return ServIdSelected()"></asp:TextBox>
                                                            </td>
                                                            <td style="width: 480px">
                                                                <asp:TextBox ID="txtServName" runat="server" Width="455px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" onblur="return ServNameSelected()"></asp:TextBox>

                                                                <div id="divwidth" style="visibility: hidden;"></div>
                                                                <asp:AutoCompleteExtender ID="AutoCompleteExtender5" runat="Server" TargetControlID="txtServCode" MinimumPrefixLength="1" ServiceMethod="GetServiceID"
                                                                    CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidth">
                                                                </asp:AutoCompleteExtender>
                                                                <asp:AutoCompleteExtender ID="AutoCompleteExtender6" runat="Server" TargetControlID="txtServName" MinimumPrefixLength="1" ServiceMethod="GetServiceName"
                                                                    CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidth">
                                                                </asp:AutoCompleteExtender>

                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtNet" runat="server" Width="100px" Height="19px" CssClass="label" MaxLength="10" BorderWidth="1px" BorderColor="#cccccc" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtQty" runat="server" Width="30px" Height="19px" CssClass="label" MaxLength="4" Text="1" BorderWidth="1px" BorderColor="#cccccc" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtType" runat="server" Width="30px" Height="19px" CssClass="label" MaxLength="4" BorderWidth="1px" BorderColor="#cccccc" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                                                            </td>

                                                            <td>
                                                                <asp:Button ID="btnServiceAdd" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 60px;" CssClass="button orange small"
                                                                    OnClick="btnServiceAdd_Click" Text="Add" OnClientClick="return PTActivityVal();" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="lblCaption1" style="height: 30px; width: 120px;">Activity Date <span style="color: red;">* </span>:
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtActivityDate" runat="server" Width="70px" Height="20px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                                <asp:CalendarExtender ID="CalendarExtender7" runat="server"
                                                                    Enabled="True" TargetControlID="txtActivityDate" Format="dd/MM/yyyy">
                                                                </asp:CalendarExtender>
                                                                <asp:MaskedEditExtender ID="MaskedEditExtender9" runat="server" Enabled="true" TargetControlID="txtActivityDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>

                                                                <asp:DropDownList ID="drpActivityHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                                                                <asp:DropDownList ID="drpActivityMin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>

                                                                &nbsp;&nbsp;&nbsp;
                                     
                                     <asp:Label ID="lblToothNo" class="lblCaption1" runat="server" Text="OBS Value :"></asp:Label>
                                                                <asp:TextBox ID="txtToothNo" runat="server" Width="200px" Height="19px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc"></asp:TextBox>


                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <br />

                                                    <asp:GridView ID="gvActivity" runat="server" AutoGenerateColumns="False"
                                                        EnableModelValidation="True" Width="100%" PageSize="200">
                                                        <HeaderStyle CssClass="GridHeader" Font-Bold="true" />
                                                        <RowStyle CssClass="GridRow" />
                                                        <Columns>

                                                            <asp:TemplateField HeaderText="Code">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkCode" runat="server" OnClick="SelectActivity_Click">
                                                                        <asp:Label ID="lblServID" CssClass="GridRow" runat="server" Text='<%# Bind("SERV_ID") %>' Visible="false"></asp:Label>
                                                                        <asp:Label ID="lblServCode" CssClass="GridRow" runat="server" Text='<%# Bind("SERV_CODE") %>'></asp:Label>
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Name">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="LinkButton3" runat="server" OnClick="SelectActivity_Click">
                                                                        <asp:Label ID="lblServName" CssClass="GridRow" runat="server" Text='<%# Bind("SERV_NAME") %>'></asp:Label>
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="QTY">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="LinkButton4" runat="server" OnClick="SelectActivity_Click">
                                                                        <asp:Label ID="lblQty" CssClass="GridRow" runat="server" Text='<%# Eval("QTY", "{0:n2}") %>'></asp:Label>
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="OBS Value">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="LinkButton12" runat="server" OnClick="SelectActivity_Click">
                                                                        <asp:Label ID="lblObsValue" CssClass="GridRow" runat="server" Text='<%# Eval("DURATION", "{0:n2}") %>'></asp:Label>
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Net">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnlNet" runat="server" OnClick="SelectActivity_Click">
                                                                        <asp:Label ID="lblNet" CssClass="GridRow" runat="server" Text='<%# Eval("HAA_NET", "{0:n2}") %>'></asp:Label>
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Activity Start">
                                                                <ItemTemplate>

                                                                    <asp:Label ID="lblActivityStart" CssClass="GridRow" runat="server" Text='<%# Bind("HAA_ACTIVITY_START") %>'></asp:Label>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Type">
                                                                <ItemTemplate>

                                                                    <asp:Label ID="lblActivityType" CssClass="GridRow" runat="server" Text='<%# Bind("HAA_ACTIVITY_TYPE") %>'></asp:Label>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Delete">
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="DeleteService" runat="server" ToolTip="Delete" ImageUrl="~/Images/icon_delete.png"
                                                                        OnClick="DeleteService_Click" />&nbsp;&nbsp;
                                                
                                                                </ItemTemplate>
                                                                <HeaderStyle Width="50px" />
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <PagerStyle CssClass="GridHeader" Font-Bold="true" HorizontalAlign="Center" />

                                                    </asp:GridView>


                                                </td>
                                            </tr>

                                            <tr>
                                                <td colspan="2" style="height: 10px;"></td>
                                            </tr>
                                            <tr>
                                                <td style="width: 50%; vertical-align: text-top;">

                                                    <span class="lblCaption1">Chief Complaints </span>
                                                    <br />
                                                    <asp:TextBox ID="txtCC" runat="server" Width="90%" CssClass="label" BorderWidth="1px" Height="50px" TextMode="MultiLine" BorderColor="#cccccc"></asp:TextBox>



                                                </td>
                                                <td style="width: 50%; vertical-align: text-top;">

                                                    <span class="lblCaption1">Management Plans </span>
                                                    <br />
                                                    <asp:TextBox ID="txtManagementPlan" runat="server" Width="90%" CssClass="label" BorderWidth="1px" Height="50px" TextMode="MultiLine" BorderColor="#cccccc"></asp:TextBox>



                                                </td>
                                            </tr>

                                        </table>

                                    </ContentTemplate>
                                </asp:TabPanel>
                                <asp:TabPanel runat="server" ID="TabPanel4" HeaderText="Attachments" Width="100%">
                                    <ContentTemplate>

                                        <table width="100%">


                                            <tr>
                                                <td colspan="2" style="width: 100%; vertical-align: text-top;">

                                                    <span class="lblCaption1">Attachments  </span><span style="color: red;">* </span>
                                                    <br />
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td>
                                                                <asp:FileUpload ID="filAttachment" runat="server" Width="300px" CssClass="ButtonStyle" accept="application/pdf" />
                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                               <asp:Button ID="btnAttaAdd" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 60px;" CssClass="button orange small"
                                                                   OnClick="btnAttaAdd_Click" Text="Add" />
                                                            </td>
                                                        </tr>
                                                    </table>

                                                    <asp:GridView ID="gvAttachments" runat="server" AutoGenerateColumns="False"
                                                        EnableModelValidation="True" Width="100%" PageSize="200">
                                                        <HeaderStyle CssClass="GridHeader" Font-Bold="true" />
                                                        <RowStyle CssClass="GridRow" />
                                                        <Columns>

                                                            <asp:TemplateField HeaderText="File Name">
                                                                <ItemTemplate>

                                                                    <asp:Label ID="lblFileName" CssClass="GridRow" runat="server" Text='<%# Bind("HAAT_FILE_NAME") %>'></asp:Label>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Actual File Name">
                                                                <ItemTemplate>

                                                                    <asp:Label ID="lblActualFileName" CssClass="GridRow" runat="server" Text='<%# Bind("HAAT_FILE_NAME_ACTUAL") %>'></asp:Label>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Delete">
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="DeleteAtta" runat="server" ToolTip="Delete" ImageUrl="~/Images/icon_delete.png"
                                                                        OnClick="DeleteAtta_Click" />&nbsp;&nbsp;
                                                
                                                                </ItemTemplate>
                                                                <HeaderStyle Width="50px" />
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <PagerStyle CssClass="GridHeader" Font-Bold="true" HorizontalAlign="Center" />

                                                    </asp:GridView>



                                                </td>
                                            </tr>

                                        </table>
                                    </ContentTemplate>
                                </asp:TabPanel>

                            </asp:TabContainer>
                        </div>
                        <br />
                        <table width="95%">

                            <tr>

                                <td>
                                    <asp:Button ID="btnGenerate" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 100px;" CssClass="button orange small" OnClick="btnGenerate_Click" Text="Submission" OnClientClick="return SubmissionVal();" />
                                    <asp:Button ID="btnDownLoad" runat="server" Style="padding-left: 5px; padding-right: 5px;" CssClass="button orange small" Width="70px" OnClick="btnDownLoad_Click" Text="Download" />
                                    <asp:Button ID="btnClear" runat="server" Style="padding-left: 5px; padding-right: 5px;" CssClass="button orange small" Width="70px" OnClick="btnClear_Click" Text="Clear" />

                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:TabPanel>
                <asp:TabPanel runat="server" ID="TabPanel1" HeaderText=" eAuthorization Files " Width="100%">
                    <ContentTemplate>
                        <br />

                        <table width="100%">
                            <tr>
                                <td class="lblCaption1" style="height: 30px;">ID
                                </td>
                                <td>
                                    <asp:TextBox ID="txteAuthHAR_ID" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="200px"></asp:TextBox>
                                </td>
                                <td class="lblCaption1" style="height: 30px;">Patient No

                                </td>
                                <td>
                                    <asp:TextBox ID="txteAuthPTD" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="100px" MaxLength="10"></asp:TextBox>



                                </td>
                                <td class="lblCaption1" style="height: 30px;">Status

                                </td>
                                <td>
                                    <asp:DropDownList ID="drpeAuthStatus" CssClass="label" runat="server" Width="150px">
                                        <asp:ListItem Text="All" Value="" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="Ready For Upload" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="Uploaded" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Downloaded" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="Uploaded(Cancel File)" Value="7"></asp:ListItem>
                                        <asp:ListItem Text="Upload Failed" Value="-1"></asp:ListItem>

                                    </asp:DropDownList>
                                    <asp:Button ID="btneAuthFind" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 60px;" CssClass="button orange small"
                                        OnClick="btneAuthFind_Click" Text="Refresh" OnClientClick="return ShoweAuthVal();" />
                                    <asp:Button ID="btnGetkXMLFile" Visible="false" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 60px;" CssClass="button orange small"
                                        OnClick="btnGetkXMLFile_Click" Text="Get XML" OnClientClick="return ShoweAuthVal();" />

                                </td>

                            </tr>
                            <tr>
                                <td class="lblCaption1" style="height: 30px;">From Date
                                </td>
                                <td>
                                    <asp:TextBox ID="txteAuthFrmDt" runat="server" Width="100px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                    <asp:CalendarExtender ID="Calendarextender1" runat="server"
                                        Enabled="True" TargetControlID="txteAuthFrmDt" Format="dd/MM/yyyy">
                                    </asp:CalendarExtender>
                                    <asp:MaskedEditExtender ID="MaskedEditExtender3" runat="server" Enabled="true" TargetControlID="txteAuthFrmDt" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>

                                </td>

                                <td class="lblCaption1" style="height: 30px;">To
                                </td>
                                <td>
                                    <asp:TextBox ID="txteAuthToDt" runat="server" Width="100px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                    <asp:CalendarExtender ID="Calendarextender2" runat="server"
                                        Enabled="True" TargetControlID="txteAuthToDt" Format="dd/MM/yyyy">
                                    </asp:CalendarExtender>
                                    <asp:MaskedEditExtender ID="MaskedEditExtender4" runat="server" Enabled="true" TargetControlID="txteAuthToDt" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>

                                </td>
                                <td class="lblCaption1" style="height: 30px;">Doctor
                                </td>
                                <td>
                                    <asp:DropDownList ID="drpeAuthDoctor" CssClass="label" BorderColor="#e3f7ef" BorderStyle="Groove" runat="server" Width="250px">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                        <div style="padding-top: 0px; width: 100%; height: 250px; overflow: auto; border-color: #CCCCCC; border-style: solid; border-width: thin;">
                            <table width="100%">
                                <tr>
                                    <td>

                                        <asp:GridView ID="gvAuth" runat="server" AutoGenerateColumns="False"
                                            EnableModelValidation="True" Width="100%" PageSize="200" GridLines="None">
                                            <HeaderStyle CssClass="GridHeader_Blue" Font-Bold="true" />
                                            <RowStyle CssClass="GridRow" />


                                            <Columns>
                                                <asp:TemplateField HeaderText="ID">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkHarID" runat="server" OnClick="SelectResubmit_Click">
                                                            <asp:Label ID="lblResubComment" CssClass="GridRow" runat="server" Text='<%# Bind("HAR_RESUB_COMMENTS") %>' Visible="false"></asp:Label>
                                                            <asp:Label ID="lblResubType" CssClass="GridRow" runat="server" Text='<%# Bind("HAR_RESUB_TYPE") %>' Visible="false"></asp:Label>
                                                            <asp:Label ID="lblFleName" CssClass="GridRow" runat="server" Text='<%# Bind("HAR_FILE_NAME") %>' Visible="false"></asp:Label>
                                                            <asp:Label ID="Label2" CssClass="GridRow" runat="server" Text='<%# Bind("HAR_PT_ID") %>' Visible="false"></asp:Label>
                                                            <asp:Label ID="Label3" CssClass="GridRow" runat="server" Text='<%# Bind("HAR_STATUS") %>' Visible="false"></asp:Label>
                                                            <asp:Label ID="lblHarID" CssClass="GridRow" runat="server" Text='<%# Bind("HAR_ID") %>' Visible="false"></asp:Label>
                                                            <asp:Label ID="lblEmrID" CssClass="GridRow" runat="server" Text='<%# Bind("HAR_EMR_ID") %>' Visible="false"></asp:Label>
                                                            <asp:Label ID="lblTreatType" CssClass="GridRow" runat="server" Text='<%# Bind("HAR_ENCOUNTER_TYPE") %>' Visible="false"></asp:Label>
                                                            <asp:Label ID="Label10" CssClass="GridRow" Width="170px" runat="server" Text='<%# Bind("HAR_TRANS_ID") %>'></asp:Label>
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Trans. Date">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lbkTranDate" runat="server" OnClick="SelectResubmit_Click">
                                                            <asp:Label ID="lblTransDate" CssClass="GridRow" Width="75px" runat="server" Text='<%# Bind("HAR_TRANS_DATEDesc") %>'></asp:Label>
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Order Date">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="LinkButton16" runat="server" OnClick="SelectResubmit_Click">
                                                            <asp:Label ID="lblOrderDate" CssClass="GridRow" Width="75px" runat="server" Text='<%# Bind("HAR_ORDER_DATEDesc") %>'></asp:Label>
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Company Name">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="LinkButton2" runat="server" OnClick="SelectResubmit_Click">
                                                            <asp:Label ID="lbleAuthInsCode" CssClass="GridRow" runat="server" Text='<%# Bind("HAR_INS_CODE") %>' Visible="false"></asp:Label>
                                                            <asp:Label ID="Label5" CssClass="GridRow" Width="200px" runat="server" Text='<%# Bind("HAR_INS_NAME") %>'></asp:Label>
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Payer ID">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkPayerID" runat="server" OnClick="SelectResubmit_Click">
                                                            <asp:Label ID="Label6" CssClass="GridRow" Width="100px" runat="server" Text='<%# Bind("HAR_PAYER_ID") %>'></asp:Label>
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Patient No">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="LinkButton1" runat="server" OnClick="SelectResubmit_Click">
                                                            <asp:Label ID="Label7" CssClass="GridRow" Width="100px" runat="server" Text='<%# Bind("HAR_PT_ID") %>'></asp:Label>
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Member ID">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="LinkButton5" runat="server" OnClick="SelectResubmit_Click">
                                                            <asp:Label ID="Label8" CssClass="GridRow" Width="100px" runat="server" Text='<%# Bind("HAR_MEMBER_ID") %>'></asp:Label>
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Dr Name">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="LinkButton14" runat="server" OnClick="SelectResubmit_Click">
                                                            <asp:Label ID="Label9" CssClass="GridRow" runat="server" Text='<%# Bind("HAR_DR_CODE") %>' Visible="false"></asp:Label>
                                                            <asp:Label ID="Label11" CssClass="GridRow" Width="120px" runat="server" Text='<%# Bind("HAR_DR_NAME") %>'></asp:Label>
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Clinician ID">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="LinkButton10" runat="server" OnClick="SelectResubmit_Click">
                                                            <asp:Label ID="Label14" CssClass="GridRow" Width="100px" runat="server" Text='<%# Bind("HAR_CLINICIAN_ID") %>'></asp:Label>
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Status">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkStatusDesc" runat="server" OnClick="SelectResubmit_Click">
                                                            <asp:Label ID="lblEndDate" CssClass="GridRow" Width="100px" runat="server" Text='<%# Bind("HAR_STATUSDesc") %>'></asp:Label>
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Authorization No">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnIDPayerk" runat="server" OnClick="SelectResubmit_Click">
                                                            <asp:Label ID="lblApprovalNo" CssClass="GridRow" Width="100px" runat="server" Text='<%# Bind("HAR_ID_PAYER") %>'></asp:Label>
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Approval Status">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkAppStatus" runat="server" OnClick="SelectResubmit_Click">
                                                            <asp:Label ID="Label12" CssClass="GridRow" Width="100px" runat="server" Text='<%# Bind("ApprovalStatus") %>'></asp:Label>
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Start Date">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="LinkButton9" runat="server" OnClick="SelectResubmit_Click">
                                                            <asp:Label ID="Label13" CssClass="GridRow" Width="75px" runat="server" Text='<%# Bind("HAR_START_DATEDesc") %>'></asp:Label>
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="End Date">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="LinkButton11" runat="server" OnClick="SelectResubmit_Click">
                                                            <asp:Label ID="Label15" CssClass="GridRow" Width="75px" runat="server" Text='<%# Bind("HAR_END_DATEDesc") %>'></asp:Label>
                                                        </asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Delete" Visible="true">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="ImageButton1" runat="server" ToolTip="Delete" ImageUrl="~/Images/icon_delete.png"
                                                            OnClick="DeleteAuth_Click" OnClientClick="return window.confirm('Do you want to Delete eAuthorization?')" />&nbsp;&nbsp;
                                                
                                                    </ItemTemplate>
                                                    <HeaderStyle Width="50px" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerStyle CssClass="GridHeader" Font-Bold="true" HorizontalAlign="Center" />

                                        </asp:GridView>

                                    </td>
                                </tr>

                            </table>
                        </div>
                        <br />
                        <table width="100%">
                            <tr>
                                <td class="lblCaption1" style="height: 30px; width: 120px;">Treatment Type<span style="color: red;">* </span>
                                </td>
                                <td style="height: 30px; width: 160px;">
                                    <asp:DropDownList ID="drpeAuthTreatmentType" CssClass="label" runat="server" Width="150px" AutoPostBack="True" OnSelectedIndexChanged="drpTreatmentType_SelectedIndexChanged">
                                        <asp:ListItem Text="Outpatient" Value="1" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="Daycare" Value="5"></asp:ListItem>
                                        <asp:ListItem Text="HomeCare" Value="12"></asp:ListItem>
                                        <asp:ListItem Text="Inpatient" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="Inpatient Emergency" Value="4"></asp:ListItem>
                                        <asp:ListItem Text="Daycare Emergency" Value="6"></asp:ListItem>
                                        <asp:ListItem Text="Outpatient Emergency" Value="2"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td class="lblCaption1" style="height: 30px; width: 120px;">Order Date<span style="color: red;">* </span>
                                </td>
                                <td>
                                    <asp:TextBox ID="txteAuthOrderDate" runat="server" Width="100px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                    <asp:CalendarExtender ID="Calendarextender6" runat="server"
                                        Enabled="True" TargetControlID="txteAuthOrderDate" Format="dd/MM/yyyy">
                                    </asp:CalendarExtender>
                                    <asp:MaskedEditExtender ID="MaskedEditExtender8" runat="server" Enabled="true" TargetControlID="txteAuthOrderDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>

                                </td>



                                <tr>
                                    <td class="lblCaption1">Resubmission Type  <span style="color: red;">* </span>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="drpResubType" CssClass="label" runat="server" Width="150px">
                                            <asp:ListItem Text="--- Select ---" Value="" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="Correction" Value="correction"></asp:ListItem>
                                            <asp:ListItem Text="Internal Complaint" Value="internal complaint"></asp:ListItem>

                                        </asp:DropDownList>
                                    </td>
                                    <td class="lblCaption1">Resubmission Comment   <span style="color: red;">* </span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtResubComment" runat="server" Width="100%" CssClass="label" BorderWidth="1px" Height="50px" TextMode="MultiLine" BorderColor="#cccccc"></asp:TextBox>

                                    </td>
                                </tr>
                        </table>
                        <br />
                        <div style="padding: 5px; width: 99%; overflow: auto; border: thin; border-color: #cccccc; border-style: groove;">
                            <asp:TabContainer ID="TabContainer3" runat="server" ActiveTabIndex="0" CssClass="ajax__tab_yuitabview-theme" Width="100%">
                                <asp:TabPanel runat="server" ID="TabeAuthPanelDiagnosis" HeaderText="Diagnosis" Width="100%">
                                    <ContentTemplate>
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 100%; vertical-align: text-top;" colspan="2">
                                                    <div style="padding: 5px; width: 99%; border: thin; border-color: #cccccc; border-style: solid; border-radius: 4px;">

                                                        <table width="100%" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td class="lblCaption1" style="width: 120PX;">Diagnosis  <span style="color: red;">* </span>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:TextBox ID="txteAuthDiagCode" runat="server" Width="100px" CssClass="label" BorderWidth="1px" BorderColor="#CCCCCC" onblur="return eAuthDiagIdSelected()"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txteAuthDiagName" runat="server" Width="450px" CssClass="label" BorderWidth="1px" BorderColor="#CCCCCC" onblur="return eAuthDiagNameSelected()"></asp:TextBox>

                                                                    <div id="divDiag1" style="visibility: hidden;"></div>
                                                                    <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" TargetControlID="txteAuthDiagCode" MinimumPrefixLength="1" ServiceMethod="GetDiagnosisID"
                                                                        CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divDiag1" DelimiterCharacters="" Enabled="True" ServicePath="">
                                                                    </asp:AutoCompleteExtender>
                                                                    <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="server" TargetControlID="txteAuthDiagName" MinimumPrefixLength="1" ServiceMethod="GetDiagnosisName"
                                                                        CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divDiag1" DelimiterCharacters="" Enabled="True" ServicePath="">
                                                                    </asp:AutoCompleteExtender>
                                                                </td>
                                                                <td>
                                                                    <asp:Button ID="btneAuthDiagAdd" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 60px;" CssClass="button orange small"
                                                                        OnClick="btneAuthDiagAdd_Click" Text="Add" OnClientClick="return PTeAuthDiagVal();" />
                                                                </td>
                                                            </tr>
                                                        </table>




                                                        <asp:GridView ID="gveAuthDiagnosis" runat="server" AutoGenerateColumns="False"
                                                            EnableModelValidation="True" Width="100%" PageSize="200">
                                                            <HeaderStyle CssClass="GridHeader" Font-Bold="true" />
                                                            <RowStyle CssClass="GridRow" />
                                                            <Columns>

                                                                <asp:TemplateField HeaderText="Code">
                                                                    <ItemTemplate>

                                                                        <asp:Label ID="Label16" CssClass="GridRow" runat="server" Text='<%# Bind("EPD_DIAG_CODE") %>'></asp:Label>

                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Name">
                                                                    <ItemTemplate>

                                                                        <asp:Label ID="Label17" CssClass="GridRow" runat="server" Text='<%# Bind("EPD_DIAG_NAME") %>'></asp:Label>

                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Delete">
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="DeleteeAuthDiag" runat="server" ToolTip="Delete" ImageUrl="~/Images/icon_delete.png"
                                                                            OnClick="DeleteeAuthDiag_Click" />&nbsp;&nbsp;
                                                
                                                                    </ItemTemplate>
                                                                    <HeaderStyle Width="50px" />
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <PagerStyle CssClass="GridHeader" Font-Bold="true" HorizontalAlign="Center" />

                                                        </asp:GridView>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </asp:TabPanel>
                                <asp:TabPanel runat="server" ID="TabPanel6" HeaderText="Activities" Width="100%">
                                    <ContentTemplate>
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 100%; vertical-align: text-top;" colspan="2">

                                                    <table width="100%">
                                                        <tr>
                                                            <td class="lblCaption1">Activities<span style="color: red;">* </span>

                                                            </td>
                                                            <td></td>
                                                            <td class="lblCaption1">Fees<span style="color: red;">* </span>
                                                            </td>

                                                            <td class="lblCaption1">Qty <span style="color: red;">* </span>
                                                            </td>
                                                            <td class="lblCaption1">Type <span style="color: red;">* </span>
                                                            </td>


                                                            <td></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 120px">
                                                                <input type="hidden" id="hideAuthServID" runat="server" />
                                                                <asp:TextBox ID="txteAuthServCode" runat="server" Width="100px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" AutoPostBack="true" OnTextChanged="txteAuthServCode_TextChanged" onblur="return eAuthServIdSelected()"></asp:TextBox>
                                                            </td>
                                                            <td style="width: 480px">
                                                                <asp:TextBox ID="txteAuthServName" runat="server" Width="455px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" onblur="return eAuthServNameSelected()"></asp:TextBox>

                                                                <div id="divwidth1" style="visibility: hidden;"></div>
                                                                <asp:AutoCompleteExtender ID="AutoCompleteExtender3" runat="Server" TargetControlID="txteAuthServCode" MinimumPrefixLength="1" ServiceMethod="GetServiceID"
                                                                    CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidth1">
                                                                </asp:AutoCompleteExtender>
                                                                <asp:AutoCompleteExtender ID="AutoCompleteExtender9" runat="Server" TargetControlID="txteAuthServName" MinimumPrefixLength="1" ServiceMethod="GetServiceName"
                                                                    CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidth1">
                                                                </asp:AutoCompleteExtender>

                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txteAuthNet" runat="server" Width="50px" Height="19px" CssClass="label" MaxLength="10" BorderWidth="1px" BorderColor="#cccccc" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txteAuthQty" runat="server" Width="30px" Height="19px" Text="1" CssClass="label" MaxLength="4" BorderWidth="1px" BorderColor="#cccccc" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                                                            </td>

                                                            <td>
                                                                <asp:TextBox ID="txteAuthType" runat="server" Width="30px" Height="19px" Text="1" CssClass="label" MaxLength="4" BorderWidth="1px" BorderColor="#cccccc" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                                                            </td>


                                                            <td>
                                                                <asp:Button ID="btneAuthActivityAdd" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 60px;" CssClass="button orange small"
                                                                    OnClick="btneAuthActivityAdd_Click" Text="Add" OnClientClick="return PTeAuthActivityVal();" />
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td class="lblCaption1" style="height: 30px;">Activity Date :<span style="color: red;">* </span>
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txteAuthActivityDate" runat="server" Width="70px" Height="20px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                                <asp:CalendarExtender ID="CalendarExtender8" runat="server"
                                                                    Enabled="True" TargetControlID="txteAuthActivityDate" Format="dd/MM/yyyy">
                                                                </asp:CalendarExtender>
                                                                <asp:MaskedEditExtender ID="MaskedEditExtender10" runat="server" Enabled="true" TargetControlID="txteAuthActivityDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>

                                                                <asp:DropDownList ID="drpeAuthActivityHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                                                                <asp:DropDownList ID="drpeAuthActivityMin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                                                                &nbsp;&nbsp;
                                         
                                     <asp:Label ID="lbleAuthToothNo" class="lblCaption1" runat="server" Text="OBS Value :"></asp:Label>
                                                                <asp:TextBox ID="txteAuthToothNo" runat="server" Width="200px" Height="19px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc"></asp:TextBox>

                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <br />
                                                    <asp:GridView ID="gveAuthActivity" runat="server" AutoGenerateColumns="False"
                                                        EnableModelValidation="True" Width="100%" PageSize="200">
                                                        <HeaderStyle CssClass="GridHeader" Font-Bold="true" />
                                                        <RowStyle CssClass="GridRow" />
                                                        <Columns>

                                                            <asp:TemplateField HeaderText="Code">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="LinkButton22" runat="server" OnClick="SelecteAuthActivity_Click">
                                                                        <asp:Label ID="lbleAuthObsValue" CssClass="GridRow" runat="server" Text='<%# Bind("DURATION") %>' Visible="false"></asp:Label>
                                                                        <asp:Label ID="lbleAuthServID" CssClass="GridRow" runat="server" Text='<%# Bind("SERV_ID") %>' Visible="false"></asp:Label>
                                                                        <asp:Label ID="lbleAuthServCode" CssClass="GridRow" runat="server" Text='<%# Bind("SERV_CODE") %>'></asp:Label>
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Name">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="LinkButton6" runat="server" OnClick="SelecteAuthActivity_Click">
                                                                        <asp:Label ID="lbleAuthServName" CssClass="GridRow" runat="server" Text='<%# Bind("SERV_NAME") %>'></asp:Label>
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Req.Qty">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="LinkButton7" runat="server" OnClick="SelecteAuthActivity_Click">
                                                                        <asp:Label ID="lbleAuthQty" CssClass="GridRow" runat="server" Text='<%# Bind("QTY") %>'></asp:Label>
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Net">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="LinkButton15" runat="server" OnClick="SelecteAuthActivity_Click">
                                                                        <asp:Label ID="lbleAuthNet" CssClass="GridRow" runat="server" Text='<%# Bind("HAA_NET") %>'></asp:Label>
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Activity Start">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="LinkButton17" runat="server" OnClick="SelecteAuthActivity_Click">
                                                                        <asp:Label ID="lbleAuthActivityStart" CssClass="GridRow" runat="server" Text='<%# Bind("HAA_ACTIVITY_START") %>'></asp:Label>
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Type">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="LinkButton18" runat="server" OnClick="SelecteAuthActivity_Click">
                                                                        <asp:Label ID="lbleAuthActivityType" CssClass="GridRow" runat="server" Text='<%# Bind("HAA_ACTIVITY_TYPE") %>'></asp:Label>
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Approved Quantity">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="LinkButton8" runat="server" OnClick="SelecteAuthActivity_Click">
                                                                        <asp:Label ID="Label26" CssClass="GridRow" runat="server" Text='<%# Bind("HAA_APPROVED_QTY") %>'></asp:Label>
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                            <asp:TemplateField HeaderText="Denial Code">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="LinkButton19" runat="server" OnClick="SelecteAuthActivity_Click">
                                                                        <asp:Label ID="lblDenialCode" CssClass="GridRow" runat="server" Text='<%# Bind("DENIAL_CODE") %>'></asp:Label>
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Delete">
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="DeleteeAuthActivity" runat="server" ToolTip="Delete" ImageUrl="~/Images/icon_delete.png"
                                                                        OnClick="DeleteeAuthActivity_Click" />&nbsp;&nbsp;
                                                
                                                                </ItemTemplate>
                                                                <HeaderStyle Width="50px" />
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <PagerStyle CssClass="GridHeader" Font-Bold="true" HorizontalAlign="Center" />

                                                    </asp:GridView>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" style="height: 10px;"></td>
                                            </tr>
                                            <tr>
                                                <td style="width: 50%; vertical-align: text-top;">

                                                    <span class="lblCaption1">Chief Complaints </span>
                                                    <br />
                                                    <asp:TextBox ID="txteAuthCC" runat="server" Width="90%" CssClass="label" BorderWidth="1px" Height="50px" TextMode="MultiLine" BorderColor="#cccccc"></asp:TextBox>

                                                </td>
                                                <td style="width: 50%; vertical-align: text-top;">

                                                    <span class="lblCaption1">Management Plans </span>
                                                    <br />
                                                    <asp:TextBox ID="txteAuthManagementPlan" runat="server" Width="100%" CssClass="label" BorderWidth="1px" Height="50px" TextMode="MultiLine" BorderColor="#cccccc"></asp:TextBox>

                                                </td>
                                            </tr>

                                        </table>
                                    </ContentTemplate>
                                </asp:TabPanel>
                                <asp:TabPanel runat="server" ID="TabPanel7" HeaderText="Attachments" Width="100%">
                                    <ContentTemplate>
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 100%; vertical-align: text-top;">

                                                    <span class="lblCaption1">Attachments  </span><span style="color: red;">* </span>
                                                    <br />
                                                    <table style="width: 100%">
                                                        <tr>
                                                            <td>
                                                                <asp:FileUpload ID="fileAuthAttachment" runat="server" Width="300px" CssClass="ButtonStyle" accept="application/pdf" />
                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                             <asp:Button ID="btneAuthAttaAdd" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 60px;" CssClass="button orange small"
                                                 OnClick="btneAuthAttaAdd_Click" Text="Add" />

                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <asp:GridView ID="gveAuthAttachments" runat="server" AutoGenerateColumns="False"
                                                        EnableModelValidation="True" Width="95%" PageSize="50">
                                                        <HeaderStyle CssClass="GridHeader" Font-Bold="true" />
                                                        <RowStyle CssClass="GridRow" />
                                                        <Columns>

                                                            <asp:TemplateField HeaderText="File Name">
                                                                <ItemTemplate>

                                                                    <asp:Label ID="lblHaat_ID" CssClass="GridRow" runat="server" Text='<%# Bind("HAAT_ID") %>' Visible="false"></asp:Label>
                                                                    <asp:Label ID="Label27" CssClass="GridRow" runat="server" Text='<%# Bind("HAAT_FILE_NAME") %>'></asp:Label>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Actual File Name">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="Label28" CssClass="GridRow" runat="server" Text='<%# Bind("HAAT_FILE_NAME_ACTUAL") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Delete">
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="DeleteeAuthAtta" runat="server" ToolTip="Delete" ImageUrl="~/Images/icon_delete.png"
                                                                        OnClick="DeleteeAuthAtta_Click" />&nbsp;&nbsp;
                                                
                                                                </ItemTemplate>
                                                                <HeaderStyle Width="50px" />
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <PagerStyle CssClass="GridHeader" Font-Bold="true" HorizontalAlign="Center" />

                                                    </asp:GridView>

                                                </td>
                                                <td style="width: 50%; vertical-align: text-top;"></td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </asp:TabPanel>
                            </asp:TabContainer>

                            <br />
                            <table width="100%">

                                <tr>
                                    <td>
                                        <asp:Button ID="btneAuthSubmision" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 130px;" CssClass="button orange small" OnClick="btneAuthSubmision_Click" Text="Submision" OnClientClick="return ResubVal();" />

                                        <asp:Button ID="btnResubmisionGenerate" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 130px;" CssClass="button orange small" OnClick="btnResubmisionGenerate_Click" Text="Resubmision" OnClientClick="return ResubVal();" />
                                        <asp:Button ID="btneAuthReport" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 100px;" Visible="true" CssClass="button orange small" Text="Report" OnClick="btneAuthReport_Click" />
                                        <asp:Button ID="btnCancelAuth" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 130px;" Visible="true" CssClass="button orange small" Text="Cancel Resubmission" OnClick="btnCancelAuth_Click" OnClientClick="return window.confirm('Do you want to Cancel eAuthorization Submission?')" />


                                    </td>
                                </tr>
                            </table>
                    </ContentTemplate>

                </asp:TabPanel>

            </asp:TabContainer>
        </div>

        <br />
        <br />
 
   </asp:Content>


