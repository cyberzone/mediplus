﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Mediplus_BAL;


namespace Mediplus
{
    public partial class MalaffiLogDisplay : System.Web.UI.Page
    {
        DataSet ds = new DataSet();
        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine("MalaffiLogDisplay " + strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindGrid()
        {
            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();

            string Criteria = " 1=1 AND  HMHM_MESSAGE_CODE IN ('ADT-A28','ADT-A31','ADT-A11','ORU-R01') ";
            string strStartDate = txtSrcFromDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            if (txtSrcFromDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),HMHM_CREATED_DATE,101),101) >= '" + strForStartDate + "'";
            }

            string strTotDate = txtSrcToDate.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }



            if (txtSrcToDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),HMHM_CREATED_DATE,101),101) <= '" + strForToDate + "'";
            }



            if (txtSrcFileNo.Text != "")
            {
                Criteria += " AND  HMHM_PT_ID ='" + txtSrcFileNo.Text + "'";
            }

            if (drpSrcStatus.SelectedIndex != 0)
            {
                Criteria += " AND   HMHM_UPLOAD_STATUS = '" + drpSrcStatus.SelectedValue + "'";
            }


            DS = objCom.fnGetFieldValue("*, CONVERT(VARCHAR,HMHM_CREATED_DATE,103) AS  HMHM_CREATED_DATEDesc,  CONVERT(VARCHAR(5),HMHM_CREATED_DATE,108) AS  HMHM_CREATED_DATETimeDesc ", "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria, "HMHM_CREATED_DATE desc");

            if (DS.Tables[0].Rows.Count > 0)
            {
                gvMalaffiLog.DataSource = DS;
                gvMalaffiLog.DataBind();

            }
            else
            {
                gvMalaffiLog.DataBind();
            }

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("Default.aspx?NoSession=1"); }

            try
            {

                if (!IsPostBack)
                {

                    DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                    txtSrcFromDate.Text = strFromDate.ToString("dd/MM/yyyy");
                    txtSrcToDate.Text = strFromDate.ToString("dd/MM/yyyy");


                    if (Convert.ToString(Session["MalaffiLogFromDate"]) != null && Convert.ToString(Session["MalaffiLogFromDate"]) != "")
                    {

                        txtSrcFromDate.Text = Convert.ToString(Session["MalaffiLogFromDate"]);
                    }


                    if (Convert.ToString(Session["MalaffiLogToDate"]) != null && Convert.ToString(Session["MalaffiLogToDate"]) != "")
                    {

                        txtSrcToDate.Text = Convert.ToString(Session["MalaffiLogToDate"]);
                    }

                    if (Convert.ToString(Session["MalaffiLogFileNo"]) != null && Convert.ToString(Session["MalaffiLogFileNo"]) != "")
                    {

                        txtSrcFileNo.Text = Convert.ToString(Session["MalaffiLogFileNo"]);
                    }

                    if (Convert.ToString(Session["MalaffiLogStatus"]) != null && Convert.ToString(Session["MalaffiLogStatus"]) != "")
                    {

                        drpSrcStatus.SelectedValue = Convert.ToString(Session["MalaffiLogStatus"]);
                    }


                    BindGrid();

                }

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void Edit_Click(object sender, EventArgs e)
        {

            ImageButton btnEdit = new ImageButton();
            btnEdit = (ImageButton)sender;

            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
            ViewState["SelectIndex"] = gvScanCard.RowIndex;

            Label lblPatientID = (Label)gvScanCard.Cells[0].FindControl("lblPatientID");
            Label lblVisitID = (Label)gvScanCard.Cells[0].FindControl("lblVisitID");

            Label lblEMR_ID = (Label)gvScanCard.Cells[0].FindControl("lblEMR_ID");
            Label lblMessageCode = (Label)gvScanCard.Cells[0].FindControl("lblMessageCode");
            Label lblMessageType = (Label)gvScanCard.Cells[0].FindControl("lblMessageType");

            Label lblRadTestReportID = (Label)gvScanCard.Cells[0].FindControl("lblRadTestReportID");
            Label lblLabTestReportID = (Label)gvScanCard.Cells[0].FindControl("lblLabTestReportID");


            if (lblMessageCode.Text == "ADT-A28" || lblMessageCode.Text == "ADT-A31")
            {
                Response.Redirect("HMS/Registration/PTAndVisitRegistration.aspx?PatientId=" + lblPatientID.Text.Trim());
            }

            if (lblMessageCode.Text == "ADT-A11")
            {
                Response.Redirect("HMS/Registration/PatientVisitUpdate.aspx?MenuName=PatientSearch&VisitID=" + lblVisitID.Text + "&PatientID=" + lblPatientID.Text.Trim());
            }


            if (lblMessageCode.Text == "ORU-R01" && lblRadTestReportID.Text != "")
            {
                Response.Redirect("Radiology/Transaction.aspx?PageName=RadWaitList&MenuName=Radiology&RadTestReportID=" + lblRadTestReportID.Text + "&PatientID=" + lblPatientID.Text.Trim());

            }

            if (lblMessageCode.Text == "ORU-R01" && lblLabTestReportID.Text != "")
            {
                Response.Redirect("Laboratory/LabTestReport.aspx?PageName=MalaffiLog&MenuName=Laboratory&LabTestReportID=" + lblLabTestReportID.Text + "&PatientID=" + lblPatientID.Text.Trim());

            }


        }



        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                Session["MalaffiLogFromDate"] = txtSrcFromDate.Text;
                Session["MalaffiLogToDate"] = txtSrcToDate.Text;
                Session["MalaffiLogFileNo"] = txtSrcFileNo.Text;
                Session["MalaffiLogStatus"] = drpSrcStatus.SelectedValue;


                BindGrid();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnSearch_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void drpUploadStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                DropDownList btnEdit = new DropDownList();
                btnEdit = (DropDownList)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

                Label lblPatientID = (Label)gvScanCard.Cells[0].FindControl("lblPatientID");
                Label lblVisitID = (Label)gvScanCard.Cells[0].FindControl("lblVisitID");

                Label lblEMR_ID = (Label)gvScanCard.Cells[0].FindControl("lblEMR_ID");
                Label lblMessageID = (Label)gvScanCard.Cells[0].FindControl("lblMessageID");

                Label lblMessageCode = (Label)gvScanCard.Cells[0].FindControl("lblMessageCode");
                Label lblMessageType = (Label)gvScanCard.Cells[0].FindControl("lblMessageType");

                Label lblRadTestReportID = (Label)gvScanCard.Cells[0].FindControl("lblRadTestReportID");
                Label lblLabTestReportID = (Label)gvScanCard.Cells[0].FindControl("lblLabTestReportID");

                DropDownList drpUploadStatus = (DropDownList)gvScanCard.Cells[0].FindControl("drpUploadStatus");

                CommonBAL objCom = new CommonBAL();

                string Criteria = "HMHM_MESSAGE_ID='" + lblMessageID.Text + "' AND HMHM_PT_ID='" + lblPatientID.Text + "'";

                string FieldNameWithValues = " HMHM_UPLOAD_STATUS='" + drpUploadStatus.SelectedValue + "'";
               

                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria);

                BindGrid();
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      drpTreatTranStatus_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void gvMalaffiLog_Sorting(object sender, GridViewSortEventArgs e)
        {

        }

        protected void gvMalaffiLog_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblUploadStatus = (Label)e.Row.FindControl("lblUploadStatus");
                    DropDownList drpUploadStatus = (DropDownList)e.Row.FindControl("drpUploadStatus"); ;


                    drpUploadStatus.SelectedValue = lblUploadStatus.Text;

                    if (lblUploadStatus.Text == "UPLOADED")
                    {
                        drpUploadStatus.Enabled = false;
                    }
                }

            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      gvMalaffiLog_RowDataBound");
                TextFileWriting(ex.Message.ToString());
            }
        }
    }
}