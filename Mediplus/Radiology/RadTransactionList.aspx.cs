﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Mediplus_BAL;
using System.Threading;

namespace Mediplus.Radiology
{
    public partial class RadTransactionList : System.Web.UI.Page
    {

        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }


        void BindRadGrid()
        {
            DataSet DS = new DataSet();


            RadiologyBAL objRad = new RadiologyBAL();
            string Criteria = " 1=1 ";
            Criteria += " AND RTR_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            string strStartDate = txtFromDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + arrDate[1] + arrDate[0];
            }


            string strTotDate = txtToDate.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + arrToDate[1] + arrToDate[0];
            }



            Criteria += " AND CONVERT(VARCHAR, RTR_DATE,112)  >=" + strForStartDate;
            Criteria += " AND CONVERT(VARCHAR, RTR_DATE,112)  <=" + strForToDate;


            DS = objRad.TestReportGet(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                gvRadiologyHistory.DataSource = DS;
                gvRadiologyHistory.DataBind();
            }
            else
            {
                gvRadiologyHistory.DataBind();
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage(' Radiology List ','No Data','Brown')", true);

            }

        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            if (!IsPostBack)
            {
                DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                txtFromDate.Text = strFromDate.ToString("dd/MM/yyyy");
                txtToDate.Text = strFromDate.ToString("dd/MM/yyyy");

                BindRadGrid();
            }

        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            BindRadGrid();
        }

        protected void chkboxSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                CheckBox ChkBoxHeader = (CheckBox)gvRadiologyHistory.HeaderRow.FindControl("chkboxSelectAll");
                foreach (GridViewRow row in gvRadiologyHistory.Rows)
                {
                    CheckBox ChkBoxRows = (CheckBox)row.FindControl("chkInvResub");
                    if (ChkBoxHeader.Checked == true)
                    {
                        ChkBoxRows.Checked = true;
                    }
                    else
                    {
                        ChkBoxRows.Checked = false;
                    }


                }



            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      chkboxSelectAll_CheckedChanged");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void btnHL7MsgGenerate_Click(object sender, EventArgs e)
        {

            foreach (GridViewRow row in gvRadiologyHistory.Rows)
            {
                CheckBox chkInvResub = (CheckBox)row.FindControl("chkInvResub");
                Label lblgvRadHistTransNo = (Label)row.FindControl("lblgvRadHistTransNo");
                Label lblgvRadHistFileNo = (Label)row.FindControl("lblgvRadHistFileNo");
                Label lblgvRadHistDrID = (Label)row.FindControl("lblgvRadHistDrID");

                if (chkInvResub.Checked == true)
                {
                    DataSet DS = new DataSet();
                    CommonBAL objCom = new CommonBAL();
                    string Criteria = "HPV_PT_ID='" + lblgvRadHistFileNo.Text.Trim() + "' AND HPV_DR_ID='" + lblgvRadHistDrID .Text + "'";
                    DS = objCom.fnGetFieldValue(" TOP 1  HPV_SEQNO ", "HMS_PATIENT_VISIT", Criteria, " HPV_SEQNO DESC");

                   if (DS.Tables[0].Rows.Count > 0)
                   {
                       HL7MessageGenerator objHL7Mess = new HL7MessageGenerator();
                       objHL7Mess.PT_ID = lblgvRadHistFileNo.Text.Trim();
                       objHL7Mess.EMR_ID = "";
                       objHL7Mess.VISIT_ID =  Convert.ToString(DS.Tables[0].Rows[0]["HPV_SEQNO"]);
                       objHL7Mess.RAD_TEST_REPORT_ID = lblgvRadHistTransNo.Text;

                       objHL7Mess.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                       objHL7Mess.BRANCH_NAME = Convert.ToString(Session["Branch_Name"]);
                       objHL7Mess.PROVIDER_ID = Convert.ToString(Session["Branch_ProviderID"]);
                       objHL7Mess.UserID = Convert.ToString(Session["User_ID"]);

                       Boolean IsError = false;
                      //// IsError = objHL7Mess.RadMessageGenerate_ORU_R01();

                   }
                }


            }

            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), "PTRegistration", "ShowErrorMessage(' Radiology List Message ',' Radiology List Message Generated','Green')", true);

        }

    }
}