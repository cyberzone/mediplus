﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="RadTransactionList.aspx.cs" Inherits="Mediplus.Radiology.RadTransactionList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        function ShowErrorMessage(vMessage1, vMessage2, vColor) {
            document.getElementById("<%=lblMessage1.ClientID%>").innerHTML = vMessage1.trim();
            document.getElementById("<%=lblMessage2.ClientID%>").innerHTML = vMessage2.trim();

            if (vColor != '') {

                document.getElementById("<%=lblMessage2.ClientID%>").style.color = vColor;


            }

            document.getElementById("divMessage").style.display = 'block';



        }

        function HideErrorMessage() {

            document.getElementById("divMessage").style.display = 'none';

            document.getElementById("<%=lblMessage1.ClientID%>").innerHTML = '';
            document.getElementById("<%=lblMessage2.ClientID%>").innerHTML = '';
        }

        function Validation() {
            var IsError = false;
            var ErrorMessage = "";
            var trNewLineChar = "</br>";



            if (IsError == true) {
                ShowErrorMessage('PLEASE CHECK BELOW ERRORS', ErrorMessage, 'Brown');

                return false;
            }

            return window.confirm('Do you want to Generate Message ?');
        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <input type="hidden" id="hidPermission" runat="server" value="9" />
    <div style="padding-left: 60%; width: 100%;">
        <div id="divMessage" style="display: none; border: groove; height: auto; width: 600px; background-color: white; font-family: arial,helvetica,clean,sans-serif; border: 5px solid #999999; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: fixed; left: 500px; top: 300px;">
            <asp:UpdatePanel ID="UpdatePanel18" runat="server">
                <ContentTemplate>
                    <span style="float: right;">

                        <input type="button" id="Button1" class="ButtonStyle" style="color: #005c7b; background-color: #f6f6f6; border: none; border-radius: 0px; font-weight: bold; cursor: pointer; font-size: 15px;" value=" X " onclick="HideErrorMessage()" />

                    </span>
                    <br />
                    <div>
                        <div style="width: 100%; height: 20px; background: #E3E3E3; text-align: center;">
                            <asp:Label ID="lblMessage1" runat="server" CssClass="label" Style="font-weight: bold; color: black; letter-spacing: 1px; font-weight: bold;"></asp:Label>
                        </div>

                        <span style="padding-left: 5px;">
                            <asp:Label ID="lblMessage2" runat="server" CssClass="label" Style="font-weight: bold; color: #666;"></asp:Label></span>
                    </div>
                    <span style="float: right; margin-right: 20px">

                        <input type="button" id="Button5" class="ButtonStyle gray" style="font-weight: bold; cursor: pointer; width: 100px;" value=" OK " onclick="HideErrorMessage()" />

                    </span>
                    <br />
                </ContentTemplate>
            </asp:UpdatePanel>

            <br />

        </div>
    </div>
     <div style="padding-left:5px;">
    <table>
        <tr>
            <td class="PageHeader">Radiology List
               
            </td>
        </tr>
    </table>

    <div style="padding-top: 0px; width: 82%; border: 1px solid #005c7b; padding: 0px; border-radius: 10px;">
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <table width="100%">
                    <tr>
                        <td class="lblCaption1" style="height: 25px;padding-left:5px;">From Date
                        </td>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtFromDate" runat="server" Width="70px" Height="20PX" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                    <asp:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" TargetControlID="txtFromDate" Format="dd/MM/yyyy">
                                    </asp:CalendarExtender>

                                </ContentTemplate>
                            </asp:UpdatePanel>

                        </td>
                        <td class="lblCaption1" style="height: 25px;">To Date
                        </td>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtToDate" runat="server" Width="70px" Height="20PX" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                    <asp:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" TargetControlID="txtToDate" Format="dd/MM/yyyy">
                                    </asp:CalendarExtender>

                                </ContentTemplate>
                            </asp:UpdatePanel>

                        </td>
                        <td style="float: right;">
                            <asp:Button ID="btnRefresh" runat="server" Text="Refresh" Width="100px" CssClass="button red small" OnClick="btnRefresh_Click" />
                        </td>

                    </tr>

                </table>

            </ContentTemplate>
        </asp:UpdatePanel>

    </div>
    <div style="height: 5px;"></div>
    <div style="padding-top: 0px; width: 82%; height: 500px; overflow: auto; border: 1px solid #005c7b; padding-top: 0px; border-radius: 10px;">
        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
            <ContentTemplate>
                <asp:GridView ID="gvRadiologyHistory" runat="server"  AutoGenerateColumns="False" BorderStyle="None" GridLines="Horizontal"
                    EnableModelValidation="True" Width="100%">
                    <HeaderStyle CssClass="GridHeader_Blue" />
                    <RowStyle CssClass="GridRow" Font-Bold="true" />
                    <Columns>
                         <asp:TemplateField   HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                <HeaderTemplate>
                                    <asp:CheckBox ID="chkboxSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkboxSelectAll_CheckedChanged" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkInvResub" runat="server" CssClass="label"   />

                                </ItemTemplate>
                            </asp:TemplateField>

                        <asp:TemplateField HeaderText="TRANS NO." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign ="Center"  HeaderStyle-Width="100px">
                            <ItemTemplate>
                                <asp:Label ID="lblgvRadHistTransNo" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("RTR_TRANS_NO") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="DATE" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblgvRadHistDate" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("RTR_DATEDesc") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                          <asp:TemplateField HeaderText="FILE NO." HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblgvRadHistFileNo" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("RTR_PATIENT_ID") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                           <asp:TemplateField HeaderText="NAME" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblgvRadHistPTName" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("RTR_PATIENT_NAME") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="CODE" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblgvRadHistCode" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("RTR_RAD_CODE") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="DESCRIPTION" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblgvRadHistDesc" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("RTR_RAD_DESCRIPTION") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="DOCTOR NAME" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                 <asp:Label ID="lblgvRadHistDrID" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("RTR_DR_CODE") %>' Visible="false"></asp:Label>
                                <asp:Label ID="lblgvRadHistDrName" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("RTR_DR_NAME") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>

            </ContentTemplate>
        </asp:UpdatePanel>

    </div>
    <div style="height: 5px;"></div>
          <div style="padding-top: 0px; width: 82%;    border: 1px solid #005c7b; padding : 0px; border-radius: 10px;">
        <table style="width: 100%">
            <tr>
                <td style="float:right;">
                    <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                        <ContentTemplate>
                            <asp:Button ID="btnHL7MsgGenerate" runat="server" Text="Message Generate" Width="150px" CssClass="button red small" OnClick="btnHL7MsgGenerate_Click"   />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
        </div> 
     </div>
</asp:Content>
