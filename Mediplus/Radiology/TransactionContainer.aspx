﻿<%@ Page Title="" Language="C#"  AutoEventWireup="true" CodeBehind="TransactionContainer.aspx.cs" Inherits="HMS.Radiology.TransactionContainer" %>
 <html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <script src="../../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-1.4.1.js" type="text/javascript"></script>


           <link href="../Styles/Style.css" rel="stylesheet" type="text/css" />
        <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />

 <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />
    <style type="text/css">
        .box-header
        {
            border-bottom: 4px solid #f4fbfd;
            height: 70px;
        }

        .box-title
        {
            padding-bottom: 5px;
            border-bottom: 4px solid #92c500;
            float: left;
            font-size: 1.2em;
            color: #2078c0;
        }

        h1, h2, h3, h4, h5, h6
        {
            font-size: 100%;
            font-weight: normal;
            font-family: "Segoe UI", Arial, Helvetica, sans-serif;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
 
    <iframe id="frmRadio" runat="server"  src = "Transaction.aspx"  height="800px" width="99%" style="border:none;" ></iframe>
      

    </form>
</body>
</html>
