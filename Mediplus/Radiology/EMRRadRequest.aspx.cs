﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Mediplus.Radiology
{
    public partial class EMRRadRequest : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

         string EMR_ID = "";
         EMR_ID = Convert.ToString( Request.QueryString["EMR_ID"]);
         Radiologies.EMR_ID = Convert.ToString(EMR_ID);

        }
    }
}