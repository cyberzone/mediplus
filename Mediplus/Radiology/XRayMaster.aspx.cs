﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Mediplus_BAL;

namespace HMS.Radiology
{
    public partial class XRayMaster : System.Web.UI.Page
    {
        CommonBAL objCom = new CommonBAL();
        RadiologyBAL objRad = new RadiologyBAL();

        public string getURL()
        {
            return Request.Url.GetLeftPart(UriPartial.Authority) + Page.ResolveUrl("~/");
        }

        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../HMSLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine("Radiology.XRayMaster" + strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindXRayMasterGrid()
        {
            DataSet DS = new DataSet();
            //RXM_DATA_LOAD_FROM ='WEB' AND 
            string Criteria = "RXM_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "' ";
            objRad = new RadiologyBAL();
            DS = objRad.XrayMasterGet(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                gvXrayMasterPopup.DataSource = DS;
                gvXrayMasterPopup.DataBind();
            }

        }

        Boolean  CheckXRayCodeDuplicate()
        {
            DataSet DS = new DataSet();
          
            string Criteria = "RXM_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "' ";
            Criteria += " AND RXM_XRAY_CODE ='" + txtMasterCode.Text.Trim() + "' ";
           
            objRad = new RadiologyBAL();
            DS = objRad.XrayMasterGet(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                return true;
            }

            return false;
        }


        void BindXRayMasterData()
        {
            DataSet DS = new DataSet();
            //RXM_DATA_LOAD_FROM ='WEB' AND
            string Criteria = "    RXM_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "' AND RXM_XRAY_CODE='" + txtMasterCode.Text + "'";

            objRad = new RadiologyBAL();
            DS = objRad.XrayMasterGet(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
               

                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    ViewState["NewFlag"] = false;
                    txtMasterNo.Text = Convert.ToString(DR["RXM_XRAY_NO"]);

                    txtCPTCode.Text = Convert.ToString(DR["RXM_CPT_CODE"]);
                    txtMasterDesc.Text = Convert.ToString(DR["RXM_XRAY_DESCRIPTION"]);

                    txtServCode.Text = Convert.ToString(DR["RXM_SERV_ID"]);
                    txtServName.Text = Convert.ToString(DR["RXM_SERV_DESCRIPTION"]);
                    txtFee.Text = Convert.ToString(DR["RXM_FEE"]);

                    txtContent.Text = Convert.ToString(DR["RXM_REPORT1"]);// Server.HtmlDecode(Convert.ToString(DR["RXM_REPORT"]));
                    txtImporession.Text = Convert.ToString(DR["RXM_XRAY_IMPRESSION"]);


                }
            }

        }

        void BindHaadServiceDtlsGet()
        {
            dboperations objdbo = new dboperations();
            DataSet DS = new DataSet();
            string Criteria = " HHS_CODE='" + txtCPTCode.Text.Trim() + "' ";
            DS = objdbo.HaadServiceGet(Criteria, "", "1");


            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    txtCPTName.Text = Convert.ToString(DR["HHS_DESCRIPTION"]);


                    if (DR.IsNull("HHS_DESCRIPTION") == false && Convert.ToString(DR["HHS_DESCRIPTION"]) != "")
                    {
                        string str = Convert.ToString(DR["HHS_DESCRIPTION"]);
                        if (str.Length > 50)
                        {
                            str = str.Substring(0, 50);
                        }
                        txtCPTName.Text = str;

                    }
                }

            }

        }

        void Clear()
        {
            ViewState["NewFlag"] = true;
            //  drpTransType.SelectedIndex = 1;


            drpStatus.SelectedIndex = 0;

            txtMasterNo.Text = "";
            txtMasterCode.Text = "";
            txtMasterDesc.Text = "";
            //  txtImporession.Text = "";
            txtContent.Text = "";
            txtServCode.Text = "";
            txtServName.Text = "";
            txtFee.Text = "";

            txtCPTCode.Text = "";
            txtCPTName.Text = "";
            txtImporession.Text = "";

            btnSave.Visible = true;

        }

        void BindServiceMasterGrid()
        {
            string Criteria = " 1=1 AND HSM_STATUS='A' AND  HSM_TYPE='R' ";


            //if (txtServCode.Text.Trim() != "")
            //{
            //    Criteria += " AND HSM_SERV_ID LIKE '" + txtServCode.Text.Trim() + "%'";
            //}

            //if (txtServCode.Text.Trim() != "")
            //{
            //    Criteria += " AND HSM_NAME LIKE '" + txtServName.Text.Trim() + "%'";
            //}
            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();
            DS = dbo.ServiceMasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                gvServMasterPopup.DataSource = DS;
                gvServMasterPopup.DataBind();
            }

        }

        void BindHaadServiceGrid()
        {
            string Criteria = " 1=1 AND HHS_STATUS='Active' AND  HHS_TYPE_VALUE = 3 ";

            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();
            DS = dbo.HaadServiceGet(Criteria, "", "30");

            gvHaadMasterPopup.Visible = false;

            if (DS.Tables[0].Rows.Count > 0)
            {
                gvHaadMasterPopup.Visible = true;
                gvHaadMasterPopup.DataSource = DS;
                gvHaadMasterPopup.DataBind();
            }
            else
            {
                gvHaadMasterPopup.DataBind();
            }

        }

        void GetServiceDtls()
        {
            string Criteria = " 1=1 AND HSM_STATUS='A' AND  HSM_TYPE='R' ";
         Criteria += " AND HSM_SERV_ID LIKE '" + txtServCode.Text.Trim() + "%'";
             

            //if (txtServCode.Text.Trim() != "")
            //{
            //    Criteria += " AND HSM_NAME LIKE '" + txtServName.Text.Trim() + "%'";
            //}
            dboperations dbo = new dboperations();
            DataSet DS = new DataSet();
            DS = dbo.ServiceMasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                txtServName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSM_NAME"]);
                txtFee.Text = Convert.ToString(DS.Tables[0].Rows[0]["HSM_FEE"]);
            }

        }


        void AuditLogAdd(string ScreenAction, string Remarks)
        {
            CommonBAL objCom = new CommonBAL();
            objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
            objCom.PT_ID = "";
            objCom.ID = "";
            objCom.ScreenID = "RAD_XRAY";
            objCom.ScreenName = "X-Ray Master";
            objCom.ScreenType = "MASTER";
            objCom.ScreenAction = ScreenAction;
            objCom.Remarks = Remarks;
            objCom.UserID = Convert.ToString(Session["User_ID"]);
            objCom.UserCategory = Convert.ToString(Session["User_Category"]).ToUpper();

            objCom.EMRAuditLogAdd();


        }

        void SetPermission()
        {
            string strPermission = "0";
            if (Convert.ToString(Session["ROLL_IDS"]) == "" || Convert.ToString(Session["ROLL_IDS"]) == null)
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../ErrorPage.aspx?MenuName=Radiology");
            }
            string Criteria = " 1=1 AND HRT_SCREEN_ID='RAD_XRAY' ";
            Criteria += " AND  HRT_ROLL_ID IN ( " + Convert.ToString(Session["ROLL_IDS"]) + ")";

            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.RollTransGet(Criteria);


            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {
                btnSave.Visible = false;
                //btnSave.Enabled = false;
                //btnDelete.Enabled = false;
                //btnClear.Enabled = false;

            }

            if (strPermission == "5")
            {
                //  btnDelete.Enabled = false;

            }

            if (strPermission == "7")
            {
                btnSave.Enabled = false;
                // btnAuthorize.Visible = false;

            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {

                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../ErrorPage.aspx?MenuName=Radiology");
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }
            lblStatus.Text = "";
            try
            {
                if (!IsPostBack)
                {
                    if (Convert.ToString(Session["HMS_AUTHENTICATION"]) == "1")
                    {
                        if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_ID"]).ToUpper() != "SUPER_ADMIN")
                        {
                            SetPermission();
                        }
                    }
                    AuditLogAdd("OPEN", "Open X-Ray Master Page");

                    ViewState["NewFlag"] = true;
                    DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                    //txtTransDate.Text = strFromDate.ToString("dd/MM/yyyy");


                    //txtTransNo.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "SCAN");

                    string XrayCode = Convert.ToString(Request.QueryString["XrayCode"]);

                    if (XrayCode != " " && XrayCode != null)
                    {

                        txtMasterCode.Text = Convert.ToString(XrayCode);
                        BindXRayMasterData();
                        BindHaadServiceDtlsGet();

                    }

                  //  BindXRayMasterGrid();
                   // BindServiceMasterGrid();
                   // BindHaadServiceGrid();

                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            try
            {
                Clear();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void XrayMasterEdit_Click(object sender, EventArgs e)
        {
            txtContent.Text = "";
            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();

            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;
            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

            Label lblgvXRayNo = (Label)gvScanCard.Cells[0].FindControl("lblgvXRayNo");
            Label lblgvXRayCode = (Label)gvScanCard.Cells[0].FindControl("lblgvXRayCode");
            Label lblgvXRayDesc = (Label)gvScanCard.Cells[0].FindControl("lblgvXRayDesc");
            Label lblgvXRayReport = (Label)gvScanCard.Cells[0].FindControl("lblgvXRayReport");


            txtMasterNo.Text = lblgvXRayNo.Text;
            txtMasterCode.Text = lblgvXRayCode.Text;
            txtMasterDesc.Text = lblgvXRayDesc.Text;
            //txtContent.Text = Server.HtmlDecode(lblgvXRayReport.Text);

            //  txtContent.Text = Server.HtmlDecode(lblgvXRayReport.Text);


            //  txtContent.Text = clsWebReport.ConvertToTxt(lblgvXRayReport.Text);


            BindXRayMasterData();
            BindHaadServiceDtlsGet();
            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "HideXrayMasterPopup()", true);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtMasterCode.Text.Trim() == "")
                {
                    lblStatus.Text = "Enter Code";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }
                if (txtMasterDesc.Text.Trim() == "")
                {
                    lblStatus.Text = "Enter Description";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                if (txtServCode.Text.Trim() == "")
                {
                    lblStatus.Text = "Enter Service Code";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                if (Convert.ToBoolean(ViewState["NewFlag"]) == true)
                {
                    if (CheckXRayCodeDuplicate() == true)
                    {
                        lblStatus.Text = "Same x-Ray code already exists.";
                        lblStatus.ForeColor = System.Drawing.Color.Red;
                        goto FunEnd;
                    }
                }

                string tempStr = "";
                tempStr = txtContent.Text;// Server.HtmlEncode(txtContent.Text);


                objRad = new RadiologyBAL();
                objRad.RTR_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);

                objRad.RTR_RAD_NO = txtMasterNo.Text.Trim();
                objRad.RTR_RAD_CODE = txtMasterCode.Text.Trim();
                objRad.RTR_RAD_DESCRIPTION = txtMasterDesc.Text.Trim();
                objRad.RTR_RAD_IMPRESSION = txtImporession.Text;
                objRad.RTR_STATUS = drpStatus.SelectedValue;
                objRad.RTR_REPORT = tempStr;
                objRad.RTR_SERV_ID = txtServCode.Text;
                objRad.RTR_SERV_DESCRIPTION = txtServName.Text;
                objRad.RTR_FEE = txtFee.Text;
                objRad.RTR_CPT_CODE = txtCPTCode.Text;
                if (Convert.ToBoolean(ViewState["NewFlag"]) == true)
                {
                    objRad.NewFlag = "A";
                }
                else
                {
                    objRad.NewFlag = "M";
                }

                objRad.UserID = Convert.ToString(Session["User_ID"]);
                objRad.XrayMasterAdd();

                if (Convert.ToBoolean(ViewState["NewFlag"]) == false)
                {
                    AuditLogAdd("MODIFY", "Modify Existing entry in the X-Ray Master, X-Ray No. is " +  txtMasterNo.Text.Trim());
                }
                else
                {
                    AuditLogAdd("ADD", "Add new entry in the X-Ray Master, X-Ray No. is " + txtMasterNo.Text.Trim());
                }

                Clear();
                lblStatus.Text = "";
                lblStatus.Text = "Data  Saved";
                lblStatus.ForeColor = System.Drawing.Color.Green;

                BindXRayMasterGrid();
            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void ServMasterEdit_Click(object sender, EventArgs e)
        {
            txtContent.Text = "";
            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();

            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;
            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

            Label lblServID = (Label)gvScanCard.Cells[0].FindControl("lblServID");
            Label lblServName = (Label)gvScanCard.Cells[0].FindControl("lblServName");
            Label lblServFee = (Label)gvScanCard.Cells[0].FindControl("lblServFee");




            txtServCode.Text = lblServID.Text;
            txtServName.Text = lblServName.Text;

            if (lblServName.Text != "")
            {
                string str = lblServName.Text;
                if (str.Length > 50)
                {
                    str = str.Substring(0, 50);
                }
                txtServName.Text = str;

            }


            txtFee.Text = Server.HtmlDecode(lblServFee.Text);



            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "HideServMasterPopup()", true);
        }


        protected void HaadMasterEdit_Click(object sender, EventArgs e)
        {
            txtContent.Text = "";
            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();

            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;
            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

            Label lblHaadCode = (Label)gvScanCard.Cells[0].FindControl("lblHaadCode");
            Label lblHaadDesc = (Label)gvScanCard.Cells[0].FindControl("lblHaadDesc");





            txtCPTCode.Text = lblHaadCode.Text;
            txtCPTName.Text = lblHaadDesc.Text;

            if (lblHaadDesc.Text != "")
            {
                string str = lblHaadDesc.Text;
                if (str.Length > 50)
                {
                    str = str.Substring(0, 50);
                }
                txtCPTName.Text = str;

            }






            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "HideHaadMasterPopup()", true);
        }

        protected void txtServCode_TextChanged(object sender, EventArgs e)
        {
            GetServiceDtls();
        }
    }
}