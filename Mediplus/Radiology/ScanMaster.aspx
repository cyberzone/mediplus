﻿<%@ Page Language="C#"  MasterPageFile="~/Site2.Master"   validaterequest="false" AutoEventWireup="true" CodeBehind="ScanMaster.aspx.cs" Inherits="HMS.Radiology.ScanMaster" %>

<%@ register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<%@ Register TagPrefix="UC1" TagName="LeftMenu" Src="~/HMS/UserControl/LeftMenu.ascx" %>
  <asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />

       
     <script src="../Scripts/AccordionScript.js"></script>
     <link rel="stylesheet" href="../Styles/Accordionstyles.css">
  <script language="javascript" type="text/javascript">
      function OnlyNumeric(evt) {
          var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
          if (chCode >= 48 && chCode <= 57 ||
               chCode == 46) {
              return true;
          }
          else {
              return false;
          }
      }

      function ShowScanMasterPopup() {

          document.getElementById("divScanMasterPopup").style.display = 'block';


      }

      function HideScanMasterPopup() {

          document.getElementById("divScanMasterPopup").style.display = 'none';

      }


      function ShowServMasterPopup() {

          document.getElementById("divSerMasterPopup").style.display = 'block';


      }

      function HideServMasterPopup() {

          document.getElementById("divSerMasterPopup").style.display = 'none';

      }


      function ShowHaadMasterPopup() {

          document.getElementById("divHaadMasterPopup").style.display = 'block';


      }

      function HideHaadMasterPopup() {

          document.getElementById("divHaadMasterPopup").style.display = 'none';

      }

      function ServicePopup(CtrlName, strValue) {

          var win = window.open("../HMS/Masters/ServiceMasterLookup.aspx?PageName=HomeCareReg&CtrlName=" + CtrlName + "&Value=" + strValue + "&Type=R", "newwin", "top=200,left=270,height=570,width=950,toolbar=no,scrollbars==yes,menubar=no");
          win.focus();


          return true;

      }


      function BindServiceDtls(ServCode, ServName, HaadCode, CtrlName, Fees) {

          document.getElementById("<%=txtServCode.ClientID%>").value = ServCode;
             document.getElementById("<%=txtServName.ClientID%>").value = ServName;
             document.getElementById("<%=txtFee.ClientID%>").value = Fees;



         }


         function HaadShow(CtrlName, strValue) {
             var CodeType = '3';
             var ServCode = '';

             var win = window.open("../HMS/Masters/HaadServiceLookup.aspx?CtrlName=" + CtrlName + "&Value=" + strValue + "&CodeType=" + CodeType + "&CategoryType=All", "newwin1", "top=200,left=270,height=500,width=750,toolbar=no,scrollbars=yes,menubar=no");

             win.focus();
             return false;

         }
         function BindHaadDtls(HaadCode, HaadName, CtrlName) {

             document.getElementById("<%=txtCPTCode.ClientID%>").value = HaadCode;
            document.getElementById("<%=txtCPTName.ClientID%>").value = HaadName;

        }


        function ScanMasterShow(CtrlName, strValue) {

            var win = window.open("ScanMasterPopup.aspx?PageName=ScanMaster&CtrlName=" + CtrlName + "&Value=" + strValue, "ScanMaster", "top=300,left=370,height=500,width=750,toolbar=no,scrollbars=yes,menubar=no");

            win.focus();
            return false;

        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
       <input type="hidden" id="hidPermission" runat="server" value="9" />
        <div>
            

            <table width="80%">
                <tr>
                    <td style="text-align: left; width: 50%;" class="PageHeader"> 
                       <asp:button id="btnNew" runat="server" cssclass="button gray small" text="New" onclick="btnNew_Click" />
                        &nbsp;

                    </td>
                    <td style="text-align: right; width: 50%;">
                        <asp:button id="btnSave" runat="server" cssclass="button red small" text="Save Record" onclick="btnSave_Click" />

                    </td>
                </tr>
            </table>
            <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="80%">
                <tr>
                    <td>
                        <asp:label id="lblStatus" runat="server" forecolor="red" style="letter-spacing: 1px; font-weight: bold;" cssclass="label"></asp:label>
                    </td>
                </tr>

            </table>
            <div style="padding-top: 0px; width: 80%; border: thin; border-color: #f6f6f6; border-style: groove; padding: 5px;">
                <table width="100%" border="0" cellpadding="3" cellspacing="3">
                    <tr>
                        <td class="lblCaption1" style="height: 25px; width: 100px;">
                            <asp:updatepanel id="UpdatePanel10" runat="server">
                                            <ContentTemplate>
                   <asp:Label ID="lblMasterCaption" runat="server"  CssClass="label" text="Scan No."></asp:Label>
                                                   </ContentTemplate>
                                     </asp:updatepanel>
                        </td>
                        <td>
                            <asp:updatepanel id="UpdatePanel3" runat="server">
                                            <ContentTemplate>
                                    <asp:TextBox ID="txtMasterNo" runat="server" Width="150px" CssClass="TextBoxStyle"  ReadOnly="True"  ></asp:TextBox>
                                            </ContentTemplate>
                                     </asp:updatepanel>
                        </td>
                      
                        <td class="lblCaption1" style="height: 25px;">Status
                        </td>
                        <td colspan="5">
                            <asp:updatepanel id="UpdatePanel2" runat="server">
                                            <ContentTemplate>
                                     <asp:dropdownlist id="drpStatus" runat="server"  style="width: 155px" class="TextBoxStyle"  >
                                      <asp:ListItem Value="A" Selected="True">Active</asp:ListItem>
                                      <asp:ListItem Value="I" >InActive</asp:ListItem>
                                    
                                   </asp:dropdownlist>
                                        </ContentTemplate>
                                     </asp:updatepanel>
                        </td>
                    </tr>
                    <tr>
                          <td class="lblCaption1" style="height: 25px; width: 100px;">
                            <asp:label id="Label2" runat="server" cssclass="label" text="Code"></asp:label>
                        </td>
                        <td>
                            <asp:updatepanel id="UpdatePanel5" runat="server">
                                            <ContentTemplate>
                                    <asp:TextBox ID="txtMasterCode" runat="server" Width="150px" CssClass="TextBoxStyle"   ondblclick="return ScanMasterShow('txtMasterCode',this.value);" ></asp:TextBox>
                                            </ContentTemplate>
                                     </asp:updatepanel>
                        </td>
                        <td class="lblCaption1" style="height: 25px;">
                            <asp:label id="Label1" runat="server" cssclass="label" text="Description"></asp:label>
                        </td>
                        <td colspan="3">
                            <asp:updatepanel id="UpdatePanel4" runat="server">
                                            <ContentTemplate>
                                    <asp:TextBox ID="txtMasterDesc" runat="server" Width="350px" CssClass="TextBoxStyle"    ondblclick="return ScanMasterShow('txtMasterDesc',this.value);" ></asp:TextBox>
                                            </ContentTemplate>
                                     </asp:updatepanel>
                        </td>


                    </tr>
                   <tr>
                        <td class="lblCaption1" style="height: 25px;">
                            <asp:label id="Label4" runat="server" cssclass="label" text="ServiceCode"></asp:label>
                        </td>
                        <td>
                            <asp:updatepanel id="UpdatePanel7" runat="server">
                                            <ContentTemplate>
                                    <asp:TextBox ID="txtServCode" runat="server" Width="150px" CssClass="TextBoxStyle"   ondblclick="return ServicePopup('txtServCode',this.value);"  AutoPostBack="true" OnTextChanged="txtServCode_TextChanged" ></asp:TextBox>
                                            </ContentTemplate>
                                     </asp:updatepanel>
                        </td>
                        <td class="lblCaption1" style="height: 25px;">
                            <asp:label id="Label5" runat="server" cssclass="label" text="Name"></asp:label>
                        </td>
                        <td>
                            <asp:updatepanel id="UpdatePanel8" runat="server">
                                            <ContentTemplate>
                                    <asp:TextBox ID="txtServName" runat="server" Width="350px" CssClass="TextBoxStyle"    ondblclick="return ServicePopup('txtServName',this.value);"></asp:TextBox>
                                            </ContentTemplate>
                                     </asp:updatepanel>
                        </td>
                        <td class="lblCaption1" style="height: 25px;">
                            <asp:label id="Label6" runat="server" cssclass="label" text="Fee"></asp:label>
                        </td>
                        <td>
                            <asp:updatepanel id="UpdatePanel9" runat="server">
                                            <ContentTemplate>
                                    <asp:TextBox ID="txtFee" runat="server" Width="150px" CssClass="TextBoxStyle"   ></asp:TextBox>
                                            </ContentTemplate>
                                     </asp:updatepanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1" style="height: 25px;">
                            <asp:label id="Label3" runat="server" cssclass="label" text="CPT Code"></asp:label>
                        </td>
                        <td>
                            <asp:updatepanel id="UpdatePanel6" runat="server">
                                            <ContentTemplate>
                                    <asp:TextBox ID="txtCPTCode" runat="server" Width="150px" CssClass="TextBoxStyle"    ondblclick="return HaadShow('txtCPTCode',this.value);"></asp:TextBox>
                                            </ContentTemplate>
                            </asp:updatepanel>
                        </td>
                        <td class="lblCaption1" style="height: 25px;">
                            <asp:label id="Label7" runat="server" cssclass="label" text="Name"></asp:label>
                        </td>
                        <td colspan="3">
                            <asp:updatepanel id="UpdatePanel1" runat="server">
                                            <ContentTemplate>
                                    <asp:TextBox ID="txtCPTName" runat="server" Width="350px" CssClass="TextBoxStyle"   ondblclick="return HaadShow('txtCPTName',this.value);" ></asp:TextBox>
                                            </ContentTemplate>
                                     </asp:updatepanel>
                        </td>
                    </tr>

                </table>
                <table width="100%" border="0" cellpadding="5" cellspacing="5">
                    <tr>
                        <td valign="top" >

                            <asp:textbox id="txtContent" height="500" maxlength="8000" width="100%" textmode="MultiLine" runat="server" class="mceEditor" style="resize: none;" />



                        </td>
                         <td style="width: 2%;"></td>
                    
                    </tr>
                </table>
                <table width="100%" cellpadding="5" cellspacing="5">
                            <tr>
                                <td class="lblCaption1" style="width: 100px;">Impression
                                </td>
                                <td>
                                    <asp:updatepanel id="UpdatePanel12" runat="server">
                                            <ContentTemplate>
                                             <asp:TextBox ID="txtImporession"     runat="server"  class="TextBoxStyle" width="535px" />
                                              </ContentTemplate>
                                     </asp:updatepanel>
                                </td>
                            </tr>
                            </table>
            </div>
        </div>

        <div id="divScanMasterPopup" style="display: none; overflow: hidden; border: groove; height: 450px; width: 600px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #eee9e9; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute; left: 400px; top: 150px;">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td style="vertical-align: top;"></td>
                    <td align="right" style="vertical-align: top;">

                        <input type="button" id="btnMsgClose" class="ButtonStyle" style="color: black; border: none;" value=" X " onclick="HideScanMasterPopup()" />
                    </td>
                </tr>
            </table>
            <div style="padding-top: 0px; width: 100%; height: 330px; overflow: auto; border: thin; border-color: #f6f6f6; border-style: groove;">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td colspan="2">


                            <asp:gridview id="gvScanMasterPopup" runat="server" allowsorting="True" autogeneratecolumns="False"
                                enablemodelvalidation="True" width="100%">
                                                                 <HeaderStyle CssClass="GridHeader_Blue" />
                                                                    <RowStyle CssClass="GridRow" />
                                                                    <AlternatingRowStyle CssClass="GridAlterRow" />
                                                                <Columns>
                                                                    
                                                                    <asp:TemplateField HeaderText="Scan No." HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px">
                                                                        <ItemTemplate>
                                                                             <asp:LinkButton ID="lnkgvScanNo" CssClass="lblCaption1"  runat="server"  OnClick="ScanMasterEdit_Click"  OnClientClick="" >

                                                                            <asp:Label ID="lblgvScanReport" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("RSM_REPORT1") %>' visible="false"></asp:Label>
                                                                            <asp:Label ID="lblgvScanNo" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("RSM_SCAN_NO") %>'></asp:Label>
                                                                                 </asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="Scan Code" HeaderStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                             <asp:LinkButton ID="lnkgvScanCode" CssClass="lblCaption1"  runat="server"  OnClick="ScanMasterEdit_Click"  OnClientClick="" >
                                                                            <asp:Label ID="lblgvScanCode" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("RSM_SCAN_CODE") %>'></asp:Label>
                                                                                 </asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Description" HeaderStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="lnkgvScanDesc" CssClass="lblCaption1"  runat="server"  OnClick="ScanMasterEdit_Click"  OnClientClick="" >
                                                                            <asp:Label ID="lblgvScanDesc" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("RSM_SCAN_DESCRIPTION") %>'></asp:Label>
                                                                                </asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                     
                                                                </Columns>
                                                            </asp:gridview>

                        </td>

                    </tr>

                </table>
            </div>
        </div>

         <div id="divSerMasterPopup" style="display: none; overflow: hidden; border: groove; height: 450px; width: 600px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #eee9e9; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute; left: 420px; top: 170px;">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td style="vertical-align: top;"></td>
                    <td align="right" style="vertical-align: top;">

                        <input type="button" id="Button1" class="ButtonStyle" style="color: black; border: none;" value=" X " onclick="HideServMasterPopup()" />
                    </td>
                </tr>
            </table>
            <div style="padding-top: 0px; width: 100%; height: 400px; overflow: auto; border: thin; border-color: #f6f6f6; border-style: groove;">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td colspan="2">


                            <asp:gridview id="gvServMasterPopup" runat="server" allowsorting="True" autogeneratecolumns="False"
                                enablemodelvalidation="True" width="100%">
                                                                 <HeaderStyle CssClass="GridHeader_Blue" />
                                                                    <RowStyle CssClass="GridRow" />
                                                                    <AlternatingRowStyle CssClass="GridAlterRow" />
                                                                <Columns>
                                                                    
                                                                    <asp:TemplateField HeaderText="Service ID" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px">
                                                                        <ItemTemplate>
                                                                             <asp:LinkButton ID="lnkServID" CssClass="lblCaption1"  runat="server"  OnClick="ServMasterEdit_Click"   >
                                                                            <asp:Label ID="lblServID" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HSM_SERV_ID") %>'></asp:Label>
                                                                                 </asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="Description" HeaderStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                             <asp:LinkButton ID="lnkServName" CssClass="lblCaption1"  runat="server"  OnClick="ServMasterEdit_Click"   >
                                                                            <asp:Label ID="lblServName" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HSM_NAME") %>'></asp:Label>
                                                                                 </asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Fee" HeaderStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="lnkServFee" CssClass="lblCaption1"  runat="server"  OnClick="ServMasterEdit_Click"   >
                                                                            <asp:Label ID="lblServFee" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HSM_FEE") %>'></asp:Label>
                                                                                </asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                     <asp:TemplateField HeaderText="Type" HeaderStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="lnkServType" CssClass="lblCaption1"  runat="server"  OnClick="ServMasterEdit_Click"   >
                                                                            <asp:Label ID="lblServType" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HSM_TYPE") %>'></asp:Label>
                                                                                </asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:gridview>

                        </td>

                    </tr>

                </table>
            </div>
        </div>

        <div id="divHaadMasterPopup" style="display: none; overflow: hidden; border: groove; height: 450px; width: 600px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #eee9e9; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute; left: 450px; top: 200px;">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td style="vertical-align: top;"></td>
                    <td align="right" style="vertical-align: top;">

                        <input type="button" id="Button2" class="ButtonStyle" style="color: black; border: none;" value=" X " onclick="HideHaadMasterPopup()" />
                    </td>
                </tr>
            </table>
            <div style="padding-top: 0px; width: 100%; height: 400px; overflow: auto; border: thin; border-color: #f6f6f6; border-style: groove;">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td colspan="2">


                            <asp:gridview id="gvHaadMasterPopup" runat="server" allowsorting="True" autogeneratecolumns="False"
                                enablemodelvalidation="True" width="100%">
                                                                 <HeaderStyle CssClass="GridHeader_Blue" />
                                                                    <RowStyle CssClass="GridRow" />
                                                                    <AlternatingRowStyle CssClass="GridAlterRow" />
                                                                <Columns>
                                                                    
                                                                    <asp:TemplateField HeaderText="Cdoe" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px">
                                                                        <ItemTemplate>
                                                                             <asp:LinkButton ID="lnkHaadCode" CssClass="lblCaption1"  runat="server"  OnClick="HaadMasterEdit_Click"   >
                                                                            <asp:Label ID="lblHaadCode" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HHS_CODE") %>'></asp:Label>
                                                                                 </asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="Description" HeaderStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                             <asp:LinkButton ID="lnkHaadDesc" CssClass="lblCaption1"  runat="server"  OnClick="HaadMasterEdit_Click"   >
                                                                            <asp:Label ID="lblHaadDesc" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("HHS_DESCRIPTION") %>'></asp:Label>
                                                                                 </asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:gridview>

                        </td>

                    </tr>

                </table>
            </div>
        </div>

    
    
 
    
<!-- Start for TinyMCE -->

<script type="text/javascript" src="../tiny_mce/tiny_mce.js"></script>

<script type="text/javascript">
    tinyMCE.init({
        // General options textareas
        mode: "textareas",
        theme: "advanced",
        editor_selector: "mceEditor",
        editor_deselector: "mceNoEditor",
        relative_urls: false,
        remove_script_host: false,
        // document_base_url : "http://localhost:1803/DIHWebsite/",
        document_base_url: '<% =getURL() %> ',
        //       setup : function(ed) {
        //          ed.onKeyUp.add(function(ed, e) {   
        //               var strip = (tinyMCE.activeEditor.getContent()).replace(/(<([^>]+)>)/ig,"");
        //               var text =   strip.length + " Characters"
        //        tinymce.DOM.setHTML(tinymce.DOM.get(tinyMCE.activeEditor.id + '_path_row'), text);   
        //         });},

        plugins: "safari,pagebreak,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",

        // Theme options
        theme_advanced_buttons1: "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,fontselect,fontsizeselect",
        theme_advanced_buttons2: "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
        theme_advanced_buttons3: "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
        theme_advanced_buttons4: "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,pagebreak",
        theme_advanced_toolbar_location: "top",
        theme_advanced_toolbar_align: "left",
        theme_advanced_statusbar_location: "bottom",
        theme_advanced_resizing: true,

        // Example content CSS (should be your site CSS)
        content_css: "css/content.css",

        // Drop lists for link/image/media/template dialogs
        template_external_list_url: "lists/template_list.js",
        external_link_list_url: "lists/link_list.js",
        external_image_list_url: "lists/image_list.js",
        media_external_list_url: "lists/media_list.js",

        // Replace values for the template plugin
        template_replace_values: {
            username: "Some User",
            staffid: "991234"
        }
    });

</script>

<!-- End for TinyMCE -->

       </asp:Content>
 
