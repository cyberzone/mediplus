﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using Mediplus_BAL;

namespace outlook_menu.Radiology
{
    public partial class ScanMasterPopup : System.Web.UI.Page
    {
        DataSet DS;
        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindGrid()
        {
            DataSet DS = new DataSet();
            LaboratoryBAL objLab = new LaboratoryBAL();
            string Criteria = " 1=1 ";// and  RSM_DATA_LOAD_FROM ='WEB'  ";
            Criteria += " AND  RSM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            if (txtSearch.Text.Trim() != "")
            {
                Criteria += " AND RSM_XRAY_CODE + ' ' +isnull(RSM_XRAY_DESCRIPTION,'')   like '%" + txtSearch.Text.Trim() + "%' ";
            }

            DS = new DataSet();

            RadiologyBAL objRad = new RadiologyBAL();
            DS = objRad.ScanMasterGet(Criteria);

            gvPopupGrid.Visible = false;

            if (DS.Tables[0].Rows.Count > 0)
            {
                gvPopupGrid.Visible = true;
                gvPopupGrid.DataSource = DS;
                gvPopupGrid.DataBind();
            }
            else
            {
                gvPopupGrid.DataBind();
            }

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            if (!IsPostBack)
            {
                try
                {
                    hidCtrlName.Value = Convert.ToString(Request.QueryString["CtrlName"]);
                    ViewState["Value"] = Convert.ToString(Request.QueryString["Value"]);
                    hidPageName.Value = Convert.ToString(Request.QueryString["PageName"]);

                    txtSearch.Text = Convert.ToString(ViewState["Value"]);

                    BindGrid();

                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      XRayMasterPopup.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }

            }

        }


        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                BindGrid();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      XRayMasterPopup.txtSearch_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }
    }
}