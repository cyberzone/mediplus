﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Mediplus_BAL;
using System.Threading;



namespace HMS.Radiology 
{
    public partial class Transaction : System.Web.UI.Page
    {
        CommonBAL objCom = new CommonBAL();
        RadiologyBAL objRad = new RadiologyBAL();

        public string getURL()
        {
            return Request.Url.GetLeftPart(UriPartial.Authority) + Page.ResolveUrl("~/");
        }

        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindTime12HrsFromDB()
        {

            objCom = new CommonBAL();
            DataSet DSHours = new DataSet();

            DSHours = objCom.HoursGet("True");

            drpTransHour.DataSource = DSHours;
            drpTransHour.DataTextField = "Name";
            drpTransHour.DataValueField = "Code";
            drpTransHour.DataBind();

            drpReqHour.DataSource = DSHours;
            drpReqHour.DataTextField = "Name";
            drpReqHour.DataValueField = "Code";
            drpReqHour.DataBind();



            drpObservStHour.DataSource = DSHours;
            drpObservStHour.DataTextField = "Name";
            drpObservStHour.DataValueField = "Code";
            drpObservStHour.DataBind();

            drpObservEndHour.DataSource = DSHours;
            drpObservEndHour.DataTextField = "Name";
            drpObservEndHour.DataValueField = "Code";
            drpObservEndHour.DataBind();

            drpObservHour.DataSource = DSHours;
            drpObservHour.DataTextField = "Name";
            drpObservHour.DataValueField = "Code";
            drpObservHour.DataBind();



            DataSet DSMin = new DataSet();

            DSMin = objCom.MinutesGet("1");


            drpTransMin.DataSource = DSMin;
            drpTransMin.DataTextField = "Name";
            drpTransMin.DataValueField = "Code";
            drpTransMin.DataBind();

            drpReqMin.DataSource = DSMin;
            drpReqMin.DataTextField = "Name";
            drpReqMin.DataValueField = "Code";
            drpReqMin.DataBind();

            drpObservStMin.DataSource = DSMin;
            drpObservStMin.DataTextField = "Name";
            drpObservStMin.DataValueField = "Code";
            drpObservStMin.DataBind();

            drpObservEndMin.DataSource = DSMin;
            drpObservEndMin.DataTextField = "Name";
            drpObservEndMin.DataValueField = "Code";
            drpObservEndMin.DataBind();

            drpObservMin.DataSource = DSMin;
            drpObservMin.DataTextField = "Name";
            drpObservMin.DataValueField = "Code";
            drpObservMin.DataBind();



        }

        void PatientDataBind()
        {
            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HPM_PT_ID = '" + txtFileNo.Text + "'";
            Criteria += " AND HPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            objCom = new CommonBAL();

            ds = objCom.PatientMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                txtFileNo.Text = ds.Tables[0].Rows[0]["HPM_PT_ID"].ToString();
                txtFName.Text = ds.Tables[0].Rows[0]["FullName"].ToString();


                txtDOB.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_DOBDesc"]);
                //  lblAge.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_Age"]) + " " + Convert.ToString(ds.Tables[0].Rows[0]["HPM_Age1"]);

                txtAge.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_Age"]);

                drpAgeType.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE_TYPE"]);

                txtAge1.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_Age1"]);

                drpAgeType1.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE_TYPE1"]);


                //   hidAgeType.Value = Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE_TYPE"]);

                txtSex.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_SEX"]);

                //txtAddress.Text = ds.Tables[0].Rows[0]["HPM_ADDR"].ToString();
                //txtPoBox.Text = ds.Tables[0].Rows[0]["HPM_POBOX"].ToString();

                //txtArea.Text = ds.Tables[0].Rows[0]["HPM_AREA"].ToString();
                //txtCity.Text = ds.Tables[0].Rows[0]["HPM_CITY"].ToString();




                if (ds.Tables[0].Rows[0].IsNull("HPM_NATIONALITY") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_NATIONALITY"]) != "")
                {
                    for (int intCount = 0; intCount < drpNationality.Items.Count; intCount++)
                    {
                        if (drpNationality.Items[intCount].Value.ToUpper() == Convert.ToString(ds.Tables[0].Rows[0]["HPM_NATIONALITY"]).ToUpper())
                        {
                            drpNationality.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_NATIONALITY"]);
                            goto ForNationality;
                        }
                    }

                }

            ForNationality: ;


                //txtPhone1.Text = ds.Tables[0].Rows[0]["HPM_PHONE1"].ToString();
                //txtMobile1.Text = ds.Tables[0].Rows[0]["HPM_MOBILE"].ToString();
                //txtMobile2.Text = ds.Tables[0].Rows[0]["HPM_PHONE2"].ToString();

                // if (ds.Tables[0].Rows[0].IsNull("HPM_ID_CAPTION") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_ID_CAPTION"]) != "")
                //   lblIDCaption.Text = ds.Tables[0].Rows[0]["HPM_ID_CAPTION"].ToString();
                // txtEmiratesID.Text = ds.Tables[0].Rows[0]["HPM_IQAMA_NO"].ToString();

                //  hidAgeType.Value = Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE_TYPE"]);
                //  hidInsCode.Value = Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_ID"]);
                //  hidInsName.Value = Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_NAME"]);
                ViewState["User_DeptID"] = Convert.ToString(ds.Tables[0].Rows[0]["HPM_DEP_NAME"]);

                txtProviderID.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_ID"]);
                txtProviderName.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_NAME"]);
                txtPolicyNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_POLICY_NO"]);
                txtIDNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_ID_NO"]);

                //txtPolicyType.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_POLICY_TYPE"]);

                //   drpDoctors.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_DR_ID"]);


                //  drpRefDoctor.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_REF_DR_ID"]);

                // GetPatientVisit();


                drpInvType.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_TYPE"]) == "CA" ? "Cash" : "Credit";
            }

        }

        void BindCommonMaster(string strType)
        {

            string Criteria = " 1=1 AND HCM_STATUS='A' ";
            Criteria += "  AND HCM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += "  AND HCM_TYPE='" + strType + "'";
            if (strType == "ResultStatus")
            {
                Criteria += "  AND HCM_CODE IN ('F','C','X') ";
            }

            CommonBAL objCommBal = new CommonBAL();
            DataSet DS = new DataSet();

            DS = objCommBal.fnGetFieldValue("*", "HMS_COMMON_MASTERS", Criteria, "HCM_DESC");

            if (DS.Tables[0].Rows.Count > 0)
            {
                if (strType == "RadDiagnosticService")
                {
                    drpDiagnosticService.DataSource = DS;
                    drpDiagnosticService.DataTextField = "HCM_DESC";
                    drpDiagnosticService.DataValueField = "HCM_CODE";
                    drpDiagnosticService.DataBind();
                }
                if (strType == "ResultStatus")
                {
                    drpResultStatus.DataSource = DS;
                    drpResultStatus.DataTextField = "HCM_DESC";
                    drpResultStatus.DataValueField = "HCM_CODE";
                    drpResultStatus.DataBind();
                }



            }


            if (strType == "RadDiagnosticService")
            {
                drpDiagnosticService.Items.Insert(0, "--- Select ---");
                drpDiagnosticService.Items[0].Value = "";
            }

            if (strType == "ResultStatus")
            {
                drpResultStatus.Items.Insert(0, "--- Select ---");
                drpResultStatus.Items[0].Value = "";
            }


        }


        void PTDtlsClear()
        {

            txtFName.Text = "";
            lblStatus.Text = "";

            txtDOB.Text = "";
            //lblAge.Text = "";

            //   hidAgeType.Value = Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE_TYPE"]);

            txtSex.Text = "";

            //txtAddress.Text = "";
            //txtPoBox.Text = "";

            //txtArea.Text = "";
            //txtCity.Text = "";

            if (drpNationality.Items.Count > 0)
                drpNationality.SelectedIndex = 0;

            //txtPhone1.Text = "";
            //txtMobile1.Text = "";
            //txtMobile2.Text = "";




            //  hidInsName.Value = Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_NAME"]);
            ViewState["User_DeptID"] = "";


            for (int intCount = 0; intCount < drpDoctors.Items.Count; intCount++)
            {
                if (drpDoctors.Items[intCount].Value == Convert.ToString(Session["User_Code"]))
                {
                    drpDoctors.SelectedValue = Convert.ToString(Session["User_Code"]);
                    goto ForDoctors;
                }
            }

        ForDoctors: ;

        }

        void BindXRayMasterGrid()
        {
            DataSet DS = new DataSet();

            string Criteria = "  RXM_STATUS='A'   AND RXM_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "' ";

            if (txtXraySrcDtls.Text.Trim() != "")
            {
                Criteria += " AND (RXM_XRAY_NO like '%" + txtXraySrcDtls.Text.Trim() + "%' OR  RXM_XRAY_CODE like '%" + txtXraySrcDtls.Text.Trim() + "%' OR  RXM_XRAY_DESCRIPTION LIKE '%" + txtXraySrcDtls.Text.Trim() + "%'";
                // Criteria += " OR IAS_MOBILE like '%" + txtSrcDtls.Text.Trim() + "%' OR  IAS_ADMISSION_NO LIKE '%" + txtSrcDtls.Text.Trim() + "%'";

                Criteria += " )";
            }


            objRad = new RadiologyBAL();
            DS = objRad.XrayMasterGet(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                gvXrayMasterPopup.DataSource = DS;
                gvXrayMasterPopup.DataBind();
            }
            else
            {
                gvXrayMasterPopup.DataBind();
            }

        }


        void BindXRayMasterData()
        {
            DataSet DS = new DataSet();
            //RXM_DATA_LOAD_FROM ='WEB'  AND
            string Criteria = "   RXM_STATUS='A' AND RXM_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "' AND RXM_XRAY_CODE='" + txtMasterCode.Text + "'";

            objRad = new RadiologyBAL();
            DS = objRad.XrayMasterGet(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    txtMasterNo.Text = Convert.ToString(DR["RXM_XRAY_NO"]);

                    txtMasterDesc.Text = Convert.ToString(DR["RXM_XRAY_DESCRIPTION"]);

                    txtServCode.Text = Convert.ToString(DR["RXM_SERV_ID"]);
                    txtServName.Text = Convert.ToString(DR["RXM_SERV_DESCRIPTION"]);
                    txtFee.Text = Convert.ToString(DR["RXM_FEE"]);

                    ////if (DR.IsNull("RXM_CPT_CODE") == false && Convert.ToString(DR["RXM_CPT_CODE"]) != "")
                    ////{
                    ////    txtCPTCode.Text = Convert.ToString(DR["RXM_CPT_CODE"]);
                    ////}

                    txtContent.Text = Convert.ToString(DR["RXM_REPORT1"]);// Server.HtmlDecode(Convert.ToString(DR["RXM_REPORT"]));
                    // txtContent.Text = clsWebReport.HTMLFromRtf(Convert.ToString(DR["RXM_REPORT"]));


                }
            }

        }

        void BindScanMasterGrid()
        {
            DataSet DS = new DataSet();
            //RSM_DATA_LOAD_FROM ='WEB' AND 
            string Criteria = " RSM_STATUS='A'  AND RSM_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "' ";
            if (txtScanSrcDtls.Text.Trim() != "")
            {
                Criteria += " AND (RSM_SCAN_NO like '%" + txtScanSrcDtls.Text.Trim() + "%' OR  RSM_SCAN_CODE like '%" + txtScanSrcDtls.Text.Trim() + "%' OR  RSM_SCAN_DESCRIPTION LIKE '%" + txtScanSrcDtls.Text.Trim() + "%'";
                // Criteria += " OR IAS_MOBILE like '%" + txtSrcDtls.Text.Trim() + "%' OR  IAS_ADMISSION_NO LIKE '%" + txtSrcDtls.Text.Trim() + "%'";

                Criteria += " )";
            }



            objRad = new RadiologyBAL();
            DS = objRad.ScanMasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                gvScanMasterPopup.DataSource = DS;
                gvScanMasterPopup.DataBind();
            }
            else
            {
                gvScanMasterPopup.DataBind();
            }
        }


        void BindScanMasterData()
        {
            DataSet DS = new DataSet();
            //RSM_DATA_LOAD_FROM ='WEB'  AND
            string Criteria = "   RSM_STATUS='A' AND RSM_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "' AND RSM_Scan_CODE='" + txtMasterCode.Text + "'";

            objRad = new RadiologyBAL();
            DS = objRad.ScanMasterGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    txtMasterNo.Text = Convert.ToString(DR["RSM_SCAN_NO"]);

                    txtMasterDesc.Text = Convert.ToString(DR["RSM_SCAN_DESCRIPTION"]);


                    txtServCode.Text = Convert.ToString(DR["RSM_SERV_ID"]);
                    txtServName.Text = Convert.ToString(DR["RSM_SERV_DESCRIPTION"]);
                    txtFee.Text = Convert.ToString(DR["RSM_FEE"]);

                    ////if (DR.IsNull("RSM_CPT_CODE") == false && Convert.ToString(DR["RSM_CPT_CODE"]) != "")
                    ////{
                    ////    txtCPTCode.Text = Convert.ToString(DR["RSM_CPT_CODE"]);
                    ////}


                    txtContent.Text = Convert.ToString(DR["RSM_REPORT1"]);// Server.HtmlDecode(Convert.ToString(DR["RSM_REPORT"]));
                }
            }

        }


        void BindWaitngList()
        {

            string strTestType = "";

            strTestType = Convert.ToString(Session["HSOM_RAD_LIST"]);


            DataSet DS = new DataSet();

            LaboratoryBAL objLab = new LaboratoryBAL();

            string Criteria = " 1=1  ";

            string strStartDate = txtSrcFromDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            string strTotDate = txtSrcToDate.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }


            if (strTestType == "EMR")
            {
                Criteria += " AND EPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
                if (txtSrcFromDate.Text != "")
                {
                    Criteria += " AND   CONVERT(datetime,convert(varchar(10),EPM_DATE ,101),101) >= '" + strForStartDate + "'";
                    Criteria += " AND   CONVERT(datetime,convert(varchar(10),EPM_DATE ,101),101) <= '" + strForToDate + "'";
                }


                if (txtFileNo.Text.Trim() != "")
                {
                    Criteria += " AND  EPM_PT_ID LIKE '%" + txtFileNo.Text.Trim() + "%'";
                }

                if (txtFileNo.Text.Trim() != "")
                {
                    Criteria += " AND  EPM_PT_NAME LIKE '%" + txtFName.Text.Trim() + "%'";
                }
            }
            else
            {
                Criteria = " 1=1   ";

                Criteria += " AND HIT_SERV_TYPE='R'  ";


                Criteria += " AND HIM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";



                if (txtSrcFromDate.Text != "")
                {
                    Criteria += " AND   CONVERT(datetime,convert(varchar(10),HIM_DATE,101),101) >= '" + strForStartDate + "'";
                    Criteria += " AND   CONVERT(datetime,convert(varchar(10),HIM_DATE,101),101) <= '" + strForToDate + "'";
                }
                if (txtSrcFileNo.Text.Trim() != "")
                {
                    Criteria += " AND  HIM_PT_ID LIKE '%" + txtSrcFileNo.Text.Trim() + "%'";
                }

                if (txtSrcPTName.Text.Trim() != "")
                {
                    Criteria += " AND  HIM_PT_NAME LIKE '%" + txtSrcPTName.Text.Trim() + "%'";
                }
            }



            string strServiceType = "";

            strServiceType = "R";


            DS = objLab.LabWaitingListShow("Waiting", GlobalValues.FileDescription, strServiceType, Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                gvWaitingList.DataSource = DS;
                gvWaitingList.DataBind();
            }
            else
            {
                gvWaitingList.DataBind();
            }

        }


        void BindRegisterWaiting()
        {

            DataSet DS = new DataSet();

            LaboratoryBAL objLab = new LaboratoryBAL();

            string Criteria = " 1=1  ";

            Criteria += " AND RTRM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            string strStartDate = txtSrcFromDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            string strTotDate = txtSrcToDate.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }



            if (txtSrcFromDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),RTRM_DATE ,101),101) >= '" + strForStartDate + "'";
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),RTRM_DATE ,101),101) <= '" + strForToDate + "'";
            }


            if (txtFileNo.Text.Trim() != "")
            {
                Criteria += " AND  RTRM_PATIENT_ID LIKE '%" + txtSrcFileNo.Text.Trim() + "%'";
            }

            if (txtFileNo.Text.Trim() != "")
            {
                Criteria += " AND  RTRM_PATIENT_NAME LIKE '%" + txtSrcPTName.Text.Trim() + "%'";
            }

            if (drpWaitListStatus.SelectedValue == "Waiting")
            {
                // Criteria += " AND (LTRM_AUTHORIZE_STATUS IS NULL OR  LTRM_AUTHORIZE_STATUS ='' ) ";
                //  Criteria += " AND (RTR_RAD_REQ_NO IS NULL OR  RTR_RAD_REQ_NO ='' ) ";

                Criteria += " AND RTRM_TRANS_NO NOT IN (SELECT ISNULL(RTR_RAD_REQ_NO,'') FROM RAD_TEST_REPORT )";

            }
            else
            {
                // Criteria += " AND (RTR_RAD_REQ_NO IS NOT  NULL AND RTR_RAD_REQ_NO <> '' ) ";
                Criteria += " AND RTRM_TRANS_NO  IN (select RTR_RAD_REQ_NO From RAD_TEST_REPORT )";
            }


            DS = objRad.TestRegisterMasterGet(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                gvRegisterWaiting.DataSource = DS;
                gvRegisterWaiting.DataBind();
            }
            else
            {
                gvRegisterWaiting.DataBind();
            }

        }

        void BindNationality()
        {
            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.Nationality();
            if (ds.Tables[0].Rows.Count > 0)
            {
                drpNationality.DataSource = ds;
                drpNationality.DataValueField = "hnm_name";
                drpNationality.DataTextField = "hnm_name";
                drpNationality.DataBind();



            }
            drpNationality.Items.Insert(0, "Select");
            drpNationality.Items[0].Value = "0";



        }

        void BindDoctor()
        {

            DataSet ds = new DataSet();
            StaffMasterBAL dbo = new StaffMasterBAL();

            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND  HSFM_SF_STATUS='Present'";// AND HSFM_DEPT_ID='RADIOLOGY' 
            Criteria += " and HSFM_DEPT_ID='RADIOLOGY' ";
            ds = dbo.GetStaffMaster(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                drpDoctors.DataSource = ds;
                drpDoctors.DataTextField = "FullName";
                drpDoctors.DataValueField = "HSFM_STAFF_ID";
                drpDoctors.DataBind();

                //drpRefDoctor.DataSource = ds;
                //drpRefDoctor.DataTextField = "FullName";
                //drpRefDoctor.DataValueField = "HSFM_STAFF_ID";
                //drpRefDoctor.DataBind();


                for (int intCount = 0; intCount < drpDoctors.Items.Count; intCount++)
                {
                    if (drpDoctors.Items[intCount].Value == Convert.ToString(Session["User_Code"]))
                    {
                        drpDoctors.SelectedValue = Convert.ToString(Session["User_Code"]);
                        goto ForDoctors;
                    }
                }
            ForDoctors: ;



            }

            drpDoctors.Items.Insert(0, "--- Select ---");
            drpDoctors.Items[0].Value = "0";

            //drpRefDoctor.Items.Insert(0, "--- Select ---");
            //drpRefDoctor.Items[0].Value = "0";
        }

        ////void BindRefDoctor()
        ////{

        ////    DataSet ds = new DataSet();
        ////    dboperations dbo = new dboperations();

        ////    string Criteria = " 1=1 ";// AND HSFM_DEPT_ID='RADIOLOGY' 

        ////    ds = dbo.RefDoctorMasterGet(Criteria);
        ////    if (ds.Tables[0].Rows.Count > 0)
        ////    {


        ////        drpRefDoctor.DataSource = ds;
        ////        drpRefDoctor.DataTextField = "FullName";
        ////        drpRefDoctor.DataValueField = "HRDM_REF_ID";
        ////        drpRefDoctor.DataBind();






        ////    }



        ////    drpRefDoctor.Items.Insert(0, "--- Select ---");
        ////    drpRefDoctor.Items[0].Value = "0";
        ////}
        void BindRadHistoryGrid()
        {
            DataSet DS = new DataSet();


            objRad = new RadiologyBAL();
            string Criteria = " 1=1 ";
            Criteria += " AND RTR_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND ISNULL(RTR_RESULT_STATUS,'') <> 'x'";


            if (txtFileNo.Text.Trim() != "")
            {
                Criteria += " AND RTR_PATIENT_ID='" + txtFileNo.Text.Trim() + "'";
            }

            CommonBAL objCom = new CommonBAL();
            DS = objCom.fnGetFieldValue(" TOP 100 CONVERT(VARCHAR,RTR_DATE,103) as RTR_DATEDesc, *", "RAD_TEST_REPORT", Criteria, "RTR_CREATED_DATE desc ");


            //  DS = objRad.TestReportGet(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                gvRadiologyHistory.DataSource = DS;
                gvRadiologyHistory.DataBind();
            }

        }

        void BindTestReport()
        {
            DataSet DS = new DataSet();


            objRad = new RadiologyBAL();
            string Criteria = " 1=1 ";// AND RTR_DATA_LOAD_FROM='WEB' ";
            Criteria += " AND RTR_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND RTR_TRANS_NO='" + txtTransNo.Text.Trim() + "'";

            DS = objRad.TestReportGet(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    ViewState["NewFlag"] = false;


                    if (DR.IsNull("RTR_TEST_TYPE") == false && Convert.ToString(DR["RTR_TEST_TYPE"]) != "")
                    {
                        for (Int32 i = 0; i < drpTransType.Items.Count; i++)
                        {
                            if (Convert.ToString(DR["RTR_TEST_TYPE"]).ToUpper() == drpTransType.Items[i].Value.ToUpper())
                            {
                                drpTransType.SelectedValue = Convert.ToString(DR["RTR_TEST_TYPE"]);
                                goto EndFor;
                            }

                        }
                    EndFor: ;
                    }


                    txtTransDate.Text = Convert.ToString(DR["RTR_DATEDesc"]);

                    if (DR.IsNull("RTR_DATE_TimeDesc") == false)
                    {
                        string strTimeUp = Convert.ToString(DR["RTR_DATE_TimeDesc"]);
                        string[] arrTimeUp1 = strTimeUp.Split(':');

                        if (arrTimeUp1.Length > 1)
                        {
                            string strHour = arrTimeUp1[0];
                            if (strHour.Length == 1)
                            {
                                strHour = "0" + strHour;
                            }

                            drpTransHour.SelectedValue = strHour;
                            drpTransMin.SelectedValue = arrTimeUp1[1];
                        }

                    }




                    txtReqDate.Text = Convert.ToString(DR["RTR_REQUEST_DATEDesc"]);

                    if (DR.IsNull("RTR_REQUEST_TimeDesc") == false)
                    {
                        string strTimeUp = Convert.ToString(DR["RTR_REQUEST_TimeDesc"]);
                        string[] arrTimeUp1 = strTimeUp.Split(':');

                        if (arrTimeUp1.Length > 1)
                        {
                            string strHour = arrTimeUp1[0];
                            if (strHour.Length == 1)
                            {
                                strHour = "0" + strHour;
                            }

                            drpReqHour.SelectedValue = strHour;
                            drpReqMin.SelectedValue = arrTimeUp1[1];
                        }

                    }




                    txtRadReqNo.Text = Convert.ToString(DR["RTR_RAD_REQ_NO"]);

                    txtObservStart.Text = Convert.ToString(DR["RTR_OBSERV_STARTDesc"]);

                    if (DR.IsNull("RTR_OBSERV_STARTTimeDesc") == false)
                    {
                        string strTimeUp = Convert.ToString(DR["RTR_OBSERV_STARTTimeDesc"]);
                        string[] arrTimeUp1 = strTimeUp.Split(':');

                        if (arrTimeUp1.Length > 1)
                        {
                            string strHour = arrTimeUp1[0];
                            if (strHour.Length == 1)
                            {
                                strHour = "0" + strHour;
                            }

                            drpObservStHour.SelectedValue = strHour;
                            drpObservStMin.SelectedValue = arrTimeUp1[1];
                        }

                    }





                    txtObservEnd.Text = Convert.ToString(DR["RTR_OBSERV_ENDEDesc"]);
                    if (DR.IsNull("RTR_OBSERV_ENDTimeDesc") == false)
                    {
                        string strTimeUp = Convert.ToString(DR["RTR_OBSERV_ENDTimeDesc"]);
                        string[] arrTimeUp1 = strTimeUp.Split(':');

                        if (arrTimeUp1.Length > 1)
                        {
                            string strHour = arrTimeUp1[0];
                            if (strHour.Length == 1)
                            {
                                strHour = "0" + strHour;
                            }

                            drpObservEndHour.SelectedValue = strHour;
                            drpObservEndMin.SelectedValue = arrTimeUp1[1];
                        }

                    }




                    txtObservDate.Text = Convert.ToString(DR["RTR_OBSERV_DATEDesc"]);
                    if (DR.IsNull("RRTR_OBSERV_TimeDesc") == false)
                    {
                        string strTimeUp = Convert.ToString(DR["RRTR_OBSERV_TimeDesc"]);
                        string[] arrTimeUp1 = strTimeUp.Split(':');

                        if (arrTimeUp1.Length > 1)
                        {
                            string strHour = arrTimeUp1[0];
                            if (strHour.Length == 1)
                            {
                                strHour = "0" + strHour;
                            }

                            drpObservHour.SelectedValue = strHour;
                            drpObservMin.SelectedValue = arrTimeUp1[1];
                        }

                    }







                    if (DR.IsNull("RTR_STATUS") == false && Convert.ToString(DR["RTR_STATUS"]) != "")
                    {
                        for (Int32 i = 0; i < drpStatus.Items.Count; i++)
                        {
                            if (Convert.ToString(DR["RTR_STATUS"]).ToUpper() == drpStatus.Items[i].Value.ToUpper())
                            {
                                drpStatus.SelectedValue = Convert.ToString(DR["RTR_STATUS"]);
                                goto EndFor;
                            }

                        }
                    EndFor: ;
                    }




                    txtFileNo.Text = Convert.ToString(DR["RTR_PATIENT_ID"]);
                    txtMasterNo.Text = Convert.ToString(DR["RTR_RAD_NO"]);
                    txtMasterCode.Text = Convert.ToString(DR["RTR_RAD_CODE"]);
                    txtMasterDesc.Text = Convert.ToString(DR["RTR_RAD_DESCRIPTION"]);
                    txtImporession.Text = Convert.ToString(DR["RTR_RAD_IMPRESSION"]);

                    txtContent.Text = Convert.ToString(DR["RTR_REPORT"]);// Server.HtmlDecode(Convert.ToString(DR["RTR_REPORT"]));



                    txtServCode.Text = Convert.ToString(DR["RTR_SERV_ID"]);
                    txtServName.Text = Convert.ToString(DR["RTR_SERV_DESCRIPTION"]);
                    txtFee.Text = Convert.ToString(DR["RTR_FEE"]);
                    txtImporession2.Text = Convert.ToString(DR["RTR_RAD_IMPRESSION2"]);
                    txtCPTCode.Text = Convert.ToString(DR["RTR_CPT_CODE"]);
                    txtInvoiceRefNo.Text = Convert.ToString(DR["RTR_INVOICENO"]);


                    ////if (DR.IsNull("RTR_APPROVAL_STATUS") == false)
                    ////{
                    ////    if (Convert.ToString(DR["RTR_APPROVAL_STATUS"]).ToUpper() == "APPROVED")
                    ////    {
                    ////        btnSave.Visible = false;
                    ////        //btnApproved.Visible = false;
                    ////    }
                    ////}

                    // txtFileNo_TextChanged("txtFileNo", new EventArgs());


                    txtFileNo.Text = Convert.ToString(DR["RTR_PATIENT_ID"]);
                    txtFName.Text = Convert.ToString(DR["RTR_PATIENT_NAME"]);
                    txtAge.Text = Convert.ToString(DR["RTR_PATIENT_AGE"]);
                    if (DR.IsNull("RTR_PATIENT_AGETYPE") == false && Convert.ToString(DR["RTR_PATIENT_AGETYPE"]) != "")
                    {
                        for (Int32 i = 0; i < drpAgeType.Items.Count; i++)
                        {
                            if (Convert.ToString(DR["RTR_PATIENT_AGETYPE"]).ToUpper() == drpAgeType.Items[i].Value.ToUpper())
                            {
                                drpAgeType.SelectedValue = Convert.ToString(DR["RTR_PATIENT_AGETYPE"]);
                                goto EndFor;
                            }

                        }
                    EndFor: ;
                    }

                    txtSex.Text = Convert.ToString(DR["RTR_PATIENT_SEX"]);


                    if (DR.IsNull("RTR_PATIENT_NATIONALITY") == false && Convert.ToString(DR["RTR_PATIENT_NATIONALITY"]) != "")
                    {
                        drpNationality.SelectedValue = Convert.ToString(DR["RTR_PATIENT_NATIONALITY"]);
                    }


                    txtAdminID.Text = Convert.ToString(DR["RTR_ADMN"]);
                    txtWard.Text = Convert.ToString(DR["RTR_WARD"]);
                    txtRoom.Text = Convert.ToString(DR["RTR_ROOM"]);
                    txtBed.Text = Convert.ToString(DR["RTR_BED"]);


                    if (DR.IsNull("RTR_DIAGNOSTIC_SERVICE") == false && Convert.ToString(DR["RTR_DIAGNOSTIC_SERVICE"]) != "")
                    {
                        drpDiagnosticService.SelectedValue = Convert.ToString(DR["RTR_DIAGNOSTIC_SERVICE"]);
                    }


                    if (DR.IsNull("RTR_RESULT_STATUS") == false && Convert.ToString(DR["RTR_RESULT_STATUS"]) != "")
                    {
                        drpResultStatus.SelectedValue = Convert.ToString(DR["RTR_RESULT_STATUS"]);
                    }







                    if (DR.IsNull("RTR_DR_CODE") == false && Convert.ToString(DR["RTR_DR_CODE"]) != "")
                    {
                        for (Int32 i = 0; i < drpDoctors.Items.Count; i++)
                        {
                            if (Convert.ToString(DR["RTR_DR_CODE"]).ToUpper() == drpDoctors.Items[i].Value.ToUpper())
                            {
                                drpDoctors.SelectedValue = Convert.ToString(DR["RTR_DR_CODE"]);
                                goto EndFor;
                            }

                        }
                    EndFor: ;
                    }




                    if (DR.IsNull("RTR_REF_DR") == false && Convert.ToString(DR["RTR_REF_DR"]) != "")
                    {
                        string strRefDrCode = "", strRefDrName = "";

                        strRefDrCode = Convert.ToString(DR["RTR_REF_DR"]);

                        strRefDrName = Convert.ToString(DR["RTR_REF_DR_NAME"]);

                        txtRefDoctorName.Text = strRefDrCode + "~" + strRefDrName;

                    }



                }

                if (drpResultStatus.SelectedValue == "X")
                {
                    lblStatus.Text = "Order Already canceled.";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    drpResultStatus.Attributes.CssStyle.Add("border-color", "red");
                }
            }

        }

        void Clear()
        {
            ViewState["NewFlag"] = true;
            //  drpTransType.SelectedIndex = 1;


            drpStatus.SelectedIndex = 0;
            txtFileNo.Text = "";
            txtMasterNo.Text = "";
            txtMasterCode.Text = "";
            txtMasterDesc.Text = "";
            txtImporession.Text = "";
            txtContent.Text = "";
            txtServCode.Text = "";
            txtServName.Text = "";
            txtFee.Text = "";
            txtImporession2.Text = "";
            txtCPTCode.Text = "";
            txtInvoiceRefNo.Text = "";


            txtAge.Text = "";
            drpAgeType.SelectedIndex = 0;

            txtAge1.Text = "";
            drpAgeType1.SelectedIndex = 0;

            txtWard.Text = "";
            txtRoom.Text = "";
            txtBed.Text = "";
            txtAdminID.Text = "";

            txtRefDoctorName.Text = "";



            btnSave.Visible = true;
            // btnApproved.Visible = true;

            drpInvType.SelectedIndex = 0;
            drpPTType.SelectedIndex = 0;

            txtProviderID.Text = "";
            txtProviderName.Text = "";
            txtPolicyNo.Text = "";
            txtIDNo.Text = "";


            txtRadReqNo.Text = "";

            txtReqDate.Text = "";
            if (drpReqHour.Items.Count > 0)
                drpReqHour.SelectedIndex = 0;

            if (drpReqMin.Items.Count > 0)
                drpReqMin.SelectedIndex = 0;


            txtObservStart.Text = "";
            if (drpObservStHour.Items.Count > 0)
                drpObservStHour.SelectedIndex = 0;

            if (drpObservStMin.Items.Count > 0)
                drpObservStMin.SelectedIndex = 0;


            txtObservEnd.Text = "";
            if (drpObservEndHour.Items.Count > 0)
                drpObservEndHour.SelectedIndex = 0;

            if (drpObservEndMin.Items.Count > 0)
                drpObservEndMin.SelectedIndex = 0;



            txtObservDate.Text = "";
            if (drpObservHour.Items.Count > 0)
                drpObservHour.SelectedIndex = 0;

            if (drpObservMin.Items.Count > 0)
                drpObservMin.SelectedIndex = 0;



            if (drpDiagnosticService.Items.Count > 0)
                drpDiagnosticService.SelectedIndex = 0;

            if (drpResultStatus.Items.Count > 0)
                drpResultStatus.SelectedIndex = 0;



            hidEMRID.Value = "";
            txtChiefComplaints.Text = "";


        }

        void New()
        {
            ViewState["NewFlag"] = true;
            DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
            txtTransDate.Text = strFromDate.ToString("dd/MM/yyyy");
            txtTransNo.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "SCAN");
        }


        private void RunMethodInSeparateThread(Action action)
        {
            var thread = new Thread(new ThreadStart(action));
            thread.Start();
        }

        void BindLoop()
        {
            for (int i = 1; i <= 10; i++)
            {
                // report the progress in percent

                Thread.Sleep(1000);
            }

        }


        void BindPopupMethords()
        {
            RunMethodInSeparateThread(BindXRayMasterGrid);

            RunMethodInSeparateThread(BindScanMasterGrid);

            RunMethodInSeparateThread(BindRadHistoryGrid);
            // RunMethodInSeparateThread(BindLoop);


        }

        void BindInvoice()
        {
            string Criteria = " 1=1  ";

            Criteria += " AND HIM_INVOICE_ID ='" + txtInvoiceRefNo.Text.Trim() + "'";


            DataSet DS = new DataSet();
            clsInvoice objInv = new clsInvoice();
            DS = objInv.InvoiceMasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {


                //NOTE: BELOW CODING COMMENDED BACAUSE REF DOCTOR LOAD FROM REGISTER PAGE NOT FROM INVOICE.
                ////if (DS.Tables[0].Rows[0].IsNull("HIM_ORDERING_DR_CODE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HIM_ORDERING_DR_CODE"]) != "")
                ////{
                ////    string strRefDrCode = "", strRefDrName = "";

                ////    strRefDrCode = Convert.ToString(DS.Tables[0].Rows[0]["HIM_ORDERING_DR_CODE"]);

                ////    strRefDrName = Convert.ToString(DS.Tables[0].Rows[0]["HIM_ORDERING_DR_NAME"]);

                ////    txtRefDoctorName.Text = strRefDrCode + "~" + strRefDrName;

                ////}


                //drpInvType.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HIM_INVOICE_TYPE"]) == "CA" ? "Cash" : "Credit";
                drpPTType.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HIM_TRTNT_TYPE"]);
                hidEMRID.Value = Convert.ToString(DS.Tables[0].Rows[0]["HIM_EMR_ID"]);

                txtProviderID.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_SUB_INS_CODE"]);
                txtProviderName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_INS_NAME"]);
                txtPolicyNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_POLICY_NO"]);
                txtIDNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_ID_NO"]);
                drpInvType.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HIM_INVOICE_TYPE"]);

            }
        }

        void BindEMRDtls()
        {
            if (txtFileNo.Text.Trim() == "" || hidEMRID.Value == "")
            {
                goto FunEnd;
            }
            string Criteria = " 1=1 ";
            Criteria += " AND EPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND EPM_PT_ID='" + txtFileNo.Text.Trim() + "' AND EPM_ID=" + hidEMRID.Value;

            DataSet DS = new DataSet();
            EMR_PTMasterBAL obj = new EMR_PTMasterBAL();
            DS = obj.GetEMR_PTMaster(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                txtChiefComplaints.Text = Convert.ToString(DS.Tables[0].Rows[0]["EPM_CC"]);

            }


        FunEnd: ;
        }


        void AuditLogAdd(string ScreenAction, string Remarks)
        {
            CommonBAL objCom = new CommonBAL();
            objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
            objCom.PT_ID = "";
            objCom.ID = "";
            objCom.ScreenID = "RAD_TRANS";
            objCom.ScreenName = "Radiology Transaction";
            objCom.ScreenType = "TRANSACTION";
            objCom.ScreenAction = ScreenAction;
            objCom.Remarks = Remarks;
            objCom.UserID = Convert.ToString(Session["User_ID"]);
            objCom.UserCategory = Convert.ToString(Session["User_Category"]).ToUpper();

            objCom.EMRAuditLogAdd();


        }

        void SetPermission()
        {
            string strPermission = "0";
            if (Convert.ToString(Session["ROLL_IDS"]) == "" || Convert.ToString(Session["ROLL_IDS"]) == null)
            {
                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../ErrorPage.aspx?MenuName=Radiology");
            }
            string Criteria = " 1=1 AND HRT_SCREEN_ID='RAD_TRANS' ";
            Criteria += " AND  HRT_ROLL_ID IN ( " + Convert.ToString(Session["ROLL_IDS"]) + ")";

            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.RollTransGet(Criteria);


            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {
                btnApproved.Visible = false;
                btnSave.Enabled = false;
                //btnDelete.Enabled = false;
                //btnClear.Enabled = false;

            }

            if (strPermission == "5")
            {
                //  btnDelete.Enabled = false;

            }

            if (strPermission == "7")
            {
                btnSave.Enabled = false;
                btnApproved.Visible = false;

            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {

                Session["ErrorMsg"] = "Permission Denied";
                Response.Redirect("../ErrorPage.aspx?MenuName=Radiology");
            }
        }
        #endregion

        #region AutoCompleteExtender


        [System.Web.Services.WebMethod]
        public static string[] RefGetDoctorName(string prefixText)
        {
            string[] Data = null;

            string criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND  HSFM_SF_STATUS='Present'";
            criteria += " AND HSFM_FNAME + ' ' +isnull(HSFM_MNAME,'') + ' '  + isnull(HSFM_LNAME,'')   like '%" + prefixText + "%' ";
            DataSet ds = new DataSet();
            StaffMasterBAL objStaff = new StaffMasterBAL();
            ds = objStaff.GetStaffMaster(criteria);
            int StaffCount = ds.Tables[0].Rows.Count;


            criteria = " 1=1 ";
            criteria += " AND HRDM_FNAME + ' ' +isnull(HRDM_MNAME,'') + ' '  + isnull(HRDM_LNAME,'')   like '%" + prefixText + "%' ";
            DataSet ds1 = new DataSet();
            dboperations dbo = new dboperations();
            ds1 = dbo.RefDoctorMasterGet(criteria);
            int RefStaffCount = ds1.Tables[0].Rows.Count;

            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[StaffCount + RefStaffCount];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["HSFM_STAFF_ID"]).Trim() + "~" + Convert.ToString(ds.Tables[0].Rows[i]["FullName"]).Trim();
                }

            }


            if (ds1.Tables[0].Rows.Count > 0)
            {
                for (int j = 0; j < ds1.Tables[0].Rows.Count; j++)
                {
                    Data[StaffCount + j] = Convert.ToString(ds1.Tables[0].Rows[j]["HRDM_REF_ID"]).Trim() + "~" + Convert.ToString(ds1.Tables[0].Rows[j]["FullName"]).Trim();
                }

                return Data;
            }
            else
            {
                return Data;
            }

            string[] Data1 = { "" };

            return Data1;
        }

        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }
            lblStatus.Text = "";
            try
            {
                if (!IsPostBack)
                {
                    if (Convert.ToString(Session["HMS_AUTHENTICATION"]) == "1")
                    {
                        if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_ID"]).ToUpper() != "SUPER_ADMIN")
                        {
                            SetPermission();
                        }
                    }
                    AuditLogAdd("OPEN", "Open Radiology Transaction Page");

                    ViewState["NewFlag"] = true;
                    string strTime = "", strAM = "";
                    DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                    txtTransDate.Text = strFromDate.ToString("dd/MM/yyyy");
                    txtSrcFromDate.Text = strFromDate.ToString("dd/MM/yyyy");
                    txtSrcToDate.Text = strFromDate.ToString("dd/MM/yyyy");

                    //  txtReqDate.Text = strFromDate.ToString("dd/MM/yyyy");
                    //  txtObservStart.Text = strFromDate.ToString("dd/MM/yyyy");
                    // txtObservEnd.Text = strFromDate.ToString("dd/MM/yyyy");

                    txtTransNo.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "SCAN");


                    strTime = objCom.fnGetDate("hh:mm");
                    strAM = objCom.fnGetDate("tt");

                    string strHour = Convert.ToString(System.DateTime.Now.Hour);
                    if (Convert.ToInt32(strHour) <= 9)
                    {
                        strHour = "0" + strHour;
                    }
                    string strMin = Convert.ToString(System.DateTime.Now.Minute);
                    if (Convert.ToInt32(strMin) <= 9)
                    {
                        strMin = "0" + strMin;
                    }

                    BindTime12HrsFromDB();

                    drpTransHour.SelectedValue = strHour;
                    drpTransMin.SelectedValue = strMin;

                    //drpReqHour.SelectedValue = strHour;
                    //drpReqMin.SelectedValue = strMin;

                    //drpObservStHour.SelectedValue = strHour;
                    //drpObservStMin.SelectedValue = strMin;

                    //drpObservEndHour.SelectedValue = strHour;
                    //drpObservEndMin.SelectedValue = strMin;

                    ////drpAuthorizHour.SelectedValue = strHour;
                    ////drpAuthorizMin.SelectedValue = strMin;


                    BindXRayMasterGrid();

                    BindScanMasterGrid();
                    BindRadHistoryGrid();

                    BindPopupMethords();

                    BindNationality();
                    BindDoctor();
                    //  BindRefDoctor();

                    BindCommonMaster("RadDiagnosticService");
                    BindCommonMaster("ResultStatus");

                    string PageName = "", RadTestReportID = "";


                    ViewState["TransID"] = Request.QueryString["TransID"];
                    ViewState["PatientID"] = Request.QueryString["PatientID"];
                    ViewState["DoctorID"] = Request.QueryString["DoctorID"];
                    ViewState["TransDate"] = Request.QueryString["TransDate"];
                    ViewState["TransType"] = Request.QueryString["TransType"];
                    ViewState["ServiceID"] = Request.QueryString["ServiceID"];
                    RadTestReportID = Request.QueryString["RadTestReportID"];


                    PageName = (string)Request.QueryString["PageName"];


                    if (PageName == "RadWaitList" || PageName == "MalaffiLog")
                    {
                        if (RadTestReportID != "")
                        {
                            txtTransNo.Text = RadTestReportID;
                            txtTransNo_TextChanged(txtTransNo, new EventArgs());

                        }
                        else
                        {
                            txtFileNo.Text = Convert.ToString(ViewState["PatientID"]);
                            txtFileNo_TextChanged(txtFileNo, new EventArgs());

                            txtCPTCode.Text = Convert.ToString(ViewState["ServiceID"]);



                            if (GlobalValues.FileDescription.ToUpper() == "SMCH" && Convert.ToString(ViewState["TransType"]).ToUpper() == "INVOICE")
                            {
                                txtInvoiceRefNo.Text = Convert.ToString(ViewState["TransID"]);
                                BindInvoice();


                                BindEMRDtls();
                                // BindLabTestDtlsInvoiceWise();

                            }
                            else
                            {
                                // BindLabTestDtlsServiceWise("'" + Convert.ToString(ViewState["ServiceID"]) + "'");
                            }
                            // BindTempTestReportDtls();
                        }
                    }

                    string XrayCode = Convert.ToString(Request.QueryString["XrayCode"]);

                    if (XrayCode != " " && XrayCode != null)
                    {
                        lblMasterCaption.Text = "X-Ray No.";
                        txtMasterCode.Text = Convert.ToString(XrayCode);
                        BindXRayMasterData();


                    }

                    string ScanCode = Convert.ToString(Request.QueryString["ScanCode"]);

                    if (ScanCode != " " && ScanCode != null)
                    {
                        lblMasterCaption.Text = "Scan No.";
                        txtMasterCode.Text = Convert.ToString(ScanCode);
                        BindScanMasterData();


                    }



                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void drpTransType_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtContent.Text = "";
            try
            {
                if (drpTransType.SelectedIndex == 0)
                {
                    // txtTransNo.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "SCAN");
                    lblMasterCaption.Text = "Scan No.";
                }
                else
                {
                    //txtTransNo.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "XRAY");
                    lblMasterCaption.Text = "X-Ray No.";
                }

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "TransType_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            try
            {
                Clear();
                PTDtlsClear();
                BindRadHistoryGrid();
                New();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnNew_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void txtFileNo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                PTDtlsClear();
                PatientDataBind();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "txtFileNo_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void txtTransNo_TextChanged(object sender, EventArgs e)
        {
            Clear();
            PTDtlsClear();
            BindTestReport();
            BindInvoice();
        }

        protected void XrayMasterEdit_Click(object sender, EventArgs e)
        {
            txtContent.Text = "";
            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();

            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;
            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

            Label lblgvXRayNo = (Label)gvScanCard.Cells[0].FindControl("lblgvXRayNo");
            Label lblgvXRayCode = (Label)gvScanCard.Cells[0].FindControl("lblgvXRayCode");
            Label lblgvXRayDesc = (Label)gvScanCard.Cells[0].FindControl("lblgvXRayDesc");
            Label lblgvXRayReport = (Label)gvScanCard.Cells[0].FindControl("lblgvXRayReport");


            txtMasterNo.Text = lblgvXRayNo.Text;
            txtMasterCode.Text = lblgvXRayCode.Text;
            txtMasterDesc.Text = lblgvXRayDesc.Text;
            //txtContent.Text = Server.HtmlDecode(lblgvXRayReport.Text);

            //  txtContent.Text = Server.HtmlDecode(lblgvXRayReport.Text);


            //  txtContent.Text = clsWebReport.ConvertToTxt(lblgvXRayReport.Text);


            BindXRayMasterData();
            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "HideXrayMasterPopup()", true);
        }

        protected void ScanMasterEdit_Click(object sender, EventArgs e)
        {
            txtContent.Text = "";

            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();

            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;
            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

            Label lblgvScanNo = (Label)gvScanCard.Cells[0].FindControl("lblgvScanNo");
            Label lblgvScanCode = (Label)gvScanCard.Cells[0].FindControl("lblgvScanCode");
            Label lblgvScanDesc = (Label)gvScanCard.Cells[0].FindControl("lblgvScanDesc");
            Label lblgvScanReport = (Label)gvScanCard.Cells[0].FindControl("lblgvScanReport");


            txtMasterNo.Text = lblgvScanNo.Text;
            txtMasterCode.Text = lblgvScanCode.Text;
            txtMasterDesc.Text = lblgvScanDesc.Text;
            //txtContent.Text = Server.HtmlDecode(lblgvXRayReport.Text);

            // txtContent.Text = Server.HtmlDecode(lblgvScanReport.Text);

            BindScanMasterData();

            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "HideScanMasterPopup()", true);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                txtTransNo.Attributes.CssStyle.Add("border-color", "black");
                if (txtTransNo.Text.Trim() == "")
                {
                    lblStatus.Text = "Enter Trans No";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    txtTransNo.Attributes.CssStyle.Add("border-color", "red");
                    goto FunEnd;
                }
                txtFileNo.Attributes.CssStyle.Add("border-color", "black");
                if (txtFileNo.Text.Trim() == "")
                {
                    lblStatus.Text = "Enter File No";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    txtFileNo.Attributes.CssStyle.Add("border-color", "red");
                    goto FunEnd;
                }

                txtInvoiceRefNo.Attributes.CssStyle.Add("border-color", "black");
                if (txtInvoiceRefNo.Text.Trim() == "")
                {
                    lblStatus.Text = "Enter Invocie Ref #";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    txtInvoiceRefNo.Attributes.CssStyle.Add("border-color", "red");
                    goto FunEnd;
                }

                string strRefDrCode = "", strRefDrName = "";

                if (Convert.ToString(Session["EMR_ENABLE_MALAFFI"]) == "1")
                {
                    txtMasterCode.Attributes.CssStyle.Add("border-color", "black");
                    if (txtMasterCode.Text.Trim() == "")
                    {
                        lblStatus.Text = "Enter Code";
                        lblStatus.ForeColor = System.Drawing.Color.Red;
                        txtMasterCode.Attributes.CssStyle.Add("border-color", "red");
                        goto FunEnd;
                    }


                    txtMasterDesc.Attributes.CssStyle.Add("border-color", "black");
                    if (txtMasterDesc.Text.Trim() == "")
                    {
                        lblStatus.Text = "Enter Description";
                        lblStatus.ForeColor = System.Drawing.Color.Red;
                        txtMasterDesc.Attributes.CssStyle.Add("border-color", "red");
                        goto FunEnd;
                    }


                    txtMasterDesc.Attributes.CssStyle.Add("border-color", "black");
                    if (txtMasterDesc.Text.Trim() == "")
                    {
                        lblStatus.Text = "Enter Description";
                        lblStatus.ForeColor = System.Drawing.Color.Red;
                        txtMasterDesc.Attributes.CssStyle.Add("border-color", "red");
                        goto FunEnd;
                    }

                    txtCPTCode.Attributes.CssStyle.Add("border-color", "black");
                    if (txtCPTCode.Text.Trim() == "")
                    {
                        lblStatus.Text = "CPT Code is empty";
                        lblStatus.ForeColor = System.Drawing.Color.Red;
                        txtCPTCode.Attributes.CssStyle.Add("border-color", "red");
                        goto FunEnd;
                    }

                    txtContent.Attributes.CssStyle.Add("border-color", "black");
                    if (txtContent.Text.Trim() == "")
                    {
                        lblStatus.Text = "Report Value is empty";
                        lblStatus.ForeColor = System.Drawing.Color.Red;
                        txtContent.Attributes.CssStyle.Add("border-color", "red");
                        goto FunEnd;
                    }


                    txtReqDate.Attributes.CssStyle.Add("border-color", "black");
                    if (txtReqDate.Text.Trim() == "")
                    {
                        lblStatus.Text = "Request Date is empty";
                        lblStatus.ForeColor = System.Drawing.Color.Red;
                        txtReqDate.Attributes.CssStyle.Add("border-color", "red");
                        goto FunEnd;
                    }

                    txtObservStart.Attributes.CssStyle.Add("border-color", "black");
                    if (txtObservStart.Text.Trim() == "")
                    {
                        lblStatus.Text = "Observation Start is empty";
                        lblStatus.ForeColor = System.Drawing.Color.Red;
                        txtObservStart.Attributes.CssStyle.Add("border-color", "red");
                        goto FunEnd;
                    }



                    txtObservEnd.Attributes.CssStyle.Add("border-color", "black");
                    if (txtObservEnd.Text.Trim() == "")
                    {
                        lblStatus.Text = "Observation End is empty";
                        lblStatus.ForeColor = System.Drawing.Color.Red;
                        txtObservEnd.Attributes.CssStyle.Add("border-color", "red");
                        goto FunEnd;
                    }


                    txtObservDate.Attributes.CssStyle.Add("border-color", "black");
                    if (txtObservDate.Text.Trim() == "")
                    {
                        lblStatus.Text = "Observation is empty";
                        lblStatus.ForeColor = System.Drawing.Color.Red;
                        txtObservDate.Attributes.CssStyle.Add("border-color", "red");
                        goto FunEnd;
                    }



                    txtTransDate.Attributes.CssStyle.Add("border-color", "black");
                    if (txtTransDate.Text.Trim() == "")
                    {
                        lblStatus.Text = "Transaction Date is empty";
                        lblStatus.ForeColor = System.Drawing.Color.Red;
                        txtTransDate.Attributes.CssStyle.Add("border-color", "red");
                        goto FunEnd;
                    }


                    drpDoctors.Attributes.CssStyle.Add("border-color", "black");
                    if (drpDoctors.SelectedIndex == 0)
                    {
                        lblStatus.Text = "Doctor Codee is empty";
                        lblStatus.ForeColor = System.Drawing.Color.Red;
                        drpDoctors.Attributes.CssStyle.Add("border-color", "red");
                        goto FunEnd;
                    }





                    if (drpDoctors.SelectedIndex != 0)
                    {

                        string DR_ID = drpDoctors.SelectedValue;

                        string DrFirstName, DrMiddleName, LicenseNo;
                        GetStaffDetails(DR_ID, out   DrFirstName, out   DrMiddleName, out   LicenseNo);



                        if (LicenseNo == "")
                        {
                            lblStatus.Text = "Doctor License No  is empty";
                            lblStatus.ForeColor = System.Drawing.Color.Red;
                            drpDoctors.Attributes.CssStyle.Add("border-color", "red");
                            goto FunEnd;
                        }

                        if (DrFirstName == "")
                        {
                            lblStatus.Text = "Doctor Name  is empty";
                            lblStatus.ForeColor = System.Drawing.Color.Red;
                            drpDoctors.Attributes.CssStyle.Add("border-color", "red");
                            goto FunEnd;
                        }

                    }





                    if (txtRefDoctorName.Text.Trim() != "")
                    {
                        string strDiagNameDtls = txtRefDoctorName.Text.Trim();
                        string[] arrDiagName = strDiagNameDtls.Split('~');

                        if (arrDiagName.Length > 0)
                        {
                            strRefDrCode = arrDiagName[0];

                            if (arrDiagName.Length > 1)
                            {
                                strRefDrName = arrDiagName[1];

                            }
                            else
                            {
                                lblStatus.Text = "Please Enter Ref. Doctor Code";
                                lblStatus.ForeColor = System.Drawing.Color.Red;
                                goto FunEnd;
                            }
                        }
                        else
                        {

                            lblStatus.Text = "Please Enter Ref. Doctor Name";
                            lblStatus.ForeColor = System.Drawing.Color.Red;
                            goto FunEnd;

                        }


                    }

                    if (strRefDrCode == "" || strRefDrName == "")
                    {
                        lblStatus.Text = "Please Enter Ref. Doctor Code and Name";
                        lblStatus.ForeColor = System.Drawing.Color.Red;
                        goto FunEnd;
                    }


                    ////if (drpRefDoctor.SelectedIndex != 0)
                    ////{

                    ////    string RefDR_ID = drpRefDoctor.SelectedValue;

                    ////    string RefDrFirstName, RefDrMiddleName, RefLicenseNo;
                    ////    GetStaffDetails(RefDR_ID, out   RefDrFirstName, out   RefDrMiddleName, out   RefLicenseNo);

                    ////    if (RefLicenseNo == "")
                    ////    {
                    ////        lblStatus.Text = "Ref. Doctor License No  is empty";
                    ////        lblStatus.ForeColor = System.Drawing.Color.Red;
                    ////        drpDoctors.Attributes.CssStyle.Add("border-color", "red");
                    ////        goto FunEnd;
                    ////    }

                    ////    if (RefDrFirstName == "")
                    ////    {
                    ////        lblStatus.Text = "Ref. Doctor Name  is empty";
                    ////        lblStatus.ForeColor = System.Drawing.Color.Red;
                    ////        drpDoctors.Attributes.CssStyle.Add("border-color", "red");
                    ////        goto FunEnd;
                    ////    }

                    ////}

                    drpDiagnosticService.Attributes.CssStyle.Add("border-color", "black");
                    if (drpDiagnosticService.SelectedIndex == 0)
                    {
                        lblStatus.Text = "Please select Diagnostic Service";
                        lblStatus.ForeColor = System.Drawing.Color.Red;
                        drpDiagnosticService.Attributes.CssStyle.Add("border-color", "red");
                        goto FunEnd;
                    }

                    drpResultStatus.Attributes.CssStyle.Add("border-color", "black");
                    if (drpResultStatus.SelectedIndex == 0)
                    {
                        lblStatus.Text = "Please select Result Status";
                        lblStatus.ForeColor = System.Drawing.Color.Red;
                        drpResultStatus.Attributes.CssStyle.Add("border-color", "red");
                        goto FunEnd;
                    }

                    string Criteria = " 1=1 ";
                    Criteria += " AND RTR_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
                    Criteria += " AND RTR_TRANS_NO='" + txtTransNo.Text.Trim() + "'";

                    DataSet DS = new DataSet();
                    CommonBAL objCom = new CommonBAL();
                    DS = objCom.fnGetFieldValue(" TOP 1  *", "RAD_TEST_REPORT", Criteria, "RTR_CREATED_DATE desc ");


                    if (drpResultStatus.SelectedValue == "F")
                    {
                        if (DS.Tables[0].Rows.Count > 0)
                        {

                            lblStatus.Text = "Final results; results stored and verified  - only for New Test Report";
                            lblStatus.ForeColor = System.Drawing.Color.Red;
                            drpResultStatus.Attributes.CssStyle.Add("border-color", "red");
                            goto FunEnd;
                        }
                    }



                    if (drpResultStatus.SelectedValue == "C" || drpResultStatus.SelectedValue == "X")
                    {
                        if (DS.Tables[0].Rows.Count <= 0)
                        {

                            lblStatus.Text = drpResultStatus.SelectedItem.Text + "Final results; results stored and verified - only for update the Test Report";
                            lblStatus.ForeColor = System.Drawing.Color.Red;
                            drpResultStatus.Attributes.CssStyle.Add("border-color", "red");
                            goto FunEnd;
                        }

                    }



                    if (txtReqDate.Text.Trim() != "" && txtObservStart.Text.Trim() != "" && txtObservEnd.Text.Trim() != "" && txtObservDate.Text.Trim() != "" && txtTransDate.Text.Trim() != "")
                    {

                        DateTime dtReqDateTime = DateTime.ParseExact(txtReqDate.Text.Trim() + " " + drpReqHour.SelectedValue + ":" + drpReqMin.SelectedValue + ":00", "dd/MM/yyyy HH:mm:ss", null);



                        DateTime dtObservStart = DateTime.ParseExact(txtObservStart.Text.Trim() + " " + drpObservStHour.SelectedValue + ":" + drpObservStMin.SelectedValue + ":00", "dd/MM/yyyy HH:mm:ss", null);

                        txtReqDate.Attributes.CssStyle.Add("border-color", "black");
                        if (DateTime.Compare(dtObservStart, dtReqDateTime) == -1)
                        {

                            lblStatus.Text = "Observ. Start date Should grater than Request Date";
                            lblStatus.ForeColor = System.Drawing.Color.Red;
                            txtReqDate.Attributes.CssStyle.Add("border-color", "red");
                            goto FunEnd;
                        }




                        DateTime dtObservEnd = DateTime.ParseExact(txtObservEnd.Text.Trim() + " " + drpObservEndHour.SelectedValue + ":" + drpObservEndMin.SelectedValue + ":00", "dd/MM/yyyy HH:mm:ss", null);

                        txtObservEnd.Attributes.CssStyle.Add("border-color", "black");
                        if (DateTime.Compare(dtObservEnd, dtObservStart) == -1)
                        {

                            lblStatus.Text = "Observ. End date Should grater than Start Date";
                            lblStatus.ForeColor = System.Drawing.Color.Red;
                            txtObservEnd.Attributes.CssStyle.Add("border-color", "red");
                            goto FunEnd;
                        }



                        DateTime dtObservDate = DateTime.ParseExact(txtObservDate.Text.Trim() + " " + drpObservHour.SelectedValue + ":" + drpObservMin.SelectedValue + ":00", "dd/MM/yyyy HH:mm:ss", null);



                        txtObservDate.Attributes.CssStyle.Add("border-color", "black");
                        if (DateTime.Compare(dtObservDate, dtObservEnd) == -1)
                        {

                            lblStatus.Text = "Observ. date Should grater than Observ. End date";
                            lblStatus.ForeColor = System.Drawing.Color.Red;
                            txtObservDate.Attributes.CssStyle.Add("border-color", "red");
                            goto FunEnd;
                        }


                        DateTime dtTransDate = DateTime.ParseExact(txtTransDate.Text.Trim() + " " + drpTransHour.SelectedValue + ":" + drpTransMin.SelectedValue + ":00", "dd/MM/yyyy HH:mm:ss", null);



                        txtTransDate.Attributes.CssStyle.Add("border-color", "black");
                        if (DateTime.Compare(dtTransDate, dtObservDate) == -1)
                        {

                            lblStatus.Text = "Trans date Should grater than Observ. Date";
                            lblStatus.ForeColor = System.Drawing.Color.Red;
                            txtTransDate.Attributes.CssStyle.Add("border-color", "red");
                            goto FunEnd;
                        }




                    }


                }



                string tempStr = "";
                tempStr = txtContent.Text;// Server.HtmlEncode(txtContent.Text);


                objRad = new RadiologyBAL();
                objRad.RTR_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objRad.RTR_TRANS_NO = txtTransNo.Text.Trim();
                objRad.RTR_TEST_TYPE = drpTransType.SelectedValue;
                objRad.RTR_DATE = txtTransDate.Text.Trim() + " " + drpTransHour.SelectedValue + ":" + drpTransMin.SelectedValue + ":00";

                if (drpResultStatus.SelectedValue == "X")
                {
                    objRad.RTR_STATUS = "I";
                }
                else
                {
                    objRad.RTR_STATUS = drpStatus.SelectedValue;
                }
                objRad.RTR_PATIENT_ID = txtFileNo.Text.Trim();
                objRad.RTR_RAD_NO = txtMasterNo.Text.Trim();
                objRad.RTR_RAD_CODE = txtMasterCode.Text.Trim();
                objRad.RTR_RAD_DESCRIPTION = txtMasterDesc.Text.Trim();
                objRad.RTR_RAD_IMPRESSION = txtImporession.Text;
                objRad.RTR_REPORT = tempStr;
                objRad.RTR_SERV_ID = txtServCode.Text;
                objRad.RTR_SERV_DESCRIPTION = txtServName.Text;
                objRad.RTR_FEE = txtFee.Text;
                objRad.RTR_RAD_IMPRESSION2 = txtImporession2.Text;
                objRad.RTR_CPT_CODE = txtCPTCode.Text;

                objRad.RTR_PATIENT_NAME = txtFName.Text.Trim();
                objRad.RTR_PATIENT_AGE = txtAge.Text.Trim();
                objRad.RTR_PATIENT_AGETYPE = drpAgeType.SelectedValue;
                objRad.RTR_PATIENT_SEX = txtSex.Text.Trim();
                objRad.RTR_PATIENT_NATIONALITY = drpNationality.SelectedValue;

                objRad.RTR_REF_DR = strRefDrCode;
                objRad.RTR_REF_DR_NAME = strRefDrName;

                objRad.RTR_DR_CODE = drpDoctors.SelectedValue;
                objRad.RTR_DR_NAME = drpDoctors.SelectedItem.Text;

                objRad.RTR_ADMN = txtAdminID.Text.Trim();
                objRad.RTR_WARD = txtWard.Text.Trim();
                objRad.RTR_ROOM = txtRoom.Text.Trim();
                objRad.RTR_BED = txtBed.Text.Trim();

                objRad.RTR_DIAGNOSTIC_SERVICE = drpDiagnosticService.SelectedValue;
                objRad.RTR_RESULT_STATUS = drpResultStatus.SelectedValue;
                objRad.RTR_RAD_REQ_NO = txtRadReqNo.Text.Trim();


                objRad.RTR_REQUEST_DATE = txtReqDate.Text.Trim() + " " + drpReqHour.SelectedValue + ":" + drpReqMin.SelectedValue + ":00";
                objRad.RTR_OBSERV_START = txtObservStart.Text.Trim() + " " + drpObservStHour.SelectedValue + ":" + drpObservStMin.SelectedValue + ":00";
                objRad.RTR_OBSERV_END = txtObservEnd.Text.Trim() + " " + drpObservEndHour.SelectedValue + ":" + drpObservEndMin.SelectedValue + ":00";
                objRad.RTR_OBSERV_DATE = txtObservDate.Text.Trim() + " " + drpObservHour.SelectedValue + ":" + drpObservMin.SelectedValue + ":00";



                objRad.InvoiceRefNo = txtInvoiceRefNo.Text;
                if (Convert.ToBoolean(ViewState["NewFlag"]) == true)
                {
                    objRad.NewFlag = "A";
                }
                else
                {
                    objRad.NewFlag = "M";
                }

                // if (drpTransType.SelectedIndex == 0)
                // {
                objRad.SCRN_ID = "SCAN";
                //}
                //else
                //{
                //    objRad.SCRN_ID = "XRAY";
                //}
                objRad.UserID = Convert.ToString(Session["User_ID"]);
                objRad.TestReportAdd();

                if (txtInvoiceRefNo.Text.Trim() != "")
                {
                    string Criteria = " HIM_INVOICE_ID='" + txtInvoiceRefNo.Text.Trim() + "'";
                    string FieldNameWithValues = " HIM_RAD_DONE = 'Y' ";
                    objCom = new CommonBAL();
                    objCom.fnUpdateTableData(FieldNameWithValues, "HMS_INVOICE_MASTER", Criteria);


                }
                TextFileWriting("Radiology btnSave_Click 1");
                if (Convert.ToBoolean(ViewState["NewFlag"]) == false)
                {
                    AuditLogAdd("MODIFY", "Modify Existing entry in the Radiology Transaction, Transaction ID is " + txtTransNo.Text.Trim());
                }
                else
                {
                    AuditLogAdd("ADD", "Add new entry in the Radiology Transaction, Transaction ID is " + txtTransNo.Text.Trim());
                }
                TextFileWriting("Radiology btnSave_Click 2");

                if (Convert.ToString(Session["EMR_ENABLE_MALAFFI"]) == "1")
                {
                    TextFileWriting("Radiology btnSave_Click 3");

                    objCom = new CommonBAL();
                    DataSet DSMalaffi = new DataSet();
                    string Criteria1 = " HMHM_UPLOAD_STATUS ='PENDING' ";
                    Criteria1 += " AND HMHM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HMHM_PT_ID='" + txtFileNo.Text.Trim() + "'";
                    Criteria1 += " AND HMHM_RAD_TEST_REPORT_ID='" + txtTransNo.Text + "'";
                    Criteria1 += " AND HMHM_MESSAGE_TYPE='RESULT' and HMHM_MESSAGE_CODE='ORU-R01'";

                    DSMalaffi = objCom.fnGetFieldValue(" TOP 1 *", "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria1, "");

                    if (DSMalaffi.Tables[0].Rows.Count > 0)
                    {
                        TextFileWriting("Malaffi table already this data available Report ID =" + txtTransNo.Text);
                        goto MalafffiSaveEnd;
                    }


                    HL7MessageGenerator objHL7Mess = new HL7MessageGenerator();
                    objHL7Mess.PT_ID = txtFileNo.Text.Trim();
                    objHL7Mess.EMR_ID = "";
                    objHL7Mess.VISIT_ID = "";   //Convert.ToString(DS.Tables[0].Rows[0]["HPV_SEQNO"]);
                    objHL7Mess.RAD_TEST_REPORT_ID = txtTransNo.Text;

                    objHL7Mess.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                    objHL7Mess.BRANCH_NAME = Convert.ToString(Session["Branch_Name"]);
                    objHL7Mess.PROVIDER_ID = Convert.ToString(Session["Branch_ProviderID"]);


                    objHL7Mess.MESSAGE_TYPE = "RESULT";
                    objHL7Mess.UPLOAD_STATUS = "PENDING";
                    objHL7Mess.MessageCode = "ORU-R01";

                    string MessageDesc = "Add Result ORU Rad";
                    if (drpResultStatus.SelectedValue == "F")
                    {
                        MessageDesc = "Add Result ORU Rad";
                    }
                    else if (drpResultStatus.SelectedValue == "C")
                    {
                        MessageDesc = "Update Result ORU Rad";
                    }
                    else if (drpResultStatus.SelectedValue == "X")
                    {
                        MessageDesc = "Delete Result ORU Rad";
                    }

                    objHL7Mess.MessageDesc = MessageDesc;

                    objHL7Mess.UserID = Convert.ToString(Session["User_ID"]);
                    objHL7Mess.MalaffiHL7MessageMasterAdd();
                    TextFileWriting("Radiology btnSave_Click 4");
                // Boolean IsError = false;
                // IsError = objHL7Mess.RadMessageGenerate_ORU_R01();
                MalafffiSaveEnd: ;

                }
                TextFileWriting("Radiology btnSave_Click 5");



                string strTestType = Convert.ToString(Session["HSOM_RAD_LIST"]);
                if (strTestType == "EMR")
                {
                    //string FieldNameWithValues = " Completed='LAB_REGISTERED' , EPL_DATE=GETDATE() ";
                    //string Criteria1 = " Completed IS NULL AND EPL_ID ='" + lblID.Text.Trim() + "' AND EPL_LAB_CODE='" + lblServiceID.Text.Trim() + "'";
                    //objCom.fnUpdateTableData(FieldNameWithValues, "EMR_PT_LABORATORY", Criteria1);
                }
                else
                {

                    string FieldNameWithValues = " HIT_RAD_STATUS='RAD_TEST_COMPLETED'  ";
                    string Criteria1 = " HIT_INVOICE_ID ='" + txtInvoiceRefNo.Text.Trim() + "' AND HIT_SERV_CODE='" + txtCPTCode.Text.Trim() + "'";
                    objCom.fnUpdateTableData(FieldNameWithValues, "HMS_INVOICE_TRANSACTION", Criteria1);

                    // LabTestRegisterTimLog(System.DateTime.Now.ToString() + "      ---HMS_INVOICE_TRANSACTION Update  completed");
                }
                TextFileWriting("Radiology btnSave_Click 6");

                Clear();
                PTDtlsClear();
                New();

                lblStatus.Text = "";
                // txtTransNo.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "SCAN");
                lblStatus.Text = "Data  Saved";
                lblStatus.ForeColor = System.Drawing.Color.Green;




            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "Radiology_Transaction_btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }




        string GetCommnMasterCode(string Name, string Type)
        {
            string strSpecialtyCode = "";
            DataSet DS = new DataSet();

            CommonBAL objCom = new CommonBAL();

            string Criteria = " HCM_TYPE='" + Type + "' AND HCM_DESC='" + Name + "'";



            DS = objCom.fnGetFieldValue("*", "HMS_COMMON_MASTERS", Criteria, "");

            if (DS.Tables[0].Rows.Count > 0)
            {
                strSpecialtyCode = Convert.ToString(DS.Tables[0].Rows[0]["HCM_CODE"]);
            }

            return strSpecialtyCode;
        }

        string GetNationalityCode(string strName)
        {
            string strCode = "";
            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();
            string Criteria = "HNM_NAME='" + strName + "'";
            DS = objCom.fnGetFieldValue("HNM_MALAFFI_CODE", "HMS_NATIONALITY_MASTER", Criteria, "");
            if (DS.Tables[0].Rows.Count > 0)
            {
                strCode = Convert.ToString(DS.Tables[0].Rows[0]["HNM_MALAFFI_CODE"]);
            }

            return strCode;
        }

        string GetCountryCode(string strName)
        {
            string strCode = "";
            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();
            string Criteria = "HCM_NAME='" + strName + "'";
            DS = objCom.fnGetFieldValue("HCM_MALAFFI_CODE", "HMS_COUNTRY_MASTER", Criteria, "");
            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("HCM_MALAFFI_CODE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HCM_MALAFFI_CODE"]) != "")
                {
                    strCode = Convert.ToString(DS.Tables[0].Rows[0]["HCM_MALAFFI_CODE"]).Trim();
                }
            }

            return strCode;
        }

        string GetKinCode(string strName)
        {
            string strCode = "";
            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();
            string Criteria = "HCM_TYPE='Relationship' and HCM_DESC='" + strName + "'";
            DS = objCom.fnGetFieldValue("HCM_CODE", "HMS_COMMON_MASTERS", Criteria, "");
            if (DS.Tables[0].Rows.Count > 0)
            {
                strCode = Convert.ToString(DS.Tables[0].Rows[0]["HCM_CODE"]);
            }

            return strCode;
        }

        void GetStaffDetails(string StaffCode, out string DrFirstName, out string DrMiddleName, out string LicenseNo)
        {
            DrFirstName = "";
            DrMiddleName = "";
            LicenseNo = "";
            DataSet DS = new DataSet();

            CommonBAL objCom = new CommonBAL();

            string Criteria = "1=1 AND HSFM_STAFF_ID='" + StaffCode + "'";



            DS = objCom.fnGetFieldValue("*", "HMS_STAFF_MASTER", Criteria, "");

            if (DS.Tables[0].Rows.Count > 0)
            {
                DrFirstName = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_FNAME"]);
                DrMiddleName = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_MNAME"]);
                LicenseNo = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_MOH_NO"]);
                LicenseNo = Convert.ToString(DS.Tables[0].Rows[0]["HSFM_MOH_NO"]);
            }


        }

        void GetCompanyDtls(string CompanyID, out string PayerID)
        {
            PayerID = "";
            DataSet DS = new DataSet();

            CommonBAL objCom = new CommonBAL();

            string Criteria = "1=1 AND HCM_PAYERID   NOT LIKE '@%' AND HCM_COMP_ID='" + CompanyID + "'";


            DS = objCom.fnGetFieldValue(" TOP 1 *", "HMS_COMPANY_MASTER", Criteria, "");

            if (DS.Tables[0].Rows.Count > 0)
            {
                PayerID = Convert.ToString(DS.Tables[0].Rows[0]["HCM_PAYERID"]);

            }
        }


        Boolean CheckPatientDetails(string PT_ID, out string ErrorMessagePTDtls)
        {
            ErrorMessagePTDtls = "";
            Boolean IsNoError = true;

            CommonBAL objCom = new CommonBAL();

            DataSet DS = new DataSet();
            string Criteria = "HPM_PT_ID='" + PT_ID + "'";

            DS = objCom.fnGetFieldValue("TOP 1 *, CONVERT(VARCHAR, HPM_DOB,103) AS HPM_DOBDesc", "HMS_PATIENT_MASTER", Criteria, "");

            if (DS.Tables[0].Rows.Count <= 0)
            {

                ErrorMessagePTDtls += "Patient Details is empty ,  ";

                IsNoError = false;


            }



            foreach (DataRow DR in DS.Tables[0].Rows)
            {


                if (DS.Tables[0].Rows[0].IsNull("HPM_PT_ID") == true || Convert.ToString(DS.Tables[0].Rows[0]["HPM_PT_ID"]) == "")
                {
                    ErrorMessagePTDtls += " Patient ID is empty ,  ";

                    IsNoError = false;
                }

                if (DS.Tables[0].Rows[0].IsNull("HPM_PT_FNAME") == true || Convert.ToString(DS.Tables[0].Rows[0]["HPM_PT_FNAME"]) == "")
                {
                    ErrorMessagePTDtls += " Patient First Name is empty ,  ";

                    IsNoError = false;
                }

                if (DS.Tables[0].Rows[0].IsNull("HPM_PT_LNAME") == true || Convert.ToString(DS.Tables[0].Rows[0]["HPM_PT_LNAME"]) == "")
                {
                    ErrorMessagePTDtls += "Patient Family Name is empty,  ";

                    IsNoError = false;
                }


                if (DS.Tables[0].Rows[0].IsNull("HPM_DOB") == true || Convert.ToString(DS.Tables[0].Rows[0]["HPM_DOB"]) == "")
                {
                    ErrorMessagePTDtls += "Patient DOB is empty,  ";

                    IsNoError = false;
                }

                if (DS.Tables[0].Rows[0].IsNull("HPM_SEX") == true || Convert.ToString(DS.Tables[0].Rows[0]["HPM_SEX"]) == "")
                {
                    ErrorMessagePTDtls += "Patient Gender is empty,  ";

                    IsNoError = false;
                }


                if (DS.Tables[0].Rows[0].IsNull("HPM_COUNTRY") == true || Convert.ToString(DS.Tables[0].Rows[0]["HPM_COUNTRY"]) == "")
                {
                    ErrorMessagePTDtls += "Patient Country is empty,  ";

                    IsNoError = false;
                }


                string strCountryCode = GetCountryCode(Convert.ToString(DR["HPM_COUNTRY"]));

                if (strCountryCode == "")
                {
                    ErrorMessagePTDtls += "Patient Country Code is empty,  ";

                    IsNoError = false;
                }

                if (DS.Tables[0].Rows[0].IsNull("HPM_ADDR") == true || Convert.ToString(DS.Tables[0].Rows[0]["HPM_ADDR"]) == "")
                {
                    ErrorMessagePTDtls += "Patient Address is empty,  ";

                    IsNoError = false;
                }

                if (DS.Tables[0].Rows[0].IsNull("HPM_CITY") == true || Convert.ToString(DS.Tables[0].Rows[0]["HPM_CITY"]) == "")
                {
                    ErrorMessagePTDtls += "Patient City is empty,  ";

                    IsNoError = false;
                }

                if (DS.Tables[0].Rows[0].IsNull("HPM_EMIRATES_CODE") == true || Convert.ToString(DS.Tables[0].Rows[0]["HPM_EMIRATES_CODE"]) == "")
                {
                    ErrorMessagePTDtls += "Patient Emirates Code is empty,  ";

                    IsNoError = false;
                }


                //if (DS.Tables[0].Rows[0].IsNull("HPM_PHONE1") == true || Convert.ToString(DS.Tables[0].Rows[0]["HPM_PHONE1"]) == "")
                //{
                //    ErrorMessagePTDtls += "Patient Home Phone is empty,  ";

                //    IsNoError = false;
                //}

                if (DS.Tables[0].Rows[0].IsNull("HPM_MOBILE") == true || Convert.ToString(DS.Tables[0].Rows[0]["HPM_MOBILE"]) == "")
                {
                    ErrorMessagePTDtls += "Patient Mobile No is empty,  ";

                    IsNoError = false;
                }


                if (DS.Tables[0].Rows[0].IsNull("HPM_MARITAL") == true || Convert.ToString(DS.Tables[0].Rows[0]["HPM_MARITAL"]) == "")
                {
                    ErrorMessagePTDtls += "Patient Marital Status is empty,  ";

                    IsNoError = false;
                }


                if (DS.Tables[0].Rows[0].IsNull("HPM_RELIGION_CODE") == true || Convert.ToString(DS.Tables[0].Rows[0]["HPM_RELIGION_CODE"]) == "")
                {
                    ErrorMessagePTDtls += "Patient Religion is empty,  ";

                    IsNoError = false;
                }

                if (DS.Tables[0].Rows[0].IsNull("HPM_IQAMA_NO") == true || Convert.ToString(DS.Tables[0].Rows[0]["HPM_IQAMA_NO"]) == "")
                {
                    ErrorMessagePTDtls += "Patient Emirates ID is empty,  ";

                    IsNoError = false;
                }

                if (DS.Tables[0].Rows[0].IsNull("HPM_NATIONALITY") == true || Convert.ToString(DS.Tables[0].Rows[0]["HPM_NATIONALITY"]) == "")
                {
                    ErrorMessagePTDtls += "Patient Nationality is empty,  ";

                    IsNoError = false;
                }



                if (DR.IsNull("HPM_KIN_NAME") == false && Convert.ToString(DR["HPM_KIN_NAME"]) != "")
                {


                    if (DS.Tables[0].Rows[0].IsNull("HPM_KIN_RELATION") == true || Convert.ToString(DS.Tables[0].Rows[0]["HPM_KIN_RELATION"]) == "")
                    {
                        ErrorMessagePTDtls += "Patient Kin Relation is empty,  ";

                        IsNoError = false;
                    }

                    if (DS.Tables[0].Rows[0].IsNull("HPM_KIN_MOBILE") == true || Convert.ToString(DS.Tables[0].Rows[0]["HPM_KIN_MOBILE"]) == "")
                    {
                        ErrorMessagePTDtls += "Patient Kin Mobile is empty,  ";

                        IsNoError = false;
                    }

                }



                //if (DS.Tables[0].Rows[0].IsNull("HPM_KNOW_FROM") == true || Convert.ToString(DS.Tables[0].Rows[0]["HPM_KNOW_FROM"]) == "")
                //{
                //    ErrorMessagePTDtls += "Patient Know From  is empty,  ";

                //    IsNoError = false;
                //}



                string PT_TYPE = "", COMP_ID = "";


                if (DR.IsNull("HPM_PT_TYPE") == false && Convert.ToString(DR["HPM_PT_TYPE"]) != "")
                {
                    PT_TYPE = Convert.ToString(DR["HPM_PT_TYPE"]).ToUpper();
                }
                else
                {
                    ErrorMessagePTDtls += "Patient Type  is empty,  ";
                    IsNoError = false;
                }

                if (PT_TYPE != "")
                {

                    if (PT_TYPE.ToUpper() == "CR" || PT_TYPE.ToUpper() == "CREDIT")
                    {

                        if (DR.IsNull("HPM_INS_COMP_ID") == false && Convert.ToString(DR["HPM_INS_COMP_ID"]) != "")
                        {
                            COMP_ID = Convert.ToString(DR["HPM_INS_COMP_ID"]).ToUpper();
                        }


                        if (DR.IsNull("HPM_POLICY_NO") == true && Convert.ToString(DR["HPM_POLICY_NO"]) == "")
                        {
                            ErrorMessagePTDtls += "Patient Policy No is empty,  ";

                            IsNoError = false;
                        }


                        string PayerID = "";
                        GetCompanyDtls(COMP_ID, out PayerID);

                        if (PayerID == "")
                        {
                            ErrorMessagePTDtls += " Company Payer ID is empty ,  ";

                            IsNoError = false;
                        }

                    }
                }


            }

            return IsNoError;

        }


        Boolean CheckLabTestReport(string PT_ID, out string ErrorMessageTestReport)
        {
            ErrorMessageTestReport = "";
            Boolean IsNoError = true;

            string Criteria = " 1=1 ";

            Criteria += " AND  RTR_PATIENT_ID = '" + PT_ID + "' AND RTR_TRANS_NO='" + txtTransNo.Text.Trim() + "'";

            DataSet DS = new DataSet();


            DS = objCom.fnGetFieldValue("*", "RAD_TEST_REPORT", Criteria, "");

            if (DS.Tables[0].Rows.Count > 0)
            {

                if (DS.Tables[0].Rows[0].IsNull("RTR_RAD_CODE") == true || Convert.ToString(DS.Tables[0].Rows[0]["RTR_RAD_CODE"]) == "")
                {
                    ErrorMessageTestReport += " Code is empty ,  ";

                    IsNoError = false;
                }

                if (DS.Tables[0].Rows[0].IsNull("RTR_RAD_DESCRIPTION") == true || Convert.ToString(DS.Tables[0].Rows[0]["RTR_RAD_DESCRIPTION"]) == "")
                {
                    ErrorMessageTestReport += "Description is empty,  ";

                    IsNoError = false;
                }

                if (DS.Tables[0].Rows[0].IsNull("RTR_SERV_ID") == true || Convert.ToString(DS.Tables[0].Rows[0]["RTR_SERV_ID"]) == "")
                {
                    ErrorMessageTestReport += "CPT Code is empty,  ";

                    IsNoError = false;
                }

                if (DS.Tables[0].Rows[0].IsNull("RTR_REPORT") == true || Convert.ToString(DS.Tables[0].Rows[0]["RTR_REPORT"]) == "")
                {
                    ErrorMessageTestReport += "Report Value is empty,  ";

                    IsNoError = false;
                }

                if (DS.Tables[0].Rows[0].IsNull("RTR_REQUEST_DATE") == true || Convert.ToString(DS.Tables[0].Rows[0]["RTR_REQUEST_DATE"]) == "")
                {
                    ErrorMessageTestReport += "Request Date is empty,  ";

                    IsNoError = false;
                }

                if (DS.Tables[0].Rows[0].IsNull("RTR_OBSERV_START") == true || Convert.ToString(DS.Tables[0].Rows[0]["RTR_OBSERV_START"]) == "")
                {
                    ErrorMessageTestReport += "Observation Start Date is empty,  ";

                    IsNoError = false;
                }

                if (DS.Tables[0].Rows[0].IsNull("RTR_OBSERV_END") == true || Convert.ToString(DS.Tables[0].Rows[0]["RTR_OBSERV_END"]) == "")
                {
                    ErrorMessageTestReport += "Observation End Date is empty,  ";

                    IsNoError = false;
                }


                if (DS.Tables[0].Rows[0].IsNull("RTR_OBSERV_DATE") == true || Convert.ToString(DS.Tables[0].Rows[0]["RTR_OBSERV_DATE"]) == "")
                {
                    ErrorMessageTestReport += "Observation  Date is empty,  ";

                    IsNoError = false;
                }




                if (DS.Tables[0].Rows[0].IsNull("RTR_DATE") == true || Convert.ToString(DS.Tables[0].Rows[0]["RTR_DATE"]) == "")
                {
                    ErrorMessageTestReport += "Transaction Date is empty,  ";

                    IsNoError = false;
                }


                ////if (DS.Tables[0].Rows[0].IsNull("RTR_AUTHORIZE_DATE") == true || Convert.ToString(DS.Tables[0].Rows[0]["RTR_AUTHORIZE_DATE"]) == "")
                ////{
                ////    ErrorMessageTestReport += "Authorize Date is empty,  ";

                ////    IsNoError = false;
                ////}

                if (DS.Tables[0].Rows[0].IsNull("RTR_DR_CODE") == true || Convert.ToString(DS.Tables[0].Rows[0]["RTR_DR_CODE"]) == "" || Convert.ToString(DS.Tables[0].Rows[0]["RTR_DR_CODE"]) == "0")
                {
                    ErrorMessageTestReport += "Doctor Code is empty,  ";

                    IsNoError = false;
                }

                if (DS.Tables[0].Rows[0].IsNull("RTR_DR_CODE") == false || Convert.ToString(DS.Tables[0].Rows[0]["RTR_DR_CODE"]) != "")
                {

                    string DR_ID = Convert.ToString(DS.Tables[0].Rows[0]["RTR_DR_CODE"]).Trim();

                    string DrFirstName, DrMiddleName, LicenseNo;
                    GetStaffDetails(DR_ID, out   DrFirstName, out   DrMiddleName, out   LicenseNo);



                    if (LicenseNo == "")
                    {
                        ErrorMessageTestReport += "Doctor License No is empty,  ";

                        IsNoError = false;
                    }

                    if (DrFirstName == "")
                    {
                        ErrorMessageTestReport += "Doctor Name is empty,  ";

                        IsNoError = false;
                    }

                }





                if (DS.Tables[0].Rows[0].IsNull("RTR_REF_DR") == true || Convert.ToString(DS.Tables[0].Rows[0]["RTR_REF_DR"]) == "" || Convert.ToString(DS.Tables[0].Rows[0]["RTR_REF_DR"]) == "0")
                {
                    ErrorMessageTestReport += "Ref. Doctor Code is empty,  ";

                    IsNoError = false;
                }



                if (DS.Tables[0].Rows[0].IsNull("RTR_REF_DR") == false || Convert.ToString(DS.Tables[0].Rows[0]["RTR_REF_DR"]) != "")
                {

                    string RefDR_ID = Convert.ToString(DS.Tables[0].Rows[0]["RTR_REF_DR"]).Trim();

                    string RefDrFirstName, RefDrMiddleName, RefLicenseNo;
                    GetStaffDetails(RefDR_ID, out   RefDrFirstName, out   RefDrMiddleName, out   RefLicenseNo);



                    if (RefLicenseNo == "")
                    {
                        ErrorMessageTestReport += "Ref Doctor License No is empty,  ";

                        IsNoError = false;
                    }

                    if (RefDrFirstName == "")
                    {
                        ErrorMessageTestReport += "Ref Doctor Name is empty,  ";

                        IsNoError = false;
                    }
                }


            }
            else
            {

                ErrorMessageTestReport += "Test Report Details is empty ,  ";

                IsNoError = false;


            }
            return IsNoError;
        }


        Boolean ValidateNL7Message(string PT_ID)
        {
            Boolean IsNoError = true;
            Boolean IsNoErrorPTDtls = true, IsNoErrorTestReport = true;

            string ErrorMessagePTDtls = "", ErrorMessagePTDVisitDls = "", ErrorMessageTestReport = "", ErrorMessageTestReportDtls = "";

            IsNoErrorPTDtls = CheckPatientDetails(PT_ID, out ErrorMessagePTDtls);

            // IsNoErrorPTDVisitDls = CheckPatientVisitDtls(PT_ID, VISIT_ID, out ErrorMessagePTDVisitDls);

            IsNoErrorTestReport = CheckLabTestReport(PT_ID, out ErrorMessageTestReport);



            if (IsNoErrorPTDtls == false || IsNoErrorTestReport == false)  ///|| IsNoErrorPTDVisitDls == false
            {

                lblStatus.Text = ErrorMessagePTDtls + ErrorMessagePTDVisitDls + ErrorMessageTestReport + ErrorMessageTestReportDtls;
                lblStatus.ForeColor = System.Drawing.Color.Red;


                IsNoError = false;
            }
            return IsNoError;
        }

        protected void btnApproved_Click(object sender, EventArgs e)
        {
            try
            {
                objCom = new CommonBAL();

                string FieldNameWithValues = " RTR_APPROVAL_STATUS='APPROVED' ";
                // FieldNameWithValues  +=",RTR_AUTHORIZE_DATE=CONVERT(DATETIME,'"+ txtAuthorizDate.Text.Trim() + " " + drpAuthorizHour.SelectedValue + ":" + drpAuthorizMin.SelectedValue + ":00',103)";
                FieldNameWithValues += ",RTR_AUTHORIZE_DATE=GETDATE()";

                string Criteria = "  RTR_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND RTR_TRANS_NO='" + txtTransNo.Text.Trim() + "'";

                objCom.fnUpdateTableData(FieldNameWithValues, "RAD_TEST_REPORT", Criteria);


                //DataSet DS = new DataSet();
                //objCom = new CommonBAL();
                //string Criteria1 = "HPV_PT_ID='" + txtFileNo.Text.Trim() + "' AND HPV_DR_ID='" + drpDoctors.SelectedValue + "'";
                //DS = objCom.fnGetFieldValue(" TOP 1  HPV_SEQNO ", "HMS_PATIENT_VISIT", Criteria1, " HPV_SEQNO DESC");

                //if (DS.Tables[0].Rows.Count > 0)
                //{

                ////if (ValidateNL7Message(txtFileNo.Text.Trim()) == false)
                ////{


                ////    goto FunEnd;
                ////}

                ////    HL7MessageGenerator objHL7Mess = new HL7MessageGenerator();
                ////    objHL7Mess.PT_ID = txtFileNo.Text.Trim();
                ////    objHL7Mess.EMR_ID = "";
                ////    objHL7Mess.VISIT_ID = "";   //Convert.ToString(DS.Tables[0].Rows[0]["HPV_SEQNO"]);
                ////    objHL7Mess.RAD_TEST_REPORT_ID = txtTransNo.Text;

                ////    objHL7Mess.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                ////    objHL7Mess.BRANCH_NAME = Convert.ToString(Session["Branch_Name"]);
                ////    objHL7Mess.PROVIDER_ID = Convert.ToString(Session["Branch_ProviderID"]);
                ////    objHL7Mess.UserID = Convert.ToString(Session["User_ID"]);

                ////    Boolean IsError = false;
                //////  //  IsError = objHL7Mess.RadMessageGenerate_ORU_R01();

                // }


                Clear();
                PTDtlsClear();

                txtTransNo.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "SCAN");


                lblStatus.Text = "Transaction  Approved";
                lblStatus.ForeColor = System.Drawing.Color.Green;

            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnApproved_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnReport_Click(object sender, EventArgs e)
        {
            if (drpResultStatus.SelectedValue == "X")
            {
                lblStatus.Text = "Order Already canceled.";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                drpResultStatus.Attributes.CssStyle.Add("border-color", "red");
                goto FunEnd;
            }

            string ReportName = "RadiologyReportWeb.rpt";

            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();

            string Criteria = "  RTR_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "' AND  RTR_TRANS_NO='" + txtTransNo.Text.Trim() + "'";
            Criteria += " AND ISNULL(RTR_DATA_LOAD_FROM,'') <> 'WEB' ";

            DS = objCom.fnGetFieldValue("TOP 1 RTR_TRANS_NO", "RAD_TEST_REPORT", Criteria, " RTR_TRANS_NO ");

            if (DS.Tables[0].Rows.Count > 0)
            {

                ReportName = "RadiologyReport.rpt";

            }


            ScriptManager.RegisterStartupScript(this, this.GetType(), "RadResult", "RadResultReport('" + ReportName + "','" + txtTransNo.Text.Trim() + "');", true);

            //   string rptcall = "../WebReport/RadReportAuthorized.aspx?PatientID=" + txtFileNo.Text + "&BranchID=" + Convert.ToString(Session["Branch_ID"]) + "&TRANS_NO=" + txtTransNo.Text.Trim();// txtCompany.Text;
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "RadResult", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1');", true);

            FunEnd: ;
        }

        protected void RadHistoryEdit_Click(object sender, EventArgs e)
        {
            txtContent.Text = "";

            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();

            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;
            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

            Label lblgvRadHistTransNo = (Label)gvScanCard.Cells[0].FindControl("lblgvRadHistTransNo");



            txtTransNo.Text = lblgvRadHistTransNo.Text;
            BindTestReport();


            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "HideRadHistoryPopup()", true);
        }

        protected void RadReport_Click(object sender, EventArgs e)
        {
            txtContent.Text = "";

            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();

            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;
            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

            Label lblgvRadHistTransNo = (Label)gvScanCard.Cells[0].FindControl("lblgvRadHistTransNo");


            string ReportName = "RadiologyReportWeb.rpt";

            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();

            string Criteria = "  RTR_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "' AND  RTR_TRANS_NO='" + lblgvRadHistTransNo.Text.Trim() + "'";
            Criteria += " AND ISNULL(RTR_DATA_LOAD_FROM,'') <> 'WEB' ";

            DS = objCom.fnGetFieldValue("TOP 1 RTR_TRANS_NO", "RAD_TEST_REPORT", Criteria, " RTR_TRANS_NO ");

            if (DS.Tables[0].Rows.Count > 0)
            {

                ReportName = "RadiologyReport.rpt";

            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "RadResult", "RadResultReport('" + ReportName + "','" + lblgvRadHistTransNo.Text + "');", true);

        }


        protected void btnRadHistory_Click(object sender, EventArgs e)
        {
            try
            {
                BindRadHistoryGrid();
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowRadHistoryPopup()", true);
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnRadHistory_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnXraySearch_Click(object sender, EventArgs e)
        {
            try
            {
                BindXRayMasterGrid();
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowXrayMasterPopup()", true);

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnXraySearch_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }



        protected void btnScanSearch_Click(object sender, EventArgs e)
        {
            try
            {
                BindScanMasterGrid();
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowScanMasterPopup()", true);

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnScanSearch_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void btnShowWaitingList_Click(object sender, EventArgs e)
        {
            try
            {
                // BindWaitngList();

                BindRegisterWaiting();


                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowWaitingListyPopup()", true);

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnShowWaitingList_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        //NOTE: BELOW EDIT NOT USING
        protected void WaitingListEdit_Click(object sender, EventArgs e)
        {
            txtContent.Text = "";

            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();

            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;
            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

            Label lblPatientId = (Label)gvScanCard.Cells[0].FindControl("lblPatientId");
            Label lblServiceID = (Label)gvScanCard.Cells[0].FindControl("lblServiceID");
            Label lblID = (Label)gvScanCard.Cells[0].FindControl("lblID");


            txtFileNo.Text = lblPatientId.Text;
            txtFileNo_TextChanged(txtFileNo, new EventArgs());

            txtCPTCode.Text = lblServiceID.Text;



            if (GlobalValues.FileDescription.ToUpper() == "SMCH" && Convert.ToString(ViewState["TransType"]).ToUpper() == "INVOICE")
            {
                txtInvoiceRefNo.Text = Convert.ToString(ViewState["TransID"]);
                BindInvoice();
                BindEMRDtls();

            }

            txtTransNo.Text = lblID.Text;
            BindTestReport();

        FunEnd: ;
            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "HideRadWaitingListPopup()", true);
        }



        protected void RegisterWaitingEdit_Click(object sender, EventArgs e)
        {
            txtContent.Text = "";

            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();

            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;
            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

            Label lblPatientId = (Label)gvScanCard.Cells[0].FindControl("lblPatientId");
            Label lblServiceID = (Label)gvScanCard.Cells[0].FindControl("lblServiceID");
            Label lblRadReqNo = (Label)gvScanCard.Cells[0].FindControl("lblRadReqNo");
            Label lblInvoiceID = (Label)gvScanCard.Cells[0].FindControl("lblInvoiceID");


            Label lblReqDateDesc = (Label)gvScanCard.Cells[0].FindControl("lblReqDateDesc");
            Label lblReqDateTimeDesc = (Label)gvScanCard.Cells[0].FindControl("lblReqDateTimeDesc");

            Label lblDoctorID = (Label)gvScanCard.Cells[0].FindControl("lblDoctorID");
            Label lblRefDoctorID = (Label)gvScanCard.Cells[0].FindControl("lblRefDoctorID");
            Label lblRefDoctorName = (Label)gvScanCard.Cells[0].FindControl("lblRefDoctorName");

            Label lblModalityUploadDateDesc = (Label)gvScanCard.Cells[0].FindControl("lblModalityUploadDateDesc");
            Label lblModalityUploadTime = (Label)gvScanCard.Cells[0].FindControl("lblModalityUploadTime");



            Label lblModalityCheckOutDateDesc = (Label)gvScanCard.Cells[0].FindControl("lblModalityCheckOutDateDesc");
            Label lblModalityCheckOutTime = (Label)gvScanCard.Cells[0].FindControl("lblModalityCheckOutTime");


            Label lblModalityValidatedDateDesc = (Label)gvScanCard.Cells[0].FindControl("lblModalityValidatedDateDesc");
            Label lblModalityValidatedTime = (Label)gvScanCard.Cells[0].FindControl("lblModalityValidatedTime");

            Label lblModality = (Label)gvScanCard.Cells[0].FindControl("lblModality");



            DataSet DS = new DataSet();
            string Criteria = " 1=1  ";
            RadiologyBAL objRad = new RadiologyBAL();
            Criteria += " AND RTR_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND RTR_RAD_REQ_NO='" + lblRadReqNo.Text.Trim() + "'";

            DS = objRad.TestReportGet(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {

                txtTransNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["RTR_TRANS_NO"]);
                txtTransNo_TextChanged(txtTransNo, new EventArgs());

                goto FunEnd;
            }
            else
            {
                txtTransNo.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "SCAN");
            }

            txtFileNo.Text = lblPatientId.Text;
            txtFileNo_TextChanged(txtFileNo, new EventArgs());

            txtCPTCode.Text = lblServiceID.Text;

            if (lblDoctorID.Text != "")
            {

                for (int intCount = 0; intCount < drpDoctors.Items.Count; intCount++)
                {
                    if (drpDoctors.Items[intCount].Value == lblDoctorID.Text)
                    {
                        drpDoctors.SelectedValue = lblDoctorID.Text;
                        goto ForDoctors;
                    }
                }
            ForDoctors: ;

            }



            txtRefDoctorName.Text = lblRefDoctorID.Text.Trim() + "~" + lblRefDoctorName.Text.Trim();

            //if (GlobalValues.FileDescription.ToUpper() == "SMCH" && Convert.ToString(ViewState["TransType"]).ToUpper() == "INVOICE")
            //{
            txtInvoiceRefNo.Text = lblInvoiceID.Text; ;
            //  BindInvoice();


            // }

            //  txtTransNo.Text = lblID.Text;
            //  BindTestReport();

            txtRadReqNo.Text = lblRadReqNo.Text;

            txtReqDate.Text = lblReqDateDesc.Text;

            if (lblReqDateTimeDesc.Text != "")
            {
                string strTimeUp = lblReqDateTimeDesc.Text;
                string[] arrTimeUp1 = strTimeUp.Split(':');

                if (arrTimeUp1.Length > 1)
                {
                    string strHour = arrTimeUp1[0];
                    if (strHour.Length == 1)
                    {
                        strHour = "0" + strHour;
                    }

                    drpReqHour.SelectedValue = strHour;
                    drpReqMin.SelectedValue = arrTimeUp1[1];
                }

            }



            txtObservStart.Text = lblModalityUploadDateDesc.Text;

            if (lblModalityUploadTime.Text != "")
            {
                string strTimeUp = lblModalityUploadTime.Text;
                string[] arrTimeUp1 = strTimeUp.Split(':');

                if (arrTimeUp1.Length > 1)
                {
                    string strHour = arrTimeUp1[0];
                    if (strHour.Length == 1)
                    {
                        strHour = "0" + strHour;
                    }

                    drpObservStHour.SelectedValue = strHour;
                    drpObservStMin.SelectedValue = arrTimeUp1[1];
                }

            }




            txtObservEnd.Text = lblModalityCheckOutDateDesc.Text;

            if (lblModalityCheckOutTime.Text != "")
            {
                string strTimeUp = lblModalityCheckOutTime.Text;
                string[] arrTimeUp1 = strTimeUp.Split(':');

                if (arrTimeUp1.Length > 1)
                {
                    string strHour = arrTimeUp1[0];
                    if (strHour.Length == 1)
                    {
                        strHour = "0" + strHour;
                    }

                    drpObservEndHour.SelectedValue = strHour;
                    drpObservEndMin.SelectedValue = arrTimeUp1[1];
                }

            }




            txtObservDate.Text = lblModalityValidatedDateDesc.Text;
            if (lblModalityValidatedTime.Text != "")
            {
                string strTimeUp = lblModalityValidatedTime.Text;
                string[] arrTimeUp1 = strTimeUp.Split(':');

                if (arrTimeUp1.Length > 1)
                {
                    string strHour = arrTimeUp1[0];
                    if (strHour.Length == 1)
                    {
                        strHour = "0" + strHour;
                    }

                    drpObservHour.SelectedValue = strHour;
                    drpObservMin.SelectedValue = arrTimeUp1[1];
                }

            }


            if (lblModality.Text != "")
            {
                if (drpDiagnosticService.Items.Count > 0)
                {

                    switch (lblModality.Text)
                    {
                        case "CT":
                            drpDiagnosticService.SelectedValue = "CT";
                            break;
                        case "CR":
                            drpDiagnosticService.SelectedValue = "XR";
                            break;
                        case "PX":
                            drpDiagnosticService.SelectedValue = "XR";
                            break;
                        case "US":
                            drpDiagnosticService.SelectedValue = "US";
                            break;
                        case "MR":
                            drpDiagnosticService.SelectedValue = "MRI";
                            break;
                        case "MG":
                            drpDiagnosticService.SelectedValue = "MAM";
                            break;
                    }

                }

            }
            BindInvoice();
            BindEMRDtls();

        FunEnd: ;

            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "HideRadWaitingListPopup()", true);
        }

        protected void btnWaitingListShow_Click(object sender, EventArgs e)
        {
            //BindWaitngList();
            BindRegisterWaiting();
            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowWaitingListyPopup()", true);


        }



    }



}