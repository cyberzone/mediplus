﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Mediplus_BAL;

namespace Mediplus.Radiology
{
    public partial class RadTestRegister : System.Web.UI.Page
    {
        CommonBAL objCom = new CommonBAL();
        RadiologyBAL objRad = new RadiologyBAL();

        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindTime12HrsFromDB()
        {

            objCom = new CommonBAL();
            DataSet DSHours = new DataSet();

            DSHours = objCom.HoursGet("True");

            drpTransHour.DataSource = DSHours;
            drpTransHour.DataTextField = "Name";
            drpTransHour.DataValueField = "Code";
            drpTransHour.DataBind();

            ////drpReqHour.DataSource = DSHours;
            ////drpReqHour.DataTextField = "Name";
            ////drpReqHour.DataValueField = "Code";
            ////drpReqHour.DataBind();







            DataSet DSMin = new DataSet();

            DSMin = objCom.MinutesGet("1");


            drpTransMin.DataSource = DSMin;
            drpTransMin.DataTextField = "Name";
            drpTransMin.DataValueField = "Code";
            drpTransMin.DataBind();

            ////drpReqMin.DataSource = DSMin;
            ////drpReqMin.DataTextField = "Name";
            ////drpReqMin.DataValueField = "Code";
            ////drpReqMin.DataBind();




        }

        void BindNationality()
        {
            dboperations dbo = new dboperations();
            DataSet ds = new DataSet();
            ds = dbo.Nationality();
            if (ds.Tables[0].Rows.Count > 0)
            {
                drpNationality.DataSource = ds;
                drpNationality.DataValueField = "hnm_name";
                drpNationality.DataTextField = "hnm_name";
                drpNationality.DataBind();



            }
            drpNationality.Items.Insert(0, "Select");
            drpNationality.Items[0].Value = "0";



        }

        void BindDoctor()
        {

            DataSet ds = new DataSet();
            StaffMasterBAL dbo = new StaffMasterBAL();

            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND  HSFM_SF_STATUS='Present'";// AND HSFM_DEPT_ID='RADIOLOGY' 
            Criteria += " and HSFM_DEPT_ID='RADIOLOGY' ";

            ds = dbo.GetStaffMaster(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                drpDoctors.DataSource = ds;
                drpDoctors.DataTextField = "FullName";
                drpDoctors.DataValueField = "HSFM_STAFF_ID";
                drpDoctors.DataBind();




                for (int intCount = 0; intCount < drpDoctors.Items.Count; intCount++)
                {
                    if (drpDoctors.Items[intCount].Value == Convert.ToString(Session["User_Code"]))
                    {
                        drpDoctors.SelectedValue = Convert.ToString(Session["User_Code"]);
                        goto ForDoctors;
                    }
                }
            ForDoctors: ;



            }

            drpDoctors.Items.Insert(0, "--- Select ---");
            drpDoctors.Items[0].Value = "";


        }


        void PatientDataBind()
        {
            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HPM_PT_ID = '" + txtFileNo.Text + "'";
            Criteria += " AND HPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            objCom = new CommonBAL();

            ds = objCom.PatientMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                txtFileNo.Text = ds.Tables[0].Rows[0]["HPM_PT_ID"].ToString();
                txtFName.Text = ds.Tables[0].Rows[0]["FullName"].ToString();


                txtDOB.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_DOBDesc"]);
                //  lblAge.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_Age"]) + " " + Convert.ToString(ds.Tables[0].Rows[0]["HPM_Age1"]);

                txtAge.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_Age"]);

                drpAgeType.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE_TYPE"]);

                txtAge1.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_Age1"]);

                drpAgeType1.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE_TYPE1"]);




                txtSex.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_SEX"]);





                if (ds.Tables[0].Rows[0].IsNull("HPM_NATIONALITY") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_NATIONALITY"]) != "")
                {
                    for (int intCount = 0; intCount < drpNationality.Items.Count; intCount++)
                    {
                        if (drpNationality.Items[intCount].Value.ToUpper() == Convert.ToString(ds.Tables[0].Rows[0]["HPM_NATIONALITY"]).ToUpper())
                        {
                            drpNationality.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_NATIONALITY"]);
                            goto ForNationality;
                        }
                    }

                }

            ForNationality: ;



                ViewState["User_DeptID"] = Convert.ToString(ds.Tables[0].Rows[0]["HPM_DEP_NAME"]);

                txtProviderID.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_ID"]);
                txtProviderName.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_NAME"]);
                txtPolicyNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_POLICY_NO"]);
                txtIDNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_ID_NO"]);


                drpInvType.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["HPM_PT_TYPE"]) == "CA" ? "Cash" : "Credit";
            }

        }


        void PTDtlsClear()
        {

            txtFName.Text = "";
            lblStatus.Text = "";

            txtDOB.Text = "";
            //lblAge.Text = "";

            //   hidAgeType.Value = Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE_TYPE"]);

            txtSex.Text = "";

            //txtAddress.Text = "";
            //txtPoBox.Text = "";

            //txtArea.Text = "";
            //txtCity.Text = "";

            if (drpNationality.Items.Count > 0)
                drpNationality.SelectedIndex = 0;

            //txtPhone1.Text = "";
            //txtMobile1.Text = "";
            //txtMobile2.Text = "";




            //  hidInsName.Value = Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_NAME"]);
            ViewState["User_DeptID"] = "";


            ////for (int intCount = 0; intCount < drpDoctors.Items.Count; intCount++)
        ////{
        ////    if (drpDoctors.Items[intCount].Value == Convert.ToString(Session["User_Code"]))
        ////    {
        ////        drpDoctors.SelectedValue = Convert.ToString(Session["User_Code"]);
        ////        goto ForDoctors;
        ////    }
        ////}

        ForDoctors: ;

        }

        void BindWaitngList()
        {

            string strTestType = "";

            strTestType = Convert.ToString(Session["HSOM_RAD_LIST"]);


            DataSet DS = new DataSet();

            LaboratoryBAL objLab = new LaboratoryBAL();

            string Criteria = " 1=1  ";

            string strStartDate = txtSrcFromDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            string strTotDate = txtSrcToDate.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }


            if (strTestType == "EMR")
            {
                Criteria += " AND EPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
                if (txtSrcFromDate.Text != "")
                {
                    Criteria += " AND   CONVERT(datetime,convert(varchar(10),EPM_DATE ,101),101) >= '" + strForStartDate + "'";
                    Criteria += " AND   CONVERT(datetime,convert(varchar(10),EPM_DATE ,101),101) <= '" + strForToDate + "'";
                }


                if (txtFileNo.Text.Trim() != "")
                {
                    Criteria += " AND  EPM_PT_ID LIKE '%" + txtFileNo.Text.Trim() + "%'";
                }

                if (txtFileNo.Text.Trim() != "")
                {
                    Criteria += " AND  EPM_PT_NAME LIKE '%" + txtFName.Text.Trim() + "%'";
                }
            }
            else
            {
                Criteria = " 1=1   ";

                Criteria += " AND HIT_SERV_TYPE='R'  ";


                Criteria += " AND HIM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";



                if (txtSrcFromDate.Text != "")
                {
                    Criteria += " AND   CONVERT(datetime,convert(varchar(10),HIM_DATE,101),101) >= '" + strForStartDate + "'";
                    Criteria += " AND   CONVERT(datetime,convert(varchar(10),HIM_DATE,101),101) <= '" + strForToDate + "'";
                }
                if (txtSrcFileNo.Text.Trim() != "")
                {
                    Criteria += " AND  HIM_PT_ID LIKE '%" + txtSrcFileNo.Text.Trim() + "%'";
                }

                if (txtSrcPTName.Text.Trim() != "")
                {
                    Criteria += " AND  HIM_PT_NAME LIKE '%" + txtSrcPTName.Text.Trim() + "%'";
                }

                if (strTestType == "EMR")
                {

                }
                else
                {
                    if (drpWaitListStatus.SelectedValue == "Waiting")
                    {
                        // Criteria += " AND (LTRM_AUTHORIZE_STATUS IS NULL OR  LTRM_AUTHORIZE_STATUS ='' ) ";
                        //  Criteria += " AND (RTR_RAD_REQ_NO IS NULL OR  RTR_RAD_REQ_NO ='' ) ";

                        /////   Criteria += " AND HIM_INVOICE_ID NOT IN (SELECT ISNULL(RTRM_INVOICE_ID,'') FROM RAD_TEST_REGISTER_MASTER )";
                        // Criteria += " AND HIT_SERV_CODE NOT IN (SELECT ISNULL(RTRM_CPT_CODE,'') FROM RAD_TEST_REGISTER_MASTER )";

                        Criteria += " AND HIT_RAD_STATUS IS NULL ";

                    }
                    else
                    {
                        // Criteria += " AND (RTR_RAD_REQ_NO IS NOT  NULL AND RTR_RAD_REQ_NO <> '' ) ";
                        //// Criteria += " AND HIM_INVOICE_ID  IN (select RTRM_INVOICE_ID From RAD_TEST_REGISTER_MASTER )";
                        //  Criteria += " AND HIT_SERV_CODE  IN (SELECT ISNULL(RTRM_CPT_CODE,'') FROM RAD_TEST_REGISTER_MASTER )";

                        Criteria += " AND HIT_RAD_STATUS IS NOT NULL ";
                    }
                }

            }

            string strServiceType = "";

            strServiceType = "R";


            DS = objLab.LabWaitingListShow("Waiting", GlobalValues.FileDescription, strServiceType, Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                gvWaitingList.DataSource = DS;
                gvWaitingList.DataBind();
            }
            else
            {
                gvWaitingList.DataBind();
            }

        }

        string GetEMRID()
        {
            string strEMR_ID = "0";
            DataSet DS = new DataSet();
            String Criteria = " 1=1  ";

            string strStartDate = Convert.ToString(ViewState["TransDate"]);
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            string strTotDate = Convert.ToString(ViewState["TransDate"]);
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }



            Criteria += " AND EPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND   CONVERT(datetime,convert(varchar(10),EPM_DATE ,101),101) >= '" + strForStartDate + "'";
            Criteria += " AND   CONVERT(datetime,convert(varchar(10),EPM_DATE ,101),101) <= '" + strForToDate + "'";

            Criteria += " AND  EPM_PT_ID LIKE '%" + Convert.ToString(ViewState["PatientID"]) + "%'";
            Criteria += " AND  EPM_DR_CODE LIKE '%" + Convert.ToString(ViewState["DoctorID"]) + "%'";


            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();
            DS = objEmrPTMast.GetEMR_PTMaster(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                strEMR_ID = Convert.ToString(DS.Tables[0].Rows[0]["EPM_ID"]);
            }


            return strEMR_ID;
        }

        void BindRadTestRegtMaster()
        {
            objRad = new RadiologyBAL();

            DataSet DS = new DataSet();

            string Criteria = " 1=1 ";

            Criteria += " AND RTRM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND RTRM_TRANS_NO='" + txtTransNo.Text.Trim() + "'";
            DS = objRad.TestRegisterMasterGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    ViewState["NewFlag"] = false;
                    txtTransNo.Text = Convert.ToString(DR["RTRM_TRANS_NO"]);
                    txtTransDate.Text = Convert.ToString(DR["RTRM_DATEDesc"]);

                    if (DR.IsNull("RTRM_DATETimeDesc") == false)
                    {
                        string strTimeUp = Convert.ToString(DR["RTRM_DATETimeDesc"]);
                        string[] arrTimeUp1 = strTimeUp.Split(':');

                        if (arrTimeUp1.Length > 1)
                        {
                            string strHour = arrTimeUp1[0];
                            if (strHour.Length == 1)
                            {
                                strHour = "0" + strHour;
                            }

                            drpTransHour.SelectedValue = strHour;
                            drpTransMin.SelectedValue = arrTimeUp1[1];
                        }

                    }







                    txtFileNo.Text = Convert.ToString(DR["RTRM_PATIENT_ID"]);
                    txtFileNo_TextChanged(txtFileNo, new EventArgs());

                    txtCPTCode.Text = Convert.ToString(DR["RTRM_CPT_CODE"]);
                    string CPTName = "";
                    GetServiceMasterName("HSM_SERV_ID", txtCPTCode.Text, out  CPTName);
                    txtCPTDesc.Text = CPTName;

                    txtCPTShortName.Text = Convert.ToString(DR["RTRM_CPT_SHORT_DESC"]);

                    if (DR.IsNull("RTRM_MODALITY") == false && Convert.ToString(DR["RTRM_MODALITY"]) != "")
                    {
                        drpModality.SelectedValue = Convert.ToString(DR["RTRM_MODALITY"]);
                    }

                    txtEMRID.Text = Convert.ToString(DR["RTRM_EMR_ID"]);
                    txtInvoiceID.Text = Convert.ToString(DR["RTRM_INVOICE_ID"]);

                    //     drpDoctors.SelectedValue = Convert.ToString(DR["RTRM_DR_CODE"]);

                    if (DR.IsNull("RTRM_DR_CODE") == false && Convert.ToString(DR["RTRM_DR_CODE"]) != "")
                    {
                        for (int intCount = 0; intCount < drpDoctors.Items.Count; intCount++)
                        {
                            if (drpDoctors.Items[intCount].Value == Convert.ToString(DR["RTRM_DR_CODE"]))
                            {
                                drpDoctors.SelectedValue = Convert.ToString(DR["RTRM_DR_CODE"]);
                                goto ForDoctors;
                            }
                        }
                    ForDoctors: ;


                    }

                    if (DR.IsNull("RTRM_REF_DR") == false && Convert.ToString(DR["RTRM_REF_DR"]) != "")
                    {
                        string strRefDrCode = "", strRefDrName = "";

                        strRefDrCode = Convert.ToString(DR["RTRM_REF_DR"]);

                        strRefDrName = Convert.ToString(DR["RTRM_REF_DR_NAME"]);

                        txtRefDoctorName.Text = strRefDrCode + "~" + strRefDrName;

                    }



                }

            }

        }

        void BindInvoiceDtls()
        {
            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();
            string Criteria = "HIM_INVOICE_ID='" + txtInvoiceID.Text + "'";
            DS = objCom.fnGetFieldValue("TOP 1 * ", "HMS_INVOICE_MASTER", Criteria, "");

            if (DS.Tables[0].Rows.Count > 0)
            {
                string strRefDrCode = "", strRefDrName = "";

                txtEMRID.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_EMR_ID"]);
                strRefDrCode = Convert.ToString(DS.Tables[0].Rows[0]["HIM_DR_CODE"]);

                strRefDrName = Convert.ToString(DS.Tables[0].Rows[0]["HIM_DR_NAME"]);

                txtRefDoctorName.Text = strRefDrCode + "~" + strRefDrName;

               

                txtProviderID.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_SUB_INS_CODE"]);
                txtProviderName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_INS_NAME"]);
                txtPolicyNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_POLICY_NO"]);
                txtIDNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_ID_NO"]);
                drpInvType.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HIM_INVOICE_TYPE"]);


            }

        }

        void Clear()
        {
            ViewState["NewFlag"] = true;
            //  drpTransType.SelectedIndex = 1;


            drpStatus.SelectedIndex = 0;

            txtCPTCode.Text = "";
            txtCPTDesc.Text = "";
            txtCPTShortName.Text = "";

            drpModality.SelectedIndex = 0;

            txtFileNo.Text = "";
            txtAge.Text = "";
            drpAgeType.SelectedIndex = 0;

            txtAge1.Text = "";
            drpAgeType1.SelectedIndex = 1;



            if (drpDoctors.Items.Count > 0)
                drpDoctors.SelectedIndex = 0;

            txtRefDoctorName.Text = "";


            btnSave.Visible = true;
            // btnApproved.Visible = true;

            drpInvType.SelectedIndex = 0;
            drpPTType.SelectedIndex = 0;

            txtProviderID.Text = "";
            txtProviderName.Text = "";
            txtPolicyNo.Text = "";
            txtIDNo.Text = "";
            txtEMRID.Text = "";
            txtInvoiceID.Text = "";

        }

        void New()
        {
            ViewState["NewFlag"] = true;
            DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
            txtTransDate.Text = strFromDate.ToString("dd/MM/yyyy");
            txtTransNo.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "RADREG");
        }

        void GetServiceMasterName(string SearchType, string ServCode, out string strServName)
        {
            strServName = "";
            string Criteria = " 1=1 AND HSM_STATUS='A' ";
            if (SearchType == "HSM_SERV_ID")
            {
                Criteria += " AND  HSM_SERV_ID   ='" + ServCode + "'";
            }
            else
            {
                Criteria += " AND  HSM_HAAD_CODE   ='" + ServCode + "'";
            }

            CommonBAL objCom = new CommonBAL();
            DataSet DSServ = new DataSet();
            DSServ = objCom.fnGetFieldValue("TOP 1 *", "HMS_SERVICE_MASTER", Criteria, "");

            if (DSServ.Tables[0].Rows.Count > 0)
            {
                strServName = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_NAME"]);

            }



        }

        #endregion

        #region AutoCompleteExtender


        [System.Web.Services.WebMethod]
        public static string[] RefGetDoctorName(string prefixText)
        {
            string[] Data=null;

            string criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND  HSFM_SF_STATUS='Present'";
            criteria += " AND HSFM_FNAME + ' ' +isnull(HSFM_MNAME,'') + ' '  + isnull(HSFM_LNAME,'')   like '%" + prefixText + "%' ";
            DataSet ds = new DataSet();
            StaffMasterBAL objStaff = new StaffMasterBAL();
            ds = objStaff.GetStaffMaster(criteria);
            int StaffCount = ds.Tables[0].Rows.Count;


            criteria = " 1=1 ";
            criteria += " AND HRDM_FNAME + ' ' +isnull(HRDM_MNAME,'') + ' '  + isnull(HRDM_LNAME,'')   like '%" + prefixText + "%' ";
            DataSet ds1 = new DataSet();
            dboperations dbo = new dboperations();
            ds1 = dbo.RefDoctorMasterGet(criteria);
            int RefStaffCount = ds1.Tables[0].Rows.Count;

            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[StaffCount + RefStaffCount];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["HSFM_STAFF_ID"]) + "~" + Convert.ToString(ds.Tables[0].Rows[i]["FullName"]);
                }

            }


            if (ds1.Tables[0].Rows.Count > 0)
            {
                 for (int j = 0 ; j < ds1.Tables[0].Rows.Count; j++)
                {
                    Data[StaffCount + j] = Convert.ToString(ds1.Tables[0].Rows[j]["HRDM_REF_ID"]) + "~" + Convert.ToString(ds1.Tables[0].Rows[j]["FullName"]);
                }

                return Data;
            }
            else
            {
                return Data;
            }

            string[] Data1 = { "" };

            return Data1;
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }
            lblStatus.Text = "";

            if (!IsPostBack)
            {
                ViewState["NewFlag"] = true;
                string strTime = "", strAM = "";
                DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                txtTransDate.Text = strFromDate.ToString("dd/MM/yyyy");
                txtSrcFromDate.Text = strFromDate.ToString("dd/MM/yyyy");
                txtSrcToDate.Text = strFromDate.ToString("dd/MM/yyyy");

                // txtReqDate.Text = strFromDate.ToString("dd/MM/yyyy");


                txtTransNo.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "RADREG");


                strTime = objCom.fnGetDate("hh:mm");
                strAM = objCom.fnGetDate("tt");

                string strHour = Convert.ToString(System.DateTime.Now.Hour);
                if (Convert.ToInt32(strHour) <= 9)
                {
                    strHour = "0" + strHour;
                }
                string strMin = Convert.ToString(System.DateTime.Now.Minute);
                if (Convert.ToInt32(strMin) <= 9)
                {
                    strMin = "0" + strMin;
                }

                BindTime12HrsFromDB();

                drpTransHour.SelectedValue = strHour;
                drpTransMin.SelectedValue = strMin;

                //  drpReqHour.SelectedValue = strHour;
                //  drpReqMin.SelectedValue = strMin;




                BindNationality();
                BindDoctor();
            }
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            try
            {
                Clear();
                PTDtlsClear();

                New();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnNew_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void txtFileNo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                PTDtlsClear();
                PatientDataBind();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "txtFileNo_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void btnShowWaitingList_Click(object sender, EventArgs e)
        {
            try
            {
                BindWaitngList();
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowWaitingListyPopup()", true);

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnShowWaitingList_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void WaitingListEdit_Click(object sender, EventArgs e)
        {
            ///  txtContent.Text = "";

            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();

            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;
            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

            Label lblPatientId = (Label)gvScanCard.Cells[0].FindControl("lblPatientId");
            Label lblServiceID = (Label)gvScanCard.Cells[0].FindControl("lblServiceID");
            Label lblID = (Label)gvScanCard.Cells[0].FindControl("lblID");
            Label lblTransDate = (Label)gvScanCard.Cells[0].FindControl("lblTransDate");

               objRad = new RadiologyBAL();

            DataSet DS = new DataSet();

            string Criteria = " 1=1 ";

            Criteria += " AND RTRM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND RTRM_INVOICE_ID='" + lblID.Text.Trim() + "' AND RTRM_CPT_CODE='" + lblServiceID.Text.Trim() + "'";
            DS = objRad.TestRegisterMasterGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                txtTransNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["RTRM_TRANS_NO"]);
                txtTransNo_TextChanged(txtTransNo, new EventArgs());

                goto FunEnd;

            }
            else
            {
                txtTransNo.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "RADREG");
            }

            txtFileNo.Text = lblPatientId.Text;
            txtFileNo_TextChanged(txtFileNo, new EventArgs());

            ViewState["TransDate"] = lblTransDate.Text;

            string strTestType = "";

            strTestType = Convert.ToString(Session["HSOM_RAD_LIST"]);

            txtCPTCode.Text = lblServiceID.Text.Trim();

            string CPTName = "";
            GetServiceMasterName("HSM_SERV_ID", txtCPTCode.Text, out  CPTName);
            txtCPTDesc.Text = CPTName;


            if (strTestType == "EMR")
            {
                txtEMRID.Text = lblID.Text;
            }

            else
            {
                txtInvoiceID.Text = lblID.Text;
                //string EMR_ID = "0";
                //EMR_ID = GetEMRID();
                //txtEMRID.Text = EMR_ID;
            }




            BindInvoiceDtls();




            //   BindTestReport();

            FunEnd: ;
            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "HideRadWaitingListPopup()", true);
        }

        protected void btnWaitingListShow_Click(object sender, EventArgs e)
        {
            BindWaitngList();
            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowWaitingListyPopup()", true);


        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtTransNo.Text.Trim() == "")
                {
                    lblStatus.Text = "Enter Trans No";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }
                if (txtFileNo.Text.Trim() == "")
                {
                    lblStatus.Text = "Enter File No";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }


                if (txtCPTCode.Text.Trim() == "")
                {
                    lblStatus.Text = "Enter CPT Code";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                if (txtCPTShortName.Text.Trim() == "")
                {
                    lblStatus.Text = "Enter Short Name";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                if (drpDoctors.SelectedIndex == 0)
                {
                    lblStatus.Text = "Enter Doctor Name";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }



                if (drpModality.SelectedIndex == 0)
                {
                    lblStatus.Text = "Please Select Modality";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                if (txtRefDoctorName.Text.Trim() == "")
                {
                    lblStatus.Text = "Please Enter Ref. Doctor Name";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                string strRefDrCode = "", strRefDrName = ""; ;

                if (txtRefDoctorName.Text.Trim() != "")
                {
                    string strDiagNameDtls = txtRefDoctorName.Text.Trim();
                    string[] arrDiagName = strDiagNameDtls.Split('~');

                    if (arrDiagName.Length > 0)
                    {
                        strRefDrCode = arrDiagName[0];

                        if (arrDiagName.Length > 1)
                        {
                            strRefDrName = arrDiagName[1];

                        }
                        else
                        {
                            lblStatus.Text = "Please Enter Ref. Doctor Code";
                            lblStatus.ForeColor = System.Drawing.Color.Red;
                            goto FunEnd;
                        }
                    }
                    else
                    {

                        lblStatus.Text = "Please Enter Ref. Doctor Name";
                        lblStatus.ForeColor = System.Drawing.Color.Red;
                        goto FunEnd;

                    }


                }

                if (strRefDrCode == "" || strRefDrName == "")
                {
                    lblStatus.Text = "Please Enter Ref. Doctor Code and Name";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                if (drpDoctors.SelectedValue == strRefDrCode)
                {
                    lblStatus.Text = "Please Select Different  Ref. Doctor Name";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                objRad = new RadiologyBAL();
                objRad.RTRM_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objRad.RTRM_TRANS_NO = txtTransNo.Text.Trim();

                objRad.RTRM_DATE = txtTransDate.Text.Trim() + " " + drpTransHour.SelectedValue + ":" + drpTransMin.SelectedValue + ":00";
                objRad.RTRM_STATUS = drpStatus.SelectedValue;

                objRad.RTRM_PATIENT_ID = txtFileNo.Text.Trim();
                objRad.RTRM_PATIENT_NAME = txtFName.Text.Trim();
                objRad.RTRM_PATIENT_AGE = txtAge.Text.Trim();
                objRad.RTRM_PATIENT_AGETYPE = drpAgeType.SelectedValue;
                objRad.RTRM_PATIENT_SEX = txtSex.Text.Trim();
                objRad.RTRM_PATIENT_NATIONALITY = drpNationality.SelectedValue;
                objRad.RTRM_REF_DR = strRefDrCode;
                objRad.RTRM_REF_DR_NAME = strRefDrName;

                objRad.RTRM_DR_CODE = drpDoctors.SelectedValue;
                objRad.RTRM_DR_NAME = drpDoctors.SelectedItem.Text;

                objRad.RTRM_CPT_CODE = txtCPTCode.Text;
                objRad.RTRM_CPT_SHORT_DESC = txtCPTShortName.Text;
                objRad.RTRM_MODALITY = drpModality.SelectedValue;


                objRad.RTRM_EMR_ID = txtEMRID.Text.Trim();
                objRad.RTRM_INVOICE_ID = txtInvoiceID.Text.Trim();

                //  objRad.InvoiceRefNo = txtInvoiceRefNo.Text;
                if (Convert.ToBoolean(ViewState["NewFlag"]) == true)
                {
                    objRad.NewFlag = "A";
                }
                else
                {
                    objRad.NewFlag = "M";
                }

                objRad.SCRN_ID = "RADREG";
                //}
                //else
                //{
                //    objRad.SCRN_ID = "XRAY";
                //}
                objRad.UserID = Convert.ToString(Session["User_ID"]);
                objRad.TestRegisterMasterAdd();


              string   strTestType = Convert.ToString(Session["HSOM_RAD_LIST"]);
                if (strTestType == "EMR")
                {
                    //string FieldNameWithValues = " Completed='LAB_REGISTERED' , EPL_DATE=GETDATE() ";
                    //string Criteria1 = " Completed IS NULL AND EPL_ID ='" + lblID.Text.Trim() + "' AND EPL_LAB_CODE='" + lblServiceID.Text.Trim() + "'";
                    //objCom.fnUpdateTableData(FieldNameWithValues, "EMR_PT_LABORATORY", Criteria1);
                }
                else
                {

                    string FieldNameWithValues = " HIT_RAD_STATUS='RAD_REGISTERED'  ";
                    string Criteria1 = " HIT_RAD_STATUS IS NULL AND HIT_INVOICE_ID ='" + txtInvoiceID.Text.Trim() + "' AND HIT_SERV_CODE='" + txtCPTCode.Text.Trim() + "'";
                    objCom.fnUpdateTableData(FieldNameWithValues, "HMS_INVOICE_TRANSACTION", Criteria1);

                    // LabTestRegisterTimLog(System.DateTime.Now.ToString() + "      ---HMS_INVOICE_TRANSACTION Update  completed");
                }

             
                Clear();
                PTDtlsClear();

                lblStatus.Text = "";
                // txtTransNo.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "SCAN");
                lblStatus.Text = "Data  Saved";
                lblStatus.ForeColor = System.Drawing.Color.Green;


                New();



            FunEnd: ;

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void txtTransNo_TextChanged(object sender, EventArgs e)
        {
            BindRadTestRegtMaster();
            BindInvoiceDtls();
        }

        protected void btnSendRequest_Click(object sender, EventArgs e)
        {
            objRad = new RadiologyBAL();

            DataSet DS = new DataSet();

            string Criteria = " 1=1 ";

            Criteria += " AND RTRM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND RTRM_TRANS_NO='" + txtTransNo.Text.Trim() + "'";
            DS = objRad.TestRegisterMasterGet(Criteria);
            if (DS.Tables[0].Rows.Count <= 0)
            {
                lblStatus.Text = "No Data";
                lblStatus.ForeColor = System.Drawing.Color.Red;

                goto funEnd;
            }


            string strRefDrCode = "", strRefDrName = ""; ;

            if (txtRefDoctorName.Text.Trim() != "")
            {
                string strDiagNameDtls = txtRefDoctorName.Text.Trim();
                string[] arrDiagName = strDiagNameDtls.Split('~');

                if (arrDiagName.Length > 0)
                {
                    strRefDrCode = arrDiagName[0];


                }


            }
            DS = new DataSet();
            objCom = new CommonBAL();
            string Criteria1 = "HPV_PT_ID='" + txtFileNo.Text.Trim() + "' AND HPV_DR_ID='" + strRefDrCode + "'";
            DS = objCom.fnGetFieldValue(" TOP 1  HPV_SEQNO ", "HMS_PATIENT_VISIT", Criteria1, " HPV_SEQNO DESC");

            if (DS.Tables[0].Rows.Count > 0)
            {
                Rad_HL7MessageGenerator objHL7Mess = new Rad_HL7MessageGenerator();
                objHL7Mess.PT_ID = txtFileNo.Text.Trim();
                objHL7Mess.EMR_ID = "";
                objHL7Mess.VISIT_ID = Convert.ToString(DS.Tables[0].Rows[0]["HPV_SEQNO"]);
                objHL7Mess.RAD_TEST_REPORT_ID = txtTransNo.Text;

                objHL7Mess.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objHL7Mess.BRANCH_NAME = Convert.ToString(Session["Branch_Name"]);
                objHL7Mess.PROVIDER_ID = Convert.ToString(Session["Branch_ProviderID"]);
                objHL7Mess.UserID = Convert.ToString(Session["User_ID"]);

                Boolean IsError = false;
                IsError = objHL7Mess.ORMRadRegMessageGenerate();

                lblStatus.Text = "Request Send";
                lblStatus.ForeColor = System.Drawing.Color.Green;

                goto funEnd;

            }
            else
            {
                lblStatus.Text = "No Visit Datails";
                lblStatus.ForeColor = System.Drawing.Color.Red;

                goto funEnd;
            }
        funEnd: ;
        }

        protected void btnShowEmrRadReq_Click(object sender, EventArgs e)
        {

            if (txtEMRID.Text.Trim() == "" || txtFileNo.Text.Trim() == "")
            {
                goto funEnd;
            }

            string strRptPath = "../EMR/WebReports/Radiology.aspx";
            string rptcall = @strRptPath + "?EMR_ID=" + txtEMRID.Text.Trim() + "&EMR_PT_ID=" + txtFileNo.Text.Trim() + "&DR_ID=" + drpDoctors.SelectedValue;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ClinucalSum", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1');", true);

        funEnd: ;
        }





    }
}