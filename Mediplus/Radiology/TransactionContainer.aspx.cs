﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HMS.Radiology
{
    public partial class TransactionContainer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }


            if (!IsPostBack)
            {


                string PageName = (string)Request.QueryString["PageName"];

                if (PageName.ToUpper() == "XRayMaster".ToUpper())
                {
                    frmRadio.Attributes.Add("src", "XRayMaster.aspx"); 
                }
                else if (PageName.ToUpper() == "ScanMaster".ToUpper())
                {
                    frmRadio.Attributes.Add("src", "ScanMaster.aspx");
                }
                else if (PageName.ToUpper() == "Transaction".ToUpper())
                {
                    frmRadio.Attributes.Add("src", "Transaction.aspx");
                }
                else if (PageName.ToUpper() == "RadWaitList".ToUpper())
                {
                        


                    ViewState["TransID"] = Request.QueryString["TransID"];
                    ViewState["PatientID"] = Request.QueryString["PatientID"];
                    ViewState["DoctorID"] = Request.QueryString["DoctorID"];
                    ViewState["TransDate"] = Request.QueryString["TransDate"];
                    ViewState["TransType"] = Request.QueryString["TransType"];
                    ViewState["ServiceID"] = Request.QueryString["ServiceID"];

                    frmRadio.Attributes.Add("src", "Transaction.aspx?PageName=RadWaitList&TransID=" + Convert.ToString(ViewState["TransID"]) + "&PatientID=" + Convert.ToString(ViewState["PatientID"]) + "&DoctorID=" + Convert.ToString(ViewState["DoctorID"]) + "&TransDate=" + Convert.ToString(ViewState["TransDate"]) + "&TransType=" + Convert.ToString(ViewState["TransType"]) + "&ServiceID=" + Convert.ToString(ViewState["ServiceID"]));
                }

            }
        }
    }
}