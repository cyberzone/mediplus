﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EMRRadRequest.aspx.cs" Inherits="Mediplus.Radiology.EMRRadRequest" %>
<%@ Register Src="~/EMR/WebReports/Radiologies.ascx" TagPrefix="UC1" TagName="Radiologies" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../style.css" rel="Stylesheet" type="text/css" />

    
</head>
<body>
    <form id="form1" runat="server">
    <div>
     <UC1:Radiologies ID="Radiologies" runat="server"></UC1:Radiologies>
    </div>
    </form>
</body>
</html>
