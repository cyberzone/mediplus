﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="XRayMasterPopup.aspx.cs" Inherits="HMS.Radiology.XRayMasterPopup" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Lab Test Profile Master Popup</title>
     <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">

        function passvalue(Code, Name) {
           
            var CtrlName = document.getElementById('hidCtrlName').value;

            if (document.getElementById('hidPageName').value == 'XRayMaster') {
                opener.location.href = "XRayMaster.aspx?MenuName=Radiology&XrayCode=" + Code;
               
            }
            else if (document.getElementById('hidPageName').value == 'RadTrans') {
                opener.location.href = "Transaction.aspx?MenuName=Radiology&XrayCode=" + Code;
        }


            window.parent.close();
            window.parent.parent.close();
            window.close();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
         <input type="hidden" id="hidCtrlName" runat="server" />
         <asp:hiddenfield id="hidPageName" runat="server" />
    <div>
              <div style="padding-top: 0px; padding-left: 0px; width: 700px; overflow: auto; border-color: #CCCCCC; border-style: solid; border-width: thin;">

                <table width="100%">
                    <tr>
                         <td class="label" style="width: 100px;">Search
                        </td>
                        <td>
                           <asp:TextBox ID="txtSearch" runat="server"  CssClass="label" Width="300px" MaxLength="10" BorderWidth="1px" BorderColor="#cccccc"  AutoPostBack="true" OnTextChanged="txtSearch_TextChanged" ></asp:TextBox>
                        </td>
                       
                    </tr>
                </table>
            </div>
            <asp:GridView ID="gvPopupGrid" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                EnableModelValidation="True" Width="700">
                <HeaderStyle CssClass="GridHeader" />
                <RowStyle CssClass="GridRow" />
                <AlternatingRowStyle CssClass="GridAlterRow" />
                <Columns>
                    <asp:TemplateField HeaderText="X-Ray No.">
                        <ItemTemplate>
                            <a href="javascript:passvalue('<%# Eval("RXM_XRAY_CODE")  %>','<%# Eval("RXM_XRAY_DESCRIPTION")%>')">
                                <asp:Label ID="Label1" CssClass="label" runat="server"  Text='<%# Bind("RXM_XRAY_NO") %>' Width="50px"></asp:Label>
                            </a>
                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Code">
                        <ItemTemplate>
                            <a href="javascript:passvalue('<%# Eval("RXM_XRAY_CODE")  %>','<%# Eval("RXM_XRAY_DESCRIPTION")%>')">
                                <asp:Label ID="Label2" CssClass="label" runat="server" Text='<%# Bind("RXM_XRAY_CODE") %>' Width="150px"></asp:Label>
                            </a>
                        </ItemTemplate>

                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Description">
                        <ItemTemplate>
                            <a href="javascript:passvalue('<%# Eval("RXM_XRAY_CODE")  %>','<%# Eval("RXM_XRAY_DESCRIPTION")%>')">
                                <asp:Label ID="Label3" CssClass="label" runat="server" runat="server" Width="480px"   Text='<%# Bind("RXM_XRAY_DESCRIPTION") %>'></asp:Label>
                            </a>
                        </ItemTemplate>

                    </asp:TemplateField>
                     
                </Columns>

            </asp:GridView>
        </div>
    </form>
</body>
</html>