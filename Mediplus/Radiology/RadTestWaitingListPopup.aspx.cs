﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using Mediplus_BAL;

namespace Mediplus.Radiology
{
    public partial class RadTestWaitingListPopup : System.Web.UI.Page
    {
        CommonBAL objCom = new CommonBAL();

        LaboratoryBAL objLab = new LaboratoryBAL();
        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strDate = "";
                strDate = DateTime.Now.ToString("dd/MM/yyyy");
                string[] arrDate;
                arrDate = strDate.Split('/');

                string strFileName = Server.MapPath("../Log/MediplusLog_" + arrDate[2] + arrDate[1] + ".txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindWaitngList()
        {

            string strTestType = "";

            strTestType = Convert.ToString(Session["HSOM_RAD_LIST"]);


            DataSet DS = new DataSet();

            objLab = new LaboratoryBAL();

            string Criteria = " 1=1  ";

            string strStartDate = txtTransFromDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            string strTotDate = txtTransToDate.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }


            if (strTestType == "EMR")
            {
                Criteria += " AND EPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
                if (txtTransFromDate.Text != "")
                {
                    Criteria += " AND   CONVERT(datetime,convert(varchar(10),EPM_DATE ,101),101) >= '" + strForStartDate + "'";
                    Criteria += " AND   CONVERT(datetime,convert(varchar(10),EPM_DATE ,101),101) <= '" + strForToDate + "'";
                }


                if (txtFileNo.Text.Trim() != "")
                {
                    Criteria += " AND  EPM_PT_ID LIKE '%" + txtFileNo.Text.Trim() + "%'";
                }

                if (txtFileNo.Text.Trim() != "")
                {
                    Criteria += " AND  EPM_PT_NAME LIKE '%" + txtFName.Text.Trim() + "%'";
                }
            }
            else
            {
                Criteria = " 1=1   ";

                Criteria += " AND HIT_SERV_TYPE='R'  ";


                Criteria += " AND HIM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";



                if (txtTransFromDate.Text != "")
                {
                    Criteria += " AND   CONVERT(datetime,convert(varchar(10),HIM_DATE,101),101) >= '" + strForStartDate + "'";
                    Criteria += " AND   CONVERT(datetime,convert(varchar(10),HIM_DATE,101),101) <= '" + strForToDate + "'";
                }
                if (txtFileNo.Text.Trim() != "")
                {
                    Criteria += " AND  HIM_PT_ID LIKE '%" + txtFileNo.Text.Trim() + "%'";
                }

                if (txtFileNo.Text.Trim() != "")
                {
                    Criteria += " AND  HIM_PT_NAME LIKE '%" + txtFName.Text.Trim() + "%'";
                }
            }

            string strServiceType = "";

            strServiceType = "R";


            DS = objLab.LabWaitingListShow(drpWaitListStatus.SelectedValue, GlobalValues.FileDescription, strServiceType, Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                gvWaitingList.DataSource = DS;
                gvWaitingList.DataBind();
            }
            else
            {
                gvWaitingList.DataBind();
            }

        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }


            if (!IsPostBack)
            {
                try
                {
                    DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                    txtTransFromDate.Text = strFromDate.ToString("dd/MM/yyyy");
                    txtTransToDate.Text = strFromDate.ToString("dd/MM/yyyy");

                    ViewState["PageName"] = (string)Request.QueryString["PageName"];

                    hidPageName.Value = (string)Request.QueryString["PageName"];


                    lblHeader.Text = "Radiology Waiting List";


                    BindWaitngList();
                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }
            }
        }

        protected void btnShow_Click(object sender, EventArgs e)
        {


            try
            {
                BindWaitngList();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnShow_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void Edit_Click(object sender, EventArgs e)
        {

            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;

            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
            ViewState["SelectIndex"] = gvScanCard.RowIndex;

            gvWaitingList.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");

            Label lblID = (Label)gvScanCard.Cells[0].FindControl("lblID");
            Label lblPatientId = (Label)gvScanCard.Cells[0].FindControl("lblPatientId");
            Label lblDoctorID = (Label)gvScanCard.Cells[0].FindControl("lblDoctorID");
            Label lblTransDate = (Label)gvScanCard.Cells[0].FindControl("lblTransDate");
            Label lblTransType = (Label)gvScanCard.Cells[0].FindControl("lblTransType");
            Label lblServiceID = (Label)gvScanCard.Cells[0].FindControl("lblServiceID");

            ViewState["TransID"] = lblID.Text;
            ViewState["PatientID"] = lblPatientId.Text;
            ViewState["DoctorID"] = lblDoctorID.Text;
            ViewState["TransDate"] = lblTransDate.Text;
            ViewState["TransType"] = lblTransType.Text;
            ViewState["ServiceID"] = lblServiceID.Text;



        }

        protected void btnShowTestReport_Click(object sender, EventArgs e)
        {
            if (Convert.ToString(ViewState["TransID"]) == "" || Convert.ToString(ViewState["TransID"]) == null)
            {
                goto FunEnd;
            }

            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "PassValue('RadWaitList','" + Convert.ToString(ViewState["TransID"]) + "','" + Convert.ToString(ViewState["PatientID"]) + "','" + Convert.ToString(ViewState["DoctorID"]) + "','" + Convert.ToString(ViewState["TransDate"]) + "','" + Convert.ToString(ViewState["TransType"]) + "','" + Convert.ToString(ViewState["ServiceID"]) + "')", true);


        FunEnd: ;

        }
    }
}