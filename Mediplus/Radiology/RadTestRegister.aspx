﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="RadTestRegister.aspx.cs" Inherits="Mediplus.Radiology.RadTestRegister" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />
    <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlightvisit
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divComp
        {
            width: 400px !important;
        }

            #divComp div
            {
                width: 400px !important;
            }

        #divDr
        {
            width: 400px !important;
        }

            #divDr div
            {
                width: 400px !important;
            }


        #divRefDr
        {
            width: 400px !important;
        }

            #divRefDr div
            {
                width: 400px !important;
            }
    </style>

    <style type="text/css">
        #overlay
        {
            position: fixed;
            z-index: 90;
            top: 0px;
            left: 0px;
            background-color: #ffffff;
            width: 100%;
            height: 100%;
            filter: alpha(Opacity=80);
            opacity: 0.80;
            -noz-opacity: 0.80;
        }

        #modalprogress
        {
            position: absolute;
            top: 50%;
            left: 40%;
            margin: -11px 0 0 -55px;
            color: white;
        }
    </style>


    <script language="javascript" type="text/javascript">

       
        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }



        function ShowWaitingListyPopup() {

            document.getElementById("divWaitingListyPopup").style.display = 'block';


        }

        function HideRadWaitingListPopup() {

            document.getElementById("divWaitingListyPopup").style.display = 'none';

        }

    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <input type="hidden" id="hidPermission" runat="server" value="9" />

    <table width="80%">
        <tr>
            <td style="text-align: left; width: 20%;" class="PageHeader">
                <asp:Button ID="btnNew" runat="server" CssClass="button gray small" Text="New" OnClick="btnNew_Click" />
                &nbsp;
                        &nbsp;
                <asp:Label ID="lblPageHeader" runat="server" CssClass="Profile Master" Text="Test Register"></asp:Label>
            </td>
            <td style="text-align: right; width: 40%; vertical-align: top;">
                <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="btnShowWaitingList" runat="server" CssClass="button gray small" Width="100px" Text="Waiting List" OnClick="btnShowWaitingList_Click" />


                        <asp:Button ID="btnSendRequest" runat="server" CssClass="button red small" Text="Send Request" OnClick="btnSendRequest_Click" />
                    </ContentTemplate>

                </asp:UpdatePanel>

            </td>
            <td style="text-align: right; width: 40%;">
                <asp:UpdatePanel ID="UpPanalSave" runat="server">
                    <ContentTemplate>

                        <asp:Button ID="btnSave" runat="server" CssClass="button red small" Text="Save Record" OnClick="btnSave_Click" />
                    </ContentTemplate>

                </asp:UpdatePanel>
                <asp:UpdateProgress ID="UpdateProgress1" runat="server" DisplayAfter="0" AssociatedUpdatePanelID="UpPanalSave">
                    <ProgressTemplate>
                        <div id="overlay">
                            <div id="modalprogress">
                                <asp:Image ID="imgLoader" runat="server" ImageUrl="~/Images/loading.gif" AlternateText="loading..." Height="250" Width="400" />
                            </div>
                        </div>

                    </ProgressTemplate>
                </asp:UpdateProgress>
            </td>
        </tr>
    </table>

    <table style="padding-left: 0px" cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="lblStatus" runat="server" ForeColor="red" Style="letter-spacing: 1px; font-weight: bold;" CssClass="lblCaption1"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>

    </table>
    <div class="lblCaption1" style="width: 80%; height: auto; border: 1px solid #005c7b; padding: 10px; border-radius: 10px;">
        <table width="100%" border="0" cellpadding="0" cellspacing="5">

            <tr>
                <td class="lblCaption1" style="height: 25px;">Reg ID
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel18" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtTransNo" runat="server" Width="250px" Height="20PX" CssClass="TextBoxStyle" ondblclick="ShowRadHistoryPopup();" AutoPostBack="true" OnTextChanged="txtTransNo_TextChanged"></asp:TextBox>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="txtTransNo" />
                        </Triggers>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1" style="height: 25px;">Date
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtTransDate" runat="server" Width="72px" Height="20PX" CssClass="TextBoxStyle" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" TargetControlID="txtTransDate" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                            <asp:DropDownList ID="drpTransHour" Style="font-size: 11px;" CssClass="TextBoxStyle" runat="server" Width="36px"></asp:DropDownList>
                            :
                                             <asp:DropDownList ID="drpTransMin" Style="font-size: 11px;" CssClass="TextBoxStyle" runat="server" Width="36px"></asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </td>
                <td class="lblCaption1" style="height: 25px;">Status</td>
                <td>
                     <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="drpStatus" runat="server" Style="width: 100%" Height="20PX" class="TextBoxStyle">
                                <asp:ListItem Value="A" Selected="True">Active</asp:ListItem>
                                <asp:ListItem Value="I">InActive</asp:ListItem>
                                <asp:ListItem Value="D">Drafted</asp:ListItem>
                            </asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td class="lblCaption1" style="height: 25px; width: 120px;">Type
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel30" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="drpInvType" runat="server" CssClass="TextBoxStyle" Width="250px" Height="20PX">
                                <asp:ListItem Value="Cash" Selected="True">Cash</asp:ListItem>
                                <asp:ListItem Value="Credit">Credit</asp:ListItem>
                            </asp:DropDownList>
                        </ContentTemplate>

                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1" style="height: 25px;">PT. Type
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel34" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="drpPTType" runat="server" CssClass="TextBoxStyle" Width="250px" Height="20PX" BorderWidth="1px">
                                <asp:ListItem Value="OP">OP</asp:ListItem>
                                <asp:ListItem Value="IP">IP</asp:ListItem>
                            </asp:DropDownList>
                        </ContentTemplate>

                    </asp:UpdatePanel>
                </td>
                <td></td>
                <td>
                      <asp:Button ID="btnShowEmrRadReq" runat="server" CssClass="button gray small" Width="100%" Text="EMR Radiology Req..." OnClick="btnShowEmrRadReq_Click" />

                </td>
            </tr>
            <tr>
                <td colspan="6">
                    <span class="lblCaption1" style="font-weight: bold;">Patient Detals</span>
                </td>
            </tr>

            <tr>
                <td class="lblCaption1" style="height: 25px;">File No
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel17" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtFileNo" runat="server" Width="250px" Height="20PX" CssClass="TextBoxStyle" AutoPostBack="true" OnTextChanged="txtFileNo_TextChanged"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>

                <td class="lblCaption1" style="height: 25px;">Name
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel20" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtFName" runat="server" Width="250px" Height="20PX" CssClass="TextBoxStyle"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>



                <td class="lblCaption1" style="height: 25px;">Gender
                </td>
                <td class="lblCaption1">
                    <asp:UpdatePanel ID="UpdatePanel24" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtSex" runat="server" Width="100%" Height="20PX" CssClass="TextBoxStyle" MaxLength="10" ReadOnly="true"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
            </tr>
            <tr>
                <td class="lblCaption1" style="height: 25px;">DOB
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel15" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtDOB" runat="server" Width="250px" Height="20PX" CssClass="TextBoxStyle" MaxLength="10" ReadOnly="true"></asp:TextBox>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>

                <td class="lblCaption1" style="height: 25px;">Age
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel21" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtAge" runat="server" Width="40px" Height="20PX" Style="text-align: center;" CssClass="TextBoxStyle" MaxLength="10" ReadOnly="true"></asp:TextBox>
                            <asp:DropDownList ID="drpAgeType" runat="server" Style="width: 70px;" Height="20PX" class="TextBoxStyle" ReadOnly="true">
                                <asp:ListItem Value="Y">Years</asp:ListItem>
                                <asp:ListItem Value="M">Months</asp:ListItem>
                                <asp:ListItem Value="D">Days</asp:ListItem>
                            </asp:DropDownList>
                            <asp:TextBox ID="txtAge1" runat="server" Width="40px" Height="20PX" Style="text-align: center;" CssClass="TextBoxStyle" MaxLength="10" ReadOnly="true"></asp:TextBox>
                            <asp:DropDownList ID="drpAgeType1" runat="server" Style="width: 70px;" Height="20PX" class="TextBoxStyle" ReadOnly="true">
                                <asp:ListItem Value="Y">Years</asp:ListItem>
                                <asp:ListItem Value="M" Selected="True">Months</asp:ListItem>
                                <asp:ListItem Value="D">Days</asp:ListItem>
                            </asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>

                <td class="lblCaption1" style="height: 25px;">Nationality</td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel31" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="drpNationality" runat="server" CssClass="TextBoxStyle" Width="100%" Height="20PX" ReadOnly="true"></asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td colspan="6" style="height:10PX;">
                    <span class="lblCaption1" style="font-weight: bold;">Insurance Detals</span>
                </td>
            </tr>
            <tr>
                <td class="lblCaption1" style="height: 25px;">Company
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel36" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtProviderID" runat="server" CssClass="TextBoxStyle" ReadOnly="true" Visible="false"></asp:TextBox>
                            <asp:TextBox ID="txtProviderName" runat="server" Width="250px" CssClass="TextBoxStyle" ReadOnly="true"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>

                <td class="lblCaption1" style="height: 25px;">Policy No
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel37" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtPolicyNo" runat="server" Width="250px" CssClass="TextBoxStyle" ReadOnly="true"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>

                <td class="lblCaption1" style="height: 25px;">ID No
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel45" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtIDNo" runat="server" Width="100%" CssClass="TextBoxStyle" ReadOnly="true"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>

            <tr>
                <td class="lblCaption1" style="height: 25px;" valign="top">Doctor Name
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel35" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="drpDoctors" runat="server" Style="width: 250px" class="TextBoxStyle"></asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>

                <td class="lblCaption1" style="height: 25px;">Ref. Doctor
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel28" runat="server">
                        <ContentTemplate>
                            <div id="divRefDr" style="visibility: hidden;"></div>
                             <asp:TextBox ID="txtRefDoctorName" runat="server" CssClass="TextBoxStyle" Width="250px" MaxLength="50"  ></asp:TextBox>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender3" runat="Server" TargetControlID="txtRefDoctorName" MinimumPrefixLength="1" ServiceMethod="RefGetDoctorName"
                                CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divRefDr">
                            </asp:AutoCompleteExtender>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1" style="height: 25px;">Invoice ID
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtInvoiceID" runat="server" Width="100%" CssClass="TextBoxStyle" ReadOnly="true"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td class="lblCaption1" style="height: 25px;">
                    CPT Code
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtCPTCode" runat="server" Width="250px" CssClass="TextBoxStyle" ></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1" style="height: 25px;">CPT Desc.
                </td>
                <td colspan="3">
                    <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtCPTDesc" runat="server" Width="100%" MaxLength="10" CssClass="TextBoxStyle"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
               
            </tr>
            <tr>
                <td class="lblCaption1" style="height: 25px;">Modality
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                        <ContentTemplate>
                            <asp:DropDownList ID="drpModality" runat="server" Style="width: 250px" class="TextBoxStyle">
                                <asp:ListItem Text="--- Select ---" Value="" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="CT" Value="CT"></asp:ListItem>
                                <asp:ListItem Text="CR" Value="CR"></asp:ListItem>
                                <asp:ListItem Text="PX" Value="PX"></asp:ListItem>
                                <asp:ListItem Text="US" Value="US"></asp:ListItem>
                                <asp:ListItem Text="MR" Value="MR"></asp:ListItem>
                                <asp:ListItem Text="MG" Value="MG"></asp:ListItem>
                            </asp:DropDownList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                 <td class="lblCaption1" style="height: 25px;">Short Name
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtCPTShortName" runat="server" Width="250px" MaxLength="10" CssClass="TextBoxStyle"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                 <td class="lblCaption1" style="height: 25px;">EMR ID
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtEMRID" runat="server" Width="100%" CssClass="TextBoxStyle" ReadOnly="true"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>

    </div>
    <div style="height: 5px;"></div>



    <div id="divWaitingListyPopup" style="display: none; overflow: hidden; border: groove; height: 460px; width: 1000px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #eee9e9; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute; left: 200px; top: 148px;">
       <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td style="vertical-align: top; width: 50%;">
                  <span class="lblCaption1" style="font-weight:bold;"> Radiology Services Waiting List </span> 

                </td>
                <td align="right" style="vertical-align: top; width: 50%;">

                    <input type="button" id="Button6" class="ButtonStyle" style="color: #005c7b; background-color: #f6f6f6; border: none; border-radius: 0px; font-weight: bold; cursor: pointer;font-size:15px;" value=" X " onclick="HideRadWaitingListPopup()" />
                </td>
            </tr>

        </table>
           <div style="height:5px;border-bottom-color:red;border-width:1px;border-bottom-style:groove;"></div>
               <div style="height:5px;"></div>
        <table width="100%" border="0" cellpadding="3" cellspacing="3">

            <tr>

                <td class="lblCaption1" style="height: 25px;">From Date
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel40" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtSrcFromDate" runat="server" Width="70px" CssClass="TextBoxStyle" Height="20PX" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" TargetControlID="txtSrcFromDate" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>

                        </ContentTemplate>
                    </asp:UpdatePanel>

                </td>
                <td class="lblCaption1" style="height: 25px;">To Date
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel41" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtSrcToDate" runat="server" Width="70px" CssClass="TextBoxStyle" Height="20PX" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            <asp:CalendarExtender ID="CalendarExtender3" runat="server" Enabled="True" TargetControlID="txtSrcToDate" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>

                        </ContentTemplate>
                    </asp:UpdatePanel>

                </td>
                <td class="lblCaption1" style="height: 25px;">File No
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel42" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtSrcFileNo" runat="server" Width="70px" Height="20PX" CssClass="label"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1" style="height: 25px;">Name
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel43" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtSrcPTName" runat="server" Width="150px" Height="20PX" CssClass="label"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                  <td class="lblCaption1" style="height: 25px;">Status
                    </td>
                    <td style="width: 200px;">
                        <asp:UpdatePanel ID="UpdatePanel52" runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="drpWaitListStatus" runat="server" Style="width: 120px" class="TextBoxStyle">
                                    <asp:ListItem Value="Waiting">Waiting</asp:ListItem>
                                    <asp:ListItem Value="Completed">Completed</asp:ListItem>
                                 </asp:DropDownList>

                            </ContentTemplate>
                        </asp:UpdatePanel>

                    </td>

                <td>
                    <asp:UpdatePanel ID="UpdatePanel44" runat="server">
                        <ContentTemplate>
                            <asp:Button ID="btnWaitingListShow" runat="server" CssClass="button gray small" Text="Show" OnClick="btnWaitingListShow_Click" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>

        </table>
        <div style="padding-top: 0px; width: 100%; height: 400px; overflow: auto; border: thin; border-color: #f6f6f6; border-style: groove;">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td colspan="2">
                        <asp:UpdatePanel ID="UpdatePanel39" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="gvWaitingList" runat="server" Width="100%"
                                    AllowSorting="True" AutoGenerateColumns="False"
                                    EnableModelValidation="True">
                                    <HeaderStyle CssClass="GridHeader_Blue" />
                                    <RowStyle CssClass="GridRow" />

                                    <Columns>

                                        <asp:TemplateField HeaderText="No" SortExpression="ID" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkSerial" runat="server" OnClick="WaitingListEdit_Click" ToolTip="Update Waiting List">
                                                    <asp:Label ID="lblTransDate" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("TransDate") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lblDoctorID" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("DoctorID") %>' Visible="false"></asp:Label>
                                                    
                                                    <asp:Label ID="lblID" CssClass="GridRow" Width="40px" runat="server"  Font-Bold="true"  Text='<%# Bind("ID") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>

                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="File No" SortExpression="FileNo" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkPTId" runat="server" OnClick="WaitingListEdit_Click" ToolTip="Update Waiting List">

                                                    <asp:Label ID="lblPatientId" CssClass="GridRow" Width="100px"  runat="server"  Font-Bold="true"  Text='<%# Bind("FileNo") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>

                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Name" SortExpression="PatientName">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkPTName" runat="server" OnClick="WaitingListEdit_Click" ToolTip="Update Waiting List">
                                                    <asp:Label ID="Label8" CssClass="GridRow" Width="250px" runat="server" Text='<%# Bind("PatientName") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>

                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Doctor" SortExpression="Doctor">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkMob" runat="server" OnClick="WaitingListEdit_Click" ToolTip="Update Waiting List">
                                                    <asp:Label ID="Label9" CssClass="GridRow" Width="150px" runat="server" Text='<%# Bind("Doctor") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Trans Type" SortExpression="TransType" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkDrName" runat="server" OnClick="WaitingListEdit_Click" ToolTip="Update Waiting List">
                                                    <asp:Label ID="lblTransType" CssClass="GridRow" Width="70px" runat="server" Text='<%# Bind("TransType") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="PT Type" SortExpression="PatientType" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkDepName" runat="server" OnClick="WaitingListEdit_Click" ToolTip="Update Waiting List">
                                                    <asp:Label ID="Label10" CssClass="GridRow" Width="70px" runat="server" Text='<%# Bind("PatientType") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="CPT Code" SortExpression="ServiceID" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkServiceID" runat="server" OnClick="WaitingListEdit_Click" ToolTip="Update Waiting List">
                                                    <asp:Label ID="lblServiceID" CssClass="GridRow" Width="70px" runat="server"  Font-Bold="true" Text='<%# Bind("ServiceID") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Description" SortExpression="ServiceRequested">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkTime" runat="server" OnClick="WaitingListEdit_Click" ToolTip="Update Waiting List">
                                                    <asp:Label ID="Label11" CssClass="GridRow" runat="server" Text='<%# Bind("ServiceDesc") %>'></asp:Label>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>

                                </asp:GridView>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="gvWaitingList" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </td>

                </tr>

            </table>
        </div>
    </div>
</asp:Content>
