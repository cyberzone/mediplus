﻿<%@ Page Title="" MasterPageFile="~/Site2.Master" Language="C#" ValidateRequest="false" AutoEventWireup="true" CodeBehind="Transaction.aspx.cs" Inherits="HMS.Radiology.Transaction" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />


    <script src="../Scripts/AccordionScript.js"></script>
    <link rel="stylesheet" href="../Styles/Accordionstyles.css">

    <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlightvisit
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divComp
        {
            width: 400px !important;
        }

            #divComp div
            {
                width: 400px !important;
            }

        #divDr
        {
            width: 400px !important;
        }

            #divDr div
            {
                width: 400px !important;
            }


        #divRefDr
        {
            width: 400px !important;
        }

            #divRefDr div
            {
                width: 400px !important;
            }
    </style>

    <style type="text/css">
        #overlay
        {
            position: fixed;
            z-index: 90;
            top: 0px;
            left: 0px;
            background-color: #ffffff;
            width: 100%;
            height: 100%;
            filter: alpha(Opacity=80);
            opacity: 0.80;
            -noz-opacity: 0.80;
        }

        #modalprogress
        {
            position: absolute;
            top: 50%;
            left: 40%;
            margin: -11px 0 0 -55px;
            color: white;
        }
    </style>


    <script language="javascript" type="text/javascript">
        function RadResultReport(ReportName, TransNo) {


            var Criteria = " 1=1 ";

            if (TransNo != "") {

                Criteria += ' AND {RAD_TEST_REPORT.RTR_TRANS_NO}=\'' + TransNo + '\'';

                var win = window.open('../CReports/ReportViewer.aspx?ReportName=' + ReportName + '&SelectionFormula=' + Criteria, 'RadReport', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')

                win.focus();

            }

        }

        function ShowWaitingList() {
            var win = window.open('RadTestWaitingListPopup.aspx?PageName=RadWaitList', 'RadWaitList', 'menubar=no,left=100,top=200,height=650,width=1075,scrollbars=1')

            win.focus();

        }



    </script>

    <script language="javascript" type="text/javascript">
        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else {
                return false;
            }
        }

        function ShowXrayMasterPopup() {

            document.getElementById("divXrayMasterPopup").style.display = 'block';


        }

        function HideXrayMasterPopup() {

            document.getElementById("divXrayMasterPopup").style.display = 'none';

        }
        function ShowScanMasterPopup() {

            document.getElementById("divScanMasterPopup").style.display = 'block';


        }

        function HideScanMasterPopup() {

            document.getElementById("divScanMasterPopup").style.display = 'none';

        }



        function ShowMasterPopup() {

            var MasterType = document.getElementById('<%=drpTransType.ClientID%>').value

            if (MasterType == "Scan") {
                ShowScanMasterPopup()
            }
            else {
                ShowXrayMasterPopup()
            }
        }

        function XRayMasterShow(CtrlName, strValue) {

            var MasterType = document.getElementById('<%=drpTransType.ClientID%>').value

            if (MasterType == "Scan") {
                var win = window.open("ScanMasterPopup.aspx?PageName=RadTrans&CtrlName=" + CtrlName + "&Value=" + strValue, "RadTrans", "top=300,left=370,height=500,width=750,toolbar=no,scrollbars=yes,menubar=no");
            }
            else {
                var win = window.open("XRayMasterPopup.aspx?PageName=RadTrans&CtrlName=" + CtrlName + "&Value=" + strValue, "RadTrans", "top=300,left=370,height=500,width=750,toolbar=no,scrollbars=yes,menubar=no");

            }




            win.focus();
            return false;

        }

        function BindXRayMasterDtls(Code, Name, CtrlName) {

            document.getElementById("<%=txtMasterCode.ClientID%>").value = Code;
             document.getElementById("<%=txtMasterDesc.ClientID%>").value = Name;

         }


         function ShowRadHistoryPopup() {

             document.getElementById("divRadHistoryPopup").style.display = 'block';


         }

         function HideRadHistoryPopup() {

             document.getElementById("divRadHistoryPopup").style.display = 'none';

         }




         function ShowWaitingListyPopup() {

             document.getElementById("divWaitingListyPopup").style.display = 'block';


         }

         function HideRadWaitingListPopup() {

             document.getElementById("divWaitingListyPopup").style.display = 'none';

         }





    </script>

    <script type="text/javascript">
        var defaultText = "Enter your text here";
        function WaterMark(txt, evt) {
            if (txt.value.length == 0 && evt.type == "blur") {
                txt.style.color = "gray";
                txt.value = defaultText;
            }
            if (txt.value == defaultText && evt.type == "focus") {
                txt.style.color = "black";
                txt.value = "";
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <input type="hidden" id="hidPermission" runat="server" value="9" />

    <input type="hidden" id="hidEMRID" runat="server" />
    <div style="width: 82%">


        <table width="100%" border="0">
            <tr>
                <td style="text-align: left; width: 20%; vertical-align: top;" class="PageHeader">
                    <asp:Button ID="btnNew" runat="server" CssClass="button gray small" Width="100px" Text="New" OnClick="btnNew_Click" />


                </td>

                <td style="text-align: right; width: 40%; vertical-align: top;">
                    <asp:UpdatePanel ID="UpdatePanel38" runat="server">
                        <ContentTemplate>
                            <asp:Button ID="btnShowWaitingList" runat="server" CssClass="button gray small" Width="100px" Text="Waiting List" OnClick="btnShowWaitingList_Click" />
                            <asp:Button ID="btnRadHistory" runat="server" CssClass="button gray small" Width="100px" Text="History" OnClick="btnRadHistory_Click" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td style="text-align: right; width: 40%;">


                    <asp:Button ID="btnApproved" runat="server" CssClass="button gray small" Width="100px" Text="Approved" OnClick="btnApproved_Click" Visible="false" OnClientClick="return window.confirm('Do you want to Approve?')" />
                    &nbsp;
                         <asp:Button ID="btnReport" runat="server" CssClass="button gray small" Width="100px" Text="Report" OnClick="btnReport_Click" />
                    &nbsp;
                        <asp:Button ID="btnSave" runat="server" CssClass="button red small" Width="100px" Text="Save" OnClick="btnSave_Click" />

                </td>
            </tr>
        </table>

        <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td>
                    <asp:Label ID="lblStatus" runat="server" ForeColor="red" Style="letter-spacing: 1px; font-weight: bold;" CssClass="lblCaption1"></asp:Label>
                </td>
            </tr>

        </table>

        <div style="padding-top: 0px; width: 100%; border: thin; border-color: #f6f6f6; border-style: groove; padding: 5px;">
            <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="290px">
                <tr>
                    <td style="width: 100%; vertical-align: top;">
                        <fieldset style="padding: 5px;">
                            <legend class="lblCaption1" style="font-weight: bold;">Transaction</legend>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">

                                <tr>
                                    <td class="lblCaption1" style="height: 25px;">Trans No
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel18" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtTransNo" runat="server" Width="160px" CssClass="TextBoxStyle" AutoPostBack="true" OnTextChanged="txtTransNo_TextChanged" ondblclick="ShowRadHistoryPopup();"></asp:TextBox>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="txtTransNo" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="lblCaption1" style="height: 25px;">Type
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel19" runat="server">
                                            <ContentTemplate>
                                                <asp:DropDownList ID="drpTransType" runat="server" Style="width: 167px" class="TextBoxStyle" AutoPostBack="true" OnSelectedIndexChanged="drpTransType_SelectedIndexChanged">
                                                    <asp:ListItem Value="Scan">Scan</asp:ListItem>
                                                    <asp:ListItem Value="X-Ray" Selected="True">X-Ray</asp:ListItem>

                                                </asp:DropDownList>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="lblCaption1" style="height: 25px;">Date
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtTransDate" runat="server" Width="72px" CssClass="TextBoxStyle" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                <asp:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" TargetControlID="txtTransDate" Format="dd/MM/yyyy">
                                                </asp:CalendarExtender>
                                                <asp:DropDownList ID="drpTransHour" Style="font-size: 11px;" CssClass="TextBoxStyle" runat="server" Width="36px"></asp:DropDownList>
                                                :
                                             <asp:DropDownList ID="drpTransMin" Style="font-size: 11px;" CssClass="TextBoxStyle" runat="server" Width="36px"></asp:DropDownList>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>

                                    </td>
                                </tr>
                                <tr>
                                    <td class="lblCaption1" style="height: 25px;">Status
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                            <ContentTemplate>
                                                <asp:DropDownList ID="drpStatus" runat="server" Style="width: 167px" class="TextBoxStyle">
                                                    <asp:ListItem Value="A" Selected="True">Active</asp:ListItem>
                                                    <asp:ListItem Value="I">InActive</asp:ListItem>
                                                    <asp:ListItem Value="D">Drafted</asp:ListItem>
                                                </asp:DropDownList>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="lblCaption1" style="height: 25px; width: 120px;">Type
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel30" runat="server">
                                            <ContentTemplate>
                                                <asp:DropDownList ID="drpInvType" runat="server" CssClass="TextBoxStyle" Width="167px">
                                                    <asp:ListItem Value="Cash" Selected="True">Cash</asp:ListItem>
                                                    <asp:ListItem Value="Credit">Credit</asp:ListItem>
                                                </asp:DropDownList>
                                            </ContentTemplate>

                                        </asp:UpdatePanel>
                                    </td>


                                </tr>
                                <tr>
                                    <td class="lblCaption1" style="height: 25px;">PT. Type
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel34" runat="server">
                                            <ContentTemplate>
                                                <asp:DropDownList ID="drpPTType" runat="server" CssClass="TextBoxStyle" Width="167px" BorderWidth="1px">
                                                    <asp:ListItem Value="OP">OP</asp:ListItem>
                                                    <asp:ListItem Value="IP">IP</asp:ListItem>
                                                </asp:DropDownList>
                                            </ContentTemplate>

                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>

                        </fieldset>

                        <fieldset style="padding: 5px;">
                            <legend class="lblCaption1" style="font-weight: bold;">Patient Detals</legend>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="lblCaption1" style="height: 25px;">File No
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel17" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtFileNo" runat="server" Width="150px" CssClass="TextBoxStyle" AutoPostBack="true" OnTextChanged="txtFileNo_TextChanged"></asp:TextBox>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="lblCaption1" style="height: 25px;">Name
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel20" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtFName" runat="server" Width="150px" CssClass="TextBoxStyle"></asp:TextBox>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>


                                </tr>
                                <tr>
                                    <td class="lblCaption1" style="height: 25px;">Sex
                                    </td>
                                    <td class="lblCaption1">
                                        <asp:UpdatePanel ID="UpdatePanel24" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtSex" runat="server" Width="150px" CssClass="TextBoxStyle" MaxLength="10" ReadOnly="true"></asp:TextBox>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                </tr>
                                <tr>
                                    <td class="lblCaption1" style="height: 25px;">DOB
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel15" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtDOB" runat="server" Width="150px" CssClass="TextBoxStyle" MaxLength="10" ReadOnly="true"></asp:TextBox>

                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="lblCaption1" style="height: 25px;">Age
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel21" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtAge" runat="server" Width="15px" Height="15PX" CssClass="TextBoxStyle" MaxLength="10" ReadOnly="true"></asp:TextBox>
                                                <asp:DropDownList ID="drpAgeType" runat="server" Style="width: 50px; font-size: 9px;" class="lblCaption1" ReadOnly="true">
                                                    <asp:ListItem Value="Y">Years</asp:ListItem>
                                                    <asp:ListItem Value="M">Months</asp:ListItem>
                                                    <asp:ListItem Value="D">Days</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:TextBox ID="txtAge1" runat="server" Width="15px" Height="15PX" CssClass="TextBoxStyle" MaxLength="10" ReadOnly="true"></asp:TextBox>
                                                <asp:DropDownList ID="drpAgeType1" runat="server" Style="width: 50px; font-size: 9px;" class="lblCaption1" ReadOnly="true">
                                                    <asp:ListItem Value="Y">Years</asp:ListItem>
                                                    <asp:ListItem Value="M">Months</asp:ListItem>
                                                    <asp:ListItem Value="D">Days</asp:ListItem>
                                                </asp:DropDownList>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="lblCaption1" style="height: 25px;">Nationality</td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel31" runat="server">
                                            <ContentTemplate>
                                                <asp:DropDownList ID="drpNationality" runat="server" CssClass="TextBoxStyle" Width="157px" ReadOnly="true"></asp:DropDownList>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="lblCaption1" style="height: 25px;">Admin ID
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel25" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtAdminID" runat="server" Width="150px" CssClass="TextBoxStyle"></asp:TextBox>
                                            </ContentTemplate>

                                        </asp:UpdatePanel>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="lblCaption1" style="height: 25px;">Ward
                                    </td>
                                    <td colspan="lblCaption1">
                                        <asp:UpdatePanel ID="UpdatePanel26" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtWard" runat="server" Width="150px" CssClass="TextBoxStyle"></asp:TextBox>
                                            </ContentTemplate>

                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="lblCaption1" style="height: 25px;">Room
                                    </td>
                                    <td colspan="lblCaption1">


                                        <asp:UpdatePanel ID="UpdatePanel32" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtRoom" runat="server" Width="150px" CssClass="TextBoxStyle"></asp:TextBox>
                                            </ContentTemplate>

                                        </asp:UpdatePanel>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="lblCaption1" style="height: 25px;">Bed
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel33" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtBed" runat="server" Width="150px" CssClass="TextBoxStyle"></asp:TextBox>
                                            </ContentTemplate>

                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>

                        <fieldset style="padding: 5px;">
                            <legend class="lblCaption1" style="font-weight: bold;">Insurance Detals</legend>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="lblCaption1" style="height: 25px;">Company
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel36" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtProviderID" runat="server" CssClass="TextBoxStyle" ReadOnly="true" Visible="false"></asp:TextBox>
                                                <asp:TextBox ID="txtProviderName" runat="server" Width="90%" CssClass="TextBoxStyle" ReadOnly="true"></asp:TextBox>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="lblCaption1" style="height: 25px;">Policy No
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel37" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtPolicyNo" runat="server" Width="90%" CssClass="TextBoxStyle" ReadOnly="true"></asp:TextBox>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="lblCaption1" style="height: 25px;">ID No
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel45" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtIDNo" runat="server" Width="90%" CssClass="TextBoxStyle" ReadOnly="true"></asp:TextBox>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>

                            </table>
                        </fieldset>
                        <fieldset style="padding: 5px;">
                            <legend class="lblCaption1" style="font-weight: bold;"></legend>
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">


                                <tr>
                                    <td class="lblCaption1" style="height: 25px;" valign="top">Doctor Name
                                    </td>


                                </tr>
                                <tr>
                                    <td valign="top">
                                        <asp:UpdatePanel ID="UpdatePanel35" runat="server">
                                            <ContentTemplate>
                                                <asp:DropDownList ID="drpDoctors" runat="server" Style="width: 225px" class="TextBoxStyle" Enabled="false" ></asp:DropDownList>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="lblCaption1" style="height: 25px;">Ref. Doctor
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel28" runat="server">
                                            <ContentTemplate>
                                                <div id="divRefDr" style="visibility: hidden;"></div>
                                                     <asp:TextBox ID="txtRefDoctorName" runat="server" CssClass="TextBoxStyle" Width="225px" MaxLength="50"  Enabled="false"  ></asp:TextBox>
                                                    <asp:AutoCompleteExtender ID="AutoCompleteExtender3" runat="Server" TargetControlID="txtRefDoctorName" MinimumPrefixLength="1" ServiceMethod="RefGetDoctorName"
                                                        CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divRefDr">
                                                    </asp:AutoCompleteExtender>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>

                                </tr>
                            </table>
                        </fieldset>
                    </td>

                    <td style="width: 80%; vertical-align: top;">
                        <div style="padding-top: 0px; width: 100%; padding: 5px;">
                            <table width="800px" border="0" cellpadding="2" cellspacing="2">
                                <tr>
                                    <td class="lblCaption1" style="height: 25px;">
                                        <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                                            <ContentTemplate>
                                                <asp:Label ID="lblMasterCaption" runat="server" CssClass="label" Text="X-Ray No."></asp:Label>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtMasterNo" runat="server" Width="100px" CssClass="TextBoxStyle" ReadOnly="True"></asp:TextBox>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td class="lblCaption1" style="height: 25px;">
                                        <asp:Label ID="Label2" runat="server" CssClass="label" Text="Code"></asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtMasterCode" runat="server" Width="100px" CssClass="TextBoxStyle" ondblclick="return ShowMasterPopup();"></asp:TextBox>
                                                <asp:TextBox ID="txtMasterDesc" runat="server" Width="300px" CssClass="TextBoxStyle" ondblclick="return ShowMasterPopup();"></asp:TextBox>

                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>



                                </tr>
                                <tr>
                                    <td class="lblCaption1" style="height: 25px;">
                                        <asp:Label ID="Label3" runat="server" CssClass="label" Text="CPT Code"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtCPTCode" runat="server" Width="100px" CssClass="TextBoxStyle" Enabled="false" ></asp:TextBox>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>

                                    <td class="lblCaption1" style="height: 25px;">Req. No.
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtRadReqNo" runat="server" Width="100px" CssClass="TextBoxStyle" Enabled="false" ></asp:TextBox>

                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td class="lblCaption1" style="height: 25px;">Req. Date
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel14" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtReqDate" runat="server" Width="73px" CssClass="TextBoxStyle" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                <asp:CalendarExtender ID="CalendarExtender4" runat="server" Enabled="True" TargetControlID="txtReqDate" Format="dd/MM/yyyy">
                                                </asp:CalendarExtender>
                                                <asp:DropDownList ID="drpReqHour" Style="font-size: 11px;" CssClass="TextBoxStyle" runat="server" Width="40px"></asp:DropDownList>
                                                :
                                             <asp:DropDownList ID="drpReqMin" Style="font-size: 11px;" CssClass="TextBoxStyle" runat="server" Width="40px"></asp:DropDownList>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>

                                    </td>



                                </tr>
                                <tr>
                                    <td class="lblCaption1" style="height: 25px;">Observ. Start
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel46" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtObservStart" runat="server" Width="73px" CssClass="TextBoxStyle" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                <asp:CalendarExtender ID="CalendarExtender5" runat="server" Enabled="True" TargetControlID="txtObservStart" Format="dd/MM/yyyy">
                                                </asp:CalendarExtender>
                                                <asp:DropDownList ID="drpObservStHour" Style="font-size: 11px;" CssClass="TextBoxStyle" runat="server" Width="40px"></asp:DropDownList>
                                                :
                                                     <asp:DropDownList ID="drpObservStMin" Style="font-size: 11px;" CssClass="TextBoxStyle" runat="server" Width="40px"></asp:DropDownList>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>

                                    </td>
                                    <td class="lblCaption1" style="height: 25px;">Observ. End
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel47" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtObservEnd" runat="server" Width="73px" CssClass="TextBoxStyle" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                <asp:CalendarExtender ID="CalendarExtender6" runat="server" Enabled="True" TargetControlID="txtObservEnd" Format="dd/MM/yyyy">
                                                </asp:CalendarExtender>
                                                <asp:DropDownList ID="drpObservEndHour" Style="font-size: 11px;" CssClass="TextBoxStyle" runat="server" Width="40px"></asp:DropDownList>
                                                :
                                                     <asp:DropDownList ID="drpObservEndMin" Style="font-size: 11px;" CssClass="TextBoxStyle" runat="server" Width="40px"></asp:DropDownList>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>

                                    </td>
                                    <td class="lblCaption1" style="height: 25px;">Observ. Date
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel48" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtObservDate" runat="server" Width="73px" CssClass="TextBoxStyle" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                <asp:CalendarExtender ID="CalendarExtender7" runat="server" Enabled="True" TargetControlID="txtObservDate" Format="dd/MM/yyyy">
                                                </asp:CalendarExtender>
                                                <asp:DropDownList ID="drpObservHour" Style="font-size: 11px;" CssClass="TextBoxStyle" runat="server" Width="40px"></asp:DropDownList>
                                                :
                                                     <asp:DropDownList ID="drpObservMin" Style="font-size: 11px;" CssClass="TextBoxStyle" runat="server" Width="40px"></asp:DropDownList>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>

                                    </td>
                                </tr>
                                <tr>
                                    <td class="lblCaption1" style="height: 25px;">Diagnostic Serv
                                   
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                            <ContentTemplate>
                                                <asp:DropDownList ID="drpDiagnosticService" runat="server" Style="width: 160px" class="TextBoxStyle">
                                                </asp:DropDownList>


                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td class="lblCaption1">Result Status
                                    
                                                        
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel50" runat="server">
                                            <ContentTemplate>
                                                <asp:DropDownList ID="drpResultStatus" runat="server" Style="width: 170px" class="TextBoxStyle">
                                                </asp:DropDownList>


                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td class="lblCaption1" style="height: 25px;">
                                        <asp:Label ID="Label7" runat="server" CssClass="label" Text="Invocie Ref #"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel49" runat="server">
                                            <ContentTemplate>

                                                <asp:TextBox ID="txtInvoiceRefNo" runat="server" Width="160px" CssClass="TextBoxStyle"></asp:TextBox>

                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>

                                <tr style="display: none;">
                                    <td class="lblCaption1" style="height: 25px;">
                                        <asp:Label ID="Label4" runat="server" CssClass="label" Text="ServiceCode"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtServCode" runat="server" Width="100px" CssClass="TextBoxStyle"></asp:TextBox>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td class="lblCaption1" style="height: 25px;">
                                        <asp:Label ID="Label5" runat="server" CssClass="label" Text="Name"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtServName" runat="server" Width="200px" CssClass="TextBoxStyle"></asp:TextBox>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td class="lblCaption1" style="height: 25px;">
                                        <asp:Label ID="Label6" runat="server" CssClass="label" Text="Fee"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtFee" runat="server" Width="100px" CssClass="TextBoxStyle"></asp:TextBox>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>


                                </tr>
                            </table>
                        </div>
                        <table width="80%" border="0" cellpadding="5" cellspacing="5">
                            <tr>
                                <td>

                                    <asp:TextBox ID="txtContent" Height="500px" MaxLength="8000" TextMode="MultiLine" runat="server" class="mceEditor" Style="resize: none; width: 100%" />



                                </td>
                            </tr>
                            <tr>
                                <td class="lblCaption1">
                                    <asp:UpdatePanel ID="UpdatePanel51" runat="server">
                                        <ContentTemplate>
                                            Chief Complaint
                                     <asp:TextBox ID="txtChiefComplaints" runat="server" class="TextBoxStyle" Width="80%" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>

                            <tr>


                                <td class="lblCaption1">
                                    <asp:UpdatePanel ID="UpdatePanel12" runat="server">
                                        <ContentTemplate>
                                            Impression 1 &nbsp;&nbsp;&nbsp;&nbsp;
                                                        <asp:TextBox ID="txtImporession" runat="server" class="TextBoxStyle" Width="80%" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td class="lblCaption1">
                                    <asp:UpdatePanel ID="UpdatePanel13" runat="server">
                                        <ContentTemplate>
                                            Impression 2 &nbsp;&nbsp;&nbsp;&nbsp;
                                            <asp:TextBox ID="txtImporession2" runat="server" class="TextBoxStyle" TextMode="MultiLine" Height="30px" Width="80%" Style="resize: none;" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>

                        </table>


                    </td>
                </tr>
            </table>



        </div>
        <br />

        <div id="divXrayMasterPopup" style="display: none; overflow: hidden; border: groove; height: 450px; width: 600px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #eee9e9; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute; left: 400px; top: 150px;">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td style="vertical-align: top;"></td>
                    <td align="right" style="vertical-align: top;">

                        <input type="button" id="btnMsgClose" class="ButtonStyle" style="color: black; border: none;" value=" X " onclick="HideXrayMasterPopup()" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel22" runat="server">
                            <ContentTemplate>

                                <asp:TextBox ID="txtXraySrcDtls" runat="server" Width="400PX" Height="20px" MaxLength="10" CssClass="TextBoxStyle"></asp:TextBox>
                                <asp:Button ID="btnXraySearch" runat="server" CssClass="button gray small" Width="70px" Text="Search" OnClick="btnXraySearch_Click" />


                            </ContentTemplate>

                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
            <div style="padding-top: 0px; width: 100%; height: 330px; overflow: auto; border: thin; border-color: #f6f6f6; border-style: groove;">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td colspan="2">

                            <asp:UpdatePanel ID="UpdatePanel23" runat="server">
                                <ContentTemplate>
                                    <asp:GridView ID="gvXrayMasterPopup" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                        EnableModelValidation="True" Width="100%">
                                        <HeaderStyle CssClass="GridHeader_Blue" />
                                        <RowStyle CssClass="GridRow" />

                                        <Columns>

                                            <asp:TemplateField HeaderText="X-Ray No." HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkgvXRayNo" CssClass="lblCaption1" runat="server" OnClick="XrayMasterEdit_Click" OnClientClick="">

                                                        <asp:Label ID="lblgvXRayReport" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("RXM_REPORT") %>' Visible="false"></asp:Label>
                                                        <asp:Label ID="lblgvXRayNo" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("RXM_XRAY_NO") %>'></asp:Label>
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="X-Ray Code" HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkgvXRayCode" CssClass="lblCaption1" runat="server" OnClick="XrayMasterEdit_Click" OnClientClick="">
                                                        <asp:Label ID="lblgvXRayCode" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("RXM_XRAY_CODE") %>'></asp:Label>
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Description" HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkgvXRayDesc" CssClass="lblCaption1" runat="server" OnClick="XrayMasterEdit_Click" OnClientClick="">
                                                        <asp:Label ID="lblgvXRayDesc" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("RXM_XRAY_DESCRIPTION") %>'></asp:Label>
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>
                                    </asp:GridView>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="gvXrayMasterPopup" />
                                </Triggers>
                            </asp:UpdatePanel>

                        </td>

                    </tr>

                </table>
            </div>
        </div>


        <div id="divScanMasterPopup" style="display: none; overflow: hidden; border: groove; height: 450px; width: 600px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #eee9e9; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute; left: 400px; top: 150px;">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td style="vertical-align: top;"></td>
                    <td align="right" style="vertical-align: top;">

                        <input type="button" id="Button1" class="ButtonStyle" style="color: black; border: none;" value=" X " onclick="HideScanMasterPopup()" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel27" runat="server">
                            <ContentTemplate>

                                <asp:TextBox ID="txtScanSrcDtls" runat="server" Width="490PX" Height="20px" MaxLength="10" CssClass="TextBoxStyle"></asp:TextBox>
                                <asp:Button ID="btnScanSearch" runat="server" CssClass="button gray small" Width="70px" Text="Search" OnClick="btnScanSearch_Click" />


                            </ContentTemplate>

                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
            <div style="padding-top: 0px; width: 100%; height: 330px; overflow: auto; border: thin; border-color: #f6f6f6; border-style: groove;">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td colspan="2">
                            <asp:UpdatePanel ID="UpdatePanel29" runat="server">
                                <ContentTemplate>

                                    <asp:GridView ID="gvScanMasterPopup" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                        EnableModelValidation="True" Width="100%">
                                        <HeaderStyle CssClass="GridHeader_Blue" />
                                        <RowStyle CssClass="GridRow" />

                                        <Columns>

                                            <asp:TemplateField HeaderText="Scan No." HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="LinkButton1" CssClass="lblCaption1" runat="server" OnClick="ScanMasterEdit_Click" OnClientClick="">

                                                        <asp:Label ID="lblgvScanReport" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("RSM_REPORT") %>' Visible="false"></asp:Label>
                                                        <asp:Label ID="lblgvScanNo" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("RSM_SCAN_NO") %>'></asp:Label>
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Scan Code" HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkgvScanCode" CssClass="lblCaption1" runat="server" OnClick="ScanMasterEdit_Click" OnClientClick="">
                                                        <asp:Label ID="lblgvScanCode" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("RSM_SCAN_CODE") %>'></asp:Label>
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Description" HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="LinkButton3" CssClass="lblCaption1" runat="server" OnClick="ScanMasterEdit_Click" OnClientClick="">
                                                        <asp:Label ID="lblgvScanDesc" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("RSM_SCAN_DESCRIPTION") %>'></asp:Label>
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>
                                    </asp:GridView>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="gvScanMasterPopup" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>

                    </tr>

                </table>
            </div>
        </div>


        <div id="divRadHistoryPopup" style="display: none; overflow: hidden; border: groove; height: 450px; width: 800px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #eee9e9; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute; left: 300px; top: 150px;">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td style="vertical-align: top;"></td>
                    <td align="right" style="vertical-align: top;">

                        <input type="button" id="Button2" class="ButtonStyle" style="color: black; border: none;" value=" X " onclick="HideRadHistoryPopup()" />
                    </td>
                </tr>
            </table>
            <div style="padding-top: 0px; width: 100%; height: 400px; overflow: auto; border: thin; border-color: #f6f6f6; border-style: groove;">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td colspan="2">

                            <asp:UpdatePanel ID="UpdatePanel16" runat="server">
                                <ContentTemplate>
                                    <asp:GridView ID="gvRadiologyHistory" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                        EnableModelValidation="True" Width="100%">
                                        <HeaderStyle CssClass="GridHeader_Blue" />
                                        <RowStyle CssClass="GridRow" />
                                        <AlternatingRowStyle CssClass="GridAlterRow" />
                                        <Columns>
                                           
                                            <asp:TemplateField HeaderText="Trans No." HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkgvRadHistTransNo" CssClass="lblCaption1" runat="server" OnClick="RadReport_Click" OnClientClick="">

                                                        <asp:Label ID="lblgvRadHistTransNo" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("RTR_TRANS_NO") %>'></asp:Label>
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Date" HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkgvRadHistDate" CssClass="lblCaption1" runat="server" OnClick="RadReport_Click" OnClientClick="">
                                                        <asp:Label ID="lblgvRadHistDate" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("RTR_DATEDesc") %>'></asp:Label>
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Code" HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkgvRadHistCode" CssClass="lblCaption1" runat="server" OnClick="RadReport_Click" OnClientClick="">
                                                        <asp:Label ID="lblgvRadHistCode" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("RTR_RAD_CODE") %>'></asp:Label>
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Description" HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkgvRadHistDesc" CssClass="lblCaption1" runat="server" OnClick="RadReport_Click" OnClientClick="">
                                                        <asp:Label ID="lblgvRadHistDesc" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("RTR_RAD_DESCRIPTION") %>'></asp:Label>
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Doctor Name" HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkgvRadHistDrName" CssClass="lblCaption1" runat="server" OnClick="RadReport_Click" OnClientClick="">
                                                        <asp:Label ID="lblgvRadHistDrName" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("RTR_DR_NAME") %>'></asp:Label>
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>
                                    </asp:GridView>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="gvRadiologyHistory" />
                                </Triggers>
                            </asp:UpdatePanel>

                        </td>

                    </tr>

                </table>
            </div>
        </div>



        <div id="divWaitingListyPopup" style="display: none; overflow: hidden; border: groove; height: 460px; width: 1000px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #eee9e9; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute; left: 200px; top: 148px;">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td class="lblCaption1" style="font-weight:bold;">Radiology Registred Waiting List </td>
                    <td align="right" style="vertical-align: top;">

                        <input type="button" id="Button3"  class="ButtonStyle" style="color: #005c7b; background-color: #f6f6f6; border: none; border-radius: 0px; font-weight: bold; cursor: pointer;font-size:15px;" value=" X " onclick="HideRadWaitingListPopup()" />
                    </td>
                </tr>
            </table>
             <div style="height:5px;border-bottom-color:red;border-width:1px;border-bottom-style:groove;"></div>
               <div style="height:5px;"></div>
            <table width="100%" border="0" cellpadding="3" cellspacing="3">

                <tr>

                    <td class="lblCaption1" style="height: 25px;">From Date
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel40" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtSrcFromDate" runat="server" Width="70px" CssClass="TextBoxStyle" Height="20PX" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                <asp:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" TargetControlID="txtSrcFromDate" Format="dd/MM/yyyy">
                                </asp:CalendarExtender>

                            </ContentTemplate>
                        </asp:UpdatePanel>

                    </td>
                    <td class="lblCaption1" style="height: 25px;">To Date
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel41" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtSrcToDate" runat="server" Width="70px" CssClass="TextBoxStyle" Height="20PX" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                <asp:CalendarExtender ID="CalendarExtender3" runat="server" Enabled="True" TargetControlID="txtSrcToDate" Format="dd/MM/yyyy">
                                </asp:CalendarExtender>

                            </ContentTemplate>
                        </asp:UpdatePanel>

                    </td>
                    <td class="lblCaption1" style="height: 25px;">File No
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel42" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtSrcFileNo" runat="server" Width="70px" Height="20PX" CssClass="label"></asp:TextBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td class="lblCaption1" style="height: 25px;">Name
                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel43" runat="server">
                            <ContentTemplate>
                                <asp:TextBox ID="txtSrcPTName" runat="server" Width="100px" Height="20PX" CssClass="label"></asp:TextBox>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                     <td class="lblCaption1" style="height: 25px;">Status
                    </td>
                    <td style="width: 200px;">
                        <asp:UpdatePanel ID="UpdatePanel52" runat="server">
                            <ContentTemplate>
                                <asp:DropDownList ID="drpWaitListStatus" runat="server" Style="width: 120px" class="TextBoxStyle">
                                    <asp:ListItem Value="Waiting">Waiting</asp:ListItem>
                                    <asp:ListItem Value="Completed">Completed</asp:ListItem>
                                 </asp:DropDownList>

                            </ContentTemplate>
                        </asp:UpdatePanel>

                    </td>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel44" runat="server">
                            <ContentTemplate>
                                <asp:Button ID="btnWaitingListShow" runat="server" CssClass="button gray small" Text="Show" OnClick="btnWaitingListShow_Click" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>

            </table>
            <div style="padding-top: 0px; width: 100%; height: 400px; overflow: auto; border: thin; border-color: #f6f6f6; border-style: groove;">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td colspan="2">
                            <asp:UpdatePanel ID="UpdatePanel39" runat="server">
                                <ContentTemplate>
                                    <asp:GridView ID="gvWaitingList" runat="server" Width="100%" Visible="false"
                                        AllowSorting="True" AutoGenerateColumns="False"
                                        EnableModelValidation="True">
                                        <HeaderStyle CssClass="GridHeader_Blue" />
                                        <RowStyle CssClass="GridRow" />

                                        <Columns>

                                            <asp:TemplateField HeaderText="No" SortExpression="ID">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkSerial" runat="server" OnClick="WaitingListEdit_Click" ToolTip="Update Waiting List">
                                                        <asp:Label ID="lblTransDate" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("TransDate") %>' Visible="false"></asp:Label>
                                                        <asp:Label ID="lblDoctorID" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("DoctorID") %>' Visible="false"></asp:Label>
                                                        <asp:Label ID="lblServiceID" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("ServiceID") %>' Visible="false"></asp:Label>
                                                        <asp:Label ID="lblID" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("ID") %>'></asp:Label>
                                                    </asp:LinkButton>
                                                </ItemTemplate>

                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="File No" SortExpression="FileNo">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkPTId" runat="server" OnClick="WaitingListEdit_Click" ToolTip="Update Waiting List">

                                                        <asp:Label ID="lblPatientId" CssClass="GridRow" Width="100px" runat="server" Text='<%# Bind("FileNo") %>'></asp:Label>
                                                    </asp:LinkButton>
                                                </ItemTemplate>

                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Name" SortExpression="PatientName">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkPTName" runat="server" OnClick="WaitingListEdit_Click" ToolTip="Update Waiting List">
                                                        <asp:Label ID="Label8" CssClass="GridRow" Width="250px" runat="server" Text='<%# Bind("PatientName") %>'></asp:Label>
                                                    </asp:LinkButton>
                                                </ItemTemplate>

                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Doctor" SortExpression="Doctor">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkMob" runat="server" OnClick="WaitingListEdit_Click" ToolTip="Update Waiting List">
                                                        <asp:Label ID="Label9" CssClass="GridRow" Width="150px" runat="server" Text='<%# Bind("Doctor") %>'></asp:Label>
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Trans Type" SortExpression="TransType">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkDrName" runat="server" OnClick="WaitingListEdit_Click" ToolTip="Update Waiting List">
                                                        <asp:Label ID="lblTransType" CssClass="GridRow" Width="70px" runat="server" Text='<%# Bind("TransType") %>'></asp:Label>
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Patient Type" SortExpression="PatientType">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkDepName" runat="server" OnClick="WaitingListEdit_Click" ToolTip="Update Waiting List">
                                                        <asp:Label ID="Label10" CssClass="GridRow" Width="70px" runat="server" Text='<%# Bind("PatientType") %>'></asp:Label>
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Service Requested" SortExpression="ServiceRequested">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkTime" runat="server" OnClick="WaitingListEdit_Click" ToolTip="Update Waiting List">
                                                        <asp:Label ID="Label11" CssClass="GridRow" runat="server" Text='<%# Bind("ServiceRequested") %>'></asp:Label>
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>

                                    </asp:GridView>




                                    <asp:GridView ID="gvRegisterWaiting" runat="server" Width="100%"
                                        AllowSorting="True" AutoGenerateColumns="False"
                                        EnableModelValidation="True">
                                        <HeaderStyle CssClass="GridHeader_Blue" />
                                        <RowStyle CssClass="GridRow" />

                                        <Columns>

                                            <asp:TemplateField HeaderText="No" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkSerial" runat="server" OnClick="RegisterWaitingEdit_Click" ToolTip="Update Waiting List">



                                                        <asp:Label ID="lblModalityUploadDateDesc" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("RTRM_MODALITY_UPLOAD_DATEDesc") %>' Visible="false"></asp:Label>
                                                        <asp:Label ID="lblModalityUploadTime" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("RTRM_MODALITY_UPLOAD_TimeDesc") %>' Visible="false"></asp:Label>



                                                        <asp:Label ID="lblModalityCheckOutDateDesc" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("RTRM_MODALITY_CHECKOUT_DATEEDesc") %>' Visible="false"></asp:Label>
                                                        <asp:Label ID="lblModalityCheckOutTime" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("RTRM_MODALITY_CHECKOUT_TimeDesc") %>' Visible="false"></asp:Label>



                                                        <asp:Label ID="lblModalityValidatedDateDesc" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("RTRM_MODALITY_VALIDATED_DATEDesc") %>' Visible="false"></asp:Label>
                                                        <asp:Label ID="lblModalityValidatedTime" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("RTRM_MODALITY_VALIDATED_TimeDesc") %>' Visible="false"></asp:Label>

                                                        <asp:Label ID="lblModality" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("RTRM_MODALITY") %>' Visible="false"></asp:Label>


                                                        <asp:Label ID="lblDoctorID" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("RTRM_DR_CODE") %>' Visible="false"></asp:Label>
                                                        <asp:Label ID="lblRefDoctorID" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("RTRM_REF_DR") %>' Visible="false"></asp:Label>
                                                        <asp:Label ID="lblRefDoctorName" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("RTRM_REF_DR_NAME") %>' Visible="false"></asp:Label>

                                                       
                                                        <asp:Label ID="Label1" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("RTRM_CPT_CODE") %>' Visible="false"></asp:Label>
                                                        <asp:Label ID="lblRadReqNo" CssClass="GridRow" runat="server" Text='<%# Bind("RTRM_TRANS_NO") %>'></asp:Label>
                                                    </asp:LinkButton>
                                                </ItemTemplate>

                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Req. Date" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkReqDateDesc" runat="server" OnClick="RegisterWaitingEdit_Click" ToolTip="Update Waiting List">
                                                        <asp:Label ID="lblReqDateDesc" CssClass="GridRow" runat="server" Text='<%# Bind("RTRM_DATEDesc") %>'></asp:Label>
                                                        <asp:Label ID="lblReqDateTimeDesc" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("RTRM_DATETimeDesc") %>' Visible="false"></asp:Label>
                                                    </asp:LinkButton>
                                                </ItemTemplate>

                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="File No" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkPTId" runat="server" OnClick="RegisterWaitingEdit_Click" ToolTip="Update Waiting List">

                                                        <asp:Label ID="lblPatientId" CssClass="GridRow" runat="server" Text='<%# Bind("RTRM_PATIENT_ID") %>'></asp:Label>
                                                    </asp:LinkButton>
                                                </ItemTemplate>

                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Name">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkPTName" runat="server" OnClick="RegisterWaitingEdit_Click" ToolTip="Update Waiting List">
                                                        <asp:Label ID="Label8" CssClass="GridRow" runat="server" Text='<%# Bind("RTRM_PATIENT_NAME") %>'></asp:Label>
                                                    </asp:LinkButton>
                                                </ItemTemplate>

                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Doctor">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkDrName" runat="server" OnClick="RegisterWaitingEdit_Click" ToolTip="Update Waiting List">
                                                        <asp:Label ID="lblDrName" CssClass="GridRow" runat="server" Text='<%# Bind("RTRM_DR_NAME") %>'></asp:Label>
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Invoice ID" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkInvoiceID" runat="server" OnClick="RegisterWaitingEdit_Click" ToolTip="Update Waiting List">
                                                        <asp:Label ID="lblInvoiceID" CssClass="GridRow" runat="server" Text='<%# Bind("RTRM_INVOICE_ID") %>'></asp:Label>
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="CPT Code" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lNKServiceID" runat="server" OnClick="RegisterWaitingEdit_Click" ToolTip="Update Waiting List">
                                                       <asp:Label ID="lblServiceID" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("RTRM_CPT_CODE") %>'  ></asp:Label>
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            
                                            <asp:TemplateField HeaderText="EMR ID" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkEMRID" runat="server" OnClick="RegisterWaitingEdit_Click" ToolTip="Update Waiting List">
                                                        <asp:Label ID="lbEMRID" CssClass="GridRow" runat="server" Text='<%# Bind("RTRM_EMR_ID") %>'></asp:Label>
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Patient Type" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkPTType" runat="server" OnClick="RegisterWaitingEdit_Click" ToolTip="Update Waiting List">
                                                        <asp:Label ID="lblPTType" CssClass="GridRow" runat="server" Text='<%# Bind("RTRM_PT_TYPE") %>'></asp:Label>
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>

                                    </asp:GridView>

                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="gvRegisterWaiting" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>

                    </tr>

                </table>
            </div>
        </div>



    </div>





    <!-- Start for TinyMCE -->

    <script type="text/javascript" src="../tiny_mce/tiny_mce.js"></script>
    <script type="text/javascript" src="../tiny_mce/tinymce.min.js"></script>
    <script type="text/javascript">
        tinyMCE.init({
            // General options textareas
            mode: "textareas",
            theme: "advanced",
            editor_selector: "mceEditor",
            editor_deselector: "mceNoEditor",
            relative_urls: false,
            remove_script_host: false,
            // document_base_url : "http://localhost:1803/DIHWebsite/",
            document_base_url: '<% =getURL() %> ',
            //       setup : function(ed) {
            //          ed.onKeyUp.add(function(ed, e) {   
            //               var strip = (tinyMCE.activeEditor.getContent()).replace(/(<([^>]+)>)/ig,"");
            //               var text =   strip.length + " Characters"
            //        tinymce.DOM.setHTML(tinymce.DOM.get(tinyMCE.activeEditor.id + '_path_row'), text);   
            //         });},

            plugins: "safari,pagebreak,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",

            // Theme options
            theme_advanced_buttons1: "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,fontselect,fontsizeselect",
            theme_advanced_buttons2: "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
            theme_advanced_buttons3: "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
            theme_advanced_buttons4: "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,pagebreak",
            theme_advanced_toolbar_location: "top",
            theme_advanced_toolbar_align: "left",
            theme_advanced_statusbar_location: "bottom",
            theme_advanced_resizing: true,

            // Example content CSS (should be your site CSS)
            content_css: "css/content.css",

            // Drop lists for link/image/media/template dialogs
            template_external_list_url: "lists/template_list.js",
            external_link_list_url: "lists/link_list.js",
            external_image_list_url: "lists/image_list.js",
            media_external_list_url: "lists/media_list.js",

            // Replace values for the template plugin
            template_replace_values: {
                username: "Some User",
                staffid: "991234"
            }
        });

    </script>

    <!-- End for TinyMCE -->

</asp:Content>
