﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Mediplus
{
    public partial class ErrorPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string strError;
                //strError = "Contact System Administrator!"; //Session["ErrorMsg"].ToString();
                strError = Session["ErrorMsg"].ToString();
                lblError.Text = strError;
                Session["ErrorMsg"] = "";
            }
            catch
            {

            }
        }
    }
}